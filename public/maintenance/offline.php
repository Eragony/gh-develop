<?php
if (!file_exists(__DIR__ . '/.active')) {
    header("Location: /");
    die;
}

header('X-AJAX-Control: reset');

$f = function (string $ff, $type): string {
    return "url(data:image/{$type};base64," . base64_encode(file_get_contents(__DIR__ . '/' . $ff)) . ')';
}
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta charset="UTF-8">


	<!-- Set page title -->
	<title>Gest'Hordes - Maintenance</title>

	<style>
        body {
            background-color: #1d2536;
            font-family: sans-serif;
        }

        #header, #content, #footer {
            position: relative;
            margin: 0 auto;
            padding: 0;
            width: 950px;
            overflow: visible;
        }

        #header {
            height: 140px;
        }

        #content {
            background-color: #3b4962;
        }

        #button {
            position: absolute;
            height: 46px;
            width: 137px;
            top: 51px;
            left: 409px;
            cursor: pointer;
            border: 2px solid #3b4962;
            background-color: #3b4962;
            -moz-border-radius: 0;
            border-radius: 0;
            color: #fff;
            font-size: 1.0em;
            text-align: center;
            text-decoration: none;
        }

        #button:hover {
            background-color: #1d2536;
            color: #fff;
        }

        #button > div {
            text-align: center;
            text-transform: uppercase;
            font-size: 19pt;
            line-height: 46px;
            height: 46px;
            font-weight: bolder;
            color: #ddd;
            text-shadow: 0 2px 0 #333469, 0 -2px 0 #333469, 2px 0 0 #333469, -2px 0 0 #333469, 2px 2px 0 #333469, -2px -2px 0 #333469, -2px 2px 0 #333469;
        }

        #button:hover > div {
            padding-top: 3px;
        }

        #content-content {
            position: relative;
            margin: 0 auto;
            padding: 0;
            width: 600px;
            overflow: hidden;
        }

        h1 {
            color: #f0d79e;
            border-bottom: 1px solid #f0d79e;
            font-size: 1.4em;
            text-align: center;
        }

        h1:not(:first-child) {
            margin: 45px 0 0;
        }

        p.text {
            text-align: justify;
            color: white;
            padding-left: 48px;
        }

        p.en {
            background: <?=$f( 'en.png', 'png')?> left top no-repeat;
        }

        p.fr {
            background: <?=$f( 'fr.png', 'png')?> left top no-repeat;
        }

        p.de {
            background: <?=$f( 'de.png', 'png')?> left top no-repeat;
        }

        p.es {
            background: <?=$f( 'es.png', 'png')?> left top no-repeat;
        }
	</style>
</head>
<body>
<div id="header">
	<div id="button" onclick="location.reload();">
		<div>Reload</div>
	</div>
</div>
<div id="content">
	<div id="content-content">
		<div>
			<h1>Maintenance Mode</h1>
			<p class="text en">
				<i>Gest'Hordes</i> is currently in <b>Maintenance Mode</b>. This usually happens when an update is being installed. Please stay patient, normal operations will continue shortly.
				Reload this page or click the button at the top to try again.
			</p>

			<h1>Maintenance en cours</h1>
			<p class="text fr">
				<i>Gest'Hordes</i> est actuellement en <b>cours de maintenance</b>. Cela se produit généralement lorsqu'une mise à jour est installée. Veuillez patienter un instant, le fonctionnement
				normal reviendra sûrement sous peu.
				Rechargez cette page ou cliquez sur le bouton en haut pour réessayer.
			</p>

			<h1>Wartungsmodus</h1>
			<p class="text de">
				<i>Gest'Hordes</i> befindet sich derzeit im <b>Wartungsmodus</b>. Dies geschieht üblicherweise, wenn ein Update installiert wird. Bitte habe einen Augenblick Geduld, es geht sicher
				gleich weiter.
				Lade diese Seite neu oder klicke auf die Schaltfläche ganz oben, um es erneut zu versuchen.
			</p>

			<h1>Modo de Mantenimiento</h1>
			<p class="text es">
				<i>Gest'Hordes</i> está actualmente en <b>Modo de Mantenimiento</b>. Esto suele ocurrir cuando se instala una actualización. Por favor, ten paciencia por un momento, el funcionamiento
				normal seguramente continuará en breve.
				Recarga esta página o haz clic en el botón de la parte superior para intentarlo de nuevo.
			</p>
		</div>
	</div>

</div>
<div id="footer"></div>
</body>
</html>
