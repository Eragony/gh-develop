const Encore = require("@symfony/webpack-encore");
const webpack = require("webpack");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");
const miniCssExtractPlugin = require("mini-css-extract-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");


// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
// /opt/alt/alt-nodejs16/root/usr/bin/npm
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");
}

let local = [];
try {
    local = require("./webpack.config.local");
} catch (e) {
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath(local.output_path ? local.output_path : "public/build/")
    // public path used by the web server to access the output path
    .setPublicPath(local.public_path ? local.public_path : (Encore.isDev() ? "http://gesthordes.localhost/build/" : "/build/"))
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix("build")

    .copyFiles({
                   from: "assets/img", to: (typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? "img/[path][name].[ext]" : "img/[path][name].[contenthash:8].[ext]",
               })

    .configureFilenames({
                            js: (typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? "js/[name].js" : "js/[name].[contenthash:8].js", css: (typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? "css/[name].css" : "css/[name].[contenthash:8].css",
                        })
    .configureImageRule({
                            filename: (typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? "img/[path][name].[ext]" : "img/[path][name].[contenthash:8].[ext]",
                        })
    .configureFontRule({
                           filename: (typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? "fonts/[path][name].[ext]" : "fonts/[path][name].[contenthash:8].[ext]",
                       })

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry("app", "./assets/react/app.tsx")

    .addStyleEntry("style", "./assets/css/style.scss")
    .addStyleEntry("fontawesome/all", "./assets/css/fontawesome/all.css")
    .enableSassLoader()

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    //.enableStimulusBridge('./assets/controllers.json')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    //.enableStimulusBridge("./assets/controllers.json")

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(true)

    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning((typeof (local.hash_filenames) !== "undefined" && !local.hash_filenames) ? false : Encore.isProduction())

    // enables and configure @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = "usage";
        config.corejs = "3.23";
    })


    // uncomment if you use TypeScript
    .enableTypeScriptLoader()

    // uncomment if you use React
    .enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()


;
if (Encore.isDev()) {
    Encore.addPlugin(new ReactRefreshWebpackPlugin())
          .configureDevServerOptions(options => {
              options.allowedHosts = "all";
              options.host = "gesthordes.localhost";
              options.port = 8081;
              options.hot = true;
              options.headers = {
                  "Access-Control-Allow-Origin": "*",
              };
              options.liveReload = false;
              options.static = {
                  watch: false,
              };
              options.watchFiles = {
                  paths: ["src/**/*.php", "templates/**/*"],
              };
          });
}

const config = Encore.getWebpackConfig();

config.module = {
    rules: [

        {
            test: /\.(scss)$/,
            use: [
                {
                    loader: miniCssExtractPlugin.loader,
                },
                {
                    loader: "css-loader",
                },
                {
                    loader: "postcss-loader",
                    options: {
                        postcssOptions: {
                            plugins:() =>[require("autoprefixer")],
                        },
                    },
                },
                {
                    loader: "sass-loader",
                    options: {
                        implementation: require("sass"), // Utiliser Dart Sass
                        sassOptions: {
                            quietDeps: true, // Masque les avertissements dans les dépendances
                        },
                    },
                },
            ],
        }, {
            test: /\.(jsx)$/, exclude: /(node_modules|bower_components)/, use: [{
                loader: "babel-loader", options: {
                    presets: ["@babel/preset-react"],
                },

            }],
        }, {
            test: /\.tsx?$/, exclude: /(node_modules|bower_components)/, use: [{
                loader: "ts-loader", options: {
                    transpileOnly        : true,
                    getCustomTransformers: () => (Encore.isDev() ? {
                        before: [require("react-refresh-typescript").default()],
                    } : {}),
                },
            }],
        }, {
            test: /\.(css)$/, use: [{
                loader: miniCssExtractPlugin.loader,
            }, {
                loader: "css-loader",
            }],
        }, {
            test: /\.svg$/, use: [{
                loader: "file-loader", options: {
                    name: "[name].[hash].[ext]", outputPath: "images/",
                },
            }],
        }, {
            test: /\.(png|jpg|gif)$/, type: "asset/resource",
        }],
};
if (Encore.isDev()) {
    config.devServer = {
        headers     : {
            "Access-Control-Allow-Origin" : "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
        },
        compress    : false,
        host        : "0.0.0.0",  // Écoute sur toutes les adresses IP
        port        : 8081,
        allowedHosts: "all",
        hot         : true,
        liveReload  : false,
    };
}

module.exports = config;

