# This file is a template, and might need editing before it works on your project.
# You can copy and paste this template into a new `.gitlab-ci.yml` file.
# You should not add this template to an existing `.gitlab-ci.yml` file by using the `include:` keyword.
#
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Bash.gitlab-ci.yml

# See https://docs.gitlab.com/ee/ci/yaml/index.html for all available options
# Select image from https://hub.docker.com/_/php/
image: registry.gitlab.com/eragony/gh-docker:latest

stages:
  - before_deploy
  - check_changes
  - build
  - maintenance_beta
  - deploy_beta
  - post_beta
  - validate
  - maintenance_prod
  - deploy_prod
  - post_prod

# 1. Avant de lancer les étapes, on installe les dépendances
before_deploy:
  stage: before_deploy
  environment:
    name: production GH
  script:
    - echo "Debut de la migration pour la nouvelle version"
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
      allow_failure: true
    - when: never



# 2. Build les assets
build_assets:
  stage: build
  environment:
    name: production GH
  script:
    - npm install
    - npm run prod
  artifacts:
    paths:
      - public/build/
  needs:
    - before_deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: on_success
      changes:
        - assets/**/*

# 4. Mise en maintenance de la beta
maintenance_beta:
  stage: maintenance_beta
  environment:
    name: production GH
  script:
    - echo "Mise en maintenance de la beta"
    - export SSHPASS=$SSH_PASSWORD
    # Mise en maintenance
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:maintenance --maintenance on;'
  needs:
    - job: build_assets
      optional: true
    - before_deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: on_success

# 5. Déploiement de la beta
deploy_js:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Déploiement de la beta"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;mkdir -p public/build_new;mv public/build public/build_old;'
    - sshpass -e scp -P 5022 -r public/build/* $URLSERVEUR:public_html/beta_gesthordesv2/public/build_new
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;rm -rf public/build_old;mv public/build_new public/build;'
  needs:
    - maintenance_beta
    - build_assets
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - assets/**/*

# 6. Récupération des sources via git
install_via_git:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "git fetch et git pull"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;git fetch; git fetch --tags origin master; git reset --hard origin/master;'
  needs:
    - maintenance_beta
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 7. Installation des dépendances
install_dependencies:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Installation des dépendances"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:dependencies --environment prod --phar -vvv;'
  needs:
    - install_via_git
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - composer.json
        - composer.lock

# 8. Migration des bdd
migration_data:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Migration des bdd"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:data:migration --environment prod --phar -vvv;php bin/console cache:clear -vvv;'
  needs:
    - install_via_git
    - job: install_dependencies
      optional: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - migrations/**

# 9. Migration des fixtures
migration_fixtures:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Migration des fixtures"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:data_migration -vvv;'
  needs:
    - job: migration_data
      optional: true
    - job: install_dependencies
      optional: true
    - install_via_git
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - src/DataFixtures/data/**
        - src/Command/Command/**
        - src/Command/UpdatePrototypeCommand.php

# 10. Transfert des traductions
transfert_traduction:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Transfert des traductions"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:translation:convert -y -vvv;'
  needs:
    - install_via_git
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - translations/**/*

# 11. Publication du changelog sur la beta
changelog_publication:
  stage: deploy_beta
  environment:
    name: production GH
  script:
    - echo "Publication du changelog"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:changelog -vvv;'
  needs:
    - job: migration_data
      optional: true
    - install_via_git
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - changelog.json

# 12. Clean du cache avant la fin de la maintenance
clean_cache_beta:
  stage: post_beta
  environment:
    name: production GH
  script:
    - echo "Nettoyage du cache avant la fin de la maintenance"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console cache:clear -vvv;'
  needs:
    - job: changelog_publication
      optional: true
    - job: deploy_js
      optional: true
    - job: migration_fixtures
      optional: true
    - job: install_dependencies
      optional: true
    - install_via_git
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 13. Post déploiement de la beta
fin_maintenance_beta:
  stage: post_beta
  environment:
    name: production GH
  script:
    - echo "Fin de la maintenance de la beta"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/beta_gesthordesv2/;php bin/console app:maintenance --maintenance off -vvv;'
  needs:
    - clean_cache_beta
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 14. Validation de la beta
validate_deploy:
  stage: validate
  environment:
    name: production GH
  script:
    - echo "Validation before production deployment"
  when: manual
  needs:
    - fin_maintenance_beta
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: manual

# 15. Mise en maintenance de la prod
maintenance_prod:
  stage: maintenance_prod
  environment:
    name: production GH
  script:
    - echo "Mise en maintenance de la prod"
    - export SSHPASS=$SSH_PASSWORD
    # Mise en maintenance
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:maintenance --maintenance on --discord;'
  needs:
    - validate_deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 16. Déploiement de la prod
deploy_js_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Déploiement de la Prod"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;mkdir -p public/build_new;mv public/build public/build_old;'
    - sshpass -e scp -P 5022 -r public/build/* $URLSERVEUR:public_html/gesthordesV2/public/build_new
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;rm -rf public/build_old;mv public/build_new public/build;'
  needs:
    - maintenance_prod
    - build_assets
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - assets/**/*

# 17. Récupération des sources via git
install_via_git_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "git fetch et git pull"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;git fetch; git fetch --tags origin master; git reset --hard origin/master;'
  needs:
    - maintenance_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 18. Installation des dépendances
install_dependencies_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Installation des dépendances"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:dependencies --environment prod --phar -vvv;'
  needs:
    - install_via_git_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - composer.json
        - composer.lock

# 19. Migration des bdd
migration_data_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Migration des bdd"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:data:migration --environment prod --phar -vvv;php bin/console cache:clear -vvv;'
  needs:
    - install_via_git_prod
    - job: install_dependencies_prod
      optional: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - migrations/**

# 20. Migration des fixtures
migration_fixtures_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Migration des fixtures"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:data_migration -vvv;'
  needs:
    - job: migration_data_prod
      optional: true
    - install_via_git_prod
    - job: install_dependencies_prod
      optional: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - src/DataFixtures/data/**
        - src/Command/Command/**
        - src/Command/UpdatePrototypeCommand.php

# 21. Transfert des traductions
transfert_traduction_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Transfert des traductions"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:translation:convert -y -vvv;'
  needs:
    - install_via_git_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - translations/**/*

# 22. Publication du changelog sur la prod
changelog_publication_prod:
  stage: deploy_prod
  environment:
    name: production GH
  script:
    - echo "Publication du changelog"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:changelog -vvv;'
  needs:
    - job: migration_data_prod
      optional: true
    - install_via_git_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success
      changes:
        - changelog.json

# 23. Clean du cache avant la fin de la maintenance
clean_cache_prod:
  stage: post_prod
  environment:
    name: production GH
  script:
    - echo "Nettoyage du cache avant la fin de la maintenance"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console cache:clear -vvv;'
  needs:
    - job: changelog_publication_prod
      optional: true
    - job: deploy_js_prod
      optional: true
    - job: migration_fixtures_prod
      optional: true
    - job: install_dependencies_prod
      optional: true
    - install_via_git_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 24. Post déploiement de la beta
fin_maintenance_prod:
  stage: post_prod
  environment:
    name: production GH
  script:
    - echo "Post déploiement de la beta"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:maintenance --maintenance off -vvv;'
  needs:
    - clean_cache_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success

# 25. Publication du changelog sur discord
publish_changelog_discord:
  stage: post_prod
  environment:
    name: production GH
  script:
    - echo "Publication du changelog sur discord"
    - export SSHPASS=$SSH_PASSWORD
    - sshpass -e ssh -p 5022 -o stricthostkeychecking=no $URLSERVEUR 'cd public_html/gesthordesV2/;php bin/console app:publication_annonce_changelog -vvv;'
  needs:
    - fin_maintenance_prod
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" '
      when: on_success