<?php

namespace App\Tests\Entity;

use App\Entity\Citoyens;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\JobPrototype;
use App\Entity\User;
use App\Enum\LevelSkill;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class CitoyensTest extends TestCase
{
    final public function testGetChanceCampingMaxWithHunterJob(): void
    {
        // Arrangement
        $citoyen = new Citoyens();
        $job     = $this->createMock(JobPrototype::class);
        $job->method('getId')->willReturn(JobPrototype::job_hunter);
        $citoyen->setJob($job);
        
        // Action & Assertion
        $this->assertEquals(100, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithLowLevelReclusSkill(): void
    {
        // Arrangement
        $citoyen   = new Citoyens();
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skill = $this->createMock(HerosSkillLevel::class);
        $skill->method('getHerosSkillType')->willReturn($skillType);
        $skill->method('getLvl')->willReturn(LevelSkill::DEBUTANT);
        
        $citoyen->addSkill($skill);
        
        // Action & Assertion
        $this->assertEquals(90, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithMasterReclusSkill(): void
    {
        // Arrangement
        $citoyen   = new Citoyens();
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skill = $this->createMock(HerosSkillLevel::class);
        $skill->method('getHerosSkillType')->willReturn($skillType);
        $skill->method('getLvl')->willReturn(LevelSkill::ELITE);
        
        $citoyen->addSkill($skill);
        
        // Action & Assertion
        $this->assertEquals(99, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithMasterReclusSkillAndHunterJob(): void
    {
        // Arrangement
        $citoyen = new Citoyens();
        
        $job = $this->createMock(JobPrototype::class);
        $job->method('getId')->willReturn(JobPrototype::job_hunter);
        $citoyen->setJob($job);
        
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skill = $this->createMock(HerosSkillLevel::class);
        $skill->method('getHerosSkillType')->willReturn($skillType);
        $skill->method('getLvl')->willReturn(LevelSkill::ELITE);
        
        $citoyen->addSkill($skill);
        
        // Action & Assertion
        $this->assertEquals(100, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithMasterReclusSkillType(): void
    {
        // Arrangement
        $citoyen = $this->getMockBuilder(Citoyens::class)
                        ->onlyMethods(['getCitoyen', 'getSkill'])
                        ->getMock();
        
        $mockUser = $this->createMock(User::class);
        
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skillLevelType = $this->createMock(HerosSkillLevel::class);
        $skillLevelType->method('getHerosSkillType')->willReturn($skillType);
        $skillLevelType->method('getLvl')->willReturn(LevelSkill::ELITE);
        
        $skillTypes = new ArrayCollection([$skillLevelType]);
        $mockUser->method('getSkillType')->willReturn($skillTypes);
        
        // On configure les mocks
        $citoyen->method('getCitoyen')->willReturn($mockUser);
        $citoyen->method('getSkill')->willReturn(new ArrayCollection());
        
        // Action & Assertion
        $this->assertEquals(99, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithoutJobOrSkills(): void
    {
        // Arrangement
        $citoyen = $this->getMockBuilder(Citoyens::class)
                        ->onlyMethods(['getCitoyen', 'getSkill'])
                        ->getMock();
        
        // Action & Assertion
        $this->assertEquals(90, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetChanceCampingMaxWithoutMasterReclusSkillType(): void
    {
        // Arrangement
        $citoyen = $this->getMockBuilder(Citoyens::class)
                        ->onlyMethods(['getCitoyen', 'getSkill'])
                        ->getMock();
        
        $mockUser = $this->createMock(User::class);
        
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skillLevelType = $this->createMock(HerosSkillLevel::class);
        $skillLevelType->method('getHerosSkillType')->willReturn($skillType);
        $skillLevelType->method('getLvl')->willReturn(LevelSkill::APPRENTI);
        
        $skillTypes = new ArrayCollection([$skillLevelType]);
        $mockUser->method('getSkillType')->willReturn($skillTypes);
        
        // On configure les mocks
        $citoyen->method('getCitoyen')->willReturn($mockUser);
        $citoyen->method('getSkill')->willReturn(new ArrayCollection());
        
        // Action & Assertion
        $this->assertEquals(90, $citoyen->getChanceCampingMax());
    }
    
    final public function testGetNbCampingProNoobWithReclusSkill(): void
    {
        // Arrangement
        $citoyen   = new Citoyens();
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skill = $this->createMock(HerosSkillLevel::class);
        $skill->method('getHerosSkillType')->willReturn($skillType);
        $skill->method('getLvl')->willReturn(LevelSkill::APPRENTI);
        
        $citoyen->addSkill($skill);
        
        // Action & Assertion
        $this->assertEquals(8, $citoyen->getNbCampingProNoob());
    }
    
    final public function testGetNbCampingProNoobWithReclusSkillType(): void
    {
        // Arrangement
        $citoyen = $this->getMockBuilder(Citoyens::class)
                        ->onlyMethods(['getCitoyen', 'getSkill'])
                        ->getMock();
        
        $mockUser = $this->createMock(User::class);
        
        $skillType = $this->createMock(HerosSkillType::class);
        $skillType->method('getId')->willReturn(HerosSkillType::ID_RECLUS);
        
        $skillLevelType = $this->createMock(HerosSkillLevel::class);
        $skillLevelType->method('getHerosSkillType')->willReturn($skillType);
        $skillLevelType->method('getLvl')->willReturn(LevelSkill::APPRENTI);
        
        $skillTypes = new ArrayCollection([$skillLevelType]);
        $mockUser->method('getSkillType')->willReturn($skillTypes);
        
        // On configure les mocks
        $citoyen->method('getCitoyen')->willReturn($mockUser);
        $citoyen->method('getSkill')->willReturn(new ArrayCollection());
        
        // Action & Assertion
        $this->assertEquals(8, $citoyen->getNbCampingProNoob());
    }
    
    final public function testGetNbCampingProNoobWithoutReclusSkill(): void
    {
        // Arrangement
        $citoyen     = $this->getMockBuilder(Citoyens::class)
                            ->onlyMethods(['getCitoyen', 'getSkill'])
                            ->getMock();
        $mockCitoyen = $this->createMock(User::class);
        
        // On configure le mock pour getCitoyen()
        $citoyen->method('getCitoyen')->willReturn($mockCitoyen);
        
        // Action & Assertion
        $this->assertEquals(6, $citoyen->getNbCampingProNoob());
    }
}
