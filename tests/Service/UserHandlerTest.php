<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Entity\Ville;
use App\Exception\UserNotFoundException;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use App\Service\Generality\MenuHandler;
use App\Service\UserHandler;
use App\Service\Utils\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class UserHandlerTest extends TestCase
{
    
    private EntityManagerInterface $em;
    private Security               $security;
    private RequestStack           $requestStack;
    private UserHandler            $userHandler;
    private SerializerService      $serializerService;
    private MenuHandler            $menuHandler;
    
    protected function setUp(): void
    {
        $this->em                = $this->createMock(EntityManagerInterface::class);
        $this->security          = $this->createMock(Security::class);
        $this->requestStack      = $this->createMock(RequestStack::class);
        $this->serializerService = $this->createMock(SerializerService::class);
        $this->menuHandler       = $this->createMock(MenuHandler::class);
        
        $this->userHandler = new UserHandler($this->em, $this->requestStack, $this->security, $this->serializerService, $this->menuHandler);
    }
    
    public function testGetCurrentUserReturnsUserWhenAuthenticated(): void
    {
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        
        $this->assertEquals($user, $this->userHandler->getCurrentUser());
    }
    
    public function testGetCurrentUserThrowsExceptionWhenNotAuthenticated(): void
    {
        $this->security->method('getUser')->willReturn(null);
        
        $this->expectException(AuthenticationException::class);
        $this->userHandler->getCurrentUser();
    }
    
    public function testGetTownUserDontExist(): void
    {
        $user = new User();
        $user->setMapId(1);
        $this->security->method('getUser')->willReturn($user);
        
        $villeRepositoryMock = $this->createMock(VilleRepository::class);
        $villeRepositoryMock->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn(null);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($villeRepositoryMock);
        
        $this->assertEquals(null, $this->userHandler->getTownOfUser($user));
    }
    
    public function testGetTownUserExist(): void
    {
        $user = new User();
        $user->setMapId(1);
        
        $ville = new Ville(1, 2);
        
        
        $this->security->method('getUser')->willReturn($user);
        
        $villeRepositoryMock = $this->createMock(VilleRepository::class);
        $villeRepositoryMock->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn($ville);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($villeRepositoryMock);
        
        $this->assertEquals($ville, $this->userHandler->getTownOfUser($user));
    }
    
    public function testGetTownUserMapIdNull(): void
    {
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        
        $this->assertEquals(null, $this->userHandler->getTownOfUser($user));
    }
    
    public function testGetUserByIdReturnsUserWhenFound(): void
    {
        $user               = new User();
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->expects($this->once())
                           ->method('findOneBy')
                           ->willReturn($user);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($userRepositoryMock);
        
        $this->assertEquals($user, $this->userHandler->getUserById(1));
    }
    
    public function testGetUserByIdThrowsExceptionWhenNotFound(): void
    {
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->expects($this->once())
                           ->method('findOneBy')
                           ->willReturn(null);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($userRepositoryMock);
        
        $this->expectException(UserNotFoundException::class);
        $this->userHandler->getUserById(1);
    }
    
    public function testResolveUserReturnsCurrentUserWhenNoIdGiven(): void
    {
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        
        $this->assertEquals($user, $this->userHandler->resolveUser(0));
    }
    
    public function testResolveUserReturnsUserByIdWhenIdGiven(): void
    {
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->expects($this->once())
                           ->method('findOneBy')
                           ->willReturn($user);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($userRepositoryMock);
        
        $this->assertEquals($user, $this->userHandler->resolveUser(1));
    }
    
    public function testUserIsNotConnected(): void
    {
        $this->security->method('getUser')->willReturn(null);
        
        $this->expectException(AuthenticationException::class);
        $this->userHandler->verifyUserIsConnected();
    }
    
    public function testVerifyUserIsConnectedReturnsNothingWhenConnected(): void
    {
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        
        $this->assertNull($this->userHandler->verifyUserIsConnected());
    }
    
}
