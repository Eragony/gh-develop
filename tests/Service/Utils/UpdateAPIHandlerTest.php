<?php

namespace App\Tests\Service\Utils;

use App\Entity\Citoyens;
use App\Entity\User;
use App\Entity\Ville;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Service\MyHordesApi;
use App\Service\UserHandler;
use App\Service\Utils\DataService;
use App\Service\Utils\DiscordService;
use App\Service\Utils\UpdateAPIHandler;
use App\Service\Ville\CarteHandlerV2;
use App\Structures\Dto\Api\MH\MyHordesMaj;
use App\Structures\Dto\GH\Utils\NewUser;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Monolog\Logger;
use Monolog\Test\TestCase;
use PHPUnit\Framework\MockObject\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateAPIHandlerTest extends TestCase
{
    private EntityManagerInterface $entityManagerMock;
    private MyHordesApi            $myHordesApiMock;
    private UserHandler            $userHandlerMock;
    private TranslatorInterface    $translatorMock;
    private LoggerInterface        $LoggerMock;
    private CarteHandlerV2         $carteHandlerV2Mock;
    private UpdateAPIHandler       $updateAPIHandler;
    private DataService            $dataServiceMock;
    private DiscordService         $discordServiceMock;
    
    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->entityManagerMock  = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $this->myHordesApiMock    = $this->getMockBuilder(MyHordesApi::class)->disableOriginalConstructor()->getMock();
        $this->translatorMock     = $this->getMockBuilder(Translator::class)->disableOriginalConstructor()->getMock();
        $this->LoggerMock         = $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->userHandlerMock    = $this->getMockBuilder(UserHandler::class)->disableOriginalConstructor()->getMock();
        $this->carteHandlerV2Mock = $this->getMockBuilder(CarteHandlerV2::class)->disableOriginalConstructor()->getMock();
        $this->dataServiceMock    = $this->createMock(DataService::class);
        $this->discordServiceMock = $this->createMock(DiscordService::class);
        
        $this->dataServiceMock->method('getListObjetIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListBatIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListChantierIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListJobIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListPictoIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListMortIndexed')->willReturn([]);
        $this->dataServiceMock->method('getListCleanUpTypeIndexed')->willReturn([]);
        
        
        $this->updateAPIHandler = new UpdateAPIHandler($this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock);
        
    }
    
    public function testAppelApiGestionAttackMaintenanceReturnsMajOnSuccess(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => null]);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $result = $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
        
        $this->assertInstanceOf(MyHordesMaj::class, $result);
        
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnApiException(): void
    {
        $token = 'testToken';
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willThrowException(new JsonException());
        
        $this->expectException(MyHordesMajApiException::class);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnInvalidUserKey(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => 'invalid_userkey']);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $this->expectException(MyHordesMajException::class);
        $this->expectExceptionCode(MyHordesMajException::INVALID_USER_KEY);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnNightlyAttack(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => 'nightly_attack']);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $this->expectException(MyHordesAttackException::class);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnOtherError(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => 'other_error']);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $this->expectException(MyHordesMajException::class);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnRateLimitReached(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => 'rate_limit_reached']);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $this->expectException(MyHordesMajException::class);
        $this->expectExceptionCode(MyHordesMajException::TOO_MANY_REQUESTS);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testAppelApiGestionAttackMaintenanceThrowsExceptionOnServerError(): void
    {
        $token       = 'testToken';
        $apiResponse = json_encode(['error' => 'server_error']);
        
        $this->myHordesApiMock->expects($this->once())
                              ->method('getMeTown')
                              ->with($token)
                              ->willReturn($apiResponse);
        
        $this->expectException(MyHordesMajException::class);
        $this->expectExceptionCode(MyHordesMajException::INTERNAL_ERROR);
        
        $this->updateAPIHandler->appelApiGestionAttackMaintenance('getMeTown', $token);
    }
    
    public function testConstructor(): void
    {
        $this->assertInstanceOf(UpdateAPIHandler::class, $this->updateAPIHandler);
    }
    
    public function testUpdateAllWithMapIdNotNull(): void
    {
        $request = new Request();
        $token   = 'someToken';
        $maj     = $this->createMock(MyHordesMaj::class);
        $user    = new User();
        
        $maj->method('getMapId')->willReturn(1);
        
        $updateAPIHandlerMock = $this->createPartialMock(
            UpdateAPIHandler::class,
            ['appelApiGestionAttackMaintenance', 'createUpdateUserMaj', 'updateTown'],
        );
        
        $updateAPIHandlerMock->method('appelApiGestionAttackMaintenance')
                             ->with('getMeTown', $token)
                             ->willReturn($maj);
        
        $updateAPIHandlerMock->method('createUpdateUserMaj')
                             ->with($request, $maj, $token)
                             ->willReturn($user);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateTown');
        
        // Utiliser l'instance normale pour l'appel réel
        $updateAPIHandlerMock->updateAll($request, $token);
    }
    
    public function testUpdateAllWithMapIdNull(): void
    {
        $request = new Request();
        $token   = 'someToken';
        $maj     = $this->createMock(MyHordesMaj::class);
        $user    = new User();
        
        $maj->method('getMapId')->willReturn(null);
        
        $updateAPIHandlerMock = $this->createPartialMock(
            UpdateAPIHandler::class,
            ['appelApiGestionAttackMaintenance', 'createUpdateUserMaj', 'updateTown'],
        );
        
        $updateAPIHandlerMock->method('appelApiGestionAttackMaintenance')
                             ->with('getMeTown', $token)
                             ->willReturn($maj);
        
        $updateAPIHandlerMock->method('createUpdateUserMaj')
                             ->with($request, $maj, $token)
                             ->willReturn($user);
        
        $updateAPIHandlerMock->expects($this->never())
                             ->method('updateTown');
        
        // Utiliser l'instance normale pour l'appel réel
        $updateAPIHandlerMock->updateAll($request, $token);
    }
    
    public function testUpdateTownExceptionMajUser(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        $ville = new Ville(1, 2);
        $ville->setJour(1);
        
        $maj->method('getMapId')->willReturn(1);
        $this->dataServiceMock->method('getDataVille')->willReturn($ville);
        $this->dataServiceMock->expects($this->once())->method('updateZoneVue')->with($ville);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails', 'updateUserDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $this->isInstanceOf(Ville::class), $user, 0, false);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateUserDetails')
                             ->with($maj, $user)->willThrowException(new \Exception('Test exception'));
        
        $this->entityManagerMock->expects($this->once())->method('persist');
        $this->entityManagerMock->expects($this->once())->method('flush');
        
        $this->expectException(\Exception::class);
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownExistingVille(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        $ville = new Ville(1, 2);
        $ville->setJour(1);
        
        $maj->method('getMapId')->willReturn(1);
        $this->dataServiceMock->method('getDataVille')->willReturn($ville);
        $this->dataServiceMock->expects($this->once())->method('updateZoneVue')->with($ville);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails', 'updateUserDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $this->isInstanceOf(Ville::class), $user, 0, false);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateUserDetails')
                             ->with($maj, $user);
        
        $this->entityManagerMock->expects($this->once())->method('persist');
        $this->entityManagerMock->expects($this->once())->method('flush');
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownHandleExceptionDuringPersist(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        
        $maj->method('getMapId')->willReturn(1);
        $this->dataServiceMock->method('getDataVille')->willReturn(null);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $this->isInstanceOf(Ville::class), $user, 0, true);
        
        $this->entityManagerMock->method('persist')->willThrowException(new \Exception('Test exception'));
        
        $this->expectException(\Exception::class);
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownMapIdNull(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        
        $maj->method('getMapId')->willReturn(null);
        $this->dataServiceMock->expects($this->never())->method('getDataVille')->willReturn(null);
        
        $this->updateAPIHandler->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownNewVille(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        
        $maj->method('getMapId')->willReturn(1);
        
        $this->dataServiceMock->method('getDataVille')->willReturn(null);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails', 'updateUserDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $this->isInstanceOf(Ville::class), $user, 0, true);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateUserDetails')
                             ->with($maj, $user);
        
        $this->entityManagerMock->expects($this->once())->method('persist');
        $this->entityManagerMock->expects($this->once())->method('flush');
        
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownShouldUpdateAll(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        $ville = new Ville(1, 2);
        $ville->setJour(2);
        
        $maj->method('getMapId')->willReturn(1);
        $this->dataServiceMock->method('getDataVille')->willReturn($ville);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails', 'updateAllDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $ville, $user, 0, false);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateAllDetails')
                             ->with($ville, $token);
        
        $this->entityManagerMock->expects($this->once())->method('persist');
        $this->entityManagerMock->expects($this->once())->method('flush');
        
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateTownShouldUpdateUserDetails(): void
    {
        $maj   = $this->createMock(MyHordesMaj::class);
        $user  = $this->createMock(User::class);
        $token = 'dummy_token';
        $ville = new Ville(1, 2);
        $ville->setJour(1);
        
        $maj->method('getMapId')->willReturn(1);
        $this->dataServiceMock->method('getDataVille')->willReturn($ville);
        
        $updateAPIHandlerMock = $this->getMockBuilder(UpdateAPIHandler::class)
                                     ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                     ->onlyMethods(['updateVilleDetails', 'updateUserDetails'])
                                     ->getMock();
        
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateVilleDetails')
                             ->with($maj, $this->isInstanceOf(Ville::class), $user, 0, false);
        
        $updateAPIHandlerMock->expects($this->once())
                             ->method('updateUserDetails')
                             ->with($maj, $user);
        
        $this->entityManagerMock->expects($this->once())->method('persist');
        $this->entityManagerMock->expects($this->once())->method('flush');
        
        
        $updateAPIHandlerMock->updateTown($maj, $user, $token);
    }
    
    public function testUpdateVilleDetails(): void
    {
        // Création des mocks
        $maj   = $this->createMock(MyHordesMaj::class);
        $ville = new Ville(1, 2);
        $ville->setJour(1)
              ->setCountMaj(10)
              ->setCountMajScript(2);
        $user = $this->createMock(User::class);
        
        // Création du mock pour UpdateAPIHandler
        $updateAPIHandler = $this->getMockBuilder(UpdateAPIHandler::class)
                                 ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                 ->onlyMethods([
                                                   'updateVilleGeneral',
                                                   'updateBanque',
                                                   'updateChantier',
                                                   'updateAvancementChantier',
                                                   'updateDefense',
                                                   'updateEstimation',
                                                   'updateJournal',
                                                   'updateUpChantier',
                                                   'updateCitoyens',
                                                   'updateExpeHordes',
                                                   'updateZonesMap',
                                               ])
                                 ->getMock();
        
        // Configuration des attentes sur les méthodes mockées
        $updateAPIHandler->expects($this->once())->method('updateVilleGeneral')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateBanque')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateAvancementChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateDefense')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateEstimation')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateJournal')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->never())->method('updateUpChantier'); // Par défaut, on n'appelle pas cette méthode
        $updateAPIHandler->expects($this->once())->method('updateCitoyens')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateExpeHordes')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateZonesMap')->with($maj, $ville, [], false, $user);
        
        // Appel de la méthode testée
        $updateAPIHandler->updateVilleDetails($maj, $ville, $user, 0, false, 1);
        
        $this->assertEquals(11, $ville->getCountMaj());
        $this->assertEquals(2, $ville->getCountMajScript());
    }
    
    public function testUpdateVilleDetailsWithScript(): void
    {
        // Création des mocks
        $maj   = $this->createMock(MyHordesMaj::class);
        $ville = new Ville(1, 2);
        $ville->setJour(1)
              ->setCountMaj(10)
              ->setCountMajScript(2);
        $user = $this->createMock(User::class);
        
        // Création du mock pour UpdateAPIHandler
        $updateAPIHandler = $this->getMockBuilder(UpdateAPIHandler::class)
                                 ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                 ->onlyMethods([
                                                   'updateVilleGeneral',
                                                   'updateBanque',
                                                   'updateChantier',
                                                   'updateAvancementChantier',
                                                   'updateDefense',
                                                   'updateEstimation',
                                                   'updateJournal',
                                                   'updateUpChantier',
                                                   'updateCitoyens',
                                                   'updateExpeHordes',
                                                   'updateZonesMap',
                                               ])
                                 ->getMock();
        
        // Configuration des attentes sur les méthodes mockées
        $updateAPIHandler->expects($this->once())->method('updateVilleGeneral')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateBanque')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateAvancementChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateDefense')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateEstimation')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateJournal')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->never())->method('updateUpChantier'); // Par défaut, on n'appelle pas cette méthode
        $updateAPIHandler->expects($this->once())->method('updateCitoyens')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateExpeHordes')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateZonesMap')->with($maj, $ville, [], false, $user);
        
        // Appel de la méthode testée
        $updateAPIHandler->updateVilleDetails($maj, $ville, $user, 1, false, 1);
        
        $this->assertEquals(11, $ville->getCountMaj());
        $this->assertEquals(3, $ville->getCountMajScript());
    }
    
    public function testUpdateVilleDetailsWithUpChantier(): void
    {
        // Création des mocks
        $maj   = $this->createMock(MyHordesMaj::class);
        $ville = $this->createMock(Ville::class);
        $user  = $this->createMock(User::class);
        
        // Création du mock pour UpdateAPIHandler
        $updateAPIHandler = $this->getMockBuilder(UpdateAPIHandler::class)
                                 ->setConstructorArgs([$this->entityManagerMock, $this->myHordesApiMock, $this->userHandlerMock, $this->translatorMock, $this->LoggerMock, $this->carteHandlerV2Mock, $this->dataServiceMock, $this->discordServiceMock])
                                 ->onlyMethods([
                                                   'updateVilleGeneral',
                                                   'updateBanque',
                                                   'updateChantier',
                                                   'updateAvancementChantier',
                                                   'updateDefense',
                                                   'updateEstimation',
                                                   'updateJournal',
                                                   'updateUpChantier',
                                                   'updateCitoyens',
                                                   'updateExpeHordes',
                                                   'updateZonesMap',
                                               ])
                                 ->getMock();
        
        // Configuration des attentes sur les méthodes mockées
        $updateAPIHandler->expects($this->once())->method('updateVilleGeneral')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateBanque')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateAvancementChantier')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateDefense')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateEstimation')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateJournal')->with($maj, $ville, $user);
        $updateAPIHandler->expects($this->once())->method('updateUpChantier'); // Par défaut, on n'appelle pas cette méthode
        $updateAPIHandler->expects($this->once())->method('updateCitoyens')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateExpeHordes')->with($maj, $ville, [], $user);
        $updateAPIHandler->expects($this->once())->method('updateZonesMap')->with($maj, $ville, [], false, $user);
        
        // Configuration de l'objet $ville
        $ville->method('getJour')->willReturn(2);
        $ville->expects($this->once())->method('incrementCountMaj');
        $ville->expects($this->never())->method('incrementCountMajScript');
        
        // Appel de la méthode testée
        $updateAPIHandler->updateVilleDetails($maj, $ville, $user, 0, false, 2);
    }
    
    public function testUpdateVilleGeneral(): void
    {
        $arrayVille = [
            'map' => [
                'bonusPts' => 50,
                'days'     => 5,
                'guide'    => 1,
                'hei'      => 25,
                'wid'      => 25,
                'phase'    => 'beta',
                'season'   => 16,
                'custom'   => false,
                'city'     => [
                    'name'   => 'Test City',
                    'x'      => 5,
                    'y'      => 6,
                    'chaos'  => false,
                    'devast' => false,
                    'door'   => false,
                    'hard'   => false,
                    'water'  => 20,
                ],
            ],
        ];
        
        $maj = new MyHordesMaj($arrayVille);
        
        $ville = new Ville(1, 2);
        $user  = new User();
        
        // Appel de la méthode à tester
        $this->updateAPIHandler->updateVilleGeneral($maj, $ville, $user);
        
        // Assertions pour vérifier les modifications apportées à Ville
        $this->assertEquals(5, $ville->getJour());
        $this->assertEquals('Test City', $ville->getNom());
        $this->assertEquals(5, $ville->getPosX());
        $this->assertEquals(6, $ville->getPosY());
        $this->assertEquals(25, $ville->getWeight());
        $this->assertEquals(25, $ville->getHeight());
        $this->assertEquals(50, $ville->getBonus());
        $this->assertFalse($ville->isPrived());
        $this->assertFalse($ville->isPorte());
        $this->assertFalse($ville->isHard());
        $this->assertFalse($ville->isChaos());
        $this->assertFalse($ville->isDevast());
        $this->assertEquals(20, $ville->getWater());
        $this->assertEquals(16, $ville->getSaison());
        $this->assertEquals('beta', $ville->getPhase());
        $this->assertSame($user, $ville->getUpdateBy());
        $this->assertInstanceOf(DateTime::class, $ville->getDateTime());
        $this->assertEquals(null, $ville->getDayDevast());
    }
    
    public function testUpdateVilleGeneralChaos(): void
    {
        $arrayVille = [
            'map' => [
                'bonusPts' => 50,
                'days'     => 5,
                'guide'    => 1,
                'hei'      => 25,
                'wid'      => 25,
                'phase'    => 'beta',
                'season'   => 16,
                'custom'   => false,
                'city'     => [
                    'name'   => 'Test City',
                    'x'      => 5,
                    'y'      => 6,
                    'chaos'  => true,
                    'devast' => false,
                    'door'   => false,
                    'hard'   => false,
                    'water'  => 20,
                ],
            ],
        ];
        
        $maj = new MyHordesMaj($arrayVille);
        
        $ville = new Ville(1, 2);
        $user  = new User();
        
        // Appel de la méthode à tester
        $this->updateAPIHandler->updateVilleGeneral($maj, $ville, $user);
        
        // Assertions pour vérifier les modifications apportées à Ville
        $this->assertEquals(null, $ville->getDayDevast());
        $this->assertEquals(5, $ville->getDayChaos());
    }
    
    public function testUpdateVilleGeneralDevast(): void
    {
        $arrayVille = [
            'map' => [
                'bonusPts' => 50,
                'days'     => 5,
                'guide'    => 1,
                'hei'      => 25,
                'wid'      => 25,
                'phase'    => 'beta',
                'season'   => 16,
                'custom'   => false,
                'city'     => [
                    'name'   => 'Test City',
                    'x'      => 5,
                    'y'      => 6,
                    'chaos'  => true,
                    'devast' => true,
                    'door'   => false,
                    'hard'   => false,
                    'water'  => 20,
                ],
            ],
        ];
        
        $maj = new MyHordesMaj($arrayVille);
        
        $ville = new Ville(1, 2);
        $ville->setDayChaos(3);
        $user = new User();
        
        // Appel de la méthode à tester
        $this->updateAPIHandler->updateVilleGeneral($maj, $ville, $user);
        
        // Assertions pour vérifier les modifications apportées à Ville
        $this->assertEquals(5, $ville->getDayDevast());
        $this->assertEquals(3, $ville->getDayChaos());
    }
    
    public function testUpdateVilleGeneralException(): void
    {
        
        
        $maj = $this->createMock(MyHordesMaj::class);
        
        $maj->method('getMap')->willThrowException(new \Exception('Test exception'));
        
        $ville = new Ville(1, 2);
        $ville->setDayChaos(3);
        $user = new User();
        try {
            // Appel de la méthode à tester
            $this->updateAPIHandler->updateVilleGeneral($maj, $ville, $user);
            $this->fail('Exception should have been thrown');
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Exception::class, $e);
        }
    }
    
    public function testUpdateVilleGeneralVilleDejaDevastChaos(): void
    {
        $arrayVille = [
            'map' => [
                'bonusPts' => 50,
                'days'     => 5,
                'guide'    => 1,
                'hei'      => 25,
                'wid'      => 25,
                'phase'    => 'beta',
                'season'   => 16,
                'custom'   => false,
                'city'     => [
                    'name'   => 'Test City',
                    'x'      => 5,
                    'y'      => 6,
                    'chaos'  => true,
                    'devast' => true,
                    'door'   => false,
                    'hard'   => false,
                    'water'  => 20,
                ],
            ],
        ];
        
        $maj = new MyHordesMaj($arrayVille);
        
        $ville = new Ville(1, 2);
        $ville->setChaos(true);
        $ville->setDevast(false);
        $user = new User();
        
        // Appel de la méthode à tester
        $this->updateAPIHandler->updateVilleGeneral($maj, $ville, $user);
        
        // Assertions pour vérifier les modifications apportées à Ville
        $this->assertEquals(5, $ville->getDayDevast());
        $this->assertEquals(null, $ville->getDayChaos());
        
        $ville1 = new Ville(1, 2);
        $ville1->setChaos(true);
        $ville1->setDevast(true);
        $user = new User();
        
        // Appel de la méthode à tester
        $this->updateAPIHandler->updateVilleGeneral($maj, $ville1, $user);
        
        // Assertions pour vérifier les modifications apportées à Ville
        $this->assertEquals(null, $ville1->getDayDevast());
        $this->assertEquals(null, $ville1->getDayChaos());
    }
    
    public function testUserCreationWithExistingUser(): void
    {
        $request = $this->createMock(Request::class);
        $maj     = $this->createMock(MyHordesMaj::class);
        $token   = 'testToken';
        
        $maj->method('getId')->willReturn(1);
        $maj->method('getAvatar')->willReturn('avatar');
        $maj->method('getLocale')->willReturn('en');
        $maj->method('getName')->willReturn('name');
        $maj->method('getMapId')->willReturn(1);
        
        $existingUser = new User();
        $existingUser->setIdMyHordes(1)
                     ->setPseudo("pseudo")
                     ->setLang("fr");
        
        $citoyens  = new Citoyens();
        $villePrec = new Ville(0, 2);
        $citoyens->setVille($villePrec);
        
        $this->dataServiceMock->method('getFindOneUser')->willReturn($existingUser);
        $this->dataServiceMock->method('getDerniereVille')->willReturn($citoyens);
        
        
        $this->entityManagerMock->expects($this->once())
                                ->method('persist')
                                ->with($existingUser);
        $this->entityManagerMock->expects($this->once())
                                ->method('flush');
        $this->entityManagerMock->expects($this->once())
                                ->method('refresh')
                                ->with($existingUser);
        
        $user = $this->updateAPIHandler->createUpdateUserMaj($request, $maj, $token);
        
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('name', $user->getPseudo());
        $this->assertEquals('avatar', $user->getAvatar());
        $this->assertEquals('fr', $user->getLang());
        $this->assertEquals(1, $user->getMapId());
    }
    
    public function testUserCreationWithExistingUserWithFailedPersist(): void
    {
        $request = $this->createMock(Request::class);
        $maj     = $this->createMock(MyHordesMaj::class);
        $token   = 'testToken';
        
        $maj->method('getId')->willReturn(1);
        $maj->method('getAvatar')->willReturn('avatar');
        $maj->method('getLocale')->willReturn('en');
        $maj->method('getName')->willReturn('name');
        $maj->method('getMapId')->willReturn(1);
        
        $existingUser = new User();
        $existingUser->setIdMyHordes(1);
        
        $citoyens  = new Citoyens();
        $villePrec = new Ville(0, 2);
        $citoyens->setVille($villePrec);
        
        $this->dataServiceMock->method('getFindOneUser')->willReturn($existingUser);
        $this->dataServiceMock->method('getDerniereVille')->willReturn($citoyens);
        
        $this->entityManagerMock->expects($this->once())
                                ->method('persist')
                                ->with($existingUser)
                                ->willThrowException(new \Exception());
        
        $this->entityManagerMock->expects($this->never())
                                ->method('flush');
        
        // Expect that the updateUser method will throw an exception
        $this->expectException(\Exception::class);
        
        // Call the method you are testing with the mocked EntityManager
        $this->updateAPIHandler->createUpdateUserMaj($request, $maj, $token);
    }
    
    public function testUserCreationWithLocaleFallback(): void
    {
        $request = $this->createMock(Request::class);
        $maj     = $this->createMock(MyHordesMaj::class);
        $token   = 'testToken';
        
        $maj->method('getId')->willReturn(1);
        $maj->method('getAvatar')->willReturn('avatar');
        $maj->method('getName')->willReturn('name');
        $maj->method('getMapId')->willReturn(1);
        
        $this->dataServiceMock->method('getFindOneUser')->willReturn(null);
        
        $newUser = new NewUser();
        $newUser->setIdMh(1)
                ->setPseudo("name")
                ->setAvatar("avatar")
                ->setLang("en")
                ->setMapId(1);
        
        $user = new User();
        $user->setIdMyHordes(1)
             ->setPseudo("name")
             ->setAvatar("avatar")
             ->setLang("en")
             ->setMapId(1);
        
        $this->userHandlerMock->method('createNewUserFromApi')->with($newUser, true)->willReturn($user);
        
        $updateAPIHandlerMock = $this->createPartialMock(
            UpdateAPIHandler::class,
            ['updateUser'],
        );
        
        $updateAPIHandlerMock->expects($this->never())
                             ->method('updateUser');
        
        $userMod = new User();
        $userMod->setIdMyHordes(1)
                ->setPseudo("name")
                ->setAvatar("avatar")
                ->setLang("fr")
                ->setMapId(1);
        
        $this->userHandlerMock->method('sauvegardeLang')->with($user, 'fr');
        
        $user = $this->updateAPIHandler->createUpdateUserMaj($request, $maj, $token);
        
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('name', $user->getPseudo());
        $this->assertEquals('avatar', $user->getAvatar());
        $this->assertEquals('en', $user->getLang());
        $this->assertEquals(1, $user->getMapId());
    }
    
    public function testUserCreationWithNewUser(): void
    {
        $request = $this->createMock(Request::class);
        $maj     = $this->createMock(MyHordesMaj::class);
        $token   = 'testToken';
        
        $maj->method('getId')->willReturn(1);
        $maj->method('getAvatar')->willReturn('avatar');
        $maj->method('getLocale')->willReturn('en');
        $maj->method('getName')->willReturn('name');
        $maj->method('getMapId')->willReturn(1);
        
        $newUser = new NewUser();
        $newUser->setIdMh(1)
                ->setPseudo("name")
                ->setAvatar("avatar")
                ->setLang("en")
                ->setMapId(1);
        
        $user = new User();
        $user->setIdMyHordes(1)
             ->setPseudo("name")
             ->setAvatar("avatar")
             ->setLang("en")
             ->setMapId(1);
        
        $this->userHandlerMock->method('createNewUserFromApi')->with($newUser, true)->willReturn($user);
        
        $updateAPIHandlerMock = $this->createPartialMock(
            UpdateAPIHandler::class,
            ['updateUser'],
        );
        
        $updateAPIHandlerMock->expects($this->never())
                             ->method('updateUser');
        
        $user = $this->updateAPIHandler->createUpdateUserMaj($request, $maj, $token);
        
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('name', $user->getPseudo());
        $this->assertEquals('avatar', $user->getAvatar());
        $this->assertEquals('en', $user->getLang());
        $this->assertEquals(1, $user->getMapId());
    }
    
    public function testUserCreationWithUserNothingLangWithLocaleFallback(): void
    {
        $request = $this->createMock(Request::class);
        $maj     = $this->createMock(MyHordesMaj::class);
        $token   = 'testToken';
        
        $maj->method('getId')->willReturn(1);
        $maj->method('getAvatar')->willReturn('avatar');
        $maj->method('getName')->willReturn('name');
        $maj->method('getLocale')->willReturn("it");
        $maj->method('getMapId')->willReturn(1);
        
        
        $existingUser = new User();
        $existingUser->setIdMyHordes(1)
                     ->setPseudo("pseudo")
                     ->setLang(null);
        
        $citoyens  = new Citoyens();
        $villePrec = new Ville(0, 2);
        $citoyens->setVille($villePrec);
        
        $this->dataServiceMock->method('getFindOneUser')->willReturn($existingUser);
        $this->dataServiceMock->method('getDerniereVille')->willReturn($citoyens);
        
        
        $this->entityManagerMock->expects($this->once())
                                ->method('persist')
                                ->with($existingUser);
        $this->entityManagerMock->expects($this->once())
                                ->method('flush');
        $this->entityManagerMock->expects($this->once())
                                ->method('refresh')
                                ->with($existingUser);
        
        
        $existingUser->setIdMyHordes(1)
                     ->setPseudo("name")
                     ->setAvatar("avatar")
                     ->setLang("fr")
                     ->setMapId(1);
        
        $this->userHandlerMock->method('sauvegardeLang')->with($existingUser, 'fr');
        
        $user = $this->updateAPIHandler->createUpdateUserMaj($request, $maj, $token);
        
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('name', $user->getPseudo());
        $this->assertEquals('avatar', $user->getAvatar());
        $this->assertEquals('fr', $user->getLang());
        $this->assertEquals(1, $user->getMapId());
    }
}