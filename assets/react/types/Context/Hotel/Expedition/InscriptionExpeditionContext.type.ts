import { SoifType }      from "../../../components/Outils/OutilsExpedition.type";
import { ExpeditionDTO } from "../../../models/expedition.dto";
import { OuvriersDTO }   from "../../../models/ouvriers.dto";
import { CitoyensDTO }   from "../../../models/citoyens.dto";

export interface InscriptionExpeditionContextType {
	expeditions: ExpeditionDTO[],
	setExpeditions: (value: ExpeditionDTO[]) => void,
	ouvriers: OuvriersDTO[],
	setOuvriers: (value: OuvriersDTO[]) => void,
	recuperationCitoyensPreinscrit: () => CitoyensDTO[],
	recuperationCitoyensPreinscritOuvrier: () => CitoyensDTO[],
	optionsSoif: SoifType[],
	jourActuel: number,
	faoDirection: number,
}