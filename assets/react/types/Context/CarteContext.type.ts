import { CarteType } from "../components/ville/Carte.type";

export interface CarteContextType {
	carte: CarteType,
	setCarte: (carte: CarteType) => void,
	idClick: string,
	setIdClick: (currentId: string) => void,
	isSelCase: boolean,
	setIsSelCase: (isSelCase: boolean) => void,
	jourActuel: number,
	setJourActuel: (jour: number) => void,
	consigneExpes: string[],
	setConsigneExpes: (consigne: string[]) => void,
}