export interface PopUpContextType {
	status: string,
	isWithButtonClose: boolean,
	showPop: boolean,
	messagePopUp: string,
	titlePopUp: string,
	setStatus: (status: string) => void;
	setIsWithButtonClose: (isWithButtonClose: boolean) => void;
	setShowPop: (showPop: boolean) => void;
	setMessagePopUp: (messagePopUp: string) => void;
	setTitlePopUp: (titlePopUp: string) => void;
	handleClose: () => void;
}