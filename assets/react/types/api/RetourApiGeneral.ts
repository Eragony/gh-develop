export type RetourApiGeneral = {
	codeRetour?: number,
	libRetour?: string
	zoneRetour?: any,
}
export type CodeRetourApi = {
	codeRetour?: number,
	libRetour?: string,
}