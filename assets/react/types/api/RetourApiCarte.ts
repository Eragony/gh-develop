import { CarteType, CategoryObjetCarte, ListItemCount, PopUpMaj } from "../components/ville/Carte.type";
import { ZoneMaj }                                                from "../components/ville/Zone.type";
import { GeneralType }                                            from "../components/Generality/General.type";
import { ZoneMapDTO }                                             from "../models/zoneMap.dto";

export type PopupMajManuel = {
	data: {
		zoneMaj?: ZoneMaj,
		error?: string,
	},
	status?: number,
	message?: string,
}

export type MajBatRetour = {
	data: {
		zoneRetour?: string,
		error?: string,
	},
	status?: number,
	message?: string,
}

export type ScrutRetour = {
	data: {
		codeRetour?: number,
		libRetour?: string,
		error?: string,
	},
	status?: number,
	message?: string,
}

export type MajCaseRetour = {
	data: {
		libRetour?: string,
		error?: string,
	},
	status?: number,
	message?: string,
}

export type RefreshRetour = {
	data: {
		zoneRetour?: CarteType,
		error?: string,
	},
	status?: number,
	message?: string,
}
export type FiltreRetour = {
	data: {
		listCategorie?: CategoryObjetCarte[];
		listItemsSolBroken?: ListItemCount[];
		zones?: ZoneMapDTO[];
		error?: string,
	},
	status?: number,
	message?: string,
}
export type AffichageCarte = {
	data: {
		carte?: CarteType;
		popUpMaj?: PopUpMaj;
		general?: GeneralType;
		error?: string,
	},
	status?: number,
	message?: string,
}