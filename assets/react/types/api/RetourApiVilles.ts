import { VillesType }  from "../components/Villes/Villes.type";
import { GeneralType } from "../components/Generality/General.type";
import { VilleDTO }    from "../models/ville.dto";

export type AllVilles = {
	data: {
		listVilles?: [VilleDTO[]],
		error?: string,
	},
	status?: number,
	message?: string,
}

export type VillesSearch = {
	data: {
		errors?: Record<string, string>;
		listVilles?: VilleDTO[],
		error?: string,
	},
	status?: number,
	message?: string,
}

export type AffichageVilles = {
	data: {
		villes?: VillesType;
		general?: GeneralType,
		error?: string,
	},
	status?: number,
	message?: string,
}
