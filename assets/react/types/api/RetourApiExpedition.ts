import { ExpeditionType } from "../components/ville/Expedition.type";

export type TracingRetour = {
	codeRetour?: number,
	libRetour?: string,
	zoneRetour: {
		pa: number,
		trace: Array<Array<(number | null)[]>>,
	},
}

export type EditRetour = {
	codeRetour?: number,
	libRetour?: string,
	zoneRetour: ExpeditionType,
}

export type SuptraceRetour = {
	codeRetour?: number,
	libRetour?: string,
}

export type SauvegardeRetour = {
	codeRetour?: number,
	libRetour?: string,
}