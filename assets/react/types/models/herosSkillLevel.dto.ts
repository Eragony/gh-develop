import { HerosSkillTypeDTO }      from "./herosSkillType.dto";
import { HerosSkillPrototypeDTO } from "./herosSkillPrototype.dto";

export interface HerosSkillLevelDTO {
	id?: number;
	name?: string;
	xp?: number;
	lvl?: number;
	heros_skill_type?: HerosSkillTypeDTO;
	pouvoir?: HerosSkillPrototypeDTO[];
}
