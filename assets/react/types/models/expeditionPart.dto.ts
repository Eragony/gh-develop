import { TraceExpeditionDTO }    from "./traceExpedition.dto";
import { ConsigneExpeditionDTO } from "./consigneExpedition.dto";
import { ExpeditionDTO }         from "./expedition.dto";
import { ExpeditionnaireDTO }    from "./expeditionnaire.dto";

export interface ExpeditionPartDTO {
	id?: number;
	number?: number;
	ouverte?: boolean;
	pa?: number;
	description?: string;
	trace?: TraceExpeditionDTO;
	consignes?: ConsigneExpeditionDTO[];
	expedition?: ExpeditionDTO;
	expeditionnaires?: ExpeditionnaireDTO[];
}
