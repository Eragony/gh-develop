import { TypeDispoJumpDTO } from "./typeDispoJump.dto";
import { CreneauJumpDTO }   from "./creneauJump.dto";

export interface DispoJumpDTO {
	id?: number;
	choix_dispo?: TypeDispoJumpDTO;
	creneau_jump?: CreneauJumpDTO;
}
