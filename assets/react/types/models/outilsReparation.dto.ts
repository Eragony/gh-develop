import { UserDTO }               from "./user.dto";
import { ReparationChantierDTO } from "./reparationChantier.dto";

export interface OutilsReparationDTO {
	id?: string;
	created_at?: string;
	modify_at?: string;
	two_step?: boolean;
	gain_def?: number;
	pa_tot?: number;
	def_base?: number;
	created_by?: UserDTO;
	modify_by?: UserDTO;
	reparation_chantiers?: ReparationChantierDTO[];
}
