import { MenuElementDTO } from "./menuElement.dto";

export interface MenuDTO {
	id?: number;
	items?: MenuElementDTO[];
}
