import { ZoneMapDTO }        from "./zoneMap.dto";
import { ExpeditionPartDTO } from "./expeditionPart.dto";

export interface ConsigneExpeditionDTO {
	id?: number;
	text?: string;
	fait?: boolean;
	ordre_consigne?: number;
	zone?: ZoneMapDTO;
	expedition_part?: ExpeditionPartDTO;
}
