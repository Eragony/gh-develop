import { ExpeditionDTO } from "./expedition.dto";
import { OuvriersDTO }   from "./ouvriers.dto";
import { UserDTO }       from "./user.dto";

export interface OutilsExpeditionDTO {
	id?: string;
	fao_direction?: number;
	nbr_ouvrier?: number;
	created_at?: string;
	modify_at?: string;
	ouvert_ouvrier?: boolean;
	expeditions?: ExpeditionDTO[];
	ouvriers?: OuvriersDTO[];
	created_by?: UserDTO;
	modify_by?: UserDTO;
}
