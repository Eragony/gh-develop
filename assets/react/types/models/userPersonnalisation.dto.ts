export interface UserPersonnalisationDTO {
	id?: number;
	fige_menu?: boolean;
	pop_up_click?: boolean;
	citoyens_mode_compact?: boolean;
	bloc_maj_citoyens?: boolean;
	affichage_nb_citoyen?: boolean;
	zombie_discret?: boolean;
	keep_menu_open?: boolean;
	menu_bandeau?: boolean;
	maj_co?: boolean;
	bloc_maj_citoyen?: boolean;
	expe_on_the_top?: boolean;
	on_top_citoyen?: boolean;
	resizabled?: boolean;
	width_case?: number;
	fix_inscription_expe?: boolean;
	affichage_bat?: boolean;
	action_diff_carte_alter?: boolean;
	afficher_nbr_items_sol?: boolean;
	motif_epuisement?: string;
	item_separed_cate?: boolean;
}
