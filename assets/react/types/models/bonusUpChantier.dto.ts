import { UpChantierPrototypeDTO } from "./upChantierPrototype.dto";

export interface BonusUpChantierDTO {
	id?: number;
	level?: number;
	bonus_ups?: UpChantierPrototypeDTO[];
}
