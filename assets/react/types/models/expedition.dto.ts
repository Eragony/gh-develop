import { UserDTO }           from "./user.dto";
import { ExpeditionPartDTO } from "./expeditionPart.dto";
import { ExpeditionTypeDTO } from "./expeditionType.dto";

export interface ExpeditionDTO {
	id?: string;
	created_at?: string;
	modify_at?: string;
	nom?: string;
	min_pdc?: number;
	nbr_partie?: number;
	priorite?: number;
	verrou?: boolean;
	verrou_at?: string;
	created_by?: UserDTO;
	modify_by?: UserDTO;
	expedition_parts?: ExpeditionPartDTO[];
	type_expe?: ExpeditionTypeDTO;
	verrou_by?: UserDTO;
}
