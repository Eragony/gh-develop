import { UserDTO }              from "./user.dto";
import { ChantierPrototypeDTO } from "./chantierPrototype.dto";
import { ProgrammeChantierDTO } from "./programmeChantier.dto";
import { UpHabitationDTO }      from "./upHabitation.dto";
import { UpAmelioDTO }          from "./upAmelio.dto";
import { EstimRessourceDTO }    from "./estimRessource.dto";
import { RepaArmeDTO }          from "./repaArme.dto";

export interface OutilsChantierDTO {
	created_at?: string;
	modify_at?: string;
	eco_ressource?: boolean;
	util_b_d_e?: boolean;
	pres_scie?: boolean;
	nbr_ouvrier?: number;
	nbr_campeur?: number;
	nbr_mort?: number;
	ration_eau?: boolean;
	ration_nourriture?: boolean;
	type_nourriture?: number;
	nbr_max_eau?: number;
	nbr_max_nourriture?: number;
	nbr_alcool_expe?: number;
	nbr_alcool_veille?: number;
	nbr_drogue_expe?: number;
	nbr_drogue_veille?: number;
	nbr_drogue_alcool?: number;
	nbr_p_a_legendaire?: number;
	nbr_p_a_fao?: number;
	pa_chantier?: number;
	def_chantier?: number;
	pa_habitation?: number;
	pa_reparation_arme?: number;
	bricot_vert?: boolean;
	nbr_kit_vert?: number;
	pa_transfo?: number;
	pa_total?: number;
	pa_tdga?: number;
	lvl_up?: number;
	def_maison?: number;
	def_upgrade?: number;
	created_by?: UserDTO;
	modify_by?: UserDTO;
	chantier_up?: ChantierPrototypeDTO;
	chantiers_programmes?: ProgrammeChantierDTO[];
	up_habitations?: UpHabitationDTO[];
	up_amelios?: UpAmelioDTO[];
	estim_ressources?: EstimRessourceDTO[];
	repa_armes?: RepaArmeDTO[];
}
