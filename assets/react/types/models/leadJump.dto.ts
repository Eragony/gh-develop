import { UserDTO }     from "./user.dto";
import { TypeLeadDTO } from "./typeLead.dto";

export interface LeadJumpDTO {
	id?: number;
	apprenti?: boolean;
	user?: UserDTO;
	type_lead?: TypeLeadDTO;
}
