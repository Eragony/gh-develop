import { BatPrototypeDTO }  from "./batPrototype.dto";
import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface ItemBatimentDTO {
	probabily?: number;
	bat_prototype?: BatPrototypeDTO;
	item?: ItemPrototypeDTO;
}
