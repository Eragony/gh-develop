export interface ObjectifVillePrototypeDTO {
	id?: number;
	nom?: string;
	icon?: string;
}
