import { CreneauHorraireDTO } from "./creneauHorraire.dto";
import { TypeDispoDTO }       from "./typeDispo.dto";

export interface DispoTypesDTO {
	id: number;
	type_creneau: number;
	creneau: CreneauHorraireDTO;
	dispo: TypeDispoDTO;
}