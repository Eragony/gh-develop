import { RessourceHomeDTO } from "./ressourceHome.dto";

export interface HomePrototypeDTO {
	id?: number;
	nom?: string;
	icon?: string;
	def?: number;
	pa?: number;
	pa_urba?: number;
	ressources?: RessourceHomeDTO[];
	ressources_urba?: RessourceHomeDTO[];
}
