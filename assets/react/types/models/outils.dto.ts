import { OutilsExpeditionDTO } from "./outilsExpedition.dto";

export interface OutilsDTO {
	id?: number;
	day?: number;
	expedition?: OutilsExpeditionDTO;
}
