import { RessourceChantierDTO } from "./ressourceChantier.dto";
import { BonusUpChantierDTO }   from "./bonusUpChantier.dto";

export interface ChantierPrototypeDTO {
	id?: number;
	nom?: string;
	icon?: string;
	def?: number;
	water?: number;
	pa?: number;
	niveau?: number;
	plan?: number;
	temp?: boolean;
	pv?: number;
	indes?: boolean;
	ruine_ho?: boolean;
	ruine_hs?: boolean;
	ruine_bu?: boolean;
	orderby?: number;
	level_max?: number;
	order_by_listing?: number;
	uid?: string;
	id_hordes?: number;
	id_mh?: number;
	actif?: boolean;
	description?: string;
	order_by_general?: number;
	specifique_ville_prive?: boolean;
	ressources?: RessourceChantierDTO[];
	parent?: ChantierPrototypeDTO;
	children?: ChantierPrototypeDTO[];
	level_ups?: BonusUpChantierDTO[];
	cat_chantier?: ChantierPrototypeDTO;
	chantier_of_cat?: ChantierPrototypeDTO[];
}
