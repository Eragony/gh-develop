import { UserDTO }           from "./user.dto";
import { PictoPrototypeDTO } from "./pictoPrototype.dto";

export interface PictosDTO {
	id?: number;
	nombre?: number;
	user?: UserDTO;
	picto?: PictoPrototypeDTO;
}
