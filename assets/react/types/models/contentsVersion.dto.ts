export interface ContentsVersionDTO {
	id?: number;
	contents?: string;
	type_content?: number;
	id_relatif?: number;
}
