import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface ChantiersDTO {
	pv?: number;
	def?: number;
	detruit?: boolean;
	nbr_construit?: number;
	jour_construction?: number[] | null;
	jour_destruction?: number[] | null;
	chantier?: ChantierPrototypeDTO;
}
