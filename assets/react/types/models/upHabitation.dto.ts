import { UserDTO }          from "./user.dto";
import { HomePrototypeDTO } from "./homePrototype.dto";

export interface UpHabitationDTO {
	id?: number;
	uuid?: string;
	citoyen?: UserDTO;
	lvl_habitation?: HomePrototypeDTO;
}
