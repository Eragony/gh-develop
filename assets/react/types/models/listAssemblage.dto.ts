import { ItemProbabilityDTO }      from "./itemProbability.dto";
import { ItemNeedDTO }             from "./itemNeed.dto";
import { TypeActionAssemblageDTO } from "./typeActionAssemblage.dto";
import { ItemPrototypeDTO }        from "./itemPrototype.dto";

export interface ListAssemblageDTO {
	id?: number;
	item_obtain?: ItemProbabilityDTO[];
	item_need?: ItemNeedDTO[];
	type_action?: TypeActionAssemblageDTO;
	item_principal?: ItemPrototypeDTO;
	item_needs?: ItemNeedDTO[];
	item_obtains?: ItemProbabilityDTO[];
}
