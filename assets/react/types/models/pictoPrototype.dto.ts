import { PictoTitrePrototypeDTO } from "./pictoTitrePrototype.dto";

export interface PictoPrototypeDTO {
	id?: number;
	name?: string;
	description?: string;
	rare?: boolean;
	community?: boolean;
	img?: string;
	uuid?: string;
	id_mh?: number;
	actif?: boolean;
	titre?: PictoTitrePrototypeDTO[];
}
