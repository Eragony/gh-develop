import { TypeCaracteristiqueDTO } from "./typeCaracteristique.dto";

export interface CaracteristiquesItemDTO {
	id?: number;
	value?: number;
	probabilite?: number;
	type_carac?: TypeCaracteristiqueDTO;
}
