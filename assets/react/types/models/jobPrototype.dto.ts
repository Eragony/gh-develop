export interface JobPrototypeDTO {
	id?: number;
	nom?: string;
	icon?: string;
	alternatif?: string;
}
