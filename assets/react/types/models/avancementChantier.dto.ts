import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface AvancementChantierDTO {
	pa?: number;
	chantier?: ChantierPrototypeDTO;
}
