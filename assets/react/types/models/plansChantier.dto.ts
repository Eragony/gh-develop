import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface PlansChantierDTO {
	chantier?: ChantierPrototypeDTO;
}
