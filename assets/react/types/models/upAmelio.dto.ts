import { UserDTO }            from "./user.dto";
import { UpHomePrototypeDTO } from "./upHomePrototype.dto";

export interface UpAmelioDTO {
	id?: number;
	lvl?: number;
	uuid?: string;
	citoyen?: UserDTO;
	amelio?: UpHomePrototypeDTO;
}
