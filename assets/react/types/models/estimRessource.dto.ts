import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface EstimRessourceDTO {
	id?: number;
	nombre?: number;
	uuid?: string;
	item?: ItemPrototypeDTO;
}
