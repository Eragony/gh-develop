import { PictoPrototypeDTO } from "./pictoPrototype.dto";

export interface HistoriquePictosDTO {
	map_id?: number;
	nombre?: number;
	picto?: PictoPrototypeDTO;
}
