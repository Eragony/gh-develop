export interface TypeActionAssemblageDTO {
	id?: number;
	nom?: string;
	description?: string;
	nom_item_need?: string;
	nom_item_obtain?: string;
}
