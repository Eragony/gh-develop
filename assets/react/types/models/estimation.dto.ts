export interface EstimationDTO {
	id?: number;
	min_jour?: number;
	max_jour?: number;
	maxed_jour?: boolean;
	min_planif?: number;
	max_planif?: number;
	maxed_planif?: boolean;
	day?: number;
}
