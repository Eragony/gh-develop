import { DispoCreneauUserTypeExpeditionDTO } from "./dispoCreneauUserTypeExpedition.dto";

export interface DispoUserTypeExpeditionDTO {
	id?: number;
	nom?: string;
	priorite?: number;
	favoris?: boolean;
	creneau?: DispoCreneauUserTypeExpeditionDTO[];
}
