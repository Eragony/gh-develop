import { BatPrototypeDTO } from "./batPrototype.dto";
import { RuinesPlansDTO }  from "./ruinesPlans.dto";
import { VilleDTO }        from "./ville.dto";

export interface RuinesDTO {
	id?: string;
	x?: number;
	y?: number;
	bat?: BatPrototypeDTO;
	plans?: RuinesPlansDTO[];
	ville?: VilleDTO;
	nombre_plan?: number;
}
