export interface DefenseDTO {
	total?: number;
	buildings?: number;
	upgrades?: number;
	objet?: number;
	bonus_od?: number;
	maison_citoyen?: number;
	gardiens?: number;
	veilleurs?: number;
	ames?: number;
	morts?: number;
	tempos?: number;
	day?: number;
	bonus_sd?: number;
	bonus_sd_pct?: number;
	def_h_s_d?: number;
	def_s_d?: number;
}
