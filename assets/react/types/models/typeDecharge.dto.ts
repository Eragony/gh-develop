import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface TypeDechargeDTO {
	id?: number;
	chantier?: ChantierPrototypeDTO;
}
