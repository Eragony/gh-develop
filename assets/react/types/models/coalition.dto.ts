import { UserDTO }       from "./user.dto";
import { DispoJumpDTO }  from "./dispoJump.dto";
import { StatutUserDTO } from "./statutUser.dto";

export interface CoalitionDTO {
	id?: number;
	num_coa?: number;
	position_coa?: number;
	createur?: boolean;
	user?: UserDTO;
	dispos?: DispoJumpDTO[];
	statut?: StatutUserDTO;
}
