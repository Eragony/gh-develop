import { UserPersoCouleurDTO } from "./userPersoCouleur.dto";

export interface ThemeUserDTO {
	id?: number;
	nom?: string;
	stats_bg_color?: string;
	primary_border_color?: string;
	background_color?: string;
	my_line_color?: string;
	primary_color?: string;
	secondary_color?: string;
	tertiary_color?: string;
	hover_font_color?: string;
	primary_font_color?: string;
	tertiary_font_color?: string;
	primary_row_color?: string;
	secondary_row_color?: string;
	base_theme?: string;
	selected?: boolean;
	stats_font_color?: string;
	stats_border_color?: string;
	bg_hover_color?: string;
	succes_color?: string;
	error_color?: string;
	specifique_color?: string;
	user_perso_couleur?: UserPersoCouleurDTO;
}
