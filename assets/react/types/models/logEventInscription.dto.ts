import { UserDTO }            from "./user.dto";
import { InscriptionJumpDTO } from "./inscriptionJump.dto";

export interface LogEventInscriptionDTO {
	id?: number;
	libelle?: string;
	event_at?: string;
	valeur_avant?: string;
	valeur_apres?: string;
	visible?: boolean;
	typologie?: number;
	declencheur?: UserDTO;
	inscription?: InscriptionJumpDTO;
}
