import { UserDTO }      from "./user.dto";
import { DechargesDTO } from "./decharges.dto";

export interface OutilsDechargeDTO {
	id?: string;
	created_at?: string;
	modify_at?: string;
	created_by?: UserDTO;
	modify_by?: UserDTO;
	decharges?: DechargesDTO[];
}
