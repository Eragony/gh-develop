import { JobPrototypeDTO }       from "./jobPrototype.dto";
import { HerosPrototypeDTO }     from "./herosPrototype.dto";
import { SacExpeditionnaireDTO } from "./sacExpeditionnaire.dto";
import { DispoExpeditionDTO }    from "./dispoExpedition.dto";
import { CitoyensDTO }           from "./citoyens.dto";

export interface ExpeditionnaireDTO {
	id?: string;
	pa_base?: number;
	soif?: boolean;
	preinscrit?: boolean;
	position?: number;
	job_fige?: boolean;
	soif_fige?: boolean;
	for_banni?: boolean;
	dispo_rapide?: number;
	commentaire?: string;
	pe_base?: number;
	job?: JobPrototypeDTO;
	action_heroic?: HerosPrototypeDTO;
	sac?: SacExpeditionnaireDTO[];
	dispo?: DispoExpeditionDTO[];
	citoyen?: CitoyensDTO;
}
