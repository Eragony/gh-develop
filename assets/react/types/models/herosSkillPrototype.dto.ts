import { HerosPrototypeDTO } from "./herosPrototype.dto";

export interface HerosSkillPrototypeDTO {
	id?: number;
	value?: number;
	value2?: number;
	text?: string;
	heros?: HerosPrototypeDTO;
}
