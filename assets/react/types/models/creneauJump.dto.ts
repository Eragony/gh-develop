import { CreneauHorraireDTO } from "./creneauHorraire.dto";

export interface CreneauJumpDTO {
	id?: number;
	date_creneau?: string;
	creneau_horraire?: CreneauHorraireDTO;
}
