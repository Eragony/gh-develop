import { VilleDTO } from "./ville.dto";
import { UserDTO }  from "./user.dto";

export interface ExpeHordesDTO {
	id?: number;
	nom?: string;
	trace?: number[][];
	length?: number;
	ville?: VilleDTO;
	citoyen?: UserDTO;
}
