import { RegroupementItemsDechargeDTO } from "./regroupementItemsDecharge.dto";

export interface DechargesDTO {
	id?: number;
	nbr_estime?: number;
	nbr_utilise?: number;
	def_by_item?: number;
	regroup_items?: RegroupementItemsDechargeDTO;
}
