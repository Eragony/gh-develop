export interface HerosPrototypeDTO {
	id?: number;
	nom?: string;
	icon?: string;
	description?: string;
	jour?: number;
	jour_cumul?: number;
	pouv_actif?: boolean;
	ordre_recup?: number;
}
