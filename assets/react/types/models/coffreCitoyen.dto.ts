import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface CoffreCitoyenDTO {
	broken?: boolean;
	nombre?: number;
	item?: ItemPrototypeDTO;
}
