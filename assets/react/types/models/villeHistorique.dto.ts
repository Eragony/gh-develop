export interface VilleHistoriqueDTO {
	id?: number;
	mapid?: number;
	origin?: number;
	nom?: string;
	saison?: number;
	der_jour?: number;
	phase?: string;
}
