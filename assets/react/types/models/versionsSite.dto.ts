import { ContentsVersionDTO } from "./contentsVersion.dto";

export interface VersionsSiteDTO {
	id?: number;
	version_majeur?: number;
	version_mineur?: number;
	version_corrective?: number;
	tag_name?: string;
	num_tag?: number;
	titre?: string;
	date_version?: string;
	contents?: ContentsVersionDTO[];
}
