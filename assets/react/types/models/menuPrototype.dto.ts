export interface MenuPrototypeDTO {
	id?: number;
	label?: string;
	category?: string;
	hab_user?: boolean;
	hab_beta?: boolean;
	hab_admin?: boolean;
	connected?: boolean;
	ville?: boolean;
	icone?: string;
	route?: string;
	my_ville?: boolean;
	for_user?: boolean;
}
