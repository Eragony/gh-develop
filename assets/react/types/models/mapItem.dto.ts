import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface MapItemDTO {
	nombre?: number;
	broked?: boolean;
	item?: ItemPrototypeDTO;
}
