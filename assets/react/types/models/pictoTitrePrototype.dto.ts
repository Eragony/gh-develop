export interface PictoTitrePrototypeDTO {
	id?: number;
	nbr?: number;
	titre?: string;
}
