export interface TypeCaracteristiqueDTO {
	id?: number;
	nom?: string;
	icon?: string;
}
