export interface CreneauHorraireDTO {
	id?: number;
	libelle?: string;
	typologie?: number;
}
