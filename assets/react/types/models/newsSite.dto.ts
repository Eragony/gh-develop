import { UserDTO }         from "./user.dto";
import { VersionsSiteDTO } from "./versionsSite.dto";

export interface NewsSiteDTO {
	id?: number;
	titre?: string;
	content?: string;
	date_ajout?: string;
	date_modif?: string;
	auteur?: UserDTO;
	version?: VersionsSiteDTO;
}
