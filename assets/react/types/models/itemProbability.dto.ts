import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface ItemProbabilityDTO {
	id?: number;
	taux?: number;
	item?: ItemPrototypeDTO;
}
