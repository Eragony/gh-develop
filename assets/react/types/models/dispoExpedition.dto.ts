import { CreneauHorraireDTO } from "./creneauHorraire.dto";
import { TypeDispoDTO }       from "./typeDispo.dto";

export interface DispoExpeditionDTO {
	id?: number;
	creneau?: CreneauHorraireDTO;
	dispo?: TypeDispoDTO;
}
