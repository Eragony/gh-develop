import { UserDTO }           from "./user.dto";
import { VilleDTO }          from "./ville.dto";
import { ExpeditionPartDTO } from "./expeditionPart.dto";

export interface TraceExpeditionDTO {
	id?: string;
	coordonnee?: number[][];
	nom?: string;
	visible?: boolean;
	created_at?: string;
	modify_at?: string;
	collab?: boolean;
	jour?: number;
	pa?: number;
	couleur?: string;
	personnel?: boolean;
	inactive?: boolean;
	brouillon?: boolean;
	biblio?: boolean;
	trace_expedition?: boolean;
	created_by?: UserDTO;
	modify_by?: UserDTO;
	ville?: VilleDTO;
	expedition_part?: ExpeditionPartDTO;
}
