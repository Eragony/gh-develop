export interface EstimationTdgDTO {
	day?: number;
	type_estim?: number;
	val_pourcentage?: number;
	min_estim?: number;
	max_estim?: number;
}
