import { UserDTO }        from "./user.dto";
import { RuinesCasesDTO } from "./ruinesCases.dto";
import { RuinesDTO }      from "./ruines.dto";

export interface RuinesPlansDTO {
	id?: number;
	created_at?: string;
	modify_at?: string;
	trace_safe?: number[][] | null;
	create_by?: UserDTO;
	modify_by?: UserDTO;
	cases?: RuinesCasesDTO[];
	ruines?: RuinesDTO;
}
