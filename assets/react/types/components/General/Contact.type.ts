export interface ContactType {
	contactData: ContactEntry;
}


export interface ContactEntry {

}

export interface ContactTypePost {
	urlForum: string;
}

export interface ContactPost {
	titre: string;
	message: string;
	tag: string[];
	urlImage: ImageFormData[];
	confidential: boolean;
}

export interface ImageFormData {
	src: string;
	legend: string;
}
