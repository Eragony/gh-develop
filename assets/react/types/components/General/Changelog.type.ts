import { VersionsSiteDTO } from "../../models/versionsSite.dto";

export interface ChangelogType {
	changelogData: VersionsSiteDTO[];
}
