export interface CustomPaginationProps {
	jour: number;
	jour_max: number;
	onChoixJour: (selectedDay: number) => void;
}

export interface EllipsisMenuProps {
	startPage: number;
	endPage: number;
	totalPages: number;
	onMenuItemClick: (selectedPage: number) => void;
}
