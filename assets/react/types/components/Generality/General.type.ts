import { UserDTO }      from "../../models/user.dto";
import { ThemeUserDTO } from "../../models/themeUser.dto";
import { VilleDTO }     from "../../models/ville.dto";
import { MenuDTO }      from "../../models/menu.dto";

export interface GeneralType {
	fuseau: string,
	inVille: boolean,
	isAdmin: boolean,
	niveauHabilitation: string,
	isCo: boolean,
	myVille: boolean,
	ptsSaison: number,
	user: UserDTO,
	ville: VilleDTO,
	version: {
		date: string,
		version: string
	},
	versionJeu: string,
	lang: string,
	lienMHE: string,
	lienMHO: string,
	lienDiscord: string,
	lienGit: string,
	lienCrowdin: string,
	habilitation: HabilitationMenu
	themeUser: ThemeUserDTO,
	miseEnAvant: EventEnAvantType[],
	menuUser: MenuDTO
}

export interface HabilitationMenu {
	ville: VilleMenu;
	hotel: HotelMenu;
	outils: OutilsMenu;
	jump: JumpMenu;
	villes: NiveauHabilitation;
	encyclopedie: EncyclopedieMenu;
	ame: AmeMenu;
	admin: NiveauHabilitation;
}

export interface VilleMenu {
	carte: NiveauHabilitation;
	banque: NiveauHabilitation;
	ruine: NiveauHabilitation;
}

export interface HotelMenu {
	citoyens: NiveauHabilitation;
	journal: NiveauHabilitation;
	plans: NiveauHabilitation;
	stats: NiveauHabilitation;
	tour: NiveauHabilitation;
	inscription: NiveauHabilitation;
}

export interface OutilsMenu {
	reparation: NiveauHabilitation;
	chantiers: NiveauHabilitation;
	veilles: NiveauHabilitation;
	decharge: NiveauHabilitation;
	expe: NiveauHabilitation;
	cm: NiveauHabilitation;
}

export interface JumpMenu {
	inscription: NiveauHabilitation;
	coalitions: NiveauHabilitation;
	creations: NiveauHabilitation;
	gestions: NiveauHabilitation;
}

export interface EncyclopedieMenu {
	batiments: NiveauHabilitation;
	chantiers: NiveauHabilitation;
	citoyens: NiveauHabilitation;
	objets: NiveauHabilitation;
}

export interface AmeMenu {
	moname: NiveauHabilitation;
	classement: NiveauHabilitation;
}

export interface NiveauHabilitation {
	user: boolean,
	beta: boolean,
	admin: boolean
}

export interface EventEnAvantType {
	nom: string,
	id: string,
	icone: string,
}