import { VilleDTO }             from "../../models/ville.dto";
import { DefenseDTO }           from "../../models/defense.dto";
import { EstimationDTO }        from "../../models/estimation.dto";
import { JournalDTO }           from "../../models/journal.dto";
import { CitoyensDTO }          from "../../models/citoyens.dto";
import { BanqueDTO }            from "../../models/banque.dto";
import { ChantiersDTO }         from "../../models/chantiers.dto";
import { ZoneMapDTO }           from "../../models/zoneMap.dto";
import { UpChantierDTO }        from "../../models/upChantier.dto";
import { CategoryObjetDTO }     from "../../models/categoryObjet.dto";
import { ChantierPrototypeDTO } from "../../models/chantierPrototype.dto";
import { JobPrototypeDTO }      from "../../models/jobPrototype.dto";
import { HomePrototypeDTO }     from "../../models/homePrototype.dto";
import { PlansChantierDTO }     from "../../models/plansChantier.dto";
import { AtkStats }             from "../Hotel/Statistiques.type";

export interface VillesType {
	top30: VilleDTO[],
	listVilles: [VilleDTO[]],
	listSaison: SaisonType[],
	maxVilles: number,
}

export interface SaisonType {
	type: number,
	nom: string,
}

export interface ComparatifVilles {
	derniere_maj: string,
	resume: {
		ville: {
			nom: string,
			etat: string,
			saison: number,
			map_id: number,
			points: number,
			jour: number,
			nom_jump?: string,
			x: number,
			y: number,
		},
		citoyens: {
			nb_vivant: number,
			nb_mort: number,
			nb_banni: number,
		},
		provisions: {
			nb_eau: number,
			nb_nourriture: number,
			nb_drogues: number,
			nb_alcool: number,
			nb_cafe: number,
		},
		ressources: {
			nb_bois: number,
			nb_metal: number,
			nb_beton: number,
			nb_compo: number,
			nb_pve: number,
			nb_tube: number,
			nb_explo: number,
			nb_piles: number,
			nb_lentille: number,
			nb_sac: number,
		},
		defense: DefenseDTO,
		estim: EstimationDTO,
	},
	gazette: JournalDTO,
	citoyens: CitoyensDTO[],
	banques: BanqueDTO[],
	chantiers: ChantiersDTO[],
	plans_chantiers: PlansChantierDTO[],
	up_chantiers: UpChantierDTO[],
	carte: {
		detail: {
			nb_epuise: number,
			nb_decouvert: number,
			nb_citoyen: number,
			nb_total: number,
			taille: number,
			zones: ZoneMapDTO[][],
		},
		ruines: ZoneMapDTO[],
		batiments: ZoneMapDTO[],
	}
	stats: AtkStats,
}

export interface OptionsVillesComparatif {
	categories: CategoryObjetDTO[],
	chantiers: ChantierPrototypeDTO[],
	plans: ChantierPrototypeDTO[],
	upChantiers: ChantierPrototypeDTO[],
	jobs: JobPrototypeDTO[],
	homes: HomePrototypeDTO[],
}