import { ChantiersDTO } from "../../models/chantiers.dto";
import { JournalDTO }   from "../../models/journal.dto";

export interface JournalType {
	chantiersList: [ChantiersDTO[]],
	chantiersDetruitList: [ChantiersDTO[]],
	listJournal: JournalDTO[],
	level_regen: number[],
	level_up_chantier: {
		id: number,
		nom: string,
		lvl_max: number,
		icon: string,
		lvl: number,
		destroy: boolean,
	}[],
}