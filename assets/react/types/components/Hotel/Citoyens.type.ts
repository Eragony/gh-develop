import { HomePrototypeDTO }   from "../../models/homePrototype.dto";
import { UpHomePrototypeDTO } from "../../models/upHomePrototype.dto";
import { CitoyensDTO }        from "../../models/citoyens.dto";
import { JobPrototypeDTO }    from "../../models/jobPrototype.dto";
import { HerosPrototypeDTO }  from "../../models/herosPrototype.dto";
import { ItemPrototypeDTO }   from "../../models/itemPrototype.dto";
import { CategoryObjetDTO }   from "../../models/categoryObjet.dto";
import { UserDTO }            from "../../models/user.dto";
import { VilleDTO }           from "../../models/ville.dto";
import { HerosSkillLevelDTO } from "../../models/herosSkillLevel.dto";
import { HerosSkillTypeDTO }  from "../../models/herosSkillType.dto";


export interface CitoyensType {
	citoyensVie: CitoyensDTO[],
	citoyensMort: CitoyensDTO[],
	compteurs: CompteursCitoyens,
	formCitoyen: FormCitoyens
	isLead: boolean,
	isOrga: boolean,
	listPouvoir: HerosPrototypeDTO[],
	listSkill: HerosSkillTypeDTO[],
	listSkillLevel: HerosSkillLevelDTO[],
	listSkillLevelBase: HerosSkillLevelDTO[],
	listAmelio: UpHomePrototypeDTO[],
	listHabitation: HomePrototypeDTO[],
	listIdPouvoir: ListIdPouvoir,
	listJob: JobPrototypeDTO[],
	moyenContact: string[],
	myVille: boolean,
	user: UserDTO,
	ville: VilleDTO,
}

export interface CompteursCitoyens {
	job: {
		id: number,
		nom: string,
		nbr: number,
		icon: string,
	}[],
	hab: {
		id: number,
		nom: string,
		nbr: number,
		icon: string,
	}[],
	pv: {
		id: number,
		nom: string,
		method: string,
		nbr: number,
		icon: string,
	}[],
}

export interface InputCitoyens {
	type: string,
	label: string,
	choices?: any[],
	class?: string,
}

export interface FormCitoyens {
	divers: {
		chargeApag: InputCitoyens,
		nbCamping: InputCitoyens,
		immuniser: InputCitoyens,
		lvlRuine: InputCitoyens,
		shoes: InputCitoyens,
	},
	pouv: {
		sauvetage: InputCitoyens,
		rdh: InputCitoyens,
		uppercut: InputCitoyens,
		donJh: InputCitoyens,
		corpsSain: InputCitoyens,
		secondSouffle: InputCitoyens,
		trouvaille: InputCitoyens,
		trouvaille_a: InputCitoyens,
		vlm: InputCitoyens,
		pef: InputCitoyens,
		camaraderie_recu: InputCitoyens,
	},
	hab: {
		lvlMaison: string,
		lvlCoinSieste: InputCitoyens,
		lvlCuisine: InputCitoyens,
		lvlLabo: InputCitoyens,
		lvlRangement: InputCitoyens,
		lvlRenfort: InputCitoyens,
		cloture: InputCitoyens,
		nbBarricade: InputCitoyens,
		nbCarton: InputCitoyens,
	}
}

export interface PopUpMajCoffreProps {
	listCategorie: CategoryObjetDTO[],
	listItems: ItemPrototypeDTO[],
}


export interface FiltreCitoyens {
	listJob: JobPrototypeDTO[],
	listPouvoir: HerosPrototypeDTO[],
	listMaison: HomePrototypeDTO[],
	listAmelio: UpHomePrototypeDTO[],
}

export interface ListIdPouvoir {
	corps_sain: number,
	camaraderie: number,
	poss_apag: number,
	second_souffle: number,
	vlm: number,
	trouvaille_ame: number,
}