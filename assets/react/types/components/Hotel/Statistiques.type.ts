export interface StatistiquesType {
	def: DefStats,
	atk: AtkStats,
	maxJour: number,
	maxAtkJour: number,
}

export interface DefStats {
	day: (number | null)[],
	total: (number | null)[],
	chantiersUpChantierAme: (number | null)[],
	objetDef: (number | null)[],
	maisonCitizen: (number | null)[],
	gardiens: (number | null)[],
	veilleurs: (number | null)[],
	morts: (number | null)[],
	tempos: (number | null)[],
}

export interface AtkStats {
	day: (number | null)[],
	attaqueJour: (number | null)[],
	def: (number | null)[],
	minEstim: (number | null)[],
	maxEstim: (number | null)[],
	estimMinTotal: (number | null)[],
	estimMaxTotal: (number | null)[],
	atk: number[],
	atk_min_theo: number[],
	atk_max_theo: number[],
	
}