import { ChantierPrototypeDTO } from "../../models/chantierPrototype.dto";
import { ChantiersDTO }         from "../../models/chantiers.dto";
import { UserDTO }              from "../../models/user.dto";
import { VilleDTO }             from "../../models/ville.dto";
import { UpChantierDTO }        from "../../models/upChantier.dto";

export interface PlansChantierType {
	banque: BanqueItemPlans[],
	listChantier: ChantierPrototypeDTO[],
	listPlans: ChantierPrototypeDTO[],
	listPlansVille: ListPlansObtenu[],
	listChantiersConstruit: ChantiersDTO[],
	listAvancement: { pa: number | null }[],
	reductionPA: number,
	myVille: boolean,
	ville: VilleDTO,
	user: UserDTO,
	listEvoChantiers: ChantierPrototypeDTO[],
	listEvoChantiersVille: UpChantierDTO[],
	translateEvo: string[],
}

export interface BanqueItemPlans {
	nbrItem: number,
	id: number,
	nom: string,
	icon: string,
}

export interface ListPlansObtenu {
	chantier: ChantierPrototypeDTO;
}

