import { UserDTO }             from "../../models/user.dto";
import { HistoriqueVilleDTO }  from "../../models/historiqueVille.dto";
import { HistoriquePictosDTO } from "../../models/historiquePictos.dto";
import { PictosDTO }           from "../../models/pictos.dto";

export interface AmeType {
	listPicto: PictosDTO[];
	userVisu: UserDTO;
	ville: { nom: string; mapId: number };
	listVilles: HistoriqueVilleDTO[];
	histoPicto: HistoriquePictosDTO[][];
	listeSaison: { id: number; nom: string }[];
	listVoisin: { pseudo: string, id: number, nbVille: number }[];
	nbrVille: number;
	commune: string[];
	stats: AmeStats;
}


export interface AmeStats {
	nbrVille: number;
	record_surv: number;
	jour_heros: number;
	jour_total: number;
	moyenne_surv: number;
}