import { ExpeditionType }            from "./Expedition.type";
import { OutilsExpeditionTypeProps } from "../Outils/OutilsExpedition.type";
import { BatPrototypeDTO }           from "../../models/batPrototype.dto";
import { UserDTO }                   from "../../models/user.dto";
import { ChantierPrototypeDTO }      from "../../models/chantierPrototype.dto";
import { ItemPrototypeDTO }          from "../../models/itemPrototype.dto";
import { CitoyensDTO }               from "../../models/citoyens.dto";
import { DefenseDTO }                from "../../models/defense.dto";
import { CategoryObjetDTO }          from "../../models/categoryObjet.dto";
import { EstimationDTO }             from "../../models/estimation.dto";
import { UpChantierDTO }             from "../../models/upChantier.dto";
import { VilleDTO }                  from "../../models/ville.dto";
import { ZoneMapDTO }                from "../../models/zoneMap.dto";
import { ExpeHordesDTO }             from "../../models/expeHordes.dto";
import { JobPrototypeDTO }           from "../../models/jobPrototype.dto";

export interface ItemCount {
	nbr_item: number,
	id: number,
	nom: string,
	icon: string,
	category_objet_id: number,
	nbr_item_bank: number,
}

export interface ListItemCount {
	itemCount: ItemCount[],
}

export interface CategoryObjetCarte {
	id: number,
	nom: string,
	objetSol: ItemCount[];
}

export interface CarteType {
	arrCreateurVivant: number[],
	batCarte: {
		batJaune: ZoneMapDTO[],
		batBleu: ZoneMapDTO[],
		ruines: ZoneMapDTO[],
		nbCampedJaune: number,
		nbCampedBleu: number,
	}
	campingActif: boolean;
	carteExpe: Array<{ [key: string]: number[] }>;
	carteExpeBiblio: Array<{ [key: string]: number[] }>;
	carteExpeBrouillon: Array<{ [key: string]: number[] }>;
	carteExpeHordes: Array<{ [key: string]: number[] }>;
	carteOptionPerso: boolean;
	carteOptionPerso_alter: boolean;
	carteOptionPerso_estim: boolean;
	chaman_banque: number;
	citoyensDehors: CitoyensDTO[],
	citoyensVille: CitoyensDTO[],
	constructionChantier: {
		tdg: boolean,
		phare: boolean,
		scrut: boolean,
		planif: boolean,
	}
	couleurHordes: string[];
	dateDernMaj: string,
	defense: DefenseDTO;
	devastLibelle: string,
	estimation: EstimationDTO,
	evoChantier: UpChantierDTO[];
	isLead: boolean;
	listBat: BatPrototypeDTO[];
	listCategorie: CategoryObjetCarte[];
	listDecharge: ChantierPrototypeDTO[];
	listExp: ExpeditionType[];
	listExpBiblio: ExpeditionType[];
	listExpBrouillon: ExpeditionType[];
	listExpCouleur: string[];
	listExpHordes: ExpeHordesDTO[];
	listExpInscrit: string[];
	listItemsSolBroken: ListItemCount[];
	listItemsBank: number[];
	listJobs: JobPrototypeDTO[];
	listRuine: String[];
	listZonage: number[];
	lvlTdG: number;
	maxKm: number;
	maxPa: number;
	maxAlter: number[];
	maxAlterAll: number;
	maxScrut: number;
	myVille: boolean;
	nbColIn: number,
	nbColOut: number,
	pctMap: StatsCarte,
	theme: string;
	typeLibelle: string,
	user: UserDTO;
	ville: VilleDTO;
	water_banque: number;
	nourriture_banque_b6: number;
	nourriture_banque_b7: number;
	alcool_banque: number;
	drogue_banque: number;
	estim_day: number;
	outilsExpe?: OutilsExpeditionTypeProps;
}

export interface PopUpMaj {
	listCategorie: CategoryObjetDTO[],
	listItems: ItemPrototypeDTO[],
}

export interface StatsCarte {
	zonePa: number[],
	epuise: number[],
	decouverte: number[],
	total: number[],
}

export interface StatsCarteUnitaire {
	epuise: number,
	decouverte: number,
	total: number,
}