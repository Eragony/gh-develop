import { UserDTO } from "../../models/user.dto";

export interface ExpeditionType {
	id: string;
	nom: string;
	created_by: UserDTO;
	modify_by: UserDTO | null;
	collab: boolean;
	jour?: number;
	pa?: number;
	couleur: string;
	coordonnee: number[][];
	personnel: boolean;
	inactive?: boolean;
	biblio?: boolean;
	brouillon?: boolean;
	trace_expedition?: boolean;
}