import { BanqueItemPlans, ListPlansObtenu } from "../Hotel/PlansChantier.type";
import { ChantierPrototypeDTO }             from "../../models/chantierPrototype.dto";
import { BanqueDTO }                        from "../../models/banque.dto";
import { DefenseDTO }                       from "../../models/defense.dto";
import { CitoyensDTO }                      from "../../models/citoyens.dto";
import { ChantiersDTO }                     from "../../models/chantiers.dto";
import { ItemPrototypeDTO }                 from "../../models/itemPrototype.dto";
import { HomePrototypeDTO }                 from "../../models/homePrototype.dto";
import { UpHomePrototypeDTO }               from "../../models/upHomePrototype.dto";
import { OutilsChantierDTO }                from "../../models/outilsChantier.dto";

export interface OutilsChantiersTypeProps {
	banque: BanqueItemPlans[],
	defense: DefenseDTO,
	listAReparer: BanqueDTO[],
	listAvancement: { pa: number | null }[],
	listChantier: ChantierPrototypeDTO[],
	listChantiersConstruit: ChantiersDTO[],
	listCitoyens: CitoyensDTO[],
	listPlansVille: ListPlansObtenu[],
	option: {
		nb_gardien: number,
		nb_ame_banque: number,
		nb_mort: number,
		id_tdga: number,
		id_cimetiere: number,
		id_cercueil: number,
		id_hamame: number,
		id_strategie: number,
		reductionPA: number,
		pa_repaChantier: number,
		list_armes: ItemPrototypeDTO[],
		list_home: HomePrototypeDTO[],
		list_ame: UpHomePrototypeDTO[],
	},
	outilsChantier: OutilsChantierDTO,
	recapDefAutre: {
		veille: number,
		decharge: number,
		reparation: number,
	}
}

export interface GroupedChantierOption {
	label: string,
	options: ChantierPrototypeDTO[],
}