import { OutilsDechargeDTO }            from "../../models/outilsDecharge.dto";
import { RegroupementItemsDechargeDTO } from "../../models/regroupementItemsDecharge.dto";

export interface OutilsDechargePropsType {
	chantier_decharge: number[],
	chantier_dh: boolean,
	itemBanque: number[],
	mapId: number,
	outils_decharge: OutilsDechargeDTO,
	regroupements: RegroupementItemsDechargeDTO[],
	userId: number,
}

