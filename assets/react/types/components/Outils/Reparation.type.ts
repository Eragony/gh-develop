import { ChantierPrototypeDTO } from "../../models/chantierPrototype.dto";
import { OutilsReparationDTO }  from "../../models/outilsReparation.dto";

export interface OutilsReparationPropsType {
	listCatChantier: ChantierPrototypeDTO[],
	mapId: number,
	outilRepa: OutilsReparationDTO,
	pvByPa: number,
	textReparation: string,
	userId: number,
}