import { CarteType }           from "../ville/Carte.type";
import { UserDTO }             from "../../models/user.dto";
import { CreneauHorraireDTO }  from "../../models/creneauHorraire.dto";
import { DispoTypesDTO }       from "../../models/dispoTypes.dto";
import { HerosPrototypeDTO }   from "../../models/herosPrototype.dto";
import { TypeDispoDTO }        from "../../models/typeDispo.dto";
import { UserPersoCouleurDTO } from "../../models/userPersoCouleur.dto";
import { MenuPrototypeDTO }    from "../../models/menuPrototype.dto";
import { HerosSkillLevelDTO }  from "../../models/herosSkillLevel.dto";
import { HerosSkillTypeDTO }   from "../../models/herosSkillType.dto";

export interface OptionsPersoType {
	carte: CarteType,
	options: OptionsListType,
	user: UserDTO,
}

export interface OptionsListType {
	color: string[],
	conditionOption: number,
	creneauList: CreneauHorraireDTO[],
	delaiList: number[]
	dispoList: TypeDispoDTO[]
	heroList: HerosPrototypeDTO[]
	themeList: string[]
	colormapList: string[]
	baseThemeList: BaseThemeList,
	creneauDispoExpe: {
		creneau: CreneauHorraireDTO[],
		dispo: DispoTypesDTO[]
	}
	motifEpuisement: string[],
	menuPrototype: MenuPrototypeDTO[],
	levelSkill: HerosSkillLevelDTO[],
	skillType: HerosSkillTypeDTO[],
}

export interface ColorConfigExport {
	couleurScrut: string,
	couleurKm: string,
	couleurPos: string,
	couleurBat: string,
	couleurSelectObj: string,
	couleurSelectBat: string,
	couleurSelectCit: string,
	couleurVueAuj: string,
	couleurVue24: string,
	couleurVue48: string,
	couleurPA: string,
	couleurZone: string,
	colorSelExp: string,
	couleurSelCaseMaj: string,
	couleurDanger0: string,
	couleurDanger1: string,
	couleurDanger2: string,
	couleurDanger3: string,
	couleurControleOk: string,
	couleurContoleNok: string,
	couleurEpuise: string,
	couleurCarte: string,
	couleurZombie: string,
	couleurEstimZombie: string,
	couleurFlag: string,
	couleurFlagFinish: string,
	couleurExpeInscrit: string,
	couleurTown: string,
	couleurCity: string,
	couleurNonVu: string,
	colorBatEpuise: string,
	couleurNbrCitoyen: string,
	couleurNbrItemsSol: string,
	carteTextured?: string,
	colorMap?: string,
	alphaColorMap?: string,
	colorStairUp?: string,
	colorStairDown?: string,
	colorSelectObjet?: string,
	colorDispo1?: string,
	colorDispo2?: string,
	colorDispo3?: string,
	colorVilleCommune?: string,
	couleurChantierConstruit?: string,
	couleurChantierRepa?: string,
	couleurChantierEnConst?: string,
	couleurChantierDispo?: string,
	couleurPlanManquant?: string,
	couleurMyExpe?: string,
	colorMyExpe?: string,
}

export interface ColorThemeConfigExport {
	nom: string,
	couleurStats: string,
	couleurBordure: string,
	couleurFond: string,
	couleurLigne: string,
	couleurPrincipale: string,
	couleurSecondaire: string,
	couleurTertiaire: string,
	couleurHover: string,
	couleurPolicePrincipale: string,
	couleurPoliceSecondaire: string,
	couleurLignePrincipale: string,
	couleurLigneSecondaire: string,
	baseTheme: string,
	couleurPoliceStats: string,
	couleurBordureStats: string,
	couleurFondHover: string,
	couleurSpecifique: string,
	couleurSucces: string,
	couleurErreur: string,
	couleurPerso: ColorConfigExport,
}

export interface ColorPickerItemProps {
	label: string,
	color: string,
	onChangeColor: (color: string) => void,
	tooltip?: string
}

export interface SwitchItemProps {
	label: string,
	forHtml: string,
	checked: boolean,
	onChangeUser: (option: boolean) => void,
	tooltip?: string
}

type ItemType = "color" | "switch" | "motif";

interface SettingItemPerso {
	label: string;
	key: string;
	type?: ItemType; // Default to "color" if not provided
	tooltip?: string;
	source?: string;
}

export interface SettingSectionPerso {
	title: string;
	items: SettingItemPerso[];
}


export interface ColorMap {
	id: number,
	nom: string,
}

export interface GestionTheme {
	list: string,
	prealim: string,
	reinit: string,
	creer: string,
	modifier: string,
	sauvegarder: string,
	supprimer: string,
	annuler: string,
	saveAndSelect: string,
	form: {
		nom: string,
		stats: string,
		border: string,
		bg: string,
		myLine: string,
		myLine_info: string,
		secondary: string,
		primary: string,
		tertiary: string,
		hover_font: string,
		primary_font: string,
		secondary_font: string,
		primary_row: string,
		secondary_row: string,
		stats_font_color: string,
		stats_border_color: string,
		bg_hover_color: string,
	},
	titre: {
		general: string,
		graphique: string,
		onglet: string,
	}
}

export interface BaseThemeList {
	dark: BaseTheme,
	light: BaseTheme,
	vintage: BaseTheme,
	hordes: BaseTheme,
	perso: BaseTheme,
}

export interface BaseTheme {
	nom: string,
	statsBgColor: string,
	primaryBorderColor: string,
	backgroundColor: string,
	myLineColor: string,
	primaryColor: string,
	secondaryColor: string,
	specialColor: string,
	tertiaryColor: string,
	hoverFontColor: string,
	primaryFontColor: string,
	tertiaryFontColor: string,
	primaryRowColor: string,
	secondaryRowColor: string,
	statsFontColor: string,
	statsBorderColor: string,
	bgHoverColor: string,
	specifiqueColor: string,
	successColor: string,
	errorColor: string,
	perso: UserPersoCouleurDTO,
}