import { EventDTO } from "../../models/event.dto";

export interface JumpListType {
	date_jour: string,
	event: EventDTO,
	listInscription: string[],
}
