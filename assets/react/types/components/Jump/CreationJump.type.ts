import { UserDTO }                   from "../../models/user.dto";
import { TypeVilleDTO }              from "../../models/typeVille.dto";
import { ObjectifVillePrototypeDTO } from "../../models/objectifVillePrototype.dto";
import { JumpDTO }                   from "../../models/jump.dto";
import { EventDTO }                  from "../../models/event.dto";
import { JobPrototypeDTO }           from "../../models/jobPrototype.dto";

export interface CreationJumpType {
	options: JumpOptionsListType,
	user: UserDTO,
	jump?: JumpDTO,
	event?: EventDTO,
}

export interface JumpOptionsListType {
	listTypeVille: TypeVilleDTO[],
	listObjectif: ObjectifVillePrototypeDTO[],
	listCommu: string[],
	listJob: JobPrototypeDTO[],
}
