import { JumpOptionsListType } from "./CreationJump.type";
import { UserDTO }             from "../../models/user.dto";
import { JumpDTO }             from "../../models/jump.dto";
import { EventDTO }            from "../../models/event.dto";

export interface FormJumpType {
	options: JumpOptionsListType,
	user: UserDTO,
	jump: JumpDTO,
}

export interface FormEventType {
	options: JumpOptionsListType,
	user: UserDTO,
	event: EventDTO,
}
