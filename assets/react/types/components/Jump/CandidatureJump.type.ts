import { JobPrototypeDTO }        from "../../models/jobPrototype.dto";
import { HerosPrototypeDTO }      from "../../models/herosPrototype.dto";
import { UserDTO }                from "../../models/user.dto";
import { TypeDispoDTO }           from "../../models/typeDispo.dto";
import { TypeLeadDTO }            from "../../models/typeLead.dto";
import { LevelRuinePrototypeDTO } from "../../models/levelRuinePrototype.dto";
import { RoleJumpDTO }            from "../../models/roleJump.dto";
import { StatutInscriptionDTO }   from "../../models/statutInscription.dto";
import { JumpDTO }                from "../../models/jump.dto";
import { InscriptionJumpDTO }     from "../../models/inscriptionJump.dto";
import { HerosSkillTypeDTO }      from "../../models/herosSkillType.dto";
import { HerosSkillLevelDTO }     from "../../models/herosSkillLevel.dto";

export interface CandidatureJumpPropsType {
	options: CandidatureOptionsListType,
	user: UserDTO,
	jump: JumpDTO,
	inscription: InscriptionJumpDTO
}

export interface CandidatureOptionsListType {
	helpRuine: string,
	listDispo: TypeDispoDTO[],
	listJob: JobPrototypeDTO[],
	listLead: TypeLeadDTO[],
	listLvlRuine: LevelRuinePrototypeDTO[],
	listPouvoir: HerosPrototypeDTO[],
	listRole: RoleJumpDTO[],
	listStatut: StatutInscriptionDTO[],
	listSkill: HerosSkillTypeDTO[],
	listSkillBase: HerosSkillLevelDTO[],
	role_lead: number,
	role_applead: number,
}
