import { JobPrototypeDTO }        from "../../models/jobPrototype.dto";
import { HerosPrototypeDTO }      from "../../models/herosPrototype.dto";
import { LevelRuinePrototypeDTO } from "../../models/levelRuinePrototype.dto";
import { RoleJumpDTO }            from "../../models/roleJump.dto";
import { StatutInscriptionDTO }   from "../../models/statutInscription.dto";
import { TypeLeadDTO }            from "../../models/typeLead.dto";
import { TypeDispoDTO }           from "../../models/typeDispo.dto";
import { UserDTO }                from "../../models/user.dto";
import { JumpDTO }                from "../../models/jump.dto";
import { InscriptionJumpDTO }     from "../../models/inscriptionJump.dto";
import { HerosSkillTypeDTO }      from "../../models/herosSkillType.dto";
import { HerosSkillLevelDTO }     from "../../models/herosSkillLevel.dto";

export interface InscriptionJumpPropsType {
	options: InscriptionOptionsListType,
	user: UserDTO,
	jump: JumpDTO,
	inscription: InscriptionJumpDTO
}

export interface InscriptionOptionsListType {
	helpRuine: string,
	listDispo: TypeDispoDTO[],
	listJob: JobPrototypeDTO[],
	listLead: TypeLeadDTO[],
	listLvlRuine: LevelRuinePrototypeDTO[],
	listPouvoir: HerosPrototypeDTO[],
	listRole: RoleJumpDTO[],
	listStatut: StatutInscriptionDTO[],
	listSkill: HerosSkillTypeDTO[],
	listSkillBase: HerosSkillLevelDTO[],
	role_lead: number,
	role_applead: number,
}

export interface FieldControlType {
	choix_metier1?: string,
	choix_metier2?: string,
	choix_metier3?: string,
	niveau_ruine?: string,
	der_pouv?: string,
	role?: string,
	lead?: string,
	applead?: string,
	dispo_semaine?: string,
	dispo_weekend?: string,
}