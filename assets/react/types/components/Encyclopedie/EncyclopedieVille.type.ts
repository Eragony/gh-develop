import { HomePrototypeDTO }   from "../../models/homePrototype.dto";
import { UpHomePrototypeDTO } from "../../models/upHomePrototype.dto";

export interface EncyclopedieVilleType {
	listHabitation: HomePrototypeDTO[];
	listUpHome: UpHomePrototypeDTO[];
}
