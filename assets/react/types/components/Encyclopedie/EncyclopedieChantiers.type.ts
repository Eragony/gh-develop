import { ChantierPrototypeDTO } from "../../models/chantierPrototype.dto";
import { BatPrototypeDTO }      from "../../models/batPrototype.dto";

export interface EncyclopedieChantiersType {
	listChantier: ChantierPrototypeDTO[],
	listEvo: ChantierPrototypeDTO[],
	listPlans: ChantierPrototypeDTO[],
	listRuines: BatPrototypeDTO[],
	translate: string[], // c'est la liste des evolutions traduites par le back en attendant de voir si on fait qqcn au front
}