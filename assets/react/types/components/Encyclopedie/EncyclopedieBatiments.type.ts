import { BatPrototypeDTO } from "../../models/batPrototype.dto";
import { JobPrototypeDTO } from "../../models/jobPrototype.dto";


export interface EncyclopedieBatimentsType {
	listBat: BatPrototypeDTO[];
	listBatMaVille: number[];
	listJob: JobPrototypeDTO[];
}
