import React, { useEffect, useState } from "react";
import { BanqueApi }                  from "../../services/api/BanqueApi";
import Banque                         from "../../containers/Ville/Banque/Banque";
import chargement                     from "../../../img/chargement.svg";
import { useNavigate, useParams }     from "react-router";
import { useGHContext }               from "../../types/Context/GHContext";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function BanqueGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	const [banqueData, setBanqueData] = useState(null);
	const { t } = useTranslation();
	
	// Fonction pour recharger les données
	const reloadBanqueData = async () => {
		const banqueApi = new BanqueApi(parseInt(params.mapId, 10));
		banqueApi.general(params.mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.banque === undefined) {
				console.error("Erreur de chargement des données banque", response);
			} else {
				setBanqueData(response.data.banque);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("ville", "banque")) {
			reloadBanqueData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{banqueData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<Banque banque={banqueData} />
			)}
		
		</>
	);
}