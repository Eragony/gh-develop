import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { ExpeditionApi }              from "../../services/api/ExpeditionApi";
import OutilsExpeditions              from "../../containers/Outils/Expedition/OutilsExpeditions";
import { useTranslation }             from "react-i18next";

export function OutilsExpeditionsGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	const { t } = useTranslation();
	const [outilsExpeditionsData, setOutilsExpeditionsData] = useState(null);
	
	// Fonction pour recharger les données
	const reloadOutilsExpeditionsData = async () => {
		const mapId = params.mapId ? parseInt(params.mapId, 10) : 0;
		const outilsExepditionsApi = new ExpeditionApi(mapId);
		outilsExepditionsApi.main(mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.outils === undefined) {
				console.error("Erreur de chargement des données outils expeditions", response);
			} else {
				setOutilsExpeditionsData(response.data.outils);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
			navigate("/");
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("outils", "expe")) {
			reloadOutilsExpeditionsData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{outilsExpeditionsData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<>
					{<OutilsExpeditions carte={outilsExpeditionsData.carte} popUpMaj={outilsExpeditionsData.popUpMaj} />}
				</>
			)}
		
		</>
	);
}