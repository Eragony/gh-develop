import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { DechargesApi }               from "../../services/api/DechargesApi";
import Decharge                       from "../../containers/Outils/Decharges/Decharge";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function OutilsDechargesGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	
	const { t } = useTranslation();
	const [outilsDechargesData, setOutilsDechargesData] = useState(null);
	
	// Fonction pour recharger les données
	const reloadOutilsDechargesData = async () => {
		const outilsDechargesApi = new DechargesApi(parseInt(params.id, 10));
		
		outilsDechargesApi.main().then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.outilsDecharge === undefined) {
					console.error("Erreur de chargement des données outils decharges", response);
				} else {
					setOutilsDechargesData(response.zoneRetour.outilsDecharge);
				}
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
			} else if (response.codeRetour === 1) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
			} else if (response.codeRetour === 2) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
				navigate("/news");
			}
		});
	};
	
	useEffect(() => {
		
		if (checkHabilitation("outils", "decharge")) {
			reloadOutilsDechargesData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{outilsDechargesData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<Decharge decharge={outilsDechargesData} />
			)}
		
		</>
	);
}