import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { EncyclopedieApi }            from "../../services/api/EncyclopedieApi";
import EncyBatiments                  from "../../containers/Encyclopedie/EncyBatiments";
import { useNavigate }                from "react-router";
import { useGHContext }               from "../../types/Context/GHContext";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useTranslation }             from "react-i18next";

export function EncyBatimentsGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [encyBatData, setEncyBatData] = useState(null);
	
	
	// Fonction pour recharger les données
	const reloadData = async () => {
		const encyclopedieBatApi = new EncyclopedieApi(parseInt(sessionStorage.getItem("mapId") ?? "0", 10));
		
		encyclopedieBatApi.main_bats().then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
				if (response.zoneRetour.batiments === undefined) {
					console.error("Erreur de chargement des batiments", response);
				} else {
					setEncyBatData(response.zoneRetour.batiments);
				}
			}
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("encyclopedie", "batiments")) {
			reloadData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{encyBatData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<EncyBatiments batiments={encyBatData} />
			)}
		
		</>
	);
}