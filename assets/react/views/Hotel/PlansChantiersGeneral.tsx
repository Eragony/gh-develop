import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { PlansChantierApi }           from "../../services/api/PlansChantierApi";
import PlansChantiers                 from "../../containers/Hotel/PlansChantiers/PlansChantiers";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function PlansChantiersGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const { t } = useTranslation();
	
	const [plansChantiersData, setPlansChantiersData] = useState(null);
	const params = useParams();
	
	// Fonction pour recharger les données
	const reloadPlansChantiersData = async () => {
		const plansChantierApi = new PlansChantierApi(parseInt(params.mapId, 10));
		
		plansChantierApi.main(params.mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
				if (response.zoneRetour.plansChantier === undefined) {
					console.error("Erreur de chargement des données plansChantier", response);
				} else {
					setPlansChantiersData(response.zoneRetour.plansChantier);
				}
			} else if (response.codeRetour === 1) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
				navigate("/news");
			}
		});
	};
	
	useEffect(() => {
		
		if (checkHabilitation("hotel", "plans")) {
			reloadPlansChantiersData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{plansChantiersData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<PlansChantiers plansChantier={plansChantiersData} />
			)}
		
		</>
	);
}