import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import InscriptionExpedition          from "../../containers/Hotel/Inscription/InscriptionExpedition";
import { InscriptionExpeditionApi }   from "../../services/api/InscriptionExpeditionApi";
import { useTranslation }             from "react-i18next";


export function InscriptionExpeditionGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [inscriptionData, setInscriptionData] = useState(null);
	const params = useParams();
	
	// Fonction pour recharger les données
	const reloadInscriptionData = async () => {
		const mapId = parseInt(params.mapId, 10);
		const inscriptionExpeditionApi = new InscriptionExpeditionApi(mapId);
		inscriptionExpeditionApi.main(mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.inscription === undefined) {
				console.error("Erreur de chargement des inscriptions", response.data);
			} else {
				setInscriptionData(response.data.inscription);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
			navigate("/news");
		});
	};
	
	useEffect(() => {
		
		if (checkHabilitation("hotel", "inscription")) {
			reloadInscriptionData().then(r => r);
		} else {
			navigate("/");
		}
	}, [refreshKey]);
	
	return (
		<>
			{inscriptionData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<InscriptionExpedition inscription={inscriptionData} />
			)}
		
		</>
	);
}