import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { TourDeGuetApi }              from "../../services/api/TourDeGuetApi";
import TourDeGuet                     from "../../containers/Hotel/TourDeGuet/TourDeGuet";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";


export function TourDeGuetGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [tdgData, setTdgData] = useState(null);
	const params = useParams();
	
	// Fonction pour recharger les données
	const reloadTdgData = async () => {
		const tourDeGuetApi = new TourDeGuetApi(parseInt(params.mapId ?? "0", 10));
		tourDeGuetApi.main(params.mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
				if (response.zoneRetour.tdg === undefined) {
					console.error("Erreur de chargement des données tdg", response);
				} else {
					setTdgData(response.zoneRetour.tdg);
				}
			} else if (response.codeRetour === 1) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
				navigate("/news");
			}
		});
	};
	
	useEffect(() => {
		
		if (checkHabilitation("hotel", "tour")) {
			reloadTdgData().then(r => r);
		} else {
			navigate("/");
		}
	}, [refreshKey]);
	
	return (
		<>
			{tdgData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<TourDeGuet tourDeGuet={tdgData} />
			)}
		
		</>
	);
}