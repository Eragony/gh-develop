import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { VilleApi }                   from "../../services/api/VilleApi";
import Villes                         from "../../containers/Villes/Villes";
import { useNavigate }                from "react-router";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useTranslation }             from "react-i18next";

export function VillesGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [villesData, setVillesData] = useState(null);
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const villeApi = new VilleApi(parseInt(sessionStorage.getItem("mapId") ?? "0", 10));
	
	// Fonction pour recharger les données
	const reloadOptionsPersoData = async () => {
		villeApi.main().then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.villes === undefined) {
				console.error("Erreur de chargement des données villes", response);
			} else {
				setVillesData(response.data.villes);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("villes")) {
			reloadOptionsPersoData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{villesData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<Villes villes={villesData} />
			)}
		
		</>
	);
}