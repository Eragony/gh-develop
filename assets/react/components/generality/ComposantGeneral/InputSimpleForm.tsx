import React from "react";

const InputSimpleForm = (props: { label: string, name: string, type_input: string, class_name: string, value: any, onChange: (valueChange: any) => void }) => {
		  const { label, name, type_input, class_name, value, onChange } = props;
		  return (
			  <div className={class_name}>
				  <label htmlFor={name}>{label}</label>
				  <input id={name} name={name} type={type_input} value={value} onChange={(event) => onChange(event.target.value)} />
			  </div>
		  );
	  }
;

export default InputSimpleForm;