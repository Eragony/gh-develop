import React                 from "react";
import { useTranslation }    from "react-i18next";
import { HerosPrototypeDTO } from "../../../types/models/herosPrototype.dto";
import SvgIcone              from "../../../components/generality/SvgIcone";


export default function TabHeros({ listHeros }: { listHeros: HerosPrototypeDTO[] }) {
	const { t } = useTranslation();
	
	return <div id="zone_expHeros">
		<div id="zoneExpHeros">
			<p>{t("Tableau donnant les différents pouvoirs héros obtenables en fonction du nombre d'expérience jour héros.", { ns: "ency" })}</p>
			<table id="ency_experience_heros">
				<thead>
				<tr className="lign_exp_heros_ency">
					<th className="tab_ency_heros_img">{t("Image", { ns: "ency" })}</th>
					<th className="tab_ency_heros_nom">{t("Nom", { ns: "ency" })}</th>
					<th className="tab_ency_heros_desc">{t("Description", { ns: "ency" })}</th>
					<th className="tab_ency_heros_jour">{t("Jours différences", { ns: "ency" })}</th>
					<th className="tab_ency_heros_jourCumul">{t("Jours cumulés", { ns: "ency" })}</th>
				</tr>
				</thead>
				<tbody>
				{Object.values(listHeros).map((heros, index) => {
					return <tr className="lign_exp_heros_ency" key={"heros_" + index}>
						<td className="tab_ency_heros_img">
							<div>
								<div className="iconItems"><SvgIcone icone={heros.icon} /></div>
							</div>
						</td>
						<td className="tab_ency_heros_nom">{t(heros.nom, { ns: "game" })}</td>
						<td className="tab_ency_heros_desc">{t(heros.description, { ns: "game" })}</td>
						<td className="tab_ency_heros_jour">{heros.jour}</td>
						<td className="tab_ency_heros_jourCumul">{heros.jour_cumul}</td>
					</tr>;
				})}
				</tbody>
			</table>
		</div>
	</div>;
	
}
