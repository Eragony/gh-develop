import React                from "react";
import { useRecordContext } from "react-admin";
import SvgRuine             from "../../../../../components/generality/SvgRuine";

export const TypeEscalierField = () => {
	const record = useRecordContext();
	return (record ? (<>
                <span>
                    <SvgRuine icone={record.id.toString()} classRuine={"ruineCarteAdmin"} />
                </span>
				<span>{record.name}</span>
			</>
		) : null
	);
};