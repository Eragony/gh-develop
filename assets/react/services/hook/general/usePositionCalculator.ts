import { useCallback } from "react";

export type Direction = {
	id: string;
	label: string;
};

export const DIRECTIONS = {
	VILLE     : { id: "1", label: "Ville" },
	NORD      : { id: "2", label: "Nord" },
	EST       : { id: "3", label: "Est" },
	SUD       : { id: "4", label: "Sud" },
	OUEST     : { id: "5", label: "Ouest" },
	NORD_EST  : { id: "6", label: "Nord-Est" },
	SUD_EST   : { id: "7", label: "Sud-Est" },
	SUD_OUEST : { id: "8", label: "Sud-Ouest" },
	NORD_OUEST: { id: "9", label: "Nord-Ouest" },
} as const;

type Position = {
	x: number;
	y: number;
};

type UsePositionCalculatorResult = {
	calculateDistanceInKm: (from: Position, to: Position) => number;
	calculateDirection: (from: Position, to: Position) => Direction;
};

export const usePositionCalculator = (): UsePositionCalculatorResult => {
	const calculateDistanceInKm = useCallback((from: Position, to: Position): number => {
		return Math.round(
			Math.sqrt(Math.pow(to.x - from.x, 2) + Math.pow(to.y - from.y, 2)),
		);
	}, []);
	
	const calculateDirection = useCallback((from: Position, to: Position): Direction => {
		const xNorm = to.x - from.x;
		const yNorm = to.y - from.y;
		
		if (xNorm === 0 && yNorm === 0) {
			return DIRECTIONS.VILLE;
		}
		
		const isClose = (x: number, y: number) =>
			Math.abs(Math.abs(x) - Math.abs(y)) < Math.min(Math.abs(x), Math.abs(y));
		
		if (xNorm !== 0 && yNorm !== 0 && isClose(xNorm, yNorm)) {
			if (xNorm < 0 && yNorm < 0) {
				return DIRECTIONS.SUD_OUEST;
			}
			if (xNorm < 0 && yNorm > 0) {
				return DIRECTIONS.NORD_OUEST;
			}
			if (xNorm > 0 && yNorm < 0) {
				return DIRECTIONS.SUD_EST;
			}
			if (xNorm > 0 && yNorm > 0) {
				return DIRECTIONS.NORD_EST;
			}
		}
		
		if (Math.abs(xNorm) > Math.abs(yNorm)) {
			return xNorm < 0 ? DIRECTIONS.OUEST : DIRECTIONS.EST;
		}
		
		return yNorm < 0 ? DIRECTIONS.SUD : DIRECTIONS.NORD;
	}, []);
	
	return {
		calculateDistanceInKm,
		calculateDirection,
	};
};