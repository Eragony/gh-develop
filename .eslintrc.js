module.exports = {
    root          : true,
    env           : {
        browser: true,
        es2021 : true,
    },
    extends       : [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended",
    ],
    ignorePatterns: ["dist", ".eslintrc.cjs", "node_modules", "build"],
    overrides     : [
        {
            env          : {
                node: true,
            },
            files        : [".eslintrc.{js,cjs}"],
            parserOptions: {
                sourceType: "script",
            },
        },
    ],
    parser        : "@typescript-eslint/parser",
    parserOptions : {
        ecmaVersion: "latest",
        sourceType : "module",
    },
    plugins       : ["@typescript-eslint", "react", "react-refresh"],
    rules         : {
        quotes                                : ["error", "double"],
        semi                                  : ["error", "always"],
        "react-refresh/only-export-components": [
            "warn",
            { allowConstantExport: true },
        ],
    },
};
