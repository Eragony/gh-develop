<?php

namespace App\Repository;

use App\Entity\RuinesObjets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuinesObjets>
 *
 * @method RuinesObjets|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuinesObjets|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuinesObjets[]    findAll()
 * @method RuinesObjets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuinesObjetsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuinesObjets::class);
    }

//    /**
//     * @return RuinesObjets[] Returns an array of RuinesObjets objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RuinesObjets
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
