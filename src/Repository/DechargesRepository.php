<?php

namespace App\Repository;

use App\Entity\Decharges;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Decharges|null find($id, $lockMode = null, $lockVersion = null)
 * @method Decharges|null findOneBy(array $criteria, array $orderBy = null)
 * @method Decharges[]    findAll()
 * @method Decharges[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DechargesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Decharges::class);
    }
    
    // /**
    //  * @return Decharges[] Returns an array of Decharges objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Decharges
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
