<?php

namespace App\Repository;

use App\Entity\DispoJump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DispoJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method DispoJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method DispoJump[]    findAll()
 * @method DispoJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispoJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DispoJump::class);
    }
    
    // /**
    //  * @return DispoJump[] Returns an array of DispoJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?DispoJump
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
