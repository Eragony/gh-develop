<?php

namespace App\Repository;

use App\Entity\BatPrototype;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ZoneMap|null find($id, $lockMode = null, $lockVersion = null)
 * @method ZoneMap|null findOneBy(array $criteria, array $orderBy = null)
 * @method ZoneMap[]    findAll()
 * @method ZoneMap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZoneMapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ZoneMap::class);
    }
    
    public function getCaseEpuise(Ville $ville): int
    {
        return $this->createQueryBuilder('zm')
                    ->select('count(zm.id)')
                    ->where('zm.ville = :ville')
                    ->andWhere('zm.dried = 1')
                    ->andWhere('zm.vue <> :vue')
                    ->setParameter('ville', $ville)
                    ->setParameter('vue', ZoneMap::NON_EXPLO)
                    ->getQuery()
                    ->getSingleScalarResult();
    }
    
    public function getExploration(Ville $ville): int
    {
        return $this->createQueryBuilder('zm')
                    ->select('count(zm.id)')
                    ->where('zm.ville = :ville')
                    ->andWhere('zm.vue <> :vue')
                    ->setParameter('ville', $ville)
                    ->setParameter('vue', ZoneMap::NON_EXPLO)
                    ->getQuery()
                    ->getSingleScalarResult();
    }
    
    public function majVue(Ville $ville)
    {
        $this->createQueryBuilder('zm')->update()
             ->set('zm.vue', ZoneMap::CASE_NONVUE)
             ->where('zm.ville = :ville')
             ->andWhere('zm.vue = :vue')
             ->andWhere('zm.day < :day or zm.day is null')
             ->setParameters(['ville' => $ville, 'vue' => ZoneMap::CASE_VUE, 'day' => $ville->getJour()])
             ->getQuery()
             ->execute();
    }
    
    public function majVuePdc(Ville $ville)
    {
        $requete = $this->createQueryBuilder('z')
                        ->update(ZoneMap::class, 'z')
                        ->set('z.vue', 0)
                        ->set('z.pdc', 0)
                        ->where('z.ville = :ville')
                        ->setParameter('ville', $ville)
                        ->getQuery();
        
        return $requete->execute();
    }
    
    /**
     * @return array|ZoneMap[]
     */
    public function recupBatiment(Ville $ville): array
    {
        
        return $this->createQueryBuilder('z')
                    ->join(BatPrototype::class, 'b', Join::WITH, 'z.bat = b')
                    ->where('z.ville = :ville')
                    ->andWhere('z.bat is not null')
                    ->andWhere('b.explorable = 0')
                    ->setParameter('ville', $ville)
                    ->orderBy('b.nom')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return array|ZoneMap[]
     */
    public function recupHypoBatiment(Ville $ville): array
    {
        
        return $this->createQueryBuilder('z')
                    ->join(BatPrototype::class, 'b', Join::WITH, 'z.batHypothese = b')
                    ->where('z.ville = :ville')
                    ->andWhere('z.batHypothese is not null')
                    ->andWhere('b.explorable = 0')
                    ->setParameter('ville', $ville)
                    ->orderBy('b.nom')
                    ->getQuery()
                    ->getResult();
        
    }
    
    public function recupRuine(Ville $ville): array
    {
        
        return $this->createQueryBuilder('z')
                    ->join(BatPrototype::class, 'b', Join::WITH, 'z.bat = b')
                    ->where('z.ville = :ville')
                    ->andWhere('z.bat is not null')
                    ->andWhere('b.explorable = 1')
                    ->setParameter('ville', $ville)
                    ->getQuery()
                    ->getResult();
        
    }
    
    public function recuperationStatBatimentByVille(?int $saison, ?string $phase): array
    {
        // On récupère la somme des chantiers par type de ville
        // (on ne prend pas en compte les chantiers en cours de construction)
        $batiment = $this->createQueryBuilder('zm')
                         ->select('b.id as id, count(zm.id) as nbr, b.nom')
                         ->addSelect('CASE
						   WHEN v.weight <= 15 THEN \'RNE\'
						   WHEN v.weight > 15 AND v.hard =0 THEN \'RE\'
						   WHEN v.hard = 1 THEN \'Pande\'
						   ELSE \'Autre\'
						   END as typeVille')
                         ->where('zm.bat is not null')
                         ->innerJoin(BatPrototype::class, 'b', Join::WITH, 'zm.bat = b')
                         ->join(Ville::class, 'v', Join::WITH, 'zm.ville = v');
        
        if ($saison !== null && $phase !== null) {
            $batiment->andwhere('v.saison = :saison')
                     ->andWhere('v.phase = :phase')
                     ->setParameter('saison', $saison)
                     ->setParameter('phase', $phase);
        }
        return $batiment->groupBy('typeVille')
                        ->addGroupBy('b.nom')
                        ->getQuery()
                        ->getResult();
    }
    
    // /**
    //  * @return ZoneMap[] Returns an array of ZoneMap objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('z.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ZoneMap
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
