<?php

namespace App\Repository;

use App\Entity\StatutInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatutInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutInscription[]    findAll()
 * @method StatutInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutInscription::class);
    }
    
    /**
     * @return StatutInscription[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('si', 'si.id')->getQuery()->getResult();
    }
    
    /**
     * @return StatutInscription[]
     */
    public function statutGestionCandidature(): array
    {
        return $this->createQueryBuilder('si')
                    ->where('si.visibleCandidature <> 0')
                    ->orderBy('si.orderInGestion', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return StatutInscription[] Returns an array of StatutInscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?StatutInscription
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
