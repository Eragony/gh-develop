<?php

namespace App\Repository;

use App\Entity\Banque;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Banque|null find($id, $lockMode = null, $lockVersion = null)
 * @method Banque|null findOneBy(array $criteria, array $orderBy = null)
 * @method Banque[]    findAll()
 * @method Banque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BanqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Banque::class);
    }
    
    /**
     * @throws Exception
     */
    public function somItemBanque(Ville $ville): array
    {
        
        $idVille = $ville->getId();
        
        $rawSQL = "SELECT SUM(b.nombre) as nbrItem, ip.id, ip.nom, ip.icon
                        FROM banque b
                        INNER JOIN item_prototype ip on b.item_id = ip.id
                    WHERE b.ville_id = :ville
                    GROUP BY ip.id
                    ORDER BY ip.id ASC";
        
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSQL);
        $stmt->bindValue(':ville', $idVille);
        
        $result = $stmt->executeQuery();
        
        return $result->fetchAllAssociative();
    }
    
    /**
     * @throws Exception
     */
    public function somItemBanqueBroke(Ville $ville): array
    {
        
        $idVille = $ville->getId();
        
        $rawSQL = "SELECT SUM(b.nombre) as nbrItem, ip.id, ip.nom, ip.icon
                        FROM banque b
                        INNER JOIN item_prototype ip on b.item_id = ip.id
                    WHERE b.ville_id = :ville
                    AND b.broked = 1
                    GROUP BY ip.id
                    ORDER BY ip.id ASC";
        
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSQL);
        $stmt->bindValue(':ville', $idVille);
        
        $result = $stmt->executeQuery();
        
        return $result->fetchAllAssociative();
    }
    
    public function somItemBanqueItems(Ville $ville, array $uidItems): int
    {
        $request = $this->createQueryBuilder('b')
                        ->select('SUM(b.nombre) as nbrItem')
                        ->join('b.item', 'i')
                        ->where('b.ville = :ville')
                        ->andWhere('i.uid in (:idCarac)')
                        ->setParameter('ville', $ville)
                        ->setParameter('idCarac', $uidItems)
                        ->getQuery();
        
        return $request->getResult()[0]['nbrItem'] ?? 0;
    }
    
    public function somItemBanqueOneTypeCarac(Ville $ville, int $idCarac): int
    {
        $request = $this->createQueryBuilder('b')
                        ->select('SUM(b.nombre) as nbrItem')
                        ->join('b.item', 'i')
                        ->join('i.caracteristiques', 'c')
                        ->join('c.typeCarac', 'tc')
                        ->where('b.ville = :ville')
                        ->andWhere('tc.id = :idCarac')
                        ->setParameter('ville', $ville)
                        ->setParameter('idCarac', $idCarac)
                        ->getQuery();
        
        return $request->getResult()[0]['nbrItem'] ?? 0;
    }
    
    
    // /**
    //  * @return Banque[] Returns an array of Banque objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Banque
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
