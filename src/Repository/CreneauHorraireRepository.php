<?php

namespace App\Repository;

use App\Entity\CreneauHorraire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreneauHorraire|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreneauHorraire|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreneauHorraire[]    findAll()
 * @method CreneauHorraire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreneauHorraireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreneauHorraire::class);
    }
    
    // /**
    //  * @return CreneauHorraire[] Returns an array of CreneauHorraire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?CreneauHorraire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
