<?php

namespace App\Repository;

use App\Entity\Coalition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Coalition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coalition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coalition[]    findAll()
 * @method Coalition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoalitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coalition::class);
    }
    
    // /**
    //  * @return Coalition[] Returns an array of Coalition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Coalition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
