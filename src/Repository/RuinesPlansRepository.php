<?php

namespace App\Repository;

use App\Entity\RuinesPlans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RuinesPlans|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuinesPlans|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuinesPlans[]    findAll()
 * @method RuinesPlans[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuinesPlansRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuinesPlans::class);
    }
    
    // /**
    //  * @return RuinesPlans[] Returns an array of RuinesPlans objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RuinesPlans
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
