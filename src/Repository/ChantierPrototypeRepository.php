<?php

namespace App\Repository;

use App\Entity\ChantierPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChantierPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChantierPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChantierPrototype[]    findAll()
 * @method ChantierPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChantierPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChantierPrototype::class);
    }
    
    public function findAllAlpha()
    {
        return $this->createQueryBuilder('c')->orderBy('c.nom', 'ASC')->getQuery()->getResult();
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('c', 'c.id')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return ChantierPrototype[]
     */
    public function findParentEnfant(): array
    {
        $idFilles = $this->createQueryBuilder('c')
                         ->select('c.id')
                         ->where('c.plan <> 0')
                         ->getQuery()
                         ->getResult();
        
        
        return $this->createQueryBuilder('c')
                    ->where('c.plan <> 0')
                    ->andWhere('c.parent in (:idFilles)')
                    ->setParameter('idFilles', $idFilles)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function findPlansChantiers(): array
    {
        
        return $this->createQueryBuilder('c')
                    ->where('c.plan <> 0')
                    ->orderBy('c.plan', 'ASC')
                    ->addOrderBy('c.nom', 'ASC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function findPlansChantiersByColor(int $lvlPlan): array
    {
        
        return $this->createQueryBuilder('c')
                    ->where('c.plan = :lvlPlan')
                    ->setParameter(':lvlPlan', $lvlPlan)
                    ->orderBy('c.nom', 'ASC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function findPlansChantiersByRuine(string $ruine): array
    {
        $requete = $this->createQueryBuilder('c')
                        ->where('c.plan <> 0');
        
        match ($ruine) {
            ChantierPrototype::RUINE_HOTEL   => $requete->andWhere('c.ruineHo = 1'),
            ChantierPrototype::RUINE_HOPITAL => $requete->andWhere('c.ruineHs = 1'),
            ChantierPrototype::RUINE_BUNKER  => $requete->andWhere('c.ruineBu = 1'),
            default                          => $requete->orderBy('c.plan', 'ASC')
                                                        ->addOrderBy('c.nom', 'ASC')
                                                        ->getQuery()
                                                        ->getResult(),
        };
        
        
        return $requete->orderBy('c.plan', 'ASC')
                       ->addOrderBy('c.nom', 'ASC')
                       ->getQuery()
                       ->getResult();
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function findPlansChantiersByRuineByColor(string $ruine, int $lvlPlan): array
    {
        $requete = $this->createQueryBuilder('c')
                        ->where('c.plan = :lvlPlan')
                        ->setParameter(':lvlPlan', $lvlPlan);
        
        match ($ruine) {
            ChantierPrototype::RUINE_HOTEL   => $requete->andWhere('c.ruineHo = 1'),
            ChantierPrototype::RUINE_HOPITAL => $requete->andWhere('c.ruineHs = 1'),
            ChantierPrototype::RUINE_BUNKER  => $requete->andWhere('c.ruineBu = 1'),
            default                          => $requete->orderBy('c.plan', 'ASC')
                                                        ->addOrderBy('c.nom', 'ASC')
                                                        ->getQuery()
                                                        ->getResult(),
        };
        
        
        return $requete->orderBy('c.plan', 'ASC')
                       ->addOrderBy('c.nom', 'ASC')
                       ->getQuery()
                       ->getResult();
    }
    
    public function lastId(): int
    {
        $result = $this->createQueryBuilder('c')->select('c.id')
                       ->orderBy('c.id', 'DESC')
                       ->setMaxResults(1)
                       ->getQuery()
                       ->getSingleScalarResult();
        
        return $result + 1;
    }
    
    /**
     * @return ChantierPrototype[]
     */
    public function recupCategorie(): array
    {
        return $this->createQueryBuilder('c')
                    ->where('c.parent is Null')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function recupChantierDechargesCarte(array $idChantier): array
    {
        return $this->createQueryBuilder('c', 'c.id')
                    ->where('c.id in (:id)')
                    ->setParameter('id', implode(',', $idChantier))
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @Return ChantierPrototype[]
     */
    public function recupChantierEvolution(): array
    {
        return $this->createQueryBuilder('c', 'c.id')
                    ->where('c.levelMax > 0')
                    ->getQuery()
                    ->getResult();
    }
    // /**
    //  * @return ChantierPrototype[] Returns an array of ChantierPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ChantierPrototype
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
