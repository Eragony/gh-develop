<?php

namespace App\Repository;

use App\Entity\HerosSkillType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HerosSkillType>
 */
class HerosSkillTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HerosSkillType::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $qb = $this->createQueryBuilder('hst')
                   ->select('MAX(hst.id)')
                   ->getQuery();
        
        return ($qb->getSingleScalarResult() ?? 0);
    }

    /**
     * @return HerosSkillType[]
     */
    public function getHerosSkillWithoutBase(): array
    {
        $qb = $this->createQueryBuilder('hst')
                   ->select('hst')
                   ->where('hst.id <> 1')
                   ->getQuery();
        
        return $qb->getResult();
    }
//    /**
//     * @return HerosSkillType[] Returns an array of HerosSkillType objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HerosSkillType
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
