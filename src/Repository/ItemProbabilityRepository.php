<?php

namespace App\Repository;

use App\Entity\ItemProbability;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemProbability|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemProbability|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemProbability[]    findAll()
 * @method ItemProbability[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemProbabilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemProbability::class);
    }
    
    // /**
    //  * @return ItemProbability[] Returns an array of ItemProbability objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ItemProbability
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
