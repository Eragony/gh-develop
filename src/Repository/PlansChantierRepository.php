<?php

namespace App\Repository;

use App\Entity\PlansChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlansChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlansChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlansChantier[]    findAll()
 * @method PlansChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlansChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlansChantier::class);
    }
    
    // /**
    //  * @return PlansChantier[] Returns an array of PlansChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?PlansChantier
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
