<?php

namespace App\Repository;

use App\Entity\TraceExpedition;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TraceExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method TraceExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method TraceExpedition[]    findAll()
 * @method TraceExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TraceExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TraceExpedition::class);
    }
    
    /**
     * @return TraceExpedition[]
     */
    public function findAllBiblio(Ville $ville): array
    {
        return $this->createQueryBuilder('te')
                    ->where('te.ville = :ville')
                    ->andWhere('te.biblio = 1')
                    ->addOrderBy('te.createdAt', 'DESC')
                    ->setParameter('ville', $ville)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return TraceExpedition[]
     */
    public function findAllBrouillon(Ville $ville): array
    {
        return $this->createQueryBuilder('te')
                    ->where('te.ville = :ville')
                    ->andWhere('te.brouillon = 1')
                    ->addOrderBy('te.createdAt', 'DESC')
                    ->setParameter('ville', $ville)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return TraceExpedition[]
     */
    public function findAllExpeOrderByDayDesc(Ville $ville, User $user): array
    {
        return $this->createQueryBuilder('te')
                    ->where('te.ville = :ville')
                    ->andWhere('te.inactive is null or te.inactive = 0')
                    ->andWhere('te.personnel = 0 or (te.personnel = 1 and te.createdBy = :user)')
                    ->andWhere('te.biblio = 0')
                    ->andWhere('te.brouillon = 0')
                    ->orderBy('te.jour', 'DESC')
                    ->addOrderBy('te.createdAt', 'DESC')
                    ->setParameter('ville', $ville)
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->getResult();
    }
    
    public function getListExpedition(Ville $ville): array
    {
        return $this->createQueryBuilder('te')
                    ->where('te.ville = :ville')
                    ->andWhere('te.collab = 1')
                    ->andWhere('te.inactive is null or te.inactive = 0')
                    ->andWhere('te.biblio = 0')
                    ->andWhere('te.brouillon = 0')
                    ->andWhere('te.traceExpedition = 0')
                    ->orderBy('te.jour', 'DESC')
                    ->addOrderBy('te.createdAt', 'DESC')
                    ->setParameter('ville', $ville)
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return TraceExpedition[] Returns an array of TraceExpedition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TraceExpedition
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
