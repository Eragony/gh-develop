<?php

namespace App\Repository;

use App\Entity\StatutUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatutUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutUser[]    findAll()
 * @method StatutUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutUser::class);
    }
    
    // /**
    //  * @return StatutUser[] Returns an array of StatutUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?StatutUser
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
