<?php

namespace App\Repository;

use App\Entity\DisponibiliteJoueurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DisponibiliteJoueurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method DisponibiliteJoueurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method DisponibiliteJoueurs[]    findAll()
 * @method DisponibiliteJoueurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisponibiliteJoueursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DisponibiliteJoueurs::class);
    }
    
    // /**
    //  * @return DisponibiliteJoueurs[] Returns an array of DisponibiliteJoueurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?DisponibiliteJoueurs
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
