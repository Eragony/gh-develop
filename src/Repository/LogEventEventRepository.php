<?php

namespace App\Repository;

use App\Entity\LogEventEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LogEventEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogEventEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogEventEvent[]    findAll()
 * @method LogEventEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogEventEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogEventEvent::class);
    }
    
    // /**
    //  * @return LogEventEvent[] Returns an array of LogEventEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?LogEventEvent
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
