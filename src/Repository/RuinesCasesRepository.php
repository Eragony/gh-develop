<?php

namespace App\Repository;

use App\Entity\Ruines;
use App\Entity\RuinesCases;
use App\Entity\RuinesPlans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RuinesCases|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuinesCases|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuinesCases[]    findAll()
 * @method RuinesCases[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuinesCasesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuinesCases::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existanceCaseId(int $id, Ruines $ruine): bool
    {
        $qb = $this->createQueryBuilder('rc');
        
        $qb->select('COUNT(rc.id)')
           ->join(RuinesPlans::class, 'rp', 'WITH', 'rp.id = rc.ruinesPlans')
           ->where('rc.id = :id')
           ->andWhere('rp.ruines = :ruine')
           ->setParameter('id', $id)
           ->setParameter('ruine', $ruine);
        
        return (bool)$qb->getQuery()->getSingleScalarResult();
    }
    
    // /**
    //  * @return RuinesCases[] Returns an array of RuinesCases objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r . exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r . id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RuinesCases
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r . exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
