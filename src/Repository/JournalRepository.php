<?php

namespace App\Repository;

use App\Entity\Journal;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Journal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Journal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Journal[]    findAll()
 * @method Journal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JournalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Journal::class);
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function dernierJournal(Ville $ville): ?Journal
    {
        
        return $this->createQueryBuilder('j')
                    ->where('j.ville = :ville')
                    ->setParameter('ville', $ville)
                    ->orderBy('j.day', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
        
    }
    
    public function moyenneAttaque(int $saison): array
    {
        
        $q = $this->createQueryBuilder('j');
        
        return $q->select('j.day as day, j.zombie')
                 ->from('App:Ville', 'v')
                 ->where('j.zombie <> 0')
                 ->andWhere('v.prived <> 1')
            //->andWhere('e.maxedJour = 1')
                 ->andWhere('j.ville = v.id')
                 ->andWhere('v.saison = :saison')
                 ->setParameter('saison', $saison)
                 ->getQuery()
                 ->getResult();
        
    }
    
    public function statsScrutateur(?int $saison, ?string $phase): array
    {
        // On récupère la somme des chantiers par type de ville
        // (on ne prend pas en compte les chantiers en cours de construction)
        $journal = $this->createQueryBuilder('j')
                        ->select('count(j.id) as nbr, j.regenDir as direction')
                        ->addSelect('CASE
						   WHEN v.weight <= 15 THEN \'RNE\'
						   WHEN v.weight > 15 AND v.hard =0 THEN \'RE\'
						   WHEN v.hard = 1 THEN \'Pande\'
						   ELSE \'Autre\'
						   END as typeVille')
                        ->join(Ville::class, 'v', Join::WITH, 'j.ville = v')
                        ->where('j.regenDir is not null')
                        ->andWhere('j.regenDir<> :regenInconnu')
                        ->andWhere('j.regenDir<> :regenInvalid')
                        ->setParameter('regenInconnu', 'Inconnue')
                        ->setParameter('regenInvalid', "invalid direction");
        
        if ($saison !== null && $phase !== null) {
            $journal->andwhere('v.saison = :saison')
                    ->andWhere('v.phase = :phase')
                    ->setParameter('saison', $saison)
                    ->setParameter('phase', $phase);
        }
        return $journal->groupBy('typeVille')
                       ->addGroupBy('j.regenDir')
                       ->getQuery()
                       ->getResult();
    }
    
    // /**
    //  * @return Journal[] Returns an array of Journal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Journal
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
