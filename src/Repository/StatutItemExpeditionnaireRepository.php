<?php

namespace App\Repository;

use App\Entity\StatutItemExpeditionnaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StatutItemExpeditionnaire>
 *
 * @method StatutItemExpeditionnaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutItemExpeditionnaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutItemExpeditionnaire[]    findAll()
 * @method StatutItemExpeditionnaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutItemExpeditionnaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutItemExpeditionnaire::class);
    }
    
    //    /**
    //     * @return StatutItemExpeditionnaire[] Returns an array of StatutItemExpeditionnaire objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?StatutItemExpeditionnaire
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
