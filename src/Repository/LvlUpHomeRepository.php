<?php

namespace App\Repository;

use App\Entity\LvlUpHome;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LvlUpHome|null find($id, $lockMode = null, $lockVersion = null)
 * @method LvlUpHome|null findOneBy(array $criteria, array $orderBy = null)
 * @method LvlUpHome[]    findAll()
 * @method LvlUpHome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LvlUpHomeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LvlUpHome::class);
    }
    
    // /**
    //  * @return LvlUpHome[] Returns an array of LvlUpHome objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?LvlUpHome
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
