<?php

namespace App\Repository;

use App\Entity\ItemBatiment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemBatiment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemBatiment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemBatiment[]    findAll()
 * @method ItemBatiment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemBatimentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemBatiment::class);
    }
    
    // /**
    //  * @return ItemBatiment[] Returns an array of ItemBatiment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ItemBatiment
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
