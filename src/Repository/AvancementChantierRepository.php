<?php

namespace App\Repository;

use App\Entity\AvancementChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AvancementChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvancementChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvancementChantier[]    findAll()
 * @method AvancementChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvancementChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvancementChantier::class);
    }
    
    // /**
    //  * @return AvancementChantier[] Returns an array of AvancementChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?AvancementChantier
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
