<?php

namespace App\Repository;

use App\Entity\Ruines;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ruines|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ruines|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ruines[]    findAll()
 * @method Ruines[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuinesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ruines::class);
    }
    
    public function existanceRuineId(string $ruineId, Ville $ville): bool
    {
        
        $retour = $this->createQueryBuilder('r')
                       ->where('r.id = :ruineId')
                       ->andWhere('r.ville = :ville')
                       ->setParameter('ruineId', $ruineId)
                       ->setParameter('ville', $ville)
                       ->getQuery()
                       ->getOneOrNullResult();
        
        return $retour !== null;
        
    }
    // /**
    //  * @return Ruines[] Returns an array of Ruines objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Ruines
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
