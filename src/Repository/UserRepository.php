<?php

namespace App\Repository;

use App\Entity\HerosPrototype;
use App\Entity\RememberMeTokens;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countUser(): int
    {
        return $this->createQueryBuilder('u')
                    ->select('count(u.id)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }
    
    /**
     * @return User[]
     */
    public function find50firstUser(): array
    {
        return $this->createQueryBuilder('u')
                    ->setMaxResults(50)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @param string $identifier
     *
     * @return UserInterface|null
     * @throws NonUniqueResultException
     */
    public function loadUserByIdentifier(string $identifier): ?User
    {
        return $this->loadUserByUsername($identifier);
    }
    
    /**
     * @return UserInterface|null
     */
    public function loadUserByUsername(string $username): ?User
    {
        $components = explode('::', $username, 2);
        [$domain, $name] = count($components) === 2 ? $components : ['tkn', $components[0]];
        
        switch ($domain) {
            //case 'gh':
            //return $this->findOneByEternalID( $name );
            case 'tkn':
                $token =
                    $this->getEntityManager()->getRepository(RememberMeTokens::class)->findOneBy(['token' => $name]);
                return $token?->getUser();
            default:
                return null;
        }
    }
    
    /**
     * @return User[]
     */
    public function searchPseudo(string $user): array
    {
        return $this->createQueryBuilder('u')
                    ->where('u.pseudo like :user')
                    ->setParameter('user', '%' . $user . '%')
                    ->getQuery()
                    ->getResult();
    }
    
    public function statistiquePowerUser(): array
    {
        return $this->createQueryBuilder('u')->join(HerosPrototype::class, 'h')->select('h.nom, count(u.id) as nbr')
                    ->where('u.apiToken is not null')
                    ->andWhere('u.derPouv = h.id')
                    ->andWhere('u.derPouv <> 5')
                    ->groupBy('h.nom')
                    ->orderBy('nbr', 'DESC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    public function statistiqueTheme(): array
    {
        
        $dateV2 = new DateTime('2022-03-26 01:30');
        
        
        return $this->createQueryBuilder('u')->select('u.theme, count(u.id) as nbr')
                    ->where('u.apiToken is not null')
            /*->andWhere('u.dateMaj >= :date')
            ->setParameter(':date', $dateV2)*/
                    ->groupBy('u.theme')
                    ->getQuery()
                    ->getResult();
        
    }
    
    public function statistiqueThemeUserActif(): array
    {
        
        $date = new DateTime('now');
        
        // calcul de la date du jour - 30 jours
        $date->modify('-30 day');
        
        
        return $this->createQueryBuilder('u')->select('u.theme, count(u.id) as nbr')
                    ->where('u.apiToken is not null')
                    ->andWhere('u.lastConnexionAt >= :date')
                    ->setParameter(':date', $date)
                    ->groupBy('u.theme')
                    ->getQuery()
                    ->getResult();
        
    }
    
}
