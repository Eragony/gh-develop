<?php

namespace App\Repository;

use App\Entity\TypeCritereExpe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeCritereExpe|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCritereExpe|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCritereExpe[]    findAll()
 * @method TypeCritereExpe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCritereExpeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCritereExpe::class);
    }
    
    // /**
    //  * @return TypeCritereExpe[] Returns an array of TypeCritereExpe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeCritereExpe
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
