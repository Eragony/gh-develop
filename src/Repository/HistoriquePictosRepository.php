<?php

namespace App\Repository;

use App\Entity\Citoyens;
use App\Entity\HistoriquePictos;
use App\Entity\PictoPrototype;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HistoriquePictos|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoriquePictos|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoriquePictos[]    findAll()
 * @method HistoriquePictos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoriquePictosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoriquePictos::class);
    }
    
    public function countPictoAll(): int
    {
        return $this->createQueryBuilder('hp')
                    ->select('COUNT(hp.id)')
                    ->getQuery()
                    ->getSingleScalarResult();
    }
    
    /**
     * @return HistoriquePictos[]
     */
    public function recupHistoPictoForSaison(User $user, int $saison): array
    {
        return $this->createQueryBuilder('hp')
                    ->join(Ville::class, 'v', 'WITH', 'hp.mapId = v.id')
                    ->where('hp.user = :user')
                    ->andWhere('v.saison = :saison')
                    ->andWhere('v.phase = :phase')
                    ->setParameter('user', $user)
                    ->setParameter('saison', $saison)
                    ->setParameter('phase', 'native')
                    ->orderBy('hp.picto', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return HistoriquePictos[]
     */
    public function recupHistoPictoForSaisonForOnePictVilleo(Ville $ville, int $saison, PictoPrototype $picto, ?int $limit = null): array
    {
        $listUser = array_map(fn(Citoyens $i) => $i->getCitoyen()->getId(), $ville->getCitoyens()->toArray());
        
        $query = $this->createQueryBuilder('hp')
                      ->select('u as user, SUM(hp.nombre) as nombre')
                      ->join(Ville::class, 'v', 'WITH', 'hp.mapId = v.id')
                      ->join(User::class, 'u', 'WITH', 'hp.user = u.id')
                      ->where('v.saison = :saison')
                      ->andWhere('v.phase = :phase')
                      ->andWhere('hp.picto = :picto')
                      ->andWhere('hp.user in (:listUser)')
                      ->setParameter('saison', $saison)
                      ->setParameter('phase', 'native')
                      ->setParameter('picto', $picto)
                      ->setParameter('listUser', $listUser)
                      ->groupBy('hp.user', 'hp.picto')
                      ->orderBy('nombre', 'DESC', 'u.pseudo', 'ASC');
        
        if ($limit !== null) {
            $query->setMaxResults($limit);
        }
        return $query->getQuery()
                     ->getResult();
    }
    
    /**
     * @return HistoriquePictos[]
     */
    public function recupHistoPictoForSaisonForOnePicto(int $saison, PictoPrototype $picto, ?int $limit = null): array
    {
        $query = $this->createQueryBuilder('hp')
                      ->select('u as user, SUM(hp.nombre) as nombre')
                      ->join(Ville::class, 'v', 'WITH', 'hp.mapId = v.id')
                      ->join(User::class, 'u', 'WITH', 'hp.user = u.id')
                      ->where('v.saison = :saison')
                      ->andWhere('v.phase = :phase')
                      ->andWhere('hp.picto = :picto')
                      ->setParameter('saison', $saison)
                      ->setParameter('phase', 'native')
                      ->setParameter('picto', $picto)
                      ->groupBy('hp.user', 'hp.picto')
                      ->orderBy('nombre', 'DESC', 'u.pseudo', 'ASC');
        if ($limit !== null) {
            $query->setMaxResults($limit);
        }
        return $query->getQuery()
                     ->getResult();
    }
    
    /**
     * @return HistoriquePictos[]|null
     */
    public function recupListHistoriqueVille(User $user): ?array
    {
        
        return $this->createQueryBuilder('hp')
                    ->where('hp.user = :user')
                    ->setParameter('user', $user)
                    ->orderBy('hp.mapId', 'ASC')
                    ->addOrderBy('hp.nombre', 'DESC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    // /**
    //  * @return HistoriquePictos[] Returns an array of HistoriquePictos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?HistoriquePictos
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
