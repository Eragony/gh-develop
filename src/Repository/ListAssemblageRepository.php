<?php

namespace App\Repository;

use App\Entity\ListAssemblage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ListAssemblage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListAssemblage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListAssemblage[]    findAll()
 * @method ListAssemblage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListAssemblageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListAssemblage::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $query = $this->createQueryBuilder('l')
                      ->select('MAX(l.id)')
                      ->getQuery();
        
        return $query->getSingleScalarResult() ?? 0;
    }
    
    // /**
    //  * @return ListAssemblage[] Returns an array of ListAssemblage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ListAssemblage
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
