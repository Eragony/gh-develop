<?php

namespace App\Repository;

use App\Entity\RessourceHome;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RessourceHome|null find($id, $lockMode = null, $lockVersion = null)
 * @method RessourceHome|null findOneBy(array $criteria, array $orderBy = null)
 * @method RessourceHome[]    findAll()
 * @method RessourceHome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessourceHomeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RessourceHome::class);
    }
    
    // /**
    //  * @return RessourceHome[] Returns an array of RessourceHome objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RessourceHome
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
