<?php

namespace App\Repository;

use App\Entity\ItemSacType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ItemSacType>
 *
 * @method ItemSacType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemSacType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemSacType[]    findAll()
 * @method ItemSacType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemSacTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemSacType::class);
    }
    
    //    /**
    //     * @return ItemSacType[] Returns an array of ItemSacType objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('i.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?ItemSacType
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
