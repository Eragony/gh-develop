<?php

namespace App\Repository;

use App\Entity\Citoyens;
use App\Entity\PictoPrototype;
use App\Entity\Pictos;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pictos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pictos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pictos[]    findAll()
 * @method Pictos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PictosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pictos::class);
    }
    
    public function getNombrePictosInArray(User $user, array $pictos): int
    {
        return $this->createQueryBuilder('p')
                    ->select('SUM(p.nombre)')
                    ->join('p.picto', 'picto')
                    ->where('p.user = :user')
                    ->andWhere('p.picto = picto.id')
                    ->andWhere('picto.uuid in (:pictos)')
                    ->setParameters(
                        new ArrayCollection([
                                                new Parameter('user', $user),
                                                new Parameter('pictos', $pictos),
                                            ]),
                    )
                    ->getQuery()
                    ->getSingleScalarResult() ?? 0;
    }
    
    /**
     * @return array|Pictos[]
     */
    public function recupClassementPicto(PictoPrototype $picto): array
    {
        return $this->createQueryBuilder('p')
                    ->where('p.picto = :picto')
                    ->setParameter('picto', $picto)
                    ->orderBy('p.nombre', 'DESC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return array|Pictos[]
     */
    public function recupClassementPictoVille(PictoPrototype $picto, Ville $ville): array
    {
        
        $listUser = array_map(fn(Citoyens $i) => $i->getCitoyen(), $ville->getCitoyens()->toArray());
        
        return $this->createQueryBuilder('p')
                    ->where('p.picto = :picto')
                    ->andWhere('p.user in (:listUser)')
                    ->setParameters(
                        new ArrayCollection([
                                                new Parameter('picto', $picto),
                                                new Parameter('listUser', $listUser),
                                            ]),
                    )
                    ->orderBy('p.nombre', 'DESC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return array|Pictos[]
     */
    public function recupClassementPictos(PictoPrototype $picto): array
    {
        return $this->createQueryBuilder('p')
                    ->where('p.picto = :picto')
                    ->setParameter('picto', $picto)
                    ->orderBy('p.nombre', 'DESC')
                    ->setMaxResults(10)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return array|Pictos[]
     */
    public function recupClassementPictosVille(PictoPrototype $picto, Ville $ville): array
    {
        
        $listUser = array_map(fn(Citoyens $i) => $i->getCitoyen()->getId(), $ville->getCitoyens()->toArray());
        
        return $this->createQueryBuilder('p')
                    ->where('p.picto = :picto')
                    ->andWhere('p.user in (:listUser)')
                    ->setParameters(
                        new ArrayCollection([
                                                new Parameter('picto', $picto),
                                                new Parameter('listUser', $listUser),
                                            ]),
                    )
                    ->orderBy('p.nombre', 'DESC')
                    ->setMaxResults(10)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return array|Pictos[]
     */
    public function recupPictosUser(User $user): array
    {
        
        return $this->createQueryBuilder('p')
                    ->join('p.picto', 'picto')
                    ->where('p.user = :user')
                    ->setParameter('user', $user)
                    ->orderBy('picto.rare', 'DESC')
                    ->addOrderBy('p.nombre', 'DESC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    // /**
    //  * @return Pictos[] Returns an array of Pictos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Pictos
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
