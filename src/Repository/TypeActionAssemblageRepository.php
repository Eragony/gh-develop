<?php

namespace App\Repository;

use App\Entity\TypeActionAssemblage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeActionAssemblage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeActionAssemblage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeActionAssemblage[]    findAll()
 * @method TypeActionAssemblage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeActionAssemblageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeActionAssemblage::class);
    }
    
    // /**
    //  * @return TypeActionAssemblage[] Returns an array of TypeActionAssemblage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeActionAssemblage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
