<?php

namespace App\Repository;

use App\Entity\HistoriqueVille;
use App\Entity\User;
use App\Entity\VilleHistorique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HistoriqueVille>
 *
 * @method HistoriqueVille|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoriqueVille|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoriqueVille[]    findAll()
 * @method HistoriqueVille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoriqueVilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoriqueVille::class);
    }
    
    public function getJourTotal(User $user): int
    {
        $qb = $this->createQueryBuilder('hv')
                   ->select('SUM(hv.dayOfDeath)')
                   ->where('hv.user = :user')
                   ->setParameter('user', $user)
                   ->getQuery();
        
        return $qb->getSingleScalarResult() ?? 0;
    }
    
    public function getNeighbours(User $user): array
    {
        // get all the cities that the user has visited and  the number of times with same other users
        $neighbours = $this->createQueryBuilder('hv')
                           ->join(VilleHistorique::class, 'vh', 'WITH', 'hv.ville_historique = vh.id')
                           ->select('hv.user, COUNT(hv.id) AS nb')
                           ->where('hv.user = :user')
                           ->setParameter('user', $user)
                           ->groupBy('hv.user')
                           ->orderBy('nb', 'DESC')
                           ->getQuery()
                           ->getResult();
        
        return $neighbours;
    }
    
    public function getRecordSurvie(User $user): int
    {
        $qb = $this->createQueryBuilder('hv')
                   ->select('MAX(hv.dayOfDeath)')
                   ->where('hv.user = :user')
                   ->setParameter('user', $user)
                   ->getQuery();
        
        return $qb->getSingleScalarResult() ?? 0;
    }
    
    public function remove(HistoriqueVille $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);
        
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
    public function save(HistoriqueVille $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);
        
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return HistoriqueVille[] Returns an array of HistoriqueVille objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HistoriqueVille
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
