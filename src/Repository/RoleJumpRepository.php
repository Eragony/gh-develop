<?php

namespace App\Repository;

use App\Entity\RoleJump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoleJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoleJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoleJump[]    findAll()
 * @method RoleJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoleJump::class);
    }
    
    // /**
    //  * @return RoleJump[] Returns an array of RoleJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RoleJump
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
