<?php

namespace App\Repository;

use App\Entity\OutilsReparation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OutilsReparation|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutilsReparation|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutilsReparation[]    findAll()
 * @method OutilsReparation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutilsReparationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutilsReparation::class);
    }
    
    // /**
    //  * @return OutilsReparation[] Returns an array of OutilsReparation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?OutilsReparation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
