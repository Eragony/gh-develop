<?php

namespace App\Repository;

use App\Entity\OutilsDecharge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OutilsDecharge|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutilsDecharge|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutilsDecharge[]    findAll()
 * @method OutilsDecharge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutilsDechargeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutilsDecharge::class);
    }
    
    // /**
    //  * @return OutilsDecharge[] Returns an array of OutilsDecharge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?OutilsDecharge
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
