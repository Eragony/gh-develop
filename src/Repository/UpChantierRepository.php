<?php

namespace App\Repository;

use App\Entity\UpChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpChantier[]    findAll()
 * @method UpChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpChantier::class);
    }
    
    // /**
    //  * @return UpChantier[] Returns an array of UpChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?UpChantier
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
