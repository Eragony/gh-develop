<?php

namespace App\Repository;

use App\Entity\TypeLead;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeLead|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeLead|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeLead[]    findAll()
 * @method TypeLead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeLeadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeLead::class);
    }
    
    /**
     * @return TypeLead[]
     */
    public function findAllOrdreAlpha(): array
    {
        return $this->createQueryBuilder('l')->orderBy('l.nom', 'ASC')->getQuery()->getResult();
    }
    
    // /**
    //  * @return TypeLead[] Returns an array of TypeLead objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeLead
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
