<?php

namespace App\Repository;

use App\Entity\TypeVille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeVille|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeVille|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeVille[]    findAll()
 * @method TypeVille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeVilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeVille::class);
    }
    
    /**
     * @return TypeVille[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('tv', 'tv.id')->getQuery()->getResult();
    }
    
    // /**
    //  * @return TypeVille[] Returns an array of TypeVille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeVille
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
