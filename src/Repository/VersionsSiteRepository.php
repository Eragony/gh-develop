<?php

namespace App\Repository;

use App\Entity\VersionsSite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VersionsSite|null find($id, $lockMode = null, $lockVersion = null)
 * @method VersionsSite|null findOneBy(array $criteria, array $orderBy = null)
 * @method VersionsSite[]    findAll()
 * @method VersionsSite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VersionsSiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VersionsSite::class);
    }
    
    /**
     * @return VersionsSite[]
     */
    public function allDesc(): array
    {
        return $this->createQueryBuilder('v')->orderBy('v.id', 'DESC')->getQuery()->getResult();
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function last(): ?VersionsSite
    {
        return $this->createQueryBuilder('v')
                    ->orderBy('v.id', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    // /**
    //  * @return VersionsSite[] Returns an array of VersionsSite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?VersionsSite
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
