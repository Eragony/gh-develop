<?php

namespace App\Repository;

use App\Entity\ItemPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemPrototype[]    findAll()
 * @method ItemPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemPrototype::class);
    }
    
    public function findAllArmesVeilles(): array
    {
        return $this->createQueryBuilder('i')
                    ->where('i.defBase is not null')
                    ->andWhere('i.defBase != 0')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function findAllDecharge(): array
    {
        
        $request = $this->createQueryBuilder('i')
                        ->select('i', 'd')
                        ->leftJoin('i.decharge', 'd')
                        ->where('i.decharge is not null')
                        ->getQuery();
        
        return $request->getResult();
        
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('i', 'i.id')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function findAllPoubelle(): array
    {
        return $this->createQueryBuilder('i')
                    ->where('i.probaPoubelle <> 0')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function findAllWeapons(): array
    {
        return $this->createQueryBuilder('i')
                    ->where('i.type is not null')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function findAllWithDecharge(): array
    {
        
        $request = $this->createQueryBuilder('i')
                        ->select('i', 'd')
                        ->leftJoin('i.decharge', 'd')
                        ->getQuery();
        
        return $request->getResult();
        
    }
    
    public function lastId(): int
    {
        $result = $this->createQueryBuilder('i')->select('i.id')
                       ->where('i.id < 2000')
                       ->orderBy('i.id', 'DESC')
                       ->setMaxResults(1)
                       ->getQuery()
                       ->getSingleScalarResult();
        
        return $result + 1;
    }
    
    // /**
    //  * @return ItemPrototype[] Returns an array of ItemPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ItemPrototype
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
