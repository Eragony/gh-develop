<?php

namespace App\Repository;

use App\Entity\RegroupementItemsDecharge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegroupementItemsDecharge|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegroupementItemsDecharge|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegroupementItemsDecharge[]    findAll()
 * @method RegroupementItemsDecharge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegroupementItemsDechargeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegroupementItemsDecharge::class);
    }
    
    /**
     * @return RegroupementItemsDecharge[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('rid', 'rid.id')->getQuery()->getResult();
    }
    
    // /**
    //  * @return RegroupementItemsDecharge[] Returns an array of RegroupementItemsDecharge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RegroupementItemsDecharge
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
