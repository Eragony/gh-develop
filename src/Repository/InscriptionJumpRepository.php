<?php

namespace App\Repository;

use App\Entity\InscriptionJump;
use App\Entity\Jump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InscriptionJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method InscriptionJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method InscriptionJump[]    findAll()
 * @method InscriptionJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscriptionJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InscriptionJump::class);
    }
    
    /**
     * @return InscriptionJump[]
     */
    public function candidatureAccepted(string $idJump): array
    {
        return $this->createQueryBuilder('ij')
                    ->where('ij.statut = 20')
                    ->andWhere('ij.Jump = :idJump')
                    ->setParameter(':idJump', $idJump)
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function countAccepted(Jump $jump): int
    {
        $retour = $this->createQueryBuilder('ij')
                       ->select('count(ij.id)')
                       ->where('ij.Jump = :idJump')
                       ->andWhere('ij.statut = 20')
                       ->setParameter('idJump', $jump)
                       ->getQuery()
                       ->getOneOrNullResult();
        
        return (int)$retour ?? 0;
    }
    
    public function leadInscription(string $idJump, int $typeLead, bool $apprenti): array
    {
        return $this->createQueryBuilder('ij')
                    ->join('ij.leadInscriptions', 'li', Join::WITH, 'ij.id = li.inscriptionJump')
                    ->where('ij.Jump = :idJump')
                    ->andWhere('li.lead = :typeLead')
                    ->andWhere('li.apprenti = :apprenti')
                    ->setParameter('idJump', $idJump)
                    ->setParameter('typeLead', $typeLead)
                    ->setParameter('apprenti', $apprenti)
                    ->getQuery()->getResult();
    }
    
    // /**
    //  * @return InscriptionJump[] Returns an array of InscriptionJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?InscriptionJump
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
