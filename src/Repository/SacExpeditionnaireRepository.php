<?php

namespace App\Repository;

use App\Entity\SacExpeditionnaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SacExpeditionnaire>
 *
 * @method SacExpeditionnaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method SacExpeditionnaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method SacExpeditionnaire[]    findAll()
 * @method SacExpeditionnaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SacExpeditionnaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SacExpeditionnaire::class);
    }
    
    //    /**
    //     * @return SacExpeditionnaire[] Returns an array of SacExpeditionnaire objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?SacExpeditionnaire
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
