<?php

namespace App\Repository;

use App\Entity\DispoUserType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DispoUserType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DispoUserType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DispoUserType[]    findAll()
 * @method DispoUserType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispoUserTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DispoUserType::class);
    }
    
    // /**
    //  * @return DispoUserType[] Returns an array of DispoUserType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?DispoUserType
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
