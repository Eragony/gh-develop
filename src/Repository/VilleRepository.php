<?php

namespace App\Repository;

use App\Entity\Ville;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ville|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ville|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ville[]    findAll()
 * @method Ville[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ville::class);
    }
    
    public function countPande(?int $saison, ?string $phase): int
    {
        $query = $this->createQueryBuilder('v')
                      ->select('COUNT(v.id)')
                      ->where('v.hard = 1');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        return (int)$query->getQuery()->getSingleScalarResult();
    }
    
    public function countPandeByDay(?int $saison, ?string $phase): array
    {
        $query = $this->createQueryBuilder('v')
                      ->select('v.jour, COUNT(v.id) as nb')
                      ->where('v.hard = 1');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        $query->groupBy('v.jour');
        
        return $query->getQuery()->getResult();
    }
    
    public function countRE(?int $saison, ?string $phase): int
    {
        $query = $this->createQueryBuilder('v')
                      ->select('COUNT(v.id)')
                      ->where('v.weight > 15')
                      ->andWhere('v.hard = 0');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        return (int)$query->getQuery()->getSingleScalarResult();
    }
    
    public function countREByDay(?int $saison, ?string $phase): array
    {
        $query = $this->createQueryBuilder('v')
                      ->select('v.jour, COUNT(v.id) as nb')
                      ->where('v.weight > 15')
                      ->andWhere('v.hard = 0');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        $query->groupBy('v.jour');
        
        return $query->getQuery()->getResult();
    }
    
    public function countRNE(?int $saison, ?string $phase): int
    {
        $query = $this->createQueryBuilder('v')
                      ->select('COUNT(v.id)')
                      ->where('v.weight <= 15');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        return (int)$query->getQuery()->getSingleScalarResult();
    }
    
    public function countRNEByDay(?int $saison, ?string $phase): array
    {
        $query = $this->createQueryBuilder('v')
                      ->select('v.jour, COUNT(v.id) as nb')
                      ->where('v.weight <= 15');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        $query->groupBy('v.jour');
        
        return $query->getQuery()->getResult();
    }
    
    /**
     * @return Ville[]
     */
    public function getAllVilles(): array
    {
        return $this->createQueryBuilder('v')
                    ->orderBy('v.jour', 'ASC')
                    ->addOrderBy("SUBSTRING(v.dateTime, 1, 10)", 'DESC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return Ville[]
     */
    public function getListVilles30J(): array
    {
        
        $dateTime         = new DateTime('now');
        $dateTimeInterval = new DateInterval('P30D');
        
        return $this->createQueryBuilder('v')
                    ->where('v.dateTime > :date')
                    ->setParameter('date', $dateTime->sub($dateTimeInterval))
                    ->orderBy('v.jour', 'ASC')
                    ->addOrderBy("SUBSTRING(v.dateTime, 1, 10)", 'DESC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return Ville[]
     */
    public function getTop15(): array
    {
        
        return $this->createQueryBuilder('v')
                    ->orderBy("SUBSTRING(v.dateTime, 1, 10)", 'DESC')
                    ->addOrderBy('v.jour', 'DESC')
                    ->setMaxResults(30)
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return Ville[]
     */
    public function getVillesSearch(array $arrCondition): array
    {
        $query = $this->createQueryBuilder('v');
        
        if (!is_null($arrCondition['nom'])) {
            $query->andwhere('v.nom like :nomVille')
                  ->setParameter('nomVille', '%' . $arrCondition['nom'] . '%');
        }
        if (!empty($arrCondition['type'])) {
            $whereCondition = [];
            foreach ($arrCondition['type'] as $key => $value) {
                switch ($value) {
                    case "RNE":
                        $whereCondition[] = 'v.height < 20';
                        break;
                    case "RE":
                        $whereCondition[] = 'v.height > 20 AND v.hard = 0';
                        break;
                    case "Pandé":
                        $whereCondition[] = 'v.hard = 1';
                        break;
                }
            }
            $query->andWhere(implode(' OR ', $whereCondition));
        }
        if (!empty($arrCondition['etat'])) {
            $whereCondition = [];
            foreach ($arrCondition['etat'] as $key => $value) {
                switch ($value) {
                    case "Normal":
                        $whereCondition[] = 'v.chaos = 0 AND v.devast = 0';
                        break;
                    case "Chaos":
                        $whereCondition[] = 'v.chaos = 1 AND v.devast = 0';
                        break;
                    case "Dévasté":
                        $whereCondition[] = 'v.chaos = 1 AND v.devast = 1';
                        break;
                }
            }
            $query->andWhere(implode(' OR ', $whereCondition));
        }
        if (!empty($arrCondition['saison'])) {
            
            $saison = $arrCondition['saison'];
            if ($saison !== -1) {
                $whereCondition = [];
                if ($saison === 151) {
                    $whereCondition = 'v.phase = :phase AND v.saison = 15';
                    $query->andWhere($whereCondition)
                          ->setParameter('phase', "beta");
                } else {
                    $saisonReel     = $saison / 10;
                    $whereCondition = '(v.phase is Null OR v.phase = :phase) AND v.saison = ' . $saisonReel;
                    $query->andWhere($whereCondition)
                          ->setParameter('phase', "native");
                }
                
            }
            
        }
        if (!empty($arrCondition['lang'])) {
            
            $lang = $arrCondition['lang'];
            $query->andWhere("v.lang IN (:lang)")
                  ->setParameter('lang', $lang);
            
            
        }
        
        
        return $query->orderBy('v.dateTime', 'DESC')
                     ->getQuery()
                     ->getResult();
        
    }
    
    // /**
    //  * @return Ville[] Returns an array of Ville objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Ville
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
