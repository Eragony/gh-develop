<?php

namespace App\Repository;

use App\Entity\Veilleur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Veilleur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Veilleur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Veilleur[]    findAll()
 * @method Veilleur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VeilleurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Veilleur::class);
    }
    
    // /**
    //  * @return Veilleur[] Returns an array of Veilleur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Veilleur
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
