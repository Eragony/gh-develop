<?php

namespace App\Repository;

use App\Entity\BatPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BatPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method BatPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method BatPrototype[]    findAll()
 * @method BatPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BatPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BatPrototype::class);
    }
    
    /**
     * @return array|BatPrototype[]
     */
    public function findAllExceptRuine(): array
    {
        return $this->createQueryBuilder('b')
                    ->where('b.explorable = 0')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return BatPrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('b', 'b.idMh')->getQuery()->getResult();
    }
    
    // /**
    //  * @return BatPrototype[] Returns an array of BatPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?BatPrototype
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
