<?php

namespace App\Repository;

use App\Entity\CoffreCitoyen;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoffreCitoyen|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoffreCitoyen|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoffreCitoyen[]    findAll()
 * @method CoffreCitoyen[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoffreCitoyenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoffreCitoyen::class);
    }
    
    // /**
    //  * @return CoffreCitoyen[] Returns an array of CoffreCitoyen objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?CoffreCitoyen
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
