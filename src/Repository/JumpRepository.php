<?php

namespace App\Repository;

use App\Entity\Jump;
use App\Entity\StatutInscription;
use App\Entity\User;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Jump|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jump|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jump[]    findAll()
 * @method Jump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jump::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function habiliterGestion(Jump $jump, int $idUser): bool
    {
        
        if ($jump->getEvent() !== null) {
            
            $retour = $this->isOrgaEvent($jump, $idUser);
            
            // Si l'utilisateur n'est pas l'organisateur de l'event, il est peut-être mis comme gestionnaire du jump dans l'event
            if (!$retour) {
                return $this->isGestionnaire($jump, $idUser);
            } else {
                return true;
            }
            
        }
        
        return $this->isGestionnaire($jump, $idUser);
        
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function isGestionnaire(Jump $jump, int $idUser): bool
    {
        $retour = $this->createQueryBuilder('j')
                       ->innerJoin('j.gestionnaires', 'g')
                       ->where('j.id = :idJump')
                       ->andWhere('g.id = :idUser')
                       ->setParameter('idJump', $jump->getId())
                       ->setParameter('idUser', $idUser)
                       ->getQuery()
                       ->getOneOrNullResult();
        
        
        if ($retour === null) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * @throws Exception
     * @throws NonUniqueResultException
     */
    public function isGestionnaireNonInscrit(Jump $jump, int $idUser): bool
    {
        
        if ($this->isGestionnaire($jump, $idUser)) {
            
            $idJump = $jump->getId();
            
            $connexion = $this->getEntityManager()->getConnection();
            
            $sql = 'Select count(*) from inscription_jump ij where ij.jump_id = :idJump and ij.user_id = :idUser';
            
            $stmt = $connexion->prepare($sql);
            
            $stmt->bindValue('idJump', $idJump);
            $stmt->bindValue('idUser', $idUser);
            
            $resultSet = $stmt->executeQuery();
            
            $retour = $resultSet->fetchOne();
            
            return $retour === 0;
            
        } else {
            return false;
        }
    }
    
    /**
     * @throws Exception
     */
    public function isOrgaEvent(Jump $jump, int $idUser): bool
    {
        $idEvent = $jump->getEvent()->getId();
        
        $connexion = $this->getEntityManager()->getConnection();
        
        $sql =
            'Select count(*) from event e, event_user eu where e.id = :idEvent and e.id = eu.event_id and eu.user_id = :idUser';
        
        $stmt = $connexion->prepare($sql);
        $stmt->bindValue('idEvent', $idEvent);
        $stmt->bindValue('idUser', $idUser);
        
        $resultSet = $stmt->executeQuery();
        
        $retour = $resultSet->fetchOne();
        
        return !empty($retour);
    }
    
    /**
     * @return Jump[]
     */
    public function jumpActif(DateTime $dateTime): array
    {
        return $this->createQueryBuilder('j')->where('j.dateDebInscription <= :date')
                    ->andWhere('j.dateFinInscription >= :date')
                    ->andWhere('j.event is null')
                    ->setParameter('date', $dateTime)
                    ->orderBy('j.dateApproxJump', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return Jump[]
     */
    public function jumpInscriptionUserArchive(User $user, DateTime $dateTime): array
    {
        return $this->createQueryBuilder('j')
                    ->join('j.inscriptionJumps', 'i')
                    ->where('j.dateDebInscription < :date')
                    ->andWhere('j.dateFinInscription < :date')
                    ->andWhere('j.event is null')
                    ->andWhere('i.user = :user')
                    ->setParameter('date', $dateTime)
                    ->setParameter('user', $user)
                    ->orderBy('j.dateApproxJump', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    public function listJumpCoalition(User $user): array
    {
        
        
        $dateJourM4J = new DateTime('Now');
        $dateJourM4J->sub(new DateInterval('P4D'));
        
        $dateJourM2M = new DateTime('Now');
        $dateJourM2M->sub(new DateInterval('P2M'));
        
        return $this->createQueryBuilder('j')
                    ->innerJoin('j.inscriptionJumps', 'i')
                    ->where('i.user = :user')
                    ->andWhere('i.statut = :statut')
                    ->andWhere('(j.dateApproxJump >= :dateJour AND j.dateJumpEffectif is null) OR j.dateJumpEffectif >= :dateJourM2M')
                    ->setParameter('user', $user)
                    ->setParameter('statut', StatutInscription::ST_ACC)
                    ->setParameter('dateJour', $dateJourM4J)
                    ->setParameter('dateJourM2M', $dateJourM2M)
                    ->addOrderBy('j.dateApproxJump', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return Jump[] Returns an array of Jump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Jump
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
