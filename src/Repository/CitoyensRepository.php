<?php

namespace App\Repository;

use App\Entity\Citoyens;
use App\Entity\JobPrototype;
use App\Entity\TypeDeath;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Citoyens|null find($id, $lockMode = null, $lockVersion = null)
 * @method Citoyens|null findOneBy(array $criteria, array $orderBy = null)
 * @method Citoyens[]    findAll()
 * @method Citoyens[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitoyensRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Citoyens::class);
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function citoyenExist(Ville $ville, int $idCitoyen): ?Citoyens
    {
        
        return $this->createQueryBuilder('c')
                    ->where('c.ville = :ville')
                    ->andWhere('c.citoyen = :idCitoyen')
                    ->setParameter(':ville', $ville)
                    ->setParameter('idCitoyen', $idCitoyen)
                    ->getQuery()
                    ->getOneOrNullResult();
        
    }
    
    /**
     * @return Collection<Citoyens>
     */
    public function citoyenVille(Ville $ville): Collection
    {
        
        $array = $this->createQueryBuilder('c')
                      ->where('c.ville = :ville')
                      ->setParameter('ville', $ville)
                      ->getQuery()
                      ->getResult();
        
        return new ArrayCollection($array);
        
    }
    
    public function derniereVille(User $user): ?Citoyens
    {
        
        if ($user->getMapId() != null) {
            
            return $this->createQueryBuilder('c')
                        ->where('c.citoyen = :user')
                        ->andWhere('c.ville <> :ville')
                        ->setParameter('user', $user)
                        ->setParameter('ville', $user->getMapId() . Ville::ORIGIN_MH)
                        ->orderBy('c.ville', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
        } else {
            return $this->createQueryBuilder('c')
                        ->where('c.citoyen = :user')
                        ->setParameter('user', $user)
                        ->orderBy('c.ville', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
        }
        
        
    }
    
    /**
     * @return Citoyens[]
     */
    public function listVille(User $user): array
    {
        
        return $this->createQueryBuilder('c', 'c.ville')
                    ->where('c.citoyen = :user')
                    ->setParameter('user', $user)
                    ->andWhere('c.ville <> :villeActuelle')
                    ->setParameter('villeActuelle', $user->getMapId() * 10 + Ville::ORIGIN_MH)
                    ->orderBy('c.ville', 'DESC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    // Fonction pour récupérer la répartition des métiers par ville
    
    public function statistiqueJobs(?int $saison, ?string $phase): array
    {
        $query = $this->createQueryBuilder('c')
                      ->join(JobPrototype::class, 'j')
                      ->join(Ville::class, 'v')
                      ->select('j.nom, COUNT(c.id) AS nbr, SUM(CASE
                              WHEN c.mort = true THEN c.deathDay
                              ELSE v.jour
                          END) as total')
                      ->where('c.job = j.id')
                      ->andWhere('v.id = c.ville');
        
        if ($saison !== null && $phase !== null) {
            $query->andWhere('v.saison = :saison')
                  ->andWhere('v.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        return $query->groupBy('j.nom')
                     ->orderBy('nbr', 'DESC')
                     ->getQuery()
                     ->getResult();
        
    }
    
    public function statistiqueMort(?int $saison, ?string $phase): array
    {
        
        $qb = $this->createQueryBuilder('c')
                   ->join(Ville::class, 'v')
                   ->select('IDENTITY(c.ville) as ville, c.deathDay as Jour, COUNT(c.id) as NombreMorts')
                   ->where('c.mort = 1')
                   ->andWhere('v.id = c.ville');
        
        if ($saison !== null && $phase !== null) {
            $qb->andWhere('v.saison = :saison')
               ->andWhere('v.phase = :phase')
               ->setParameter('saison', $saison)
               ->setParameter('phase', $phase);
        }
        
        return $qb->groupBy('c.ville, c.deathDay')
                  ->orderBy('ville', 'ASC')
                  ->addOrderBy('c.deathDay', 'ASC')
                  ->getQuery()->getResult();
    }
    
    public function statistiqueTypeMort(?int $saison, ?string $phase): array
    {
        
        $qb = $this->createQueryBuilder('c')
                   ->join(TypeDeath::class, 'tm')
                   ->join(Ville::class, 'v')
                   ->select('tm.label, COUNT(c.id) as nbr')
                   ->where('c.mort = 1')
                   ->andWhere('c.deathType = tm.idMort')
                   ->andWhere('v.id = c.ville');
        
        if ($saison !== null && $phase !== null) {
            $qb->andWhere('v.saison = :saison')
               ->andWhere('v.phase = :phase')
               ->setParameter('saison', $saison)
               ->setParameter('phase', $phase);
        }
        
        return $qb->groupBy('tm.idMort')
                  ->orderBy('nbr', 'DESC')
                  ->getQuery()
                  ->getResult();
    }
    
}
