<?php

namespace App\Repository;

use App\Entity\UpAmelio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpAmelio|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpAmelio|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpAmelio[]    findAll()
 * @method UpAmelio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpAmelioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpAmelio::class);
    }
    
    // /**
    //  * @return UpAmelio[] Returns an array of UpAmelio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?UpAmelio
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
