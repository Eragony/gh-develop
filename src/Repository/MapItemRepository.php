<?php

namespace App\Repository;

use App\Entity\MapItem;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MapItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method MapItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method MapItem[]    findAll()
 * @method MapItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MapItem::class);
    }
    
    public function deleteZone(int $zone)
    {
        $requete = $this->createQueryBuilder('i')
                        ->delete(MapItem::class, 'i')
                        ->where('i.zone = :zoneId')
                        ->setParameter('zoneId', $zone)
                        ->getQuery();
        
        $requete->execute();
    }
    
    /**
     * @return array|MapItem[]
     */
    public function recupMapItemAllVille(Ville $ville): array
    {
        $zoneInf = $ville->getId() * 10000;
        $zoneSup = ($ville->getId() + 1) * 10000;
        
        return $this->createQueryBuilder('mi')
                    ->where('mi.zone>= :zoneInf')
                    ->andWhere('mi.zone < :zoneSup')
                    ->setParameters(['zoneInf' => $zoneInf, 'zoneSup' => $zoneSup])
                    ->orderBy('mi.zone', 'ASC')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function sumItemZone(Ville $ville): array
    {
        $zoneInf = $ville->getId() * 10000;
        $zoneSup = ($ville->getId() + 1) * 10000;
        $mapId   = $ville->getId();
        
        $rawSQL = "SELECT SUM(i.nombre) as nbr_item, it.id, it.nom, it.icon,it.category_objet_id, COALESCE(b2.nbrItem, 0) as nbr_item_bank
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                        LEFT JOIN (SELECT sum(b.nombre) as nbrItem, b.item_id from banque b where b.ville_id = :ville group by b.item_id) as b2 on i.item_id = b2.item_id
                    WHERE i.ville_id >= :zoneInf
                        AND i.ville_id < :zoneSup
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id ASC, nbrItem DESC, i.item_id ASC";
        
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSQL);
        $stmt->bindValue(':zoneInf', $zoneInf);
        $stmt->bindValue(':zoneSup', $zoneSup);
        $stmt->bindValue(':ville', $mapId);
        
        $result = $stmt->executeQuery();
        
        return $result->fetchAllAssociative();
        
    }
    
    /**
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function sumItemZoneBroked(Ville $ville): array
    {
        
        $zoneInf = $ville->getId() * 10000;
        $zoneSup = ($ville->getId() + 1) * 10000;
        $mapId   = $ville->getId();
        
        
        $rawSQL = "SELECT SUM(i.nombre) as nbr_item, it.id, it.nom, it.icon,it.category_objet_id, COALESCE(b2.nbrItem, 0) as nbr_item_bank
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                        LEFT JOIN (SELECT sum(b.nombre) as nbrItem, b.item_id from banque b where b.ville_id = :ville and b.broked = 1 group by b.item_id) as b2 on i.item_id = b2.item_id
                    WHERE i.ville_id >= :zoneInf
                        AND i.ville_id < :zoneSup
                        AND i.broked = 1
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id ASC, nbrItem DESC, i.item_id ASC";
        
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSQL);
        $stmt->bindValue(':zoneInf', $zoneInf);
        $stmt->bindValue(':zoneSup', $zoneSup);
        $stmt->bindValue(':ville', $mapId);
        
        $result = $stmt->executeQuery();
        
        return $result->fetchAllAssociative();
        
    }
    
    // /**
    //  * @return MapItem[] Returns an array of MapItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?MapItem
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
