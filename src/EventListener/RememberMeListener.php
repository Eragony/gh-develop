<?php


namespace App\EventListener;


use App\Entity\RememberMeTokens;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class RememberMeListener implements EventSubscriberInterface
{
    public function __construct(protected TokenStorageInterface  $tokenStorage,
                                protected EntityManagerInterface $entityManager)
    {
    }
    
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
    
    public function onKernelRequest(RequestEvent $event)
    {
        $request          = $event->getRequest();
        $rememberMeCookie = $request->cookies->get('gh_remember_me');
        
        if (!$rememberMeCookie) {
            return;
        }
        
        // get user from database
        $user = $this->entityManager->getRepository(RememberMeTokens::class)->findOneBy(['token' => $rememberMeCookie])
                                    ->getUser();
        
        // create token and set it to token storage
        $token = new UsernamePasswordToken($user, 'main', $user->getRoles());
        $this->tokenStorage->setToken($token);
    }
}