<?php

namespace App\Structures\Conf;

class GestHordesConf extends Conf
{
    public const CONF_MIGRATION_DATA       = 'migration_data_ini';
    public const CONF_MIGRATION_USER       = 'old.migration_user';
    public const CONF_MIGRATION_DATA_JUMP  = 'migration_data_jump';
    public const CONF_SESSION_DATA         = 'session_data';
    public const CONF_OLD_MIGRATION_VILLE  = 'old.migration_old';
    public const CONF_MIGRATION_OLD_PLAN   = 'old.migration_old_data_plans';
    public const CONF_MIGRATION_OLD_ESTIM  = 'old.migration_old_data_estim_tdg';
    public const CONF_MIGRATION_OLD_RUINE  = 'old.migration_old_data_ruine';
    public const CONF_MIGRATION_OLD_JUMP   = 'old.migration_old_data_jump';
    public const CONF_MIGRATION_OLD_OUTILS = 'old.migration_old_data_outils';
    public const CONF_MIGRATION_OLD_EXPE   = 'old.migration_old_data_expe';
    
    public const CONF_FONCTION_EVENT = 'fonctionnalite_event';
    
    public const CONF_LANG = 'langs';
    
    public const       CONF_BACKUP_PATH        = 'backup.path';
    public const       CONF_BACKUP_COMPRESSION = 'backup.compression';
    public const       CONF_BACKUP_LIMITS_INC  = 'backup.limits.';
    const              CONF_LANGS              = 'langs';
    
}