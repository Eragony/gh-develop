<?php


namespace App\Structures\Collection;


use App\Entity\MapItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class ItemsZones
{
    
    /**
     * @var Collection|MapItem[]
     */
    private array|Collection $mapItems;
    
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }
    
    /**
     * @return array|Collection
     */
    public function getMapItems(): Collection|array
    {
        return $this->mapItems;
    }
    
    /**
     * @param array|Collection $mapItems
     */
    public function setMapItems(Collection|array $mapItems): void
    {
        $this->mapItems = $mapItems;
    }
    
    /**
     * @return MapItem[]|Collection
     * @throws Exception
     */
    public function triByNom(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->mapItems->getIterator();
        
        $iterrable->uasort(
            function (MapItem $a, MapItem $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                
                $nomA = str_replace('"', '', $this->translator->trans($a->getItem()->getNom(), [], 'items'));
                $nomB = str_replace('"', '', $this->translator->trans($b->getItem()->getNom(), [], 'items'));
                
                if ($nomA === $nomB) {
                    $retour += 0;
                } else {
                    $retour += $nomA < $nomB ? -1 : 1;
                }
                
                
                return $retour;
                
            },
        );
        
        $tmpItem = [];
        foreach (iterator_to_array($iterrable) as $item) {
            $tmpItem[] = $item;
        }
        
        return new ArrayCollection($tmpItem);
    }
    
}