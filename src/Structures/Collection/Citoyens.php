<?php


namespace App\Structures\Collection;


use App\Entity\Citoyens as Citoyen;
use App\Entity\HerosPrototype;
use App\Entity\HomePrototype;
use App\Entity\JobPrototype;
use App\Service\GestHordesHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class Citoyens
{
    /**
     * @var Citoyen[]|Collection
     */
    private array|Collection $citoyens;
    
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }
    
    /**
     * @return Citoyen[]
     */
    public function citoyensDehorsEnVille(bool $enVille = false): array
    {
        if ($enVille) {
            return $this->citoyens->filter(
                fn(Citoyen $citoyens) => !$citoyens->getMort() && (!$citoyens->getDehors() ||
                                                                   ($citoyens->getVille()->getPosX() ===
                                                                    $citoyens->getX() &&
                                                                    $citoyens->getVille()->getPosY() ===
                                                                    $citoyens->getY())),
            )->toArray();
        } else {
            return $this->citoyens->filter(
                fn(Citoyen $citoyens) => !$citoyens->getMort() && $citoyens->getDehors() &&
                                         ($citoyens->getVille()->getPosX() !== $citoyens->getX() ||
                                          $citoyens->getVille()->getPosY() !== $citoyens->getY()),
            )->toArray();
            
        }
    }
    
    public function compteurs(EntityManagerInterface $em, GestHordesHandler $gh): array
    {
        
        $array = [];
        
        $listMetier = $em->getRepository(JobPrototype::class)->findAll();
        
        foreach ($listMetier as $job) {
            $array['job'][$gh->svgImg('h_' . $job->getIcon()) . ' ' .
                          $this->translator->trans($job->getNom(), [], 'game')] =
                $this->getCitoyens()->filter(fn(Citoyen $i) => $i->getJob()->getId() == $job->getId())->count();
        }
        
        $listPouvHeros = $em->getRepository(HerosPrototype::class)->findPouvActif();
        
        foreach ($listPouvHeros as $pouvHero) {
            $array['pouv'][$pouvHero->getNom()] = $this->getCitoyens()->filter(function (Citoyen $i) use ($pouvHero) {
                $method = 'get' . ucfirst((string)$pouvHero->getMethod());
                
                return $i->$method();
            })->count();
        }
        
        $listHabitation = $em->getRepository(HomePrototype::class)->findAll();
        
        foreach ($listHabitation as $habitation) {
            $array['hab'][$habitation->getNom()] =
                $this->getCitoyens()->filter(fn(Citoyen $i) => $i->getLvlMaison()->getId() == $habitation->getId())
                     ->count();
        }
        
        return $array;
        
    }
    
    /**
     * @return Citoyen[]|Collection
     */
    public function getCitoyens(): Collection|array
    {
        return $this->citoyens;
    }
    
    /**
     * @param Citoyen[]|Collection $citoyens
     * @return Citoyens
     */
    public function setCitoyens(Collection|array $citoyens): Citoyens
    {
        $this->citoyens = $citoyens;
        
        return $this;
    }
    
    /**
     * @throws Exception
     */
    public function triByDeathDay(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->citoyens->getIterator();
        
        $iterrable->uasort(
            fn(Citoyen $a, Citoyen $b) => $a->getDeathDay() > $b->getDeathDay() ? -1 : 1,
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
        
    }
    
    /**
     * @throws Exception
     */
    public function triByPseudo(): self
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->citoyens->getIterator();
        
        $iterrable->uasort(
            fn(Citoyen $a, Citoyen $b) => ucfirst($a->getCitoyen()->getPseudo()) <
                                          ucfirst($b->getCitoyen()->getPseudo()) ? -1 : 1,
        );
        
        $this->citoyens = new ArrayCollection(iterator_to_array($iterrable));
        
        return $this;
        
    }
}