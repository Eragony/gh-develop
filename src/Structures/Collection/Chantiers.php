<?php


namespace App\Structures\Collection;


use App\Entity\ChantierPrototype;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class Chantiers
{
    
    /**
     * @var Collection|ChantierPrototype[]
     */
    private array|Collection $chantiers;
    
    /**
     * @return ChantierPrototype[]|Collection
     */
    public function getChantiers(): array|Collection
    {
        return $this->chantiers;
    }
    
    /**
     * @param ChantierPrototype[] $chantiers
     * @return Chantiers
     */
    public function setChantiers(array|Collection $chantiers): Chantiers
    {
        $this->chantiers = $chantiers;
        
        return $this;
    }
    
    /**
     * @return ChantierPrototype[]|Collection
     * @throws Exception
     */
    public function triByOrder(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->chantiers->getIterator();
        
        $iterrable->uasort(
            function (ChantierPrototype $a, ChantierPrototype $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if ($a->getOrderByListing() === $b->getOrderByListing()) {
                    $retour += 0;
                } else {
                    $retour += $a->getOrderByListing() < $b->getOrderByListing() ? -5 : 5;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
    
}