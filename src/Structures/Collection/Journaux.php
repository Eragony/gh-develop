<?php

namespace App\Structures\Collection;

use App\Entity\Banque;
use App\Entity\Journal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class Journaux
{
    
    /**
     * @var Collection|Journal[]
     */
    private array|Collection $journaux;
    
    /**
     * @return Journal[]|Collection
     */
    public function getJournaux(): array|Collection
    {
        return $this->journaux;
    }
    
    /**
     * @param Journal[]|Collection $journaux
     * @return self
     */
    public function setJournaux(array|Collection $journaux): self
    {
        $this->journaux = $journaux;
        
        return $this;
    }
    
    
    /**
     * @return Banque[]|Collection
     * @throws Exception
     */
    public function triByDay(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->journaux->getIterator();
        
        $iterrable->uasort(
            fn(Journal $a, Journal $b) => $a->getDay() > $b->getDay() ? -1 : 1,
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
    
}