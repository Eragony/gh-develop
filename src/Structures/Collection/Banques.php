<?php

namespace App\Structures\Collection;

use App\Entity\Banque;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class Banques
{
    /**
     * @var Collection|Banque[]
     */
    private array|Collection $banques;
    
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }
    
    /**
     * @return Banque[]|Collection
     */
    public function getBanques(): array|Collection
    {
        return $this->banques;
    }
    
    /**
     * @param Banque[] $banques
     * @return Banques
     */
    public function setBanques(array|Collection $banques): Banques
    {
        $this->banques = $banques;
        
        return $this;
    }
    
    /**
     * @return Banque[]|Collection
     * @throws Exception
     */
    public function triByCategoryNombreNom(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->banques->getIterator();
        
        $iterrable->uasort(
            function (Banque $a, Banque $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if ($a->getItem()->getCategoryObjet()->getId() === $b->getItem()->getCategoryObjet()->getId()) {
                    $retour += 0;
                } else {
                    $retour += $a->getItem()->getCategoryObjet()->getId() < $b->getItem()->getCategoryObjet()->getId() ?
                        -5 : 5;
                }
                if ($a->getItem()->getNom() === $b->getItem()->getNom()) {
                    $retour += 0;
                } else {
                    $retour += $this->translator->trans($a->getItem()->getNom(), [], 'items') <
                               $this->translator->trans($b->getItem()->getNom(), [], 'items') ? -1 : 1;
                }
                if ($a->getNombre() === $b->getNombre()) {
                    $retour += 0;
                } else {
                    $retour += $a->getNombre() > $b->getNombre() ? -2 : 2;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
}