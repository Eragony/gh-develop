<?php

namespace App\Structures\Dto\Api\Autre\Ville;

use App\Structures\Dto\Api\MH\ApiMaj;

class MapItemApi extends ApiMaj
{
    public function getId(): ?int
    {
        return $this->getField("idObjet");
    }
    
    public function getNombre(): ?int
    {
        return $this->getField('nombre');
    }
    
    public function getType(): ?int
    {
        return $this->getField('type');
    }
}