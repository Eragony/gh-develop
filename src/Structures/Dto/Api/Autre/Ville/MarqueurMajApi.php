<?php

namespace App\Structures\Dto\Api\Autre\Ville;

use App\Structures\Dto\Api\MH\ApiMaj;

class MarqueurMajApi extends ApiMaj
{
    
    public function getKey(): ?string
    {
        return $this->getField("userKey");
    }
    
    public function getKill(): ?int
    {
        return $this->getField('nbrKill');
    }
    
    public function getMapId(): ?int
    {
        return $this->getField('idMap');
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
}

