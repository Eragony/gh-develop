<?php

namespace App\Structures\Dto\Api\Autre\Hotel\Citoyens;


use App\Structures\Dto\Api\MH\ApiMaj;

class PouvoirMaj extends ApiMaj
{
    public function getApag(): ?int
    {
        return (is_int($this->getField('apag'))) ? $this->getField('apag') : null;
        
    }
    
    public function getCorpsSain(): ?bool
    {
        return (is_bool($this->getField('corpsSain'))) ? $this->getField('corpsSain') : null;
        
    }
    
    public function getDonJH(): ?bool
    {
        return (is_bool($this->getField('donJH'))) ? $this->getField('donJH') : null;
        
    }
    
    public function getPef(): ?bool
    {
        return (is_bool($this->getField('pef'))) ? $this->getField('pef') : null;
        
    }
    
    public function getRdh(): ?bool
    {
        return (is_bool($this->getField('rdh'))) ? $this->getField('rdh') : null;
    }
    
    public function getSauvetage(): ?bool
    {
        return (is_bool($this->getField('sauvetage'))) ? $this->getField('sauvetage') : null;
    }
    
    public function getSecondSouffle(): ?bool
    {
        return (is_bool($this->getField('secondSouffle'))) ? $this->getField('secondSouffle') : null;
        
    }
    
    public function getTrouvaille(): ?bool
    {
        return (is_bool($this->getField('trouvaille'))) ? $this->getField('trouvaille') : null;
        
    }
    
    public function getUs(): ?bool
    {
        return (is_bool($this->getField('us'))) ? $this->getField('us') : null;
    }
    
    public function getVlm(): ?bool
    {
        return (is_bool($this->getField('vlm'))) ? $this->getField('vlm') : null;
        
    }
    
}