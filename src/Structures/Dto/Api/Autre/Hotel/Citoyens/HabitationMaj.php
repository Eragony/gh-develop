<?php


namespace App\Structures\Dto\Api\Autre\Hotel\Citoyens;


use App\Structures\Dto\Api\MH\ApiMaj;

class HabitationMaj extends ApiMaj
{
    public function getCloture(): ?bool
    {
        return (is_bool($this->getField('cloture'))) ? $this->getField('cloture') : null;
    }
    
    public function getCs(): ?int
    {
        return ($this->getField('cs') <= 3 && $this->getField('cs') >= 0) ? $this->getField('cs') : null;
    }
    
    public function getCuisine(): ?int
    {
        return ($this->getField('cuisine') <= 4 && $this->getField('cuisine') >= 0) ? $this->getField('cuisine') : null;
    }
    
    public function getLabo(): ?int
    {
        return ($this->getField('labo') <= 4 && $this->getField('labo') >= 0) ? $this->getField('labo') : null;
    }
    
    public function getRangement(): ?int
    {
        return ($this->getField('rangement') <= 13 && $this->getField('rangement') >= 0) ?
            $this->getField('rangement') : null;
    }
    
    public function getRenfort(): ?int
    {
        return ($this->getField('renfort') <= 10 && $this->getField('renfort') >= 0) ? $this->getField('renfort') :
            null;
    }
    
}