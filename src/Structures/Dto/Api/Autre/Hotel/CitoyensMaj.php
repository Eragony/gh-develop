<?php


namespace App\Structures\Dto\Api\Autre\Hotel;


use App\Structures\Dto\Api\Autre\Hotel\Citoyens\HabitationMaj;
use App\Structures\Dto\Api\Autre\Hotel\Citoyens\PouvoirMaj;
use App\Structures\Dto\Api\MH\ApiMaj;

class CitoyensMaj extends ApiMaj
{
    
    public function getHabitation(): ?HabitationMaj
    {
        
        $maisonMaj = $this->getField('maison');
        if ($maisonMaj == null) {
            return null;
        } else {
            return new HabitationMaj($maisonMaj);
        }
    }
    
    public function getId(): ?int
    {
        return $this->getField('idMh');
    }
    
    public function getKey(): ?string
    {
        return $this->getField("userKey");
    }
    
    public function getPouvoir(): ?PouvoirMaj
    {
        
        $actionMaj = $this->getField('actionsHero');
        if ($actionMaj == null) {
            return null;
        } else {
            return new PouvoirMaj($actionMaj);
        }
    }
    
    
}