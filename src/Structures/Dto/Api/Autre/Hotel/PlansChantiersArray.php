<?php

namespace App\Structures\Dto\Api\Autre\Hotel;

use App\Structures\Collection\PlansChantiers;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

class PlansChantiersArray
{
    /**
     * @var PlansChantiers[] $plansChantier ;
     */
    #[SerializedName("")]
    #[Groups(['plan'])]
    private array $plansChantiers;
    
    /**
     * @return array
     */
    public function getPlansChantiers(): array
    {
        return $this->plansChantiers;
    }
    
    /**
     * @return PlansChantiersArray
     */
    public function setPlansChantiers(array $plansChantiers): PlansChantiersArray
    {
        $this->plansChantiers = $plansChantiers;
        
        return $this;
    }
}