<?php

namespace App\Structures\Dto\Api\MH;

class RegenDirMaj extends ApiMaj
{
    public function getContentDE(): ?string
    {
        return $this->getField('de');
    }
    
    public function getContentEN(): ?string
    {
        return $this->getField('en');
    }
    
    public function getContentES(): ?string
    {
        return $this->getField('es');
    }
    
    public function getContentFR(): ?string
    {
        return $this->getField('fr');
    }
    
}