<?php

namespace App\Structures\Dto\Api\MH;

class ChantiersRessourcesMaj extends ApiMaj
{
    public function getAmount(): ?string
    {
        return $this->getField('amount');
    }
    
    public function getRsc(): ?ItemsRessourcesMaj
    {
        return new ItemsRessourcesMaj($this->getField('rsc'));
    }
    
}