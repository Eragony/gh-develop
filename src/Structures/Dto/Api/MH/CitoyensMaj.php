<?php

namespace App\Structures\Dto\Api\MH;

class CitoyensMaj extends ApiMaj
{
    
    public function getAvatar(): ?string
    {
        return $this->getField('avatar');
    }
    
    public function getBaseDef(): ?int
    {
        return $this->getField('baseDef');
    }
    
    public function getCleanupType(): ?string
    {
        if ($this->getField('cleanup') !== null) {
            $cleanType = $this->getField('cleanup')['type'];
        } else {
            $cleanType = null;
        }
        
        return $cleanType;
    }
    
    public function getDtype(): ?int
    {
        return $this->getField('dtype');
    }
    
    public function getHomeMessage(): ?string
    {
        return $this->getField('homeMessage');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getJob(): ?string
    {
        return $this->getField('job')['uid'] ?? null;
    }
    
    public function getMapId(): ?int
    {
        return $this->getField('mapId');
    }
    
    public function getMsg(): ?string
    {
        return $this->getField('msg');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
    public function getOrigin(): mixed
    {
        return $this->getField('origin');
    }
    
    /**
     * @return PictoMaj[]|null
     */
    public function getPictos(): ?array
    {
        return array_map(fn($i) => new PictoMaj($i), $this->getField('rewards'));
    }
    
    /**
     * @return VilleHistoriqueMaj[]|null
     */
    public function getPlayedMap(): ?array
    {
        return array_map(fn($i) => new VilleHistoriqueMaj($i), $this->getField('playedMaps'));
    }
    
    public function getScore(): ?int
    {
        return $this->getField('score');
    }
    
    public function getSurvival(): ?int
    {
        return $this->getField('survival');
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
    public function getcleanupName(): ?string
    {
        if ($this->getField('cleanup') !== null) {
            $cleanName = $this->getField('cleanup')['user'];
        } else {
            $cleanName = null;
        }
        
        return $cleanName;
    }
    
    public function isBan(): ?bool
    {
        return $this->getField('ban');
    }
    
    public function isDead(): ?bool
    {
        return $this->getField('dead');
    }
    
    public function isGhost(): ?bool
    {
        return $this->getField('isGhost');
    }
    
    public function isHero(): ?bool
    {
        return $this->getField('hero');
    }
    
    public function isOut(): ?bool
    {
        return $this->getField('out');
    }
    
}