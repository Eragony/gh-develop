<?php

namespace App\Structures\Dto\Api\MH;

class BatsMaj extends ApiMaj
{
    public function getDesc(): ?string
    {
        return $this->getField('desc');
    }
    
    public function getExplorable(): ?bool
    {
        return $this->getField('explorable');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getImg(): ?string
    {
        return $this->getField('img');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
}