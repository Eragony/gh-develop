<?php

namespace App\Structures\Dto\Api\MH;

class ItemMaj extends ApiMaj
{
    
    public function getCount(): ?int
    {
        return $this->getField('count');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getUid(): ?string
    {
        return $this->getField('uid');
    }
    
    public function isBroken(): ?bool
    {
        return $this->getField('broken');
    }
    
}