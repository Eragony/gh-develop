<?php

namespace App\Structures\Dto\Api\MH;

class VilleHistoriqueMaj extends ApiMaj
{
    public function getDureeVille(): ?int
    {
        return $this->getField('day');
    }
    
    public function getJourMort(): ?int
    {
        return $this->getField('survival');
    }
    
    public function getMapId(): ?int
    {
        return $this->getField('mapId');
    }
    
    public function getMessageMort(): ?string
    {
        return $this->getField('msg') ?? '';
    }
    
    public function getNomVille(): ?string
    {
        return $this->getField('mapName') ?? '';
    }
    
    public function getOrigin(): ?string
    {
        return $this->getField('origin');
    }
    
    public function getPhase(): ?string
    {
        return $this->getField('phase') ?? null;
    }
    
    public function getPointAme(): ?int
    {
        return $this->getField('sp')??0;
    }
    
    /**
     * @return PictoMaj[]|null
     */
    public function getRewards(): ?array
    {
        return array_map(fn($i) => new PictoMaj($i), $this->getField('rewards'));
    }
    
    public function getScoreVille(): ?int
    {
        return $this->getField('score');
    }
    
    public function getSeason(): ?int
    {
        $saison = $this->getField('season');
        
        if ($saison === 0 && $this->getPhase() === 'alpha') {
            $saison = 15;
        }
        return $saison;
    }
    
    public function getType(): ?int
    {
        return $this->getField('type');
    }
    
    public function getTypeMort(): ?int
    {
        return $this->getField('dtype');
    }
    
    public function getcleanupName(): ?string
    {
        if ($this->getField('cleanup') !== null) {
            $cleanName = $this->getField('cleanup')['user'];
        } else {
            $cleanName = null;
        }
        
        return $cleanName;
    }
    
    public function getcleanupType(): ?string
    {
        if ($this->getField('cleanup') !== null) {
            $cleanName = $this->getField('cleanup')['type'];
        } else {
            $cleanName = null;
        }
        
        return $cleanName;
    }
    
}