<?php

namespace App\Structures\Dto\Api\MH;

class PictosMajPrototype extends ApiMaj
{
    
    /**
     * @return PictosMaj[]|null
     */
    public function getPictos(): ?array
    {
        return array_combine(array_keys($this->data), array_map(fn($i) => new PictosMaj($i), $this->data));
    }
    
    
}