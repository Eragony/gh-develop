<?php

namespace App\Structures\Dto\Api\MH;

class ItemsMaj extends ApiMaj
{
    public function getCat(): ?string
    {
        return $this->getField('cat');
    }
    
    public function getDeco(): ?string
    {
        return $this->getField('deco');
    }
    
    public function getDef(): ?int
    {
        return $this->getField('guard');
    }
    
    public function getDesc(): ?string
    {
        return $this->getField('desc');
    }
    
    public function getEncombrant(): ?int
    {
        return $this->getField('heavy');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getImg(): ?string
    {
        return $this->getField('img');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
}