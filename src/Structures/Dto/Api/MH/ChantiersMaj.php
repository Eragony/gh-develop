<?php

namespace App\Structures\Dto\Api\MH;

class ChantiersMaj extends ApiMaj
{
    public function getDef(): ?int
    {
        return $this->getField('def');
    }
    
    public function getDesc(): ?string
    {
        return $this->getField('desc');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getImg(): ?string
    {
        return $this->getField('img');
    }
    
    public function getIndes(): ?bool
    {
        return !$this->getField('breakable');
    }
    
    public function getMaxLife(): ?int
    {
        return $this->getField('maxLife');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
    public function getPa(): ?int
    {
        return $this->getField('pa');
    }
    
    public function getParent(): ?int
    {
        return $this->getField('parent');
    }
    
    public function getRarity(): ?int
    {
        return $this->getField('rarity');
    }
    
    /**
     * @return ChantiersRessourcesMaj[]
     */
    public function getRessources(): array
    {
        
        $temp = array_map(fn($i) => new ChantiersRessourcesMaj($i), $this->getField('resources'));
        
        return array_combine(array_map(fn(ChantiersRessourcesMaj $i) => $i->getRsc()->getId(), $temp), $temp);
    }
    
    public function getTemporary(): ?bool
    {
        return $this->getField('temporary');
    }
    
    
}