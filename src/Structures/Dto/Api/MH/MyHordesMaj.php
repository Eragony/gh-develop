<?php

namespace App\Structures\Dto\Api\MH;

class MyHordesMaj extends ApiMaj
{
    
    public function getAvatar(): ?string
    {
        return $this->getField('avatar');
    }
    
    public function getAvatarData(): ?AvatarDataMaj
    {
        $avatarDataMaj = $this->getField('avatarData');
        if ($avatarDataMaj == null) {
            return null;
        } else {
            return new AvatarDataMaj($avatarDataMaj);
        }
        
    }
    
    public function getError(): ?string
    {
        return $this->getField('error') ?? null;
    }
    
    public function getId(): ?int
    {
        return $this->getField("id");
    }
    
    public function getLocale(): ?string
    {
        return $this->getField('locale');
    }
    
    public function getMap(): ?MapMaj
    {
        $mapMaj = $this->getField('map');
        if ($mapMaj == null) {
            return null;
        } else {
            return new MapMaj($mapMaj);
        }
    }
    
    public function getMapId(): ?int
    {
        return $this->getField('mapId');
    }
    
    public function getName(): ?string
    {
        return $this->getField("name");
    }
    
    /**
     * @return VilleHistoriqueMaj[]|null
     */
    public function getPlayedMap(): ?array
    {
        if ($this->getField('playedMaps') == null) {
            return null;
        }
        return array_map(fn($i) => new VilleHistoriqueMaj($i), $this->getField('playedMaps') ?? []);
    }
    
    /**
     * @return PictoMaj[]|null
     */
    public function getRewards(): ?array
    {
        if ($this->getField('rewards') == null) {
            return null;
        }
        return array_map(fn($i) => new PictoMaj($i), $this->getField('rewards'));
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
    public function isBan(): ?bool
    {
        return $this->getField('ban');
    }
    
    public function isDead(): ?bool
    {
        return $this->getField('dead');
    }
    
    public function isGhost(): ?bool
    {
        return $this->getField('isGhost');
    }
    
    public function isHero(): ?bool
    {
        return $this->getField('hero');
    }
    
    public function isOut(): ?bool
    {
        return $this->getField('out');
    }
    
    
}