<?php

namespace App\Structures\Dto\Api\MH;

class PictosMaj extends ApiMaj
{
    public function getCommunity(): ?bool
    {
        return $this->getField('community');
    }
    
    public function getDesc(): ?string
    {
        return $this->getField('desc');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getImg(): ?string
    {
        return $this->getField('img');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
    public function getRare(): ?bool
    {
        return $this->getField('rare');
    }
    
}