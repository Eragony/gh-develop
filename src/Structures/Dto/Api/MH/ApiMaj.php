<?php

namespace App\Structures\Dto\Api\MH;

class ApiMaj
{
    public ?array $data;
    
    public function __construct(?array $response)
    {
        
        $this->data = $response;
        
    }
    
    /**
     * Returns a field from the Graph node data.
     *
     * @return mixed
     */
    protected function getField(string $key): mixed
    {
        return $this->data[$key] ?? null;
    }
    
}