<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions;

use App\Structures\Dto\GH\Hotel\Expeditions\Objet\Expedition;
use App\Structures\Dto\GH\Hotel\Expeditions\Objet\InscriptionExpeditionCharacteristic;
use App\Structures\Dto\GH\Hotel\Expeditions\Objet\Ouvrier;
use Symfony\Component\Serializer\Attribute\Groups;

class InscriptionExpeditionSauvegarde
{
    #[Groups('outils_expe')]
    private ?Expedition $expedition = null;
    
    #[Groups('outils_expe')]
    private ?Ouvrier $ouvrier = null;
    
    #[Groups('outils_expe')]
    private ?InscriptionExpeditionCharacteristic $modification = null;
    
    /**
     * @return Expedition|null
     */
    public function getExpedition(): ?Expedition
    {
        return $this->expedition;
    }
    
    /**
     * @param Expedition|null $expedition
     * @return InscriptionExpeditionSauvegarde
     */
    final  public function setExpedition(?Expedition $expedition): InscriptionExpeditionSauvegarde
    {
        $this->expedition = $expedition;
        return $this;
    }
    
    /**
     * @return InscriptionExpeditionCharacteristic|null
     */
    public function getModification(): ?InscriptionExpeditionCharacteristic
    {
        return $this->modification;
    }
    
    /**
     * @param InscriptionExpeditionCharacteristic|null $inscriptionExpeditionCharacteristic
     * @return InscriptionExpeditionSauvegarde
     */
    public function setModification(?InscriptionExpeditionCharacteristic $inscriptionExpeditionCharacteristic): InscriptionExpeditionSauvegarde
    {
        $this->modification = $inscriptionExpeditionCharacteristic;
        return $this;
    }
    
    /**
     * @return Ouvrier|null
     */
    public function getOuvrier(): ?Ouvrier
    {
        return $this->ouvrier;
    }
    
    /**
     * @param Ouvrier|null $ouvrier
     * @return InscriptionExpeditionSauvegarde
     */
    public function setOuvrier(?Ouvrier $ouvrier): InscriptionExpeditionSauvegarde
    {
        $this->ouvrier = $ouvrier;
        return $this;
    }
}