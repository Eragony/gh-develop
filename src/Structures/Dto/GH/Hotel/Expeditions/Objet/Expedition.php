<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions\Objet;

use Symfony\Component\Serializer\Attribute\Groups;

class Expedition
{
    #[Groups('outils_expe')]
    private string $expeditionId;
    
    #[Groups('outils_expe')]
    private string $expeditionnaireId;
    
    /**
     * @return string
     */
    public function getExpeditionId(): string
    {
        return $this->expeditionId;
    }
    
    /**
     * @param string $expeditionId
     * @return Expedition
     */
    public function setExpeditionId(string $expeditionId): Expedition
    {
        $this->expeditionId = $expeditionId;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getExpeditionnaireId(): string
    {
        return $this->expeditionnaireId;
    }
    
    /**
     * @param string $expeditionnaireId
     * @return Expedition
     */
    public function setExpeditionnaireId(string $expeditionnaireId): Expedition
    {
        $this->expeditionnaireId = $expeditionnaireId;
        return $this;
    }
}