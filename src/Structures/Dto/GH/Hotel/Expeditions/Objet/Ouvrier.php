<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions\Objet;

use Symfony\Component\Serializer\Attribute\Groups;

class Ouvrier
{
    #[Groups('outils_expe')]
    private int $ouvrierId;
    
    /**
     * @return int
     */
    public function getOuvrierId(): int
    {
        return $this->ouvrierId;
    }
    
    /**
     * @param int $ouvrierId
     * @return Ouvrier
     */
    public function setOuvrierId(int $ouvrierId): Ouvrier
    {
        $this->ouvrierId = $ouvrierId;
        return $this;
    }
}