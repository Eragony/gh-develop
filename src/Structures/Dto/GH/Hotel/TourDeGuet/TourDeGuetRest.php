<?php

namespace App\Structures\Dto\GH\Hotel\TourDeGuet;

use App\Entity\EstimationTdg;
use Symfony\Component\Serializer\Attribute\Groups;

class TourDeGuetRest
{
    #[Groups(['tdg'])]
    private ?int $mapId  = null;
    #[Groups(['tdg'])]
    private ?int $userId = null;
    #[Groups(['tdg'])]
    private ?int $jour   = null;
    /**
     * @var EstimationTdg[]|null
     */
    #[Groups(['tdg'])]
    private ?array $tdg = null;
    /**
     * @var EstimationTdg[]|null
     */
    #[Groups(['tdg'])]
    private ?array $planif = null;
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return TourDeGuetRest
     */
    public function setJour(?int $jour): TourDeGuetRest
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return TourDeGuetRest
     */
    public function setMapId(?int $mapId): TourDeGuetRest
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return array|null
     */
    public function getPlanif(): ?array
    {
        return $this->planif;
    }
    
    /**
     * @param array|null $planif
     * @return TourDeGuetRest
     */
    public function setPlanif(?array $planif): TourDeGuetRest
    {
        $this->planif = $planif;
        return $this;
    }
    
    /**
     * @return array|null
     */
    public function getTdg(): ?array
    {
        return $this->tdg;
    }
    
    /**
     * @param array|null $tdg
     * @return TourDeGuetRest
     */
    public function setTdg(?array $tdg): TourDeGuetRest
    {
        $this->tdg = $tdg;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return TourDeGuetRest
     */
    public function setUserId(?int $userId): TourDeGuetRest
    {
        $this->userId = $userId;
        return $this;
    }
    
}
