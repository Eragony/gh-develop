<?php

namespace App\Structures\Dto\GH\Ville\Carte;

use Symfony\Component\Serializer\Attribute\Groups;

class FiltreObjet
{
    #[Groups('filtre_objet')]
    private ?int $mapId = null;
    
    #[Groups('filtre_objet')]
    private ?int $userId = null;
    
    #[Groups('filtre_objet')]
    private ?int $typeFiltre = null;
    
    #[Groups('filtre_objet')]
    private ?int $min = null;
    
    #[Groups('filtre_objet')]
    private ?int $max = null;
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return FiltreObjet
     */
    public function setMapId(?int $mapId): FiltreObjet
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMax(): ?int
    {
        return $this->max;
    }
    
    /**
     * @param int|null $max
     * @return FiltreObjet
     */
    public function setMax(?int $max): FiltreObjet
    {
        $this->max = $max;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMin(): ?int
    {
        return $this->min;
    }
    
    /**
     * @param int|null $min
     * @return FiltreObjet
     */
    public function setMin(?int $min): FiltreObjet
    {
        $this->min = $min;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getTypeFiltre(): ?int
    {
        return $this->typeFiltre;
    }
    
    /**
     * @param int|null $typeFiltre
     * @return FiltreObjet
     */
    public function setTypeFiltre(?int $typeFiltre): FiltreObjet
    {
        $this->typeFiltre = $typeFiltre;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return FiltreObjet
     */
    public function setUserId(?int $userId): FiltreObjet
    {
        $this->userId = $userId;
        return $this;
    }
    
}