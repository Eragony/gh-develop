<?php

namespace App\Structures\Dto\GH\Ville\Carte;

class SomItemSolDTO
{
    private int $nbrItemBank;
    
    public function __construct(private int $nbrItem, private int $id, private string $nom, private string $icon,
                                private int $category_objet_id)
    {
    }
    
    /**
     * @return int
     */
    public function getCategoryObjetId(): int
    {
        return $this->category_objet_id;
    }
    
    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return int
     */
    public function getNbrItem(): int
    {
        return $this->nbrItem;
    }
    
    /**
     * @return int
     */
    public function getNbrItemBank(): int
    {
        return $this->nbrItemBank;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setNbrItemBank(int $nbrItemBank): SomItemSolDTO
    {
        $this->nbrItemBank = $nbrItemBank;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setCategoryObjetId(int $category_objet_id): SomItemSolDTO
    {
        $this->category_objet_id = $category_objet_id;
        return $this;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setIcon(string $icon): SomItemSolDTO
    {
        $this->icon = $icon;
        return $this;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setId(int $id): SomItemSolDTO
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setNbrItem(int $nbrItem): SomItemSolDTO
    {
        $this->nbrItem = $nbrItem;
        return $this;
    }
    
    /**
     * @return SomItemSolDTO
     */
    public function setNom(string $nom): SomItemSolDTO
    {
        $this->nom = $nom;
        return $this;
    }
    
}