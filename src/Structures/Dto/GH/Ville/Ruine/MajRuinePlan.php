<?php

namespace App\Structures\Dto\GH\Ville\Ruine;

use App\Entity\RuinesCases;
use Symfony\Component\Serializer\Attribute\Groups;

class MajRuinePlan
{
    #[Groups(['ruine'])]
    private string $ruineId;
    
    #[Groups(['ruine'])]
    private int $mapId;
    
    /** @var RuinesCases[] */
    #[Groups(['ruine'])]
    private array $plan;
    
    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    /**
     * @return MajRuinePlan
     */
    public function setMapId(int $mapId): MajRuinePlan
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getPlan(): array
    {
        return $this->plan;
    }
    
    /**
     * @return MajRuinePlan
     */
    public function setPlan(array $plan): MajRuinePlan
    {
        $this->plan = $plan;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getRuineId(): string
    {
        return $this->ruineId;
    }
    
    /**
     * @return MajRuinePlan
     */
    public function setRuineId(string $ruineId): MajRuinePlan
    {
        $this->ruineId = $ruineId;
        return $this;
    }
}