<?php

namespace App\Structures\Dto\GH\Ville\Ruine;

use App\Entity\RuinesCases;
use Symfony\Component\Serializer\Attribute\Groups;

class MajRuineObjetCase
{
    #[Groups(['ruine'])]
    private string $ruineId;
    
    #[Groups(['ruine'])]
    private int $mapId;
    
    #[Groups(['ruine'])]
    private RuinesCases $ruinesCases;
    
    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    /**
     * @return MajRuinePlan
     */
    public function setMapId(int $mapId): MajRuineObjetCase
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getRuineId(): string
    {
        return $this->ruineId;
    }
    
    /**
     * @return MajRuinePlan
     */
    public function setRuineId(string $ruineId): MajRuineObjetCase
    {
        $this->ruineId = $ruineId;
        return $this;
    }
    
    /**
     * @return RuinesCases
     */
    public function getRuinesCases(): RuinesCases
    {
        return $this->ruinesCases;
    }
    
    /**
     * @param RuinesCases $ruinesCases
     * @return MajRuineObjetCase
     */
    public function setRuinesCases(RuinesCases $ruinesCases): MajRuineObjetCase
    {
        $this->ruinesCases = $ruinesCases;
        return $this;
    }
}