<?php

namespace App\Structures\Dto\GH\Ville\Ruine;

use Symfony\Component\Serializer\Attribute\Groups;

class MajFilAriane
{
    #[Groups(['ruine'])]
    private string $ruineId;
    #[Groups(['ruine'])]
    private int    $mapId;
    #[Groups(['ruine'])]
    private array  $coord;
    
    /**
     * @return array
     */
    public function getCoord(): array
    {
        return $this->coord;
    }
    
    /**
     * @return MajFilAriane
     */
    public function setCoord(array $coord): MajFilAriane
    {
        $this->coord = $coord;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    /**
     * @return MajFilAriane
     */
    public function setMapId(int $mapId): MajFilAriane
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getRuineId(): string
    {
        return $this->ruineId;
    }
    
    /**
     * @return MajFilAriane
     */
    public function setRuineId(string $ruineId): MajFilAriane
    {
        $this->ruineId = $ruineId;
        return $this;
    }
    
}