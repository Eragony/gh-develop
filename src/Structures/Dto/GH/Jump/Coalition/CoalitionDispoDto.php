<?php


namespace App\Structures\Dto\GH\Jump\Coalition;


use Symfony\Component\Serializer\Attribute\Groups;

class CoalitionDispoDto
{
    #[Groups(['coalition'])]
    private string $idJump;
    
    #[Groups(['coalition'])]
    private int $userId;
    
    #[Groups(['coalition'])]
    private int $idDispo;
    
    #[Groups(['coalition'])]
    private int $idCreneau;
    
    /**
     * @return int
     */
    public function getIdCreneau(): int
    {
        return $this->idCreneau;
    }
    
    /**
     * @return CoalitionDispoDto
     */
    public function setIdCreneau(int $idCreneau): CoalitionDispoDto
    {
        $this->idCreneau = $idCreneau;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdDispo(): int
    {
        return $this->idDispo;
    }
    
    /**
     * @return CoalitionDispoDto
     */
    public function setIdDispo(int $idDispo): CoalitionDispoDto
    {
        $this->idDispo = $idDispo;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdJump(): string
    {
        return $this->idJump;
    }
    
    /**
     * @return CoalitionDispoDto
     */
    public function setIdJump(string $idJump): CoalitionDispoDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @return CoalitionDispoDto
     */
    public function setUserId(int $userId): CoalitionDispoDto
    {
        $this->userId = $userId;
        return $this;
    }
    
}