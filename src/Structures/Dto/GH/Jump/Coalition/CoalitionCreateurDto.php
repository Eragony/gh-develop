<?php


namespace App\Structures\Dto\GH\Jump\Coalition;


use Symfony\Component\Serializer\Attribute\Groups;

class CoalitionCreateurDto
{
    #[Groups(['coalition'])]
    private ?string $idJump = null;
    
    #[Groups(['coalition'])]
    private ?int $userId = null;
    
    #[Groups(['coalition'])]
    private ?bool $createur = null;
    
    #[Groups(['coalition'])]
    private ?int $numCoa = null;
    
    #[Groups(['coalition'])]
    private ?int $posCoa = null;
    
    /**
     * @return bool|null
     */
    public function getCreateur(): ?bool
    {
        return $this->createur;
    }
    
    /**
     * @param bool|null $createur
     * @return CoalitionCreateurDto
     */
    public function setCreateur(?bool $createur): CoalitionCreateurDto
    {
        $this->createur = $createur;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return CoalitionCreateurDto
     */
    public function setIdJump(?string $idJump): CoalitionCreateurDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getNumCoa(): ?int
    {
        return $this->numCoa;
    }
    
    /**
     * @param int|null $numCoa
     * @return CoalitionCreateurDto
     */
    public function setNumCoa(?int $numCoa): CoalitionCreateurDto
    {
        $this->numCoa = $numCoa;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPosCoa(): ?int
    {
        return $this->posCoa;
    }
    
    /**
     * @param int|null $posCoa
     * @return CoalitionCreateurDto
     */
    public function setPosCoa(?int $posCoa): CoalitionCreateurDto
    {
        $this->posCoa = $posCoa;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return CoalitionCreateurDto
     */
    public function setUserId(?int $userId): CoalitionCreateurDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}