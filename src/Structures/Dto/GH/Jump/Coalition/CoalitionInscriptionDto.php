<?php


namespace App\Structures\Dto\GH\Jump\Coalition;


use Symfony\Component\Serializer\Attribute\Groups;

class CoalitionInscriptionDto
{
    #[Groups(['coalition'])]
    private ?string $idJump = null;
    
    #[Groups(['coalition'])]
    private ?int $userId = null;
    
    #[Groups(['coalition'])]
    private ?int $idUserFournis = null;
    
    #[Groups(['coalition'])]
    private ?int $idUserEchange = null;
    
    #[Groups(['coalition'])]
    private ?int $numCoa = null;
    
    #[Groups(['coalition'])]
    private ?int $posCoa = null;
    
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return CoalitionInscriptionDto
     */
    public function setIdJump(?string $idJump): CoalitionInscriptionDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUserEchange(): ?int
    {
        return $this->idUserEchange;
    }
    
    /**
     * @param int|null $idUserEchange
     * @return CoalitionInscriptionDto
     */
    public function setIdUserEchange(?int $idUserEchange): CoalitionInscriptionDto
    {
        $this->idUserEchange = $idUserEchange;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUserFournis(): ?int
    {
        return $this->idUserFournis;
    }
    
    /**
     * @param int|null $idUserFournis
     * @return CoalitionInscriptionDto
     */
    public function setIdUserFournis(?int $idUserFournis): CoalitionInscriptionDto
    {
        $this->idUserFournis = $idUserFournis;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getNumCoa(): ?int
    {
        return $this->numCoa;
    }
    
    /**
     * @param int|null $numCoa
     * @return CoalitionInscriptionDto
     */
    public function setNumCoa(?int $numCoa): CoalitionInscriptionDto
    {
        $this->numCoa = $numCoa;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPosCoa(): ?int
    {
        return $this->posCoa;
    }
    
    /**
     * @param int|null $posCoa
     * @return CoalitionInscriptionDto
     */
    public function setPosCoa(?int $posCoa): CoalitionInscriptionDto
    {
        $this->posCoa = $posCoa;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return CoalitionInscriptionDto
     */
    public function setUserId(?int $userId): CoalitionInscriptionDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}