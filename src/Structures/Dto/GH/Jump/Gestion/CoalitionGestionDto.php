<?php


namespace App\Structures\Dto\GH\Jump\Gestion;


use Symfony\Component\Serializer\Attribute\Groups;

class CoalitionGestionDto
{
    #[Groups(['coalition'])]
    private ?string $idJump = null;
    
    #[Groups(['coalition'])]
    private ?int $userId = null;
    
    #[Groups(['coalition'])]
    private ?bool $fige = null;
    
    /**
     * @return bool|null
     */
    public function getFige(): ?bool
    {
        return $this->fige;
    }
    
    /**
     * @param bool|null $fige
     * @return CoalitionGestionDto
     */
    public function setFige(?bool $fige): CoalitionGestionDto
    {
        $this->fige = $fige;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return CoalitionGestionDto
     */
    public function setIdJump(?string $idJump): CoalitionGestionDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return CoalitionGestionDto
     */
    public function setUserId(?int $userId): CoalitionGestionDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}