<?php


namespace App\Structures\Dto\GH\Jump\Gestion;


use App\Entity\Jump;
use Symfony\Component\Serializer\Attribute\Groups;

class JumpDto
{
    #[Groups(['jump'])]
    private Jump $jump;
    
    public function getJump(): Jump
    {
        return $this->jump;
    }
    
    public function setJump(Jump $jump): void
    {
        $this->jump = $jump;
    }
}