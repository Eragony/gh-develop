<?php

namespace App\Structures\Dto\GH\Outils\Decharges;

use App\Entity\Decharges;
use App\Structures\Dto\GH\Outils\Reparation\ReparationsSaveDto;
use Symfony\Component\Serializer\Attribute\Groups;

class DechargesSaveDto
{
    #[Groups(['decharges'])]
    private int $mapId;
    #[Groups(['decharges'])]
    private int $userId;
    /**
     * @var Decharges[]
     */
    #[Groups(['decharges'])]
    private array $decharge;
    
    /**
     * @return array
     */
    public function getDecharge(): array
    {
        return $this->decharge;
    }
    
    /**
     * @param array $decharge
     * @return DechargesSaveDto
     */
    public function setDecharge(array $decharge): DechargesSaveDto
    {
        $this->decharge = $decharge;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setMapId(int $mapId): DechargesSaveDto
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setUserId(int $userId): DechargesSaveDto
    {
        $this->userId = $userId;
        return $this;
    }
    
}