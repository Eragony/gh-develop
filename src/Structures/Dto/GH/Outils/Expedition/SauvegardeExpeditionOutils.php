<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use App\Entity\Expedition;
use Symfony\Component\Serializer\Attribute\Groups;

class SauvegardeExpeditionOutils
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    #[Groups('outils_expe')]
    private Expedition $expedition;
    
    #[Groups('outils_expe')]
    private ?int $jour = null;
    
    /**
     * @return Expedition
     */
    public function getExpedition(): Expedition
    {
        return $this->expedition;
    }
    
    /**
     * @param Expedition $expedition
     * @return SauvegardeExpeditionOutils
     */
    public function setExpedition(Expedition $expedition): SauvegardeExpeditionOutils
    {
        $this->expedition = $expedition;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return SauvegardeExpeditionOutils
     */
    public function setIdUser(?int $idUser): SauvegardeExpeditionOutils
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return SauvegardeExpeditionOutils
     */
    public function setJour(?int $jour): SauvegardeExpeditionOutils
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return SauvegardeExpeditionOutils
     */
    public function setMapId(?int $mapId): SauvegardeExpeditionOutils
    {
        $this->mapId = $mapId;
        return $this;
    }
}