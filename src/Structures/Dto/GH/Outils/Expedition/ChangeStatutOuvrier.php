<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use Symfony\Component\Serializer\Attribute\Groups;

class ChangeStatutOuvrier
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    #[Groups('outils_expe')]
    private ?int $jour = null;
    
    #[Groups('outils_expe')]
    private ?bool $statut = false;
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return ChangeStatutExpedition
     */
    public function setIdUser(?int $idUser): ChangeStatutOuvrier
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return ChangeStatutExpedition
     */
    public function setJour(?int $jour): ChangeStatutOuvrier
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return ChangeStatutExpedition
     */
    public function setMapId(?int $mapId): ChangeStatutOuvrier
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getStatut(): ?bool
    {
        return $this->statut;
    }
    
    /**
     * @param bool|null $statut
     * @return ChangeStatutExpedition
     */
    public function setStatut(?bool $statut): ChangeStatutOuvrier
    {
        $this->statut = $statut;
        return $this;
    }
}