<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use Symfony\Component\Serializer\Attribute\Groups;

class VerrouillageExpedition
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    #[Groups('outils_expe')]
    private ?int $jour = null;
    
    #[Groups('outils_expe')]
    private ?string $expeditionId = null;
    
    #[Groups('outils_expe')]
    private ?bool $verrouillage = null;
    
    public function getExpeditionId(): ?string
    {
        return $this->expeditionId;
    }
    
    public function setExpeditionId(?string $expeditionId): void
    {
        $this->expeditionId = $expeditionId;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return ChangeStatutExpedition
     */
    public function setIdUser(?int $idUser): VerrouillageExpedition
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return ChangeStatutExpedition
     */
    public function setJour(?int $jour): VerrouillageExpedition
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return ChangeStatutExpedition
     */
    public function setMapId(?int $mapId): VerrouillageExpedition
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    public function isVerrouillage(): ?bool
    {
        return $this->verrouillage;
    }
    
    public function setVerrouillage(?bool $verrouillage): void
    {
        $this->verrouillage = $verrouillage;
    }
    
}