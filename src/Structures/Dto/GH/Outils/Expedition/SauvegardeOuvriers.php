<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use App\Entity\Ouvriers;
use Symfony\Component\Serializer\Attribute\Groups;

class SauvegardeOuvriers
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    /** @var Ouvriers[] $ouvriers */
    #[Groups('outils_expe')]
    private array $ouvriers = [];
    
    #[Groups('outils_expe')]
    private ?int $jour = null;
    
    #[Groups('outils_expe')]
    private ?int $directionFao = null;
    
    /**
     * @return int|null
     */
    public function getDirectionFao(): ?int
    {
        return $this->directionFao;
    }
    
    /**
     * @param int|null $directionFao
     * @return SauvegardeOuvriers
     */
    public function setDirectionFao(?int $directionFao): SauvegardeOuvriers
    {
        $this->directionFao = $directionFao;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return SauvegardeOuvriers
     */
    public function setIdUser(?int $idUser): SauvegardeOuvriers
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return SauvegardeOuvriers
     */
    public function setJour(?int $jour): SauvegardeOuvriers
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return SauvegardeOuvriers
     */
    public function setMapId(?int $mapId): SauvegardeOuvriers
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getOuvriers(): array
    {
        return $this->ouvriers;
    }
    
    /**
     * @param array $ouvriers
     * @return SauvegardeOuvriers
     */
    public function setOuvriers(array $ouvriers): SauvegardeOuvriers
    {
        $this->ouvriers = $ouvriers;
        return $this;
    }
    
    
}