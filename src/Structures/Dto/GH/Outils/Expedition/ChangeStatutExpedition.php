<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use Symfony\Component\Serializer\Attribute\Groups;

class ChangeStatutExpedition
{
    #[Groups('outils_expe')]
    private ?int   $mapId  = null;
    #[Groups('outils_expe')]
    private ?int   $idUser = null;
    #[Groups('outils_expe')]
    private string $expeditionId;
    #[Groups('outils_expe')]
    private ?int   $jour   = null;
    #[Groups('outils_expe')]
    private ?bool  $statut = false;
    
    /**
     * @return string
     */
    public function getExpeditionId(): string
    {
        return $this->expeditionId;
    }
    
    /**
     * @param string $expeditionId
     * @return ChangeStatutExpedition
     */
    public function setExpeditionId(string $expeditionId): ChangeStatutExpedition
    {
        $this->expeditionId = $expeditionId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return ChangeStatutExpedition
     */
    public function setIdUser(?int $idUser): ChangeStatutExpedition
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return ChangeStatutExpedition
     */
    public function setJour(?int $jour): ChangeStatutExpedition
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return ChangeStatutExpedition
     */
    public function setMapId(?int $mapId): ChangeStatutExpedition
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getStatut(): ?bool
    {
        return $this->statut;
    }
    
    /**
     * @param bool|null $statut
     * @return ChangeStatutExpedition
     */
    public function setStatut(?bool $statut): ChangeStatutExpedition
    {
        $this->statut = $statut;
        return $this;
    }
}