<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use Symfony\Component\Serializer\Attribute\Groups;

class DuplicateOuvriers
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    #[Groups('outils_expe')]
    private ?int $fromDay = null;
    
    #[Groups('outils_expe')]
    private ?int $toDay = null;
    
    #[Groups('outils_expe')]
    private ?bool $withPreinscrit = null;
    
    public function getFromDay(): ?int
    {
        return $this->fromDay;
    }
    
    public function setFromDay(?int $fromDay): DuplicateOuvriers
    {
        $this->fromDay = $fromDay;
        return $this;
    }
    
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    public function setIdUser(?int $idUser): DuplicateOuvriers
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    public function setMapId(?int $mapId): DuplicateOuvriers
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    public function getToDay(): ?int
    {
        return $this->toDay;
    }
    
    public function setToDay(?int $toDay): DuplicateOuvriers
    {
        $this->toDay = $toDay;
        return $this;
    }
    
    public function getWithPreinscrit(): ?bool
    {
        return $this->withPreinscrit;
    }
    
    public function setWithPreinscrit(?bool $withPreinscrit): DuplicateOuvriers
    {
        $this->withPreinscrit = $withPreinscrit;
        return $this;
    }
    
}