<?php

namespace App\Structures\Dto\GH\Outils\Expedition\Response;

use App\Entity\Expedition;
use Symfony\Component\Serializer\Attribute\Groups;

class SauvegardeExpeditionOutilsResponse
{
    /** @var Expedition[] $expeditions */
    #[Groups('outils_expe')]
    private array $expeditions = [];
    
    public function getExpeditions(): array
    {
        return $this->expeditions;
    }
    
    public function setExpeditions(array $expeditions): void
    {
        $this->expeditions = $expeditions;
    }
}