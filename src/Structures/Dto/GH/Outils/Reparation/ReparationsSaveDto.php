<?php

namespace App\Structures\Dto\GH\Outils\Reparation;

use App\Entity\OutilsReparation;
use Symfony\Component\Serializer\Attribute\Groups;

class ReparationsSaveDto
{
    #[Groups(['outils_repa'])]
    private int $mapId;
    
    #[Groups(['outils_repa'])]
    private int $userId;
    
    #[Groups(['outils_repa'])]
    private OutilsReparation $outilsReparations;
    
    #[Groups(['outils_repa'])]
    private bool $sauvegarde;
    
    /**
     * @return int
     */
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setMapId(int $mapId): ReparationsSaveDto
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return OutilsReparation
     */
    public function getOutilsReparations(): OutilsReparation
    {
        return $this->outilsReparations;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setOutilsReparations(OutilsReparation $outilsReparations): ReparationsSaveDto
    {
        $this->outilsReparations = $outilsReparations;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setUserId(int $userId): ReparationsSaveDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isSauvegarde(): bool
    {
        return $this->sauvegarde;
    }
    
    /**
     * @return ReparationsSaveDto
     */
    public function setSauvegarde(bool $sauvegarde): ReparationsSaveDto
    {
        $this->sauvegarde = $sauvegarde;
        return $this;
    }
    
}