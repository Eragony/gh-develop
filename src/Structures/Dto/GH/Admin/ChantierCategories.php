<?php

namespace App\Structures\Dto\GH\Admin;

use App\Entity\ChantierPrototype;
use Symfony\Component\Serializer\Attribute\Groups;

class ChantierCategories
{
    /**
     * @var ChantierPrototype[]
     */
    #[Groups('admin_arbre')]
    private array $categories;
    
    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
    
    /**
     * @return ChantierCategories
     */
    public function setCategories(array $categories): ChantierCategories
    {
        $this->categories = $categories;
        return $this;
    }
}