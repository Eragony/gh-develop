<?php

namespace App\Structures\Dto\GH\Villes;

use App\Entity\Banque;
use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\Journal;
use App\Entity\PlansChantier;
use App\Entity\UpChantier;
use App\Structures\Dto\GH\Villes\Objets\CarteDto;
use App\Structures\Dto\GH\Villes\Objets\ResumeDto;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Attribute\Groups;

class ComparatifVillesDto
{
    #[Groups(['comparatif'])]
    private ?DateTimeImmutable $derniereMaj = null;
    
    #[Groups(['comparatif'])]
    private ResumeDto $resume;
    
    #[Groups(['comparatif'])]
    private Journal $gazette;
    
    /**
     * @var Collection<Citoyens>
     */
    #[Groups(['comparatif'])]
    private Collection $citoyens;
    
    /**
     * @var Collection<Banque>
     */
    #[Groups(['comparatif'])]
    private Collection $banques;
    
    /**
     * @var Collection<Chantiers>
     */
    #[Groups(['comparatif'])]
    private Collection $chantiers;
    
    /**
     * @var Collection<PlansChantier>
     */
    #[Groups(['comparatif'])]
    private Collection $plansChantiers;
    
    /**
     * @var Collection<UpChantier>
     */
    #[Groups(['comparatif'])]
    private Collection $upChantiers;
    
    #[Groups(['comparatif'])]
    private CarteDto $carte;
    
    #[Groups(['comparatif'])]
    private array $stats;
    
    public function __construct()
    {
        $this->resume         = new ResumeDto();
        $this->gazette        = new Journal(0);
        $this->citoyens       = new ArrayCollection();
        $this->banques        = new ArrayCollection();
        $this->chantiers      = new ArrayCollection();
        $this->plansChantiers = new ArrayCollection();
        $this->upChantiers    = new ArrayCollection();
        $this->carte          = new CarteDto();
    }
    
    public function addBanque(Banque $banque): self
    {
        if (!$this->banques->contains($banque)) {
            $this->banques[] = $banque;
        }
        
        return $this;
    }
    
    public function addChantier(Chantiers $chantier): self
    {
        if (!$this->chantiers->contains($chantier)) {
            $this->chantiers[] = $chantier;
        }
        
        return $this;
    }
    
    public function addCitoyen(Citoyens $citoyen): self
    {
        if (!$this->citoyens->contains($citoyen)) {
            $this->citoyens[] = $citoyen;
        }
        
        return $this;
    }
    
    public function addPlanChantier(PlansChantier $plansChantier): self
    {
        if (!$this->plansChantiers->contains($plansChantier)) {
            $this->plansChantiers[] = $plansChantier;
        }
        
        return $this;
    }
    
    public function addUpChantier(UpChantier $upChantier): self
    {
        if (!$this->upChantiers->contains($upChantier)) {
            $this->upChantiers[] = $upChantier;
        }
        
        return $this;
    }
    
    public function getBanques(): Collection
    {
        return $this->banques;
    }
    
    public function setBanques(Collection $banques): ComparatifVillesDto
    {
        $this->banques = $banques;
        return $this;
    }
    
    public function getCarte(): CarteDto
    {
        return $this->carte;
    }
    
    public function setCarte(CarteDto $carte): ComparatifVillesDto
    {
        $this->carte = $carte;
        return $this;
    }
    
    public function getChantiers(): Collection
    {
        return $this->chantiers;
    }
    
    public function setChantiers(Collection $chantiers): ComparatifVillesDto
    {
        $this->chantiers = $chantiers;
        return $this;
    }
    
    public function getCitoyens(): Collection
    {
        return $this->citoyens;
    }
    
    public function setCitoyens(Collection $citoyens): ComparatifVillesDto
    {
        $this->citoyens = $citoyens;
        return $this;
    }
    
    public function getDerniereMaj(): ?DateTimeImmutable
    {
        return $this->derniereMaj;
    }
    
    public function setDerniereMaj(?DateTimeImmutable $derniereMaj): ComparatifVillesDto
    {
        $this->derniereMaj = $derniereMaj;
        return $this;
    }
    
    public function getGazette(): Journal
    {
        return $this->gazette;
    }
    
    public function setGazette(Journal $gazette): ComparatifVillesDto
    {
        $this->gazette = $gazette;
        return $this;
    }
    
    public function getPlansChantiers(): Collection
    {
        return $this->plansChantiers;
    }
    
    public function setPlansChantiers(Collection $plansChantiers): ComparatifVillesDto
    {
        $this->plansChantiers = $plansChantiers;
        return $this;
    }
    
    public function getResume(): ResumeDto
    {
        return $this->resume;
    }
    
    public function setResume(ResumeDto $resume): ComparatifVillesDto
    {
        $this->resume = $resume;
        return $this;
    }
    
    public function getStats(): array
    {
        return $this->stats;
    }
    
    public function setStats(array $stats): ComparatifVillesDto
    {
        $this->stats = $stats;
        return $this;
    }
    
    public function getUpChantiers(): Collection
    {
        return $this->upChantiers;
    }
    
    public function setUpChantiers(Collection $upChantiers): ComparatifVillesDto
    {
        $this->upChantiers = $upChantiers;
        return $this;
    }
    
    public function removeBanque(Banque $banque): self
    {
        $this->banques->removeElement($banque);
        return $this;
    }
    
    public function removeChantier(Chantiers $chantier): self
    {
        $this->chantiers->removeElement($chantier);
        return $this;
    }
    
    public function removeCitoyen(Citoyens $citoyen): self
    {
        $this->citoyens->removeElement($citoyen);
        
        return $this;
    }
    
    public function removePlanChantier(PlansChantier $plansChantier): self
    {
        $this->plansChantiers->removeElement($plansChantier);
        return $this;
    }
    
    public function removeUpChantier(UpChantier $upChantier): self
    {
        $this->upChantiers->removeElement($upChantier);
        return $this;
    }
    
}