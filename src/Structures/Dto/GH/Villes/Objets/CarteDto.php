<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use App\Entity\ZoneMap;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Attribute\Groups;

class CarteDto
{
    #[Groups(['comparatif'])]
    private CarteDetailDto $detail;
    
    /**
     * @var Collection<ZoneMap>
     */
    #[Groups(['comparatif'])]
    private Collection $batiments;
    
    /**
     * @var Collection<ZoneMap>
     */
    #[Groups(['comparatif'])]
    private Collection $ruines;
    
    public function __construct()
    {
        $this->detail    = new CarteDetailDto();
        $this->batiments = new ArrayCollection();
        $this->ruines    = new ArrayCollection();
    }
    
    public function addBatiment(ZoneMap $batiment): CarteDto
    {
        $this->batiments[] = $batiment;
        return $this;
    }
    
    public function addRuine(ZoneMap $ruine): CarteDto
    {
        $this->ruines[] = $ruine;
        return $this;
    }
    
    public function getBatiments(): Collection
    {
        return $this->batiments;
    }
    
    public function setBatiments(Collection $batiments): CarteDto
    {
        $this->batiments = $batiments;
        return $this;
    }
    
    public function getDetail(): CarteDetailDto
    {
        return $this->detail;
    }
    
    public function setDetail(CarteDetailDto $detail): CarteDto
    {
        $this->detail = $detail;
        return $this;
    }
    
    public function getRuines(): Collection
    {
        return $this->ruines;
    }
    
    public function setRuines(Collection $ruines): CarteDto
    {
        $this->ruines = $ruines;
        return $this;
    }
    
    public function removeBatiment(ZoneMap $batiment): CarteDto
    {
        $this->batiments->removeElement($batiment);
        return $this;
    }
    
    public function removeRuine(ZoneMap $ruine): CarteDto
    {
        $this->ruines->removeElement($ruine);
        return $this;
    }
}