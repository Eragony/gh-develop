<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use Symfony\Component\Serializer\Attribute\Groups;

class RessourcesDto
{
    #[Groups(['comparatif'])]
    private int $nbBois = 0;
    
    #[Groups(['comparatif'])]
    private int $nbMetal = 0;
    
    #[Groups(['comparatif'])]
    private int $nbBeton = 0;
    
    #[Groups(['comparatif'])]
    private int $nbCompo = 0;
    
    #[Groups(['comparatif'])]
    private int $nbPve = 0;
    
    #[Groups(['comparatif'])]
    private int $nbTube = 0;
    
    #[Groups(['comparatif'])]
    private int $nbExplo = 0;
    
    #[Groups(['comparatif'])]
    private int $nbPiles = 0;
    
    #[Groups(['comparatif'])]
    private int $nbLentille = 0;
    
    #[Groups(['comparatif'])]
    private int $nbSac = 0;
    
    public function getNbBeton(): int
    {
        return $this->nbBeton;
    }
    
    public function setNbBeton(int $nbBeton): RessourcesDto
    {
        $this->nbBeton = $nbBeton;
        return $this;
    }
    
    public function getNbBois(): int
    {
        return $this->nbBois;
    }
    
    public function setNbBois(int $nbBois): RessourcesDto
    {
        $this->nbBois = $nbBois;
        return $this;
    }
    
    public function getNbCompo(): int
    {
        return $this->nbCompo;
    }
    
    public function setNbCompo(int $nbCompo): RessourcesDto
    {
        $this->nbCompo = $nbCompo;
        return $this;
    }
    
    public function getNbExplo(): int
    {
        return $this->nbExplo;
    }
    
    public function setNbExplo(int $nbExplo): RessourcesDto
    {
        $this->nbExplo = $nbExplo;
        return $this;
    }
    
    public function getNbLentille(): int
    {
        return $this->nbLentille;
    }
    
    public function setNbLentille(int $nbLentille): RessourcesDto
    {
        $this->nbLentille = $nbLentille;
        return $this;
    }
    
    public function getNbMetal(): int
    {
        return $this->nbMetal;
    }
    
    public function setNbMetal(int $nbMetal): RessourcesDto
    {
        $this->nbMetal = $nbMetal;
        return $this;
    }
    
    public function getNbPiles(): int
    {
        return $this->nbPiles;
    }
    
    public function setNbPiles(int $nbPiles): RessourcesDto
    {
        $this->nbPiles = $nbPiles;
        return $this;
    }
    
    public function getNbPve(): int
    {
        return $this->nbPve;
    }
    
    public function setNbPve(int $nbPve): RessourcesDto
    {
        $this->nbPve = $nbPve;
        return $this;
    }
    
    public function getNbSac(): int
    {
        return $this->nbSac;
    }
    
    public function setNbSac(int $nbSac): RessourcesDto
    {
        $this->nbSac = $nbSac;
        return $this;
    }
    
    public function getNbTube(): int
    {
        return $this->nbTube;
    }
    
    public function setNbTube(int $nbTube): RessourcesDto
    {
        $this->nbTube = $nbTube;
        return $this;
    }
}