<?php

namespace App\Structures\Dto\GH\RuineGame;

class KillZombieRuineGame
{
    private int           $idUser;
    private CaseRuineGame $case;
    private string        $idRuineGame;
    
    /**
     * @return CaseRuineGame
     */
    public function getCase(): CaseRuineGame
    {
        return $this->case;
    }
    
    /**
     * @param CaseRuineGame $case
     * @return KillZombieRuineGame
     */
    public function setCase(CaseRuineGame $case): KillZombieRuineGame
    {
        $this->case = $case;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return KillZombieRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): KillZombieRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return KillZombieRuineGame
     */
    public function setIdUser(int $idUser): KillZombieRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
}