<?php

namespace App\Structures\Dto\GH\RuineGame;

class NiveauDifficulte
{
    public const O2_ULTRA_FACILE = 230;
    public const O2_TRES_FACILE  = 200;
    public const O2_FACILE       = 150;
    public const O2_MOYEN        = 100;
    public const O2_DIFFICILE    = 70;
    public const MAX_ETAGE       = 3;
    public const MIN_ETAGE       = 1;
    public const PLAN_COMPLET    = 0;
    public const PLAN_TRACE      = 1;
    public const SANS_PLAN       = 2;
    
    private int $nombreEtage;
    private int $oxygene;
    private int $typePlan;
    private int $nbZombie;
    
    /**
     * @return int
     */
    public function getNbZombie(): int
    {
        return $this->nbZombie;
    }
    
    /**
     * @param int $nbZombie
     * @return NiveauDifficulte
     */
    public function setNbZombie(int $nbZombie): NiveauDifficulte
    {
        $this->nbZombie = $nbZombie;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getNombreEtage(): int
    {
        return $this->nombreEtage;
    }
    
    /**
     * @param int $nombreEtage
     * @return NiveauDifficulte
     */
    public function setNombreEtage(int $nombreEtage): NiveauDifficulte
    {
        $this->nombreEtage = $nombreEtage;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getOxygene(): int
    {
        return $this->oxygene;
    }
    
    /**
     * @param int $oxygene
     * @return NiveauDifficulte
     */
    public function setOxygene(int $oxygene): NiveauDifficulte
    {
        $this->oxygene = $oxygene;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getTypePlan(): int
    {
        return $this->typePlan;
    }
    
    /**
     * @param int $typePlan
     * @return NiveauDifficulte
     */
    public function setTypePlan(int $typePlan): NiveauDifficulte
    {
        $this->typePlan = $typePlan;
        return $this;
    }
    
}