<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

class DeplacementRuineResponse
{
    private int $perteOxy;
    private int $oxygenLostTotal;
    
    /**
     * @return int
     */
    public function getOxygenLostTotal(): int
    {
        return $this->oxygenLostTotal;
    }
    
    /**
     * @param int $oxygenLostTotal
     * @return DeplacementRuineResponse
     */
    public function setOxygenLostTotal(int $oxygenLostTotal): DeplacementRuineResponse
    {
        $this->oxygenLostTotal = $oxygenLostTotal;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPerteOxy(): int
    {
        return $this->perteOxy;
    }
    
    /**
     * @param int $perteOxygene
     * @return DeplacementRuineResponse
     */
    public function setPerteOxy(int $perteOxygene): DeplacementRuineResponse
    {
        $this->perteOxy = $perteOxygene;
        return $this;
    }
}