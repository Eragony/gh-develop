<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

use App\Entity\RuineGame;
use App\Structures\Dto\GH\RuineGame\CaseRuineGame;
use DateTimeImmutable;
use Symfony\Component\Serializer\Attribute\Groups;

class RuineMazeResponse
{
    /** @var CaseRuineGame[] $ruineMaze */
    #[Groups(['ruine'])]
    private ?array $ruineMaze = null;
    
    
    /** @var CaseRuineGame[] $ruineMazeVide */
    #[Groups(['ruine'])]
    private ?array $ruineMazeVide = null;
    
    #[Groups(['ruine'])]
    private ?RuineGame $game = null;
    
    /**
     * @return DateTimeImmutable
     */
    public function getEndGame(): ?DateTimeImmutable
    {
        return $this->endGame;
    }
    
    /**
     * @return RuineGame
     */
    public function getGame(): ?RuineGame
    {
        return $this->game;
    }
    
    /**
     * @param RuineGame $game
     * @return RuineMazeResponse
     */
    public function setGame(?RuineGame $game): RuineMazeResponse
    {
        $this->game = $game;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getRuineMaze(): ?array
    {
        return $this->ruineMaze;
    }
    
    /**
     * @param array $ruineMaze
     * @return RuineMazeResponse
     */
    public function setRuineMaze(?array $ruineMaze): RuineMazeResponse
    {
        $this->ruineMaze = $ruineMaze;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getRuineMazeVide(): ?array
    {
        return $this->ruineMazeVide;
    }
    
    /**
     * @param array $ruineMazeVide
     * @return RuineMazeResponse
     */
    public function setRuineMazeVide(?array $ruineMazeVide): RuineMazeResponse
    {
        $this->ruineMazeVide = $ruineMazeVide;
        return $this;
    }
    
    /**
     * @return DateTimeImmutable
     */
    public function getStartGame(): ?DateTimeImmutable
    {
        return $this->startGame;
    }
    
    /**
     * @param DateTimeImmutable $endGame
     * @return RuineMazeResponse
     */
    public function setEndGame(?DateTimeImmutable $endGame): RuineMazeResponse
    {
        $this->endGame = $endGame;
        return $this;
    }
    
    /**
     * @param DateTimeImmutable $startGame
     * @return RuineMazeResponse
     */
    public function setStartGame(?DateTimeImmutable $startGame): RuineMazeResponse
    {
        $this->startGame = $startGame;
        return $this;
    }
    
}