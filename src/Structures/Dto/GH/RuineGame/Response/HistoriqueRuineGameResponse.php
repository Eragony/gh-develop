<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

use App\Entity\RuineGame;
use Symfony\Component\Serializer\Attribute\Groups;

class HistoriqueRuineGameResponse
{
    /** @var RuineGame[] $listHistorique */
    #[Groups(['ruine', 'admin_ruine'])]
    private array $listHistorique = [];
    
    /** @var string[] $traductionRuineGame */
    #[Groups(['ruine', 'admin_ruine'])]
    private array $traductionRuineGame = [];
    
    /**
     * @return array
     */
    public function getListHistorique(): array
    {
        return $this->listHistorique;
    }
    
    /**
     * @param array $listHistorique
     * @return HistoriqueRuineGameResponse
     */
    public function setListHistorique(array $listHistorique): HistoriqueRuineGameResponse
    {
        $this->listHistorique = $listHistorique;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getTraductionRuineGame(): array
    {
        return $this->traductionRuineGame;
    }
    
    /**
     * @param array $traductionRuineGame
     * @return HistoriqueRuineGameResponse
     */
    public function setTraductionRuineGame(array $traductionRuineGame): HistoriqueRuineGameResponse
    {
        $this->traductionRuineGame = $traductionRuineGame;
        return $this;
    }
    
}