<?php

namespace App\Structures\Dto\GH\RuineGame;

class FuiteRuineGame
{
    private int           $idUser;
    private CaseRuineGame $case;
    private string        $idRuineGame;
    private bool          $caseFuite;
    
    /**
     * @return CaseRuineGame
     */
    public function getCase(): CaseRuineGame
    {
        return $this->case;
    }
    
    /**
     * @param CaseRuineGame $case
     * @return DeplacementRuineGame
     */
    public function setCase(CaseRuineGame $case): FuiteRuineGame
    {
        $this->case = $case;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return DeplacementRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): FuiteRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return DeplacementRuineGame
     */
    public function setIdUser(int $idUser): FuiteRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isCaseFuite(): bool
    {
        return $this->caseFuite;
    }
    
    /**
     * @param bool $caseFuite
     * @return DeplacementRuineGame
     */
    public function setCaseFuite(bool $caseFuite): FuiteRuineGame
    {
        $this->caseFuite = $caseFuite;
        return $this;
    }
}