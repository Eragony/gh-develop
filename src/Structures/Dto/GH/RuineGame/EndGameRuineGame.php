<?php

namespace App\Structures\Dto\GH\RuineGame;

class EndGameRuineGame
{
    private int $idUser;
    
    /** @var CaseRuineGame[][][] $planRuine */
    private array $planRuine = [];
    
    private string $idRuineGame;
    
    private int $oxygene;
    
    private int $mana;
    
    private bool $ejecter;
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return EndGameRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): EndGameRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return EndGameRuineGame
     */
    public function setIdUser(int $idUser): EndGameRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getMana(): int
    {
        return $this->mana;
    }
    
    /**
     * @param int $mana
     * @return EndGameRuineGame
     */
    public function setMana(int $mana): EndGameRuineGame
    {
        $this->mana = $mana;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getOxygene(): int
    {
        return $this->oxygene;
    }
    
    /**
     * @param int $oxygene
     * @return EndGameRuineGame
     */
    public function setOxygene(int $oxygene): EndGameRuineGame
    {
        $this->oxygene = $oxygene;
        return $this;
    }
    
    /**
     * @return CaseRuineGame[][][]
     */
    public function getPlanRuine(): array
    {
        return $this->planRuine;
    }
    
    /**
     * @param CaseRuineGame[][][] $planRuine
     * @return EndGameRuineGame
     */
    public function setPlanRuine(array $planRuine): EndGameRuineGame
    {
        $this->planRuine = $planRuine;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isEjecter(): bool
    {
        return $this->ejecter;
    }
    
    /**
     * @param bool $ejecter
     * @return EndGameRuineGame
     */
    public function setEjecter(bool $ejecter): EndGameRuineGame
    {
        $this->ejecter = $ejecter;
        return $this;
    }
    
}