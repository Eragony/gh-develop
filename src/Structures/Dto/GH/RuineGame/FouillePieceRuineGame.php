<?php

namespace App\Structures\Dto\GH\RuineGame;

class FouillePieceRuineGame
{
    private int           $idUser;
    private CaseRuineGame $case;
    private string        $idRuineGame;
    
    /**
     * @return CaseRuineGame
     */
    public function getCase(): CaseRuineGame
    {
        return $this->case;
    }
    
    /**
     * @param CaseRuineGame $case
     * @return FouillePieceRuineGame
     */
    public function setCase(CaseRuineGame $case): FouillePieceRuineGame
    {
        $this->case = $case;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return FouillePieceRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): FouillePieceRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return FouillePieceRuineGame
     */
    public function setIdUser(int $idUser): FouillePieceRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
}