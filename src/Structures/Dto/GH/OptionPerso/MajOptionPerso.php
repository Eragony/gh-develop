<?php

namespace App\Structures\Dto\GH\OptionPerso;

use App\Entity\User;
use Symfony\Component\Serializer\Attribute\Groups;

class MajOptionPerso
{
    #[Groups(['option'])]
    private User $user;
    
    #[Groups(['option'])]
    private int $themeId;
    
    /**
     * @return int
     */
    public function getThemeId(): int
    {
        return $this->themeId;
    }
    
    /**
     * @param int $themeId
     * @return MajOptionPerso
     */
    public function setThemeId(int $themeId): MajOptionPerso
    {
        $this->themeId = $themeId;
        return $this;
    }
    
    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
    
    /**
     * @return MajOptionPerso
     */
    public function setUser(User $user): MajOptionPerso
    {
        $this->user = $user;
        return $this;
    }
    
}