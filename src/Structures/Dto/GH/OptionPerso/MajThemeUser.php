<?php

namespace App\Structures\Dto\GH\OptionPerso;

use App\Entity\ThemeUser;
use Symfony\Component\Serializer\Attribute\Groups;

class MajThemeUser
{
    #[Groups(['option'])]
    private int $userId;
    
    #[Groups(['option'])]
    private ThemeUser $themeUser;
    
    #[Groups(['option'])]
    private bool $themeToSelect;
    
    /**
     * @return ThemeUser
     */
    public function getThemeUser(): ThemeUser
    {
        return $this->themeUser;
    }
    
    /**
     * @param ThemeUser $themeUser
     * @return MajThemeUser
     */
    public function setThemeUser(ThemeUser $themeUser): MajThemeUser
    {
        $this->themeUser = $themeUser;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @param int $userId
     * @return MajThemeUser
     */
    public function setUserId(int $userId): MajThemeUser
    {
        $this->userId = $userId;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isThemeToSelect(): bool
    {
        return $this->themeToSelect;
    }
    
    /**
     * @param bool $themeToSelect
     * @return MajThemeUser
     */
    public function setThemeToSelect(bool $themeToSelect): MajThemeUser
    {
        $this->themeToSelect = $themeToSelect;
        return $this;
    }
    
    
}