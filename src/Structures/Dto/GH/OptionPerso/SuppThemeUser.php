<?php

namespace App\Structures\Dto\GH\OptionPerso;

use Symfony\Component\Serializer\Attribute\Groups;

class SuppThemeUser
{
    #[Groups(['option'])]
    private int $userId;
    
    #[Groups(['option'])]
    private int $themeUserId;
    
    /**
     * @return int
     */
    public function getThemeUserId(): int
    {
        return $this->themeUserId;
    }
    
    /**
     * @param int $themeUserId
     * @return SuppThemeUser
     */
    public function setThemeUserId(int $themeUserId): SuppThemeUser
    {
        $this->themeUserId = $themeUserId;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @param int $userId
     * @return SuppThemeUser
     */
    public function setUserId(int $userId): SuppThemeUser
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}