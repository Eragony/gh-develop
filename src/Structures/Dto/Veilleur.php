<?php


namespace App\Structures\Dto;


use App\Entity\Citoyens;
use App\Entity\HistoriqueVeille;
use Doctrine\Common\Collections\ArrayCollection;

class Veilleur
{
    public Citoyens $citoyens;
    /**
     * @var ArrayCollection|HistoriqueVeille[]
     */
    public ArrayCollection|array $historiques;
    
    public function __construct()
    {
        $this->historiques = new ArrayCollection();
    }
    
    public function addHistorique(HistoriqueVeille $historiqueVeille): Veilleur
    {
        if (!$this->historiques->contains($historiqueVeille)) {
            $this->historiques[$historiqueVeille->getJour()] = $historiqueVeille;
        }
        
        return $this;
    }
    
    /**
     * @return Citoyens
     */
    public function getCitoyens(): Citoyens
    {
        return $this->citoyens;
    }
    
    /**
     * @return Veilleur
     */
    public function setCitoyens(Citoyens $citoyens): Veilleur
    {
        $this->citoyens = $citoyens;
        
        return $this;
    }
    
    /**
     * @return HistoriqueVeille|null
     */
    public function getHistorique(int $jour): ?HistoriqueVeille
    {
        return $this->historiques[$jour] ?? null;
    }
    
    /**
     * @return array|ArrayCollection
     */
    public function getHistoriques(): ArrayCollection|array
    {
        return $this->historiques;
    }
    
    /**
     * @param array|ArrayCollection $historiques
     * @return Veilleur
     */
    public function setHistoriques(ArrayCollection|array $historiques): Veilleur
    {
        
        foreach ($historiques as $historique) {
            $this->addHistorique($historique);
        }
        
        return $this;
    }
    
    public function removeHistorique(HistoriqueVeille $historiqueVeille): Veilleur
    {
        $this->historiques->removeElement($historiqueVeille);
        
        return $this;
    }
    
}