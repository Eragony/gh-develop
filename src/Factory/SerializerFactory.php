<?php


namespace App\Factory;


use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerFactory
{
    public function create(): SerializerInterface
    {
        $encoder              = new JsonEncoder();
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $nameConverter        = new CamelCaseToSnakeCaseNameConverter();
        
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                if ($object instanceof ChantierPrototype) {
                    return $object->getId();
                } elseif ($object instanceof ItemPrototype) {
                    return $object->getId();
                } elseif ($object instanceof BatPrototype) {
                    return $object->getId();
                } else {
                    return $object->getId();
                }
            },
        ];
        
        $normaliser = new ObjectNormalizer(
            $classMetadataFactory,
            $nameConverter,
            null,
            new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]),
            null,
            null,
            $defaultContext,
        );
        
        return new Serializer([new BackedEnumNormalizer(), new DateTimeNormalizer(), $normaliser, new  GetSetMethodNormalizer(), new ArrayDenormalizer()], [$encoder]);
    }
}