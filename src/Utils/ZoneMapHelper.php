<?php


namespace App\Utils;

use App\Entity\Ville;
use App\Entity\ZoneMap;

class ZoneMapHelper
{
    
    /**
     * Calcul de l'estimation de zombie
     * Si carte amélioré est construite, toute la zone de TDG n'est alors pas recalculé, on reprend directement les zombies.
     * Sinon, on calcule 1zombie par jour de différence par rapport à la dernière mise à jour.
     * Si on a le niveau de danger de la case, borne le danger au nombre max de zombies pour ce niveau
     */
    public function getEstimZombie(ZoneMap $zoneMap, Ville $ville, ?bool $carteAmelioConstruit = false,
                                   ?int    $lvlTDG = 0): ?int
    {
        $km = $zoneMap->getKm() ?? 0;
        if ($zoneMap->getVue() !== ZoneMap::NON_EXPLO && $zoneMap->getVue() !== ZoneMap::VILLE) {
            if ($carteAmelioConstruit && $lvlTDG == 1 && $km <= 3) {
                return $zoneMap->getZombie();
            } elseif ($carteAmelioConstruit && $lvlTDG == 2 && $km <= 6) {
                return $zoneMap->getZombie();
            } elseif ($carteAmelioConstruit && $lvlTDG >= 3 && $km <= 10) {
                return $zoneMap->getZombie();
            } elseif ($zoneMap->getVue() === ZoneMap::CASE_VUE) {
                $maxZombiesParDanger = [0, 2, 5, 400];
                $danger              = $zoneMap->getDanger() ?? 3;
                return min(($zoneMap->getZombie() ?? 0) + abs($ville->getJour() - ($zoneMap->getDay() ?? 1)),
                           $maxZombiesParDanger[$danger]);
            } else {
                return ($zoneMap->getZombie() ?? 0) + abs($ville->getJour() - ($zoneMap->getDay() ?? 1));
            }
            
        } else {
            return null;
        }
    }
}
