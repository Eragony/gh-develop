<?php

namespace App\Controller\Api\Ville;

use App\Controller\AbstractApiGestHordesController;
use App\Entity\ItemPrototype;
use App\Entity\MapItem;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Structures\Dto\Api\Autre\Ville\CaseMajApi;
use App\Structures\Dto\Api\Autre\Ville\MapItemApi;
use App\Structures\Dto\Api\Autre\Ville\MarqueurMajApi;
use DateTime;
use DateTimeImmutable;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;


class CarteApiController extends AbstractApiGestHordesController
{
    #[Route(path: '/api/majCase', name: 'api_carte_maj_case', methods: ['POST'])]
    public function majCase(Request $request): JsonResponse
    {
        
        $caseMaj = new CaseMajApi(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));
        
        //$this->logger->error("MajCase : " . $request->getContent());
        
        // Récupération utilisateur
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['apiToken' => $caseMaj->getKey()]);
        
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("L'utilisateur est inconnu. Code erreur :{code}", ['{code}' => $codeErreur], 'app',);
            $this->logger->error("$codeErreur - La clef user n'existe pas dans la base de donnée : {$caseMaj->getKey()}");
            
            return new JsonResponse($retour);
        }
        
        // Récupération ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $caseMaj->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "La ville n'existe pas. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : {$caseMaj->getMapId()}.");
            
            return new JsonResponse($retour);
        }
        if ($caseMaj->getX() === null || $caseMaj->getY() === null || $caseMaj->getX() < 0 ||
            $caseMaj->getX() > $ville->getWeight() || $caseMaj->getY() < 0 ||
            $caseMaj->getY() > $ville->getHeight()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Les coordonnées fournis sont erronées. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Les coordonnées sont incorrectes : x - {$caseMaj->getX()}, y - {$caseMaj->getY()}.");
            
            return new JsonResponse($retour);
        }
        
        if ($caseMaj->getNbrZombie() === null) {
            $nbrZombie = null;
            $danger    = null;
        } else {
            $nbrZombie = $caseMaj->getNbrZombie();
            $danger    = ($nbrZombie === 0) ? 0 :
                (($nbrZombie >= 1 && $nbrZombie <= 2) ? 1 : (($nbrZombie >= 3 && $nbrZombie < 5) ? 2 : 3));
        }
        
        $dried = $caseMaj->getEpuise() === 1 ?? false;
        
        $objet = $caseMaj->getItems() ?? [];
        
        
        // création d'un tableau d'objet à traiter.
        $idTab =
            array_combine(array_map(fn(MapItemApi $i) => $i->getId() * 10 + (((int)$i->getType() === 2) ? 1 : 0),
                $objet),  $objet);
        
        // récupération de la zone :
        $zone = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['x'     => $caseMaj->getX(),
                                                                                'y'     => $caseMaj->getY(),
                                                                                'ville' => $ville]);
        
        foreach ($zone->getItems() as $itemZone) {
            $item_id = $itemZone->getItem()->getIdMh() * 10 + ($itemZone->getBroked() ? 1 : 0);
            
            if (isset($idTab[$item_id])) {
                $itemZone->setNombre((int)$idTab[$item_id]->getNombre());
                unset($idTab[$item_id]);
            } else {
                $zone->removeItem($itemZone);
            }
            
        }
        
        foreach ($idTab as $itemMaj) {
            $itemProto = $this->entityManager->getRepository(ItemPrototype::class)
                                             ->findOneBy(['idMh' => $itemMaj->getId()]);
            
            $itemZone = new MapItem($itemProto, (int)$itemMaj->getType() === 2);
            $itemZone->setNombre((int)$itemMaj->getNombre());
            
            $zone->addItem($itemZone);
        }
        
        // Mise à jour du reste de la zone
        $zone->setZombie($nbrZombie)
             ->setDried($dried)
             ->setDanger($danger)
             ->setDay($ville->getJour())
             ->setVue(ZoneMap::CASE_VUE)
             ->setCitoyen($user)
             ->setMajAt(new DateTimeImmutable('now'))
             ->setHeureMaj((new DateTime())->format('H:i:s'));
        
        
        try {
            $this->entityManager->persist($zone);
            $this->entityManager->flush();
            $this->codeRetour = 0;
            $this->libRetour  = $this->translator->trans("Mise à jour effectuée", [], 'app');
            
            return $this->returnJson();
        } catch (Exception $exception) {
            $codeErreur = $this->randomString();
            
            $this->codeRetour = 1;
            $this->libRetour  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Probleme mise à jour Case id {$zone->getId()} : " .
                                 $exception->getMessage());
            
            return $this->returnJson();
        }
        
    }
    
    #[Route(path: '/api/majZombieTue', name: 'api_carte_maj_zombie', methods: ['POST'])]
    public function majZombieTue(Request $request): JsonResponse
    {
        
        $caseMaj = new MarqueurMajApi(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));
        
        // Récupération utilisateur
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['apiToken' => $caseMaj->getKey()]);
        
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "L'utilisateur est inconnu. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - La clef user n'existe pas dans la base de donnée : {$caseMaj->getKey()}");
            
            return new JsonResponse($retour);
        }
        
        // Récupération ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $caseMaj->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "La ville n'existe pas. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : {$caseMaj->getMapId()}.");
            
            return new JsonResponse($retour);
        }
        if ($caseMaj->getX() === null || $caseMaj->getY() === null || $caseMaj->getX() < 0 ||
            $caseMaj->getX() > $ville->getWeight() || $caseMaj->getY() < 0 ||
            $caseMaj->getY() > $ville->getHeight()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Les coordonnées fournis sont erronées. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Les coordonnées sont incorrectes : x - {$caseMaj->getX()}, y - {$caseMaj->getY()}.");
            
            return new JsonResponse($retour);
        }
        
        // création d'un tableau d'objet à traiter.
        $itemKill = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => 5004]);
        
        
        // récupération de la zone :
        $zone         = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['x'     => $caseMaj->getX(),
                                                                                        'y'     => $caseMaj->getY(),
                                                                                        'ville' => $ville]);
        $itemExistant = false;
        foreach ($zone->getItems() as $itemZone) {
            if ($itemZone->getItem()->getId() === $itemKill->getId()) {
                $itemExistant = true;
                if ($caseMaj->getKill() > 0) {
                    $itemZone->setNombre($caseMaj->getKill());
                } else {
                    $zone->removeItem($itemZone);
                }
            }
            
        }
        
        if (!$itemExistant && $caseMaj->getKill() > 0) {
            $itemZone = new MapItem($itemKill, false);
            $itemZone->setNombre($caseMaj->getKill());
            
            $zone->addItem($itemZone);
        }
        
        
        // Mise à jour du reste de la zone
        $zone->setDay($ville->getJour())
             ->setMajAt(new DateTimeImmutable('now'))
             ->setVue(ZoneMap::CASE_VUE)
             ->setCitoyen($user)
             ->setHeureMaj((new DateTime())->format('H:i:s'));
        
        
        try {
            $this->entityManager->persist($zone);
            $this->entityManager->flush();
            $this->codeRetour = 0;
            $this->libRetour  = $this->translator->trans("Mise à jour effectuée", [], 'app');
            
            return $this->returnJson();
        } catch
        (Exception $exception) {
            $codeErreur = $this->randomString();
            
            $this->codeRetour = 1;
            $this->libRetour  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Probleme mise à jour Case id {$zone->getId()} : " .
                                 $exception->getMessage());
            
            return $this->returnJson();
        }
        
    }
}