<?php


namespace App\Controller\Api\Hotel;


use App\Controller\AbstractApiGestHordesController;
use App\Entity\Citoyens;
use App\Entity\Ville;
use App\Structures\Dto\Api\Autre\Hotel\CitoyensMaj;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


class CitoyensApiController extends AbstractApiGestHordesController
{
    
    
    /**
     * @return JsonResponse
     */
    #[Route('/api/citoyens/maj', name: 'api_majcitoyens', methods: ['POST'])]
    public function majCitoyens(Request $request): JsonResponse
    {
        $citoyensMaj = new CitoyensMaj(json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR));
        
        $user = $this->checkUser($citoyensMaj->getId(), $citoyensMaj->getKey());
        
        if ($user === null) {
            return new JsonResponse(['lib' => $this->translator->trans("Utilisateur inexistant ou problème entre l'id et la userkey",
                                                                       [], 'hotel')], Response::HTTP_UNAUTHORIZED);
        }
        
        // récupération du citoyens ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['id' => $user->getMapId() * 10 +
                                                                                       Ville::ORIGIN_MH]);
        
        if ($ville === null) {
            return new JsonResponse(['lib' => $this->translator->trans("Ville inconnu, impossible de mettre à jour.",
                                                                       [], 'hotel')], Response::HTTP_UNAUTHORIZED);
        }
        
        $citoyens =
            $this->entityManager->getRepository(Citoyens::class)->findOneBy(['citoyen' => $user, 'ville' => $ville]);
        
        if ($citoyens === null) {
            return new JsonResponse(['lib' => $this->translator->trans("Citoyens introuvable, impossible de mettre à jour.",
                                                                       [], 'hotel')], Response::HTTP_UNAUTHORIZED);
        }
        
        $miseAjour = false;
        
        /**
         * maj des pouvoirs (le controle est directement effectué au sain de la classe
         */
        if ($citoyens->getRdh()) {
            if ($citoyensMaj?->getPouvoir()?->getRdh() !== null) {
                $citoyens->setRdh($citoyensMaj->getPouvoir()->getRdh());
                $miseAjour = true;
            }
        }
        if ($citoyens->getUppercut()) {
            if ($citoyensMaj?->getPouvoir()?->getUs() !== null) {
                $citoyens->setUppercut($citoyensMaj->getPouvoir()->getUs());
                $miseAjour = true;
            }
        }
        if ($citoyens->getSauvetage()) {
            if ($citoyensMaj?->getPouvoir()?->getSauvetage() !== null) {
                $citoyens->setSauvetage($citoyensMaj->getPouvoir()->getSauvetage());
                $miseAjour = true;
            }
        }
        if ($citoyens->getDonJh()) {
            if ($citoyensMaj?->getPouvoir()?->getDonJH() !== null) {
                $citoyens->setDonJh($citoyensMaj->getPouvoir()->getDonJH());
                $miseAjour = true;
            }
        }
        if ($citoyens->getPef()) {
            if ($citoyensMaj?->getPouvoir()?->getPef() !== null) {
                $citoyens->setPef($citoyensMaj->getPouvoir()->getPef());
                $miseAjour = true;
            }
        }
        if ($citoyens->getTrouvaille()) {
            if ($citoyensMaj?->getPouvoir()?->getTrouvaille() !== null) {
                $citoyens->setTrouvaille($citoyensMaj->getPouvoir()->getTrouvaille());
                $miseAjour = true;
            }
        }
        
        if ($citoyens->getCorpsSain()) {
            if ($citoyensMaj?->getPouvoir()?->getCorpsSain() !== null) {
                $citoyens->setCorpsSain($citoyensMaj->getPouvoir()->getCorpsSain());
                $miseAjour = true;
            }
        }
        if ($citoyens->getSecondSouffle()) {
            if ($citoyensMaj?->getPouvoir()?->getSecondSouffle() !== null) {
                $citoyens->setSecondSouffle($citoyensMaj->getPouvoir()->getSecondSouffle());
                $miseAjour = true;
            }
        }
        if ($citoyens->getVlm()) {
            if ($citoyensMaj?->getPouvoir()?->getVlm() !== null) {
                $citoyens->setVlm($citoyensMaj->getPouvoir()->getVlm());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getPouvoir()?->getApag() !== null) {
            if ($citoyens->getChargeApag() > $citoyensMaj->getPouvoir()->getApag()) {
                $citoyens->setChargeApag($citoyensMaj->getPouvoir()->getApag());
                $miseAjour = true;
            }
        }
        
        /**
         * Mise à jour des Habitations
         */
        
        if ($citoyensMaj?->getHabitation()?->getCuisine() !== null) {
            if ($citoyens->getLvlCuisine() < $citoyensMaj->getHabitation()->getCuisine()) {
                $citoyens->setLvlCuisine($citoyensMaj->getHabitation()->getCuisine());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getHabitation()?->getLabo() !== null) {
            if ($citoyens->getLvlLabo() < $citoyensMaj->getHabitation()->getLabo()) {
                $citoyens->setLvlLabo($citoyensMaj->getHabitation()->getLabo());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getHabitation()?->getRangement() !== null) {
            if ($citoyens->getLvlRangement() < $citoyensMaj->getHabitation()->getRangement()) {
                $citoyens->setLvlRangement($citoyensMaj->getHabitation()->getRangement());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getHabitation()?->getRenfort() !== null) {
            if ($citoyens->getLvlCuisine() < $citoyensMaj->getHabitation()->getRenfort()) {
                $citoyens->setLvlRenfort($citoyensMaj->getHabitation()->getRenfort());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getHabitation()?->getCs() !== null) {
            if ($citoyens->getLvlCoinSieste() < $citoyensMaj->getHabitation()->getCs()) {
                $citoyens->setLvlCoinSieste($citoyensMaj->getHabitation()->getCs());
                $miseAjour = true;
            }
        }
        
        if ($citoyensMaj?->getHabitation()?->getCloture() !== null) {
            if (!$citoyens->getCloture()) {
                $citoyens->setCloture($citoyensMaj->getHabitation()->getCloture());
                $miseAjour = true;
            }
        }
        
        if (!$miseAjour) {
            return new JsonResponse(['lib' => $this->translator->trans("Tout est déjà correctement à jour.", [],
                                                                       'hotel')], Response::HTTP_OK);
        } else {
            
            try {
                
                $citoyens->setDateMaj(new DateTime('now'))
                         ->setUpdateBy($user);
                
                $this->entityManager->persist($citoyens);
                $this->entityManager->flush();
                
                return new JsonResponse(['lib' => $this->translator->trans("Mise à jour des informations citoyens ok.",
                                                                           [], 'hotel')], Response::HTTP_OK);
            } catch (Exception $exception) {
                
                $this->logger->error("Problème mise à jour via API - User : {$user->getId()} / {$user->getPseudo()} - Erreur : {$exception->getMessage()}");
                
                return new JsonResponse(['lib' => $this->translator->trans("Utilisateur inexistant ou problème entre l'id et la userkey",
                                                                           [], 'hotel')],
                                        Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            
        }
        
        
    }
    
    
}