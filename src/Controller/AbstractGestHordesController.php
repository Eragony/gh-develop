<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Ville;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractGestHordesController extends AbstractController
{
    
    public ?Ville $ville;
    
    public ?User $user;
    
    public Serializer $serializer;
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected RequestStack           $requestStack,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
    )
    {
        
        $this->user = $this->security->getUser();
        
        $this->ville = $this->userHandler->getTownBySession();
        
        $encoder              = new JsonEncoder();
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $nameConverter        = new CamelCaseToSnakeCaseNameConverter();
        
        $defaultContext   = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => fn($object, $format, $context) => $object->getId(),
        ];
        $normaliser       =
            new ObjectNormalizer($classMetadataFactory, $nameConverter, null, null, null, null, $defaultContext);
        $this->serializer = new Serializer([new DateTimeNormalizer(), $normaliser], [$encoder]);
        
        
    }
    
    public function randomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string     = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[random_int(0, strlen($characters) - 1)];
        }
        
        return $string;
    }
}