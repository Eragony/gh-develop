<?php

namespace App\Controller;


use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\LocalFormatterUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class GeneralController extends AbstractGestHordesController
{
    public function __construct(
        protected EntityManagerInterface     $entityManager,
        protected RequestStack               $requestStack,
        protected UserHandler                $userHandler,
        protected GestHordesHandler          $gh,
        protected Security                   $security,
        protected TranslatorInterface        $translator,
        protected LoggerInterface            $logger,
        protected TranslateHandler           $translateHandler,
        private readonly LocalFormatterUtils $lfu,
    )
    {
        parent::__construct($entityManager, $this->requestStack, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler);
    }
    
    /**
     * @return RedirectResponse
     */
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        // Récupération du theme de l'utilisateur
        $listTheme = $this->user?->getThemesUser() ?? new ArrayCollection([]);
        // Si l'utilisateur n'a pas de thème, on définit un tableau vide
        if ($listTheme->count() === 0) {
            $themeSeleted = null;
        } else {
            // On recherche le theme sélectionné
            foreach ($listTheme as $theme) {
                if ($theme->isSelected()) {
                    $themeSeleted = $theme;
                    break;
                }
            }
        }
        
        return $this->render('defaut_react.html.twig', [
            'themeUser' => $themeSeleted ?? null,
        ]);
    }
    
    /**
     *
     * @param Request $request
     * @param string $lang
     * @return RedirectResponse
     */
    #[Route('/majLang/{lang}', name: 'maj_lang', requirements: ['lang' => '[a-z]{2}'])]
    public function majLang(Request $request, string $lang): RedirectResponse
    {
        
        $url      = $_SERVER['HTTP_REFERER'] ?? $this->generateUrl('index');
        $response = new RedirectResponse($url);
        
        switch ($lang) {
            case 'en':
            case 'fr':
            case 'de':
            case 'es':
                $this->userHandler->sauvegardeLang($this->getUser(), $lang);
                $request->getSession()->set('_locale', $lang);
                $request->cookies->set('_locale', $lang);
                
                $response->headers->setCookie(new Cookie('_locale', $lang));
                
                $request->setDefaultLocale($lang);
                break;
        }
        
        return $response;
    }
    
    public function react(): Response
    {
        // Récupération du theme de l'utilisateur
        $listTheme = $this->user?->getThemesUser() ?? new ArrayCollection([]);
        // Si l'utilisateur n'a pas de thème, on définit un tableau vide
        if ($listTheme->count() === 0) {
            $themeSeleted = null;
        } else {
            // On recherche le theme sélectionné
            foreach ($listTheme as $theme) {
                if ($theme->isSelected()) {
                    $themeSeleted = $theme;
                    break;
                }
            }
        }
        return $this->render('defaut_react.html.twig', [
            'themeUser' => $themeSeleted ?? null,
        ]);
    }
    
}

