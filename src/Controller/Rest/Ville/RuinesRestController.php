<?php

namespace App\Controller\Rest\Ville;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Ruines;
use App\Entity\RuinesCases;
use App\Entity\RuinesPlans;
use App\Entity\User;
use App\Entity\Ville;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Ville\RuineService;
use App\Structures\Dto\GH\Ville\Ruine\MajFilAriane;
use App\Structures\Dto\GH\Ville\Ruine\MajRuineObjetCase;
use App\Structures\Dto\GH\Ville\Ruine\MajRuinePlan;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


#[Route('/rest/v1/ruine', name: 'rest_ruine_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class RuinesRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected RuineService           $ruineService,
        protected ItemsHandler           $itemsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route(path: '/{mapId}', name: 'ville_ruine', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId): Response
    {
        try {
            $ville = $this->userHandler->getTownBySession($mapId);
            
            if ($ville === null) {
                throw new GestHordesException('Le numéro de la ville est incorrecte | fichier ' . __FILE__ .
                                              ' | ligne ' . __LINE__);
            }
            
            $listRuins = $this->entityManager->getRepository(Ruines::class)->findBy(['ville' => $ville]);
            
            $listRuinsJson =
                $this->serializerService->serializeArray($listRuins, 'json', ['ruine', 'general_res', 'ruine_plan']);
            
            return new JsonResponse([
                                        'ruine'   => [
                                            'listRuines' => $listRuinsJson,
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapId),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuinesRestController', 'main');
        }
        
        
    }
    
    #[Route('/maj_ariane', name: 'majariane', methods: ['POST'])]
    public function majBat(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $arianeMaj = $this->gh->getSerializer()->deserialize($request->getContent(), MajFilAriane::class, 'json',
                                                             ['groups' => ['ruine']]);
        
        // Vérification si les champs sont non null
        
        // Vérification si les champs sont non null
        if ($arianeMaj->getMapId() === null || $arianeMaj->getRuineId() === null ||
            count($arianeMaj->getCoord()) === 0) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - La data fournis est incomplète - maj ariane - {$this->user->getId()}.";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $arianeMaj->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - Le numéro de la ville est incorrecte : " . $arianeMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // verification présence de l'utilisateur dans la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - Tentative de modification d'une ruine où le joueur n'a pas été : " .
                          $arianeMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (strlen($arianeMaj->getRuineId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - La longueur de l'id ruine n'est pas correcte : " . $arianeMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification que l'id de la ruine existe pour la ville du joueur voulant mettre à jour.
        if (!$this->entityManager->getRepository(Ruines::class)->existanceRuineId($arianeMaj->getRuineId(), $ville)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'id de la ruine n'existe pas pour cette ville : " . $arianeMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        try {
            $ruinesPlan = $this->entityManager->getRepository(RuinesPlans::class)
                                              ->findOneBy(['ruines' => $arianeMaj->getRuineId()]);
            $ruinesPlan->setTraceSafe($arianeMaj->getCoord());
            
            $this->entityManager->persist($ruinesPlan);
            $this->entityManager->flush();
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app')]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            
            $messageLog = "$codeErreur - Probleme mise à jour du fil d'ariane {$arianeMaj->getMapId()} : " .
                          $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    #[Route('/maj_ruine', name: 'majruine', methods: ['POST'])]
    public function majRuine(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $ruineMaj = $this->gh->getSerializer()->deserialize($request->getContent(), MajRuinePlan::class, 'json',
                                                            ['groups' => ['ruine']]);
        
        // Vérification si les champs sont non null
        if ($ruineMaj->getMapId() === null || $ruineMaj->getRuineId() === null ||
            count($ruineMaj->getPlan()) % 210 !== 0) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - La data fournis est incomplète - maj ruine - {$this->user->getId()}.";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $ruineMaj->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - Le numéro de la ville est incorrecte : " . $ruineMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // verification présence de l'utilisateur dans la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - Tentative de modification d'une ruine où le joueur n'a pas été : " .
                          $ruineMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (strlen($ruineMaj->getRuineId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - La longueur de l'id ruine n'est pas correcte : " . $ruineMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification que l'id de la ruine existe pour la ville du joueur voulant mettre à jour.
        if (!$this->entityManager->getRepository(Ruines::class)->existanceRuineId($ruineMaj->getRuineId(), $ville)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            
            $messageLog = "$codeErreur - L'id de la ruine n'existe pas pour cette ville : " . $ruineMaj->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        try {
            $ruines =
                $this->ruineService->traitementMapping($ruineMaj->getRuineId(), $ruineMaj->getPlan(), $this->user);
            
            $this->entityManager->persist($ruines);
            $this->entityManager->flush();
            
            $listRuins     = $this->entityManager->getRepository(Ruines::class)->findBy(['ville' => $ville]);
            $listRuinsJson = json_decode($this->gh->getSerializer()
                                                  ->serialize($listRuins,
                                                              'json',
                                                              ['groups' => ['ruine',
                                                                            'general_res',
                                                                            'ruine_plan']]),
                                         null, 512,
                                         JSON_THROW_ON_ERROR);
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'zoneRetour' => ['listRuines' => $listRuinsJson]]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            
            $messageLog = "$codeErreur - Probleme mise à jour plans de la ruine {$ruineMaj->getMapId()} : " .
                          $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    #[Route('/maj_case', name: 'majcase', methods: ['POST'])]
    public function maj_case(Request $request): JsonResponse
    {
        try {
            $this->userHandler->verifyUserIsConnected();
            
            $ville = $this->userHandler->getTownOfUser($this->user);
            if ($ville === null) {
                throw new GestHordesException("Utilisateur pas en ville | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            $ruineMaj = new MajRuineObjetCase();
            $this->serializerService->deserialize($request->getContent(), MajRuineObjetCase::class, 'json', $ruineMaj, ['ruine']);
            
            // Controle si le mapId est identique à celui fournis par la requête
            if ($ruineMaj->getMapId() !== $ville->getMapId()) {
                throw new GestHordesException("Le mapId fournis n'est pas identique à celui de la ville | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            // Récupération de la ruine et verification de son existance
            $ruine = $this->entityManager->getRepository(Ruines::class)->findOneBy(['id' => $ruineMaj->getRuineId()]);
            if ($ruine === null) {
                throw new GestHordesException("La ruine n'existe pas | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            // Verification cohérance ruine et ville
            if ($ruine->getVille()->getId() !== $ville->getId()) {
                throw new GestHordesException("La ruine n'appartient pas à la ville | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            // Verification que l'id de la case fournie appartient bien à la ruine fournie
            if (!$this->entityManager->getRepository(RuinesCases::class)->existanceCaseId($ruineMaj->getRuinesCases()->getId(), $ruine)) {
                throw new GestHordesException("La case n'appartient pas à la ruine | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            // Mise à jour via le service
            $ruineUpdated = $this->ruineService->miseAJourCase($ruineMaj->getRuinesCases(), $this->user);
            
            // récupération de tous les objets pour en faire le listing
            $allObjet = $this->ruineService->recuperationObjetRuine($ruineUpdated->getRuinesPlans());
            
            return new JsonResponse([
                                        'ruineCase'      => $this->serializerService->serializeArray($ruineUpdated, 'json', ['ruine', 'ruine_plan', 'general_res']),
                                        'allRuineObjets' => $this->serializerService->serializeArray($allObjet, 'json', ['ruine']),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuinesRestController', 'maj_case');
        }
    }
    
    /**
     *
     * @param Request $request
     * @param string $ruineId
     * @param int $mapId
     * @return Response
     */
    #[Route(path: '/{ruineId}/{mapId}', name: 'ville_ruine_plan', requirements: ['mapId' => '\d+', 'ruineId' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function ruinePlans(Request $request, string $ruineId, int $mapId): Response
    {
        try {
            $ville = $this->userHandler->getTownBySession($mapId);
            
            if ($ville === null) {
                throw new GestHordesException('Le numéro de la ville est incorrecte | fichier ' . __FILE__ . ' | ligne ' . __LINE__);
            }
            
            
            $planRuine = $this->entityManager->getRepository(RuinesPlans::class)->findOneBy(['ruines' => $ruineId]);
            
            $ruine = $this->entityManager->getRepository(Ruines::class)->findOneBy(['id' => $ruineId]);
            
            $ruineCases = [];
            $filAriane  = [];
            $maxEtage   = 1;
            $allObjet   = [];
            if ($planRuine === null) {
                $planRuine = new RuinesPlans();
                
                $planRuine->setRuines($ruine);
                for ($z = 0; $z < 1; $z++) {
                    for ($y = 0; $y < 14; $y++) {
                        for ($x = 0; $x < 15; $x++) {
                            $ruineCase = new RuinesCases();
                            
                            $ruineCase->setX($x)
                                      ->setY($y)
                                      ->setTypeCase(($x == 7 && $y == 0 && $z == 0) ? RuinesCases::CASE_ENTRE :
                                                        RuinesCases::CASE_VIDE)
                                      ->setNbrZombie(($x == 7 && $y == 0 && $z == 0) ? 0 : null)
                                      ->setTypePorte(null)
                                      ->setTypeEscalier(null);
                            
                            $ruineCases[$z][$y][$x] = $ruineCase;
                            
                            $filAriane[$z][$y][$x][] = null;
                            
                        }
                    }
                }
                
                $arrayPoint = [[7, 0, 0]];
            } else {
                foreach ($planRuine->getCases() as $case) {
                    $ruineCases[$case->getZ()][$case->getY()][$case->getX()]  = $case;
                    $filAriane[$case->getZ()][$case->getY()][$case->getX()][] = null;
                    
                    if ($case->getItems()->count() > 0) {
                        foreach ($case->getItems() as $item) {
                            $idItem = $item->getItem()->getId() * 10 + ($item->isBroken() ? 1 : 0);
                            
                            if (!isset($allObjet[$idItem])) {
                                $allObjet[$idItem]['count']  = $item->getNombre();
                                $allObjet[$idItem]['broken'] = $item->isBroken();
                                $allObjet[$idItem]['item']   = $item->getItem();
                            } else {
                                $allObjet[$idItem]['count'] += $item->getNombre();
                            }
                        }
                    }
                    
                    if ($case->getZ() + 1 > $maxEtage) {
                        $maxEtage = $case->getZ() + 1;
                    }
                }
                
                if ($planRuine->getTraceSafe() !== null) {
                    $arrayPoint = $planRuine->getTraceSafe();
                } else {
                    $arrayPoint = [[7, 0, 0]];
                }
                
            }
            
            $popUpMaj = $this->itemsHandler->recuperationItems([9, 10]);
            
            $this->ruineService->traitementFilAriane($filAriane, $arrayPoint);
            
            $listRuins = $this->entityManager->getRepository(Ruines::class)->findBy(['ville' => $ville]);
            
            
            $listRuinsJson  = $this->serializerService->serializeArray($listRuins, 'json', ['ruine', 'general_res']);
            $ruinsJson      = $this->serializerService->serializeArray($ruine, 'json', ['ruine']);
            $planRuineJson  = $this->serializerService->serializeArray($planRuine, 'json', ['ruine', 'ruine_plan']);
            $ruineCasesJson =
                $this->serializerService->serializeArray($ruineCases, 'json', ['ruine', 'ruine_plan', 'general_res']);
            $allObjetJson   = $this->serializerService->serializeArray($allObjet, 'json', ['ruine']);
            $userJson       = $this->serializerService->serializeArray($this->user ?? new User(), 'json', ['ruine']);
            
            $tradPorte = [
                RuinesCases::TYPE_PORTE_OUV   => "small_enter",
                RuinesCases::TYPE_PORTE_FERM  => "item_lock",
                RuinesCases::TYPE_PORTE_DECAP => "item_classicKey",
                RuinesCases::TYPE_PORTE_PERCU => "item_bumpKey",
                RuinesCases::TYPE_PORTE_MAGN  => "item_magneticKey",
            ];
            
            return new JsonResponse([
                                        'ruine'   => [
                                            'allObjet'   => $allObjetJson,
                                            'coord'      => $arrayPoint,
                                            'filAriane'  => $filAriane,
                                            'listRuines' => $listRuinsJson,
                                            'maxEtage'   => $maxEtage,
                                            'planRuine'  => $planRuineJson,
                                            'popUpMaj'   => $popUpMaj,
                                            'ruine'      => $ruinsJson,
                                            'ruineCases' => $ruineCasesJson,
                                            'trad_porte' => $tradPorte,
                                            'userOption' => $userJson,
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapId),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuinesRestController', 'ruinePlans');
        }
        
    }
    
}