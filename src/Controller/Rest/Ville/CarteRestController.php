<?php

namespace App\Controller\Rest\Ville;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Banque;
use App\Entity\BatPrototype;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\ItemPrototype;
use App\Entity\Journal;
use App\Entity\MapItem;
use App\Entity\TypeObjet;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Exception\AuthenticationException;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Service\Ville\CarteHandler;
use App\Structures\Collection\ItemsZones;
use App\Structures\Dto\GH\Ville\Carte\FiltreObjet;
use App\Structures\Dto\Ville\CaseMaj;
use App\Utils\ZoneMapHelper;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


#[Route('/rest/v1/carte', name: 'rest_carte_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class CarteRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected CarteHandler           $carteHandler,
        protected UpGradeHandler         $upGrade,
        protected ItemsHandler           $itemsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/filtre', name: 'filtre_objet', methods: ['POST'])]
    public function filtre_objet(Request $request): JsonResponse
    {
        try {
            $filtrage = new FiltreObjet();
            $this->serializerService->deserialize($request->getContent(), FiltreObjet::class, 'json', $filtrage);
            
            // Vérification si les champs sont non null
            if ($filtrage->getMapId() === null || $filtrage->getTypeFiltre() === null || $filtrage->getMin() === null ||
                $filtrage->getMax() === null) {
                throw new GestHordesException("La data fournis est incomplète - filtre - {$this->user?->getId()}.");
            }
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $filtrage->getMapId()]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte : {$filtrage->getMapId()}");
            }
            
            if ($filtrage->getMin() > $filtrage->getMax()) {
                throw new GestHordesException("Le min est > au max : {$filtrage->getMapId()} - {$filtrage->getMin()} - {$filtrage->getMax()}");
            }
            
            $maxKm = $ville->getMaxKm();
            
            if ($filtrage->getTypeFiltre() === 0 && $filtrage->getMax() > $maxKm) {
                $filtrage->setMax($maxKm);
            }
            
            
            $mapItem      = $this->entityManager->getRepository(MapItem::class)->recupMapItemAllVille($ville);
            $mapItemArray = [];
            foreach ($mapItem as $item) {
                $mapItemArray[$item->getZone()->getId()][$item->getItem()->getId() * 10 +
                                                         ($item->getBroked() ? 1 : 0)] = $item;
            }
            
            $carteAmelioConstruit = $this->entityManager->getRepository(Chantiers::class)->chantierConstruit(
                    $ville,
                    ChantierPrototype::ID_CHANTIER_CARTE_AME,
                ) != null;
            
            $lvlTDG = $this->upGrade->recupLevelChantier($ville, ChantierPrototype::ID_CHANTIER_PO);
            
            $idMapItem = [];
            
            $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = 0;
            $maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = 0;
            $maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = 0;
            $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = 0;
            $maxAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = 0;
            $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = 0;
            
            $zones = $ville->getZones();
            foreach ($zones as $zoneMap) {
                $zoneMap->setIndicVisite($this->carteHandler->recupIndicVisit($ville->getJour(), $zoneMap->getDay() ?? 0, $zoneMap->getVue()));
                $zoneMap->setZombieEstim((new ZoneMapHelper())->getEstimZombie($zoneMap, $ville, $carteAmelioConstruit, $lvlTDG));
                $marqueurOnly = false;
                if (isset($mapItemArray[$zoneMap->getId()])) {
                    if (($filtrage->getTypeFiltre() === 0 && $filtrage->getMin() <= $zoneMap->getKm() &&
                         $filtrage->getMax() >= $zoneMap->getKm()) ||
                        ($filtrage->getTypeFiltre() === 1 && $filtrage->getMin() <= $zoneMap->getZone() &&
                         $filtrage->getMax() >= $zoneMap->getZone())
                    ) {
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = 0;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = 0;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = 0;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = 0;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = 0;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = 0;
                        $carteScrut                                                 = 0;
                        $itemSolArray                                               = [];
                        $itemSolArrayIcone                                          = [];
                        $presenceMarqueur                                           = false;
                        $presenceAutre                                              = false;
                        /** @var MapItem $mapItem */
                        foreach ($mapItemArray[$zoneMap->getId()] as $mapItem) {
                            if ($mapItem->getItem()->getTypeDecharge() !== null) {
                                $carteAlter[$mapItem->getItem()->getTypeDecharge()->getChantier()->getId()] += $mapItem->getNombre();
                            }
                            if (!isset($itemSolArray['item_' . $mapItem->getItem()->getId()])) {
                                $itemSolArray['item_' . $mapItem->getItem()->getId()] = true;
                            }
                            if (!isset($itemSolArrayIcone['item_' . $mapItem->getItem()->getId()])) {
                                $itemSolArrayIcone['item_' . $mapItem->getItem()->getId()] = $mapItem->getItem()->getIcon();
                            }
                            if ($mapItem->getItem()?->getTypeObjet()?->getId() === TypeObjet::MARQUEUR) {
                                if ($mapItem->getItem()->getId() === ItemPrototype::ITEM_MARQUEURS_SCRUT) {
                                    $carteScrut += $mapItem->getNombre();
                                }
                                $presenceMarqueur = true;
                            } else {
                                $presenceAutre = true;
                            }
                        }
                        
                        if ($presenceMarqueur && !$presenceAutre) {
                            $marqueurOnly = true;
                        }
                        
                        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]);
                        $maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = max($maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE], $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]);
                        $maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = max($maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS], $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]);
                        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]);
                        $maxAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = max($maxAlter[ChantierPrototype::ID_CHANTIER_APPAT], $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]);
                        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE]);
                        $itemsSol                                                 = array_keys($itemSolArray);
                        $itemsSolIcone                                            = $itemSolArrayIcone;
                        $idMapItem[$zoneMap->getId()]                             = 1;
                    } else {
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = null;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = null;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = null;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = null;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = null;
                        $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = null;
                        $itemsSol                                                   = [];
                        $itemsSolIcone                                              = [];
                        $carteScrut                                                 = null;
                    }
                    
                } else {
                    $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = null;
                    $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = null;
                    $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = null;
                    $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = null;
                    $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = null;
                    $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = null;
                    $itemsSol                                                   = [];
                    $itemsSolIcone                                              = [];
                    $carteScrut                                                 = null;
                    
                }
                
                $zoneMap->setCarteAlter($carteAlter)
                        ->setItemSol($itemsSol)
                        ->setCarteScrut($carteScrut)
                        ->setMarqueurOnly($marqueurOnly)
                        ->setItemSolIcone($itemsSolIcone);
            }
            
            
            $zonesSeria = $this->serializerService->serializeArray($zones, 'json', ['carte', 'carte_gen']);
            
            
            $itemSolTmp      = $this->carteHandler->recupSommeItemSol($ville, array_keys($idMapItem));
            $itemSolBrokeTmp = $this->carteHandler->recupSommeItemSolBroke($ville, array_keys($idMapItem));
            
            $categorieObjet = $this->entityManager->getRepository(CategoryObjet::class)->findAllExceptInactif();
            
            $itemBanqueTmp      = $this->entityManager->getRepository(Banque::class)->somItemBanque($ville);
            $itemBanqueBrokeTmp = $this->entityManager->getRepository(Banque::class)->somItemBanqueBroke($ville);
            $itemBanque         = [];
            $itemBanqueBroke    = [];
            foreach ($itemBanqueTmp as $item) {
                $itemBanque[$item['id']] = $item['nbrItem'];
            }
            foreach ($itemBanqueBrokeTmp as $item) {
                $itemBanqueBroke[$item['id']] = $item['nbrItem'];
            }
            
            $itemSolBroken = [];
            $listCate      = [];
            if ($this->user?->getUserPersonnalisation()?->isItemSeparedCate() ?? false) {
                foreach ($itemSolTmp as $item) {
                    $item->setNbrItemBank($itemBanque[$item->getId()] ?? 0);
                }
                $listCateTmp['objetSol'] = $this->serializerService->serializeArray($itemSolTmp);
                $listCate[]              = $listCateTmp;
                // De la même façon pour les items cassés
                foreach ($itemSolBrokeTmp as $item) {
                    $item->setNbrItemBank($itemBanqueBroke[$item->getId()] ?? 0);
                }
                $itemBrokeTmp    = array_combine(array_map(fn($i) => $i->getId(), $itemSolBrokeTmp), $itemSolBrokeTmp);
                $itemSolBroken[] = $this->serializerService->serializeArray($itemBrokeTmp);
            } else {
                foreach ($categorieObjet as $cate) {
                    $listCateTmp             = $this->serializerService->serializeArray($cate, 'json', ['carte']);
                    $listCateTmp['objetSol'] =
                        array_filter($itemSolTmp, fn($i) => $i->getCategoryObjetId() == $cate->getId());
                    
                    
                    foreach ($listCateTmp['objetSol'] as $objet) {
                        $objet->setNom($this->translator->trans($objet->getNom(), [], 'items'))
                              ->setNbrItemBank($itemBanque[$objet->getId()] ?? 0);
                    }
                    
                    $listCateTmp['objetSol'] = $this->serializerService->serializeArray($listCateTmp['objetSol']);
                    
                    $itemSolBrokedTmpFiltre =
                        array_filter($itemSolBrokeTmp, fn($i) => $i->getCategoryObjetId() == $cate->getId());
                    foreach ($itemSolBrokedTmpFiltre as $objet) {
                        $objet->setNom($this->translator->trans($objet->getNom(), [], 'items'))
                              ->setNbrItemBank($itemBanqueBroke[$objet->getId()] ?? 0);
                    }
                    
                    $itemBrokeTmp = array_combine(array_map(fn($i) => $i->getId(), $itemSolBrokedTmpFiltre),
                                                  $itemSolBrokedTmpFiltre);
                    
                    $itemSolBroken[$cate->getId()] = $this->serializerService->serializeArray($itemBrokeTmp);
                    $listCate[]                    = $listCateTmp;
                }
            }
            
            
            return new JsonResponse([
                                        'listCategorie'      => $listCate,
                                        'listItemsSolBroken' => $itemSolBroken,
                                        'zones'              => $zonesSeria,
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'filtre_objet');
        }
        
        
    }
    
    #[Route('/{mapId}', name: 'general', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function index(Request $request, int $mapId): JsonResponse
    {
        try {
            $ville = $this->userHandler->getTownBySession($mapId);
            
            
            // Si la ville est null, mais que l'utilisateur a un numéro de ville, on ne fait pas d'erreur "ville incorrecte", mais une nouvelle erreur pour rediriger
            
            if ($ville === null && (($this->user === null) ||
                                    ($mapId !== $this->user->getMapId()))) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            } elseif ($ville === null) {
                return new JsonResponse([], Response::HTTP_NO_CONTENT);
            }
            
            $carte = $this->carteHandler->recupInfoVilleCarte($ville, $this->user);
            
            $popUpMaj = $this->itemsHandler->recuperationItems([10]);
            
            return new JsonResponse([
                                        'carte'    => $carte,
                                        'popUpMaj' => $popUpMaj,
                                        'general'  => $this->generateArrayGeneral($request, $mapId),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'index');
        }
        
    }
    
    #[Route('/majBat', name: 'majBat', methods: ['POST'])]
    public function majBat(Request $request): JsonResponse
    {
        try {
            
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            $data   = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
            $x      = $data->x;
            $y      = $data->y;
            $camped = (int)$data->camped;
            $empty  = (int)$data->empty;
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->mapId]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte : $data->mapId");
            }
            
            $citoyen = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                        'citoyen' => $this->user]);
            if ($citoyen === null) {
                throw new GestHordesException("Vous ne faites pas partis de la ville. : $data->mapId");
            }
            
            if ($x > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée x n'est pas correcte. : $data->mapId");
            }
            
            if ($y > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée y n'est pas correcte : $data->mapId");
            }
            
            if (($camped !== 0 && $camped !== 1)) {
                throw new GestHordesException("La valeur pour \"campé\" n'est pas correcte : $data->mapId");
            }
            
            if (($empty !== 0 && $empty !== 1)) {
                throw new GestHordesException("La valeur pour \"vide\" n'est pas correcte. : $data->mapId");
            }
            
            $zone = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['x'     => $x, 'y' => $y,
                                                                                    'ville' => $ville]);
            if ($zone === null) {
                throw new GestHordesException("Un problème est survenue dans la récupération de la zone : $data->mapId");
            }
            
            if ($zone->getBat() === null) {
                throw new GestHordesException("Il n'y a pas de bâtiment sur cette zone : $data->mapId");
            }
            
            $zone->setEmpty(!($empty === 0))
                 ->setCamped(!($camped === 0));
            
            
            $this->entityManager->persist($zone);
            $this->entityManager->flush();
            
            $retour['zoneRetour'] = $this->translator->trans("Maj OK!", [], 'app');
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'majBat');
        }
        
    }
    
    #[Route('/majCase', name: 'maj_case', methods: ['POST'])]
    public function majCase(Request $request): JsonResponse
    {
        try {
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            /**
             * @var CaseMaj $caseMaj
             */
            $caseMaj = $this->serializerService->deserialize($request->getContent(), CaseMaj::class);
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $caseMaj->getMapid()]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            // Vérification joueur mettant à jour avec le joueur connecté
            if ($this->user->getId() !== $caseMaj->getUserid()) {
                throw new GestHordesException("Utilisateur fournis différent de celui connecté");
            }
            
            // récupération de la zone dans la base de donnée
            $idZone = ($caseMaj->getMapid() * 10 + Ville::ORIGIN_MH) * 10000 + $caseMaj->getZoneMaj()->getY() * 100 +
                      $caseMaj->getZoneMaj()->getX();
            
            $zone = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['id' => $idZone]);
            
            if ($zone === null) {
                throw new GestHordesException("La zone devrait exister id de la zone reconstruit : $idZone");
            }
            
            $balise     = ($caseMaj->getZoneMaj()->getBalise() !== -1) ? $caseMaj->getZoneMaj()->getBalise() : null;
            $zombie     = ($caseMaj->getZoneMaj()->getZombie() !== -1) ? $caseMaj->getZoneMaj()->getZombie() : null;
            $zombie_min =
                ($caseMaj->getZoneMaj()->getZombieMin() !== -1) ? $caseMaj->getZoneMaj()->getZombieMin() : null;
            $zombie_max =
                ($caseMaj->getZoneMaj()->getZombieMax() !== -1) ? $caseMaj->getZoneMaj()->getZombieMax() : null;
            $danger     = null;
            if ($zombie !== null) {
                $danger = ($zombie === 0) ? 0 :
                    (($zombie >= 1 && $zombie <= 2) ? 1 : (($zombie >= 3 && $zombie < 5) ? 2 : 3));
            }
            $epuise = $caseMaj->getZoneMaj()->getDried() === 1;
            
            
            $listObjet = $this->entityManager->getRepository(ItemPrototype::class)->createQueryBuilder('i', 'i.id')
                                             ->getQuery()->getResult();
            
            // création d'un tableau d'objet à traiter et vérification si le tableau à traiter contient uniquement des objets de type Marqueur
            $idTab            = [];
            $presenceMarqueur = false;
            foreach ($caseMaj->getZoneMaj()->getItems() as $itemMaj) {
                $idTab[$itemMaj->getItem()->getId() * 10 + ($itemMaj->getBroked() ? 1 : 0)] = $itemMaj;
            }
            
            $zone->setDried($epuise)
                 ->setZombie($zombie)
                 ->setLvlBalisage($balise)
                 ->setZombieMin($zombie_min)
                 ->setZombieMax($zombie_max)
                 ->setDanger($danger);
            
            
            foreach ($zone->getItems()->toArray() as $itemZone) {
                $item_id = $itemZone->getItem()->getId() * 10 + ($itemZone->getBroked() ? 1 : 0);
                
                if (isset($idTab[$item_id])) {
                    if ($idTab[$item_id]->getItem()->getTypeObjet()?->getId() === TypeObjet::MARQUEUR) {
                        if ($itemZone->getNombre() !== $idTab[$item_id]->getNombre()) {
                            $presenceMarqueur = true;
                        }
                    }
                    $itemZone->setNombre($idTab[$item_id]->getNombre());
                    unset($idTab[$item_id]);
                } else {
                    $zone->removeItem($itemZone);
                }
                
            }
            
            foreach ($idTab as $itemMaj) {
                
                if ($itemMaj->getItem()->getTypeObjet()?->getId() === TypeObjet::MARQUEUR) {
                    $presenceMarqueur = true;
                }
                
                $itemZone = new MapItem($listObjet[$itemMaj->getItem()->getId()], $itemMaj->getBroked());
                $itemZone->setNombre($itemMaj->getNombre());
                
                $zone->addItem($itemZone);
            }
            
            
            // Si la zone n'a pas été vu par le joueur ou si on met à jour que des marqueurs, on met à jour la date de mise à jour du marqueur et qui l'a fait, on ne met pas à jour le vu également
            if ($zone->getVue() === ZoneMap::NON_EXPLO || $presenceMarqueur) {
                $zone->setMarqueurMajDay($ville->getJour())
                     ->setMarqueurMajHeure((new DateTime())->format('H:i:s'))
                     ->setMarqueurMajBy($this->user);
            }
            if ($zone->getVue() !== ZoneMap::NON_EXPLO && !$presenceMarqueur) {
                $zone->setVue(ZoneMap::CASE_VUE);
                $zone->setCitoyen($this->user)
                     ->setHeureMaj((new DateTime())->format('H:i:s'))
                     ->setMajAt(new DateTimeImmutable('now'))
                     ->setDay($ville->getJour());
            }
            
            
            if ($zone->getPdc() === null) {
                $zone->setPdc(0);
            }
            
            
            $this->entityManager->persist($zone);
            $this->entityManager->flush();
            
            return new JsonResponse(['libRetour' => $this->translator->trans("Mise à jour OK.", [], 'app')],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'majCase');
        }
        
    }
    
    #[Route('/majHypo', name: 'majHypo', methods: ['POST'])]
    public function majHypo(Request $request): JsonResponse
    {
        try {
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            $data = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
            $x    = $data->x;
            $y    = $data->y;
            $hypo = (int)$data->hypo;
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->mapId]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte " . $data->mapId);
            }
            
            $citoyen = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                        'citoyen' => $this->user]);
            if ($citoyen === null) {
                throw new GestHordesException("Vous ne faites pas partis de la ville " . $data->mapId);
            }
            
            if ($x > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée x n'est pas correcte " . $data->mapId);
            }
            
            if ($y > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée y n'est pas correcte " . $data->mapId);
            }
            
            if ($hypo === 0) {
                throw new GestHordesException("La valeur pour \"l'hypothèse du bâtiment\" n'est pas correcte " .
                                              $data->mapId);
            }
            
            
            $zone = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['x'     => $x, 'y' => $y,
                                                                                    'ville' => $ville]);
            if ($zone === null) {
                throw new GestHordesException("Un problème est survenue dans la récupération de la zone " .
                                              $data->mapId);
            }
            
            if ($zone->getBat() === null) {
                throw new GestHordesException("Il n'y a pas de bâtiment sur cette zone " . $data->mapId);
            }
            
            $batiment = $this->entityManager->getRepository(BatPrototype::class)->findOneBy(['id' => $hypo]);
            
            $zone->setBatHypothese($batiment);
            
            
            $this->entityManager->persist($zone);
            $this->entityManager->flush();
            
            $retour['zoneRetour'] = $this->translator->trans("Maj OK!", [], 'app');
            
            return new JsonResponse($retour);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'majHypo');
        }
        
        
    }
    
    #[Route('/majScrut', name: 'maj_scrut', methods: ['POST'])]
    public function majScrut(Request $request): JsonResponse
    {
        try {
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            $data = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->mapId]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte " . $data->mapId);
            }
            
            
            $scrutConstruit = $this->entityManager->getRepository(Chantiers::class)->chantierConstruit(
                    $ville,
                    ChantierPrototype::ID_CHANTIER_SCRUT,
                ) != null;
            
            if (!$scrutConstruit) {
                return new JsonResponse(['codeRetour' => 2], Response::HTTP_OK);
            }
            
            // récupération de la direction du scrut
            $regenDir = $this->entityManager->getRepository(Journal::class)->findOneBy(['ville' => $ville,
                                                                                        'day'   => $ville->getJour()]);
            
            if ($regenDir === null || $regenDir->getRegenDir() === null) {
                return new JsonResponse(['codeRetour' => 2], Response::HTTP_OK);
            }
            
            // récupération de l'item marqueurs scrut
            $marqueurs = $this->entityManager->getRepository(ItemPrototype::class)
                                             ->findOneBy(['id' => ItemPrototype::ITEM_MARQUEURS_SCRUT]);
            if ($marqueurs === null) {
                return new JsonResponse(['codeRetour' => 2], Response::HTTP_OK);
            }
            
            $regenDirNum = match ($regenDir->getRegenDir()) {
                'le nord'            => ZoneMap::DIRECTION_NORD,
                "l'est", "l’est"     => ZoneMap::DIRECTION_EST,
                'le sud'             => ZoneMap::DIRECTION_SUD,
                "l'ouest", "l’ouest" => ZoneMap::DIRECTION_OUEST,
                'le nord-est'        => ZoneMap::DIRECTION_NORD_EST,
                'le sud-est'         => ZoneMap::DIRECTION_SUD_EST,
                'le nord-ouest'      => ZoneMap::DIRECTION_NORD_OUEST,
                'le sud-ouest'       => ZoneMap::DIRECTION_SUD_OUEST,
            };
            
            $zoneRegen = false;
            if ($ville->getDayMajScrut() !== $ville->getJour()) {
                foreach ($ville->getZone() as $zone) {
                    if ($zone->getVue() == ZoneMap::CASE_NONVUE || $zone->getVue() == ZoneMap::CASE_VUE) {
                        if ($zone->getDay() != 0 || $zone->getDay() === null) {
                            $diffDay = abs($ville->getJour() - $zone->getDay() ?? 0);
                            $km      = $zone->getKm();
                            if ($diffDay >= 1) {
                                
                                if ($zone->getDirection() == $regenDirNum && ($km > 2 || $ville->getWeight() <= 15)) {
                                    $itemDejaPres = false;
                                    foreach ($zone->getItems() as $item) {
                                        if ($item->getItem()->getId() == $marqueurs->getId()) {
                                            $item->setNombre($item->getNombre() + 1);
                                            $itemDejaPres = true;
                                        }
                                        
                                    }
                                    if (!$itemDejaPres) {
                                        $itemMap = new MapItem($marqueurs, false);
                                        $itemMap->setNombre(1);
                                        $zone->addItem($itemMap);
                                    }
                                    
                                    $zone->setMarqueurMajDay($ville->getJour())
                                         ->setMarqueurMajHeure((new DateTime())->format('H:i:s'))
                                         ->setMarqueurMajBy($this->user);
                                    
                                    $zoneRegen = true;
                                }
                            }
                        }
                    }
                }
            }
            
            if ($zoneRegen) {
                $ville->setDayMajScrut($ville->getJour());
                $this->entityManager->persist($ville);
                $this->entityManager->flush();
                
                return new JsonResponse(['codeRetour' => 0,
                                         'libRetour'  => $this->translator->trans("Ajout des marqueurs de scrutateur effectué.",
                                                                                  [],
                                                                                  'app')],
                                        Response::HTTP_OK);
            }
            
            return new JsonResponse(['codeRetour' => 2], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'majScrut');
        }
        
    }
    
    #[Route('/popUpInfo', name: 'popUp', methods: ['POST'])]
    public function popUp(Request $request): JsonResponse
    {
        try {
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            $data = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
            $x    = $data->x;
            $y    = $data->y;
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->mapId]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte " . $data->mapId);
            }
            
            // Verification citoyen
            $citoyen = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                        'citoyen' => $this->user]);
            if ($citoyen === null) {
                throw new GestHordesException("Vous ne faites pas partis de la ville " . $data->mapId);
            }
            
            if ($x > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée x n'est pas correcte " . $data->mapId);
            }
            
            if ($y > $ville->getWeight()) {
                throw new GestHordesException("La coordonnée y n'est pas correcte " . $data->mapId);
            }
            
            $zone = $this->entityManager->getRepository(ZoneMap::class)->findOneBy(['x'     => $x, 'y' => $y,
                                                                                    'ville' => $ville]);
            if ($zone === null) {
                throw new GestHordesException("Un problème est survenu dans la récupération de la zone " .
                                              $data->mapId);
            }
            $objetTrie = new ItemsZones($this->translator);
            $objetTrie->setMapItems($zone->getItems());
            
            $zone->setItems($objetTrie->triByNom());
            
            $lvlTDG = $this->upGrade->recupLevelChantier($ville, ChantierPrototype::ID_CHANTIER_PO);
            
            $paAller = $zone->getPa();
            
            $reductionPA = 0;
            if ($lvlTDG === 4) {
                $reductionPA = 2;
            }
            if ($lvlTDG === 5) {
                $reductionPA = 3;
            }
            
            if ($reductionPA != 0 && ($zone->getXRel() === 0 || $zone->getYRel() === 0)) {
                $reductionPA--;
            }
            
            
            if ($paAller < $reductionPA) {
                $paRetour = $paAller;
            } else {
                $paRetour = 2 * $paAller - $reductionPA;
            }
            
            
            $retour['dried']      = ($zone->getDried() ?? false) ? 1 : 0;
            $retour['zombie']     = $zone->getZombie();
            $retour['balise']     = $zone->getLvlBalisage();
            $retour['zombie_min'] = $zone->getZombieMin();
            $retour['zombie_max'] = $zone->getZombieMax();
            $retour['vue']        = $zone->getVue();
            $retour['x']          = $zone->getX();
            $retour['y']          = $zone->getY();
            $retour['xRel']       = $zone->getXRel();
            $retour['yRel']       = $zone->getYRel();
            $retour['distance']   = $this->gh->calculKM($ville, $zone->getX(), $zone->getY());
            $retour['paAller']    = $paAller;
            $retour['paRetour']   = $paRetour;
            $retour['items']      =
                array_map(fn(MapItem $mapItem) => $this->serializerService->serializeArray($mapItem, 'json', ['carte']),
                    $zone->getItems()->toArray());
            
            
            return new JsonResponse(['zoneMaj' => $retour], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'popUp');
        }
        
    }
    
    #[Route('/refresh', name: 'refresh_carte', methods: ['POST'])]
    public function refreshCarte(Request $request): JsonResponse
    {
        try {
            $data   = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
            $mapId  = $data->mapId;
            $userId = $data->userId;
            $outils = $data->outils ?? false;
            
            // Vérification existance de la ville
            $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId]);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte " . $mapId);
            }
            
            // Vérification existance de la ville
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $userId]);
            
            
            $carte = $this->carteHandler->recupInfoVilleCarte($ville, $user ?? new User(), $outils);
            
            return new JsonResponse(['zoneRetour' => $carte], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CarteRestController', 'refreshCarte');
        }
        
        
    }
    
}