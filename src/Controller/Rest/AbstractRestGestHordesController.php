<?php

namespace App\Controller\Rest;

use App\Entity\ThemeUser;
use App\Entity\User;
use App\Entity\UserPersoCouleur;
use App\Entity\UserPersonnalisation;
use App\Entity\VersionsSite;
use App\Entity\Ville;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractRestGestHordesController extends AbstractController
{
    public ?Ville $ville;
    public ?User  $user;
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
    )
    {
        
        $this->ville = $this->userHandler->getTownBySession();
        
        $this->user = $this->security->getUser();
        
    }
    
    /**
     * @throws JsonException
     */
    public function generateArrayGeneral(Request $request, ?int $mapId = 0): array
    {
        $this->ville = $this->userHandler->getTownBySession($mapId);
        
        $lastVersion = $this->entityManager->getRepository(VersionsSite::class)->last();
        
        $date = $lastVersion !== null ? $lastVersion->getDateVersion() : new DateTime();
        
        if ($lastVersion === null) {
            $versionGH = "unknown";
        } else {
            if ($lastVersion->getTagName() !== null) {
                $versionGH =
                    "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}-{$lastVersion->getTagName()}.{$lastVersion->getNumTag()}";
            } else {
                $versionGH =
                    "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}";
            }
        }
        
        $jsonString = file_get_contents('../paramGeneraux.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $lang = $request->getLocale() ?? 'fr';
        
        // Récupération du theme de l'utilisateur
        $listTheme    = $this->user?->getThemesUser() ?? new ArrayCollection([]);
        $themeSeleted = null;
        // Si l'utilisateur n'a pas de thème, on définit un tableau vide
        if ($listTheme->count() !== 0) {
            // On recherche le theme sélectionné
            foreach ($listTheme as $theme) {
                if ($theme->isSelected()) {
                    $themeSeleted = $this->serializerService->serializeArray($theme, 'json', ['option']);
                    break;
                }
            }
        }
        
        if ($themeSeleted === null) {
            $themeObjet = new ThemeUser();
            $themeObjet->setUserPersoCouleur(new UserPersoCouleur());
            $themeSeleted = $this->serializerService->serializeArray($themeObjet, 'json', ['option']);
        }
        
        $menuUser = $this->serializerService->serializeArray($this->userHandler->getMenuUser($this->isGranted('IS_AUTHENTICATED_REMEMBERED'), $this->ville), 'json', ['general']);
        
        $userGeneral = $this->user ?? ((new User())->setUserPersonnalisation(new UserPersonnalisation()));
        
        return [
            'fuseau'             => $this->user?->getFuseau() ?? 'Europe/Paris',
            'inVille'            => $this->userHandler->inVille(),
            'isAdmin'            => $this->isGranted('ROLE_ADMIN'),
            'niveauHabilitation' => ($this->isGranted('ROLE_ADMIN')) ? 'admin' : (($this->isGranted('ROLE_BETA')) ? 'beta' : 'user'),
            'isCo'               => $this->isGranted('IS_AUTHENTICATED_REMEMBERED'),
            'myVille'            => $this->userHandler->myVille($this->ville),
            'ptsSaison'          => $this->gh->calculPointSaison($this->ville),
            'user'               => $this->serializerService->serializeArray($userGeneral, 'json', ['general']),
            'ville'              => $this->serializerService->serializeArray($this->ville, 'json', ['general']),
            'version'            => [
                'date'    => $this->serializerService->serializeArray($date),
                'version' => $versionGH,
            ],
            'versionJeu'         => $data['versionJeu'],
            'lang'               => $lang,
            'lienMHE'            => $data['lienMHE'],
            'lienMHO'            => $data['lienMHO'],
            'lienDiscord'        => $data['lienDiscord'],
            'lienGit'            => $data['lienGit'],
            'lienCrowdin'        => $data['lienCrowdin'],
            'habilitation'       => $data['habilitation'],
            'miseEnAvant'        => $data['miseEnAvant'],
            'themeUser'          => $themeSeleted ?? null,
            'menuUser'           => $menuUser,
        ];
    }
    
    public function randomString($length = 20): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string     = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[random_int(0, strlen($characters) - 1)];
        }
        return $string;
    }
}