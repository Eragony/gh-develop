<?php

namespace App\Controller\Rest\Villes;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\HomePrototype;
use App\Entity\JobPrototype;
use App\Entity\Ville;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Villes\VilleHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JetBrains\PhpStorm\Pure;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


#[Route('/rest/v1/villes', name: 'rest_villes_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class VillesRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected ValidatorInterface     $validator, private readonly VilleHandler $villeHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route(path: '/all', name: 'all', methods: ['GET'])]
    public function all(Request $request): JsonResponse
    {
        try {
            // récuparation du mapId session dans le header
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            // La récupération de toutes les villes n'influences pas sur le top 30 déjà fournis, on va donc se concentrer uniquement sur la liste des villes
            $vr            = $this->entityManager->getRepository(Ville::class);
            $listAllVilles = $vr->getAllVilles();
            
            $arrVilles = [];
            foreach ($listAllVilles as $key => $ville) {
                $arrVilles[$ville->getJour()][] = $ville;
            }
            
            $villesJson = $this->serializerService->serializeArray($arrVilles, 'json', ['villes']);
            
            return new JsonResponse(['listVilles' => $villesJson], Response::HTTP_OK);
        } catch (Exception $e) {
            return $this->errorHandler->handleException($e, 'VillesRestController', 'all');
        }
    }
    
    #[Route(path: '/comparatif', name: 'comparatif', methods: ['GET'])]
    public function comparatif(Request $request): JsonResponse
    {
        try {
            // récuparation du mapId session dans le header
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            // On récupère les ids des villes à comparer
            $ids = $request->query->get('ids');
            if (empty($ids)) {
                throw new GestHordesException($this->translator->trans("Les identifiants des villes n'ont pas été fournis", [], "villes"), GestHordesException::SHOW_MESSAGE_INFO);
            }
            
            $ids = explode(',', $ids);
            
            if (count($ids) > 10) {
                throw new GestHordesException($this->translator->trans("Vous ne pouvez pas comparer plus de 10 villes en même temps.", [], "villes"), GestHordesException::SHOW_MESSAGE_INFO);
            }
            
            $villes = [];
            foreach ($ids as $id) {
                if (!is_numeric($id)) {
                    throw new GestHordesException("Les identifiants des villes sont invalides", GestHordesException::SHOW_MESSAGE_INFO);
                }
                $villes[] = $this->villeHandler->getComparaisonVille($id);
            }
            
            $options['categories']  = $this->serializerService->serializeArray($this->entityManager->getRepository(CategoryObjet::class)->findAllExceptMarqueurs(), 'json', ['comparatif']);
            $options['chantiers']   = $this->serializerService->serializeArray($this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie(), 'json', ['comparatif']);
            $options['plans']       = $this->serializerService->serializeArray($this->entityManager->getRepository(ChantierPrototype::class)->findPlansChantiers(), 'json', ['comparatif']);
            $options['upChantiers'] = $this->serializerService->serializeArray($this->entityManager->getRepository(ChantierPrototype::class)->recupChantierEvolution(), 'json', ['comparatif_spe']);
            $options['jobs']        = $this->serializerService->serializeArray($this->entityManager->getRepository(JobPrototype::class)->findAll(), 'json', ['comparatif']);
            $options['homes']       = $this->serializerService->serializeArray($this->entityManager->getRepository(HomePrototype::class)->findAll(), 'json', ['comparatif']);
            
            
            return new JsonResponse([
                                        'villes'  => $this->serializerService->serializeArray($villes, 'json', ['comparatif']),
                                        'options' => $options,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $e) {
            return $this->errorHandler->handleException($e, 'VillesRestController', 'comparatif');
        }
    }
    
    #[Route(path: '/', name: 'villes', methods: ['GET'])]
    public function main(Request $request): Response
    {
        try {
            // récuparation du mapId session dans le header
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $vr           = $this->entityManager->getRepository(Ville::class);
            
            $top30 = $vr->getTop15();
            
            $listVilles = $this->villesByDay($vr->getListVilles30J());
            
            $top30Json      =
                json_decode($this->gh->getSerializer()->serialize($top30, 'json', ['groups' => ['general', 'villes']]),
                            null, 512, JSON_THROW_ON_ERROR);
            $listVillesJson = json_decode($this->gh->getSerializer()->serialize($listVilles, 'json',
                                                                                ['groups' => ['general', 'villes']]),
                                          null, 512, JSON_THROW_ON_ERROR);
            
            $listSaison = [
                [
                    'type' => 151,
                    'nom'  => $this->translator->trans('Saison {numSaison} - {flag}',
                                                       ['{numSaison}' => 15, '{flag}' => 'beta'], 'villes'),
                ],
                [
                    'type' => 150,
                    'nom'  => $this->translator->trans('Saison {numSaison}', ['{numSaison}' => 15], 'villes'),
                ],
                [
                    'type' => 160,
                    'nom'  => $this->translator->trans('Saison {numSaison}', ['{numSaison}' => 16], 'villes'),
                ],
                [
                    'type' => 170,
                    'nom'  => $this->translator->trans('Saison {numSaison}', ['{numSaison}' => 17], 'villes'),
                ],
            ];
            
            return new JsonResponse([
                                        'villes'  => [
                                            'top30'      => $top30Json,
                                            'listVilles' => $listVillesJson,
                                            'listSaison' => $listSaison,
                                            'maxVilles'  => 10,
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $e) {
            return $this->errorHandler->handleException($e, 'VillesRestController', 'main');
        }
        
    }
    
    #[Route(path: '/search', name: 'search', methods: ['POST'])]
    public function search(Request $request): JsonResponse
    {
        try {
            
            $listSaison = [-1, 151, 150, 160, 170];
            $listCommu  = ["fr", "en", "es", "de", "multi"];
            
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            
            // On fait d'abord les controles avant de faire la requete
            $formErrors = [];
            
            $requiredFields         = ['nom', 'type', 'etat'];
            $atLeastOneFieldPresent = false;
            
            foreach ($requiredFields as $field) {
                if (!empty($data[$field])) {
                    $atLeastOneFieldPresent = true;
                    break;
                }
            }
            
            if (!$atLeastOneFieldPresent) {
                $formErrors['form'] =
                    $this->translator->trans("Au moins l'un des champs suivants : nom de ville, type de ville ou statut de la ville doivent être renseignés.",
                                             [], 'villes');
            }
            
            // Validation pour les valeurs de type
            if (!empty($data['type'])) {
                $typeValues = $data['type'];
                
                $invalidValues = [];
                $validChoices  = ['RE', 'RNE', 'Pandé'];
                
                foreach ($typeValues as $value) {
                    if (!in_array($value, $validChoices)) {
                        $invalidValues[] = $value;
                    }
                }
                
                
                if (!empty($invalidValues)) {
                    $formErrors['type'] =
                        $this->translator->trans('Les valeurs de type de villes sont invalides.', [], 'villes');
                }
            }
            
            // Validation pour les valeurs d'état
            if (!empty($data['etat'])) {
                
                $etatValues = $data['etat'];
                
                $invalidValues = [];
                $validChoices  = ['Normal', 'Chaos', 'Dévasté'];
                
                foreach ($etatValues as $value) {
                    if (!in_array($value, $validChoices)) {
                        $invalidValues[] = $value;
                    }
                }
                
                
                if (!empty($invalidValues)) {
                    $formErrors['etat'] = $this->translator->trans("Les valeurs d'état sont invalides.", [], 'villes');
                }
            }
            // Validation pour les valeurs d'état
            if (!empty($data['saison'])) {
                
                $saisonValue = $data['saison'];
                
                $validChoices = $listSaison;
                
                if (!in_array($saisonValue, $validChoices)) {
                    $invalidValues[] = $saisonValue;
                }
                
                
                if (!empty($invalidValues)) {
                    $formErrors['saison'] =
                        $this->translator->trans("La valeur sélectionnée pour la saison est invalide.", [], 'villes');
                }
            }
            // Validation pour les valeurs d'état
            if (!empty($data['lang'])) {
                
                $langValue = $data['lang'];
                
                $validChoices = $listCommu;
                
                foreach ($langValue as $value) {
                    if (!in_array($value, $validChoices)) {
                        $invalidValues[] = $value;
                    }
                }
                
                
                if (!empty($invalidValues)) {
                    $formErrors['lang'] =
                        $this->translator->trans("La valeur sélectionnée pour la communauté est invalide.", [], 'villes');
                }
            }
            
            if (!empty($formErrors)) {
                return new JsonResponse(['errors' => $formErrors], 400);
            }
            
            // La récupération de toutes les villes n'influences pas sur le top 30 déjà fournis, on va donc se concentrer uniquement sur la liste des villes
            $vr         = $this->entityManager->getRepository(Ville::class);
            $listVilles = $vr->getVillesSearch($data);
            
            
            $villesJson = $this->serializerService->serializeArray($listVilles, 'json', ['villes']);
            
            return new JsonResponse(['listVilles' => $villesJson], 200);
            
        } catch (Exception $e) {
            return $this->errorHandler->handleException($e, 'VillesRestController', 'search');
        }
        
    }
    
    #[Route(path: '/{mapId}', name: 'mapId', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function setMapid(Request $request, int $mapId): JsonResponse
    {
        //Verification de si la ville existe bien
        $vr    = $this->entityManager->getRepository(Ville::class);
        $ville = $vr->findBy(['mapId' => $mapId]);
        
        if ($ville !== null) {
            return new JsonResponse([], Response::HTTP_OK);
        } else {
            return new JsonResponse([], 400);
        }
    }
    
    /**
     * @param Ville[] $villes
     *
     * @return array
     */
    #[Pure] private function villesByDay(array $villes): array
    {
        $arrDay = [];
        foreach ($villes as $key => $ville) {
            $arrDay[$ville->getJour()][] = $ville;
        }
        
        return $arrDay;
    }
    
    
}