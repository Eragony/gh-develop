<?php

namespace App\Controller\Rest\RuineGame;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\RuineGame\RuineGameHandler;
use App\Service\RuineGame\RuineMazeHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\RuineGame\DeplacementPieceRuineGame;
use App\Structures\Dto\GH\RuineGame\DeplacementRuineGame;
use App\Structures\Dto\GH\RuineGame\EndGameRuineGame;
use App\Structures\Dto\GH\RuineGame\FouillePieceRuineGame;
use App\Structures\Dto\GH\RuineGame\FuiteRuineGame;
use App\Structures\Dto\GH\RuineGame\KillZombieRuineGame;
use App\Structures\Dto\GH\RuineGame\NiveauDifficulte;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/ruineGame', name: 'rest_ruineGame_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class RuineGameRestController extends AbstractRestGestHordesController
{
    
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected RuineGameHandler       $ruineGameHandler,
        protected RuineMazeHandler       $ruineMazeHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/deplacementPiece', name: 'deplacementPiece', methods: ['POST'])]
    public function deplacementPiece(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données du niveau pour générer le labyrinthe
            $dataDeplacementPiece = new DeplacementPieceRuineGame();
            $this->serializerService->deserialize($request->getContent(), DeplacementPieceRuineGame::class, 'json',
                                                  $dataDeplacementPiece);
            
            $this->ruineGameHandler->traitementDeplacementPiece($dataDeplacementPiece);
            return new JsonResponse([], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'getRuineMaze');
        }
    }
    
    #[Route('/deplacementRuine', name: 'deplacementRuine', methods: ['POST'])]
    public function deplacementRuine(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données pour le déplacement dans la ruine
            $dataDeplacementRuine = new DeplacementRuineGame();
            $this->serializerService->deserialize($request->getContent(), DeplacementRuineGame::class, 'json',
                                                  $dataDeplacementRuine, ['ruine']);
            
            $retour =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementDeplacementRuine($dataDeplacementRuine));
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'deplacementRuine');
        }
    }
    
    #[Route('/endGame', name: 'endGame', methods: ['POST'])]
    public function endGame(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données du niveau pour générer le labyrinthe
            $dataEndGame = new EndGameRuineGame();
            $this->serializerService->deserialize($request->getContent(), EndGameRuineGame::class, 'json',
                                                  $dataEndGame);
            
            $retour =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementEndRuineGame($dataEndGame));
            
            return new JsonResponse($retour,
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'endGame');
        }
    }
    
    #[Route('/fouillePiece', name: 'fouillePiece', methods: ['POST'])]
    public function fouillePiece(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données du niveau pour générer le labyrinthe
            $dataFouillePiece = new FouillePieceRuineGame();
            $this->serializerService->deserialize($request->getContent(), FouillePieceRuineGame::class, 'json',
                                                  $dataFouillePiece);
            
            $this->ruineGameHandler->traitementFouillePiece($dataFouillePiece);
            
            return new JsonResponse([], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'getRuineMaze');
        }
    }
    
    #[Route('/fuiteRuine', name: 'fuiteRuine', methods: ['POST'])]
    public function fuiteRuine(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données pour le déplacement dans la ruine
            $dataFuiteRuine = new FuiteRuineGame();
            $this->serializerService->deserialize($request->getContent(), FuiteRuineGame::class, 'json',
                                                  $dataFuiteRuine);
            
            $retour =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementFuiteRuine($dataFuiteRuine));
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'fuiteRuine');
        }
    }
    
    #[Route('/getOldMaze/{idRuine}', name: 'getOldMaze', requirements: ['idRuine' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function getOldMaze(string $idRuine): Response
    {
        try {
            
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $response =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementMapOld($idRuine), 'json',
                                                         ['ruine', 'ruine_plan', 'general_res']);
            
            return new JsonResponse($response, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'main');
        }
        
    }
    
    #[Route('/getRuineMaze', name: 'getRuineMaze', methods: ['POST'])]
    public function getRuineMaze(Request $request): Response
    {
        try {
            
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données du niveau pour générer le labyrinthe
            $dataNiveauRuine = new NiveauDifficulte();
            $this->serializerService->deserialize($request->getContent(), NiveauDifficulte::class, 'json',
                                                  $dataNiveauRuine);
            
            $retour =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementGetMazeRuine($dataNiveauRuine),
                                                         'json', ['ruine', 'ruine_plan', 'general_res']);
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'getRuineMaze');
        }
    }
    
    #[Route('/historique', name: 'historique', methods: ['GET'])]
    public function historique(): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $retour = $this->ruineGameHandler->traitementHistoriquePartie();
            
            return new JsonResponse($this->serializerService->serializeArray($retour, 'json', ['ruine']),
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'killZombie');
        }
    }
    
    #[Route('/killZombie', name: 'killZombie', methods: ['POST'])]
    public function killZombie(Request $request): Response
    {
        try {
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On récupère les données du niveau pour gérer le kill
            $dataKillZombie = new KillZombieRuineGame();
            $this->serializerService->deserialize($request->getContent(), KillZombieRuineGame::class, 'json',
                                                  $dataKillZombie);
            
            $retour = $this->ruineGameHandler->traitementKillZombie($dataKillZombie);
            if ($retour === null) {
                return new JsonResponse([], Response::HTTP_OK);
            } else {
                return new JsonResponse($this->serializerService->serializeArray($retour), Response::HTTP_OK);
            }
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'killZombie');
        }
    }
    
    #[Route('/', name: 'main', methods: ['GET'])]
    public function main(Request $request): Response
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $ruineGameArray =
                $this->serializerService->serializeArray($this->ruineGameHandler->traitementMain(), 'json',
                                                         ['ruine', 'ruine_plan', 'general_res']);
            
            return new JsonResponse([
                                        'ruine_game' => $ruineGameArray,
                                        'general'    => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'RuineGameRestController', 'main');
        }
        
    }
    
}