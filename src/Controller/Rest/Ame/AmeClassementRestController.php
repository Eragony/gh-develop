<?php

namespace App\Controller\Rest\Ame;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\HistoriquePictos;
use App\Entity\Pictos;
use App\Exception\GestHordesException;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Service\Ame\AmeHandler;
use App\Service\Ame\PictoHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Villes\VilleHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/ame', name: 'rest_ame_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class AmeClassementRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected AmeHandler             $ameHandler,
        protected VilleHandler           $villeService,
        protected PictoHandler           $pictoHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/cumul/{saison}/{userId}', name: 'cumul', requirements: ['userId' => '\d+', 'saison' => '\d+'], methods: ['GET'])]
    public function cumulPictoUser(int $saison, int $userId = 0): Response
    {
        try {
            $user = $this->userHandler->resolveUser($userId);
            
            $pictos = $this->pictoHandler->getCumulPictosForASaison($user, $saison);
            
            return new JsonResponse([
                                        'pictos' => $this->serializerService->serializeArray($pictos, 'json', ['picto']),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'cumulPictoUser');
        }
        
        
    }
    
    #[Route('/{userId}', name: 'ame_de_user', requirements: ['userId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $userId = 0): Response
    {
        try {
            
            $currentUser = $this->user;
            
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            $user         = $this->userHandler->resolveUserMh($userId);
            
            $villeString       = $this->villeService->getVilleString($user);
            $listOldVilleHisto = $this->villeService->getOldVillesHistos($user);
            $listMyPicto       = $this->pictoHandler->getListPictoUser($user);
            if ($user === $currentUser || ($user->isShowHistoPicto())) {
                $histoPicto = $this->pictoHandler->getHistoPictoUser($user);
            } else {
                $histoPicto = [];
            }
            
            $listVoisin = ($currentUser !== null && $currentUser->getIdMyHordes() === $userId) ? $this->villeService->getNeighbor($user, null, null) : [];
            
            $listVilleCommune = ($currentUser !== null && $currentUser->getIdMyHordes() !== $userId) ? $this->villeService->getVilleCommune($currentUser, $user) : [];
            
            $nbrVille = $this->villeService->getNbrVille($user, null, null);
            
            $stats = $this->ameHandler->getStatsForUser($user);
            
            $ame = [
                'listPicto'   => $this->serializerService->serializeArray($listMyPicto, 'json', ['picto']),
                'userVisu'    => $this->serializerService->serializeArray($user, 'json', ['general', 'picto']),
                'ville'       => $villeString,
                'listVilles'  => $this->serializerService->serializeArray($listOldVilleHisto, 'json', ['picto']),
                'histoPicto'  => $this->serializerService->serializeArray($histoPicto, 'json', ['picto']),
                'listeSaison' => $this->ameHandler->getListeSaisonForCumul(),
                'listVoisin'  => $listVoisin,
                'nbrVille'    => $nbrVille,
                'commune'     => $listVilleCommune,
                'stats'       => $stats,
            ];
            
            return new JsonResponse([
                                        'ame'     => $ame,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'main');
        }
        
        
    }
    
    /**
     * @return Response
     */
    #[Route(path: '/classement', name: 'classement', methods: ['GET'])]
    public function mainClassement(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            $this->userHandler->verifyUserIsConnected();
            $listPictoProto = $this->pictoHandler->getListPicto();
            
            $listPicto      = $this->pictoHandler->getListPictoAll($listPictoProto);
            $villeUser      = $this->userHandler->getTownOfUser($this->user);
            $listPictoVille = ($villeUser !== null) ? $this->pictoHandler->getListPictoVille($listPictoProto, $villeUser) : [];
            
            $test = $this->entityManager->getRepository(HistoriquePictos::class)->recupHistoPictoForSaisonForOnePicto(16, $listPictoProto[0], 10);
            
            $classement = [
                'listPicto'       => $this->serializerService->serializeArray($listPictoProto, 'json', ['picto']),
                'classPicto'      => $this->serializerService->serializeArray($listPicto, 'json', ['classement', 'general_res']),
                'classPictoVille' => $this->serializerService->serializeArray($listPictoVille, 'json', ['classement', 'general_res']),
                'listeSaison'     => $this->ameHandler->getListeSaisonForCumul(),
            ];
            
            return new JsonResponse([
                                        'classement' => $classement,
                                        'general'    => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'mainClassement');
        }
        
    }
    
    /**
     * @param int $picto
     * @return JsonResponse
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route(path: '/classement/{picto}', name: 'classement_picto', requirements: ['picto' => '[0-9]{1,3}'], methods: ['GET'])]
    public function mainPicto(int $picto): JsonResponse
    {
        try {
            $pictoProto      = $this->pictoHandler->getPicto($picto);
            $classementPicto = $this->entityManager->getRepository(Pictos::class)->recupClassementPicto($pictoProto);
            
            $classementPictoJson =
                $this->serializerService->serializeArray($classementPicto, 'json', ['classement', 'general_res']);
            $pictoProtoJson      = $this->serializerService->serializeArray($pictoProto, 'json', ['picto']);
            
            return new JsonResponse(['picto' => $pictoProtoJson, 'classement' => $classementPictoJson],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'mainPicto');
        }
        
    }
    
    /**
     * @param int $picto
     * @return JsonResponse
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route(path: '/classement/{picto}/{saison}', name: 'classement_picto_saison', requirements: ['picto' => '[0-9]{1,3}', 'saison' => '\d+'], methods: ['GET'])]
    public function mainPictoSaison(int $picto, int $saison): JsonResponse
    {
        try {
            $pictoProto      = $this->pictoHandler->getPicto($picto);
            $classementPicto = $this->entityManager->getRepository(HistoriquePictos::class)->recupHistoPictoForSaisonForOnePicto($saison, $pictoProto);
            
            $classementPictoJson = $this->serializerService->serializeArray($classementPicto, 'json', ['classement', 'general_res']);
            $pictoProtoJson      = $this->serializerService->serializeArray($pictoProto, 'json', ['picto']);
            
            return new JsonResponse(['picto' => $pictoProtoJson, 'classement' => $classementPictoJson],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'mainPicto');
        }
        
    }
    
    #[Route(path: '/classement/saison/{saison}', name: 'classement_saison', requirements: ['saison' => '\d+'], methods: ['GET'])]
    public function pictoBySaison(Request $request, int $saison): Response
    {
        try {
            $this->userHandler->verifyUserIsConnected();
            $listPictoProto = $this->pictoHandler->getListPicto();
            $villeUser      = $this->userHandler->getTownOfUser($this->user);
            if ($saison === -1) {
                $classPicto     = $this->pictoHandler->getListPictoAll($listPictoProto);
                $listPictoVille = ($villeUser !== null) ? $this->pictoHandler->getListPictoVille($listPictoProto, $villeUser) : [];
            } else {
                $classPicto     = $this->pictoHandler->getListPictoAllForSaison($listPictoProto, $saison, 10);
                $listPictoVille = ($villeUser !== null) ? $this->pictoHandler->getListPictoAllForSaisonVille($listPictoProto, $saison, $villeUser, 10) : [];
            }
            
            return new JsonResponse([
                                        'classPicto'      => $this->serializerService->serializeArray($classPicto, 'json', ['classement', 'general_res']),
                                        'classPictoVille' => $this->serializerService->serializeArray($listPictoVille, 'json', ['classement', 'general_res']),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'pictoBySaison');
        }
    }
    
    /**
     * @param int $picto
     * @return JsonResponse
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route('/classement/maville/{picto}', name: 'classement_picto_maVille', requirements: ['picto' => '[0-9]{1,3}'], methods: ['GET'])]
    public function pictoVille(int $picto): JsonResponse
    {
        
        try {
            $pictoProto = $this->pictoHandler->getPicto($picto);
            
            $ville = $this->userHandler->getTownOfUser($this->user);
            if ($ville === null) {
                throw new GestHordesException("Utilisateur pas en ville | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            $classementPicto = $this->entityManager->getRepository(Pictos::class)->recupClassementPictoVille($pictoProto, $ville);
            
            $classementPictoJson = $this->serializerService->serializeArray($classementPicto, 'json', ['classement', 'general_res']);
            $pictoProtoJson      = $this->serializerService->serializeArray($pictoProto, 'json', ['picto']);
            
            return new JsonResponse(['picto' => $pictoProtoJson, 'classement' => $classementPictoJson],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'pictoVille');
        }
        
    }
    
    /**
     * @param int $picto
     * @param int $saison
     * @return JsonResponse
     */
    #[Route('/classement/maville/{picto}/{saison}', name: 'classement_picto_maVille_saison', requirements: ['picto' => '[0-9]{1,3}', 'saison' => '\d+'], methods: ['GET'])]
    public function pictoVilleSaison(int $picto, int $saison): JsonResponse
    {
        
        try {
            $pictoProto = $this->pictoHandler->getPicto($picto);
            
            $ville = $this->userHandler->getTownOfUser($this->user);
            if ($ville === null) {
                throw new GestHordesException("Utilisateur pas en ville | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            $classementPicto = $this->entityManager->getRepository(HistoriquePictos::class)->recupHistoPictoForSaisonForOnePictVilleo($ville, $saison, $pictoProto);
            
            $classementPictoJson = $this->serializerService->serializeArray($classementPicto, 'json', ['classement', 'general_res']);
            $pictoProtoJson      = $this->serializerService->serializeArray($pictoProto, 'json', ['picto']);
            
            return new JsonResponse(['picto' => $pictoProtoJson, 'classement' => $classementPictoJson],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'pictoVille');
        }
        
    }
    
    #[Route('/updatemysoul', name: 'updateMySoul', methods: ['GET'])]
    public function updateMySoul(): Response
    {
        try {
            $this->ameHandler->updateMySoul($this->user);
            
            return new JsonResponse([], Response::HTTP_OK);
            
        } catch (MyHordesAttackException|MyHordesMajApiException|MyHordesMajException|Exception $e) {
            return $this->errorHandler->handleException($e, 'AmeClassementRestController', 'updateMySoul');
        }
    }
    
    #[Route('/voisin/{saison}/{userId}', name: 'voisin', requirements: ['userId' => '\d+', 'saison' => '\d+(-alpha|-beta)?'], methods: ['GET'])]
    public function voisinUser(string $saison, int $userId = 0): Response
    {
        try {
            $user = $this->userHandler->resolveUser($userId);
            
            // on split la saison pour récupérer le numéro de la saison et la phase
            $splitSaison = explode('-', $saison);
            $saisonId    = $splitSaison[0];
            $phase       = (isset($splitSaison[1])) ? $splitSaison[1] : ($saisonId > 14 ? 'native' : 'import');

//            if ($saisonId == 15 && $phase === 'alpha') {
//                $saisonId = 0;
//            }
            
            return new JsonResponse([
                                        'listVoisin' => $this->villeService->getNeighbor($user, $saisonId, $phase),
                                        'nbrVille'   => $this->villeService->getNbrVille($user, $saisonId, $phase),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'AmeClassementRestController', 'voisinUser');
        }
        
        
    }
    
}