<?php

namespace App\Controller\Rest\OptionPerso;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\BatPrototype;
use App\Entity\Citoyens;
use App\Entity\ConsigneExpedition;
use App\Entity\CreneauHorraire;
use App\Entity\DispoUserTypeExpedition;
use App\Entity\Expedition;
use App\Entity\ExpeditionPart;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\MapItem;
use App\Entity\Menu;
use App\Entity\ThemeUser;
use App\Entity\TraceExpedition;
use App\Entity\TypeDispo;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Enum\LevelSkill;
use App\Enum\ListColor;
use App\Enum\MotifEpuisement;
use App\Exception\AuthenticationException;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\MenuHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Hotel\InscriptionExpeditionHandler;
use App\Service\OptionPerso\OptionPersoHandler;
use App\Service\Outils\ExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\ColorGeneratorHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Ville\CarteHandler;
use App\Structures\Dto\GH\OptionPerso\MajDispoUser;
use App\Structures\Dto\GH\OptionPerso\MajOptionPerso;
use App\Structures\Dto\GH\OptionPerso\MajThemeUser;
use App\Structures\Dto\GH\OptionPerso\SuppThemeUser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;


#[Route('/rest/v1/perso', name: 'rest_perso_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class OptionPersoRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface       $entityManager,
        protected UserHandler                  $userHandler,
        protected GestHordesHandler            $gh,
        protected Security                     $security,
        protected TranslatorInterface          $translator,
        protected LoggerInterface              $logger,
        protected TranslateHandler             $translateHandler,
        protected SerializerService            $serializerService,
        protected ErrorHandlingService         $errorHandler,
        protected DiscordService               $discordService,
        protected CarteHandler                 $carteHandler,
        protected ColorGeneratorHandler        $color,
        protected ErrorHandlingService         $errorHandlingService,
        protected OptionPersoHandler           $optionPersoHandler,
        protected ExpeditionHandler            $expeditionHandler,
        protected InscriptionExpeditionHandler $inscriptionExpeditionHandler,
        protected MenuHandler                  $menuHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/', name: 'main', methods: ['GET'])]
    public function main(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            
            $this->userHandler->verifyUserIsConnected();
            
            $listColor = ListColor::cases();
            
            $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
            
            foreach ($listCreneau as $creneau) {
                $creneau->setLibelle($this->translator->trans($creneau->getLibelle(), [], 'jump'));
            }
            
            if (count($this->user->getDispoTypes()?->toArray() ?? []) === 0) {
                
                $dispoBase = $this->entityManager->getRepository(TypeDispo::class)->findOneBy(['id' => 1]);
                foreach ($listCreneau as $creneau) {
                    $this->user->setDispoType(1, $creneau, $dispoBase);
                    $this->user->setDispoType(2, $creneau, $dispoBase);
                }
            }
            
            $listCreneauJson = $this->serializerService->serializeArray($listCreneau, 'json', ['option']);
            
            /**
             * @var int
             *  0 - aucune
             *  1 - première fois
             *  2 - date atteinte
             *  3 - maj forcé
             */
            $conditionOption = 0;
            
            $carte = $this->generateMiniCarte();
            
            for ($i = 0; $i <= 2; $i++) {
                $color[$i] = $this->color->randomColorHexa();
            }
            
            $delai     = $this->entityManager->getRepository(HerosPrototype::class)->delaiPouv();
            $delai_arr = array_combine(array_values($delai), $delai);
            
            $listPouvoirHeros = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
            $listDispo        = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
            
            $herosSkillType  = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
            $herosSkillLevel = $this->entityManager->getRepository(HerosSkillLevel::class)->getHerosSkillLevel($herosSkillType);
            
            $userOptions = $this->user;
            
            // Si l'utilisateur n'a pas de skill débloqué d'enregistrer en base, on lui créer les levels apprentis.
            if ($userOptions->getSkill()->count() === 0) {
                foreach ($herosSkillLevel as $skillLevel) {
                    if ($skillLevel->getLvl() === LevelSkill::APPRENTI) {
                        $userOptions->addSkill($skillLevel);
                    }
                }
            }
            
            // Si l'utilisateur n'a pas de skill type d'enregistrer en base, on lui créer les types de skill.
            if ($userOptions->getSkillType()->count() === 0) {
                foreach ($herosSkillLevel as $skillLevel) {
                    if ($skillLevel->getLvl() === LevelSkill::DEBUTANT) {
                        $userOptions->addSkillType($skillLevel);
                    }
                }
            }
            
            $userJson = $this->serializerService->serializeArray($userOptions, 'json', ['option']);
            
            $creneauDispoExpe = $this->inscriptionExpeditionHandler->recuperationTypologieCreneauExpedition();
            
            $listMenuForUser = $this->menuHandler->getPrototypeMenuListWithHabilitation($this->user);
            
            
            $optionsList = [
                'color'            => $color,
                'conditionOption'  => $conditionOption,
                'creneauList'      => $listCreneauJson,
                'delaiList'        => $delai_arr,
                'dispoList'        => $this->serializerService->serializeArray($listDispo, 'json', ['option']),
                'heroList'         => $this->serializerService->serializeArray($listPouvoirHeros, 'json', ['option']),
                'themeList'        => [
                    User::THEME_LIGHT   => "clair",
                    User::THEME_DARK    => "foncé",
                    User::THEME_VINTAGE => "vintage",
                    User::THEME_HORDES  => "hordes",
                ],
                'colormapList'     => $listColor,
                'baseThemeList'    => $this->userHandler->getBaseTheme(),
                'creneauDispoExpe' => $creneauDispoExpe,
                'motifEpuisement'  => array_map(fn(MotifEpuisement $motif) => $motif->value, MotifEpuisement::cases()),
                'menuPrototype'    => $this->serializerService->serializeArray($listMenuForUser, 'json', ['general']),
                'levelSkill'       => $this->serializerService->serializeArray($herosSkillLevel, 'json', ['option']),
                'skillType'        => $this->serializerService->serializeArray($herosSkillType, 'json', ['ency']),
            ];
            
            return new JsonResponse([
                                        'options' => [
                                            'carte'   => $carte,
                                            'options' => $optionsList,
                                            'user'    => $userJson,
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'OptionPersoRestController', 'main');
        }
        
        
    }
    
    #[Route('/maj_dispo_user', name: 'maj_dispo_user', methods: ['POST'])]
    public function maj_dispo_user(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            $majDispo = new MajDispoUser();
            $this->serializerService->deserialize($request->getContent(), MajDispoUser::class, 'json', $majDispo, ['option']);
            
            // On vérifie que l'utilisateur connecté est bien celui qui est modifié
            if ($majDispo->getUserId() !== $this->user->getId()) {
                throw new GestHordesException("Utilisateur connecté différent de l'utilisateur à modifier");
            }
            
            $dispo = $this->optionPersoHandler->majDispoExpeditionUser($this->user, $majDispo->getDispoType());
            
            return new JsonResponse(['dispo' => $this->serializerService->serializeArray($dispo, 'json', ['option'])], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandlingService->handleException($exception, 'OptionPersoRestController', 'maj_theme_user');
        }
    }
    
    #[Route('/maj_options', name: 'maj_options', methods: ['POST'])]
    public function maj_options(Request $request): JsonResponse
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        try {
            if ($this->user === null) {
                throw new GestHordesException("Utilisateur non connecté");
            }
            
            $majOption = new MajOptionPerso();
            $this->serializerService->deserialize($request->getContent(), MajOptionPerso::class, 'json', $majOption, ['option']);
            
            $userNew = $majOption->getUser();
            
            // Utilisation du service pour controler et mettre à jour le user
            $user = $this->optionPersoHandler->controleSaisieUtilisateur($userNew, $majOption->getThemeId());
            
            // Mise à jour des options
            $user->setDateMaj(new DateTime('now'));
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            
            $userModifier = $this->serializerService->serializeArray($user, 'json', ['option']);
            
            return new JsonResponse([
                                        'libRetour' => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'user'      => $userModifier,
                                        'general'   => $this->generateArrayGeneral($request, $mapIdSession),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandlingService->handleException($exception, 'OptionPersoRestController', 'maj_options');
        }
    }
    
    #[Route('/maj_theme_user', name: 'maj_theme_user', methods: ['POST'])]
    public function maj_themeUser(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                throw new GestHordesException("Utilisateur non connecté");
            }
            
            $majThemeUser = new MajThemeUser();
            
            $this->serializerService->deserialize($request->getContent(), MajThemeUser::class, 'json', $majThemeUser, ['option']);
            
            // controle si l'id de l'utilisateur est bien celui de l'utilisateur connecté
            if ($majThemeUser->getUserId() !== $this->user->getId()) {
                throw new GestHordesException("Utilisateur connecté différent de l'utilisateur à modifier");
            }
            
            // Utilisation du service pour controler et mettre à jour le user
            $themeUserModifier = $this->optionPersoHandler->controleSaisieUtilisateurForThemeUser($this->user, $majThemeUser->getThemeUser());
            
            // Si le top themeToSelect est à true, on met à jour le theme de l'utilisateur. On déselectionne les autres thèmes de l'utilisateur, puis on sélectionne le thème choisi
            if ($majThemeUser->isThemeToSelect()) {
                $this->optionPersoHandler->majThemeUserDeselect($this->user);
                
                $themeUserModifier->setSelected(true);
                $this->entityManager->persist($themeUserModifier);
                $this->user->setTheme($themeUserModifier->getBaseTheme());
                $this->entityManager->persist($this->user);
            } else {
                $this->entityManager->persist($themeUserModifier);
            }
            
            $this->entityManager->flush();
            
            // On récupère le user mis à jour
            $user = $this->entityManager->getRepository(User::class)->find($this->user->getId());
            
            $userModifier = $this->serializerService->serializeArray($user, 'json', ['option']);
            
            return new JsonResponse([
                                        'libRetour' => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'user'      => $userModifier,
                                        'general'   => $this->generateArrayGeneral($request, $mapIdSession),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandlingService->handleException($exception, 'OptionPersoRestController',
                                                                'maj_theme_user');
        }
    }
    
    #[Route('/save-menu', name: 'save_menu', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function save_menu(Request $request): JsonResponse
    {
        
        try {
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            // On récupère le menu de l'utilisateur s'il existe
            $menu = $this->entityManager->getRepository(Menu::class)
                                        ->createQueryBuilder("m")
                                        ->where('m.user = :user')
                                        ->setParameter('user', $this->user)
                                        ->setMaxResults(1)
                                        ->getQuery()
                                        ->getOneOrNullResult();
            if (!$menu) {
                // Si aucun menu n'existe, en créer un nouveau
                $menu = new Menu();
                $menu->setUser($this->user); // ou toute autre propriété nécessaire
            }
            
            $menuNew = new Menu();
            $this->serializerService->deserialize($request->getContent(), Menu::class, 'json', $menuNew, ['option']);
            
            $this->menuHandler->miseAJourMenu($menu, $menuNew);
            
            if ($menu->getItems()->count() > 0) {
                $this->entityManager->persist($menu);
                $this->entityManager->flush();
                $this->entityManager->refresh($menu);
                
                $serializedMenus = $this->serializerService->serializeArray($menu, 'json', ['option']);
            } else {
                $this->entityManager->remove($menu);
                $this->entityManager->flush();
                
                $serializedMenus = [];
            }
            
            return new JsonResponse(['menu' => $serializedMenus], Response::HTTP_OK, []);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception);
        }
    }
    
    #[Route('/suppDispoUser/{id}', name: 'supp_dispo_user', methods: ['DELETE'])]
    public function supp_dispo_user(DispoUserTypeExpedition $dispoUserTypeExpedition): JsonResponse
    {
        try {
            
            if ($this->user === null) {
                throw new AuthenticationException();
            }
            
            if ($dispoUserTypeExpedition->getUser()->getId() !== $this->user->getId()) {
                throw new GestHordesException("Utilisateur connecté différent de l'utilisateur à modifier");
            }
            
            $this->entityManager->remove($dispoUserTypeExpedition);
            $this->entityManager->flush();
            
            return new JsonResponse([], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandlingService->handleException($exception, 'OptionPersoRestController', 'maj_theme_user');
        }
    }
    
    #[Route('/supp_theme_user', name: 'supp_theme_user', methods: ['POST'])]
    public function supp_themeUser(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                throw new GestHordesException("Utilisateur non connecté");
            }
            
            /** @var SuppThemeUser $majThemeUser */
            $suppThemeUser = new SuppThemeUser();
            $this->serializerService->deserialize($request->getContent(), SuppThemeUser::class, 'json', $suppThemeUser, ['option']);
            
            // controle si l'id de l'utilisateur est bien celui de l'utilisateur connecté
            if ($suppThemeUser->getUserId() !== $this->user->getId()) {
                throw new GestHordesException("Utilisateur connecté différent de l'utilisateur à modifier");
            }
            
            // Recherche du themeUser à supprimer
            $themeUser = $this->entityManager->getRepository(ThemeUser::class)->find($suppThemeUser->getThemeUserId());
            
            // Si le themeUser n'existe pas, on renvoie une erreur
            if ($themeUser === null) {
                throw new GestHordesException("ThemeUser non trouvé");
            }
            
            // On verifie que le themeUser à supprimer appartient bien à l'utilisateur connecté
            if ($themeUser->getUser()->getId() !== $this->user->getId()) {
                throw new GestHordesException("ThemeUser n'appartient pas à l'utilisateur connecté");
            }
            
            // On verifie que le themeUser à supprimer n'est pas l'un des themes par défaut appartenant à la liste des themes par défaut
            if (in_array($themeUser->getBaseTheme(), User::LIST_THEME_DEFAULT)) {
                throw new GestHordesException("ThemeUser fait partie des themes par défaut, impossible de le supprimer - ThemeId : {$themeUser->getId()}");
            }
            
            // Si le themeUser à supprimer est le themeUser sélectionné, on selectionne le themeUser par défaut
            if ($themeUser->isSelected()) {
                $themeUserDefaut =
                    $this->entityManager->getRepository(ThemeUser::class)->findOneBy(['user'      => $this->user,
                                                                                      'baseTheme' => User::THEME_HORDES]);
                $themeUserDefaut->setSelected(true);
                $this->entityManager->persist($themeUserDefaut);
                $this->user->setTheme($themeUserDefaut->getBaseTheme());
                $this->entityManager->persist($this->user);
            }
            
            // On supprime le themeUser
            $this->entityManager->remove($themeUser);
            
            $this->entityManager->flush();
            
            // On récupère le user mis à jour
            $user = $this->entityManager->getRepository(User::class)->find($this->user->getId());
            
            $userModifier = $this->serializerService->serializeArray($user, 'json', ['option']);
            
            return new JsonResponse([
                                        'libRetour' => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'user'      => $userModifier,
                                        'general'   => $this->generateArrayGeneral($request, $mapIdSession),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandlingService->handleException($exception, 'OptionPersoRestController',
                                                                'supp_themeUser');
        }
    }
    
    private function generateMiniCarte(): array
    {
        $serializer = $this->gh->getSerializer();
        $format     = 'json';
        
        $villeMc = new Ville(0, Ville::ORIGIN_MH);
        
        $villeMc->setPosX(3)
                ->setPosY(5)
                ->setJour(6)
                ->setWeight(7)
                ->setHeight(7)
                ->setHard(false)
                ->setDevast(false)
                ->setNom('Test');
        
        $zombie        = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 6, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [3, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 3, 6, 0],
        ];
        $pdc           = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [0, 0, 3, 0, 0, 0, 0],
            [6, 0, 3, 1, 0, 0, 0],
            [2, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $tag           = [
            [0, 0, 0, 0, 0, 5, 5],
            [5, 0, 5, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 5, 5],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 5, 5, 5, 5, 6],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 5, 5, 0, 6, 0, 0],
        ];
        $danger        = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 3, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [2, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 2, 3, 0],
        ];
        $day           = [
            [0, 0, 5, 5, 5, 5, 5],
            [5, 6, 0, 0, 0, 0, 5],
            [1, 5, 6, 5, 5, 5, 1],
            [6, 1, 6, 6, 0, 0, 5],
            [6, 0, 1, 5, 5, 0, 1],
            [6, 6, 6, 6, 5, 1, 1],
            [0, 5, 0, 1, 1, 6, 1],
        ];
        $dried         = [
            [0, 0, 0, 1, 1, 1, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 1, 1, 0, 0],
            [0, 0, 0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0, 1, 0],
        ];
        $vue           = [
            [-1, -1, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, -2, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
        ];
        $batId         = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 61, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 17, 0, 0, 14, 0],
            [-1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $camped        = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $vide          = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $dig           = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [10, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $km            = [
            [6, 5, 5, 5, 5, 5, 6],
            [5, 4, 4, 4, 4, 4, 5],
            [4, 4, 3, 3, 3, 4, 4],
            [4, 3, 2, 2, 2, 3, 4],
            [3, 2, 1, 1, 1, 2, 3],
            [3, 2, 1, 0, 1, 2, 3],
            [3, 2, 1, 1, 1, 2, 3],
        ];
        $bord_km       = [
            [1, 9, 1, 1, 1, 3, 1],
            [9, 9, 1, 1, 1, 3, 3],
            [9, null, 9, 1, 3, null, 3],
            [null, 9, 9, 1, 3, 3, null],
            [9, 9, 9, 1, 3, 3, 3],
            [8, 8, 8, null, 2, 2, 2],
            [12, 12, 12, 4, 6, 6, 6],
        ];
        $pa            = [
            [8, 7, 6, 5, 6, 7, 8],
            [7, 6, 5, 4, 5, 6, 7],
            [6, 5, 4, 3, 4, 5, 6],
            [5, 4, 3, 2, 3, 4, 5],
            [4, 3, 2, 1, 2, 3, 4],
            [3, 2, 1, 0, 1, 2, 3],
            [4, 3, 2, 1, 2, 3, 4],
        ];
        $bord_pa       = [
            [9, 9, 9, 11, 3, 3, 3],
            [9, 9, 9, 11, 3, 3, 3],
            [9, 9, 9, 11, 3, 3, 3],
            [9, 9, 9, 11, 3, 3, 3],
            [9, 9, 9, 11, 3, 3, 3],
            [13, 13, 13, null, 7, 7, 7],
            [12, 12, 12, 14, 6, 6, 6],
        ];
        $bord_scrut    = [
            [null, 8, null, null, null, 2, 0],
            [null, 12, 32, null, 26, 6, null],
            [null, null, 8, null, 2, null, null],
            [null, null, 12, 58, 6, null, null],
            [1, 3, null, 10, null, 9, 1],
            [null, 49, 5, null, 5, 61, null],
            [4, 6, null, 10, null, 12, 4],
        ];
        $zonage        = [
            [13, 11, 9, 9, 9, 11, 13],
            [11, 9, 7, 7, 7, 9, 11],
            [9, 7, 5, 5, 5, 7, 9],
            [7, 5, 3, 3, 3, 5, 7],
            [5, 3, 3, 3, 3, 3, 5],
            [5, 3, 3, 3, 3, 3, 5],
            [5, 3, 3, 3, 3, 3, 5],
        ];
        $bord_zonage   = [
            [9, 9, 9, 1, 3, 3, 3],
            [9, 9, 9, 1, 3, 3, 3],
            [9, 9, 9, 1, 3, 3, 3],
            [9, 9, 9, 1, 3, 3, 3],
            [9, 9, null, null, null, 3, 3],
            [8, 8, null, null, null, 2, 2],
            [12, 12, null, null, null, 6, 6],
        ];
        $itemCarte     = [
            [0, 0, 0, 1, 1, 0, 0],
            [0, 0, 8, 1, 0, 7, 1],
            [0, 0, 0, 10, 0, 0, 0],
            [9, 0, 0, 1, 0, 0, 0],
            [1, 5, 0, 1, 4, 0, 0],
            [1, 3, 0, 0, 1, 2, 0],
            [0, 1, 2, 1, 1, 3, 5],
        ];
        $item2Carte    = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1],
        ];
        $consigne      = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        $consigneFinis = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ];
        
        $item = new MapItem($this->entityManager->getRepository(ItemPrototype::class)->find(49), 0);
        $item->setNombre(1);
        $item2 = new MapItem($this->entityManager->getRepository(ItemPrototype::class)->find(2), 0);
        $item2->setNombre(1);
        
        
        $zoneJson   = [];
        $listZonage = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27];
        
        
        foreach ($zombie as $y => $lignx) {
            foreach ($lignx as $x => $val) {
                $zone = new ZoneMap();
                
                
                $zone->setX($x)
                     ->setY($y)
                     ->createId($x, $y, 0)
                     ->setZombie($val)
                     ->setVue($vue[$y][$x])
                     ->setTag($tag[$y][$x])
                     ->setDanger($danger[$y][$x])
                     ->setDay($day[$y][$x])
                     ->setDried($dried[$y][$x])
                     ->setCamped($camped[$y][$x] === 1)
                     ->setEmpty($vide[$y][$x] === 1)
                     ->setKm($km[$y][$x])
                     ->setPa($pa[$y][$x])
                     ->setZone($zonage[$y][$x])
                     ->setBordKm($bord_km[$y][$x])
                     ->setBordPa($bord_pa[$y][$x])
                     ->setBordScrut($bord_scrut[$y][$x])
                     ->setBordZonage($bord_zonage[$y][$x])
                     ->setDig(0)
                     ->setCitoyen(null)
                     ->setPdc($pdc[$y][$x])
                     ->setHeureMaj(null);
                
                
                if ($consigne[$y][$x] === 1) {
                    $trace = new TraceExpedition();
                    $trace->setId('3');
                    $exped = new ExpeditionPart();
                    $exped->setTrace($trace);
                    $expedition = new Expedition();
                    $expedition->setId('3')
                               ->addExpeditionPart($exped);
                    $consigneobj = new ConsigneExpedition();
                    $consigneobj->setId(1)
                                ->setExpeditionPart($exped)
                                ->setFait(false);
                    $zone->addConsigneExpedition($consigneobj);
                }
                if ($consigneFinis[$y][$x] === 1) {
                    $trace = new TraceExpedition();
                    $trace->setId('3');
                    $exped = new ExpeditionPart();
                    $exped->setTrace($trace);
                    $expedition = new Expedition();
                    $expedition->setId('3')
                               ->addExpeditionPart($exped);
                    $consigneobj = new ConsigneExpedition();
                    $consigneobj->setId(1)
                                ->setExpeditionPart($exped)
                                ->setFait(true);
                    $zone->addConsigneExpedition($consigneobj);
                }
                
                if ($batId[$y][$x] !== 0) {
                    $bat = $this->entityManager->getRepository(BatPrototype::class)->find($batId[$y][$x]);
                    
                    $zone->setBat($bat)
                         ->setDig($dig[$y][$x]);
                } else {
                    $zone->setBat(null)
                         ->setDig(0);
                }
                
                
                $zone->setIndicVisite($this->carteHandler->recupIndicVisit($villeMc->getJour(), $zone->getDay() ?? 0, $zone->getVue()));
                // Calcul de l'estimation des zombies
                if ($zone->getVue() !== ZoneMap::NON_EXPLO && $zone->getVue() !== ZoneMap::VILLE) {
                    $zone->setZombieEstim(($zone->getZombie() ?? 0) + abs(6 - ($zone->getDay() ?? 1)));
                } else {
                    $zone->setZombieEstim(null);
                }
                
                
                if ($itemCarte[$y][$x] >= 1 || $item2Carte[$y][$x] === 1) {
                    $carteAlter[112]   = 0;
                    $itemSolArray      = [];
                    $itemSolArrayIcone = [];
                    
                    if ($itemCarte[$y][$x] >= 1) {
                        $zone->addItem($item);
                        $carteAlter[112] = $itemCarte[$y][$x];
                        if (!isset($itemSolArray[1])) {
                            $itemSolArray[1] = true;
                        }
                        if (!isset($itemSolArrayIcone[1])) {
                            $itemSolArrayIcone[1] = $item->getItem()->getIcon();
                        }
                    }
                    if ($item2Carte[$y][$x] === 1) {
                        $zone->addItem($item2);
                        if (!isset($itemSolArray[2])) {
                            $itemSolArray[2] = true;
                        }
                        if (!isset($itemSolArrayIcone[2])) {
                            $itemSolArrayIcone[2] = $item2->getItem()->getIcon();
                        }
                    }
                    $itemsSol      = array_keys($itemSolArray);
                    $itemsSolIcone = $itemSolArrayIcone;
                } else {
                    $carteAlter[112] = null;
                    $itemsSol        = [];
                    $itemsSolIcone   = [];
                }
                
                $zone->setCarteAlter($carteAlter)
                     ->setItemSol($itemsSol)
                     ->setItemSolIcone($itemsSolIcone);
                
                $villeMc->addZone($zone);
            }
        }
        
        $userGen = (new User())->setId(0)->setPseudo('test');
        $job     = $this->entityManager->getRepository(JobPrototype::class)->find(5);
        $derPouv = $this->entityManager->getRepository(HerosPrototype::class)->find(5);
        
        $userGen->setDerPouv($derPouv);
        
        $userGenSelect = (new User())->setId(1)->setPseudo('test');
        $userGen2      = (new User())->setId(2)->setPseudo('test');
        $userGen3      = (new User())->setId(3)->setPseudo('test');
        $userGen4      = (new User())->setId(4)->setPseudo('test');
        $userGenSelect->setDerPouv($derPouv);
        $userGen2->setDerPouv($derPouv);
        $userGen3->setDerPouv($derPouv);
        $userGen4->setDerPouv($derPouv);
        
        $villeMc->addCitoyen((new Citoyens())->setX(2)->setY(3)->setCitoyen($userGen)->setJob($job)->setMort(false));
        $villeMc->addCitoyen((new Citoyens())->setX(3)->setY(3)->setCitoyen($this->user)->setJob($job)->setMort(false));
        $villeMc->addCitoyen((new Citoyens())->setX(0)->setY(4)->setCitoyen($userGen4)->setJob($job)->setMort(false));
        $villeMc->addCitoyen((new Citoyens())->setX(0)->setY(3)->setCitoyen($userGen2)->setJob($job)->setMort(false));
        $villeMc->addCitoyen((new Citoyens())->setX(1)->setY(1)->setCitoyen($userGen3)->setJob($job)->setMort(false));
        $villeMc->addCitoyen((new Citoyens())->setX(2)->setY(2)->setCitoyen($userGenSelect)->setJob($job)
                                             ->setMort(false));
        
        
        $arrayCitoyenVie   = [];
        $arrayCitoyenVille = [];
        
        foreach ($villeMc->getCitoyens() as $citoyen) {
            
            if (!$citoyen->getMort() && !$villeMc->isDevast()) {
                $arrayCitoyenVie[$citoyen->getY() * 100 + $citoyen->getX()][] = $citoyen;
                
                if ($citoyen->getCitoyen()->getId() === $this->user->getId()) {
                    $positionUser = $citoyen->getY() * 100 + $citoyen->getX();
                }
                
            }
            $arrayCitoyenVille[$citoyen->getCitoyen()->getId()] = $citoyen;
            
        }
        $villeMc->setListCitoyenVie($arrayCitoyenVie)
                ->setListCitoyenVille($arrayCitoyenVille)
                ->setMaPosition($positionUser ?? 0);
        
        $listBat = $this->entityManager->getRepository(BatPrototype::class)->findAll();
        
        
        $listBatJson = $this->serializerService->serializeArray($listBat, $format, ['carte']);
        
        $villeSerialize = $this->serializerService->serializeArray($villeMc, $format, ['carte', 'carte_gen']);
        
        $userJson = $this->serializerService->serializeArray($this->user, $format, ['carte_user']);
        
        return [
            'campingActif'           => false,
            'carteExpe'              => [],
            'carteExpeBiblio'        => [],
            'carteExpeBrouillon'     => [],
            'carteExpeHordes'        => [],
            'carteOptionPerso'       => true,
            'carteOptionPerso_alter' => false,
            'constructionChantier'   => [
                'tdg'    => $tdgConstruit ?? false,
                'planif' => $planifConstruit ?? false,
                'scrut'  => $scrutConstruit ?? false,
                'phare'  => $phareConstruit ?? false,
            ],
            'couleurHordes'          => $listCouleurMyHordes ?? [],
            'listBat'                => $listBatJson,
            'listExp'                => [],
            'listExpHordes'          => [],
            'listExpInscrit'         => ['3'],
            'listRuine'              => [],
            'lvlTdG'                 => 0,
            'maxAlter'               => [112 => 10],
            'maxAlterAll'            => 10,
            'myVille'                => 0,
            'optionPopUpClick'       => $this->user->getUserPersonnalisation()->isPopUpClick() ?? false,
            'theme'                  => $this->user->getTheme() ?? User::THEME_LIGHT,
            'user'                   => $userJson,
            'ville'                  => $villeSerialize,
        ];
        
    }
    
}