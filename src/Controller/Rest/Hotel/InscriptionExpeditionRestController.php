<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Outils;
use App\Entity\TraceExpedition;
use App\Exception\GestHordesException;
use App\Security\Voter\InVilleVoter;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Hotel\InscriptionExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Hotel\Expeditions\ConsigneValidation;
use App\Structures\Dto\GH\Hotel\Expeditions\InscriptionExpeditionSauvegarde;
use App\Structures\Dto\GH\Hotel\Expeditions\SauvegardeCommentaire;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/inscription_expedition', name: 'rest_inscription_expedition_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class InscriptionExpeditionRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface       $entityManager,
        protected UserHandler                  $userHandler,
        protected GestHordesHandler            $gh,
        protected Security                     $security,
        protected TranslatorInterface          $translator,
        protected LoggerInterface              $logger,
        protected TranslateHandler             $translateHandler,
        protected SerializerService            $serializerService,
        protected ErrorHandlingService         $errorHandler,
        protected DiscordService               $discordService,
        protected InscriptionExpeditionHandler $inscriptionExpeditionHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger, $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/appercu/{id}', name: 'appercu_trace', methods: ['GET'])]
    public function appercu_trace(Request $request, TraceExpedition $traceExpedition): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            
            return new JsonResponse(['appercu' => $this->inscriptionExpeditionHandler->generationMiniMap($ville, $this->user, $traceExpedition)], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'validation_consigne');
        }
    }
    
    #[Route('/archive/{mapId}/{jour}', name: 'archive', requirements: ['mapId' => '\d+', 'jour' => '\d+'], methods: ['GET'])]
    public function archive(Request $request, int $mapId = 0, int $jour = 0): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            if ($mapIdSession !== $mapId) {
                throw new GestHordesException('La ville fournie ne correspond pas à la ville du header');
            }
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapId);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            
            return new JsonResponse([
                                        'archive' => $this->inscriptionExpeditionHandler->getArchiveInscriptionExpedition($ville, $jour),
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'main');
        }
        
        
    }
    
    #[Route('/editCommentaireExpe', name: 'edit_commentaire_expe', methods: ['POST'])]
    public function editCommentaireExpe(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $data = new SauvegardeCommentaire();
            
            $this->serializerService->deserialize($request->getContent(), SauvegardeCommentaire::class, 'json', $data);
            
            // controle que l'outils expédition existe bien et qu'il y a bien un outils expédition pour le jour de la ville
            $outilsExpedition = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            if ($outilsExpedition === null) {
                throw new GestHordesException('Outils non trouvé');
            }
            if ($outilsExpedition->getExpedition() === null) {
                throw new GestHordesException('Outils expedition non trouvé');
            }
            
            // On traite la consigne
            $this->inscriptionExpeditionHandler->sauvegardeCommentaire($ville, $outilsExpedition->getExpedition(), $data);
            
            return new JsonResponse([], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'validation_consigne');
        }
    }
    
    #[Route('/expedition/{id}', name: 'expedition', requirements: ['id' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function expedition_joueur(Request $request, string $id = ""): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $expeditionJson = $this->serializerService->serializeArray($this->inscriptionExpeditionHandler->recupererExpeditionPart($ville, $id), 'json', ['inscription_exp_visu', 'general_res']);
            
            $creneaux = $this->inscriptionExpeditionHandler->recuperationTypologieCreneauExpedition();
            
            $userOption = $this->serializerService->serializeArray($this->user, 'json', ['outils_expe', 'expe']);
            
            return new JsonResponse(['expedition' => $expeditionJson, 'creneaux' => $creneaux['creneau'], 'userOption' => $userOption], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'expedition_joueur');
        }
    }
    
    #[Route('/{mapId}', name: 'main', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId = 0): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            if ($mapIdSession !== $mapId) {
                throw new GestHordesException('La ville fournie ne correspond pas à la ville du header');
            }
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapId);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            
            return new JsonResponse([
                                        'inscription' => $this->inscriptionExpeditionHandler->getInscriptionExpedition($ville),
                                        'general'     => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'main');
        }
        
        
    }
    
    #[Route('/sauvegarder', name: 'sauvegarder', methods: ['POST'])]
    public function sauvegarder(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $data = new InscriptionExpeditionSauvegarde();
            
            $this->serializerService->deserialize($request->getContent(), InscriptionExpeditionSauvegarde::class, 'json', $data, ['outils_expe', 'general']);
            
            // controle que l'outils expédition existe bien et qu'il y a bien un outils expédition pour le jour de la ville
            $outilsExpedition = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            if ($outilsExpedition === null) {
                throw new GestHordesException('Outils non trouvé');
            }
            if ($outilsExpedition->getExpedition() === null) {
                throw new GestHordesException('Outils expedition non trouvé');
            }
            
            // Si on traite des expeditions
            if ($data->getExpedition() !== null) {
                $this->inscriptionExpeditionHandler->sauvegarderInscriptionExpedition($outilsExpedition->getExpedition(), $data);
            } elseif ($data->getOuvrier() !== null) {
                $this->inscriptionExpeditionHandler->sauvegarderInscriptionExpeditionOuvrier($outilsExpedition->getExpedition(), $data);
            }
            
            return new JsonResponse([], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'sauvegarder');
        }
        
        
    }
    
    #[Route('/validConsigne', name: 'validation_consigne', methods: ['POST'])]
    public function validation_consigne(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException('Ville non trouvée');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $data = new ConsigneValidation();
            
            $this->serializerService->deserialize($request->getContent(), ConsigneValidation::class, 'json', $data);
            
            // controle que l'outils expédition existe bien et qu'il y a bien un outils expédition pour le jour de la ville
            $outilsExpedition = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            if ($outilsExpedition === null) {
                throw new GestHordesException('Outils non trouvé');
            }
            if ($outilsExpedition->getExpedition() === null) {
                throw new GestHordesException('Outils expedition non trouvé');
            }
            
            // On traite la consigne
            $this->inscriptionExpeditionHandler->validationConsigne($ville, $outilsExpedition->getExpedition(), $data);
            
            return new JsonResponse([], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'InscriptionExpeditionRestController', 'validation_consigne');
        }
    }
    
}