<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Estimation;
use App\Entity\Journal;
use App\Structures\Collection\Statistiques;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/rest/v1/stats', name: 'rest_stats_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class StatistiquesRestController extends AbstractRestGestHordesController
{
    
    #[Route(path: '/{mapId}', name: 'hotel_stats', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId): Response
    {
        $this->ville = $this->userHandler->getTownBySession($mapId);
        
        if ($this->ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        $stats = new Statistiques();
        
        $minMaxEstimTmp = $this->entityManager->getRepository(Estimation::class)->minMaxEstimation($this->ville->getSaison());
        $minMaxEstim    = [];
        foreach ($minMaxEstimTmp as $minMax) {
            $minMaxEstim[$minMax['day']] = $minMax;
        }
        
        $minMaxAttkTmp = $this->entityManager->getRepository(Journal::class)->moyenneAttaque($this->ville->getSaison());
        $minMaxAttk    = [];
        
        $maxTheorique = [];
        for ($current_day = 1; $current_day <= 50; $current_day++) {
            $ratio_max                  = ($current_day <= 3 ? ($current_day <= 1 ? 0.4 : 0.66) : 1.0) * 1.1;
            $maxTheorique[$current_day] = round($ratio_max * ($current_day * 0.75 + 3.5) ** 3) * 1.2 * 1.1;
        }
        
        foreach ($minMaxAttkTmp as $minMax) {
            $jour = $minMax['day'] - 1;
            if ($maxTheorique[$jour] >= $minMax['zombie']) {
                $minMaxAttk[$jour][] = $minMax;
            }
        }
        ksort($minMaxAttk);
        
        
        $stats->setDefenses($this->ville->getDefense())
              ->setEstimation($this->ville->getEstimation())
              ->setJournaux($this->ville->getJournal())
              ->setEstimMinMax($minMaxEstim)
              ->setAttaqueMoy($minMaxAttk);
        $maxJour    = $this->ville->getDefense()->last()->getDay();
        $maxAtkJour = round($maxTheorique[$maxJour + 2] * 1.1 / 100) * 100;
        $array_atk  = $stats->recupAtkArray();
        
        $array_react['def']        = $stats->recupDefArray();
        $array_react['atk']        = $array_atk;
        $array_react['maxJour']    = $maxJour + 3;
        $array_react['maxAtkJour'] = $maxAtkJour;
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'stats'   => $array_react,
            'general' => $this->generateArrayGeneral($request, $mapId),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
}
