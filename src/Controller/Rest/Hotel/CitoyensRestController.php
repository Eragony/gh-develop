<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\CategoryObjet;
use App\Entity\Citoyens;
use App\Entity\CoffreCitoyen;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\LeadJump;
use App\Entity\UpHomePrototype;
use App\Entity\Ville;
use App\Enum\LevelSkill;
use App\Exception\GestHordesException;
use App\Security\Voter\InVilleVoter;
use App\Service\ErrorHandlingService;
use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Hotel\Citoyens\CitoyensMajRest;
use App\Structures\Dto\GH\Hotel\Citoyens\CompetenceCitoyensMajRest;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/citoyens', name: 'rest_citoyens_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class CitoyensRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected LocalFormatterUtils    $lfu,
        protected ItemsHandler           $itemsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/filtre', name: 'filtre', methods: ['POST'])]
    public function filtre(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        $data = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
        
        // Vérification si les champs sont non null
        if ($data->mapId === null || $data->userId === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La data fournis est incomplète filtre citoyen - {$this->user->getId()}.");
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->userId) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non cohérant : {$data->userId} <> {$this->user->getId()}");
            
            return new JsonResponse($retour);
        }
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->mapId]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : " . $data->mapId);
            
            return new JsonResponse($retour);
        }
        
        
        if (!$this->userHandler->myVille($ville)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Il n'est pas possible de filtrer sur une ville qui est pas autre que le user : " .
                                 $data->mapId);
            
            return new JsonResponse($retour);
        }
        
        if ($data->filtre == '') {
            
            $citoyensVie = $this->ville->getCitoyens()->filter(
                fn(Citoyens $c) => !$c->getMort(),
            );
            
        } else {
            
            $filtre = $data->filtre;
            
            $queryBuilder = new QueryBuilder($this->entityManager);
            
            
            $queryBuilder->select('c')
                         ->from('App:Citoyens', 'c')
                         ->from('App:User', 'u')
                         ->from('App:HerosPrototype', 'h');
            
            $queryBuilder->where('c.citoyen = u.id');
            $queryBuilder->andWhere('u.derPouv = h.id');
            
            $queryBuilder->andWhere('c.ville = :ville');
            $queryBuilder->andWhere('c.mort = 0');
            $queryBuilder->setParameter(':ville', $ville);
            
            $queryBuilder->andWhere($data->filtre->sql);
            
            foreach ($filtre->params as $key => $param) {
                $queryBuilder->setParameter($key, $param);
            }
            
            
            //dd($queryBuilder->getQuery());
            
            $citoyensVie = new ArrayCollection($queryBuilder->getQuery()->getResult());
            
        }
        
        
        try {
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'zoneRetour' => [
                                            'citoyen' => $this->serializerService->serializeArray($citoyensVie, 'json', ['citoyens']),
                                        ]]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Probleme filtre citoyens {$data->mapId} : " . $exception->getMessage());
            
            return new JsonResponse($retour);
        }
    }
    
    /**
     * @throws Exception
     */
    #[Route(path: '/{mapId}', name: 'hotel_citoyen', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId = 0): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $this->ville  = $this->userHandler->getTownBySession($mapId);
        
        if ($this->ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        
        $compteur = [];
        
        
        $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAll();
        
        foreach ($listJob as $job) {
            $compteur['job'][$job->getId()]['nom']  = $job->getNom();
            $compteur['job'][$job->getId()]['icon'] = $job->getIcon();
            $compteur['job'][$job->getId()]['id']   = $job->getId();
            $compteur['job'][$job->getId()]['nbr']  = 0;
        }
        
        $listHabitation = $this->entityManager->getRepository(HomePrototype::class)->findAll();
        
        foreach ($listHabitation as $hab) {
            $compteur['hab'][$hab->getId()]['nom']  = $hab->getNom();
            $compteur['hab'][$hab->getId()]['icon'] = $hab->getIcon();
            $compteur['hab'][$hab->getId()]['id']   = $hab->getId();
            $compteur['hab'][$hab->getId()]['nbr']  = 0;
            
        }
        
        
        $listPvCpt = $this->entityManager->getRepository(HerosPrototype::class)->findPouvActif();
        foreach ($listPvCpt as $pouv) {
            
            $compteur['pv'][$pouv->getOrdreRecup()]['nom']  = $pouv->getNom();
            $compteur['pv'][$pouv->getOrdreRecup()]['icon'] = $pouv->getIcon();
            // Convertir la première lettre en minuscule
            // Insérer un underscore avant chaque lettre majuscule
            // Convertir la chaîne en minuscules
            $compteur['pv'][$pouv->getOrdreRecup()]['method'] =
                strtolower(preg_replace('/([A-Z])/', '_$1', lcfirst((string)$pouv->getMethod())));
            $compteur['pv'][$pouv->getOrdreRecup()]['id']     = $pouv->getOrdreRecup();
            $compteur['pv'][$pouv->getOrdreRecup()]['nbr']    = 0;
            
        }
        
        /**
         * {% if infoVille.jump is not null %}
         * {% if infoVille.jump.inscriptionJump(citoyen.citoyen.id) is not null %}
         * {% if infoVille.jump.inscriptionJump(citoyen.citoyen.id).typeDiffusion == 0 %}
         * <div class="moyenContactCitoyenV">
         * <span class="infoBulle">
         * <i class="fa-solid fa-phone"></i>
         * <span class="info">{{ infoVille.jump.inscriptionJump(citoyen.citoyen.id).dechiffreMoyenContact(gh) |raw }}</span>
         * </span>
         * </div>
         * {% else %}
         * {% if isLead or isOrga %}
         * <div class="moyenContactCitoyenV">
         * <span class="infoBulle">
         * <i class="fa-solid fa-phone"></i>
         * <span class="info">{{ infoVille.jump.inscriptionJump(citoyen.citoyen.id).dechiffreMoyenContact(gh) | raw }}</span>
         * </span>
         * </div>
         * {% endif %}
         * {% endif %}
         * {% endif %}
         * {% endif %}
         */
        
        $jump = null;
        
        if ($mapId == 0 || $mapId == $this->user?->getMapId() ?? null) {
            
            $jump = $this->ville->getJump();
            
        }
        
        
        $citoyensMort = [];
        $citoyensVie  = [];
        $moyenContact = [];
        foreach ($this->ville->getCitoyens() as $citoyen) {
            if ($citoyen->getMort()) {
                $citoyensMort[] = $citoyen;
            } else {
                if ($citoyen->getCoffres()->count() !== 0) {
                    foreach ($citoyen->getCoffres() as $item) {
                        $itemCoffre = $item->getItem();
                    }
                }
                
                
                if ($jump !== null) {
                    
                    $inscriptionCitoyen = $jump->getInscriptionJump($citoyen->getCitoyen()->getId());
                    
                    if ($inscriptionCitoyen !== null) {
                        
                        if ($inscriptionCitoyen->getTypeDiffusion() == 0 ||
                            ($inscriptionCitoyen->getTypeDiffusion() == 1 && $this->userHandler->isLeadOrOrga($jump))
                        ) {
                            $moyenContact[$citoyen->getCitoyen()->getId()] =
                                $inscriptionCitoyen->dechiffreMoyenContact($this->gh);
                        }
                        
                        
                    }
                    
                }
                
                
                $citoyensVie[] = $citoyen;
                
            }
        }
        
        
        $listPouvoirHeros = $this->entityManager->getRepository(HerosPrototype::class)->findAll();
        
        $listUpHome = $this->entityManager->getRepository(UpHomePrototype::class)->findAll();
        
        
        $formCitoyen = [];
        $isLead      = false;
        $isOrga      = false;
        if ($this->ville->getJump() !== null && $this->user !== null) {
            $leads = $this->entityManager->getRepository(LeadJump::class)->findBy(['user' => $this->user,
                                                                                   'jump' => $this->ville->getJump()]);
            if (count($leads) > 0) {
                $isLead = true;
            }
            
            $orga = $this->ville->getJump()->getGestionnaire($this->user->getId());
            
            if ($orga !== null) {
                $isOrga = true;
            }
        }
        
        if ($mapId == 0 || ($mapId == $this->user?->getMapId() ?? null) || $isLead || $isOrga) {
            
            $maxlvlCuisine   = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'cuisine']))->getLevels()->count();
            $maxlvlLabo      =
                ($this->entityManager->getRepository(UpHomePrototype::class)->findOneBy(['nom' => 'labo']))->getLevels()
                                                                                                           ->count();
            $maxlvlRenfort   = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'renfort']))->getLevels()->count();
            $maxlvlRangement = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'rangement']))->getLevels()->count();
            $maxlvlCS        =
                ($this->entityManager->getRepository(UpHomePrototype::class)->findOneBy(['nom' => 'cs']))->getLevels()
                                                                                                         ->count();
            
            $listPouvoirHeros = array_filter($listPouvoirHeros, fn(HerosPrototype $i) => $i->getJour() > 0);
            
            $formCitoyen = [
                'divers' => [
                    'chargeApag' => [
                        'type'    => 'select',
                        'label'   => 'Charges apag :',
                        'choices' => [0, 1, 2, 3, 4],
                    ],
                    'nbCamping'  => [
                        'type'    => 'select',
                        'label'   => 'Nb camping :',
                        'choices' => range(0, 10),
                    ],
                    'immuniser'  => [
                        'type'  => 'coche',
                        'label' => 'Immunisé',
                    ],
                    'lvlRuine'   => [
                        'type'    => 'select',
                        'label'   => 'Niveau Ruine :',
                        'choices' => range(0, 4),
                    ],
                    'shoes'      => [
                        'type'  => 'coche',
                        'label' => 'Chaussures équipées',
                    ],
                ],
                'pouv'   => [
                    'sauvetage'        => [
                        'type'  => 'coche',
                        'label' => 'Sauvetage',
                        'class' => 'doubleChoixCit',
                    ],
                    'rdh'              => [
                        'type'  => 'coche',
                        'label' => 'RDH',
                        'class' => 'doubleChoixCit',
                    ],
                    'uppercut'         => [
                        'type'  => 'coche',
                        'label' => 'Uppercut',
                        'class' => 'doubleChoixCit',
                    ],
                    'donJh'            => [
                        'type'  => 'coche',
                        'label' => 'Camaraderie',
                        'class' => 'doubleChoixCit',
                    ],
                    'corpsSain'        => [
                        'type'  => 'coche',
                        'label' => 'Corps sain',
                        'class' => 'simpleChoixCit',
                    ],
                    'secondSouffle'    => [
                        'type'  => 'coche',
                        'label' => 'Second Souffle',
                        'class' => 'simpleChoixCit',
                    ],
                    'trouvaille'       => [
                        'type'  => 'coche',
                        'label' => 'Trouvaille',
                        'class' => 'doubleChoixCit',
                    ],
                    'trouvaille_a'     => [
                        'type'  => 'coche',
                        'label' => 'Trouvaille Améliorée',
                        'class' => 'simpleChoixCit',
                    ],
                    'vlm'              => [
                        'type'  => 'coche',
                        'label' => 'VLM',
                        'class' => 'doubleChoixCit',
                    ],
                    'pef'              => [
                        'type'  => 'coche',
                        'label' => 'PEF',
                        'class' => 'doubleChoixCit',
                    ],
                    'camaraderie_recu' => [
                        'type'  => 'coche',
                        'label' => 'Camaraderie reçue',
                        'class' => 'simpleChoixCit',
                    ],
                ],
                'hab'    => [
                    'lvlMaison'     => 'Habitation actuelle :',
                    'lvlCoinSieste' => [
                        'type'    => 'select',
                        'label'   => 'Coin sieste :',
                        'choices' => range(0, $maxlvlCS),
                        'class'   => 'doubleChoixCit',
                    ],
                    'lvlCuisine'    => [
                        'type'    => 'select',
                        'label'   => 'Cuisine :',
                        'choices' => range(0, $maxlvlCuisine),
                        'class'   => 'doubleChoixCit',
                    ],
                    'lvlLabo'       => [
                        'type'    => 'select',
                        'label'   => 'Laboratoire :',
                        'choices' => range(0, $maxlvlLabo),
                        'class'   => 'doubleChoixCit',
                    ],
                    'lvlRangement'  => [
                        'type'    => 'select',
                        'label'   => 'Rangement :',
                        'choices' => range(0, $maxlvlRangement),
                        'class'   => 'doubleChoixCit',
                    ],
                    'lvlRenfort'    => [
                        'type'    => 'select',
                        'label'   => 'Renfort :',
                        'choices' => range(0, $maxlvlRenfort),
                        'class'   => 'doubleChoixCit',
                    ],
                    'cloture'       => [
                        'type'  => 'coche',
                        'label' => 'Clôture :',
                        'class' => 'doubleChoixCit',
                    ],
                    'nbBarricade'   => [
                        'type'  => 'input',
                        'label' => 'Barricade :',
                        'class' => 'doubleChoixCit',
                    ],
                    'nbCarton'      => [
                        'type'  => 'input',
                        'label' => 'Carton :',
                        'class' => 'doubleChoixCit',
                    ],
                ],
            ];
            
        }
        
        
        $citoyensVieJson      = $this->serializerService->serializeArray($citoyensVie, 'json', ['citoyens']);
        $citoyensMortJson     = $this->serializerService->serializeArray($citoyensMort, 'json', ['citoyens']);
        $citoyensVille        = $this->serializerService->serializeArray($this->ville, 'json', ['citoyens']);
        $listPouvoirHerosJson = $this->serializerService->serializeArray($listPouvoirHeros, 'json', ['citoyens']);
        $listHabitationJson   = $this->serializerService->serializeArray($listHabitation, 'json', ['citoyens']);
        $listJobJson          = $this->serializerService->serializeArray($listJob, 'json', ['citoyens']);
        $listUpHomeJson       = $this->serializerService->serializeArray($listUpHome, 'json', ['citoyens']);
        
        
        /**
         *                 'citoyensVie'  => $citoyensVie,
         * 'citoyensMort' => $citoyensMort,
         * 'form'         => $form,
         * 'compteur'     => $compteur,
         * 'affFiltre'    => false,
         * 'filtre'       => $filtre,
         * 'formFiltre'   => $formFiltre,
         * 'isLead'       => $isLead,
         * 'isOrga'       => $isOrga,
         * 'coffreItems'  => $coffreItems,
         */
        
        $popUpMaj = $this->itemsHandler->recuperationItemsArray([9, 10]);
        
        /** @var CategoryObjet[] $listCategorie */
        $listCategorie = $popUpMaj['listCategorie'];
        
        /** @var ItemPrototype[] $listItems */
        $listItems = $popUpMaj['listItems'];
        
        
        $popUpMaj['listCategorie'] = $this->serializerService->serializeArray($listCategorie, 'json', ['general']);
        
        $popUpMaj['listItems'] = $this->serializerService->serializeArray($listItems, 'json', ['general']);
        
        $listSkillLevelBase = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => LevelSkill::DEBUTANT]);
        $herosSkillType     = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
        $herosSkillLevel    = $this->entityManager->getRepository(HerosSkillLevel::class)->getHerosSkillLevel($herosSkillType);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'citoyens' => [
                'citoyensVie'        => $citoyensVieJson,
                'citoyensMort'       => $citoyensMortJson,
                'compteurs'          => $compteur,
                'formCitoyen'        => $formCitoyen,
                'isLead'             => $isLead,
                'isOrga'             => $isOrga,
                'listAmelio'         => $listUpHomeJson,
                'listHabitation'     => $listHabitationJson,
                'listIdPouvoir'      => [
                    'camaraderie'    => HerosPrototype::ID_DON_JH,
                    'corps_sain'     => HerosPrototype::ID_CORPS_SAIN,
                    'poss_apag'      => HerosPrototype::ID_APAG,
                    'second_souffle' => HerosPrototype::ID_SS,
                    'vlm'            => HerosPrototype::ID_VLM,
                    'trouvaille_ame' => HerosPrototype::ID_TROUVAILLE_AME,
                ],
                'listJob'            => $listJobJson,
                'listPouvoir'        => $listPouvoirHerosJson,
                'listSkillLevelBase' => $this->serializerService->serializeArray($listSkillLevelBase, 'json', ['option']),
                'listSkill'          => $this->serializerService->serializeArray($herosSkillType, 'json', ['citoyens_skills']),
                'listSkillLevel'     => $this->serializerService->serializeArray($herosSkillLevel, 'json', ['option']),
                'moyenContact'       => $moyenContact,
                'myVille'            => $this->userHandler->myVille($this->ville),
                'user'               => $this->serializerService->serializeArray($this->user, 'json', ['general', 'citoyens']),
                'ville'              => $citoyensVille,
            
            ],
            'popUpMaj' => $popUpMaj,
            'general'  => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
        
    }
    
    #[Route('/majCitoyens', name: 'majCitoyens', methods: ['POST'])]
    public function maj_citoyens(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        $data = new CitoyensMajRest();
        
        $this->serializerService->deserialize($request->getContent(), CitoyensMajRest::class, 'json', $data, ['citoyens']);
        
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La data fournis est incomplète - maj citoyens - {$this->user->getId()}.");
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}");
            
            return new JsonResponse($retour);
        }
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : " . $data->getMapId());
            
            return new JsonResponse($retour);
        }
        
        // Verification des valeurs de pouvoirs/amélioration
        
        if ($data->getCitoyens() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le citoyen n'a pas été fournis : " . $data->getMapId());
            
            return new JsonResponse($retour);
        }
        
        // Récupération du Citoyen
        $citoyen = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                    'citoyen' => $data->getCitoyens()
                                                                                                      ->getCitoyen()]);
        
        
        if ($citoyen === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le citoyen n'existe pas dans la ville fournis : " . $data->getMapId());
            
            return new JsonResponse($retour);
        }
        
        $userMaj = $citoyen->getCitoyen();
        
        $listObjet =
            $this->entityManager->getRepository(ItemPrototype::class)->createQueryBuilder('i', 'i.id')->getQuery()
                                ->getResult();
        
        if ($data->getNameForm() !== 'coffres') {
            $maxlvlCS        =
                ($this->entityManager->getRepository(UpHomePrototype::class)->findOneBy(['nom' => 'cs']))->getLevels()
                                                                                                         ->count();
            $maxlvlCuisine   = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'cuisine']))->getLevels()->count();
            $maxlvlLabo      =
                ($this->entityManager->getRepository(UpHomePrototype::class)->findOneBy(['nom' => 'labo']))->getLevels()
                                                                                                           ->count();
            $maxlvlRenfort   = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'renfort']))->getLevels()->count();
            $maxlvlRangement = ($this->entityManager->getRepository(UpHomePrototype::class)
                                                    ->findOneBy(['nom' => 'rangement']))->getLevels()->count();
            switch ($data->getNameForm()) {
                case 'charge_apag':
                    if ($data->getCitoyens()->getChargeApag() < 0 || $data->getCitoyens()->getChargeApag() > 4) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur apag est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setChargeApag($data->getCitoyens()->getChargeApag());
                    break;
                case 'nb_camping':
                    if ($data->getCitoyens()->getNbCamping() < 0 || $data->getCitoyens()->getNbCamping() > 10) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur nb camping est incorrecte : " .
                                             $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setNbCamping($data->getCitoyens()->getNbCamping());
                    break;
                case 'immuniser':
                    if (!is_bool($data->getCitoyens()->getImmuniser())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur immunité est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setImmuniser($data->getCitoyens()->getImmuniser());
                    break;
                case 'shoes':
                    if (!is_bool($data->getCitoyens()->isShoes())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur shoes est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setShoes($data->getCitoyens()->isShoes());
                    break;
                case 'lvl_ruine':
                    if ($data->getCitoyens()->getLvlRuine() < 0 || $data->getCitoyens()->getLvlRuine() > 4) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur lvl Ruine est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlRuine($data->getCitoyens()->getLvlRuine());
                    break;
                case 'sauvetage':
                    if (!is_bool($data->getCitoyens()->getSauvetage())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur sauvetage est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setSauvetage($data->getCitoyens()->getSauvetage());
                    break;
                case 'rdh':
                    if (!is_bool($data->getCitoyens()->getRdh())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur rdh est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setRdh($data->getCitoyens()->getRdh());
                    break;
                case 'uppercut':
                    if (!is_bool($data->getCitoyens()->getUppercut())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur uppercut est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setUppercut($data->getCitoyens()->getUppercut());
                    break;
                case 'don_jh':
                    if (!is_bool($data->getCitoyens()->getDonJh())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur don jh est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    if (!$data->getCitoyens()->getDonJh() && $citoyen->getCitoyen()->getNbChargeCamaraderie() == 0) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Vous n'avez plus de charge disponible, il n'est pas possible de mettre à jour Camaraderie, verifiez votre nombre de charge dans votre espace personnel",
                            [],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - Le pouvoir camaraderie n'est pas possible d'être mis à jour, le nombre de charge disponible est de 0 : " .
                                             $data->getUserId());
                        
                        return new JsonResponse($retour);
                    }
                    
                    if (!$data->getCitoyens()->getDonJh() && $citoyen->getCitoyen()->getNbChargeCamaraderie() > 0) {
                        $userMaj->setNbChargeCamaraderie($userMaj->getNbChargeCamaraderie() - 1);
                    }
                    
                    $citoyen->setDonJh($data->getCitoyens()->getDonJh());
                    break;
                case 'corps_sain':
                    if (!is_bool($data->getCitoyens()->getCorpsSain())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur corps sain est incorrecte : " .
                                             $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setCorpsSain($data->getCitoyens()->getCorpsSain());
                    break;
                case 'second_souffle':
                    if (!is_bool($data->getCitoyens()->getSecondSouffle())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur second souffle est incorrecte : " .
                                             $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setSecondSouffle($data->getCitoyens()->getSecondSouffle());
                    break;
                case 'vlm':
                    if (!is_bool($data->getCitoyens()->getVlm())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur vlm est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setVlm($data->getCitoyens()->getVlm());
                    break;
                case 'trouvaille':
                    if (!is_bool($data->getCitoyens()->getTrouvaille())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur trouvaille est incorrecte : " .
                                             $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setTrouvaille($data->getCitoyens()->getTrouvaille());
                    break;
                case 'pef':
                    if (!is_bool($data->getCitoyens()->getPef())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur pef est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setPef($data->getCitoyens()->getPef());
                    break;
                case 'lvl_coin_sieste':
                    if ($data->getCitoyens()->getLvlCoinSieste() < 0 ||
                        $data->getCitoyens()->getLvlCoinSieste() > $maxlvlCS) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur lvl CS est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlCoinSieste($data->getCitoyens()->getLvlCoinSieste());
                    break;
                case 'lvl_cuisine':
                    if ($data->getCitoyens()->getLvlCuisine() < 0 ||
                        $data->getCitoyens()->getLvlCuisine() > $maxlvlCuisine) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur cuisine est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlCuisine($data->getCitoyens()->getLvlCuisine());
                    break;
                case 'lvl_labo':
                    if ($data->getCitoyens()->getLvlLabo() < 0 || $data->getCitoyens()->getLvlLabo() > $maxlvlLabo) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur labo est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlLabo($data->getCitoyens()->getLvlLabo());
                    break;
                case 'lvl_rangement':
                    if ($data->getCitoyens()->getLvlRangement() < 0 ||
                        $data->getCitoyens()->getLvlRangement() > $maxlvlRangement) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur rangement est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlRangement($data->getCitoyens()->getLvlRangement());
                    break;
                case 'lvl_renfort':
                    if ($data->getCitoyens()->getLvlRenfort() < 0 ||
                        $data->getCitoyens()->getLvlRenfort() > $maxlvlRenfort) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur renfort est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setLvlRenfort($data->getCitoyens()->getLvlRenfort());
                    break;
                case 'nb_barricade':
                    if ($data->getCitoyens()->getNbBarricade() < 0 || $data->getCitoyens()->getNbBarricade() > 99) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur barricade est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setNbBarricade($data->getCitoyens()->getNbBarricade());
                    break;
                case 'nb_carton':
                    if ($data->getCitoyens()->getNbCarton() < 0 || $data->getCitoyens()->getNbCarton() > 99) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur carton est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setNbCarton($data->getCitoyens()->getNbCarton());
                    break;
                case 'cloture':
                    if (!is_bool($data->getCitoyens()->getCloture())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur cloture est incorrecte : " . $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setCloture($data->getCitoyens()->getCloture());
                    break;
                case 'charge_camaraderie':
                    if (!is_bool($data->getCitoyens()->isChargeCamaraderie())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $this->logger->error("$codeErreur - La valeur camaraderie reçu est incorrecte : " .
                                             $data->getMapId());
                        
                        return new JsonResponse($retour);
                    }
                    $citoyen->setChargeCamaraderie($data->getCitoyens()->isChargeCamaraderie());
                    break;
                default:
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'outils',
                    );
                    $this->logger->error("$codeErreur - Valeur inexistante : " . $data->getMapId());
                    
                    return new JsonResponse($retour);
            }
        } else {
            
            
            // création d'un tableau d'objet à traiter.
            $idTab = array_combine(                             array_map(fn(CoffreCitoyen $i) => ($i->getItem()
                                                                                                     ?->getId() ??
                                                                                                   0 * 10) +
                                                                                                  ($i->getBroken() ? 1 :
                                                                                                      0),
                $data->getCitoyens()->getCoffres()->toArray()), $data->getCitoyens()->getCoffres()->toArray());
            
            foreach ($citoyen->getCoffres()->toArray() as $itemCoffre) {
                $item_id = $itemCoffre->getItem()->getId() * 10 + ($itemCoffre->getBroken() ? 1 : 0);
                
                if (isset($idTab[$item_id])) {
                    $itemCoffre->setNombre($idTab[$item_id]->getNombre());
                    unset($idTab[$item_id]);
                } else {
                    $citoyen->removeCoffre($itemCoffre);
                }
                
            }
            
            foreach ($idTab as $itemMaj) {
                
                $itemCoffre =
                    new CoffreCitoyen($listObjet[$itemMaj->getItem()?->getId() ?? 1], $itemMaj->getBroken());
                $itemCoffre->setNombre($itemMaj->getNombre());
                
                $citoyen->addCoffre($itemCoffre);
            }
        }
        
        
        try {
            
            $citoyen->setDateMaj(new DateTime('now'))
                    ->setUpdateBy($this->user);
            
            $this->entityManager->persist($citoyen);
            $this->entityManager->persist($userMaj);
            $this->entityManager->flush();
            
            if ($data->getNameForm() === 'coffres') {
                foreach ($listObjet as $item) {
                    $item->setNom($this->translator->trans($item->getNom(), [], 'items'));
                }
            }
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'zoneRetour' => [
                                            'citoyen' => $this->serializerService->serializeArray($citoyen, 'json', ['citoyens']),
                                        ]]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Probleme mise à jour Plans de chantier {$data->getMapId()} : " .
                                 $exception->getMessage());
            
            return new JsonResponse($retour);
        }
    }
    
    #[Route('/saveCompetence', name: 'majCompetence', methods: ['POST'])]
    public function maj_competence(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException("Ville non trouvée | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // controle si l'utilisateur a le droit de voir la ville car il ne peut modifier que sa propre ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville, 'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $data = new CompetenceCitoyensMajRest();
            
            $this->serializerService->deserialize($request->getContent(), CompetenceCitoyensMajRest::class, 'json', $data, ['citoyens']);
            
            // On vérifie que la personne qui modifie est bien la personne connectée
            if ($this->user->getId() !== $data->getUserId()) {
                throw new GestHordesException("Utilisateur non cohérant | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // Récupération du Citoyen
            $citoyen = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['ville' => $ville, 'citoyen' => $data->getUserId()]);
            
            // On vérifie que le citoyen est bien le même que celui connecté
            if ($citoyen === null) {
                throw new GestHordesException("Citoyen non trouvé | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            if ($citoyen->getId() !== $data->getCitoyenId()) {
                throw new GestHordesException("Citoyen non cohérant | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On récupère la compétence type qu'on souhaite modifier (permet de vérifier si la compétence existe)
            $competenceType = $this->entityManager->getRepository(HerosSkillType::class)->findOneBy(['id' => $data->getSkillId()]);
            
            if ($competenceType === null) {
                throw new GestHordesException("Compétence non trouvée | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On récupère le level de la compétence (permet de vérifier si le level existe)
            $competenceLevel = $this->entityManager->getRepository(HerosSkillLevel::class)->findOneBy(['herosSkillType' => $competenceType, 'lvl' => $data->getLevel()]);
            if ($competenceLevel === null) {
                throw new GestHordesException("Level de compétence non trouvé | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On balaye les compétences du citoyen pour retrouver la competence qui faut retirer (mise à jour du niveau) afin d'ajouter le nouveau à la place
            $competenceCitoyen = null;
            foreach ($citoyen->getSkill() as $competence) {
                if ($competence->getHerosSkillType()->getId() === $data->getSkillId()) {
                    $competenceCitoyen = $competence;
                    break;
                }
            }
            
            // Si la competence existe, on la retire
            if ($competenceCitoyen !== null) {
                $citoyen->removeSkill($competenceCitoyen);
            }
            
            // On ajoute la nouvelle compétence qu'on a récupéré plus haut
            $citoyen->addSkill($competenceLevel);
            
            // On met à jour le citoyen
            $this->entityManager->persist($citoyen);
            $this->entityManager->flush();
            $this->entityManager->refresh($citoyen);
            
            
            return new JsonResponse([
                                        'citoyen' => $this->serializerService->serializeArray($citoyen, 'json', ['citoyens']),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CitoyensRestController', 'majCompetence');
        }
        
    }
    
}