<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ChantierPrototype;
use App\Entity\EstimationTdg;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Hotel\TourDeGuet\TourDeGuetRest;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/tdg', name: 'rest_tdg_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class TourDeGuetRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected LocalFormatterUtils    $lfu,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/{mapId}', name: 'hotel_tour', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $this->ville  = $this->userHandler->getTownBySession($mapIdSession);
        
        if ($this->ville === null || $this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        $jour = $this->ville->getJour();
        
        $estimations =
            $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                               'day'       => $jour,
                                                                               'typeEstim' => EstimationTdg::TYPE_ESTIM_TDG]);
        
        $estimationsJson =
            json_decode($this->gh->getSerializer()->serialize($estimations, 'json', ['groups' => ['tdg']]), null, 512,
                        JSON_THROW_ON_ERROR);
        
        $planif_construit =
            $this->gh->chantierConstruitJour($this->ville, ChantierPrototype::ID_CHANTIER_PLANIF, $jour);
        
        if ($planif_construit) {
            $planif     = $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                                             'day'       => $jour,
                                                                                             'typeEstim' => EstimationTdg::TYPE_ESTIM_PLANIF]);
            $planifJson =
                json_decode($this->gh->getSerializer()->serialize($planif, 'json', ['groups' => ['tdg']]), null, 512,
                            JSON_THROW_ON_ERROR);
            
        } else {
            $planifJson = [];
        }
        
        $villeJson = $this->serializerService->serializeArray($this->ville, 'json', ['general']);
        
        $userJson = $this->serializerService->serializeArray($this->user, 'json', ['genereal_res']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'tdg'     => [
                'jour'        => $jour,
                'tdg'         => $estimationsJson,
                'planif_cons' => $planif_construit,
                'planif'      => $planifJson,
                'user'        => $userJson,
                'ville'       => $villeJson,
            ],
            'general' => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/recupEstims', name: 'recupEstims', methods: ['POST'])]
    public function recupEstims(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new TourDeGuetRest();
        
        
        $this->gh->getSerializer()->deserialize($request->getContent(), TourDeGuetRest::class, 'json',
                                                ['groups' => ['tdg'], 'object_to_populate' => $data]);
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - La data fournis est incomplète - recup estim TDG - {$this->user->getId()}.";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Le numéro de la ville est incorrecte : " . $data->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Verification user dans les citoyens de la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Le user fournis n'est pas dans la ville : " . $data->getMapId() . ' - user fournis : ' .
                $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Récupération des estimations
        $jour = $data->getJour() ?? 1;
        
        $estimations =
            $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                               'day'       => $jour,
                                                                               'typeEstim' => EstimationTdg::TYPE_ESTIM_TDG]);
        
        $estimationsJson =
            json_decode($this->gh->getSerializer()->serialize($estimations, 'json', ['groups' => ['tdg']]), null, 512,
                        JSON_THROW_ON_ERROR);
        
        $planif_construit = $this->gh->chantierConstruitJour($ville, ChantierPrototype::ID_CHANTIER_PLANIF, $jour);
        
        if ($planif_construit) {
            $planif     = $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                                             'day'       => $jour,
                                                                                             'typeEstim' => EstimationTdg::TYPE_ESTIM_PLANIF]);
            $planifJson =
                json_decode($this->gh->getSerializer()->serialize($planif, 'json', ['groups' => ['tdg']]), null, 512,
                            JSON_THROW_ON_ERROR);
            
        } else {
            $planifJson = [];
        }
        
        try {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = '';
            $retour['zoneRetour'] = [
                'tdg'         => $estimationsJson,
                'planif_cons' => $planif_construit,
                'planif'      => $planifJson,
            ];
            
            return new JsonResponse($retour);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Probleme récupération des estimations {$data->getMapId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    #[Route('/saveEstims', name: 'saveEstims', methods: ['POST'])]
    public function saveEstims(Request $request): JsonResponse
    {
        
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new TourDeGuetRest();
        
        
        $this->gh->getSerializer()->deserialize($request->getContent(), TourDeGuetRest::class, 'json',
                                                ['groups' => ['tdg'], 'object_to_populate' => $data]);
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - La data fournis est incomplète - sauvegarde estim - {$this->user->getId()}.";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Le numéro de la ville est incorrecte : " . $data->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Verification user dans les citoyens de la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Le user fournis n'est pas dans la ville : " . $data->getMapId() . ' - user fournis : ' .
                $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $jour = $data->getJour() ?? 1;
        
        
        // Controle des estimations soumis
        $libErreur = '';
        if (!$this->controleEstimation($ville, $this->user, EstimationTdg::TYPE_ESTIM_TDG, $data->getTdg(),
                                       $libErreur)) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $libErreur;
            
            return new JsonResponse($retour);
        }
        
        $planif_construit = $this->gh->chantierConstruitJour($ville, ChantierPrototype::ID_CHANTIER_PLANIF, $jour);
        
        if ($planif_construit) {
            
            // Controle des estimations planif soumis (que si il est construit)
            $libErreur = '';
            if (!$this->controleEstimation($ville, $this->user, EstimationTdg::TYPE_ESTIM_PLANIF, $data->getPlanif(),
                                           $libErreur)) {
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $libErreur;
                
                
                return new JsonResponse($retour);
            }
        }
        
        
        try {
            
            // Récupération des estimations
            $estimations = $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                                              'day'       => $jour,
                                                                                              'typeEstim' => EstimationTdg::TYPE_ESTIM_TDG]);
            
            // mise en tableau en fonction des %
            /**
             * @var EstimationTdg[] $estim_tab
             */
            $estim_tab = [];
            foreach ($estimations as $estimation) {
                $estim_tab[$estimation->getValPourcentage()] = $estimation;
            }
            
            // Mise en forme en tableau des data reçus
            foreach ($data->getTdg() as $estimation) {
                if (!isset($estim_tab[$estimation->getValPourcentage()])) {
                    $estimation->setVille($ville)
                               ->setDay($jour);
                    $this->entityManager->persist($estimation);
                } else {
                    $mod_estim = $estim_tab[$estimation->getValPourcentage()];
                    
                    $mod_estim->setMinEstim($estimation->getMinEstim())
                              ->setMaxEstim($estimation->getMaxEstim())
                              ->setDay($jour);
                    
                    $this->entityManager->persist($estimation);
                    unset($estim_tab[$estimation->getValPourcentage()]);
                }
            }
            
            // On supprime les enregistrements qui ont été effacé
            foreach ($estim_tab as $estimation) {
                $this->entityManager->remove($estimation);
            }
            
            
            if ($planif_construit) {
                $planifs = $this->entityManager->getRepository(EstimationTdg::class)->findBy(['ville'     => $this->ville,
                                                                                              'day'       => $jour,
                                                                                              'typeEstim' => EstimationTdg::TYPE_ESTIM_PLANIF]);
                
                // mise en tableau en fonction des %
                /**
                 * @var EstimationTdg[] $planif_tab
                 */
                $planif_tab = [];
                foreach ($planifs as $planif) {
                    $planif_tab[$planif->getValPourcentage()] = $planif;
                }
                
                // Mise en forme en tableau des data reçus
                foreach ($data->getPlanif() as $planif) {
                    if (!isset($planif_tab[$planif->getValPourcentage()])) {
                        $planif->setVille($ville)
                               ->setDay($jour);
                        $this->entityManager->persist($planif);
                    } else {
                        $mod_planif = $planif_tab[$planif->getValPourcentage()];
                        
                        $mod_planif->setMinEstim($planif->getMinEstim())
                                   ->setMaxEstim($planif->getMaxEstim())
                                   ->setDay($jour);
                        
                        $this->entityManager->persist($planif);
                        unset($planif_tab[$planif->getValPourcentage()]);
                    }
                }
                
                // On supprime les enregistrements qui ont été effacé
                foreach ($planif_tab as $planif) {
                    $this->entityManager->remove($planif);
                }
                
            }
            
            $this->entityManager->flush();
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'zoneRetour' => [],
                                    ]);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Probleme récupération des estimations {$data->getMapId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    /**
     * @param EstimationTdg[] $estimations
     * @return bool
     */
    private function controleEstimation(Ville  $ville, User $user, int $type, array $estimations,
                                        string &$libErreur): bool
    {
        $longueur_estim = count($estimations);
        
        if ($longueur_estim === 0 && $type === EstimationTdg::TYPE_ESTIM_TDG) {
            $codeErreur = $this->randomString();
            $libErreur  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $mesageLog  = "$codeErreur - Estimations fournis vide - {$ville->getMapId()} - {$user->getId()}";
            $this->logger->error($mesageLog);
            $this->gh->generateMessageDiscord($mesageLog);
            return false;
        }
        
        if (count($estimations) > 1) {
            for ($i = 0; $i < $longueur_estim - 1; $i++) {
                $current_estim = $estimations[$i];
                $next_estim    = $estimations[$i + 1];
                
                if ($current_estim->getMinEstim() > $next_estim->getMinEstim()) {
                    if ($type == 1) {
                        $libErreur =
                            $this->translator->trans('Il y a une erreur avec vos estimations mini, elles ne sont pas correctement croissantes ou égales.',
                                                     [], 'hotel');
                    } else {
                        $libErreur =
                            $this->translator->trans('Il y a une erreur avec vos estimations du planificateur mini, elles ne sont pas correctement croissantes ou égales.',
                                                     [], 'hotel');
                    }
                    
                    return false;
                }
                if ($current_estim->getMaxEstim() < $next_estim->getMaxEstim()) {
                    if ($type == 1) {
                        $libErreur =
                            $this->translator->trans('Il y a une erreur avec vos estimations maxi, elles ne sont pas correctement décroissantes ou égales.',
                                                     [], 'hotel');
                    } else {
                        $libErreur =
                            $this->translator->trans('Il y a une erreur avec vos estimations du planificateur maxi, elles ne sont pas correctement décroissantes ou égales.',
                                                     [], 'hotel');
                    }
                    return false;
                }
                
            }
        }
        
        for ($i = 0; $i < $longueur_estim; $i++) {
            $current_estim = $estimations[$i];
            if ($current_estim->getMinEstim() > $current_estim->getMaxEstim()) {
                if ($type == 1) {
                    $libErreur =
                        $this->translator->trans("L'estimation mini ne peut pas être supérieur au maximum.", [],
                                                 'hotel');
                } else {
                    $libErreur =
                        $this->translator->trans("L'estimation du planificateur mini ne peut pas être supérieur au maximum.",
                                                 [], 'hotel');
                }
                
                return false;
            }
            if ($current_estim->getTypeEstim() !== $type) {
                $codeErreur = $this->randomString();
                $libErreur  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $mesageLog  =
                    "$codeErreur - Le type d'estimation n'est pas raccord avec les estimations fournis - {$ville->getMapId()} - {$user->getId()}";
                $this->logger->error($mesageLog);
                $this->gh->generateMessageDiscord($mesageLog);
                return false;
            }
            
        }
        
        
        return true;
        
    }
    
}