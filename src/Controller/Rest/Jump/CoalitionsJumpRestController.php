<?php

namespace App\Controller\Rest\Jump;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\InscriptionJump;
use App\Entity\Jump;
use App\Entity\StatutInscription;
use App\Exception\AuthenticationException;
use App\Exception\GestHordesException;
use App\Security\Voter\InJumpVoter;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Jump\CoalitionHandler;
use App\Service\Jump\InscriptionHandler;
use App\Service\Jump\JumpHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Jump\Coalition\CoalitionCreateurDto;
use App\Structures\Dto\GH\Jump\Coalition\CoalitionDispoDto;
use App\Structures\Dto\GH\Jump\Coalition\CoalitionInscriptionDto;
use App\Structures\Dto\GH\Jump\Coalition\CoalitionStatutDto;
use App\Structures\Dto\GH\Jump\Coalition\CompetenceCoalitionMajRest;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/coalition', name: 'rest_coalition_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class CoalitionsJumpRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected JumpHandler            $jumpHandler,
        protected CoalitionHandler       $coalitionHandler,
        protected InscriptionHandler     $inscriptionHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     *
     * @return Response
     */
    #[Route(path: '/{idJump}', name: 'jump_coa_gestion', requirements: ['idJump' => '[a-zA-Z0-9]{24}'], methods: ['GET']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function gestionCoalition(string $idJump, Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                throw new AuthenticationException("Utilisateur pas en ville | fichier " . __FILE__ . " | ligne " .
                                                  __LINE__);
            }
            
            $jump = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $idJump]);
            
            if ($jump === null) {
                throw new GestHordesException($this->translator->trans("Jump inexistant !", [], 'jumpEvent'));
            }
            
            // Contrôle d'accés à la page
            $inscriptionJoueur = $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(
                ['Jump' => $jump, 'user' => $this->user, 'statut' => StatutInscription::ST_ACC],
            );
            if ($inscriptionJoueur === null) {
                throw new GestHordesException($this->translator->trans("Vous n'êtes pas inscrit au jump ou encore accepté.",
                                                                       [], 'jumpEvent'),
                    code:                     GestHordesException::SHOW_MESSAGE);
            }
            
            // Contrôle de l'accessibilité à la page coa
            $dateJourM2M = new DateTime('now');
            $dateJourM2M->sub(new DateInterval('P2M'));
            
            $dateJourM4J = new DateTime('now');
            $dateJourM4J->sub(new DateInterval('P4D'));
            
            if (($jump->getDateApproxJump() < $dateJourM4J && $jump->getDateJumpEffectif() == null) ||
                ($jump->getDateJumpEffectif() !== null && $jump->getDateJumpEffectif() < $dateJourM2M)) {
                throw new GestHordesException($this->translator->trans("Vous n'êtes plus autorisé à accéder à la page coalition de ce jump.",
                                                                       [], 'jumpEvent'), GestHordesException::SHOW_MESSAGE);
            }
            
            $listMoyenContact = $this->jumpHandler->recuperationMoyenContact($jump);
            
            $listJob = $this->jumpHandler->recuperationJobVille($jump->getVillePrive() ?? false);
            
            $listStatut = $this->jumpHandler->recuperationListStatut();
            
            $listTypeDispo = $this->jumpHandler->getListTypeDispo();
            
            $this->jumpHandler->getListPouvoir();
            
            $this->jumpHandler->getListCreneau();
            
            $listSkill       = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
            $listSkillBase   = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
            $herosSkillLevel = $this->entityManager->getRepository(HerosSkillLevel::class)->getHerosSkillLevel($listSkill);
            
            
            $jumpJson               = $this->serializerService->serializeArray($jump, 'json', ['coalition', 'general_res']);
            $listJobJson            = $this->serializerService->serializeArray($listJob, 'json', ['coalition']);
            $listStatutJson         = $this->serializerService->serializeArray($listStatut, 'json', ['coalition_gen']);
            $listTypeDispoJson      = $this->serializerService->serializeArray($listTypeDispo, 'json', ['coalition_gen']);
            $userJson               = $this->serializerService->serializeArray($this->user, 'json', ['general_res']);
            $listSkillJson          = $this->serializerService->serializeArray($listSkill, 'json', ['ency']);
            $listSkillBaseJson      = $this->serializerService->serializeArray($listSkillBase, 'json', ['option']);
            $listSkillLevelBaseJson = $this->serializerService->serializeArray($herosSkillLevel, 'json', ['option']);
            
            
            $retour = [
                'coalition' => [
                    'jump'             => $jumpJson,
                    'listMoyenContact' => $listMoyenContact,
                    'options'          => [
                        'listJob'       => $listJobJson,
                        'listStatut'    => $listStatutJson,
                        'listTypeDispo' => $listTypeDispoJson,
                        'listSkill'     => $listSkillJson,
                        'listSkillBase' => $listSkillBaseJson,
                        'listSkillLvl'  => $listSkillLevelBaseJson,
                        'statut_acc'    => StatutInscription::ST_ACC,
                        'id_pouv_archi' => HerosPrototype::ID_ARCHI,
                        'id_pouv_vp'    => HerosPrototype::ID_VP,
                        'isOrga'        => $this->userHandler->isOrga($jump),
                        'isLeadOrOrga'  => $this->userHandler->isLeadOrOrga($jump),
                    ],
                    'user'             => $userJson,
                ],
                'general'   => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CoalitionsJumpRestController', 'gestionCoalition');
        }
        
    }
    
    
    /**
     * @return JsonResponse
     */
    #[Route(path: '/inscription', name: 'inscript', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function inscriptionCoalition(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new CoalitionInscriptionDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionInscriptionDto::class, 'json', $data, ['coalition']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getNumCoa() === null || $data->getNumCoa() < 1 || $data->getNumCoa() > 8) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le numéro de la coalition fournis est incorrect.- {$this->user->getId()} - {$data->getIdJump()}- {$data->getNumCoa()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getPosCoa() === null || $data->getPosCoa() < 1 || $data->getPosCoa() > 5) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - La position dans la coalition fournis est incorrecte.- {$this->user->getId()} - {$data->getIdJump()} - {$data->getPosCoa()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() != $this->user->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur ne correspondent pas.- {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Jump inconnu.- {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance de l'inscription au statut accepté pour l'utilisateur
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $this->user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdUserFournis() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur fournis est incorrect.- {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $userFournis = $this->userHandler->recuperationUser($data->getIdUserFournis());
        if ($userFournis === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - User fournis non existant.- {$this->user->getId()} - {$data->getUserId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        // Vérification de l'existance de l'inscription au statut accepté pour le user à placer dans la coa
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $userFournis)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- {$this->user->getId()} - {$data->getUserId()} - {$userFournis->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        $userEchangeFournis = null;
        // Cas où effectue un échange avec un autre joueur
        if ($data->getIdUserEchange() !== null) {
            $userEchangeFournis = $this->userHandler->recuperationUser($data->getIdUserEchange());
            if ($userEchangeFournis === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           =
                    "$codeErreur - User à échanger non existant.- {$this->user->getId()} - {$data->getUserId()} - {$data->getUserId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            } else {
                // Vérification de l'existance de l'inscription au statut accepté pour le user à échanger dans la coa
                if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $userEchangeFournis)) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    $messageLog           =
                        "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- {$this->user->getId()} - {$data->getUserId()} - {$userEchangeFournis->getId()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                }
            }
        } else {
            // Verification de la coa vide dans le cas où on s'inscrit dans une coa vide (pour éviter les doubles inscriptions en même temps)
            if (!$this->coalitionHandler->verificationPlaceCoaLibre($jump, $data->getNumCoa(), $data->getPosCoa())) {
                $retour['codeRetour'] = 1;
                $retour['libRetour']  =
                    $this->translator->trans("La place dans la coalition est déjà prise, merci d'actualiser ou choisir une autre place.",
                                             [], 'jumpEvent');
                return new JsonResponse($retour);
            }
        }
        
        
        if ($data->getIdUserEchange() !== null) {
            $result =
                $this->coalitionHandler->echangeDeCoalition($jump, $userFournis, $userEchangeFournis, $this->user);
        } else {
            $result = $this->coalitionHandler->enregistrementNouvelleCoalition($jump, $userFournis, $data->getNumCoa(),
                                                                               $data->getPosCoa(), $this->user);
        }
        
        if ($result) {
            
            $jumpNew = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $jump->getId()]);
            
            $jumpJson = $this->serializerService->serializeArray($jumpNew, 'json', ['coalition', 'general_res']);
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [
                'jump' => $jumpJson,
            ];
        } else {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Un problème est survenu, veuillez actualiser et recommencer. Si le problème est toujours persistant, contacter l'administrateur.",
                                         [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        }
        return new JsonResponse($retour);
        
    }
    
    #[Route(path: '/', name: 'jump_coa', methods: ['GET']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function main(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                throw new AuthenticationException("Utilisateur pas en ville | fichier " . __FILE__ . " | ligne " .
                                                  __LINE__);
            }
            
            // récup la liste des jumps éligibles et où le joueur est accepté
            $listJump = $this->entityManager->getRepository(Jump::class)->listJumpCoalition($this->user);
            
            $listJumpJson = $this->serializerService->serializeArray($listJump, 'json', ['list_coa', 'general_res']);
            
            
            $retour = [
                'list'    => [
                    'listJump' => $listJumpJson,
                ],
                'general' => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CoalitionsJumpRestController', 'main');
        }
        
    }
    
    #[Route(path: '/majCompetence', name: 'majCompetenceInscription', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majCompetenceInscription(Request $request): JsonResponse
    {
        try {
            // vérification si le joueur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            $data = new CompetenceCoalitionMajRest();
            
            $this->serializerService->deserialize($request->getContent(), CompetenceCoalitionMajRest::class, 'json', $data, ['coalition']);
            
            // On vérifie que l'id du joueur qui met à jour est bien celui de l'utilisateur connecté
            if ($data->getUserIdMaj() !== $this->user->getId()) {
                throw new GestHordesException("Utilisateur non autorisé | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On récupère le jump
            $jump = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $data->getIdJump()]);
            
            // On vérifie que le jump existe
            if ($jump === null) {
                throw new GestHordesException("Jump non trouvé | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // controle si l'utilisateur a le droit de voir la ville car il ne peut modifier que sa propre ville
            $this->denyAccessUnlessGranted(InJumpVoter::EDIT_SPE, $jump, 'Acces non autorisé, il ne s\'agit pas de votre jump');
            
            // On récupère l'inscription du citoyen ciblé
            $inscription = $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['Jump' => $jump, 'user' => $data->getUserId()]);
            
            // On récupère la compétence type qu'on souhaite modifier (permet de vérifier si la compétence existe)
            $competenceType = $this->entityManager->getRepository(HerosSkillType::class)->findOneBy(['id' => $data->getIdSkill()]);
            
            if ($competenceType === null) {
                throw new GestHordesException("Compétence non trouvée | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On récupère le level de la compétence (permet de vérifier si le level existe)
            $competenceLevel = $this->entityManager->getRepository(HerosSkillLevel::class)->findOneBy(['herosSkillType' => $competenceType, 'lvl' => $data->getLvlSkill()]);
            if ($competenceLevel === null) {
                throw new GestHordesException("Level de compétence non trouvé | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On balaye les compétences du citoyen pour retrouver la competence qui faut retirer (mise à jour du niveau) afin d'ajouter le nouveau à la place
            $competenceCitoyen = null;
            foreach ($inscription->getSkill() as $competence) {
                if ($competence->getHerosSkillType()->getId() === $data->getIdSkill()) {
                    $competenceCitoyen = $competence;
                    break;
                }
            }
            
            // Si la competence existe, on la retire
            if ($competenceCitoyen !== null) {
                $inscription->removeSkill($competenceCitoyen);
            }
            
            // On ajoute la nouvelle compétence qu'on a récupéré plus haut
            $inscription->addSkill($competenceLevel);
            
            // On met à jour le citoyen
            $this->entityManager->persist($inscription);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscription);
            
            
            return new JsonResponse([
                                        'skill' => $this->serializerService->serializeArray($inscription->getSkill(), 'json', ['coalition']),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'CoalitionsJumpRestController', 'majCompetenceInscription');
        }
    }
    
    /**
     * @return JsonResponse
     */
    #[Route(path: '/majCreateur', name: 'createur', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majCreateur(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new CoalitionCreateurDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionCreateurDto::class, 'json', $data, ['coalition']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - maj createur - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - maj createur - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getNumCoa() === null || $data->getNumCoa() < 1 || $data->getNumCoa() > 8) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le numéro de la coalition fournis est incorrect.- maj createur - {$this->user->getId()} - {$data->getIdJump()}- {$data->getNumCoa()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getPosCoa() === null || $data->getPosCoa() < 1 || $data->getPosCoa() > 5) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - La position dans la coalition fournis est incorrecte.- maj createur - {$this->user->getId()} - {$data->getIdJump()} - {$data->getPosCoa()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getCreateur() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - La valeur du createur est incorrecte.- maj createur - {$this->user->getId()} - {$data->getIdJump()} - {$data->getPosCoa()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- maj createur - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        // Vérification de l'existance du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu.- maj createur - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance de l'inscription au statut accepté pour l'utilisateur
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $this->user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- maj createur - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $user = $this->userHandler->recuperationUser($data->getUserId());
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - User fournis non existant.- maj createur - {$this->user->getId()} - {$data->getUserId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        // Vérification de l'existance de l'inscription au statut accepté pour le user à placer dans la coa
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- maj createur - {$this->user->getId()} - {$data->getUserId()} - {$user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        // Verification de la coa
        if (!$this->coalitionHandler->controleExistanceCoalition($jump, $user)) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("Utilisateur non inscrit en coalition.", [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        
        $result = $this->coalitionHandler->enregistrementCreateurCoalition($jump, $user, $data->getNumCoa(),
                                                                           $data->getPosCoa(), $data->getCreateur(),
                                                                           $this->user);
        
        if ($result) {
            
            $jumpNew = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $jump->getId()]);
            
            $jumpJson = $this->serializerService->serializeArray($jumpNew, 'json', ['coalition', 'general_res']);
            
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [
                'jump' => $jumpJson,
            ];
        } else {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Un problème est survenu, veuillez actualiser et recommencer. Si le problème est toujours persistant, contacter l'administrateur.",
                                         [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        }
        return new JsonResponse($retour);
        
    }
    
    /**
     * @return JsonResponse
     */
    #[Route(path: '/majDispo', name: 'dispo', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majDispoCoalition(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new CoalitionDispoDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionDispoDto::class, 'json', $data, ['coalition']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - Coalition - maj dispo - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur fournis est incorrect. - Coalition - maj dispo -{$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération de l'utilisateur
        $user = $this->userHandler->recuperationUser($data->getUserId());
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur inconnu. - Coalition - maj dispo - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance de l'inscription au statut accepté pour l'utilisateur
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $this->user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- Coalition - maj dispo- {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->coalitionHandler->controleExistanceCoalition($jump, $user)) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("Utilisateur non inscrit en coalition.", [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($data->getIdDispo() === null || !$this->coalitionHandler->controleExistanceDispoJump($data->getIdDispo())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le type de dispo fournis est incorrect.. - Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()} - {$data->getIdDispo()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdCreneau() === null ||
            !$this->coalitionHandler->controleExistanceCreneauJump($jump, $data->getIdCreneau())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Créneau fournis est incorrect. - Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()} - {$data->getIdCreneau()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($this->coalitionHandler->miseAJourDispoCoa($jump, $user, $data->getIdCreneau(), $data->getIdDispo())) {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème dans la sauvegarde des dispos. - Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour);
        
    }
    
    /**
     * @return JsonResponse
     */
    #[Route(path: '/majStatut', name: 'statut', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majStatutCoalition(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new CoalitionStatutDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionStatutDto::class, 'json', $data, ['coalition']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - maj statut- {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - maj statut- {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur fournis est incorrect. - Coalition - maj statut-{$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        if ($data->getIdStatut() === null || !$this->coalitionHandler->controleExistanceStatut($data->getIdStatut())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le statut fournis est incorrect. - Coalition - maj statut-{$this->user->getId()} - {$data->getIdStatut()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération de l'utilisateur
        $user = $this->userHandler->recuperationUser($data->getUserId());
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur inconnu. - Coalition - maj statut - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - maj statut- {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance de l'inscription au statut accepté pour l'utilisateur
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $this->user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'y a pas d'inscription valide pour ce joueur.- Coalition - maj statut- {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->coalitionHandler->controleExistanceCoalition($jump, $user)) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("Utilisateur non inscrit en coalition.", [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($this->coalitionHandler->miseAJourStatutCoa($jump, $user, $data->getIdStatut())) {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème dans la sauvegarde des dispos. - Coalition - maj dispo - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour);
        
    }
    
    
}