<?php

namespace App\Controller\Rest\Jump;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Coalition;
use App\Entity\CreneauHorraire;
use App\Entity\Event;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\InscriptionJump;
use App\Entity\JobPrototype;
use App\Entity\Jump;
use App\Entity\LeadJump;
use App\Entity\LevelRuinePrototype;
use App\Entity\LogEventEvent;
use App\Entity\LogEventInscription;
use App\Entity\LogEventJump;
use App\Entity\MasquageEvent;
use App\Entity\MasquageJump;
use App\Entity\ObjectifVillePrototype;
use App\Entity\RoleJump;
use App\Entity\StatutInscription;
use App\Entity\TypeDispo;
use App\Entity\TypeLead;
use App\Entity\TypeVille;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Jump\EventHandler;
use App\Service\Jump\GestionHandler;
use App\Service\Jump\JumpHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\JSONRequestParser;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Jump\Gestion\AssociateVilleDto;
use App\Structures\Dto\GH\Jump\Gestion\CoalitionGestionDto;
use App\Structures\Dto\GH\Jump\Gestion\EventDto;
use App\Structures\Dto\GH\Jump\Gestion\JumpDto;
use App\Structures\Dto\GH\Jump\Inscription\InscriptionJumpRestDto;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/gestion', name: 'rest_gestion_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class GestionJumpRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected LocalFormatterUtils    $lfu,
        protected JumpHandler            $jumpHandler,
        protected EventHandler           $eventHandler,
        protected GestionHandler         $gestionHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     *
     * @param string $idJump
     * @param int $userId
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route(path: '/jump/{idJump}/{userId}', name: 'jump_gestion_candidature', requirements: ['idJump' => '[a-zA-Z0-9]{24}',
                                                                                              'userId' => '\d+'], methods: ['GET'])]
    public function aff_gestionCandidature(string $idJump, int $userId, Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $jump = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $idJump]);
        
        if ($jump === null) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Le jump sur lequel vous essayez de gérer la candidature n'existe pas.", [],
                                         'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne faites pas partis des organisateurs du jump que vous essayez d'acceder !",
                                         [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        $inscription =
            $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['Jump' => $idJump,
                                                                                    'user' => $userId]);
        
        if ($inscription === null) {
            
            $retour['codeRetour'] = 3;
            $retour['libRetour']  =
                $this->translator->trans("La candidature que vous essayez de consulter n'existe pas, veuillez réessayer ou vérifier si la candidature existe bien.",
                                         [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion_jump', ['idJump' => $idJump]);
        }
        
        
        // conversion du moyen de contact pour le fournir au front correctement
        $inscription->setMoyenContact($inscription->dechiffreMoyenContact($this->gh));
        
        foreach ($inscription->getLogEventInscriptions() as $logEventInscription) {
            $logEventInscription->setLibelle($this->translator->trans($logEventInscription->getLibelle(), [],
                                                                      'jumpEvent'));
            if ($logEventInscription->getTypologie() === 4) {
                $logEventInscription->setValeurAvant($this->translator->trans($logEventInscription->getValeurAvant(),
                                                                              [], 'jump'))
                                    ->setValeurApres($this->translator->trans($logEventInscription->getValeurApres(),
                                                                              [], 'jump'));
            }
        }
        
        $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
        
        foreach ($listCreneau as $creneau) {
            $creneau->setLibelle($this->translator->trans($creneau->getLibelle(), [], 'jump'));
        }
        
        
        $listStatut    = $this->entityManager->getRepository(StatutInscription::class)->statutGestionCandidature();
        $listPouvoir   = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
        $listLvlRuine  = $this->entityManager->getRepository(LevelRuinePrototype::class)->findAll();
        $listSkillType = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
        $listSkillBase = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
        
        $helpRuine = "";
        
        foreach ($listLvlRuine as $lvlRuine) {
            $helpRuine .= "{$this->translator->trans($lvlRuine->getNom(),[],'jump')} : {$this->translator->trans($lvlRuine->getDescription(),[],'jump')} <br/>";
        }
        
        if ($jump->getVillePrive() ?? false) {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
        } else {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
        }
        
        $listDispo = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
        $listRole  = $this->entityManager->getRepository(RoleJump::class)->findAll();
        $listLead  = $this->entityManager->getRepository(TypeLead::class)->findAllOrdreAlpha();
        
        // Traduction des listes
        foreach ($listStatut as $statut) {
            $statut->setNom($this->translator->trans($statut->getNom(), [], 'jump'))
                   ->setNomGestion($this->translator->trans($statut->getNomGestion(), [], 'jump'))
                   ->setNomGestionCourt($this->translator->trans($statut->getNomGestionCourt(), [], 'jump'));
        }
        
        foreach ($listPouvoir as $pouvoir) {
            $pouvoir->setNom($this->translator->trans($pouvoir->getNom(), [], 'game'));
        }
        
        foreach ($listJob as $job) {
            $job->setAlternatif($this->translator->trans($job->getAlternatif(), [], 'game'));
            $job->setNom($this->translator->trans($job->getNom(), [], 'game'));
        }
        
        foreach ($listLvlRuine as $ruine) {
            $ruine->setNom($this->translator->trans($ruine->getNom(), [], 'jump'));
        }
        
        foreach ($listDispo as $dispo) {
            $dispo->setNom($this->translator->trans($dispo->getNom(), [], 'jump'));
        }
        
        foreach ($listRole as $role) {
            $role->setNom($this->translator->trans($role->getNom(), [], 'jump'));
        }
        
        foreach ($listLead as $lead) {
            $lead->setNom($this->translator->trans($lead->getNom(), [], 'jump'));
        }
        
        $jumpJson        = $this->serializerService->serializeArray($jump, 'json', ['gestion_jump', 'gestion_jump_spe', 'jump', 'general_res']);
        $inscriptionJson = $this->serializerService->serializeArray($inscription, 'json', ['candidature_jump', 'jump', 'general_res']);
        
        $listLeadJson      = $this->serializerService->serializeArray($listLead, 'json', ['jump']);
        $listStatutJson    = $this->serializerService->serializeArray($listStatut, 'json', ['jump']);
        $listPouvoirJson   = $this->serializerService->serializeArray($listPouvoir, 'json', ['gestion_jump']);
        $listLvlRuineJson  = $this->serializerService->serializeArray($listLvlRuine, 'json', ['jump']);
        $listJobJson       = $this->serializerService->serializeArray($listJob, 'json', ['gestion_jump']);
        $listRoleJson      = $this->serializerService->serializeArray($listRole, 'json', ['jump']);
        $listDispoJson     = $this->serializerService->serializeArray($listDispo, 'json', ['jump']);
        $listSkillJson     = $this->serializerService->serializeArray($listSkillType, 'json', ['option']);
        $listSkillBaseJson = $this->serializerService->serializeArray($listSkillBase, 'json', ['option']);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'inscription' => [
                'jump'        => $jumpJson,
                'inscription' => $inscriptionJson,
                'options'     => [
                    'helpRuine'     => $helpRuine,
                    'listDispo'     => $listDispoJson,
                    'listJob'       => $listJobJson,
                    'listLead'      => $listLeadJson,
                    'listLvlRuine'  => $listLvlRuineJson,
                    'listPouvoir'   => $listPouvoirJson,
                    'listRole'      => $listRoleJson,
                    'listStatut'    => $listStatutJson,
                    'listSkill'     => $listSkillJson,
                    'listSkillBase' => $listSkillBaseJson,
                    'role_lead'     => RoleJump::ROLE_LEAD,
                    'role_applead'  => RoleJump::ROLE_APLEAD,
                ],
                'user'        => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
            ],
            'general'     => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    /**
     * @param string $idEvent
     * @param Request $request
     * @param string $photoDir
     * @return Response
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route(path: '/event/{idEvent}', name: 'jump_gestion_event', requirements: ['idEvent' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function aff_gestionEvent(string $idEvent, Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $event = $this->entityManager->getRepository(Event::class)->findOneBy(['id' => $idEvent]);
        
        if ($event === null) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  = $this->translator->trans("L'event n'existe pas.", [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        if (!$this->entityManager->getRepository(Event::class)
                                 ->habiliterGestion($event->getId(), $this->user->getId())) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne faites pas partis des organisateurs de l'event que vous essayez d'acceder !",
                                         [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        foreach ($event->getLogEvent() as $log) {
            $log->setLibelle($this->translator->trans($log->getLibelle(), [], 'jumpEvent'));
        }
        
        $eventJson = $this->serializerService->serializeArray($event, 'json', ['gestion_event', 'event', 'jump', 'general_res']);
        
        $listTypeVille = $this->entityManager->getRepository(TypeVille::class)->findAll();
        $listObjectif  = $this->entityManager->getRepository(ObjectifVillePrototype::class)->findAll();
        
        foreach ($listObjectif as $objectif) {
            $objectif->setNom($this->translator->trans($objectif->getNom(), [], 'jump'));
        }
        
        foreach ($listTypeVille as $type) {
            $type->setNom($this->translator->trans($type->getNom(), [], 'game'));
            $type->setAbrev($this->translator->trans($type->getAbrev(), [], 'game'));
        }
        
        $listTypeVilleJson = $this->serializerService->serializeArray($listTypeVille, 'json', ['jump']);
        $listObjectifJson  = $this->serializerService->serializeArray($listObjectif, 'json', ['jump']);
        
        $listJob     = $this->entityManager->getRepository(JobPrototype::class)->findAll();
        $listJobJson = $this->serializerService->serializeArray($listJob, 'json', ['jump']);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'gestionEvent' => [
                'event'   => $eventJson,
                'options' => [
                    'listCommu'     => [
                        'de',
                        'es',
                        'en',
                        'fr',
                        'int',
                    ],
                    'listTypeVille' => $listTypeVilleJson,
                    'listObjectif'  => $listObjectifJson,
                    'statutAcc'     => StatutInscription::ST_ACC,
                    'listJob'       => $listJobJson,
                ],
                'user'    => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
            ],
            'general'      => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    /**
     * @param string $idJump
     * @param Request $request
     * @param string $photoDir
     * @return Response
     * @throws GuzzleException
     * @throws JsonException
     */
    #[Route(path: '/jump/{idJump}', name: 'jump_gestion_jump', requirements: ['idJump' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function aff_gestionJump(string $idJump, Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $jump = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $idJump]);
        
        if ($jump === null) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  = $this->translator->trans("Le jump n'existe pas.", [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne faites pas partis des organisateurs de l'event que vous essayez d'acceder !",
                                         [], 'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_gestion');
        }
        
        
        // dans le cas d'un jump, vérification de si l'orga a fait son inscription
        if ($jump->getEvent() === null &&
            $this->entityManager->getRepository(Jump::class)->isGestionnaireNonInscrit($jump, $this->user->getId())) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Vous devez vous inscrire avant de pouvoir accéder à la gestion du jump !", [],
                                         'jumpEvent');
            
            return new JsonResponse($retour);
            //return $this->redirectToRoute('jump_inscription_specifique', ['idJump' => $jump->getId()]);
        }
        
        
        $listTypeVille = $this->entityManager->getRepository(TypeVille::class)->findAll();
        $listObjectif  = $this->entityManager->getRepository(ObjectifVillePrototype::class)->findAll();
        $listLead      = $this->entityManager->getRepository(TypeLead::class)->findAll();
        $listStatut    = $this->entityManager->getRepository(StatutInscription::class)->statutGestionCandidature();
        $listSkill     = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
        $listSkillBase = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
        
        if ($jump->getVillePrive() ?? false) {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
        } else {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
        }
        
        $jumpJson = $this->serializerService->serializeArray($jump, 'json', ['gestion_jump', 'gestion_jump_spe', 'jump', 'general_res']);
        
        $listTypeVilleJson = $this->serializerService->serializeArray($listTypeVille, 'json', ['jump']);
        $listObjectifJson  = $this->serializerService->serializeArray($listObjectif, 'json', ['jump']);
        $listLeadJson      = $this->serializerService->serializeArray($listLead, 'json', ['jump']);
        $listStatutJson    = $this->serializerService->serializeArray($listStatut, 'json', ['jump']);
        $listSkillJson     = $this->serializerService->serializeArray($listSkill, 'json', ['option']);
        $listSkillBaseJson = $this->serializerService->serializeArray($listSkillBase, 'json', ['option']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'gestionJump' => [
                'jump'    => $jumpJson,
                'options' => [
                    'listCommu'     => [
                        'de',
                        'es',
                        'en',
                        'fr',
                        'int',
                    ],
                    'listTypeVille' => $listTypeVilleJson,
                    'listObjectif'  => $listObjectifJson,
                    'listLead'      => $listLeadJson,
                    'listStatut'    => $listStatutJson,
                    'listSkill'     => $listSkillJson,
                    'listSkillBase' => $listSkillBaseJson,
                    'maxAccepted'   => 40,
                    'statutAcc'     => StatutInscription::ST_ACC,
                ],
                'user'    => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
            
            ],
            'general'     => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route(path: '/associate', name: 'associate', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function associate(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new AssociateVilleDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), AssociateVilleDto::class, 'json', $data, ['jump']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct gestion - associate - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() !== $this->user->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur ne correspondent pas- associate - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump non fournis - associate - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($this->user->getMapId() === null) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("L'association de la ville n'est pas possible, vous n'êtes actuellement pas en ville",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $this->user->getMapId()]);
        $jump  = $this->jumpHandler->recuperationJump($data->getIdJump());
        
        if ($ville !== null) {
            if ($ville->getJump() !== null) {
                $retour['codeRetour'] = 1;
                $retour['libRetour']  =
                    $this->translator->trans("La ville actuelle a déjà été associé à un jump.", [], 'jumpEvent');
                return new JsonResponse($retour);
            } else {
                $jump->setVille($ville);
                
                try {
                    // On balaye tous les citoyens avec les différentes candidatures pour mettre à jour le lvl ruine
                    
                    foreach ($ville->getCitoyens() as $citoyen) {
                        
                        $inscriptionJump = $jump->getInscriptionJump($citoyen->getCitoyen()->getId());
                        
                        if ($inscriptionJump != null) {
                            
                            $citoyen->setLvlRuine($inscriptionJump->getLvlRuine()->getLevel());
                            
                            if ($citoyen->getSkill()->count() > 0 && $inscriptionJump->getSkill()->count() > 0) {
                                foreach ($citoyen->getSkill() as $skill) {
                                    $citoyen->removeSkill($skill);
                                }
                            }
                            foreach ($inscriptionJump->getSkill() as $skill) {
                                $citoyen->addSkill($skill);
                            }
                            
                            $this->entityManager->persist($citoyen);
                            
                        }
                        
                    }
                    
                    $logAssociate = new LogEventJump();
                    $logAssociate->setLibelle("Association de la ville")
                                 ->setValeurAvant(null)
                                 ->setValeurApres(null)
                                 ->setDeclencheur($this->user)
                                 ->setEventAt(new DateTime('now'));
                    
                    $jump->addLogEvent($logAssociate);
                    
                    $this->entityManager->persist($jump);
                    $this->entityManager->flush();
                    $this->entityManager->refresh($jump);
                    
                    $retour['codeRetour'] = 0;
                    $retour['libRetour']  = $this->translator->trans("Association réussi !", [], 'app');
                    $retour['zoneRetour'] = ['jump' => $this->serializerService->serializeArray($jump, 'json', ['gestion_jump', 'gestion_jump_spe', 'jump', 'general_res'])];
                    return new JsonResponse($retour);
                } catch (Exception $exception) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    $messageLog           =
                        "$codeErreur - Problème association ville - Joueur : {$this->user?->getId()} - {$exception->getMessage()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                }
            }
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Ville inconnus - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
    }
    
    #[Route('/creation_banniere', name: 'creationBanniere', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function creationBanniere(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        
        $upload  = $data['up'] ?? 1;
        $oldName = $data['oldName'] ?? null;
        
        if (isset($data['image'])) {
            $file_chunks = explode(";base64,", (string)$data['image']);
            //$fileType   = explode("image/", $file_chunks[0]);
            //$image_type = $fileType[1];
            $image = base64_decode($file_chunks[1]);
            $mime  = $data['mime'] ?? 'png';
        } else {
            $image = null;
            $mime  = null;
        }
        
        
        if ($image === null && $upload == 1) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous n'avez pas fourni d'image.", [], 'jumpEvent') . ' - ' . $codeErreur;
            $messageLog           = "$codeErreur - Image non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($oldName !== null && $oldName !== '' && file_exists('../public' . $oldName)) {
            unlink('../public' . $oldName);
        }
        
        if ($upload == 1) {
            
            $filenameNew = bin2hex(random_bytes(12)) . '.' . $mime;
            
            
            $erreur = $this->gestionHandler->gestionBanniere($image, $filenameNew, $mime);
            
            if ($erreur !== GestionHandler::NoError) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  =
                    $this->translator->trans("Un problème est survenu lors de l'enregistrement de l'image.", [],
                                             'jumpEvent') . ' - ' . $codeErreur;
                $messageLog           = "$codeErreur - Problème image - Joueur : {$this->user?->getId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            }
            
            $retour['zoneRetour'] = ['urlBanniere' => $filenameNew, 'fileName' => $filenameNew];
            
        } else {
            $retour['zoneRetour'] = ['urlBanniere' => null, 'fileName' => null];
        }
        
        
        $retour['codeRetour'] = 0;
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route('/creation_event', name: 'creationEvent', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function creationEvent(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $newEventTmp = new EventDto();
        
        $this->serializerService->deserialize($request->getContent(), EventDto::class, 'json', $newEventTmp, ['creation_jump', 'creation_event', 'event', 'jump']);
        
        $newEvent = $newEventTmp->getEvent();
        
        if ($newEvent->getNom() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Nom Event non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($newEvent->getDebInscriptionDateAt() > $newEvent->getFinInscriptionDateAt()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de début d'inscription doit être inférieur à la date de fin d'inscription.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($newEvent->getFinInscriptionDateAt() > $newEvent->getEventBeginAt()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de fin d'inscription doit être inférieur à la date du début de l'évènement.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($newEvent->getTypeVille() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Type ville n'est pas fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            $typeVille =
                $this->entityManager->getRepository(TypeVille::class)->findOneBy(['id' => $newEvent->getTypeVille()
                                                                                                   ->getId()]);
            if ($typeVille === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           = "$codeErreur - Type ville non récupéré - Joueur : {$this->user?->getId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            } else {
                $newEvent->setTypeVille($typeVille);
            }
        }
        if ($newEvent->getCommunity() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Communauté non pas fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($newEvent->getBanniere() === '') {
            $newEvent->setBanniere(null);
        } else {
            $newEvent->setBanniere($newEvent->getBanniere());
        }
        
        if (count($newEvent->getListJump()) === 0) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Le nombre de jump doit être > 0 - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            foreach ($newEvent->getListJump() as $jump) {
                
                if ($jump->getNom() === null || $jump->getNom() === '') {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    $messageLog           =
                        "$codeErreur - le nom des villes n'a pas été fournis dans l'event - Joueur : {$this->user?->getId()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                } else {
                    
                    $libTranslate = $this->translator->trans("Création d'un jump par l'événement", [], 'jumpEvent');
                    $logCreation  = new LogEventJump();
                    $logCreation->setLibelle("Création d'un jump par l'événement")
                                ->setValeurApres(null)
                                ->setValeurAvant(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                    
                    
                    $jump->setId(null)
                         ->setBanniere($newEvent->getBanniere())
                         ->setDateCreation(new DateTime('Now'))
                         ->setDateDebInscription($newEvent->getDebInscriptionDateAt())
                         ->setDateFinInscription($newEvent->getFinInscriptionDateAt())
                         ->setDateApproxJump($newEvent->getEventBeginAt())
                         ->setVillePrive($newEvent->getEventPrived())
                         ->setTypeVille($newEvent->getTypeVille())
                         ->setCreatedBy($this->user)
                         ->setCommunity($newEvent->getCommunity())
                         ->addLogEvent($logCreation);
                    
                    // Si le jump est en jump spécific, on balaye les métiers pour récupérer les prototypes
                    if ($jump->isJobSpecific()) {
                        foreach ($jump->getJob() as $job) {
                            if ($job === null) {
                                $codeErreur           = $this->randomString();
                                $retour['codeRetour'] = 1;
                                $retour['libRetour']  = $this->translator->trans(
                                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                                    ['{code}' => $codeErreur],
                                    'app',
                                );
                                $messageLog           = "$codeErreur - Le metier n'a pas été renseigné alors qu'il y a l'option job spécifique : {$this->user?->getId()}";
                                $this->logger->error($messageLog);
                                $this->gh->generateMessageDiscord($messageLog);
                                return new JsonResponse($retour);
                            }
                            $jobProto = $this->entityManager->getRepository(JobPrototype::class)->findOneBy(['id' => $job->getId()]);
                            
                            if ($jobProto !== null) {
                                $jump->removeJob($job);
                                $jump->addJob($jobProto);
                            }
                        }
                    }
                    
                }
            }
        }
        
        
        try {
            
            $newEvent->addOrganisateur($this->user);
            
            $libTranslate = $this->translator->trans("Création de l'event", [], 'jumpEvent');
            $logCreation  = new LogEventEvent();
            $logCreation->setLibelle("Création de l'event")
                        ->setValeurAvant(null)
                        ->setValeurApres(null)
                        ->setDeclencheur($this->user)
                        ->setEventAt(new DateTime('now'));
            
            $newEvent->addLogEvent($logCreation);
            
            $this->entityManager->persist($newEvent);
            $this->entityManager->flush();
            $this->entityManager->refresh($newEvent);
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  =
                $this->translator->trans("Création réussi, vous allez être redirigé vers la page d'inscription de l'évènement afin de vous y inscrire et d'être comptabilisé dans l'un des jumps.",
                                         [], 'app');
            $retour['zoneRetour'] = ['event' => $newEvent->getId()];
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème création event - User {$this->user->getId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route('/creation_jump', name: 'creationJump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function creationJump(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $newJumpTmp = new JumpDto();
        
        $this->serializerService->deserialize($request->getContent(), JumpDto::class, 'json', $newJumpTmp, ['creation_jump', 'jump']);
        
        $newJump = $newJumpTmp->getJump();
        
        if ($newJump->getNom() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Nom jump non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($newJump->getDateDebInscription() > $newJump->getDateFinInscription()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Date de début > date de fin - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($newJump->getDateFinInscription() > $newJump->getDateApproxJump()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Date de début > date de fin - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($newJump->getTypeVille() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Type ville n'est pas fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            $typeVille =
                $this->entityManager->getRepository(TypeVille::class)->findOneBy(['id' => $newJump->getTypeVille()
                                                                                                  ->getId()]);
            if ($typeVille === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           = "$codeErreur - Type ville non récupéré - Joueur : {$this->user?->getId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            } else {
                $newJump->setTypeVille($typeVille);
            }
        }
        if ($newJump->getCommunity() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Communauté n'est pas fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (count($newJump->getObjectif()) === 0) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Objectif non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            foreach ($newJump->getObjectif() as $objectif) {
                $objectifBdd = $this->entityManager->getRepository(ObjectifVillePrototype::class)
                                                   ->findOneBy(['id' => $objectif->getId()]);
                
                if ($objectifBdd === null) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    $messageLog           = "$codeErreur - Objectif non connus - Joueur : {$this->user?->getId()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                } else {
                    $newJump->removeObjectif($objectif);
                    $newJump->addObjectif($objectifBdd);
                }
            }
        }
        
        if ($newJump->getBanniere() === '') {
            $newJump->setBanniere(null);
        } else {
            $newJump->setBanniere($newJump->getBanniere());
        }
        
        
        $newJump->addGestionnaire($this->user)
                ->setCreatedBy($this->user)
                ->setDateCreation(new DateTime("Now"));
        
        
        $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
        
        foreach ($listCreneau as $creneau) {
            $newJump->addCreneauSemaine($creneau);
            $newJump->addCreneauWeekend($creneau);
        }
        
        $libTranslate = $this->translator->trans("Création du jump", [], 'jumpEvent');
        $logCreation  = new LogEventJump();
        $logCreation->setLibelle("Création du jump")
                    ->setValeurAvant(null)
                    ->setValeurApres(null)
                    ->setDeclencheur($this->user)
                    ->setEventAt(new DateTime('now'));
        
        $newJump->addLogEvent($logCreation);
        
        // Création de 2 créneaux de jump
        $this->jumpHandler->calculCrenauJump($newJump);
        
        
        try {
            $this->entityManager->persist($newJump);
            $this->entityManager->flush();
            $this->entityManager->refresh($newJump);
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  =
                $this->translator->trans("Création réussie, vous allez être redirigé vers la page d'inscription afin de vous y inscrire et d'être comptabilisé dans le jump.",
                                         [], 'app');
            $retour['zoneRetour'] = ['jump' => $newJump->getId()];
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème création jump - User {$this->user->getId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route('/gestion_event/{id}', name: 'gestionEvent', methods: ['PUT']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function gestionEvent(Request $request, Event $event): JsonResponse
    {
        
        $modificationJumpDate   = false;
        $modificationJumpType   = false;
        $modificationJumpPrived = false;
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        if ($event === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Event non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Event::class)
                                 ->habiliterGestion($event->getId(), $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Utilisateur non habilité à modifier l'event";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new EventDto();
        
        try {
            
            $this->serializerService->deserialize($request->getContent(), EventDto::class, 'json', $data, ['gestion_event', 'event', 'general_res']);
            
            $eventModifier = $data->getEvent();
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct event - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($event->getId() !== $eventModifier->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Event fournis : {$event->getId()} - Event récupérer : {$eventModifier->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $listErreur = [];
        
        if ($eventModifier->getNom() === '' || $eventModifier->getNom() === null) {
            $listErreur[] = $this->translator->trans("Nom de l'event est vide.", [], 'jumpEvent');
        } else {
            if ($event->getNom() !== $eventModifier->getNom()) {
                $libTranslate    = $this->translator->trans("Modification du nom de l'event", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification du nom de l'event")
                                ->setValeurAvant($event->getNom())
                                ->setValeurApres($eventModifier->getNom())
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setNom($eventModifier->getNom())
                      ->addLogEvent($logModification);
                
            }
        }
        
        if ($eventModifier->getBanniere() !== $event->getBanniere()) {
            $libTranslate    = $this->translator->trans("Modification de la bannière de l'event", [], 'jumpEvent');
            $logModification = new LogEventEvent();
            $logModification->setLibelle("Modification de la bannière de l'event")
                            ->setValeurAvant($event->getBanniere())
                            ->setValeurApres($eventModifier->getBanniere())
                            ->setDeclencheur($this->user)
                            ->setEventAt(new DateTime('now'));
            
            $event->setBanniere($eventModifier->getBanniere())
                  ->addLogEvent($logModification);
            
        }
        
        
        if ($eventModifier->getDebInscriptionDateAt() === '' || $eventModifier->getDebInscriptionDateAt() === null) {
            $listErreur[] = $this->translator->trans("La date de début d'inscription est vide.", [], 'jumpEvent');
        } else {
            if ($event->getDebInscriptionDateAt()->getTimestamp() !==
                $eventModifier->getDebInscriptionDateAt()->getTimestamp()) {
                $libTranslate    =
                    $this->translator->trans("Modification de la date de début d'inscription de l'event", [],
                                             'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de la date de début d'inscription de l'event")
                                ->setValeurAvant(($event->getDebInscriptionDateAt())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($eventModifier->getDebInscriptionDateAt())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setDebInscriptionDateAt($eventModifier->getDebInscriptionDateAt())
                      ->addLogEvent($logModification);
                $modificationJumpDate = true;
            }
        }
        
        if ($eventModifier->getFinInscriptionDateAt() === '' || $eventModifier->getFinInscriptionDateAt() === null) {
            $listErreur[] = $this->translator->trans("La date de fin d'inscription est vide.", [], 'jumpEvent');
        } else {
            if ($event->getFinInscriptionDateAt()->getTimestamp() !==
                $eventModifier->getFinInscriptionDateAt()->getTimestamp()) {
                $libTranslate    =
                    $this->translator->trans("Modification de la date de fin d'inscription de l'event", [],
                                             'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de la date de fin d'inscription de l'event")
                                ->setValeurAvant(($event->getFinInscriptionDateAt())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($eventModifier->getFinInscriptionDateAt())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setFinInscriptionDateAt($eventModifier->getFinInscriptionDateAt())
                      ->addLogEvent($logModification);
                $modificationJumpDate = true;
            }
        }
        
        if ($eventModifier->getEventBeginAt() === '' || $eventModifier->getEventBeginAt() === null) {
            $listErreur[] = $this->translator->trans("La date de début de l'événement est vide.", [], 'jumpEvent');
        } else {
            if ($event->getEventBeginAt()->getTimestamp() !== $eventModifier->getEventBeginAt()->getTimestamp()) {
                $libTranslate    =
                    $this->translator->trans("Modification de la date de début de l'événement", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de la date de début de l'événement")
                                ->setValeurAvant(($event->getEventBeginAt())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($eventModifier->getEventBeginAt())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setEventBeginAt($eventModifier->getEventBeginAt())
                      ->addLogEvent($logModification);
                $modificationJumpDate = true;
            }
        }
        
        if ($eventModifier->getTypeVille() === '' || $eventModifier->getTypeVille() === null) {
            $listErreur[] = $this->translator->trans("Le type de ville ne peut pas être vide.", [], 'jumpEvent');
        } else {
            if ($event->getTypeVille()->getId() !== $eventModifier->getTypeVille()->getId()) {
                $libTranslate    = $this->translator->trans("Modification du type de ville", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification du type de ville")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $typeVilleBdd = $this->entityManager->getRepository(TypeVille::class)
                                                    ->findOneBy(['id' => $eventModifier->getTypeVille()->getId()]);
                
                $event->setTypeVille($typeVilleBdd)
                      ->addLogEvent($logModification);
                $modificationJumpType = true;
            }
        }
        
        if ($eventModifier->getEventPrived() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Event fournis : {$event->getId()} - valeur event privée erronée";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            if ($event->getEventPrived() !== $eventModifier->getEventPrived()) {
                $libTranslate    = $this->translator->trans("Modification de l'option event privée", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de l'option event privée")
                                ->setValeurAvant($event->getEventPrived() ? '1' : '0')
                                ->setValeurApres($eventModifier->getEventPrived() ? '1' : '0')
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $modificationJumpPrived = true;
                
                $event->setEventPrived($eventModifier->getEventPrived())
                      ->addLogEvent($logModification);
                
            }
        }
        
        if ($eventModifier->getOneMetier() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Event fournis : {$event->getId()} - valeur one métier erronée";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            if ($event->getOneMetier() !== $eventModifier->getOneMetier()) {
                $libTranslate    =
                    $this->translator->trans("Modification de l'option 'Un seul métier'", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de l'option 'Un seul métier'")
                                ->setValeurAvant($event->getOneMetier() ? '1' : '0')
                                ->setValeurApres($eventModifier->getOneMetier() ? '1' : '0')
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setOneMetier($eventModifier->getOneMetier())
                      ->addLogEvent($logModification);
                
            }
        }
        
        if ($eventModifier->getCommunity() === '' || $eventModifier->getCommunity() === null) {
            $listErreur[] = $this->translator->trans("La communauté ne peux pas être vide.", [], 'jumpEvent');
        } else {
            if ($event->getCommunity() !== $eventModifier->getCommunity()) {
                $libTranslate    = $this->translator->trans("Modification de la communauté", [], 'jumpEvent');
                $logModification = new LogEventEvent();
                $logModification->setLibelle("Modification de la communauté")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $event->setCommunity($eventModifier->getCommunity())
                      ->addLogEvent($logModification);
                
            }
        }
        
        
        if ($event->getDescription() !== $eventModifier->getDescription()) {
            $libTranslate    = $this->translator->trans("Modification de la description", [], 'jumpEvent');
            $logModification = new LogEventEvent();
            $logModification->setLibelle("Modification de la description")
                            ->setValeurAvant(substr($event->getDescription(), 0, 255))
                            ->setValeurApres(substr($eventModifier->getDescription(), 0, 255))
                            ->setDeclencheur($this->user)
                            ->setEventAt(new DateTime('now'));
            
            $event->setDescription($eventModifier->getDescription())
                  ->addLogEvent($logModification);
            
        }
        
        // Controle sur les dates
        
        if ($eventModifier->getDebInscriptionDateAt() > $eventModifier->getFinInscriptionDateAt()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de début d'inscription doit être inférieur à la date de fin d'inscription.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($eventModifier->getFinInscriptionDateAt() > $eventModifier->getEventBeginAt()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de fin d'inscription doit être inférieur à la date du début de l'évènement.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        // Sauvegarder des organisateurs
        $orgaInEvent = (json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))['event']['organisateurs'];
        
        if (count($orgaInEvent) === 0) {
            $listErreur[] =
                $this->translator->trans("Il doit obligatoirement avoir des organisateurs à l'événement.", [],
                                         'jumpEvent');
        } else {
            $listNewOrga = [];
            foreach ($orgaInEvent as $orga) {
                $listNewOrga[$orga['id']] = $orga;
            }
            
            foreach ($event->getOrganisateurs() as $organisateur) {
                if (!isset($listNewOrga[$organisateur->getId()])) {
                    $libTranslate    = $this->translator->trans("Suppression d'un organisateur", [], 'jumpEvent');
                    $logModification = new LogEventEvent();
                    $logModification->setLibelle("Suppression d'un organisateur")
                                    ->setValeurAvant($organisateur->getPseudo())
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $event->removeOrganisateur($organisateur)
                          ->addLogEvent($logModification);
                    
                } else {
                    unset($listNewOrga[$organisateur->getId()]);
                }
            }
            
            foreach ($listNewOrga as $orga) {
                $organisateur = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $orga['id']]);
                
                if ($organisateur !== null) {
                    $libTranslate    = $this->translator->trans("Ajout d'un organisateur", [], 'jumpEvent');
                    $logModification = new LogEventEvent();
                    $logModification->setLibelle("Ajout d'un organisateur")
                                    ->setValeurAvant(null)
                                    ->setValeurApres($organisateur->getPseudo())
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $event->addOrganisateur($organisateur)
                          ->addLogEvent($logModification);
                }
            }
            
        }
        
        if (count($eventModifier->getListJump()) === 0) {
            $listErreur[] =
                $this->translator->trans("Il doit obligatoirement avoir des jumps à l'événement.", [], 'jumpEvent');
        } else {
            $listNewJump = [];
            foreach ($eventModifier->getListJump() as $jump) {
                $listNewJump[$jump->getId()] = $jump;
            }
            
            foreach ($event->getListJump() as $jump) {
                if (!isset($listNewJump[$jump->getId()])) {
                    
                    $libTranslate    = $this->translator->trans("Suppression d'une ville", [], 'jumpEvent');
                    $logModification = new LogEventEvent();
                    $logModification->setLibelle("Suppression d'une ville")
                                    ->setValeurAvant($jump->getNom())
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    
                    foreach ($jump->getInscriptionJumps() as $inscriptionJump) {
                        foreach ($inscriptionJump->getUser()->getOrgaEvents() as $orgaEvent) {
                            if ($orgaEvent->getId() === $event->getId()) {
                                $inscritOtherJump = false;
                                foreach ($event->getListJump() as $jumpEvent) {
                                    if ($jumpEvent->getInscriptionJump($inscriptionJump->getUser()->getId()) !== null &&
                                        $jumpEvent->getId() !== $jump->getId()) {
                                        $inscritOtherJump = true;
                                    }
                                }
                                if (!$inscritOtherJump) {
                                    $event->removeOrganisateur($orgaEvent);
                                }
                            }
                        }
                    }
                    
                    $event->removeListJump($jump)
                          ->addLogEvent($logModification);
                } else {
                    
                    if ($listNewJump[$jump->getId()]->getNom() !== $jump->getNom()) {
                        $libTranslate    = $this->translator->trans("Modification d'une ville", [], 'jumpEvent');
                        $logModification = new LogEventEvent();
                        $logModification->setLibelle("Modification d'une ville")
                                        ->setValeurAvant($jump->getNom())
                                        ->setValeurApres($listNewJump[$jump->getId()]->getNom())
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $event->addLogEvent($logModification);
                        
                        $libTranslate    =
                            $this->translator->trans("Modification d'un nom de ville par l'Event", [], 'jumpEvent');
                        $logModification = new LogEventJump();
                        $logModification->setLibelle("Modification d'un nom de ville par l'Event")
                                        ->setValeurAvant($jump->getNom())
                                        ->setValeurApres($listNewJump[$jump->getId()]->getNom())
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $jump->setNom($listNewJump[$jump->getId()]->getNom())
                             ->setDateModif(new DateTime('now'))
                             ->addLogEvent($logModification);
                    }
                    
                    // Si le jump change de statut spécific
                    if ($listNewJump[$jump->getId()]->isJobSpecific() !== $jump->isJobSpecific()) {
                        $libTranslate    = $this->translator->trans("Modification d'une ville", [], 'jumpEvent');
                        $logModification = new LogEventEvent();
                        $logModification->setLibelle("Modification d'une ville")
                                        ->setValeurAvant($jump->getNom())
                                        ->setValeurApres($listNewJump[$jump->getId()]->getNom())
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $event->addLogEvent($logModification);
                        
                        $libTranslate    =
                            $this->translator->trans("Modification métier spécifique par l'Event", [], 'jumpEvent');
                        $logModification = new LogEventJump();
                        $logModification->setLibelle("Modification métier spécifique par l'Event")
                                        ->setValeurAvant($jump->getNom())
                                        ->setValeurApres($listNewJump[$jump->getId()]->getNom())
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $jump->setJobSpecific($listNewJump[$jump->getId()]->isJobSpecific())
                             ->addLogEvent($logModification);
                    }
                    
                    // Si le jump est en jump spécific, on balaye les métiers pour récupérer les prototypes
                    if ($jump->isJobSpecific()) {
                        foreach ($jump->getJob() as $index => $job) {
                            if ($listNewJump[$jump->getId()]->getJob()[$index] === null) {
                                $listErreur[] = $this->translator->trans("Il manque un métier de saisie sur une des villes avec l'option Job spécifique", [], 'jumpEvent');
                            }
                            $jobProto = $this->entityManager->getRepository(JobPrototype::class)->findOneBy(['id' => $listNewJump[$jump->getId()]->getJob()[$index]->getId()]);
                            
                            if ($jobProto !== null) {
                                $jump->removeJob($job);
                                $jump->addJob($jobProto);
                            }
                        }
                    }
                    
                    if ($modificationJumpDate) {
                        
                        $libTranslate    =
                            $this->translator->trans("Modification des dates de la ville par l'Event", [], 'jumpEvent');
                        $logModification = new LogEventJump();
                        $logModification->setLibelle("Modification des dates de la ville par l'Event")
                                        ->setValeurAvant(null)
                                        ->setValeurApres(null)
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $jump->setDateDebInscription($event->getDebInscriptionDateAt())
                             ->setDateFinInscription($event->getFinInscriptionDateAt())
                             ->setDateApproxJump($event->getEventBeginAt())
                             ->setDateModif(new DateTime('now'))
                             ->addLogEvent($logModification);
                    }
                    if ($modificationJumpType) {
                        
                        $libTranslate    =
                            $this->translator->trans("Modification du type des villes par l'Event", [], 'jumpEvent');
                        $logModification = new LogEventJump();
                        $logModification->setLibelle("Modification du type des villes par l'Event")
                                        ->setValeurAvant(null)
                                        ->setValeurApres(null)
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $jump->setTypeVille($event->getTypeVille())
                             ->setDateModif(new DateTime('now'))
                             ->addLogEvent($logModification);
                    }
                    if ($modificationJumpPrived) {
                        
                        $libTranslate    =
                            $this->translator->trans("Modification de l'option 'Ville privée' des villes par l'Event",
                                                     [], 'jumpEvent');
                        $logModification = new LogEventJump();
                        $logModification->setLibelle("Modification de l'option 'Ville privée' des villes par l'Event")
                                        ->setValeurAvant(null)
                                        ->setValeurApres(null)
                                        ->setDeclencheur($this->user)
                                        ->setEventAt(new DateTime('now'));
                        
                        $jump->setVillePrive($event->getEventPrived())
                             ->setDateModif(new DateTime('now'))
                             ->addLogEvent($logModification);
                    }
                    
                    unset($listNewJump[$jump->getId()]);
                }
            }
            
            foreach ($listNewJump as $jump) {
                $jumpBdd = $this->entityManager->getRepository(Jump::class)->findOneBy(['id' => $jump->getId()]);
                
                if ($jumpBdd === null) {
                    $libTranslate    = $this->translator->trans("Création d'une ville", [], 'jumpEvent');
                    $logModification = new LogEventEvent();
                    $logModification->setLibelle("Création d'une ville")
                                    ->setValeurAvant(null)
                                    ->setValeurApres($jump->getNom())
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    
                    $event->addLogEvent($logModification);
                    
                    $newJump = new Jump();
                    
                    $libTranslate    = $this->translator->trans("Création d'une ville par l'Event", [], 'jumpEvent');
                    $logModification = new LogEventJump();
                    $logModification->setLibelle("Création d'une ville par l'Event")
                                    ->setValeurAvant(null)
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    
                    $newJump->setNom($jump->getNom())
                            ->setDateDebInscription($event->getDebInscriptionDateAt())
                            ->setDateFinInscription($event->getFinInscriptionDateAt())
                            ->setDateApproxJump($event->getEventBeginAt())
                            ->setVillePrive($event->getEventPrived())
                            ->setTypeVille($event->getTypeVille())
                            ->addLogEvent($logModification)
                            ->setCreatedBy($this->user)
                            ->setJobSpecific($jump->isJobSpecific())
                            ->setDateCreation(new DateTime('now'));
                    
                    // Si le jump est en jump spécific, on balaye les métiers pour récupérer les prototypes
                    if ($jump->isJobSpecific()) {
                        foreach ($jump->getJob() as $job) {
                            $jobProto = $this->entityManager->getRepository(JobPrototype::class)->findOneBy(['id' => $job->getId()]);
                            
                            if ($jobProto !== null) {
                                $newJump->addJob($jobProto);
                            }
                        }
                    }
                    
                    $event->addListJump($newJump);
                }
            }
            
            
        }
        
        try {
            
            if (count($listErreur) > 0) {
                
                $retour['codeRetour'] = 2;
                $retour['libRetour']  =
                    $this->translator->trans("Modification impossible, il y a des erreurs.", [], 'app');
                $retour['zoneRetour'] = ['libErreur' => $listErreur];
            } else {
                $this->entityManager->persist($event);
                $this->entityManager->flush();
                $this->entityManager->refresh($event);
                
                $retour['codeRetour'] = 0;
                $retour['libRetour']  = $this->translator->trans("Modification réussie.", [], 'app');
                $retour['zoneRetour'] = ['event' => $this->serializerService->serializeArray($event, 'json', ['gestion_event', 'event', 'jump', 'general_res'])];
            }
            
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème modification event - User {$this->user->getId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        
        return new JsonResponse($retour, 200);
        
    }
    
    #[Route('/gestion_jump/{id}', name: 'gestionJump', methods: ['PUT']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function gestionJump(Request $request, Jump $jump): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Jump non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Utilisateur non habilité à modifier le jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new JumpDto();
        
        try {
            
            $this->serializerService->deserialize($request->getContent(), JumpDto::class, 'json', $data, ['gestion_jump', 'jump', 'general_res']);
            
            $jumpModifier = $data->getJump();
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($jump->getId() !== $jumpModifier->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$jump->getId()} - Event récupérer : {$jumpModifier->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $listErreur = [];
        
        if ($jumpModifier->getNom() === '' || $jumpModifier->getNom() === null) {
            $listErreur[] = $this->translator->trans("Nom du jump est vide.", [], 'jumpEvent');
        } else {
            if ($jump->getNom() !== $jumpModifier->getNom()) {
                $libTranslate    = $this->translator->trans("Modification du nom du jump", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification du nom de l'event")
                                ->setValeurAvant($jump->getNom())
                                ->setValeurApres($jumpModifier->getNom())
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $jump->setNom($jumpModifier->getNom())
                     ->addLogEvent($logModification);
                
            }
        }
        
        if ($jumpModifier->getBanniere() !== $jump->getBanniere()) {
            $libTranslate    = $this->translator->trans("Modification de la bannière du jump", [], 'jumpEvent');
            $logModification = new LogEventJump();
            $logModification->setLibelle("Modification de la bannière du jump")
                            ->setValeurAvant($jump->getBanniere())
                            ->setValeurApres($jumpModifier->getBanniere())
                            ->setDeclencheur($this->user)
                            ->setEventAt(new DateTime('now'));
            
            $jump->setBanniere($jumpModifier->getBanniere())
                 ->addLogEvent($logModification);
            
        }
        
        
        if ($jumpModifier->getDateDebInscription() === '' || $jumpModifier->getDateDebInscription() === null) {
            $listErreur[] = $this->translator->trans("La date de début d'inscription est vide.", [], 'jumpEvent');
        } else {
            if ($jump->getDateDebInscription()->getTimestamp() !==
                $jumpModifier->getDateDebInscription()->getTimestamp()) {
                $libTranslate    =
                    $this->translator->trans("Modification de la date de début d'inscription du jump", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification de la date de début d'inscription du jump")
                                ->setValeurAvant(($jump->getDateDebInscription())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($jumpModifier->getDateDebInscription())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $jump->setDateDebInscription($jumpModifier->getDateDebInscription())
                     ->addLogEvent($logModification);
            }
        }
        
        if ($jumpModifier->getDateFinInscription() === '' || $jumpModifier->getDateFinInscription() === null) {
            $listErreur[] = $this->translator->trans("La date de fin d'inscription est vide.", [], 'jumpEvent');
        } else {
            if ($jump->getDateFinInscription()->getTimestamp() !==
                $jumpModifier->getDateFinInscription()->getTimestamp()) {
                $libTranslate    =
                    $this->translator->trans("Modification de la date de fin d'inscription du jump", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification de la date de fin d'inscription du jump")
                                ->setValeurAvant(($jump->getDateFinInscription())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($jumpModifier->getDateFinInscription())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $jump->setDateFinInscription($jumpModifier->getDateFinInscription())
                     ->addLogEvent($logModification);
            }
        }
        
        if ($jumpModifier->getDateApproxJump() === '' || $jumpModifier->getDateApproxJump() === null) {
            $listErreur[] = $this->translator->trans("La date du jump approximatif est vide.", [], 'jumpEvent');
        } else {
            if ($jump->getDateApproxJump()->getTimestamp() !== $jumpModifier->getDateApproxJump()->getTimestamp()) {
                $libTranslate    = $this->translator->trans("Modification de la date de jump", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification de la date de jump")
                                ->setValeurAvant(($jump->getDateApproxJump())->format('Y-m-d H:i:s'))
                                ->setValeurApres(($jumpModifier->getDateApproxJump())->format('Y-m-d H:i:s'))
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $jump->setDateApproxJump($jumpModifier->getDateApproxJump())
                     ->addLogEvent($logModification);
                
                // Modification des 2 créneaux de jump
                $this->jumpHandler->calculCrenauJump($jump);
            }
        }
        
        if ($jumpModifier->getTypeVille() === '' || $jumpModifier->getTypeVille() === null) {
            $listErreur[] = $this->translator->trans("Le type de ville ne peut pas être vide.", [], 'jumpEvent');
        } else {
            if ($jump->getTypeVille()->getId() !== $jumpModifier->getTypeVille()->getId()) {
                $libTranslate    = $this->translator->trans("Modification du type de ville", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification du type de ville")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                $typeVilleBdd = $this->entityManager->getRepository(TypeVille::class)
                                                    ->findOneBy(['id' => $jumpModifier->getTypeVille()->getId()]);
                
                $jump->setTypeVille($typeVilleBdd)
                     ->addLogEvent($logModification);
            }
        }
        
        if ($jumpModifier->getVillePrive() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Event fournis : {$jump->getId()} - valeur event privée erronée";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        } else {
            if ($jump->getVillePrive() !== $jumpModifier->getVillePrive()) {
                $libTranslate    =
                    $this->translator->trans("Modification de l'option jump en ville privée", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification de l'option event privée")
                                ->setValeurAvant($jump->getVillePrive() ? '1' : '0')
                                ->setValeurApres($jumpModifier->getVillePrive() ? '1' : '0')
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                
                $jump->setVillePrive($jumpModifier->getVillePrive())
                     ->addLogEvent($logModification);
                
            }
        }
        
        if ($jumpModifier->getCommunity() === '' || $jumpModifier->getCommunity() === null) {
            $listErreur[] = $this->translator->trans("La communauté ne peux pas être vide.", [], 'jumpEvent');
        } else {
            if ($jump->getCommunity() !== $jumpModifier->getCommunity()) {
                $libTranslate    = $this->translator->trans("Modification de la communauté du jump", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Modification de la communauté du jump")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                
                
                $jump->setCommunity($jumpModifier->getCommunity())
                     ->addLogEvent($logModification);
                
            }
        }
        
        if ($jump->getDescription() !== $jumpModifier->getDescription()) {
            $libTranslate    = $this->translator->trans("Modification de la description", [], 'jumpEvent');
            $logModification = new LogEventJump();
            $logModification->setLibelle("Modification de la description")
                            ->setValeurAvant(substr($jump->getDescription(), 0, 255))
                            ->setValeurApres(substr($jumpModifier->getDescription(), 0, 255))
                            ->setDeclencheur($this->user)
                            ->setEventAt(new DateTime('now'));
            
            $jump->setDescription($jumpModifier->getDescription())
                 ->addLogEvent($logModification);
            
        }
        
        // Controle sur les dates
        
        if ($jumpModifier->getDateDebInscription()->getTimestamp() >
            $jumpModifier->getDateFinInscription()->getTimestamp()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de début d'inscription doit être inférieur à la date de fin d'inscription.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($jumpModifier->getDateFinInscription()->getTimestamp() >
            $jumpModifier->getDateApproxJump()->getTimestamp()) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("La date de fin d'inscription doit être inférieur à la date approximative de jump.",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        // Sauvegarder des organisateurs
        $objectifJump = (json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))['jump']['objectif'];
        if (count($objectifJump) === 0 && $jump->getEvent() === null) {
            $listErreur[] =
                $this->translator->trans("Il doit obligatoirement avoir un objectif au jump.", [], 'jumpEvent');
        } else {
            $listNewObjectif = [];
            foreach ($objectifJump as $objectif) {
                $listNewObjectif[$objectif['id']] = $objectif;
            }
            
            foreach ($jump->getObjectif() as $objectif) {
                if (!isset($listNewObjectif[$objectif->getId()])) {
                    $libTranslate    = $this->translator->trans("Suppression d'un objectif", [], 'jumpEvent');
                    $logModification = new LogEventJump();
                    $logModification->setLibelle("Suppression d'un objectif")
                                    ->setValeurAvant(null)
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $jump->removeObjectif($objectif)
                         ->addLogEvent($logModification);
                    
                } else {
                    unset($listNewObjectif[$objectif->getId()]);
                }
            }
            
            foreach ($listNewObjectif as $objectif) {
                $objectifBdd = $this->entityManager->getRepository(ObjectifVillePrototype::class)
                                                   ->findOneBy(['id' => $objectif['id']]);
                
                if ($objectifBdd !== null) {
                    $libTranslate    = $this->translator->trans("Ajout d'un objectif", [], 'jumpEvent');
                    $logModification = new LogEventJump();
                    $logModification->setLibelle("Ajout d'un objectif")
                                    ->setValeurAvant(null)
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $jump->addObjectif($objectifBdd)
                         ->addLogEvent($logModification);
                }
            }
            
        }
        
        
        // Sauvegarder des organisateurs
        $orgaInJump = (json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))['jump']['gestionnaires'];
        
        if (count($orgaInJump) === 0) {
            $listErreur[] =
                $this->translator->trans("Il doit obligatoirement avoir des organisateurs au jump.", [], 'jumpEvent');
        } else {
            $listNewOrga = [];
            foreach ($orgaInJump as $orga) {
                $listNewOrga[$orga['id']] = $orga;
            }
            
            foreach ($jump->getGestionnaires() as $organisateur) {
                if (!isset($listNewOrga[$organisateur->getId()])) {
                    $libTranslate    = $this->translator->trans("Suppression d'un organisateur", [], 'jumpEvent');
                    $logModification = new LogEventJump();
                    $logModification->setLibelle("Suppression d'un organisateur")
                                    ->setValeurAvant($organisateur->getPseudo())
                                    ->setValeurApres(null)
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $jump->removeGestionnaire($organisateur)
                         ->addLogEvent($logModification);
                    
                } else {
                    unset($listNewOrga[$organisateur->getId()]);
                }
            }
            
            foreach ($listNewOrga as $orga) {
                $organisateur = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $orga['id']]);
                
                if ($organisateur !== null) {
                    $libTranslate    = $this->translator->trans("Ajout d'un organisateur", [], 'jumpEvent');
                    $logModification = new LogEventJump();
                    $logModification->setLibelle("Ajout d'un organisateur")
                                    ->setValeurAvant(null)
                                    ->setValeurApres($organisateur->getPseudo())
                                    ->setDeclencheur($this->user)
                                    ->setEventAt(new DateTime('now'));
                    $jump->addGestionnaire($organisateur)
                         ->addLogEvent($logModification);
                }
            }
            
        }
        
        $leadInJump = (json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR))['jump']['lead'];
        
        $listLeadJump = [];
        foreach ($leadInJump as $lead) {
            $listLeadJump[$lead['user']['id']][$lead['type_lead']['id']][$lead['apprenti'] ? 1 : 0] = $lead;
        }
        
        foreach ($jump->getLead() as $lead) {
            if (!isset($listLeadJump[$lead->getUser()->getId()][$lead->getTypeLead()->getId()][$lead->getApprenti() ?
                    1 : 0])) {
                
                $libTranslate    = $this->translator->trans("Suppression d'un lead", [], 'jumpEvent');
                $logModification = new LogEventJump();
                $logModification->setLibelle("Suppression d'un lead")
                                ->setValeurAvant($lead->getUser()->getPseudo())
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setEventAt(new DateTime('now'));
                $jump->removeLead($lead)
                     ->addLogEvent($logModification);
            } else {
                unset($listLeadJump[$lead->getUser()->getId()][$lead->getTypeLead()->getId()][$lead->getApprenti() ? 1 :
                        0]);
            }
        }
        
        foreach ($listLeadJump as $key => $leadUser) {
            $userBdd = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $key]);
            
            if ($userBdd !== null) {
                
                foreach ($leadUser as $keyLad => $leadOrApprenti) {
                    foreach ($leadOrApprenti as $apprenti => $lead) {
                        $leadBdd = $this->entityManager->getRepository(TypeLead::class)
                                                       ->findOneBy(['id' => $lead['type_lead']['id']]);
                        
                        if ($leadBdd !== null) {
                            $newLead = new LeadJump();
                            
                            $libTranslate = $this->translator->trans("Ajout d'un lead", [], 'jumpEvent');
                            $logAjout     = new LogEventJump();
                            $logAjout->setLibelle("Ajout d'un lead")
                                     ->setValeurAvant(null)
                                     ->setValeurApres($userBdd->getPseudo())
                                     ->setDeclencheur($this->user)
                                     ->setEventAt(new DateTime('now'));
                            
                            $newLead->setApprenti($apprenti === 1)
                                    ->setTypeLead($leadBdd)
                                    ->setUser($userBdd);
                            
                            $jump->addLead($newLead)
                                 ->addLogEvent($logAjout);
                        }
                    }
                }
            }
        }
        
        
        try {
            
            if (count($listErreur) > 0) {
                
                $retour['codeRetour'] = 2;
                $retour['libRetour']  =
                    $this->translator->trans("Modification impossible, il y a des erreurs.", [], 'app');
                $retour['zoneRetour'] = ['libErreur' => $listErreur];
            } else {
                $this->entityManager->persist($jump);
                $this->entityManager->flush();
                $this->entityManager->refresh($jump);
                
                $retour['codeRetour'] = 0;
                $retour['libRetour']  = $this->translator->trans("Modification réussie.", [], 'app');
                $retour['zoneRetour'] = ['jump' => $this->serializerService->serializeArray($jump, 'json', ['gestion_jump', 'gestion_jump_spe', 'jump', 'general_res', 'candidature_jump'])];
            }
            
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème modification jump - User {$this->user->getId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        
        return new JsonResponse($retour, 200);
        
    }
    
    #[Route('/gestion_candidature_jump', name: 'gestion_candidature_jump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function gestion_candidature_jump(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new InscriptionJumpRestDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), InscriptionJumpRestDto::class, 'json', $data, ['inscription', 'gestion_candidature', 'jump', 'general_res']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getJumpId() === null || $data->getUserId() === null || $data->getInscription() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$data->getJumpId()} - info fournis manquante - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getJumpId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getJumpId());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Utilisateur non habilité à gérer la candidature";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $inscription = $data->getInscription();
        
        
        // Verification de l'existance de l'inscription en vu de modifier
        $inscriptionBdd =
            $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['id' => $inscription->getId()]);
        if ($inscriptionBdd === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Inscription inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        if ($inscriptionBdd->getBlocNotes() !== $inscription->getBlocNotes()) {
            $libTranslate    = $this->translator->trans("Modification du bloc notes", [], 'jumpEvent');
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification du bloc notes")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(false)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setBlocNotes($inscription->getBlocNotes())
                           ->addLogEventInscription($logModification);
        }
        
        if ($inscriptionBdd->getReponse() !== $inscription->getReponse()) {
            $libTranslate    = $this->translator->trans("Modification de la réponse", [], 'jumpEvent');
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification de la réponse")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setReponse($inscription->getReponse())
                           ->addLogEventInscription($logModification);
        }
        
        if ($inscriptionBdd->getStatut()->getId() !== $inscription->getStatut()->getId()) {
            
            if ($inscription->getStatut()->getId() === StatutInscription::ST_ACC) {
                $countAccepted = $this->entityManager->getRepository(InscriptionJump::class)->countAccepted($jump);
                
                if ($countAccepted >= 40) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  =
                        $this->translator->trans('Il y a trop de candidature au statut "acceptée"', [], 'jumpEvent');
                    return new JsonResponse($retour);
                    
                }
            }
            
            $statut = $this->jumpHandler->recuperationStatut($inscription->getStatut()->getId());
            if ($statut === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           =
                    "$codeErreur - Le staut inconnu - maj  statut inscription -{$this->user->getId()} - {$data->getInscription()->getStatut()->getId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            }
            $libTranslate    =
                $this->translator->trans("Modification du statut d'inscription du jump", [], 'jumpEvent');
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification du statut d'inscription du jump")
                            ->setValeurAvant($inscriptionBdd->getStatut()->getNom())
                            ->setValeurApres($inscription->getStatut()->getNom())
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(4)
                            ->setEventAt(new DateTime('now'));
            
            $inscriptionBdd->setStatut($statut)
                           ->addLogEventInscription($logModification);
            
            if ($statut->getId() !== StatutInscription::ST_ACC) {
                // Recherche si la candidature était déjà en coa
                $coalition = $this->entityManager->getRepository(Coalition::class)->findOneBy(['jump' => $jump,
                                                                                               'user' => $inscriptionBdd->getUser()]);
                
                if ($coalition !== null) {
                    $this->entityManager->remove($coalition);
                }
            }
        }
        
        try {
            
            $this->entityManager->persist($inscriptionBdd);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscriptionBdd);
            
            // conversion du moyen de contact pour le fournir au front correctement
            $inscriptionBdd->setMoyenContact($inscriptionBdd->dechiffreMoyenContact($this->gh));
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Enregistrement effectué !", [], 'jumpEvent');
            $retour['zoneRetour'] = ['inscription' => $this->serializerService->serializeArray($inscriptionBdd, 'json', ['candidature_jump', 'jump', 'general_res'])];
            
        } catch (Exception $e) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème modification inscription jump - User {$this->user->getId()} : " .
                $e->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route('/creation', name: 'creation', methods: ['GET']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function main_creation(Request $request): JsonResponse
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
        
        foreach ($listCreneau as $creneau) {
            $creneau->setLibelle($this->translator->trans($creneau->getLibelle(), [], 'jumpEvent'));
        }
        try {
            if (count($this->user->getDispoTypes()?->toArray() ?? []) === 0) {
                
                $dispoBase = $this->entityManager->getRepository(TypeDispo::class)->findOneBy(['id' => 1]);
                
                foreach ($listCreneau as $creneau) {
                    
                    $this->user->setDispoType(1, $creneau, $dispoBase);
                    $this->user->setDispoType(2, $creneau, $dispoBase);
                    
                }
                
            }
            $userJson = $this->serializerService->serializeArray($this->user, 'json', ['general_res']);
            
        } catch (Exception $exception) {
            $messageLog =
                "Un problème sur la serialisation de l'utilisateur a eu lieu - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        $listTypeVille = $this->entityManager->getRepository(TypeVille::class)->findAll();
        $listObjectif  = $this->entityManager->getRepository(ObjectifVillePrototype::class)->findAll();
        
        foreach ($listObjectif as $objectif) {
            $objectif->setNom($this->translator->trans($objectif->getNom(), [], 'jump'));
        }
        
        foreach ($listTypeVille as $type) {
            $type->setNom($this->translator->trans($type->getNom(), [], 'game'));
            $type->setAbrev($this->translator->trans($type->getAbrev(), [], 'game'));
            
        }
        
        $listTypeVilleJson = $this->serializerService->serializeArray($listTypeVille, 'json', ['jump']);
        $listObjectifJson  = $this->serializerService->serializeArray($listObjectif, 'json', ['jump']);
        $listJob           = $this->entityManager->getRepository(JobPrototype::class)->findAll();
        $listJobJson       = $this->serializerService->serializeArray($listJob, 'json', ['jump']);
        
        $jump = new Jump();
        $jump->setDateDebInscription(new DateTime('now'))
             ->setDateFinInscription(new DateTime('now'))
             ->setDateApproxJump(new DateTime('now'));
        $event = new Event();
        $event->setDebInscriptionDateAt(new DateTime('now'))
              ->setFinInscriptionDateAt(new DateTime('now'))
              ->setEventBeginAt(new DateTime('now'));
        
        $jumpJson  = $this->serializerService->serializeArray($jump, 'json', ['creation_jump']);
        $eventJson = $this->serializerService->serializeArray($event, 'json', ['creation_event']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'creation' => [
                'options' => [
                    'listTypeVille' => $listTypeVilleJson,
                    'listObjectif'  => $listObjectifJson,
                    'listCommu'     => [
                        'de',
                        'es',
                        'en',
                        'fr',
                        'int',
                    ],
                    'listJob'       => $listJobJson,
                ],
                'user'    => $userJson,
                'jump'    => $jumpJson,
                'event'   => $eventJson,
            ],
            'general'  => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route(path: '/jump', name: 'jump_gestion', methods: ['GET'])]
    public function main_gestion(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $listJump = new ArrayCollection($this->user->getGestionJumps()->toArray());
        
        $listJump = $listJump->filter(function (Jump $jump) {
            
            $dateMax = $jump->getDateJumpEffectif() ?? $jump->getDateApproxJump();
            
            $dateCalcul = DateTime::createFromFormat('d/m/Y H:i', $dateMax->format('d/m/Y H:i'));
            
            $dateCalcul->add(new DateInterval('P4M'));
            
            $dateJour = new DateTime('now');
            
            return $dateJour <= $dateCalcul;
        });
        
        $listJumpArch = new ArrayCollection($this->user->getGestionJumps()->toArray());
        
        $listJumpArch = $listJumpArch->filter(function (Jump $jump) {
            
            $dateMax = $jump->getDateJumpEffectif() ?? $jump->getDateApproxJump();
            
            $dateCalcul = DateTime::createFromFormat('d/m/Y H:i', $dateMax->format('d/m/Y H:i'));
            
            $dateCalcul->add(new DateInterval('P4M'));
            
            $dateJour = new DateTime('now');
            
            return $dateJour > $dateCalcul;
        });
        
        
        /**
         * @var Event[] $listEvent
         */
        $listEvent = $this->user->getOrgaEvents();
        
        $listEvent = $listEvent->filter(function (Event $event) {
            
            $dateMax = $event->getEventBeginAt() ?? $event->getDebInscriptionDateAt();
            
            $dateCalcul = DateTime::createFromFormat('d/m/Y H:i', $dateMax->format('d/m/Y H:i'));
            
            $dateCalcul->add(new DateInterval('P2M'));
            
            $dateJour = new DateTime('now');
            
            return $dateJour <= $dateCalcul;
        });
        /**
         * @var Event[] $listEventArch
         */
        $listEventArch = $this->user->getOrgaEvents();
        
        $listEventArch = $listEventArch->filter(function (Event $event) {
            
            $dateMax = $event->getEventBeginAt() ?? $event->getDebInscriptionDateAt();
            
            $dateCalcul = DateTime::createFromFormat('d/m/Y H:i', $dateMax->format('d/m/Y H:i'));
            
            $dateCalcul->add(new DateInterval('P2M'));
            
            $dateJour = new DateTime('now');
            
            return $dateJour > $dateCalcul;
        });
        
        $listJumpJson      = $this->serializerService->serializeArray($listJump->toArray(), 'json', ['list_jump']);
        $listJumpArchJson  = $this->serializerService->serializeArray($listJumpArch->toArray(), 'json', ['list_jump']);
        $listEventJson     = $this->serializerService->serializeArray($listEvent->toArray(), 'json', ['list_event']);
        $listEventArchJson = $this->serializerService->serializeArray($listEventArch->toArray(), 'json', ['list_event']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'list'    => [
                'listJump'      => $listJumpJson,
                'listEvent'     => $listEventJson,
                'listJumpArch'  => $listJumpArchJson,
                'listEventArch' => $listEventArchJson,
            ],
            'general' => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route(path: '/figeComp', name: 'figecomp', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majFigeComp(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new CoalitionGestionDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionGestionDto::class, 'json', $data, ['coalition']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - fige metier - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() !== $this->user->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur ne correspondent pas.- fige metier - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->isOrga($jump)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous n'êtes pas autorisé à modifier ce choix", [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($data->getFige() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Choix incorrect. - Coalition - fige metier - {$this->user->getId()} - {$data->getFige()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($this->jumpHandler->majCompFige($jump, $data->getFige())) {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème dans la sauvegarde des métiers. - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour);
        
    }
    
    #[Route(path: '/figeMetier', name: 'figemetier', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majFigeMetier(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new CoalitionGestionDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), CoalitionGestionDto::class, 'json', $data, ['coalition']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - fige metier - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() !== $this->user->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur ne correspondent pas.- fige metier - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->isOrga($jump)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous n'êtes pas autorisé à modifier ce choix", [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        if ($data->getFige() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Choix incorrect. - Coalition - fige metier - {$this->user->getId()} - {$data->getFige()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($this->jumpHandler->majMetierFige($jump, $data->getFige())) {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème dans la sauvegarde des métiers. - fige metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour);
        
    }
    
    #[Route('/modif_banniere', name: 'modifBanniere', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function modifBanniere(JSONRequestParser $parser): JsonResponse
    {
        $id_banniere = $parser->get('id');
        $upload      = $parser->get_num('up', default: 1);
        $image       = $parser->get_base64('image');
        $mime        = $parser->get('mime', default: 'png');
        $filenameNew = bin2hex(random_bytes(12)) . '.' . $mime;
        
        if ($image === null && $upload == 1) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous n'avez pas fourni d'image.", [], 'jumpEvent') . ' - ' . $codeErreur;
            $messageLog           = "$codeErreur - Image non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        switch ($parser->get('type')) {
            case 'event':
                $event = $this->eventHandler->recuperationEvent($id_banniere);
                if ($event === null) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'outils',
                    );
                    $messageLog           = "$codeErreur - Event inexistant - Joueur : {$this->user?->getId()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                } else {
                    if (!$this->entityManager->getRepository(Event::class)
                                             ->habiliterGestion($id_banniere, $this->user->getId())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $messageLog           =
                            "$codeErreur - Utilisateur non habilité à modifier la banniere de l'event - Joueur : {$this->user?->getId()}";
                        $this->logger->error($messageLog);
                        $this->gh->generateMessageDiscord($messageLog);
                        return new JsonResponse($retour);
                    }
                    
                    $filenameOld = $event->getBanniere();
                    
                    if (file_exists('../public/uploads/banniere/' . $event->getBanniere())) {
                        unlink('../public/uploads/banniere/' . $event->getBanniere());
                    }
                    
                    if ($upload == 1) {
                        
                        $erreur = $this->gestionHandler->gestionBanniere($image, $filenameNew, $mime);
                        
                        if ($erreur !== GestionHandler::NoError) {
                            $codeErreur           = $this->randomString();
                            $retour['codeRetour'] = 1;
                            $retour['libRetour']  = $this->translator->trans(
                                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                                ['{code}' => $codeErreur],
                                'outils',
                            );
                            $messageLog           =
                                "$codeErreur - Problème lors de l'enregistrement de l'image - Joueur : {$this->user?->getId()}";
                            $this->logger->error($messageLog);
                            $this->gh->generateMessageDiscord($messageLog);
                            return new JsonResponse($retour);
                        }
                        
                    } else {
                        
                        $filenameNew = null;
                    }
                    
                    $event->setBanniere($filenameNew);
                    
                    foreach ($event->getListJump() as $jump) {
                        if ($jump->getBanniere() === $filenameOld) {
                            $jump->setBanniere($filenameNew);
                        }
                    }
                    
                    $retourEnreg = $this->gestionHandler->updateBanniere($id_banniere, $filenameNew, $upload, $event);
                    
                    $retour['codeRetour'] = $retourEnreg->getCodeRetour();
                    $retour['libRetour']  = $retourEnreg->getLibRetour();
                    $retour['zoneRetour'] = $retourEnreg->getZoneRetour();
                    
                    return new JsonResponse($retour);
                }
            case 'jump':
                $jump = $this->jumpHandler->recuperationJump($id_banniere);
                $this->logger->error('test ban apres recup jump');
                if ($jump === null) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'outils',
                    );
                    $messageLog           = "$codeErreur - Jump inexistant - Joueur : {$this->user?->getId()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    return new JsonResponse($retour);
                } else {
                    if (!$this->entityManager->getRepository(Jump::class)
                                             ->habiliterGestion($jump, $this->user->getId())) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'outils',
                        );
                        $messageLog           =
                            "$codeErreur - Utilisateur non habilité à modifier la banniere du jump - Joueur : {$this->user?->getId()}";
                        $this->logger->error($messageLog);
                        $this->gh->generateMessageDiscord($messageLog);
                        return new JsonResponse($retour);
                    }
                    if ($jump->getBanniere() !== null &&
                        file_exists('../public/uploads/banniere/' . $jump->getBanniere())) {
                        unlink('../public/uploads/banniere/' . $jump->getBanniere());
                    }
                    
                    if ($upload == 1) {
                        $erreur = $this->gestionHandler->gestionBanniere($image, $filenameNew, $mime);
                        if ($erreur !== GestionHandler::NoError) {
                            $codeErreur           = $this->randomString();
                            $retour['codeRetour'] = 1;
                            $retour['libRetour']  = $this->translator->trans(
                                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                                ['{code}' => $codeErreur],
                                'outils',
                            );
                            $messageLog           =
                                "$codeErreur - Problème lors de l'enregistrement de l'image - Joueur : {$this->user?->getId()}";
                            $this->logger->error($messageLog);
                            $this->gh->generateMessageDiscord($messageLog);
                            return new JsonResponse($retour);
                        }
                        
                    } else {
                        
                        $filenameNew = null;
                    }
                    
                    $jump->setBanniere($filenameNew);
                    
                    $retourEnreg =
                        $this->gestionHandler->updateBanniere($id_banniere, $filenameNew, $upload, jump: $jump);
                    
                    $retour['codeRetour'] = $retourEnreg->getCodeRetour();
                    $retour['libRetour']  = $retourEnreg->getLibRetour();
                    $retour['zoneRetour'] = $retourEnreg->getZoneRetour();
                    
                    return new JsonResponse($retour);
                }
            default:
                
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $messageLog           =
                    "$codeErreur - Acces à une page autre que jump/event - Joueur : {$this->user?->getId()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            
        }
        
    }
    
    #[Route('/supp_event/{id}', name: 'supprimerEvent', methods: ['DELETE']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function supprimerEvent(Event $event): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        if ($event === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Event non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Event::class)
                                 ->habiliterGestion($event->getId(), $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Utilisateur non habilité à supprimer l'event";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        try {
            
            // On récupère le masquage des jumps pour les supprimer en cas de besoin
            $listMasquage = $this->entityManager->getRepository(MasquageEvent::class)->findBy(['event' => $event]);
            
            foreach ($listMasquage as $masquage) {
                $this->entityManager->remove($masquage);
            }
            $this->entityManager->remove($event);
            $this->entityManager->flush();
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Event supprimé !", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
            return new JsonResponse($retour);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Suppression en erreur - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    #[Route('/supp_jump/{id}', name: 'supprimerJump', methods: ['DELETE']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function supprimerJump(Jump $jump): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Jump non fournis - Joueur : {$this->user?->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Utilisateur non habilité à supprimer le jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        if ($jump->getEvent() !== null) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas supprimer un jump lié à un event depuis cette page, merci de passer par la gestion d'event pour supprimer une ville !",
                                         [], 'jumpEvent');
            return new JsonResponse($retour);
        }
        
        try {
            
            // On récupère le masquage des jumps pour les supprimer en cas de besoin
            $listMasquage = $this->entityManager->getRepository(MasquageJump::class)->findBy(['jump' => $jump]);
            
            foreach ($listMasquage as $masquage) {
                $this->entityManager->remove($masquage);
            }
            
            foreach ($jump->getObjectif() as $objectif) {
                $jump->getObjectif()->removeElement($objectif);
            }
            
            foreach ($jump->getCoalitions() as $coalition) {
                foreach ($coalition->getDispos() as $dispo) {
                    $coalition->getDispos()->removeElement($dispo);
                }
                $jump->getCoalitions()->removeElement($coalition);
            }
            
            foreach ($jump->getInscriptionJumps() as $inscription) {
                $jump->getInscriptionJumps()->removeElement($inscription);
            }
            
            $this->entityManager->remove($jump);
            $this->entityManager->flush();
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Jump supprimé !", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
            return new JsonResponse($retour);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Suppression en erreur - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
}