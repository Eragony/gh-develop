<?php

namespace App\Controller\Rest\Jump;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Coalition;
use App\Entity\CreneauHorraire;
use App\Entity\DisponibiliteJoueurs;
use App\Entity\Event;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use App\Entity\InscriptionJump;
use App\Entity\JobPrototype;
use App\Entity\Jump;
use App\Entity\LeadInscription;
use App\Entity\LevelRuinePrototype;
use App\Entity\LogEventInscription;
use App\Entity\MasquageEvent;
use App\Entity\MasquageJump;
use App\Entity\RoleJump;
use App\Entity\StatutInscription;
use App\Entity\TypeDispo;
use App\Entity\TypeLead;
use App\Entity\User;
use App\Exception\AuthenticationException;
use App\Exception\GestHordesException;
use App\Service\Admin\JobHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Jump\EventHandler;
use App\Service\Jump\InscriptionHandler;
use App\Service\Jump\JumpHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Jump\Inscription\InscriptionEventMasqueRest;
use App\Structures\Dto\GH\Jump\Inscription\InscriptionJumpMasqueRest;
use App\Structures\Dto\GH\Jump\Inscription\InscriptionJumpRestDto;
use App\Structures\Dto\GH\Jump\Inscription\MajMetierDefinitifDto;
use App\Structures\Dto\GH\Jump\Inscription\MajStatutInscriptionDto;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/inscription', name: 'rest_inscription_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class InscriptionJumpRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected LocalFormatterUtils    $lfu,
        protected JumpHandler            $jumpHandler,
        protected EventHandler           $eventHandler,
        protected InscriptionHandler     $inscriptionHandler,
        protected JobHandler             $jobHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route(path: '/desist_inscription_jump', name: 'desist_inscription_jump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function desist_inscription_jump(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new InscriptionJumpRestDto();
        
        try {
            
            $this->serializerService->deserialize($request->getContent(), InscriptionJumpRestDto::class, 'json', $data, ['inscription', 'creation_ins', 'jump',
                                                                                                                         'general_res']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getJumpId() === null || $data->getUserId() === null || $data->getInscription() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$data->getJumpId()} - info fournis manquante - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getJumpId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getJumpId());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $inscription = $data->getInscription();
        
        
        // Verification de l'existance de l'inscription en vu de modifier
        $inscriptionBdd =
            $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['id' => $inscription->getId()]);
        if ($inscriptionBdd === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Inscription inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $statutDesist = $this->entityManager->getRepository(StatutInscription::class)->find(99);
        
        $libTranslate    = $this->translator->trans("Désistement du jump", [], 'jumpEvent');
        $logModification = new LogEventInscription();
        $logModification->setLibelle("Désistement du jump")
                        ->setValeurAvant(null)
                        ->setValeurApres(null)
                        ->setDeclencheur($this->user)
                        ->setVisible(true)
                        ->setTypologie(0)
                        ->setEventAt(new DateTime('now'));
        
        $inscriptionBdd->setStatut($statutDesist)
                       ->setDateModification(new DateTime('Now'))
                       ->addLogEventInscription($logModification);
        
        // Recherche si la candidature était déjà en coa
        $coalition = $this->entityManager->getRepository(Coalition::class)->findOneBy(['jump' => $jump,
                                                                                       'user' => $inscription->getUser()]);
        
        if ($coalition !== null) {
            $this->entityManager->remove($coalition);
        }
        
        
        try {
            
            $this->entityManager->persist($inscriptionBdd);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscriptionBdd);
            
            // conversion du moyen de contact pour le fournir au front correctement
            $inscriptionBdd->setMoyenContact($inscriptionBdd->dechiffreMoyenContact($this->gh));
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Désistement effectué !", [], 'jumpEvent');
            $retour['zoneRetour'] = ['inscription' => $this->serializerService->serializeArray($inscriptionBdd, 'json', ['inscription_jump', 'jump', 'general_res'])];
            
        } catch (Exception $e) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème désistement inscription jump - User {$this->user->getId()} : " .
                $e->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route(path: '/jump/{idJump}', name: 'jump_inscription_specifique', requirements: ['idJump' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function inscription(string $idJump, Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $isModeArch   = filter_var($request->query->get("arch") ?? "false", FILTER_VALIDATE_BOOLEAN);
            if ($this->user === null) {
                throw new AuthenticationException("Utilisateur non connecté | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            /**
             * @var User $user
             */
            $user = $this->user;
            
            
            // vérif que le jump est bien existant
            $jump = $this->entityManager->getRepository(Jump::class)->find($idJump);
            if ($jump === null) {
                throw new GestHordesException($this->translator->trans("Le jump sur lequel vous essayez de vous inscrire n'existe pas. Veuillez en choisir un dans la liste ci-dessous :", [], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            $dateActuel = new DateTime("now");
            
            if (!$isModeArch && $jump->getDateDebInscription() >= $dateActuel && $jump->getCreatedBy()->getId() != $this->user->getId()) {
                throw new GestHordesException($this->translator->trans("Le jump sur lequel vous essayez de vous inscrire n'est pas encore ouvert aux inscriptions, veuillez vous rapprocher de l'organisateur - {createurPseudo}", ['{createurPseudo}' => $jump->getCreatedBy()->getPseudo()], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            if (!$isModeArch && $jump->getDateFinInscription() <= $dateActuel) {
                throw new GestHordesException($this->translator->trans("Le jump sur lequel vous essayez de vous inscrire a ses inscriptions cloturées, veuillez vous rapprocher de l'organisateur - {createurPseudo}.", ['{createurPseudo}' => $jump->getCreatedBy()->getPseudo()], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            $inscription = $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['Jump' => $idJump, 'user' => $this->user->getId()]);
            
            $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
            
            foreach ($listCreneau as $creneau) {
                $creneau->setLibelle($this->translator->trans($creneau->getLibelle(), [], 'jump'));
            }
            
            
            if ($inscription === null) {
                $inscription = new InscriptionJump();
                $statut      = $this->entityManager->getRepository(StatutInscription::class)->find(0);
                $inscription->setStatut($statut)
                            ->setUser($this->user)
                            ->setFuseau($user?->getFuseau() ?? 'Europe/Paris')
                            ->setPouvoirFutur($user->getDerPouv());
                
                if ($user->getDispoTypes()->count() !== 0) {
                    
                    foreach ($user->getDispoTypes() as $dispoType) {
                        $dispoJoueur = new DisponibiliteJoueurs();
                        
                        $dispoJoueur->setTypeCreneau($dispoType->getTypeCreneau())
                                    ->setCreneau($dispoType->getCreneau())
                                    ->setDispo($dispoType->getDispo());
                        
                        $inscription->addDispo($dispoJoueur);
                        
                    }
                } else {
                    if (count($user->getDispoTypes()?->toArray() ?? []) === 0) {
                        
                        $listCreneau = $this->entityManager->getRepository(CreneauHorraire::class)->findBy(['typologie' => 1]);
                        
                        $dispoBase = $this->entityManager->getRepository(TypeDispo::class)->findOneBy(['id' => 1]);
                        
                        foreach ($listCreneau as $creneau) {
                            
                            
                            $user->setDispoType(1, $creneau, $dispoBase);
                            $user->setDispoType(2, $creneau, $dispoBase);
                            
                        }
                        
                        foreach ($user->getDispoTypes() as $dispoType) {
                            $dispoJoueur = new DisponibiliteJoueurs();
                            
                            $dispoJoueur->setTypeCreneau($dispoType->getTypeCreneau())
                                        ->setCreneau($dispoType->getCreneau())
                                        ->setDispo($dispoType->getDispo());
                            
                            $inscription->addDispo($dispoJoueur);
                        }
                    }
                }
            } else {
                // conversion du moyen de contact pour le fournir au front correctement
                $inscription->setMoyenContact($inscription->dechiffreMoyenContact($this->gh));
            }
            
            
            $listStatut    = $this->entityManager->getRepository(StatutInscription::class)->statutGestionCandidature();
            $listPouvoir   = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
            $listLvlRuine  = $this->entityManager->getRepository(LevelRuinePrototype::class)->findAll();
            $listSkillType = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
            $listSkillBase = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
            
            $helpRuine = "";
            
            foreach ($listLvlRuine as $lvlRuine) {
                $helpRuine .= "{$this->translator->trans($lvlRuine->getNom(),[],'jump')} : {$this->translator->trans($lvlRuine->getDescription(),[],'jump')} <br/>";
            }
            
            if ($jump->getVillePrive() ?? false) {
                $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
            } else {
                $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
            }
            
            $listDispo = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
            $listRole  = $this->entityManager->getRepository(RoleJump::class)->findAll();
            $listLead  = $this->entityManager->getRepository(TypeLead::class)->findAllOrdreAlpha();
            
            $jumpJson        = $this->serializerService->serializeArray($jump, 'json', ['inscription_jump', 'gestion_jump_spe', 'jump', 'general_res']);
            $inscriptionJson = $this->serializerService->serializeArray($inscription, 'json', ['inscription_jump', 'jump', 'general_res', 'candidature_jump']);
            
            $listLeadJson      = $this->serializerService->serializeArray($listLead, 'json', ['jump']);
            $listStatutJson    = $this->serializerService->serializeArray($listStatut, 'json', ['jump']);
            $listPouvoirJson   = $this->serializerService->serializeArray($listPouvoir, 'json', ['gestion_jump']);
            $listLvlRuineJson  = $this->serializerService->serializeArray($listLvlRuine, 'json', ['jump']);
            $listJobJson       = $this->serializerService->serializeArray($listJob, 'json', ['gestion_jump']);
            $listRoleJson      = $this->serializerService->serializeArray($listRole, 'json', ['jump']);
            $listDispoJson     = $this->serializerService->serializeArray($listDispo, 'json', ['jump']);
            $listSkillJson     = $this->serializerService->serializeArray($listSkillType, 'json', ['option']);
            $listSkillBaseJson = $this->serializerService->serializeArray($listSkillBase, 'json', ['option']);
            
            return new JsonResponse([
                                        'inscription' => [
                                            'jump'        => $jumpJson,
                                            'inscription' => $inscriptionJson,
                                            'options'     => [
                                                'helpRuine'     => $helpRuine,
                                                'listDispo'     => $listDispoJson,
                                                'listJob'       => $listJobJson,
                                                'listLead'      => $listLeadJson,
                                                'listLvlRuine'  => $listLvlRuineJson,
                                                'listPouvoir'   => $listPouvoirJson,
                                                'listRole'      => $listRoleJson,
                                                'listStatut'    => $listStatutJson,
                                                'listSkill'     => $listSkillJson,
                                                'listSkillBase' => $listSkillBaseJson,
                                                'role_lead'     => RoleJump::ROLE_LEAD,
                                                'role_applead'  => RoleJump::ROLE_APLEAD,
                                            ],
                                            'user'        => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
                                        ],
                                        'general'     => $this->generateArrayGeneral($request,
                                                                                     $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, InscriptionJumpRestController::class, __FUNCTION__);
        }
        
    }
    
    #[Route(path: '/event/{idEvent}/{idJump}', name: 'event_inscription_specifique', requirements: ['idJump' => '[a-zA-Z0-9]{24}', 'idEvent' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function inscriptionEvent(string $idJump, string $idEvent, Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $isModeArch   = filter_var($request->query->get("arch") ?? "false", FILTER_VALIDATE_BOOLEAN);
            
            if ($this->user === null) {
                throw new AuthenticationException("Utilisateur non connecté | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            /**
             * @var User $user
             */
            $user = $this->user;
            
            // verif que l'event est bien existant
            $event = $this->entityManager->getRepository(Event::class)->find($idEvent);
            if ($event === null) {
                throw new GestHordesException($this->translator->trans("L'event sur lequel vous essayez de vous inscrire n'existe pas. Veuillez en choisir un dans la liste ci-dessous :", [], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            // vérif que le jump est bien existant
            $jump = $this->entityManager->getRepository(Jump::class)->find($idJump);
            if ($jump === null) {
                throw new GestHordesException($this->translator->trans("La ville de l'event sur lequel vous essayez de vous inscrire n'existe pas. Veuillez en choisir un dans la liste ci-dessous :", [], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            $dateActuel = new DateTime("now");
            
            if (!$isModeArch && $jump->getDateDebInscription() >= $dateActuel && $jump->getCreatedBy()->getId() != $this->user->getId()) {
                throw new GestHordesException($this->translator->trans("Le jump sur lequel vous essayez de vous inscrire n'est pas encore ouvert aux inscriptions, veuillez vous rapprocher de l'organisateur - {pseudoCreateur}", ['{pseudoCreateur}' => $jump->getCreatedBy()->getPseudo()], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            if (!$isModeArch && $jump->getDateFinInscription() <= $dateActuel) {
                throw new GestHordesException($this->translator->trans("Le jump sur lequel vous essayez de vous inscrire a ses inscriptions cloturées, veuillez vous rapprocher de l'organisateur - {pseudoCreateur}.", ['{pseudoCreateur}' => $jump->getCreatedBy()->getPseudo()], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            $inscription = $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['Jump' => $idJump, 'user' => $this->user->getId()]);
            
            
            if ($inscription === null) {
                $inscription = new InscriptionJump();
                $statut      = $this->entityManager->getRepository(StatutInscription::class)->find(0);
                $inscription->setStatut($statut)->setPouvoirFutur($user->getDerPouv())->setUser($this->user);
                
            } else {
                // conversion du moyen de contact pour le fournir au front correctement
                $inscription->setMoyenContact($inscription->dechiffreMoyenContact($this->gh));
                
                foreach ($inscription->getLogEventInscriptions() as $logEventInscription) {
                    $logEventInscription->setLibelle($this->translator->trans($logEventInscription->getLibelle(), [], 'jumpEvent'));
                    if ($logEventInscription->getTypologie() === 4) {
                        $logEventInscription->setValeurAvant($this->translator->trans($logEventInscription->getValeurAvant(), [], 'jump'))
                                            ->setValeurApres($this->translator->trans($logEventInscription->getValeurApres(), [], 'jump'));
                    }
                }
                
            }
            
            
            $listStatut    = $this->entityManager->getRepository(StatutInscription::class)->statutGestionCandidature();
            $listPouvoir   = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
            $listLvlRuine  = $this->entityManager->getRepository(LevelRuinePrototype::class)->findAll();
            $listSkillType = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
            $listSkillBase = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
            
            $helpRuine = "";
            
            foreach ($listLvlRuine as $lvlRuine) {
                $helpRuine .= "{$this->translator->trans($lvlRuine->getNom(),[],'jump')} : {$this->translator->trans($lvlRuine->getDescription(),[],'jump')} <br/>";
            }
            
            if ($jump->getVillePrive() ?? false) {
                $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
            } else {
                $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
            }
            
            $listDispo = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
            $listRole  = $this->entityManager->getRepository(RoleJump::class)->findAll();
            $listLead  = $this->entityManager->getRepository(TypeLead::class)->findAllOrdreAlpha();
            
            
            $jumpJson        = $this->serializerService->serializeArray($jump, 'json', ['inscription_jump', 'gestion_jump_spe', 'jump', 'general_res']);
            $inscriptionJson = $this->serializerService->serializeArray($inscription, 'json', ['inscription_jump', 'jump', 'general_res', 'candidature_jump']);
            
            
            $listLeadJson      = $this->serializerService->serializeArray($listLead, 'json', ['jump']);
            $listStatutJson    = $this->serializerService->serializeArray($listStatut, 'json', ['jump']);
            $listPouvoirJson   = $this->serializerService->serializeArray($listPouvoir, 'json', ['gestion_jump']);
            $listLvlRuineJson  = $this->serializerService->serializeArray($listLvlRuine, 'json', ['jump']);
            $listJobJson       = $this->serializerService->serializeArray($listJob, 'json', ['gestion_jump']);
            $listRoleJson      = $this->serializerService->serializeArray($listRole, 'json', ['jump']);
            $listDispoJson     = $this->serializerService->serializeArray($listDispo, 'json', ['jump']);
            $listSkillJson     = $this->serializerService->serializeArray($listSkillType, 'json', ['option']);
            $listSkillBaseJson = $this->serializerService->serializeArray($listSkillBase, 'json', ['option']);
            
            return new JsonResponse([
                                        'inscription' => [
                                            'jump'        => $jumpJson,
                                            'inscription' => $inscriptionJson,
                                            'options'     => [
                                                'helpRuine'     => $helpRuine,
                                                'listDispo'     => $listDispoJson,
                                                'listJob'       => $listJobJson,
                                                'listLead'      => $listLeadJson,
                                                'listLvlRuine'  => $listLvlRuineJson,
                                                'listPouvoir'   => $listPouvoirJson,
                                                'listRole'      => $listRoleJson,
                                                'listStatut'    => $listStatutJson,
                                                'listSkill'     => $listSkillJson,
                                                'listSkillBase' => $listSkillBaseJson,
                                                'role_lead'     => RoleJump::ROLE_LEAD,
                                                'role_applead'  => RoleJump::ROLE_APLEAD,
                                            ],
                                            'user'        => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
                                        ],
                                        'general'     => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, InscriptionJumpRestController::class, __FUNCTION__);
        }
    }
    
    #[Route(path: '/inscription_jump', name: 'inscriptionJump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function inscription_jump(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new InscriptionJumpRestDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), InscriptionJumpRestDto::class, 'json', $data, ['inscription', 'jump', 'general_res', 'creation_ins']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getJumpId() === null || $data->getUserId() === null || $data->getInscription() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$data->getJumpId()} - info fournis manquante - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getJumpId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getJumpId());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $jumpInEvent = $jump->getEvent() !== null;
        
        $inscription = $data->getInscription();
        $listErreur  = [];
        
        // Controle de la saisie
        if ($inscription->getVoeuxMetier1() === null) {
            $listErreur[] = $this->translator->trans("Le métier ne peut pas être vide", [], 'jumpEvent');
        }
        if ($inscription->getVoeuxMetier1()?->getId() === $inscription->getVoeuxMetier2()?->getId() ||
            $inscription->getVoeuxMetier1()?->getId() === $inscription->getVoeuxMetier3()?->getId() ||
            ($inscription->getVoeuxMetier2() !== null &&
             $inscription->getVoeuxMetier2()?->getId() === $inscription->getVoeuxMetier3()?->getId())
        ) {
            $listErreur[] =
                $this->translator->trans("Vous ne pouvez pas choisir un métier que vous avez déjà choisis", [],
                                         'jumpEvent');
        }
        if ($inscription->getLvlRuine() === null) {
            $listErreur[] = $this->translator->trans("Le level de la ruine ne peut pas être vide", [], 'jumpEvent');
        }
        if ($inscription->getPouvoirFutur() === null) {
            $listErreur[] = $this->translator->trans("Le pouvoir héros ne peut pas être vide", [], 'jumpEvent');
        }
        
        // gestion des dispos uniquement dans un jump sans event
        if (!$jumpInEvent) {
            
            if ($inscription->getFuseau() === null) {
                $listErreur[] = $this->translator->trans("Le fuseau horaire ne peut pas être vide.", [], 'jumpEvent');
            }
            
            // récupération dispo
            if (count($inscription->getDispo()) !== 16) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $messageLog           =
                    "$codeErreur - Nombre de dispo <> 16 - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                
                return new JsonResponse($retour);
            }
            
            $dispo_semaine = 0;
            $dispo_week    = 0;
            
            foreach ($inscription->getDispo() as $dispo) {
                if ($dispo->getTypeCreneau() === 1 && $dispo->getDispo()->getId() === 1) {
                    $dispo_semaine++;
                }
                if ($dispo->getTypeCreneau() === 2 && $dispo->getDispo()->getId() === 1) {
                    $dispo_week++;
                }
            }
            
            if ($dispo_semaine === 8) {
                $listErreur[] =
                    $this->translator->trans("Vous ne pouvez pas être 'Pas co' tous le temps en semaine", [],
                                             'jumpEvent');
            }
            if ($dispo_week === 8) {
                $listErreur[] =
                    $this->translator->trans("Vous ne pouvez pas être 'Pas co' tous le temps le week-end", [],
                                             'jumpEvent');
            }
            
        }
        
        $roles = $inscription->getRoleJoueurJumps();
        
        if (count($roles) === 8) {
            $listErreur[] = $this->translator->trans("Vous devez choisir un rôle", [], 'jumpEvent');
        }
        
        $choix_lead     = false;
        $choix_app_lead = false;
        
        foreach ($roles as $role) {
            if ($role->getId() === RoleJump::ROLE_LEAD) {
                $choix_lead = true;
            }
            if ($role->getId() === RoleJump::ROLE_APLEAD) {
                $choix_app_lead = true;
            }
        }
        
        if ($choix_lead) {
            $presence_lead = false;
            foreach ($inscription->getLeadInscriptions() as $lead) {
                if (!$lead->getApprenti()) {
                    $presence_lead = true;
                }
            }
            
            if (!$presence_lead) {
                $listErreur[] =
                    $this->translator->trans("Vous avez choisis d'être lead, mais la spécialité est vide", [],
                                             'jumpEvent');
            }
        }
        
        if ($choix_app_lead) {
            $presence_lead = false;
            foreach ($inscription->getLeadInscriptions() as $lead) {
                if ($lead->getApprenti()) {
                    $presence_lead = true;
                }
            }
            
            if (!$presence_lead) {
                $listErreur[] =
                    $this->translator->trans("Vous avez choisis d'être apprenti lead, mais la spécialité est vide", [],
                                             'jumpEvent');
            }
        }
        
        if (count($listErreur) > 0) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  = $this->translator->trans("Modification impossible, il y a des erreurs.", [], 'app');
            $retour['zoneRetour'] = ['libErreur' => $listErreur];
            return new JsonResponse($retour, 200);
        }
        
        // Balayage pour récupérer les entités correctement dans la base de donnée
        
        $listCreneauSemaine = $jump->getCreneauSemaine();
        $listCreneauWeekEnd = $jump->getCreneauWeekend();
        $listDispo          = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
        $listLevelRuine     = $this->entityManager->getRepository(LevelRuinePrototype::class)->findAll();
        $listRole           = $this->entityManager->getRepository(RoleJump::class)->findAll();
        $listLead           = $this->entityManager->getRepository(TypeLead::class)->findAllOrdreAlpha();
        $listPouvoir        = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
        
        if ($jump->getVillePrive() ?? false) {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
        } else {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
        }
        
        $listJobIndex        = [];
        $listCreneauIndexS   = [];
        $listCreneauIndexW   = [];
        $listDispoIndex      = [];
        $listRoleIndex       = [];
        $listLeadIndex       = [];
        $listLevelRuineIndex = [];
        $listPouvoirIndex    = [];
        
        foreach ($listJob as $job) {
            $listJobIndex[$job->getId()] = $job;
        }
        foreach ($listLevelRuine as $ruine) {
            $listLevelRuineIndex[$ruine->getId()] = $ruine;
        }
        
        foreach ($listCreneauSemaine as $creneau) {
            $listCreneauIndexS[$creneau->getId()] = $creneau;
        }
        foreach ($listCreneauWeekEnd as $creneau) {
            $listCreneauIndexW[$creneau->getId()] = $creneau;
        }
        
        foreach ($listDispo as $dispo) {
            $listDispoIndex[$dispo->getId()] = $dispo;
        }
        
        foreach ($listRole as $role) {
            $listRoleIndex[$role->getId()] = $role;
        }
        
        foreach ($listLead as $lead) {
            $listLeadIndex[$lead->getId()] = $lead;
        }
        
        foreach ($listPouvoir as $pouvoir) {
            $listPouvoirIndex[$pouvoir->getId()] = $pouvoir;
        }
        
        // création de l'objet à mettre en base de donnée
        $inscriptionNew = new InscriptionJump();
        $inscriptionNew->setJump($jump);
        
        
        $inscriptionNew->setLvlRuine($listLevelRuineIndex[$inscription->getLvlRuine()->getId()] ??
                                     $listLevelRuineIndex[0]);
        
        if (isset($listJobIndex[$inscription->getVoeuxMetier1()->getId()])) {
            $inscriptionNew->setVoeuxMetier1($listJobIndex[$inscription->getVoeuxMetier1()->getId()]);
        }
        
        if ($inscription->getVoeuxMetier2() !== null &&
            isset($listJobIndex[$inscription->getVoeuxMetier2()->getId()])) {
            $inscriptionNew->setVoeuxMetier2($listJobIndex[$inscription->getVoeuxMetier2()->getId()]);
        }
        
        if ($inscription->getVoeuxMetier3() !== null &&
            isset($listJobIndex[$inscription->getVoeuxMetier3()->getId()])) {
            $inscriptionNew->setVoeuxMetier3($listJobIndex[$inscription->getVoeuxMetier3()->getId()]);
        }
        
        if (!$jumpInEvent) {
            foreach ($inscription->getDispo() as $dispoJoueur) {
                if ($dispoJoueur->getTypeCreneau() === 1) {
                    $creneau = $listCreneauIndexS[$dispoJoueur->getCreneau()->getId()] ?? $listCreneauIndexS[0];
                } else {
                    $creneau = $listCreneauIndexW[$dispoJoueur->getCreneau()->getId()] ?? $listCreneauIndexS[0];
                }
                
                $dispoJoueurNew = new DisponibiliteJoueurs();
                
                $dispoJoueurNew->setDispo($listDispoIndex[$dispoJoueur->getDispo()->getId()] ?? $listDispoIndex[1])
                               ->setCreneau($creneau)
                               ->setTypeCreneau($dispoJoueur->getTypeCreneau());
                
                $inscriptionNew->addDispo($dispoJoueurNew);
            }
        }
        
        foreach ($inscription->getRoleJoueurJumps() as $roleJoueurJump) {
            $inscriptionNew->addRoleJoueurJump($listRoleIndex[$roleJoueurJump->getId()] ?? $listRoleIndex[0]);
        }
        
        foreach ($inscription->getLeadInscriptions() as $leadInscription) {
            $leadInscriptionNew = new LeadInscription();
            $leadInscriptionNew->setApprenti($leadInscription->getApprenti())
                               ->setLead($listLeadIndex[$leadInscription->getLead()->getId()] ?? $listLeadIndex[0]);
            
            $inscriptionNew->addLeadInscription($leadInscriptionNew);
        }
        
        // chiffrement des moyens de contact
        $moyenContactSansSaut =
            preg_replace("# {2,}#", " ", preg_replace("#(\r\n|\n\r|\n|\r)#", " ", $inscription->getMoyenContact()));
        
        openssl_public_encrypt($moyenContactSansSaut, $moyenContact, $this->gh->getPublicKey());
        
        $inscriptionNew->setMoyenContact(base64_encode((string)$moyenContact));
        
        
        if ($jump->getCreatedBy()->getId() === $this->user->getId()) {
            $statutInscrit = $this->entityManager->getRepository(StatutInscription::class)->find(20);
        } else {
            $statutInscrit = $this->entityManager->getRepository(StatutInscription::class)->find(1);
        }
        
        
        $libTranslate = $this->translator->trans("Création de l'inscription", [], 'jumpEvent');
        $logCreation  = new LogEventInscription();
        $logCreation->setLibelle("Création de l'inscription")
                    ->setValeurAvant(null)
                    ->setValeurApres(null)
                    ->setDeclencheur($this->user)
                    ->setVisible(true)
                    ->setEventAt(new DateTime('now'));
        
        $inscriptionNew->setStatut($statutInscrit)
                       ->setUser($this->user)
                       ->setBanCommu($inscription->getBanCommu())
                       ->setFuseau($inscription->getFuseau() ?? 'Europe/Paris')
                       ->setGouleCommu($inscription->getGouleCommu())
                       ->setDateInscription(new DateTime('now'))
                       ->setTypeDiffusion($inscription->getTypeDiffusion())
                       ->setMotivations($inscription->getMotivations())
                       ->setCommentaires($inscription->getCommentaires())
                       ->setPouvoirFutur($listPouvoirIndex[$inscription->getPouvoirFutur()->getId()] ??
                                         $listPouvoirIndex[5])
                       ->addLogEventInscription($logCreation);
        
        try {
            $this->entityManager->persist($inscriptionNew);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscriptionNew);
            
            // conversion du moyen de contact pour le fournir au front correctement
            $inscriptionNew->setMoyenContact($inscriptionNew->dechiffreMoyenContact($this->gh));
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Inscription réussi !", [], 'jumpEvent');
            $retour['zoneRetour'] = ['inscription' => $this->serializerService->serializeArray($inscriptionNew, 'json', ['inscription_jump', 'jump', 'general_res'])];
            
        } catch (Exception $e) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème inscription jump - User {$this->user->getId()} : " . $e->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route(path: '/event/{idEvent}', name: 'event_list_jump', requirements: ['idEvent' => '[a-zA-Z0-9]{24}'], methods: ['GET'])]
    public function listJumpOnEvent(string $idEvent, Request $request): Response
    {
        try {
            
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $isModeArch   = filter_var($request->query->get("arch") ?? "false", FILTER_VALIDATE_BOOLEAN);
            
            if ($this->user === null) {
                throw new AuthenticationException("Utilisateur non connecté | fichier " . __FILE__ . " | ligne " . __LINE__);
            }
            
            
            $event = $this->entityManager->getRepository(Event::class)->findOneBy(['id' => $idEvent]);
            
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAll();
            foreach ($listJob as $job) {
                $job->setNom($this->translator->trans($job->getNom(), [], 'game'));
            }
            $listStatut = $this->entityManager->getRepository(StatutInscription::class)->findAll();
            foreach ($listStatut as $statut) {
                $statut->setNom($this->translator->trans($statut->getNom(), [], 'jump'));
            }
            
            if ($event === null) {
                throw new GestHordesException($this->translator->trans("L'event sur lequel vous essayez de vous inscrire n'existe pas. Veuillez en choisir un dans la liste ci-dessous :", [], 'jumpEvent'), GestHordesException::SHOW_MESSAGE_REDIRECT);
            }
            
            $eventJson = $this->serializerService->serializeArray($event, 'json', ['event_inscription', 'general_res', 'avatar']);
            $dateJson  = $this->serializerService->serializeArray([new DateTime('now')], 'json', []);
            
            $listInscription = [];
            foreach ($this->user->getInscriptionJumps() as $inscriptionJump) {
                $listInscription[] = $inscriptionJump->getJump()->getId();
            }
            
            return new JsonResponse([
                                        'event'   => [
                                            'date_jour'       => $dateJson,
                                            'event'           => $eventJson,
                                            'listInscription' => $listInscription,
                                        ],
                                        'general' => $this->generateArrayGeneral($request,
                                                                                 $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, InscriptionJumpRestController::class, __FUNCTION__);
        }
        
        
    }
    
    #[Route(path: '/', name: 'jump_inscription', methods: ['GET'])]
    public function main(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            if ($this->user === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $messageLog           = "$codeErreur - Utilisateur non connecté";
                $this->logger->error($messageLog);
                // On redirige vers la page de connexion
                return $this->redirectToRoute('myhordes_loginMH');
            }
            
            
            $jumps = $this->entityManager->getRepository(Jump::class)->jumpActif(new DateTime("now"));
            
            $events = $this->entityManager->getRepository(Event::class)->eventInscriptionOuvert();
            
            $jumpArchive  = $this->entityManager->getRepository(Jump::class)->jumpInscriptionUserArchive($this->user, new DateTime("now"));
            $eventArchive = $this->entityManager->getRepository(Event::class)->eventUserArchve();
            
            $arrayTmpInscription = $this->user->getInscriptionJumps()?->toArray() ?? [];
            
            /**
             * @var InscriptionJump[] $listJumpInscrit
             */
            $listJumpInscrit =
                array_combine(
                    array_map(fn(InscriptionJump $inscription) => $inscription->getJump()->getId(), $arrayTmpInscription),
                    $arrayTmpInscription,
                );
            
            $jumpMasque  = $this->entityManager->getRepository(MasquageJump::class)->eventMasqueUser($this->user);
            $eventMasque = $this->entityManager->getRepository(MasquageEvent::class)->eventMasqueUser($this->user);
            
            
            $tabMasqueJump = [];
            foreach ($jumpMasque as $jump) {
                $tabMasqueJump[] = $jump->getJump()->getId();
            }
            $tabMasqueEvent = [];
            foreach ($eventMasque as $event) {
                $tabMasqueEvent[] = $event->getEvent()->getId();
            }
            
            $retour['codeRetour'] = 0;
            $retour['zoneRetour'] = [
                'listJumpEvent' => [
                    'listJump'        => $this->serializerService->serializeArray($jumps, 'json', ['list_jump', 'jump', 'general_res']),
                    'listJumpArch'    => $this->serializerService->serializeArray($jumpArchive, 'json', ['list_jump', 'jump', 'general_res']),
                    'listEvent'       => $this->serializerService->serializeArray($events, 'json', ['list_event', 'event', 'general_res']),
                    'listInscription' => $this->serializerService->serializeArray($listJumpInscrit, 'json', ['list_jump']),
                    'listEventArch'   => $this->serializerService->serializeArray($eventArchive, 'json', ['list_event', 'event', 'general_res']),
                    'masqueJump'      => $tabMasqueJump,
                    'masqueEvent'     => $tabMasqueEvent,
                    'user'            => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
                ],
                'general'       => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
        } catch (Exception $e) {
            return $this->errorHandler->handleException($e, 'Liste des jumps', "main");
        }
    }
    
    #[Route(path: '/majMasquageEvent', name: 'majMasquageEvent', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majMasquageEvent(Request $request): JsonResponse
    {
        
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new InscriptionEventMasqueRest();
        
        $this->serializerService->deserialize($request->getContent(), InscriptionEventMasqueRest::class, 'json', $data, ['inscription']);
        
        // Vérification si les champs sont non null
        if ($data->getIdEvent() === null || $data->getUserId() === null || $data->getSens() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - La data fournis est incomplète. - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getIdEvent()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getIdEvent()} - {$data->getUserId()}- {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $event = $this->eventHandler->recuperationEvent($data->getIdEvent());
        if ($event === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Event inconnu - {$data->getIdEvent()}- {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $masquageEvent = $this->entityManager->getRepository(MasquageEvent::class)->findOneBy(['user'  => $this->user,
                                                                                               'event' => $event]);
        
        
        try {
            if ($data->getSens()) {
                
                if ($masquageEvent === null) {
                    $masquageEvent = new MasquageEvent();
                    $masquageEvent->setUser($this->user)
                                  ->setEvent($event);
                    $this->entityManager->persist($masquageEvent);
                    
                }
            } else {
                if ($masquageEvent !== null) {
                    $this->entityManager->remove($masquageEvent);
                }
            }
            
            $this->entityManager->flush();
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
            
            return new JsonResponse($retour);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Problème mise à jour masquage sur l'event : {$event->getId()} " .
                                    $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    #[Route(path: '/majMasquageJump', name: 'majMasquageJump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majMasquageJump(Request $request): JsonResponse
    {
        
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        
        $data = new InscriptionJumpMasqueRest();
        
        $this->serializerService->deserialize($request->getContent(), InscriptionJumpMasqueRest::class, 'json', $data, ['inscription']);
        
        // Vérification si les champs sont non null
        if ($data->getIdJump() === null || $data->getUserId() === null || $data->getSens() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - La data fournis est incomplète - masquage jump - {$this->user->getId()}.";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - L'identifiant fournis est incorrect - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Jump inconnu - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $masquageJump = $this->entityManager->getRepository(MasquageJump::class)->findOneBy(['user' => $this->user,
                                                                                             'jump' => $jump]);
        
        
        try {
            if ($data->getSens()) {
                
                if ($masquageJump === null) {
                    $masquageJump = new MasquageJump();
                    $masquageJump->setUser($this->user)
                                 ->setJump($jump);
                    $this->entityManager->persist($masquageJump);
                    
                }
            } else {
                if ($masquageJump !== null) {
                    $this->entityManager->remove($masquageJump);
                }
            }
            
            $this->entityManager->flush();
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
            
            return new JsonResponse($retour);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Problème mise à jour masquage sur le jump : {$jump->getId()} " .
                                    $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
    }
    
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    #[Route(path: '/majMetier', name: 'metier_def', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majMetierInscription(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new MajMetierDefinitifDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), MajMetierDefinitifDto::class, 'json', $data, ['coalition']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct coalition - metier definitif - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect. - maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() !== $this->user->getId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur ne correspondent pas.- maj metier - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserIdMaj() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur à maj est incorrect.- maj metier - {$this->user->getId()} - {$data->getUserIdMaj()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération de l'utilisateur
        $user = $this->userHandler->recuperationUser($data->getUserIdMaj());
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur inconnu. - Coalition - maj metier - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->inscriptionHandler->controleExistanceInscriptionAcc($jump, $user)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Il n'existe pas d'inscription accepté pour ce joueur. - maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJob() === null || !$this->jobHandler->controleExistanceJob($data->getIdJob())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le métier fournis est incorrect. - maj metier -{$this->user->getId()} - {$data->getIdJob()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($this->inscriptionHandler->miseAJourMetierDef($jump, $user, $data->getIdJob())) {
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
            $retour['zoneRetour'] = [];
        } else {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème dans la sauvegarde des métiers. - maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour);
        
    }
    
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    #[Route(path: '/majStatut', name: 'maj_statut', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function majStatutInscription(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new MajStatutInscriptionDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), MajStatutInscriptionDto::class, 'json', $data, ['jump']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct gestion - maj statut - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdJump() === null || strlen($data->getIdJump()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - maj statut inscription - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - L'identifiant de l'utilisateur est incorrect.- maj statut inscription - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération de l'utilisateur
        $user = $this->userHandler->recuperationUser($data->getUserId());
        if ($user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Utilisateur inconnu. - Gestion - maj statut inscription - {$this->user->getId()} - {$data->getUserId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getIdJump());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu. - Coalition - maj metier - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if (!$this->entityManager->getRepository(Jump::class)->habiliterGestion($jump, $this->user->getId())) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur non habilité à modifier - maj statut inscription - {$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getIdStatut() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le staut fournis est incorrect - maj  statut inscription -{$this->user->getId()} - {$data->getIdJump()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $statut = $this->jumpHandler->recuperationStatut($data->getIdStatut());
        if ($statut === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le staut inconnu - maj  statut inscription -{$this->user->getId()} - {$data->getIdStatut()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // la candidature existe
        $candidature = $jump->getInscriptionJump($data->getUserId());
        if ($candidature === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Le staut inconnu - maj  statut inscription -{$this->user->getId()} - {$data->getIdStatut()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        try {
            if ($this->jumpHandler->majStatut($candidature, $statut, $this->user)) {
                $retour['codeRetour'] = 0;
                $retour['libRetour']  = $this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent');
                $retour['zoneRetour'] = [];
            } else {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           =
                    "$codeErreur - Problème dans la sauvegarde des métiers. - maj statut - {$this->user->getId()} - {$data->getIdJump()}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
            }
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Problème grave dans la sauvegarde des métiers. - maj statut - {$this->user->getId()} - {$data->getIdJump()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        
        return new JsonResponse($retour);
        
    }
    
    #[Route(path: '/mod_inscription_jump', name: 'mod_inscription_jump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function mod_inscription_jump(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new InscriptionJumpRestDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), InscriptionJumpRestDto::class, 'json', $data, ['inscription', 'creation_ins', 'jump', 'general_res']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getJumpId() === null || $data->getUserId() === null || $data->getInscription() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$data->getJumpId()} - info fournis manquante - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getJumpId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getJumpId());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $jumpInEvent = $jump->getEvent() !== null;
        
        $inscription = $data->getInscription();
        
        
        // Verification de l'existance de l'inscription en vu de modifier
        $inscriptionBdd =
            $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['id' => $inscription->getId()]);
        if ($inscriptionBdd === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Inscription inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $listErreur = [];
        
        // Si le statut de l'inscription est < 10, il est possible de modifier tous les champs, sinon, il n'est possible de modifier que le moyen de contact, commentaire, les roles, leads, et type de diffusion
        
        if ($inscriptionBdd->getStatut()->getId() < 10) {
            // Controle de la saisie
            if ($inscription->getVoeuxMetier1() === null) {
                $listErreur[] = $this->translator->trans("Le métier ne peut pas être vide", [], 'jumpEvent');
            }
            if ($inscription->getVoeuxMetier1()?->getId() === $inscription->getVoeuxMetier2()?->getId() ||
                $inscription->getVoeuxMetier1()?->getId() === $inscription->getVoeuxMetier3()?->getId() ||
                ($inscription->getVoeuxMetier2() !== null &&
                 $inscription->getVoeuxMetier2()?->getId() === $inscription->getVoeuxMetier3()?->getId())
            ) {
                $listErreur[] =
                    $this->translator->trans("Vous ne pouvez pas choisir un métier que vous avez déjà choisis", [],
                                             'jumpEvent');
            }
            if ($inscription->getLvlRuine() === null) {
                $listErreur[] = $this->translator->trans("Le level de la ruine ne peut pas être vide", [], 'jumpEvent');
            }
            if ($inscription->getPouvoirFutur() === null) {
                $listErreur[] = $this->translator->trans("Le pouvoir héros ne peut pas être vide", [], 'jumpEvent');
            }
            
            
            if (!$jumpInEvent) {
                
                if ($inscription->getFuseau() === null) {
                    $listErreur[] =
                        $this->translator->trans("Le fuseau horaire ne peut pas être vide", [], 'jumpEvent');
                }
                
                // récupération dispo
                if (count($inscription->getDispo()) !== 16) {
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'outils',
                    );
                    $messageLog           =
                        "$codeErreur - Nombre de dispo <> 16 - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
                    $this->logger->error($messageLog);
                    $this->gh->generateMessageDiscord($messageLog);
                    
                    return new JsonResponse($retour);
                }
                
                $dispo_semaine = 0;
                $dispo_week    = 0;
                
                foreach ($inscription->getDispo() as $dispo) {
                    if ($dispo->getTypeCreneau() === 1 && $dispo->getDispo()->getId() === 1) {
                        $dispo_semaine++;
                    }
                    if ($dispo->getTypeCreneau() === 2 && $dispo->getDispo()->getId() === 1) {
                        $dispo_week++;
                    }
                }
                
                if ($dispo_semaine === 8) {
                    $listErreur[] =
                        $this->translator->trans("Vous ne pouvez pas être 'Pas co' tous le temps en semaine", [],
                                                 'jumpEvent');
                }
                if ($dispo_week === 8) {
                    $listErreur[] =
                        $this->translator->trans("Vous ne pouvez pas être 'Pas co' tous le temps le week-end", [],
                                                 'jumpEvent');
                }
            }
            
            
        }
        
        
        $roles = $inscription->getRoleJoueurJumps();
        
        if (count($roles) === 8) {
            $listErreur[] = $this->translator->trans("Vous devez choisir un rôle", [], 'jumpEvent');
        }
        
        $choix_lead     = false;
        $choix_app_lead = false;
        
        foreach ($roles as $role) {
            if ($role->getId() === RoleJump::ROLE_LEAD) {
                $choix_lead = true;
            }
            if ($role->getId() === RoleJump::ROLE_APLEAD) {
                $choix_app_lead = true;
            }
        }
        
        if ($choix_lead) {
            $presence_lead = false;
            foreach ($inscription->getLeadInscriptions() as $lead) {
                if (!$lead->getApprenti()) {
                    $presence_lead = true;
                }
            }
            
            if (!$presence_lead) {
                $listErreur[] =
                    $this->translator->trans("Vous avez choisis d'être lead, mais la spécialité est vide", [],
                                             'jumpEvent');
            }
        }
        
        if ($choix_app_lead) {
            $presence_lead = false;
            foreach ($inscription->getLeadInscriptions() as $lead) {
                if ($lead->getApprenti()) {
                    $presence_lead = true;
                }
            }
            
            if (!$presence_lead) {
                $listErreur[] =
                    $this->translator->trans("Vous avez choisis d'être apprenti lead, mais la spécialité est vide", [],
                                             'jumpEvent');
            }
        }
        
        if (count($listErreur) > 0) {
            $retour['codeRetour'] = 2;
            $retour['libRetour']  = $this->translator->trans("Modification impossible, il y a des erreurs.", [], 'app');
            $retour['zoneRetour'] = ['libErreur' => $listErreur];
            return new JsonResponse($retour, 200);
        }
        
        // Balayage pour récupérer les entités correctement dans la base de donnée
        
        $listCreneauSemaine = $jump->getCreneauSemaine();
        $listCreneauWeekEnd = $jump->getCreneauWeekend();
        $listDispo          = $this->entityManager->getRepository(TypeDispo::class)->findBy(['typologie' => 1]);
        $listLevelRuine     = $this->entityManager->getRepository(LevelRuinePrototype::class)->findAll();
        $listRole           = $this->entityManager->getRepository(RoleJump::class)->findAll();
        $listLead           = $this->entityManager->getRepository(TypeLead::class)->findAllOrdreAlpha();
        $listPouvoir        = $this->entityManager->getRepository(HerosPrototype::class)->experienceHeros();
        $listSkillType      = $this->entityManager->getRepository(HerosSkillType::class)->getHerosSkillWithoutBase();
        $listSkillBase      = $this->entityManager->getRepository(HerosSkillLevel::class)->findBy(['lvl' => 2]);
        
        if ($jump->getVillePrive() ?? false) {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllOrdo();
        } else {
            $listJob = $this->entityManager->getRepository(JobPrototype::class)->findAllExceptChaman();
        }
        
        $listJobIndex        = [];
        $listCreneauIndexS   = [];
        $listCreneauIndexW   = [];
        $listDispoIndex      = [];
        $listRoleIndex       = [];
        $listLeadIndex       = [];
        $listLevelRuineIndex = [];
        $listPouvoirIndex    = [];
        
        foreach ($listJob as $job) {
            $listJobIndex[$job->getId()] = $job;
        }
        foreach ($listLevelRuine as $ruine) {
            $listLevelRuineIndex[$ruine->getId()] = $ruine;
        }
        
        foreach ($listCreneauSemaine as $creneau) {
            $listCreneauIndexS[$creneau->getId()] = $creneau;
        }
        foreach ($listCreneauWeekEnd as $creneau) {
            $listCreneauIndexW[$creneau->getId()] = $creneau;
        }
        
        foreach ($listDispo as $dispo) {
            $listDispoIndex[$dispo->getId()] = $dispo;
        }
        
        foreach ($listRole as $role) {
            $listRoleIndex[$role->getId()] = $role;
        }
        
        foreach ($listLead as $lead) {
            $listLeadIndex[$lead->getId()] = $lead;
        }
        
        foreach ($listPouvoir as $pouvoir) {
            $listPouvoirIndex[$pouvoir->getId()] = $pouvoir;
        }
        
        if ($inscriptionBdd->getStatut()->getId() < 10) {
            
            // Verification modification
            if ($inscriptionBdd->getBanCommu() !== $inscription->getBanCommu()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification de banni communautaire")
                                ->setValeurAvant($inscriptionBdd->getBanCommu())
                                ->setValeurApres($inscription->getBanCommu())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setBanCommu($inscription->getBanCommu())
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getGouleCommu() !== $inscription->getGouleCommu()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification de goule communautaire")
                                ->setValeurAvant($inscriptionBdd->getGouleCommu())
                                ->setValeurApres($inscription->getGouleCommu())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setGouleCommu($inscription->getGouleCommu())
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getVoeuxMetier1()->getId() !== $inscription->getVoeuxMetier1()->getId()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification du voeux metier 1")
                                ->setValeurAvant($inscriptionBdd->getVoeuxMetier1()->getId())
                                ->setValeurApres($inscription->getVoeuxMetier1()->getId())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(1)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setVoeuxMetier1($listJobIndex[$inscription->getVoeuxMetier1()->getId()] ??
                                                 $listJobIndex[1])
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getVoeuxMetier2()?->getId() !== $inscription->getVoeuxMetier2()?->getId()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification du voeux metier 2")
                                ->setValeurAvant($inscriptionBdd->getVoeuxMetier2()?->getId())
                                ->setValeurApres($inscription->getVoeuxMetier2()?->getId())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(1)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setVoeuxMetier2(($inscription->getVoeuxMetier2() !== null) ?
                                                     ($listJobIndex[$inscription->getVoeuxMetier2()->getId()] ??
                                                      $listJobIndex[1]) : null)
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getVoeuxMetier3()?->getId() !== $inscription->getVoeuxMetier3()?->getId()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification du voeux metier 3")
                                ->setValeurAvant($inscriptionBdd->getVoeuxMetier3()?->getId())
                                ->setValeurApres($inscription->getVoeuxMetier3()?->getId())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(1)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setVoeuxMetier3(($inscription->getVoeuxMetier3() !== null) ?
                                                     ($listJobIndex[$inscription->getVoeuxMetier3()->getId()] ??
                                                      $listJobIndex[1]) : null)
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getLvlRuine()->getId() !== $inscription->getLvlRuine()->getId()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification du niveaux de ruine")
                                ->setValeurAvant($inscriptionBdd->getLvlRuine()->getId())
                                ->setValeurApres($inscription->getLvlRuine()->getId())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(2)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setLvlRuine($listLevelRuineIndex[$inscription->getLvlRuine()->getId()] ??
                                             $listLevelRuineIndex[0])
                               ->addLogEventInscription($logModification);
            }
            if ($inscriptionBdd->getPouvoirFutur()->getId() !== $inscription->getPouvoirFutur()->getId()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification du pouvoir héros")
                                ->setValeurAvant($inscriptionBdd->getPouvoirFutur()->getId())
                                ->setValeurApres($inscription->getPouvoirFutur()->getId())
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(3)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setPouvoirFutur($listPouvoirIndex[$inscription->getPouvoirFutur()->getId()] ??
                                                 $listPouvoirIndex[5])
                               ->addLogEventInscription($logModification);
            }
            
            if ($inscriptionBdd->getMotivations() !== $inscription->getMotivations()) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Modification des motivations")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(0)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->setMotivations($inscription->getMotivations())
                               ->addLogEventInscription($logModification);
            }
            
            if (!$jumpInEvent) {
                
                // Sauvegarder des organisateurs
                $dispoInJump = $inscription->getDispo();
                
                /**
                 * @var DisponibiliteJoueurs[][] $listNewDispo
                 */
                $listNewDispo = [];
                foreach ($dispoInJump as $dispo) {
                    $listNewDispo[$dispo->getTypeCreneau()][$dispo->getCreneau()->getId()] = $dispo;
                }
                
                foreach ($inscriptionBdd->getDispo() as $dispo) {
                    if (!isset($listNewDispo[$dispo->getTypeCreneau()][$dispo->getCreneau()->getId()])) {
                        $logModification = new LogEventInscription();
                        $logModification->setLibelle("Suppression d'une dispo")
                                        ->setValeurAvant(null)
                                        ->setValeurApres(null)
                                        ->setDeclencheur($this->user)
                                        ->setVisible(true)
                                        ->setTypologie(0)
                                        ->setEventAt(new DateTime('now'));
                        $inscriptionBdd->removeDispo($dispo)
                                       ->addLogEventInscription($logModification);
                        
                    } else {
                        unset($listNewDispo[$dispo->getTypeCreneau()][$dispo->getCreneau()->getId()]);
                    }
                }
                
                foreach ($listNewDispo as $typedispo) {
                    foreach ($typedispo as $dispo) {
                        if ($dispo->getTypeCreneau() === 1) {
                            $creneau = $listCreneauIndexS[$dispo->getCreneau()->getId()] ?? $listCreneauIndexS[0];
                        } else {
                            $creneau = $listCreneauIndexW[$dispo->getCreneau()->getId()] ?? $listCreneauIndexS[0];
                        }
                        
                        $dispoJoueurNew = new DisponibiliteJoueurs();
                        
                        $dispoJoueurNew->setDispo($listDispoIndex[$dispo->getDispo()->getId()] ?? $listDispoIndex[1])
                                       ->setCreneau($creneau)
                                       ->setTypeCreneau($dispo->getTypeCreneau());
                        
                        
                        $logModification = new LogEventInscription();
                        $logModification->setLibelle("Ajout d'une dispo")
                                        ->setValeurAvant(null)
                                        ->setValeurApres(null)
                                        ->setDeclencheur($this->user)
                                        ->setVisible(true)
                                        ->setTypologie(0)
                                        ->setEventAt(new DateTime('now'));
                        $inscriptionBdd->addDispo($dispoJoueurNew)
                                       ->addLogEventInscription($logModification);
                    }
                }
            }
            
            
        }
        
        
        if ($inscriptionBdd->getCommentaires() !== $inscription->getCommentaires()) {
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification des commentaires")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setCommentaires($inscription->getCommentaires())
                           ->addLogEventInscription($logModification);
        }
        if ($inscriptionBdd->dechiffreMoyenContact($this->gh) !== $inscription->getMoyenContact()) {
            
            // chiffrement des moyens de contact
            $moyenContactSansSaut =
                preg_replace("# {2,}#", " ", preg_replace("#(\r\n|\n\r|\n|\r)#", " ", $inscription->getMoyenContact()));
            
            openssl_public_encrypt($moyenContactSansSaut, $moyenContact, $this->gh->getPublicKey());
            
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification du moyen de contact")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setMoyenContact(base64_encode((string)$moyenContact))
                           ->addLogEventInscription($logModification);
        }
        if ($inscriptionBdd->getTypeDiffusion() !== $inscription->getTypeDiffusion()) {
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification de la diffusion du moyen de contact")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setTypeDiffusion($inscription->getTypeDiffusion())
                           ->addLogEventInscription($logModification);
        }
        
        
        if (!$jumpInEvent && $inscriptionBdd->getFuseau() !== $inscription->getFuseau()) {
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Modification du fuseau horaires")
                            ->setValeurAvant($inscriptionBdd->getFuseau())
                            ->setValeurApres($inscription->getFuseau())
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->setFuseau($inscription->getFuseau())
                           ->addLogEventInscription($logModification);
        }
        
        
        // Sauvegarder les roles
        $roleInJump = $inscription->getRoleJoueurJumps();
        
        /**
         * @var RoleJump[] $listNewRole
         */
        $listNewRole = [];
        foreach ($roleInJump as $role) {
            $listNewRole[$role->getId()] = $role;
        }
        
        foreach ($inscriptionBdd->getRoleJoueurJumps() as $role) {
            if (!isset($listNewRole[$role->getId()])) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Suppression d'un role")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(0)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->removeRoleJoueurJump($role)
                               ->addLogEventInscription($logModification);
                
            } else {
                unset($listNewRole[$role->getId()]);
            }
        }
        
        foreach ($listNewRole as $role) {
            $logModification = new LogEventInscription();
            $logModification->setLibelle("Ajout d'un rôle")
                            ->setValeurAvant(null)
                            ->setValeurApres(null)
                            ->setDeclencheur($this->user)
                            ->setVisible(true)
                            ->setTypologie(0)
                            ->setEventAt(new DateTime('now'));
            $inscriptionBdd->addRoleJoueurJump($listRoleIndex[$role->getId()] ?? $listRoleIndex[0])
                           ->addLogEventInscription($logModification);
        }
        
        
        // Sauvegarder les leads
        $leadInJump = $inscription->getLeadInscriptions();
        
        /**
         * @var LeadInscription[][] $listNewLead
         */
        $listNewLead = [];
        foreach ($leadInJump as $lead) {
            $listNewLead[$lead->getLead()->getId()][$lead->getApprenti() ? 1 : 0] = $lead;
        }
        
        foreach ($inscriptionBdd->getLeadInscriptions() as $lead) {
            if (!isset($listNewLead[$lead->getLead()->getId()][$lead->getApprenti() ? 1 : 0])) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Suppression d'un lead")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(0)
                                ->setEventAt(new DateTime('now'));
                $inscriptionBdd->removeRoleJoueurJump($role)
                               ->addLogEventInscription($logModification);
                
            } else {
                unset($listNewLead[$lead->getLead()->getId()][$lead->getApprenti() ? 1 : 0]);
            }
        }
        
        foreach ($listNewLead as $appOrLead) {
            foreach ($appOrLead as $lead) {
                $logModification = new LogEventInscription();
                $logModification->setLibelle("Ajout d'un lead")
                                ->setValeurAvant(null)
                                ->setValeurApres(null)
                                ->setDeclencheur($this->user)
                                ->setVisible(true)
                                ->setTypologie(0)
                                ->setEventAt(new DateTime('now'));
                
                $newLead = new LeadInscription();
                $newLead->setApprenti($lead->getApprenti())
                        ->setLead($listLeadIndex[$lead->getLead()->getId()] ?? $listLeadIndex[0]);
                
                $inscriptionBdd->addLeadInscription($newLead)
                               ->addLogEventInscription($logModification);
            }
            
        }
        
        
        try {
            
            $inscriptionBdd->setDateModification(new DateTime('now'));
            
            $this->entityManager->persist($inscriptionBdd);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscriptionBdd);
            
            // conversion du moyen de contact pour le fournir au front correctement
            $inscriptionBdd->setMoyenContact($inscriptionBdd->dechiffreMoyenContact($this->gh));
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Modification réussie !", [], 'jumpEvent');
            $retour['zoneRetour'] = ['inscription' => $this->serializerService->serializeArray($inscriptionBdd, 'json', ['inscription_jump', 'jump', 'general_res', 'candidature_jump'])];
            
        } catch (Exception $e) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème modification inscription jump - User {$this->user->getId()} : " .
                $e->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour, 200);
    }
    
    #[Route(path: '/reinscription_jump', name: 'reinscription_jump', methods: ['POST']), IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function reinscription_jump(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           = "$codeErreur - Utilisateur non connecté - gestion jump";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        $data = new InscriptionJumpRestDto();
        
        try {
            $this->serializerService->deserialize($request->getContent(), InscriptionJumpRestDto::class, 'json', $data, ['inscription', 'creation_ins', 'jump', 'general_res']);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct jump - gestion - {$this->user->getId()} - {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($data->getJumpId() === null || $data->getUserId() === null || $data->getInscription() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Joueur - : {$this->user?->getId()} - Jump fournis : {$data->getJumpId()} - info fournis manquante - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // controle sur la longueur de l'id donné
        if (strlen($data->getJumpId()) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - L'identifiant fournis est incorrect - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        // Vérification de l'existance et récupération du jump
        $jump = $this->jumpHandler->recuperationJump($data->getJumpId());
        if ($jump === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Jump inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $inscription = $data->getInscription();
        
        
        // Verification de l'existance de l'inscription en vu de modifier
        $inscriptionBdd =
            $this->entityManager->getRepository(InscriptionJump::class)->findOneBy(['id' => $inscription->getId()]);
        if ($inscriptionBdd === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Inscription inconnu - {$data->getJumpId()} - Joueur {$data->getUserId()} - {$request->getRequestUri()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour);
        }
        
        $statutReinscription = $this->entityManager->getRepository(StatutInscription::class)->find(1);
        
        $libTranslate    = $this->translator->trans("Reinscription au jump", [], 'jumpEvent');
        $logModification = new LogEventInscription();
        $logModification->setLibelle("Désistement du jump")
                        ->setValeurAvant(null)
                        ->setValeurApres(null)
                        ->setDeclencheur($this->user)
                        ->setVisible(true)
                        ->setTypologie(0)
                        ->setEventAt(new DateTime('now'));
        
        $inscriptionBdd->setStatut($statutReinscription)
                       ->setDateModification(new DateTime('Now'))
                       ->addLogEventInscription($logModification);
        
        
        try {
            
            $this->entityManager->persist($inscriptionBdd);
            $this->entityManager->flush();
            $this->entityManager->refresh($inscriptionBdd);
            
            // conversion du moyen de contact pour le fournir au front correctement
            $inscriptionBdd->setMoyenContact($inscriptionBdd->dechiffreMoyenContact($this->gh));
            
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Réinscription effectué !", [], 'jumpEvent');
            $retour['zoneRetour'] = ['inscription' => $this->serializerService->serializeArray($inscriptionBdd, 'json', ['inscription_jump', 'jump', 'general_res'])];
            
        } catch (Exception $e) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $retour['zoneRetour'] = ['jump' => null];
            $messageLog           =
                "$codeErreur - Problème désistement inscription jump - User {$this->user->getId()} : " .
                $e->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
        }
        
        return new JsonResponse($retour, 200);
    }
    
}