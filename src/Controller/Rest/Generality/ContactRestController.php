<?php

namespace App\Controller\Rest\Generality;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\StatsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpdateAPIHandler;
use App\Structures\Dto\GH\Generality\ContactDto;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/contact', name: 'rest_generality_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class ContactRestController extends AbstractRestGestHordesController
{
    
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected UpdateAPIHandler       $updateAPIHandler,
        protected StatsHandler           $statsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/contact', name: 'contact', methods: ['GET'])]
    public function contact(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $contact      = [];
            
            return new JsonResponse([
                                        'contact' => $contact,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ContactRestController', 'contact');
        }
        
    }
    
    #[Route('/contact', name: 'contact_send', methods: ['POST'])]
    public function contact_send(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            // On récupère les données
            /** @var ContactDto $data */
            $data = $this->serializerService->deserialize($request->getContent(), ContactDto::class);
            
            // On formate le message pour rajouter des informations sur le joueur - pseudo, id et numéro de ville actuelle
            $messageConfidentiel = "- Pseudo : {$this->user?->getPseudo()}\n";
            $messageConfidentiel .= "- ID : {$this->user?->getId()}\n";
            if ($this->ville !== null) {
                $messageConfidentiel .= "***Information sur la ville actuelle***\n";
                $messageConfidentiel .= "- Ville : {$this->ville->getNom()}\n";
                $messageConfidentiel .= "- MapId : {$this->ville->getMapId()}\n";
                $messageConfidentiel .= "- Jour : {$this->ville->getJour()}\n";
            }
            
            
            $idChannel = $this->discordService->generateMessageThreadDiscord($data->getTitre(),
                                                                             $data->getMessage(),
                                                                             $data->getTag(),
                                                                             $data->getUrlImage(),
                                                                             $data->isConfidential(),
                                                                             $messageConfidentiel,
            );
            $contact   = ['urlForum' => "https://discord.com/channels/801202909559521301/{$idChannel}/{$idChannel}",];
            
            return new JsonResponse([
                                        'contact' => $contact,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ContactRestController', 'contact_send');
        } catch (GuzzleException $e) {
            return $this->errorHandler->handleException($e, 'ContactRestController', 'contact_send');
        }
        
    }
    
    #[Route('/creation_image', name: 'creation_image', methods: ['POST'])]
    public function creation_image(Request $request): Response
    {
        
        try {
            
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            
            $upload  = $data['up'] ?? 1;
            $oldName = $data['oldName'] ?? null;
            
            if (isset($data['image'])) {
                $file_chunks = explode(";base64,", (string)$data['image']);
                $image       = base64_decode($file_chunks[1]);
                $mime        = $data['mime'] ?? 'png';
            } else {
                $image = null;
                $mime  = null;
            }
            
            
            if ($image === null && $upload == 1) {
                throw new GestHordesException("Image non fournis - Joueur : {$this->user?->getId()}");
            }
            $urlSortie = null;
            if ($oldName !== null && $oldName !== '' && file_exists('../public/uploads/discord/' . $oldName)) {
                unlink('../public/uploads/discord/' . $oldName);
            }
            
            if ($upload == 1) {
                
                $filenameNew = bin2hex(random_bytes(12)) . '.' . $mime;
                
                $fileopen = (fopen('../public/uploads/discord/' . $filenameNew, 'w'));
                fwrite($fileopen, (string)$image);
                fclose($fileopen);
                $urlSortie = $filenameNew;
            }
            
            return new JsonResponse(['urlBanniere' => $urlSortie,], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ContactRestController', 'creation_image');
        }
        
    }
    
}