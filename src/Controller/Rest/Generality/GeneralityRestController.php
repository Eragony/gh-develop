<?php

namespace App\Controller\Rest\Generality;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\NewsSite;
use App\Entity\VersionsSite;
use App\Exception\AuthenticationException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\StatsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpdateAPIHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/generality', name: 'rest_generality_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class GeneralityRestController extends AbstractRestGestHordesController
{
    
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected UpdateAPIHandler       $updateAPIHandler,
        protected StatsHandler           $statsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     * @throws JsonException
     */
    #[Route(path: '/changelog', name: 'changelog', methods: ['GET'])]
    public function changelog(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $versions     = $this->entityManager->getRepository(VersionsSite::class)->allDesc();
        
        foreach ($versions as $version) {
            $version->setTitre($this->translator->trans($version->getTitre(), [], 'version'));
            
            foreach ($version->getContents() as $content) {
                $content->setContents($this->translator->trans($content->getContents(), [], 'version'));
            }
        }
        
        $changelogJson =
            json_decode($this->gh->getSerializer()->serialize($versions, 'json', ['groups' => ['changelog']]), null,
                        512, JSON_THROW_ON_ERROR);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'changelog' => ['changelogData' => $changelogJson],
            'general'   => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    /**
     * @throws JsonException
     */
    #[Route(path: '/general', name: 'general', methods: ['GET'])]
    public function general(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $retour       = [
            'general' => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        return new JsonResponse($retour, Response::HTTP_OK);
        
    }
    
    #[Route(path: '/', name: 'index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $listNews     = $this->entityManager->getRepository(NewsSite::class)->find10Last();
        
        /* foreach ($listNews as $news) {
             $news->setTitre($this->translator->trans($news->getTitre(), [], 'version'));
             
             if ($news->getVersion() !== null) {
                 foreach ($news->getVersion()->getContents() as $content) {
                     $content->setContents($this->translator->trans($content->getContents(), [], 'version'));
                 }
             } else {
                 $news->setContent($this->translator->trans($news->getContent(), [], 'version'));
             }
         }*/
        
        $newsJson =
            json_decode($this->gh->getSerializer()->serialize($listNews, 'json', ['groups' => ['news']]), null, 512,
                        JSON_THROW_ON_ERROR);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'news'    => [
                'newsData' => $newsJson,
            ],
            'general' => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
        
    }
    
    #[Route('/majLang', name: 'maj_lang', methods: ['POST'])]
    public function majLang(Request $request): JsonResponse
    {
        $lang = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR) ?? 'fr';
        switch ($lang) {
            case 'en':
            case 'fr':
            case 'de':
            case 'es':
                $this->userHandler->sauvegardeLang($this->getUser(), $lang);
                $request->getSession()->set('_locale', $lang);
                return new JsonResponse(['ok' => true], Response::HTTP_OK);
            default:
                return new JsonResponse(['ok' => false], 400);
            
        }
        
    }
    
    #[Route('/ping', name: 'ping', methods: ['POST'])]
    public function ping(Request $request): JsonResponse
    {
        try {
            if ($this->user === null) {
                return new JsonResponse(['reload' => false], Response::HTTP_OK);
            }
            
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            
            $lastVersion = $this->entityManager->getRepository(VersionsSite::class)->last();
            
            if ($lastVersion === null) {
                $versionGH = "unknown";
            } else {
                if ($lastVersion->getTagName() !== null) {
                    $versionGH =
                        "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}-{$lastVersion->getTagName()}.{$lastVersion->getNumTag()}";
                } else {
                    $versionGH =
                        "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}";
                }
            }
            
            if ($data['versionClient'] !== $versionGH) {
                return new JsonResponse(['reload' => true], Response::HTTP_OK);
            } else {
                $this->user->setClientUpdated(true);
                $this->entityManager->persist($this->user);
                $this->entityManager->flush();
                return new JsonResponse(['reload' => false], Response::HTTP_OK);
            }
        } catch (Exception $exception) {
            $this->logger->error($request->getContent());
            $this->logger->error($request);
            return $this->errorHandler->handleException($exception, 'GeneralityRestController', 'ping');
        }
        
    }
    
    #[Route('/refresh', name: 'refresh_site', methods: ['GET'])]
    public function refresh(Request $request): JsonResponse
    {
        $mapIdSession         = (int)$request->headers->get('gh-mapId') ?? 0;
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = ['general' => $this->generateArrayGeneral($request, $mapIdSession),];
        
        
        return new JsonResponse($retour, Response::HTTP_OK);
        
    }
    
    #[Route('/reset', name: 'reset', methods: ['POST'])]
    public function reset(Request $request): JsonResponse
    {
        $request->getSession()->remove('mapId');
        return new JsonResponse(['ok' => true], Response::HTTP_OK);
    }
    
    #[Route('/statistiques', name: 'statistique', methods: ['GET'])]
    public function statistique(Request $request): Response
    {
        try {
            $mapIdSession    = (int)$request->headers->get('gh-mapId') ?? 0;
            $statsThemes     = $this->statsHandler->getThemes();
            $statsPouvoirs   = $this->statsHandler->getPower();
            $statsJobs       = $this->statsHandler->getJobs(null, null);
            $statsMort       = $this->statsHandler->getMort(null, null);
            $statsTypeMort   = $this->statsHandler->getTypeMort(null, null);
            $statsChantiers  = $this->statsHandler->getBuildings(null, null);
            $statsBatiments  = $this->statsHandler->getBatiments(null, null);
            $statsScrutateur = $this->statsHandler->getScrutateurStats(null, null);
            $statsTypeVille  = $this->statsHandler->getTypeVille(null, null);
            $statsDayVille   = $this->statsHandler->recuperationStatVilleByDay(null, null);
            
            
            $stats = [
                'statsData' => [
                    'users'      => [
                        'themes' => $statsThemes,
                        'power'  => $statsPouvoirs,
                    ],
                    'cities'     => [
                        'jobs'      => $statsJobs,
                        'mort'      => $statsMort,
                        'typeMort'  => $statsTypeMort,
                        'typeVille' => $statsTypeVille,
                        'dayVille'  => $statsDayVille,
                    ],
                    'buildings'  => [
                        'work' => $statsChantiers,
                    ],
                    'maps'       => [
                        'ruins' => $statsBatiments,
                        'scrut' => $statsScrutateur,
                    ],
                    'listSaison' => $this->statsHandler->getListSaison(),
                ],
            ];
            
            return new JsonResponse([
                                        'stats'   => $stats,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'GeneralityRestController', 'statistique');
        }
        
    }
    
    #[Route('/stats/{saisonId}', name: 'statistique_saison', methods: ['GET'])]
    public function statistique_saison(Request $request, int $saisonId): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            $arraySaison  = $this->statsHandler->returnSaisonPhase($saisonId);
            
            $saison = $arraySaison['saison'];
            $phase  = $arraySaison['phase'];
            
            $statsThemes      = $this->statsHandler->getThemes();
            $statsPouvoirs    = $this->statsHandler->getPower();
            $statsJobs        = $this->statsHandler->getJobs($saison, $phase);
            $statsMort        = $this->statsHandler->getMort($saison, $phase);
            $statsTypeMort    = $this->statsHandler->getTypeMort($saison, $phase);
            $statsChantiers   = $this->statsHandler->getBuildings($saison, $phase);
            $statsBatiments   = $this->statsHandler->getBatiments($saison, $phase);
            $statsScrutateurs = $this->statsHandler->getScrutateurStats($saison, $phase);
            $statsTypeVille   = $this->statsHandler->getTypeVille($saison, $phase);
            $statsDayVille    = $this->statsHandler->recuperationStatVilleByDay($saison, $phase);
            
            $stats = [
                'statsData' => [
                    'users'      => [
                        'themes' => $statsThemes,
                        'power'  => $statsPouvoirs,
                    ],
                    'cities'     => [
                        'jobs'      => $statsJobs,
                        'mort'      => $statsMort,
                        'typeMort'  => $statsTypeMort,
                        'typeVille' => $statsTypeVille,
                        'dayVille'  => $statsDayVille,
                    ],
                    'buildings'  => [
                        'work' => $statsChantiers,
                    ],
                    'maps'       => [
                        'ruins' => $statsBatiments,
                        'scrut' => $statsScrutateurs,
                    ],
                    'listSaison' => $this->statsHandler->getListSaison(),
                ],
            ];
            
            return new JsonResponse([
                                        'stats'   => $stats,
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'GeneralityRestController', 'statistique');
        }
        
    }
    
    #[Route('/update', name: 'update_site', methods: ['POST'])]
    public function updateSite(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            
            if ($this->user === null) {
                throw new AuthenticationException('Utilisateur non connecté');
            }
            if ($this->user->getApiToken() === null) {
                $this->logger->error($this->user->getPseudo());
            }
            
            
            $this->updateAPIHandler->updateAll($request, $this->user->getApiToken());
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
            
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface|Exception $exception) {
            return $this->errorHandler->handleException($exception, 'GeneralityRestController', 'updateSite');
        }
        
    }
    
}