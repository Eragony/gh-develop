<?php

namespace App\Controller\Rest\Outils;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Banque;
use App\Entity\ChantierPrototype;
use App\Entity\Defense;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\UpHomePrototype;
use App\Exception\GestHordesException;
use App\Security\Voter\InVilleVoter;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ChantierHandler;
use App\Service\Outils\ReparationHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Structures\Collection\PlansChantiers;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/outils/chantiers', name: 'rest_outils_chantiers_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class ChantiersRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected ChantierHandler        $chantierHandler,
        protected ChantiersHandler       $chantiersHandler,
        protected UpGradeHandler         $upGradeHandler,
        protected ReparationHandler      $reparationHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     * @throws GestHordesException
     * @throws JsonException
     */
    #[Route(path: '/', name: 'outil_chantier', methods: ['GET'])]
    public function main(Request $request): Response
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
            // controle si l'utilisateur est connecté
            $this->userHandler->verifyUserIsConnected();
            
            // récupération de la ville de l'utilisateur
            $ville = $this->userHandler->getTownOfUser($this->user);
            
            // Coontrole si la ville est définie
            if ($ville === null) {
                throw new GestHordesException('La ville de l\'utilisateur n\'est pas définie');
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville,
                                           'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            // Récupération des outils de la ville
            $outils = $this->chantierHandler->getOutilsChantierOfTown($ville);
            
            // Récupération des outils chantier
            $outilsChantier = $outils->getOutilsChantier();
            
            // Récupération de la def actuelle de la ville
            /** @var Defense $defVille */
            $defVille = $this->ville->getDefense()->last();
            
            /// Récupération des plans disponibles
            
            $plansCollec = new PlansChantiers();
            $plansCollec->setPlansChantiers($this->ville->getPlansChantiers());
            
            /// Récupération des chantiers
            $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie();
            
            // On va regrouper les items cassés avec les non cassés
            $banque         = $this->entityManager->getRepository(Banque::class)->somItemBanque($this->ville);
            $listItemBanque = array_combine(array_map(fn($i) => $i['id'], $banque), $banque);
            
            // On récupére la liste de tous les objets réparables
            $listArmes = $this->entityManager->getRepository(ItemPrototype::class)->findBy(['reparable' => true, 'actif' => true]);
            
            // On récupère les items cassés pour les réparer
            $armeARepa = $this->entityManager->getRepository(Banque::class)->findBy(['broked' => true, 'ville' => $this->ville]);
            
            // Récupération des âmes en banque qui n'ont pas été encore purifiés
            $items_soul       = $this->entityManager->getRepository(ItemPrototype::class)->findBy(['uid' => ['soul_blue_#00', 'soul_red_#00']]);
            $items_soulInBank = $this->entityManager->getRepository(Banque::class)->findBy(['item' => $items_soul, 'ville' => $this->ville]);
            
            // Calcul du nombre de gardien et de mort
            $nb_gardien = 0;
            $nb_mort    = 0;
            foreach ($this->ville->getCitoyens() as $citoyen) {
                if (!$citoyen->getMort() && $citoyen->getJob()->getId() == JobPrototype::job_guardian) {
                    $nb_gardien++;
                }
                if ($citoyen->getMort()) {
                    $nb_mort++;
                }
            }
            
            $reductionPA = $this->upGradeHandler->recupBonusAtelierReductionPA($this->ville);
            
            // Récupération de la liste des habitations
            $listHabitation = $this->entityManager->getRepository(HomePrototype::class)->findAllIndexed();
            
            $listAme = $this->entityManager->getRepository(UpHomePrototype::class)->findAllIndexed();
            
            
            // Récupération des defs annexes à l'outils
            $def_veilles   = $outils->getOutilsVeille()?->getDefVeilleur() ?? 0;
            $def_decharges = $outils->getOutilsDecharge()?->getDefTotale() ?? 0;
            
            // Appel du service Reparation pour faire le recalcul des pa et def
            if ($outils->getOutilsReparation() !== null) {
                $outilsReparation = $outils->getOutilsReparation();
                $this->reparationHandler->recalculPaDefReparation($outilsReparation, $this->ville, $def_decharges);
                $pa_repa  = $outilsReparation->getPaTot();
                $def_repa = $outilsReparation->getGainDef();
                
            } else {
                $def_repa = 0;
                $pa_repa  = 0;
            }
            $evoSD = $this->upGradeHandler->recupBonusSD($this->ville);
            $defVille->setBonusSdPct($evoSD);
            $defense            = $this->serializerService->serializeArray($defVille, 'json', ['defense']);
            $banque             = $this->serializerService->serializeArray($listItemBanque, 'json', ['banque']);
            $listChantier       = $this->serializerService->serializeArray($listChantiers, 'json', ['chantier']);
            $listObjetReparer   = $this->serializerService->serializeArray($armeARepa, 'json', ['banque']);
            $listArmesAll       = $this->serializerService->serializeArray($listArmes, 'json', ['banque']);
            $listPlansVille     =
                $this->serializerService->serializeArray($plansCollec->getPlansChantiers(), 'json', ['plan']);
            $plansConstruit     =
                $this->serializerService->serializeArray($ville->getChantiers(), 'json', ['plan_construit']);
            $avancementChantier =
                $this->serializerService->serializeArray($ville->getAvancementChantiers(), 'json', ['chantier']);
            $outilsChantierJson = $this->serializerService->serializeArray($outilsChantier, 'json', ['outils_chantier', 'general', 'item', 'chantier']);
            $listHabitationJson = $this->serializerService->serializeArray($listHabitation, 'json', ['outils_chantier', 'general', 'item', 'chantier']);
            $listAmeJson        = $this->serializerService->serializeArray($listAme, 'json', ['outils_chantier', 'general', 'item', 'chantier']);
            $listCitoyensJson   = $this->serializerService->serializeArray($ville->getCitoyens()->toArray(), 'json', ['outils_chantier', 'general_res', 'item', 'chantier']);
            
            
            $outilsChantierTabs = [
                'banque'                 => $banque,
                'defense'                => $defense,
                'listAvancement'         => $avancementChantier,
                'listAReparer'           => $listObjetReparer,
                'listChantier'           => $listChantier,
                'listChantiersConstruit' => $plansConstruit,
                'listCitoyens'           => $listCitoyensJson,
                'listPlansVille'         => $listPlansVille,
                'outilsChantier'         => $outilsChantierJson,
                'option'                 => [
                    'nb_gardien'      => $nb_gardien,
                    'nb_ame_banque'   => count($items_soulInBank),
                    'nb_mort'         => $nb_mort,
                    'id_tdga'         => ChantierPrototype::ID_CHANTIER_TDGA,
                    'id_cimetiere'    => ChantierPrototype::ID_CHANTIER_CIME,
                    'id_cercueil'     => ChantierPrototype::ID_CHANTIER_CERCUEIL,
                    'id_hamame'       => ChantierPrototype::ID_CHANTIER_HAMAME,
                    'id_strategie'    => ChantierPrototype::ID_CHANTIER_STRATEGIE,
                    'reductionPA'     => $reductionPA,
                    'pa_repaChantier' => $pa_repa,
                    'list_armes'      => $listArmesAll,
                    'list_home'       => $listHabitationJson,
                    'list_ame'        => $listAmeJson,
                ],
                'recapDefAutre'          => [
                    'veille'     => $def_veilles,
                    'decharge'   => $def_decharges,
                    'reparation' => $def_repa,
                ],
            ];
            
            return new JsonResponse([
                                        'outilsChantier' => $outilsChantierTabs,
                                        'general'        => $this->generateArrayGeneral($request, $mapIdSession),
                                    ], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ChantiersRestController', 'main');
        }
    }
    
}
