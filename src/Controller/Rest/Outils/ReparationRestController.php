<?php

namespace App\Controller\Rest\Outils;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Defense;
use App\Entity\Outils;
use App\Entity\OutilsReparation;
use App\Entity\ReparationChantier;
use App\Entity\Ville;
use App\Security\Voter\InVilleVoter;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ReparationHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Structures\Dto\GH\Outils\Reparation\ReparationsSaveDto;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/outils/reparation', name: 'rest_outils_reparation_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class ReparationRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected UpGradeHandler         $upGradeHandler,
        protected ChantiersHandler       $chantiersHandler,
        protected ReparationHandler      $reparationHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
        
    }
    
    /**
     * @throws JsonException
     */
    #[Route(path: '/', name: 'outil_repa', methods: ['GET'])]
    public function main(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $ville        = $this->userHandler->getTownBySession($mapIdSession);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        try {
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville,
                                           $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville",
                                                                    [], 'outils'));
        } catch (Exception $exception) {
            
            $this->logger->error('DechargesController - deny - ' . $exception->getMessage());
            
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville", [], 'outils');
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
            
            //return $this->redirectToRoute('index');
        }
        
        $manager = $this->entityManager;
        
        $outils = $manager->getRepository(Outils::class)->findOneBy(['ville' => $ville,
                                                                     'day'   => $ville->getJour()]);
        
        if ($outils === null || $outils->getOutilsReparation() === null) {
            
            if ($outils === null) {
                $outils = new Outils($ville, $ville->getJour());
                
                try {
                    $manager->persist($outils);
                    $manager->flush();
                } catch (Exception $exception) {
                    $this->logger->error('ReparationsController - sauvegarde outils - ' . $exception->getMessage());
                    
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    return new JsonResponse($retour, Response::HTTP_OK);
                }
            }
            
            $outilsReparation = new OutilsReparation();
            
        } else {
            
            $outilsReparation = $outils->getOutilsReparation();
            
        }
        
        
        // Récupération de la défense actuelle
        $defense = $manager->getRepository(Defense::class)->findOneBy(['ville' => $ville,
                                                                       'day'   => $ville->getJour()]);
        
        if ($defense === null) {
            $defense = new Defense($ville->getJour());
            $defense->setBuildings(0);
        }
        
        $pvByPa = $this->upGradeHandler->recupBonusAtelierPv($ville);
        $this->reparationHandler->recalculPaDefReparation($outilsReparation, $ville, $pvByPa);
        $outilsReparation->setDefBase($defense->getBuildings());
        
        $categorieChantiers = $manager->getRepository(ChantierPrototype::class)
                                      ->recupCategorie();
        
        
        if ($outilsReparation->getCreatedAt() !== null) {
            try {
                $textReparation = $this->reparationHandler->generateText($outilsReparation, $ville, $pvByPa);
            } catch (Exception $e) {
                $textReparation = $e->getMessage();
            }
        } else {
            $textReparation = "";
        }
        
        
        $categorieChantiersJson = json_decode($this->gh->getSerializer()->serialize($categorieChantiers, 'json',
                                                                                    ['groups' => ['reparation']]), null,
            512, JSON_THROW_ON_ERROR);
        $outilsReparationJson   = json_decode($this->gh->getSerializer()
                                                       ->serialize($outilsReparation,
                                                                   'json',
                                                                   ['groups' => ['outils_repa',
                                                                                 'general_res']]),
                                              null,
                                              512,
                                              JSON_THROW_ON_ERROR);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'reparation' => [
                'listCatChantier' => $categorieChantiersJson,
                'mapId'           => $ville->getMapId(),
                'outilRepa'       => $outilsReparationJson,
                'pvByPa'          => $pvByPa,
                'textReparation'  => $textReparation,
                'userId'          => $this->user->getId(),
            ],
            'general'    => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/save', name: 'save', methods: ['POST'])]
    public function saveReparation(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        $data = new ReparationsSaveDto();
        
        
        try {
            $this->serializerService->deserialize($request->getContent(), ReparationsSaveDto::class, 'json', $data, ['outils_repa', 'general_res']);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Format fournis non correct outils réparations - {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - La data fournis est incomplète - outils réparations." . $this->user->getId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils réparations - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils réparations - Le numéro de la ville est incorrecte : " . $data->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Verification user dans les citoyens de la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils réparations - Le user fournis n'est pas dans la ville : " . $data->getMapId() .
                ' - user fournis : ' . $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Récupération de l'outils de la ville, et ensuite des réparations
        
        $outils = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville,
                                                                                 'day'   => $ville->getJour()]);
        
        if ($outils === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils réparations - Tentative d'utilisation d'une data outils non crée : " .
                $data->getMapId() . ' - user fournis : ' . $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        if ($outils->getOutilsReparation() === null) {
            $outilsReparation = new OutilsReparation();
        } else {
            $outilsReparation = $outils->getOutilsReparation();
        }
        
        // Récupération de la défense actuelle
        $defense = $this->entityManager->getRepository(Defense::class)->findOneBy(['ville' => $ville,
                                                                                   'day'   => $ville->getJour()]);
        
        if ($defense === null) {
            $defense = new Defense($ville->getJour());
            $defense->setBuildings(0);
        }
        
        $pvByPa      = $this->upGradeHandler->recupBonusAtelierPv($ville);
        $paTotReCalc = 0;
        $gainDefTot  = 0;
        
        $listChantier = $this->entityManager->getRepository(Chantiers::class)->recupChantierAReparer($ville);
        // On balaye les chantiers dans le cas d'un existant
        foreach ($outilsReparation->getReparationChantiers() as $reparationChantier) {
            if (isset($listChantier[$reparationChantier->getChantier()->getId()])) {
                if ($reparationChantier->getPvActuel() !=
                    $listChantier[$reparationChantier->getChantier()->getId()]->getPv()) {
                    $this->reparationHandler->calculReparation(
                        $reparationChantier,
                        $listChantier[$reparationChantier->getChantier()->getId()],
                        $pvByPa,
                        $paTotReCalc,
                        $gainDefTot,
                    );
                } else if ($reparationChantier->getPctRepa() !== $data->getOutilsReparations()->getReparationChantier($reparationChantier->getChantier())->getPctRepa()) {
                    $this->reparationHandler->calculReparation(
                        $reparationChantier,
                        $listChantier[$reparationChantier->getChantier()->getId()],
                        $pvByPa,
                        $paTotReCalc,
                        $gainDefTot,
                        $data->getOutilsReparations()->getReparationChantier($reparationChantier->getChantier())->getPctRepa(),
                    );
                } else {
                    $this->reparationHandler->calculCoutPARepa($reparationChantier, $paTotReCalc);
                    $this->reparationHandler->calculGainDef($reparationChantier, $gainDefTot);
                }
                unset($listChantier[$reparationChantier->getChantier()->getId()]);
            } else {
                $outilsReparation->removeReparationChantier($reparationChantier);
            }
        }
        
        foreach ($listChantier as $chantier) {
            $reparationChantier = new ReparationChantier();
            $reparationChantier->setChantier($chantier->getChantier());
            $this->reparationHandler->calculReparation($reparationChantier, $chantier, $pvByPa, $paTotReCalc,
                                                       $gainDefTot);
            $outilsReparation->addReparationChantier($reparationChantier);
        }
        
        $paTot   = 0;
        $gainDef = 0;
        
        foreach ($outilsReparation->getReparationChantiers() as $reparationChantier) {
            
            $reparationChantier->setPctRepa(
                $data->getOutilsReparations()->getReparationChantier($reparationChantier->getChantier())->getPctRepa(),
            );
            
            $this->reparationHandler->calculCoutPARepa($reparationChantier, $paTot);
            $this->reparationHandler->calculGainDef($reparationChantier, $gainDef);
            
        }
        
        $outilsReparation->setPaTot($paTot)
                         ->setGainDef($gainDef)
                         ->setTwoStep($data->getOutilsReparations()->getTwoStep())
                         ->setDefBase($defense->getBuildings());
        
        
        $outilsReparation->setOutils($outils);
        
        if ($data->isSauvegarde()) {
            
            if ($outilsReparation->getCreatedAt() === null) {
                $outilsReparation->setCreatedAt(new DateTime("Now"))
                                 ->setCreatedBy($this->user);
            } else {
                $outilsReparation->setModifyAt(new DateTime("Now"))
                                 ->setModifyBy($this->user);
            }
            
            try {
                
                $this->entityManager->persist($outilsReparation);
                $this->entityManager->flush();
                $this->entityManager->refresh($outilsReparation);
                
                
            } catch (Exception $exception) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $messageLog           =
                    "$codeErreur - Problèmes dans la sauvegarde des réparations {$data->getMapId()} : " .
                    $exception->getMessage();
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return new JsonResponse($retour);
            }
            
        }
        
        if ($outilsReparation->getCreatedAt() !== null) {
            try {
                $textReparation = $this->reparationHandler->generateText($outilsReparation, $ville, $pvByPa);
            } catch (Exception $e) {
                $textReparation = $e->getMessage();
            }
        } else {
            $textReparation = "";
        }
        
        
        $outilsReparationJson = json_decode($this->gh->getSerializer()
                                                     ->serialize($outilsReparation,
                                                                 'json',
                                                                 ['groups' => ['outils_repa',
                                                                               'general_res']]),
                                            null,
                                            512,
                                            JSON_THROW_ON_ERROR);
        
        return new JsonResponse([
                                    'codeRetour' => 0,
                                    'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                    'zoneRetour' => [
                                        'outilsReparations' => $outilsReparationJson,
                                        'textReparation'    => $textReparation,
                                    ],
                                ]);
        
    }
    
}