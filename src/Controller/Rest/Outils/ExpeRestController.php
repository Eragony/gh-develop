<?php

namespace App\Controller\Rest\Outils;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Citoyens;
use App\Entity\ExpeditionPart;
use App\Entity\TraceExpedition;
use App\Entity\User;
use App\Entity\Ville;
use App\Exception\GestHordesException;
use App\Security\Voter\InVilleVoter;
use App\Service\ErrorHandlingService;
use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Ville\CarteHandler;
use App\Structures\Dto\GH\Outils\Expedition\ChangeStatutExpedition;
use App\Structures\Dto\GH\Outils\Expedition\ChangeStatutOuvrier;
use App\Structures\Dto\GH\Outils\Expedition\DuplicateOuvriers;
use App\Structures\Dto\GH\Outils\Expedition\GetExpedition;
use App\Structures\Dto\GH\Outils\Expedition\RefreshExpedition;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeExpeditionOutils;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeOrdreExpeditions;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeOuvriers;
use App\Structures\Dto\GH\Outils\Expedition\SuppressionExpedition;
use App\Structures\Dto\GH\Outils\Expedition\VerrouillageExpedition;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/expe', name: 'rest_expe_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class ExpeRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected CarteHandler           $carteHandler,
        protected ExpeditionHandler      $expeditionHandler,
        protected ItemsHandler           $itemsHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/changeOrderExpedition', name: 'changeOrderExpedition', methods: ['POST'])]
    public function changeOrderExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On vérifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataExpeditions = new SauvegardeOrdreExpeditions();
            $this->serializerService->deserialize($request->getContent(), SauvegardeOrdreExpeditions::class, 'json',
                                                  $dataExpeditions, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataExpeditions->getMapId() ||
                $dataExpeditions->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataExpeditions->getMapId() . " - " . $dataExpeditions->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->changeOrderExpedition($dataExpeditions);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'changeOrderExpedition');
        }
    }
    
    #[Route('/changeStatutExpedition', name: 'changeStatutExpedition', methods: ['POST'])]
    public function changeStatutExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On verifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataStatut = new ChangeStatutExpedition();
            $this->serializerService->deserialize($request->getContent(), ChangeStatutExpedition::class, 'json',
                                                  $dataStatut, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataStatut->getMapId() ||
                $dataStatut->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataStatut->getMapId() . " - " . $dataStatut->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->changeStatutExpedition($dataStatut);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'changeStatutExpedition');
        }
    }
    
    #[Route('/changeStatutOuvrier', name: 'changeStatutOuvrier', methods: ['POST'])]
    public function changeStatutOuvrier(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On verifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataStatut = new ChangeStatutOuvrier();
            $this->serializerService->deserialize($request->getContent(), ChangeStatutOuvrier::class, 'json',
                                                  $dataStatut, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataStatut->getMapId() ||
                $dataStatut->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataStatut->getMapId() . " - " . $dataStatut->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->changeStatutOuvrier($ville, $dataStatut);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'changeStatutExpedition');
        }
    }
    
    #[Route('/deleteExpedition', name: 'deleteExpedition', methods: ['POST'])]
    public function deleteExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On vérifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataSupp = new SuppressionExpedition();
            $this->serializerService->deserialize($request->getContent(), SuppressionExpedition::class, 'json',
                                                  $dataSupp, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataSupp->getMapId() ||
                $dataSupp->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataSupp->getMapId() . " - " . $dataSupp->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->suppressionExpedition($dataSupp);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'deleteExpedition');
        }
    }
    
    #[Route('/duplicateOuvrier', name: 'duplicateOuvrier', methods: ['POST'])]
    public function duplicateOuvrier(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On vérifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataSave = new DuplicateOuvriers();
            $this->serializerService->deserialize($request->getContent(), DuplicateOuvriers::class, 'json', $dataSave, ['outils_expe']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataSave->getMapId() || $dataSave->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " . $dataSave->getMapId() . " - " . $dataSave->getIdUser() . " / contexte : " . $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->duplicateOuvrier($dataSave);
            
            return new JsonResponse([], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'sauvegardeOuvrier');
        }
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/edit', name: 'editExpe', methods: ['POST'])]
    public function editExpe(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        // récupération de la mapId depuis le headers dans la requete
        $mapId = (int)$request->headers->get('gh-mapId');
        
        $ville = $this->userHandler->getTownBySession($mapId);
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La ville fournis n'existe pas : " . $mapId);
            
            return new JsonResponse($retour);
        }
        
        $data    = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
        $idTrace = $data->expe_id;
        
        if (strlen((string)$idTrace) != 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'identifiant fournis n'est pas correcte : " . $idTrace);
            
            return new JsonResponse($retour);
        }
        
        // Recherche des tracés de l'expédition
        
        $traceExpe = $this->entityManager->getRepository(TraceExpedition::class)->findOneBy(['id'    => $idTrace,
                                                                                             'ville' => $ville]);
        
        if ($traceExpe === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous essayez d'éditer un tracé probablement supprimé, veuillez raffraichir la carte. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Edition - le tracés fournit n'existe pas en base de donnée : " .
                                 $idTrace);
            
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.", [],
                                         'outils');
            
            return new JsonResponse($retour);
        }
        
        $retour['zoneRetour'] = json_decode($this->gh->getSerializer()
                                                     ->serialize($traceExpe,
                                                                 'json',
                                                                 ['groups' => ['expe',
                                                                               'general_res']]),
                                            true,
                                            512,
                                            JSON_THROW_ON_ERROR);
        $retour['codeRetour'] = 0;
        
        return new JsonResponse($retour);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/enregTrace', name: 'enregistrement_trace', methods: ['POST'])]
    public function enregTrace(Request $request): JsonResponse
    {
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        $data = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
        
        $idTrace    = $data->id ?? '';
        $mapId      = $data->mapId ?? '';
        $crea_id    = $data->crea_id ?? '';
        $mod_id     = $data->mod_id ?? '';
        $nom_expe   = $data->nom ?? '';
        $pa_expe    = $data->pa_expe ?? 0;
        $jour_expe  = $data->jour_expe ?? 0;
        $collab     = $data->collab ?? null;
        $couleur    = $data->couleur ?? null;
        $coordonnee = $data->coordonnee ?? null;
        $personnel  = $data->personnel ?? null;
        $biblio     = $data->biblio ?? false;
        $brouillon  = $biblio ? false : ($data->brouillon ?? false);
        
        if ($idTrace !== '' && strlen((string)$idTrace) !== 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'identifiant fournis n'est pas correcte : " . $idTrace);
            
            return new JsonResponse($retour);
        }
        if ($mapId === '') {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'identifiant de la ville fournis n'est pas correcte : " . $idTrace);
            
            return new JsonResponse($retour);
        }
        if ($crea_id === '') {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'utilisateur fournis n'est pas correcte - createur : " . $crea_id);
            
            return new JsonResponse($retour);
        }
        if ($idTrace !== '' && $mod_id === '') {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'utilisateur fournis n'est pas correcte - modification : " . $mod_id);
            
            return new JsonResponse($retour);
        }
        if ($nom_expe === '') {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Le nom de l'expédition ne peut pas être vide.", [], 'outils');
            
            return new JsonResponse($retour);
        }
        if (strlen((string)$nom_expe) > 255) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Le nom de l'expédition est trop long, il ne doit pas dépasser 255 caractères.",
                                         [], 'outils');
            
            return new JsonResponse($retour);
        }
        if ($pa_expe === 0) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("L'expédition ne peut pas faire 0 PA !", [], 'outils');
            
            return new JsonResponse($retour);
        }
        
        if ($collab === null || $personnel === null || $jour_expe === 0 || $couleur === null || $coordonnee === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Tentative de création d'expedition sans donner toutes les informations nécessaire");
            
            return new JsonResponse($retour);
        }
        
        if (strlen((string)$couleur) !== 7) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La couleur fournis ne respecte pas le format attendu : " . $couleur);
            
            return new JsonResponse($retour);
        }
        
        if (count($coordonnee) <= 1) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("L'expédition est trop courte pour être enregistré, il doit y avoir au moins 2 points.",
                                         [], 'outils');
            
            return new JsonResponse($retour);
        }
        
        if ($idTrace === '') {
            $createur = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $crea_id]);
            if ($createur === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $this->logger->error("$codeErreur - Utilisateur non trouvé - createur : " . $crea_id);
                
                return new JsonResponse($retour);
            }
        } else {
            $modificateur = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $mod_id]);
            if ($modificateur === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $this->logger->error("$codeErreur - Utilisateur non trouvé - modificateur : " . $mod_id);
                
                return new JsonResponse($retour);
            }
        }
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Ville inconnue : " . $mapId);
            
            
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.", [],
                                         'outils');
            
            return new JsonResponse($retour);
        }
        
        $trace_expedition = false;
        
        // Recherche du tracé de l'expédition si l'id est fournis
        
        if ($idTrace !== '') {
            $traceExpe = $this->entityManager->getRepository(TraceExpedition::class)->findOneBy(['id'    => $idTrace,
                                                                                                 'ville' => $ville]);
            
            if ($traceExpe === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'outils',
                );
                $this->logger->error("$codeErreur - Le tracés fournit n'existe pas en base de donnée : $idTrace");
                
                return new JsonResponse($retour);
            }
            
            if ($traceExpe->isTraceExpedition()) {
                $trace_expedition = true;
            }
            
            $traceExpe->setModifyBy($modificateur ?? null)
                      ->setModifyAt(new DateTime('now'));
        } else {
            $traceExpe = new TraceExpedition();
            $traceExpe->setVille($ville)
                      ->setCreatedBy($createur)
                      ->setCreatedAt(new DateTime('now'));
        }
        
        if (!$trace_expedition) {
            $traceExpe->setJour($jour_expe)
                      ->setNom($nom_expe)
                      ->setCollab($collab)
                      ->setPersonnel($personnel)
                      ->setBrouillon($brouillon)
                      ->setBiblio($biblio);
        } else {
            // On récupère l'expédition associé afin de mettre à jour les PA
            $expedition = $traceExpe->getExpeditionPart();
            $expedition->setPa($pa_expe);
            $this->entityManager->persist($expedition);
        }
        
        $traceExpe->setPa($pa_expe)
                  ->setCouleur($couleur)
                  ->setCoordonnee($coordonnee);
        
        
        try {
            $this->entityManager->persist($traceExpe);
            $this->entityManager->flush();
            $retour['codeRetour'] = 0;
            if ($idTrace === '') {
                $retour['libRetour'] = $this->translator->trans("Le tracé a été correctement créé.", [], 'outils');
            } else {
                $retour['libRetour'] = $this->translator->trans("Le tracé a été correctement modifié.", [], 'outils');
            }
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Probleme mise à jour : " . $exception->getMessage());
        }
        
        return new JsonResponse($retour);
    }
    
    #[Route('/getExpedition', name: 'getExpedition', methods: ['POST'])]
    public function getExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataExped = new GetExpedition();
            $this->serializerService->deserialize($request->getContent(), GetExpedition::class, 'json',
                                                  $dataExped, ['outils_expe']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataExped->getMapId() ||
                $dataExped->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {recup verrouillage : " .
                                              $dataExped->getMapId() . " - " . $dataExped->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $expedition = $this->expeditionHandler->recuperationExpeditionById($dataExped, $ville);
            
            // On vérifie que l'expédition n'est pas verrouillé, et que le délai n'est pas dépassé
            if ($expedition->isVerrou()) {
                $dateVerrou = $expedition->getVerrouAt()->add(new DateInterval('PT5M'));
                if ($dateVerrou < new DateTime('now')) {
                    $expedition->setVerrou(false);
                    $this->entityManager->persist($expedition);
                    $this->entityManager->flush();
                } else {
                    if ($expedition->getVerrouBy()->getId() !== $this->user->getId()) {
                        throw new GestHordesException($this->translator->trans("L'expédition est verrouillée par {user}, vous ne pouvez pas la modifier.", ["{user}" => $expedition->getVerrouBy()->getPseudo()], 'outils'), GestHordesException::SHOW_MESSAGE_INFO);
                    }
                }
            }
            
            
            return new JsonResponse([
                                        'expedition' => $this->serializerService->serializeArray($expedition, 'json', ['outils_expe', 'general_res']),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'getExpedition');
        }
    }
    
    #[Route('/getVerrouExpedition', name: 'getVerrouExpedition', methods: ['POST'])]
    public function getVerrouExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataRefresh = new RefreshExpedition();
            $this->serializerService->deserialize($request->getContent(), RefreshExpedition::class, 'json',
                                                  $dataRefresh, ['outils_expe']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataRefresh->getMapId() ||
                $dataRefresh->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataRefresh->getMapId() . " - " . $dataRefresh->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $outilsExpedition = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $dataRefresh->getJour());
            
            $expeditionVerouiller = $this->expeditionHandler->getExpeditionVerou($outilsExpedition);
            
            
            return new JsonResponse([
                                        'verrou' => $expeditionVerouiller,
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'refreshOutils');
        }
    }
    
    #[Route('/{mapId}', name: 'main', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On vérifie que le mapId concorde
            if ($mapIdSession !== $mapId) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $mapIdSession . " / contexte : " . $mapId . "}");
            }
            
            $ville = $this->userHandler->getTownBySession($mapId);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            // controle si l'utilisateur a le droit de voir la ville
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville,
                                           'Acces non autorisé, il ne s\'agit pas de votre ville');
            
            $carte = $this->carteHandler->recupInfoVilleCarte($ville, $this->user, true);
            
            $popUpMaj = $this->itemsHandler->recuperationItems([10]);
            
            return new JsonResponse([
                                        'outils'  => [
                                            'carte'    => $carte,
                                            'popUpMaj' => $popUpMaj,
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapId),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'index');
        }
        
    }
    
    #[Route('/refreshOutils', name: 'refreshOutils', methods: ['POST'])]
    public function refreshOutils(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataRefresh = new RefreshExpedition();
            $this->serializerService->deserialize($request->getContent(), RefreshExpedition::class, 'json',
                                                  $dataRefresh, ['outils_expe']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataRefresh->getMapId() ||
                $dataRefresh->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataRefresh->getMapId() . " - " . $dataRefresh->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            
            return new JsonResponse([
                                        'outils'  => $this->expeditionHandler->recuperationOutilsExpeditionVilleJour($ville,
                                                                                                                     $dataRefresh->getJour()),
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'refreshOutils');
        }
    }
    
    #[Route('/sauvegardeExpedition', name: 'sauvegardeExpedition', methods: ['POST'])]
    public function sauvegardeExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On verifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataSauvegarde = new SauvegardeExpeditionOutils();
            $this->serializerService->deserialize($request->getContent(), SauvegardeExpeditionOutils::class, 'json',
                                                  $dataSauvegarde, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataSauvegarde->getMapId() ||
                $dataSauvegarde->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataSauvegarde->getMapId() . " - " . $dataSauvegarde->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->sauvegardeExpeditionOutils($dataSauvegarde);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'sauvegardeExpedition');
        }
    }
    
    #[Route('/sauvegardeOuvrier', name: 'sauvegardeOuvrier', methods: ['POST'])]
    public function sauvegardeOuvrier(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            // On vérifie que la ville existe
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataSave = new SauvegardeOuvriers();
            $this->serializerService->deserialize($request->getContent(), SauvegardeOuvriers::class, 'json',
                                                  $dataSave, ['outils_expe', 'general_res', 'carte_gen']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataSave->getMapId() ||
                $dataSave->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {sauvegarde : " .
                                              $dataSave->getMapId() . " - " . $dataSave->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->sauvegardeOuvrier($dataSave);
            
            return new JsonResponse([
                                        'general' => $this->generateArrayGeneral($request, $ville->getMapId()),
                                    ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'sauvegardeOuvrier');
        }
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/supTrace', name: 'supTrace', methods: ['POST'])]
    public function suppTrace(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        // récupération de la mapId depuis le headers dans la requete
        $mapId = (int)$request->headers->get('gh-mapId');
        
        $ville = $this->userHandler->getTownBySession($mapId);
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La ville fournis n'existe pas : " . $mapId);
            
            return new JsonResponse($retour);
        }
        
        $data    = json_decode($request->getContent(), null, 512, JSON_THROW_ON_ERROR);
        $idTrace = $data->expe_id;
        
        if (strlen((string)$idTrace) !== 24) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - L'identifiant fournis n'est pas correcte : " . $idTrace);
            
            return new JsonResponse($retour);
        }
        
        // Recherche des tracés de l'expédition
        
        $traceExpe = $this->entityManager->getRepository(TraceExpedition::class)->findOneBy(['id'    => $idTrace,
                                                                                             'ville' => $ville]);
        
        if ($traceExpe === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous essayez de supprimer un tracé probablement déjà supprimé, veuillez raffraichir la carte. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Suppression - le tracés fournit n'existe pas en base de donnée : " .
                                 $idTrace);
            
            return new JsonResponse($retour);
        }
        
        // On va chercher si le tracé est utilisé pour une expédition (sur une partie)
        $expedition = $this->entityManager->getRepository(ExpeditionPart::class)->findOneBy(['trace' => $traceExpe]);
        if ($expedition !== null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans("Vous essayez de supprimer un tracé qui a été mis sur une expédition, il n'est pas possible de le supprimer. Code erreur :{code}",
                                                             ['{code}' => $codeErreur],
                                                             'outils',
            );
            $this->logger->error("$codeErreur - Suppression - le tracés fournit n'existe pas en base de donnée : " .
                                 $idTrace);
            
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.", [],
                                         'outils');
            
            return new JsonResponse($retour);
        }
        
        // verification si le créateur est encore en vie dans la ville
        $citoyensTraceur = $this->entityManager->getRepository(Citoyens::class)->findOneBy(['citoyen' => $traceExpe->getCreatedBy(), 'ville' => $ville]);
        $isAlive         = !($citoyensTraceur?->getMort() ?? false);
        
        try {
            if (!$isAlive || $traceExpe->getCreatedBy()?->getId() === $this->user->getId()) {
                $this->entityManager->remove($traceExpe);
            } elseif ($traceExpe->getCollab()) {
                $traceExpe->setInactive(true);
                $this->entityManager->persist($traceExpe);
            }
            $this->entityManager->flush();
            $retour['codeRetour'] = 0;
            $retour['libRetour']  = $this->translator->trans("Le tracé a été correctement supprimé.", [], 'outils');
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Problème survenu : " . $exception->getMessage());
        }
        
        return new JsonResponse($retour);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/tracing', name: 'tracing', methods: ['POST'])]
    public function tracingExpe(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        $data  = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $mapId = $data['mapId'];
        $coord = $data['coord'];
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : " . $mapId);
            
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($mapId)) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas tracer pour une ville dont vous ne faites pas partis.", [],
                                         'outils');
            
            return new JsonResponse($retour);
        }
        
        $traceExpedition = new TraceExpedition();
        
        if (!is_array($coord)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Les coordonnées fournis ne correspondent pas au formalisme.");
            
            return new JsonResponse($retour);
        } else {
            $traceExpedition->setCoordonnee($coord);
        }
        
        $tabExpe = $this->expeditionHandler->calculExpedition($ville, $traceExpedition->getCoordonnee());
        $pa      = $this->expeditionHandler->calculPAExpedition($traceExpedition->getCoordonnee());
        
        $retour['codeRetour']          = 0;
        $retour['zoneRetour']['trace'] = $tabExpe;
        $retour['zoneRetour']['pa']    = $pa;
        
        return new JsonResponse($retour);
    }
    
    #[Route('/verrouExpedition', name: 'verrouExpedition', methods: ['POST'])]
    public function verouilleExpedition(Request $request): JsonResponse
    {
        try {
            $mapIdSession = (int)($request->headers->get('gh-mapId') ?? 0);
            // On vérifie que le joueur est bien connecté
            $this->userHandler->verifyUserIsConnected();
            
            $ville = $this->userHandler->getTownBySession($mapIdSession);
            
            if ($ville === null) {
                throw new GestHordesException("Le numéro de la ville est incorrecte.");
            }
            
            $dataVerrou = new VerrouillageExpedition();
            $this->serializerService->deserialize($request->getContent(), VerrouillageExpedition::class, 'json',
                                                  $dataVerrou, ['outils_expe']);
            
            // Controle de cohérence entre l'utilisateur et la ville
            if ($mapIdSession !== $dataVerrou->getMapId() ||
                $dataVerrou->getIdUser() !== $this->user->getId()) {
                throw new GestHordesException("Incohérence entre les données, et le contexte. - {verouillage : " .
                                              $dataVerrou->getMapId() . " - " . $dataVerrou->getIdUser() .
                                              " / contexte : " .
                                              $mapIdSession . " - " . $this->user->getId() . "}");
            }
            
            if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($ville->getMapId())) {
                throw new GestHordesException("Vous ne pouvez pas éditer pour une ville dont vous ne faites pas partis.");
            }
            
            $this->expeditionHandler->majVerrouillageExpedition($dataVerrou, $ville);
            
            return new JsonResponse([], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'ExpeRestController', 'verouilleExpedition');
        }
    }
}