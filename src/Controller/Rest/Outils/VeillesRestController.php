<?php

namespace App\Controller\Rest\Outils;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Outils;
use App\Entity\OutilsVeille;
use App\Security\Voter\InVilleVoter;
use DateInterval;
use DateTime;
use Exception;
use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/rest/v1/outils/veilles', name: 'rest_outils_veilles_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class VeillesRestController extends AbstractRestGestHordesController
{
    /**
     * @throws JsonException
     */
    #[Route(path: '/', name: 'outil_veilles', methods: ['GET'])]
    public function main(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        // && !$this->isGranted('ROLE_BETA')
        if ($_ENV['TRANSITION_V1_V2'] === "true") {
            $retour['codeRetour'] = 3;
            $retour['zoneRetour'] = [
                'outils'  => [
                    'text'    => $this->translator->trans("Vous pouvez accéder à l'ancienne version à partir d'ici afin de pouvoir utiliser les outils {titre}. Vous pourrez réaliser une mise à jour de l'ancienne version afin d'obtenir des données, certains outils devront être mis à jour tel que les plans de chantiers pour pouvoir utiliser pleinement l'ancien site.",
                                                          ['{titre}' => 'Veilles'], 'app'),
                    'url'     => $_ENV['URLV1'],
                    'userKey' => $this->user->getApiToken(),
                    'btn'     => $this->translator->trans("Aller vers l'ancien site", [], 'app'),
                ],
                'general' => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
        } else {
            
            $this->ville = $this->userHandler->getTownBySession();
            
            if ($this->ville === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
                return new JsonResponse($retour, Response::HTTP_OK);
            }
            
            try {
                $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $this->ville,
                                               $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville",
                                                                        [], 'outils'));
            } catch (Exception $exception) {
                $this->logger->error('VeillesController - deny - ' . $exception->getMessage());
                
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 2;
                $retour['libRetour']  =
                    $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville", [], 'outils');
                $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
                return new JsonResponse($retour, Response::HTTP_OK);
            }
            
            $manager = $this->entityManager;
            
            $outils = $manager->getRepository(Outils::class)->findOneBy(['ville' => $this->ville,
                                                                         'day'   => $this->ville->getJour()]);
            
            if ($outils === null || $outils->getOutilsVeille() === null) {
                
                if ($outils === null) {
                    $outils = new Outils($this->ville, $this->ville->getJour());
                    
                    try {
                        $manager->persist($outils);
                        $manager->flush();
                    } catch (Exception $exception) {
                        $codeErreur           = $this->randomString();
                        $retour['codeRetour'] = 1;
                        $retour['libRetour']  = $this->translator->trans(
                            "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                            ['{code}' => $codeErreur],
                            'app',
                        );
                        $this->logger->error('VeillesController - sauvegarde - ' . $exception->getMessage());
                        return new JsonResponse($retour, Response::HTTP_OK);
                        
                    }
                }
                
                $outilsVeille = new OutilsVeille();
                
                
            } else {
                
                $outilsVeille = $outils->getOutilsVeille();
                
            }
            
            
            if ($this->user->getTokenVeille() === null || ($this->user->getDateExpTokenVeille() !== null &&
                                                           (new DateTime('now')) >
                                                           $this->user->getDateExpTokenVeille())) {
                $this->user->setTokenVeille($this->gh->generateToken())
                           ->setDateExpTokenVeille(dateExpTokenVeille: (new DateTime('now'))->add(new DateInterval('PT4H')));
                
                $this->entityManager->persist($this->user);
                $this->entityManager->flush();
            }
            
            $outilsVeilleTabs = [];
            
            $retour['codeRetour'] = 0;
            $retour['zoneRetour'] = [
                'outilsChantier' => $outilsVeilleTabs,
                'general'        => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        }
        
    }
    
}