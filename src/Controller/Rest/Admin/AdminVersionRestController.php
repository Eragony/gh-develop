<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ContentsVersion;
use App\Entity\NewsSite;
use App\Entity\User;
use App\Entity\VersionsSite;
use App\Repository\ContentsVersionRepository;
use App\Repository\VersionsSiteRepository;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\HtmlCleanerService;
use App\Service\Utils\SerializerService;
use App\Utils\StringManipulation;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/admin/versions', name: 'rest_admin_version_')]
class AdminVersionRestController extends AbstractRestGestHordesController
{
    private const ALLOWED_SORT_FIELDS = ['id', 'titre', 'date_version'];
    
    public function __construct(
        private readonly VersionsSiteRepository    $versionsSiteRepository,
        private readonly ContentsVersionRepository $contentsVersionRepository,
        protected EntityManagerInterface           $entityManager,
        protected UserHandler                      $userHandler,
        protected GestHordesHandler                $gh,
        protected Security                         $security,
        protected TranslatorInterface              $translator,
        protected LoggerInterface                  $logger,
        protected TranslateHandler                 $translateHandler,
        protected SerializerService                $serializerService,
        protected ErrorHandlingService             $errorHandler,
        protected DiscordService                   $discordService,
        private HtmlCleanerService                 $htmlCleaner,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger, $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        $versionNew  = $this->deserializeVersion($request);
        $versionSite = $this->createVersionFromData($versionNew);
        
        $this->entityManager->persist($versionSite);
        $this->entityManager->flush();
        $this->entityManager->refresh($versionSite);
        
        $userEra = $this->entityManager->getRepository(User::class)->findOneBy(['idMyHordes' => $_ENV['ID_ERA']]);
        
        // On crée la news qui va avec la création de la version
        $tag     = $versionSite->getTagName() ? '-' . $versionSite->getTagName() . ($versionSite->getNumTag() ? '.' . $versionSite->getNumTag() : '') : '';
        $newsBdd = $this->entityManager->getRepository(NewsSite::class)->findOneBy(['titre' => "Mise à jour Version {$versionSite->getVersionMajeur()}.{$versionSite->getVersionMineur()}.{$versionSite->getVersionCorrective()}$tag"]);
        
        $news = $newsBdd ?? new NewsSite();
        $news->setTitre("Mise à jour Version {$versionSite->getVersionMajeur()}.{$versionSite->getVersionMineur()}.{$versionSite->getVersionCorrective()}$tag")
             ->setVersion($versionSite)
             ->setAuteur($userEra)
             ->setDateAjout($versionSite->getDateVersion());
        
        $this->entityManager->persist($news);
        $this->entityManager->flush();
        
        return $this->createVersionResponse($versionSite);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(VersionsSite $versionsSite): JsonResponse
    {
        
        // On récupère la news qui va avec la suppression de la version
        $news = $this->entityManager->getRepository(NewsSite::class)->findOneBy(['version' => $versionsSite]);
        
        if ($news) {
            $this->entityManager->remove($news);
        }
        
        $this->entityManager->remove($versionsSite);
        $this->entityManager->flush();
        
        return new JsonResponse(null, 204);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        $filter = $this->getFilterFromRequest($request);
        $range  = $this->getRangeFromRequest($request);
        $sort   = $this->getSortFromRequest($request);
        
        $qb            = $this->createFilteredQueryBuilder($filter, $range, $sort);
        $versionsSites = $qb->getQuery()->getResult();
        
        $totalVersionsAll = $this->versionsSiteRepository->count([]);
        $totalVersions    = count($versionsSites);
        
        return $this->createPaginatedResponse($versionsSites, $totalVersions, $totalVersionsAll);
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listVersions = $this->entityManager->getRepository(VersionsSite::class)->findAll();
            
            $listVersionsArray = [];
            
            foreach ($listVersions as $version) {
                
                // Construction de la clé de version (ex: "2.0.0-Beta.1")
                $versionKey = sprintf(
                    '%d.%d.%d%s%s',
                    $version->getVersionMajeur(),
                    $version->getVersionMineur(),
                    $version->getVersionCorrective(),
                    $version->getTagName() ? '-' . $version->getTagName() : '',
                    $version->getNumTag() ? '.' . $version->getNumTag() : '',
                );
                
                // Initialisation des tableaux pour les contents et contentsFix
                $contentsFeature = [];
                $contentsFix     = [];
                $contentsOther   = [];
                
                // Tri des contents selon leur type
                foreach ($version->getContents() as $content) {
                    if ($content->getTypeContent() === ContentsVersion::FEATURE) {
                        $contentsFeature[] = $content->getIdRelatif();
                    } else if ($content->getTypeContent() === ContentsVersion::FIX) {
                        $contentsFix[] = $content->getIdRelatif();
                    } else {
                        $contentsOther[] = $content->getIdRelatif();
                    }
                }
                
                // Construction du tableau pour cette version
                $versionArray = [
                    'titre'         => $version->getTitre(),
                    'dateVersion'   => $version->getDateVersion()->format('d/m/Y H:i'),
                    'contents'      => $contentsFeature,
                    'contentsFix'   => $contentsFix,
                    'contentsOther' => $contentsOther,
                ];
                
                $listVersionsArray[$versionKey] = $versionArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../changelog.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listVersionsArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                throw new Exception("Erreur lors de l'écriture dans le fichier.");
            }
            
            $contents = $this->entityManager->getRepository(ContentsVersion::class)->findAll();
            
            $listContentsArray = [];
            foreach ($contents as $content) {
                $listContentsArray[$content->getIdRelatif()] = $content->getContents();
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../contentsChangelog.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listContentsArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                throw new Exception("Erreur lors de l'écriture dans le fichier.");
            }
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return $this->errorHandler->handleException($exception);
        }
        
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(VersionsSite $versionsSite): JsonResponse
    {
        return $this->createVersionResponse($versionsSite);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, VersionsSite $versionsSite): JsonResponse
    {
        $versionsNew = $this->deserializeVersion($request);
        $this->updateVersionFromData($versionsSite, $versionsNew);
        
        $this->entityManager->flush();
        
        return $this->createVersionResponse($versionsSite);
    }
    
    private function addNewContents(VersionsSite $versionsSite, array $newContents): void
    {
        $idRelatif = $this->contentsVersionRepository->getLastIdRelatif();
        
        foreach ($newContents as $content) {
            $contentVersion = $this->createContentVersion(++$idRelatif, $content);
            $versionsSite->addContent($contentVersion);
        }
    }
    
    private function applySorting(QueryBuilder $qb, ?array $sort): void
    {
        if (!$sort || count($sort) !== 2) {
            $qb->orderBy('vs.id', 'DESC');
            return;
        }
        
        [$field, $order] = $sort;
        if (!in_array($field, self::ALLOWED_SORT_FIELDS, true)) {
            return;
        }
        
        $field = (new StringManipulation())->underscoreToCamelCase($field);
        $qb->orderBy('vs.' . $field, $order);
    }
    
    private function applyTextSearch(QueryBuilder $qb, string $textSearch): void
    {
        if ($textSearch === '') {
            return;
        }
        
        $orX = $qb->expr()->orX();
        $orX->add($qb->expr()->like('vs.titre', ':titre'));
        $qb->setParameter('titre', '%' . $textSearch . '%');
        
        if (is_numeric($textSearch)) {
            $orX->add($qb->expr()->eq('vs.id', ':id'));
            $qb->setParameter('id', (int)$textSearch);
        }
        
        $qb->andWhere($orX);
    }
    
    private function categorizeContents(iterable $contents): array
    {
        $newContents = [];
        $modContents = [];
        
        foreach ($contents as $content) {
            if ($content->getIdRelatif() === null) {
                $newContents[] = $content;
            } else {
                $modContents[$content->getIdRelatif()] = $content;
            }
        }
        
        return [$newContents, $modContents];
    }
    
    private function createContentVersion(int $idRelatif, ContentsVersion $content): ContentsVersion
    {
        $contentVersion = new ContentsVersion();
        $contentVersion->setIdRelatif($idRelatif)
                       ->setContents($this->htmlCleaner->cleanHtmlParag($content->getContents()))
                       ->setTypeContent($content->getTypeContent());
        
        return $contentVersion;
    }
    
    private function createFilteredQueryBuilder(array $filter, array $range, ?array $sort): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('vs')
           ->from(VersionsSite::class, 'vs')
           ->setFirstResult($range[0])
           ->setMaxResults($range[1] - $range[0] + 1);
        
        $this->applyTextSearch($qb, $filter['q'] ?? '');
        $this->applySorting($qb, $sort);
        
        return $qb;
    }
    
    private function createPaginatedResponse(array $versions, int $totalVersions, int $totalVersionsAll): JsonResponse
    {
        $serialized = $this->serializerService->serialize($versions, 'json', ['changelog']);
        $response   = new JsonResponse($serialized, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalVersions/$totalVersionsAll");
        
        return $response;
    }
    
    private function createVersionFromData(VersionsSite $versionNew): VersionsSite
    {
        $versionSite = new VersionsSite();
        $this->updateVersionMetadata($versionSite, $versionNew);
        
        $idRelatif = $this->contentsVersionRepository->getLastIdRelatif();
        
        foreach ($versionNew->getContents() as $content) {
            $contentVersion = $this->createContentVersion(++$idRelatif, $content);
            $versionSite->addContent($contentVersion);
        }
        
        return $versionSite;
    }
    
    private function createVersionResponse(VersionsSite $versionSite): JsonResponse
    {
        $serialized = $this->serializerService->serialize($versionSite, 'json', ['changelog']);
        return new JsonResponse($serialized, 200, [], true);
    }
    
    private function deserializeVersion(Request $request): VersionsSite
    {
        $version = new VersionsSite();
        $this->serializerService->deserialize(
            $request->getContent(),
            VersionsSite::class,
            'json',
            $version,
            ['changelog'],
        );
        return $version;
    }
    
    /**
     * @throws JsonException
     */
    private function getFilterFromRequest(Request $request): array
    {
        return json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
    }
    
    /**
     * @throws JsonException
     */
    private function getRangeFromRequest(Request $request): array
    {
        $range = $request->query->get('range');
        return json_decode($range, true, 512, JSON_THROW_ON_ERROR);
    }
    
    /**
     * @throws JsonException
     */
    private function getSortFromRequest(Request $request): ?array
    {
        $sort = $request->query->get('sort');
        return $sort ? json_decode($sort, true, 512, JSON_THROW_ON_ERROR) : null;
    }
    
    private function processExistingContents(VersionsSite $versionsSite, array $modContents): void
    {
        foreach ($versionsSite->getContents() as $content) {
            if (isset($modContents[$content->getIdRelatif()])) {
                $this->updateContent($content, $modContents[$content->getIdRelatif()]);
            } else {
                $this->entityManager->remove($content);
            }
        }
    }
    
    private function updateContent(ContentsVersion $target, ContentsVersion $source): void
    {
        $target->setContents($this->htmlCleaner->cleanHtmlParag($source->getContents()))
               ->setTypeContent($source->getTypeContent());
    }
    
    private function updateVersionFromData(VersionsSite $versionsSite, VersionsSite $versionsNew): void
    {
        $this->updateVersionMetadata($versionsSite, $versionsNew);
        
        [$newContents, $modContents] = $this->categorizeContents($versionsNew->getContents());
        
        $this->processExistingContents($versionsSite, $modContents);
        $this->addNewContents($versionsSite, $newContents);
    }
    
    private function updateVersionMetadata(VersionsSite $target, VersionsSite $source): void
    {
        $target->setVersionMajeur($source->getVersionMajeur())
               ->setVersionMineur($source->getVersionMineur())
               ->setVersionCorrective($source->getVersionCorrective())
               ->setTagName($source->getTagName())
               ->setNumTag($source->getNumTag())
               ->setTitre($source->getTitre())
               ->setDateVersion($source->getDateVersion() ?? new DateTime());
    }
    
}