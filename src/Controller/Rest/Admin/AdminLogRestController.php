<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use DateTime;
use DirectoryIterator;
use SplFileInfo;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/admin/logs', name: 'rest_admin_logs_')]
class AdminLogRestController extends AbstractRestGestHordesController
{
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(ParameterBagInterface $params, TranslatorInterface $trans): JsonResponse
    {
        // Récupérez la configuration du répertoire des logs
        $log_base_dir      = "{$params->get('kernel.project_dir')}/var/log";
        $log_spl_base_path = new SplFileInfo($log_base_dir);
        
        // Fonction pour extraire les informations de tag
        $extract_log_type = function (SplFileInfo $f) use ($log_spl_base_path, $trans): array {
            if ($f->getPathInfo(SplFileInfo::class)->getRealPath() === $log_spl_base_path->getRealPath()) {
                return [
                    'tag'   => 'Kernel',
                    'color' => '#F71735',
                ];
            } else {
                return match ($f->getPathInfo(SplFileInfo::class)->getFilename()) {
                    'update' => [
                        'tag'   => "Nouvelle mise à jour",
                        'color' => '#82846D',
                    ],
                    'admin'  => [
                        'tag'   => "Admin",
                        'color' => '#ff6633',
                    ],
                    default  => [
                        'tag'   => "Inconnu",
                        'color' => '#646165',
                    ],
                };
            }
        };
        
        // Récupérez la liste des fichiers de log
        $log_files = array_map(fn($e) => [
            'info'   => $e,
            'rel'    => $e->getRealPath(),
            'time'   => (new DateTime())->setTimestamp($e->getMTime()),
            'access' => str_replace(['/', '\\'], '::', $e->getRealPath()),
            'tags'   => [$extract_log_type($e)],
        ], $this->list_files($log_base_dir, 'log'));
        
        // Triez les fichiers de log par date décroissante
        usort($log_files, fn($a, $b) => $b['time'] <=> $a['time']);
        
        $page = $this->render(
            'admin/log.html.twig', [
            'logs' => $log_files,],
        );
        
        // Retournez les logs au format JSON
        return $this->json(['logs' => $page->getContent()]);
        
    }
    
    
    /**
     * @param string|string[] $extensions
     * @return SplFileInfo[]
     */
    protected function list_files(string $base_path, $extensions): array
    {
        if (!is_array($extensions)) {
            $extensions = [$extensions];
        }
        
        $result = [];
        
        $paths = [$base_path];
        while (!empty($paths)) {
            $path = array_pop($paths);
            if (!is_dir($path)) {
                continue;
            }
            foreach (new DirectoryIterator($path) as $fileInfo) {
                /** @var SplFileInfo $fileInfo */
                if ($fileInfo->isDot() || $fileInfo->isLink()) {
                    continue;
                } elseif ($fileInfo->isFile() && in_array(strtolower($fileInfo->getExtension()), $extensions)) {
                    $result[] = $fileInfo->getFileInfo(SplFileInfo::class);
                } elseif ($fileInfo->isDir()) {
                    $paths[] = $fileInfo->getRealPath();
                }
            }
        }
        
        return $result;
    }
    
}