<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\CaracteristiquesItem;
use App\Entity\CategoryObjet;
use App\Entity\ItemPrototype;
use App\Entity\ListAssemblage;
use App\Entity\TypeCaracteristique;
use App\Entity\TypeDecharge;
use App\Entity\TypeObjet;
use App\Utils\StringManipulation;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/items', name: 'rest_admin_items_')]
class AdminItemsRestController extends AbstractRestGestHordesController
{
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        //$range      = $request->query->get('range');
        //$range      = json_decode($range, true); // Conversion de la chaîne JSON en tableau PHP
        //$start      = 0;
        //$end        = 9999;
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('i')
           ->from(ItemPrototype::class, 'i');
        //->setFirstResult($start)
        //->setMaxResults($end - $start + 1); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('i.nom', ':objet'));
            $qb->setParameter('objet', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('i.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if (!empty($sort)) {
            $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        } else {
            $sortArray = false;
        }
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'nom', 'update_by_admin', 'actif'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('i.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(ItemPrototype::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->gh->getSerializer()->serialize($items, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen', 'admin_gen_obj'],
        ]);
        
        $data            = json_decode($serializedItems, true, 512, JSON_THROW_ON_ERROR);
        $serializedItems = json_encode($data, JSON_THROW_ON_ERROR);
        
        $response = new JsonResponse($serializedItems, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listObjets = $this->entityManager->getRepository(ItemPrototype::class)->findAllWithDecharge();
            
            $listObjetsArray = [];
            
            foreach ($listObjets as $item) {
                $objetArray = [
                    'id'            => $item->getId(),
                    'nom'           => $item->getNom(),
                    'icon'          => $item->getIcon(),
                    'description'   => $item->getDescription(),
                    'objet_veille'  => $item->getObjetVeille(),
                    'type_objet'    => $item->getTypeObjet()?->getId() ?? null,
                    'category'      => $item->getCategoryObjet()->getId(),
                    'encombrant'    => $item->getEncombrant(),
                    'conteneur'     => $item->getConteneur(),
                    'usageUnique'   => $item->getUsageUnique(),
                    'reparable'     => $item->getReparable(),
                    'defBase'       => $item->getDefBase(),
                    'armurerie'     => $item->getArmurerie(),
                    'magasin'       => $item->getMagasin(),
                    'lanceBete'     => $item->getLanceBete(),
                    'tourelle'      => $item->getTourelle(),
                    'uid'           => $item->getUid(),
                    'idHordes'      => $item->getIdHordes() ?? null,
                    'kill'          => ($item->getKillMax() === null) ? $item->getKillMax() : [$item->getKillMin(), $item->getKillMax()],
                    'chance_kill'   => $item->getChanceKill(),
                    'chance'        => $item->getChance(),
                    'type'          => $item->getType(),
                    'proba'         => $item->getProbaPoubelle(),
                    'actif'         => $item->isActif(),
                    'deco'          => $item->getDeco(),
                    'type_decharge' => $item->getTypeDecharge()?->getId() ?? null,
                    'decharge'      => $item->getDecharge()?->getId() ?? null,
                    'expedition'    => $item->isExpedition() ?? false,
                    'id_mh'         => $item->getIdMh() ?? null,
                ];
                
                
                if ($item->getItemResult()->count() > 1) {
                    foreach ($item->getItemResult() as $itemRes) {
                        $objetArray['id_transform'][] = $itemRes->getId();
                    }
                } else {
                    if (isset($item->getItemResult()[0]) && $item->getItemResult()[0] !== null) {
                        $objetArray['id_transform'] = $item->getItemResult()[0]->getId();
                    }
                }
                
                foreach ($item->getCaracteristiques() as $carac) {
                    $caracArray                       = [
                        'id'          => $carac->getId(),
                        'typeCarac'   => $carac->getTypeCarac()->getId(),
                        'probabilite' => $carac->getProbabilite(),
                        'value'       => $carac->getValue(),
                    ];
                    $objetArray['caracteristiques'][] = $caracArray;
                }
                
                
                $listObjetsArray[] = $objetArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/listObjet.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listObjetsArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            
            // On va traiter les assemblages
            $listAssemblages = $this->entityManager->getRepository(ListAssemblage::class)->findAll();
            
            $listAssemblagesArray = [];
            foreach ($listAssemblages as $assemblage) {
                $itemObtains = [];
                foreach ($assemblage->getItemObtains() as $itemObtain) {
                    $itemObtains[] = [$itemObtain->getItem()->getId(), $itemObtain->getTaux()];
                }
                
                $itemNeeds = [];
                foreach ($assemblage->getItemNeeds() as $itemNeed) {
                    if ($itemNeed->getNumber() > 1) {
                        $itemNeeds[] = [$itemNeed->getItem()->getId(), $itemNeed->getNumber()];
                    } else {
                        $itemNeeds[] = [$itemNeed->getItem()->getId()];
                    }
                }
                
                $assemblageArray = [
                    'item_obtains' => $itemObtains,
                    'item_needs'   => $itemNeeds,
                    'item_pp'      => $assemblage->getItemPrincipal()->getId(),
                    'type_action'  => $assemblage->getTypeAction()->getId(),
                ];
                
                $listAssemblagesArray[] = $assemblageArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePathAss = '../src/DataFixtures/data/listAssemblage.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonStringAss = json_encode($listAssemblagesArray,
                                            JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                            JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePathAss, $newJsonStringAss) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
        
    }
    
    #[Route('/{id}', name: 'get_id', requirements: ['id' => '\d+'], methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(ItemPrototype $itemPrototype): JsonResponse
    {
        
        
        $serializedItem = $this->gh->getSerializer()->serialize($itemPrototype, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj', 'admin_gen'],
        ]);
        
        $data           = json_decode($serializedItem, true, 512, JSON_THROW_ON_ERROR);
        $serializedItem = json_encode($data, JSON_THROW_ON_ERROR);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
    #[Route('/typeCarac', name: 'get_type_carac', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function type_carac(Request $request): JsonResponse
    {
        try {
            // Création instance du QueryBuilder
            $qb = $this->entityManager->createQueryBuilder();
            
            $qb->select('tc')
               ->from(TypeCaracteristique::class, 'tc');
            
            // Exécution de la requête et récupération les résultats
            $types = $qb->getQuery()->getResult();
            
            $totalTypes = count($types);
            
            $serializedItems = $this->serializerService->serialize($types, 'json', ['admin_type']);
            
            
            $response = new JsonResponse($serializedItems, 200, [], true);
            $response->headers->set('Content-Range', "0-$totalTypes/$totalTypes");
            
            return $response;
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception);
        }
    }
    
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, ItemPrototype $item): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /**
         * @var ItemPrototype $itemNew
         */
        $itemNew = $this->gh->getSerializer()->deserialize($request->getContent(), ItemPrototype::class, 'json', [
            'groups'                               => ['admin_obj', 'admin_gen'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['typeDecharge', 'typeObjet'],
        ]);
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $item->setId($itemNew->getId())
             ->setNom($itemNew->getNom())
             ->setIcon($itemNew->getIcon())
             ->setDescription($itemNew->getDescription())
             ->setObjetVeille($itemNew->getObjetVeille())
             ->setDefBase($itemNew->getDefBase())
             ->setArmurerie($itemNew->getArmurerie())
             ->setMagasin($itemNew->getMagasin())
             ->setTourelle($itemNew->getTourelle())
             ->setLanceBete($itemNew->getLanceBete())
             ->setEncombrant($itemNew->getEncombrant())
             ->setUsageUnique($itemNew->getUsageUnique())
             ->setReparable($itemNew->getReparable())
             ->setUid($itemNew->getUid())
             ->setIdMh($itemNew->getIdMh())
             ->setConteneur($itemNew->getConteneur())
             ->setActif($itemNew->isActif())
             ->setKillMin($itemNew->getKillMin())
             ->setKillMax($itemNew->getKillMax())
             ->setChanceKill($itemNew->getChanceKill())
             ->setChance($itemNew->getChance())
             ->setType($itemNew->getType())
             ->setDeco($itemNew->getDeco())
             ->setProbaPoubelle($itemNew->getProbaPoubelle());
        
        // On va balayer les caractéristiques venant du front pour mettre à jour, supprimer ou en créer
        $caracBdd = [];
        $caracNew = [];
        foreach ($itemNew->getCaracteristiques() as $carac) {
            if ($carac->getId() !== null) {
                $caracBdd[$carac->getId()] = $carac;
            } else {
                $caracNew[] = $carac;
            }
        }
        
        foreach ($item->getCaracteristiques() as $carac) {
            if (!isset($caracBdd[$carac->getId()])) {
                $item->removeCaracteristique($carac);
            } else {
                $caracUniqueBdd = $caracBdd[$carac->getId()];
                
                // récupération du type de carac
                $typeCaracBdd = $this->entityManager->getRepository(TypeCaracteristique::class)->findOneBy(['id' => $caracUniqueBdd->getTypeCarac()->getId()]);
                
                if ($typeCaracBdd !== null) {
                    $carac->setTypeCarac($typeCaracBdd);
                } else {
                    throw new Exception("Type de caractéristique introuvable");
                }
                
                $carac->setProbabilite($caracUniqueBdd->getProbabilite())
                      ->setValue($caracUniqueBdd->getValue());
            }
        }
        
        // On balaye les nouvelles carac
        foreach ($caracNew as $key => $carac) {
            $caracteristique = new CaracteristiquesItem();
            // récupération du dernier id disponible pour la création de l'id
            $caracteristique->setId($this->entityManager->getRepository(CaracteristiquesItem::class)->getLastId() + 1 + $key);
            $typeCaracBdd = $this->entityManager->getRepository(TypeCaracteristique::class)->findOneBy(['id' => $carac->getTypeCarac()->getId()]);
            
            if ($typeCaracBdd !== null) {
                $caracteristique->setTypeCarac($typeCaracBdd);
            } else {
                throw new Exception("Type de caractéristique introuvable");
            }
            
            $caracteristique->setProbabilite($carac->getProbabilite())
                            ->setValue($carac->getValue());
            
            $item->addCaracteristique($caracteristique);
        }
        
        
        if ($itemNew->getCategoryObjet() !== null) {
            $categorieObjet = $this->entityManager->getRepository(CategoryObjet::class)
                                                  ->findOneBy(['id' => $itemNew->getCategoryObjet()->getId()]);
            if ($categorieObjet !== null) {
                $item->setCategoryObjet($categorieObjet);
            }
        }
        
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        
        if ($data['type_decharge']['id'] !== null) {
            $typeDecharge = $this->entityManager->getRepository(TypeDecharge::class)
                                                ->findOneBy(['id' => $data['type_decharge']['id']]);
            if ($typeDecharge !== null) {
                $item->setTypeDecharge($typeDecharge);
            }
        }
        if ($data['type_objet']['id'] !== null) {
            $typeObjet =
                $this->entityManager->getRepository(TypeObjet::class)->findOneBy(['id' => $data['type_objet']['id']]);
            if ($typeObjet !== null) {
                $item->setTypeObjet($typeObjet);
            }
        }
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($item);
        $this->entityManager->flush();
        
        $serializedItem = $this->gh->getSerializer()->serialize($item, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj', 'admin_gen'],
        ]);
        
        $data           = json_decode($serializedItem, true, 512, JSON_THROW_ON_ERROR);
        $serializedItem = json_encode($data, JSON_THROW_ON_ERROR);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
}