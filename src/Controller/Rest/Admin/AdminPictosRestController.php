<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\PictoPrototype;
use App\Entity\PictoTitrePrototype;
use App\Utils\StringManipulation;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/pictos', name: 'rest_admin_pictos_')]
class AdminPictosRestController extends AbstractRestGestHordesController
{
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = $request->query->get('range');
        $range      =
            json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Conversion de la chaîne JSON en tableau PHP
        $start      = $range[0];
        $end        = $range[1];
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('p')
           ->from(PictoPrototype::class, 'p')
           ->setFirstResult($start)
           ->setMaxResults($end - $start + 1); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('p.name', ':name'));
            $qb->setParameter('name', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('p.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort      = $request->query->get('sort');
        $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'name'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('p.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $pictos = $qb->getQuery()->getResult();
        
        $totalPictosAll = count($this->entityManager->getRepository(PictoPrototype::class)->findAll());
        
        
        $totalPictos = count($pictos);
        
        $serializedPictos = $this->serializerService->serialize($pictos, 'json', ['admin_picto_list']);
        
        $response = new JsonResponse($serializedPictos, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalPictos/$totalPictosAll");
        
        return $response;
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listPictos = $this->entityManager->getRepository(PictoPrototype::class)->findAll();
            
            $listPictosArray = [];
            
            foreach ($listPictos as $picto) {
                $objetArray = [
                    'id'          => $picto->getId(),
                    'name'        => $picto->getName(),
                    'description' => $picto->getDescription(),
                    'rare'        => $picto->getRare(),
                    'community'   => $picto->getCommunity(),
                    'img'         => $picto->getImg(),
                    'uuid'        => $picto->getUuid(),
                    'actif'       => $picto->isActif(),
                    'id_mh'       => $picto->getIdMh(),
                ];
                
                
                // On va traiter les titres
                $titreArray = [];
                foreach ($picto->getTitre() as $titre) {
                    $titreArray[] = [
                        'id'    => $titre->getId(),
                        'nbr'   => $titre->getNbr(),
                        'titre' => $titre->getTitre(),
                    ];
                }
                
                $objetArray['title'] = $titreArray;
                
                
                $listPictosArray[] = $objetArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/pictos.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listPictosArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
        
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(PictoPrototype $pictoPrototype): JsonResponse
    {
        
        $serializedPicto = $this->serializerService->serialize($pictoPrototype, 'json', ['admin_picto']);
        
        return new JsonResponse($serializedPicto, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, PictoPrototype $pictoPrototype): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $pictoNew = new PictoPrototype();
        $this->serializerService->deserialize($request->getContent(), PictoPrototype::class, 'json', $pictoNew, ['admin_picto']);
        
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $pictoPrototype->setActif($pictoNew->isActif())
                       ->setDescription($pictoNew->getDescription())
                       ->setImg($pictoNew->getImg())
                       ->setName($pictoNew->getName())
                       ->setRare($pictoNew->getRare())
                       ->setCommunity($pictoNew->getCommunity());
        
        // On va mettre à jour les titres, on va balayer à la fois l'existant et le nouveau, si on trouve une différence, on va ajouter, modifer ou supprimer
        // On va mettre les nouveaux titres dont on a un id dans un tableau, ceux sans id dans un autre tableau pour les ajouter forcément
        $titreNewWithId    = [];
        $titreNewWithoutId = [];
        foreach ($pictoNew->getTitre() as $titre) {
            if ($titre->getId() === null) {
                $titreNewWithoutId[] = $titre;
            } else {
                $titreNewWithId[$titre->getId()] = $titre;
            }
        }
        
        // On va balayer les anciens titres, si on ne trouve pas le titre dans les nouveaux, on le supprime, sinon on le met à jour
        foreach ($pictoPrototype->getTitre() as $titre) {
            if (!isset($titreNewWithId[$titre->getId()])) {
                $pictoPrototype->removeTitre($titre);
                $this->entityManager->remove($titre);
            } else {
                $titreNew = $titreNewWithId[$titre->getId()];
                $titre->setTitre($titreNew->getTitre());
                $titre->setNbr($titreNew->getNbr());
            }
        }
        
        // On ajoute les nouveaux titres
        foreach ($titreNewWithoutId as $titre) {
            $titreNew = new PictoTitrePrototype();
            
            $titreNew->setNbr($titre->getNbr())
                     ->setTitre($titre->getTitre());
            
            $pictoPrototype->addTitre($titre);
        }
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($pictoPrototype);
        $this->entityManager->flush();
        $this->entityManager->refresh($pictoPrototype);
        
        $serializedPicto = $this->serializerService->serialize($pictoPrototype, 'json', ['admin_picto']);
        
        return new JsonResponse($serializedPicto, 200, [], true);
    }
    
}