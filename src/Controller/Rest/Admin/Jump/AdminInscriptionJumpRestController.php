<?php

namespace App\Controller\Rest\Admin\Jump;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\InscriptionJump;
use App\Entity\TypeVille;
use App\Utils\StringManipulation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/inscription_jump', name: 'rest_admin_inscription_jump_')]
class AdminInscriptionJumpRestController extends AbstractRestGestHordesController
{
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter         = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $id_jump_search = $filter['jump_id'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('ij')
           ->from(InscriptionJump::class, 'ij'); // Calcul du nombre d'éléments à récupérer
        
        if ($id_jump_search !== '') {
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher avec jump_id
            $orX->add($qb->expr()->like('ij.Jump', ':jump'));
            $qb->setParameter('jump', '%' . $id_jump_search . '%');
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if ($sort === null) {
            $sort = '["id","ASC"]';
        }
        $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('ij.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(InscriptionJump::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->serializerService->serialize($items, 'json', ['jump', 'gestion_jump', 'general_res']);
        
        $response = new JsonResponse($serializedItems, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(TypeVille $typeVille): JsonResponse
    {
        $serializedJump = $this->serializerService->serialize($typeVille, 'json', ['jump']);
        
        return new JsonResponse($serializedJump, 200, [], true);
    }
    
}