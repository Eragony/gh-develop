<?php

namespace App\Controller\Rest\Admin\Villes;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\TraceExpedition;
use App\Entity\Ville;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/ville_expedition', name: 'rest_admin_ville_expedition_')]
class AdminVilleExpeditionRestController extends AbstractRestGestHordesController
{
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(TraceExpedition $traceExpedition): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($traceExpedition);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Expedition supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter  = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $villeId = $filter['villeId'] ?? '0';
        
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['id' => $villeId]);
        
        if ($ville === null) {
            $expeditions = [];
        } else {
            $expeditions = $this->entityManager->getRepository(TraceExpedition::class)
                                               ->createQueryBuilder('te')
                                               ->where('te.ville = :ville')
                                               ->setParameter(':ville', $ville)
                                               ->orderBy('te.createdAt', 'DESC')
                                               ->getQuery()->getResult();
        }
        
        
        $totalExpeditionsAll = count($expeditions);
        
        
        $totalExpeditions = count($expeditions);
        
        $serializedExpedition = $this->gh->getSerializer()->serialize($expeditions, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen', 'general_res'],
        ]);
        
        $response = new JsonResponse($serializedExpedition, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalExpeditions/$totalExpeditionsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(TraceExpedition $traceExpedition): JsonResponse
    {
        
        $serializedTrace = $this->gh->getSerializer()->serialize($traceExpedition, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_expe', 'general_res'],
        ]);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, TraceExpedition $traceExpedition): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $traceNew = $this->gh->getSerializer()->deserialize($request->getContent(), TraceExpedition::class, 'json', [
            'groups' => ['admin_expe'],
        ]);
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $traceExpedition->setVisible($traceNew->getVisible())
                        ->setPersonnel($traceNew->isPersonnel())
                        ->setInactive($traceNew->isInactive())
                        ->setCollab($traceNew->getCollab());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($traceExpedition);
        $this->entityManager->flush();
        
        $serializedTrace = $this->gh->getSerializer()->serialize($traceExpedition, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_expe'],
        ]);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
}