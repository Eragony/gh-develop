<?php

namespace App\Controller\Rest\Admin\Villes;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\RuinesCases;
use App\Entity\RuinesPlans;
use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/ville_ruine_case', name: 'rest_admin_ville_ruine_case_')]
class AdminVilleRuineCasesRestController extends AbstractRestGestHordesController
{
    /**
     * @throws JsonException
     */
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter      = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $ruinePlanId = $filter['ruinesId'] ?? '0';
        
        // Ajustement du plan pour le filtre
        $xFiltre = isset($filter['x']) ? $filter['x'] + 7 : null;
        $yFiltre = $filter['y'] ?? null;
        $zFiltre = $filter['z'] ?? null;
        
        $ruinePlan = $this->entityManager->getRepository(RuinesPlans::class)->findOneBy(['ruines' => $ruinePlanId]);
        
        if ($ruinePlan === null) {
            $cases = [];
        } else {
            $queryBuilder = $this->entityManager->getRepository(RuinesCases::class)->createQueryBuilder('rc')
                                                ->where('rc.ruinesPlans = :ruinesPlans')
                                                ->setParameter('ruinesPlans', $ruinePlan);
            
            if ($xFiltre !== null) {
                $queryBuilder->andWhere('rc.x = :xFiltre')->setParameter('xFiltre', $xFiltre);
            }
            if ($yFiltre !== null) {
                $queryBuilder->andWhere('rc.y = :yFiltre')->setParameter('yFiltre', $yFiltre);
            }
            if ($zFiltre !== null) {
                $queryBuilder->andWhere('rc.z = :zFiltre')->setParameter('zFiltre', $zFiltre);
            }
            
            $cases = $queryBuilder->getQuery()->getResult();
        }
        
        
        $totalCasesAll = count($cases);
        
        
        $serializedCases = $this->serializerService->serialize($cases, 'json', ['admin_ruine', 'admin_gen']);
        
        $response = new JsonResponse($serializedCases, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalCasesAll/$totalCasesAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(RuinesCases $ruinesCases): JsonResponse
    {
        $serializedRuine = $this->serializerService->serialize($ruinesCases, 'json', ['admin_ruine', 'admin_gen', 'general_res']);
        
        return new JsonResponse($serializedRuine, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, RuinesCases $ruinesCases): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet RuinesCases
        $ruineNew = new RuinesCases();
        $this->serializerService->deserialize($request->getContent(), RuinesCases::class, 'json', $ruineNew, ['admin_ruine']);
        
        // On met à jour la case
        $ruinesCases->setTypeCase($ruineNew->getTypeCase())
                    ->setTypeEscalier($ruineNew->getTypeEscalier())
                    ->setTypePorte($ruineNew->getTypePorte())
                    ->setNbrZombie($ruineNew->getNbrZombie());
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($ruinesCases);
        $this->entityManager->flush();
        
        $serializedTrace = $this->serializerService->serialize($ruinesCases, 'json', ['admin_ruine', 'admin_gen', 'general_res']);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
}