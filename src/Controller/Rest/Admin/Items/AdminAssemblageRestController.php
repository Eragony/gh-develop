<?php

namespace App\Controller\Rest\Admin\Items;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ItemNeed;
use App\Entity\ItemProbability;
use App\Entity\ItemPrototype;
use App\Entity\ListAssemblage;
use App\Entity\TypeActionAssemblage;
use App\Utils\StringManipulation;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/items_assemblage', name: 'rest_admin_items_assemblage_')]
class AdminAssemblageRestController extends AbstractRestGestHordesController
{
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $assemblage = new ListAssemblage();
        
        $this->serializerService->deserialize($request->getContent(), ListAssemblage::class, 'json', $assemblage, ['admin_ass']);
        
        $assemblageCreate = new ListAssemblage();
        
        // Pour chaque item need, on va rechercher l'itemPrototype pour la création
        foreach ($assemblage->getItemNeed() as $need) {
            // On verifie avant si l'item need n'existe pas déjà
            $itemNeedBdd = $this->entityManager->getRepository(ItemNeed::class)->findOneBy(['id' => $need->getId()]);
            if ($itemNeedBdd !== null) {
                $assemblageCreate->addItemNeed($itemNeedBdd);
            } else {
                $itemProtoNeed = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $need->getItem()->getId()]);
                if ($itemProtoNeed !== null) {
                    $needCreated = new ItemNeed($itemProtoNeed, $need->getNumber());
                    $assemblageCreate->addItemNeed($needCreated);
                }
            }
        }
        // Pour chaque item obtenu, on va rechercher l'itemPrototype pour la création
        foreach ($assemblage->getItemObtain() as $obtain) {
            // On verifie avant si l'item obtenu n'existe pas déjà en bdd
            $itemObtainBdd = $this->entityManager->getRepository(ItemProbability::class)->findOneBy(['id' => $obtain->getId()]);
            if ($itemObtainBdd !== null) {
                $assemblageCreate->addItemObtain($itemObtainBdd);
            } else {
                $itemProtoObtain = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $obtain->getItem()->getId()]);
                if ($itemProtoObtain !== null) {
                    $obtainCreated = new ItemProbability($itemProtoObtain, $obtain->getTaux());
                    $assemblageCreate->addItemObtain($obtainCreated);
                }
                
            }
            
        }
        
        // Récupération du type d'action
        $typeAction = $this->entityManager->getRepository(TypeActionAssemblage::class)->findOneBy(['id' => $assemblage->getTypeAction()->getId()]);
        if ($typeAction !== null) {
            $assemblageCreate->setTypeAction($typeAction);
        }
        
        // Récupération de l'item principal
        $itemPrincipal = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $assemblage->getItemPrincipal()->getId()]);
        if ($itemPrincipal !== null) {
            $assemblageCreate->setItemPrincipal($itemPrincipal);
        }
        
        // Calcul de l'id pour la création
        $assemblageCreate->setId($this->entityManager->getRepository(ListAssemblage::class)->getLastId() + 1);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($assemblageCreate);
        $this->entityManager->flush();
        $this->entityManager->refresh($assemblageCreate);
        
        $serializedItem = $this->serializerService->serializeArray($assemblageCreate, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 201, []);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, ListAssemblage $listAssemblage): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($listAssemblage);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Assemblage supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('la')
           ->from(ListAssemblage::class, 'la')
           ->leftJoin('la.itemPrincipal', 'ip', Join::WITH, 'la.itemPrincipal = ip.id'); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('ip.nom', ':objet'));
            $qb->setParameter('objet', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('la.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if ($sort !== null) {
            $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
            if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
                // Analyse du paramètre de tri (qui peut être au format 'field,order')
                [$field, $order] = $sortArray;
                
                // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
                $allowedFields = ['id'];
                if (in_array($field, $allowedFields)) {
                    // On convertit le field fournis en camelCase
                    $field = (new StringManipulation())->underscoreToCamelCase($field);
                    // On ajoute le tri à la requête Doctrine
                    $qb->orderBy('la.' . $field, $order);
                }
            }
        }
        
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(ListAssemblage::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->serializerService->serializeArray($items, 'json', ['admin_ass_list']);
        
        $response = new JsonResponse($serializedItems, 200, []);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(ListAssemblage $listAssemblage): JsonResponse
    {
        $assemblage     = $this->entityManager->getRepository(ListAssemblage::class)->findOneBy(['id' => $listAssemblage->getId()]);
        $serializedItem = $this->serializerService->serializeArray($assemblage, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 200, []);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, ListAssemblage $assemblage): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $assemblageNew = new ListAssemblage();
        
        $this->serializerService->deserialize($request->getContent(), ListAssemblage::class, 'json', $assemblageNew, ['admin_ass']);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        // On va vider l'assemblage de ses items need et obtains
        $assemblage->removeItemNeedAll();
        $assemblage->removeItemObtainAll();
        
        // Pour chaque item need, on va rechercher l'itemPrototype pour la création
        foreach ($assemblageNew->getItemNeed() as $need) {
            // On va recalculer l'id de l'item pour la modification ou création car si le nombre change, l'id change
            $newId = $need->getNumber() * 10000 + $need->getItem()->getId();
            // On verifie avant si l'item need n'existe pas déjà
            $itemNeedBdd = $this->entityManager->getRepository(ItemNeed::class)->findOneBy(['id' => $newId]);
            if ($itemNeedBdd !== null) {
                $assemblage->addItemNeed($itemNeedBdd);
            } else {
                $itemProtoNeed = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $need->getItem()->getId()]);
                if ($itemProtoNeed !== null) {
                    $needCreated = new ItemNeed($itemProtoNeed, $need->getNumber());
                    $assemblage->addItemNeed($needCreated);
                }
            }
        }
        // Pour chaque item obtenu, on va rechercher l'itemPrototype pour la création
        foreach ($assemblageNew->getItemObtain() as $obtain) {
            // On va recalculer l'id de l'item obtenu pour la modification ou création car si le taux change, l'id change
            $newId = $obtain->getTaux() * 10000 + $obtain->getItem()->getId();
            
            // On verifie avant si l'item obtenu n'existe pas déjà en bdd
            $itemObtainBdd = $this->entityManager->getRepository(ItemProbability::class)->findOneBy(['id' => $newId]);
            if ($itemObtainBdd !== null) {
                $assemblage->addItemObtain($itemObtainBdd);
            } else {
                $itemProtoObtain = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $obtain->getItem()->getId()]);
                if ($itemProtoObtain !== null) {
                    $obtainCreated = new ItemProbability($itemProtoObtain, $obtain->getTaux());
                    $assemblage->addItemObtain($obtainCreated);
                }
                
            }
            
        }
        
        // Récupération du type d'action
        $typeAction = $this->entityManager->getRepository(TypeActionAssemblage::class)->findOneBy(['id' => $assemblageNew->getTypeAction()->getId()]);
        if ($typeAction !== null) {
            $assemblage->setTypeAction($typeAction);
        }
        
        // Récupération de l'item principal
        $itemPrincipal = $this->entityManager->getRepository(ItemPrototype::class)->findOneBy(['id' => $assemblageNew->getItemPrincipal()->getId()]);
        if ($itemPrincipal !== null) {
            $assemblage->setItemPrincipal($itemPrincipal);
        }
        
        $this->entityManager->persist($assemblage);
        $this->entityManager->flush();
        $this->entityManager->refresh($assemblage);
        $serializedItem = $this->serializerService->serializeArray($assemblage, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 201, []);
    }
    
}