<?php

namespace App\Controller\Rest\Admin\Chantiers;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ChantierPrototype;
use App\Entity\TypeDecharge;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/chantier_decharge', name: 'rest_admin_chantier_decharge_')]
class AdminDechargeRestController extends AbstractRestGestHordesController
{
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /**
         * @var TypeDecharge $dechargeNew
         */
        $dechargeNew = $this->gh->getSerializer()->deserialize($request->getContent(), TypeDecharge::class, 'json', [
            'groups'                               => ['admin_ch'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
        ]);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        // On verifie d'abord si l'id n'existe pas
        if ($this->entityManager->getRepository(TypeDecharge::class)->findOneBy(['id' => $dechargeNew->getId()]) !==
            null) {
            return $this->json(['message' => 'Conflit détecté : Catégorie déjà existant'], Response::HTTP_CONFLICT);
        }
        
        // On récupère le chantier
        $chantierProto = $this->entityManager->getRepository(ChantierPrototype::class)
                                             ->findOneBy(['id' => $dechargeNew->getChantier()
                                                                              ->getId()]);
        if ($chantierProto === null) {
            return $this->json(['message' => 'Chantier non existant'], Response::HTTP_BAD_REQUEST);
        }
        
        $typeDechargeNew = new TypeDecharge($dechargeNew->getId());
        $typeDechargeNew->setChantier($chantierProto);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($typeDechargeNew);
        $this->entityManager->flush();
        
        $serializedItem = $this->gh->getSerializer()->serialize($typeDechargeNew, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_ch'],
        ]);
        
        return new JsonResponse($serializedItem, 201, [], true);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, TypeDecharge $typeDecharge): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($typeDecharge);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Décharge supprimée']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('td')
           ->from(TypeDecharge::class, 'td'); // Calcul du nombre d'éléments à récupérer
        
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(TypeDecharge::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->gh->getSerializer()->serialize($items, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_ch'],
        ]);
        
        $response = new JsonResponse($serializedItems, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(TypeDecharge $typeDecharge): JsonResponse
    {
        
        $serializedItem = $this->gh->getSerializer()->serialize($typeDecharge, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj'],
        ]);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, TypeDecharge $typeDecharge): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /**
         * @var TypeDecharge $dechargeNew
         */
        $dechargeNew = $this->gh->getSerializer()->deserialize($request->getContent(), TypeDecharge::class, 'json', [
            'groups'                               => ['admin_ch'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
        ]);
        
        // Récupération du chantier
        $chantierDecharge = $this->entityManager->getRepository(ChantierPrototype::class)
                                                ->findOneBy(['id' => $dechargeNew->getChantier()
                                                                                 ->getId()]);
        
        if ($chantierDecharge === null) {
            return $this->json(['message' => 'Chantier non existant'], Response::HTTP_BAD_REQUEST);
        }
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        $typeDecharge->setChantier($chantierDecharge);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($typeDecharge);
        $this->entityManager->flush();
        
        $serializedItem = $this->gh->getSerializer()->serialize($typeDecharge, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_ch'],
        ]);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
}