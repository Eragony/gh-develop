<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\BatPrototype;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use App\Entity\PictoPrototype;
use App\Entity\RessourceChantier;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\MyHordesApi;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\Api\MH\BatsMajPrototype;
use App\Structures\Dto\Api\MH\ChantiersMajPrototype;
use App\Structures\Dto\Api\MH\ItemsMajPrototype;
use App\Structures\Dto\Api\MH\PictosMajPrototype;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/admin', name: 'rest_admin_')]
class AdminRestController extends AbstractRestGestHordesController
{
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected MyHordesApi            $myHordesApi,
        protected ChantiersHandler       $chantiersHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/cache', name: 'cache_clear', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function cache_clear(): JsonResponse
    {
        try {
            // Vider le cache
            $process = new Process(['php', 'bin/console', 'cache:clear']);
            $process->setWorkingDirectory($_ENV['CHEMIN_CACHE']);
            $process->run();
            
            // Vérifier si la commande s'est exécutée correctement
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            
            $output = $process->getOutput();
            
            $this->discordService->generateMessageLogDiscord($output);
            
            // Retourner la sortie de la commande
            return new JsonResponse(['output' => $output]);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception);
        } catch (GuzzleException $e) {
            return $this->errorHandler->handleException($e);
        }
    }
    
    #[Route('/general', name: 'general', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function general(Request $request): JsonResponse
    {
        $jsonString = file_get_contents('../paramGeneraux.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $response = new JsonResponse([
                                         'generaux' => [
                                             'lienMHE'      => $data['lienMHE'],
                                             'lienMHO'      => $data['lienMHO'],
                                             'lienGit'      => $data['lienGit'],
                                             'lienCrowdin'  => $data['lienCrowdin'],
                                             'versionJeu'   => $data['versionJeu'],
                                             'lienDiscord'  => $data['lienDiscord'],
                                             'habilitation' => $data['habilitation'],
                                             'miseEnAvant'  => $data['miseEnAvant'],
                                         ],
                                     ],
                                     200,
                                     [],
                                     false);
        $response->headers->set('Content-Range', "0-0/0");
        
        return $response;
    }
    
    #[Route('/stats', name: 'stats', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        $statTheme = $this->entityManager->getRepository(User::class)->statistiqueTheme();
        
        $themeArray = array_map(fn($i) => $i['theme'], $statTheme);
        $nbrArray   = array_map(fn($i) => $i['nbr'], $statTheme);
        
        
        $countStat = $this->entityManager->getRepository(Ville::class)->createQueryBuilder('v')
                                         ->select('COUNT(v.countMaj) as totalCountMaj',
                                                  'AVG(v.countMaj) as averageCountMaj',
                                                  'AVG(v.countMajScript) as averageCountMajScript ')
                                         ->where('v.countMaj != :zero')
                                         ->setParameter('zero', 0)
                                         ->getQuery()
                                         ->getSingleResult();
        
        
        $response =
            new JsonResponse(['stat' => ['theme' => $themeArray, 'nbr' => $nbrArray, 'stats_count' => $countStat]], 200,
                             [], false);
        $response->headers->set('Content-Range', "0-0/0");
        
        return $response;
    }
    
    #[Route(path: '/majParent_chantier', name: 'majParent_chantier', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majParent_chantier(Request $request): JsonResponse
    {
        $majChantiers =
            new ChantiersMajPrototype(json_decode($this->myHordesApi->getChantiers($this->user->getApiToken()), true,
                                                  512, JSON_THROW_ON_ERROR));
        
        $chantierBdd = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        
        $chantierBddUid =
            array_combine(array_map(fn(ChantierPrototype $i) => $i->getUid(), $chantierBdd), $chantierBdd);
        
        $listChantier =
            $this->entityManager->getRepository(ChantierPrototype::class)->createQueryBuilder('c', 'c.idMh')->getQuery()
                                ->getResult();
        
        try {
            foreach ($majChantiers->getChantiers() as $key => $chantier) {
                
                if (isset($chantierBddUid[$key])) {
                    /**
                     * @var ChantierPrototype $chantierMaj
                     */
                    $chantierMaj = $chantierBddUid[$key];
                    if ($chantier->getParent() !== 0) {
                        $chantierMaj->setParent($listChantier[$chantier->getParent()]);
                    }
                    
                    
                    $this->entityManager->persist($chantierMaj);
                    $this->entityManager->flush();
                }
                
                
            }
            
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des chantiers. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour chantiers prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
        
        
        $listChantierParent = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        
        
        try {
            foreach ($listChantierParent as $chantier) {
                $chantier->setCatChantier($this->chantiersHandler->recurciveDeterministeCategory($chantier));
                $this->entityManager->persist($chantier);
                $this->entityManager->flush();
            }
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des chantiers. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour chantiers prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
        
        return new JsonResponse(['codeRetour' => 0, 'libRetour' => 'Mise à jour des chantiers effectuée'], 200, [],
                                false);
        
    }
    
    #[Route(path: '/majPrototype_bats', name: 'majPrototype_bats', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majPrototype_bats(Request $request): JsonResponse
    {
        $majBats = new BatsMajPrototype(json_decode($this->myHordesApi->getBats($this->user->getApiToken()), true, 512,
                                                    JSON_THROW_ON_ERROR));
        $batsBdd = $this->entityManager->getRepository(BatPrototype::class)->findAllIndexed();
        
        try {
            foreach ($majBats->getBats() as $key => $bat) {
                
                if (isset($batsBdd[$bat->getId()])) {
                    /**
                     * @var BatPrototype $batMaj
                     */
                    $batMaj = $batsBdd[$bat->getId()];
                    
                    $batMaj->setIdMh($bat->getId())
                           ->setNom($bat->getName())
                           ->setDescription($bat->getDesc());
                    
                    preg_match('/^ruin\/(.*?)\./', $bat->getImg(), $matches);
                    $batMaj->setIcon($matches[1]);
                    
                    $this->entityManager->persist($batMaj);
                    $this->entityManager->flush();
                    
                } else {
                    
                    
                    $batMaj = new BatPrototype();
                    $batMaj->setId($bat->getId())
                           ->setIdMh($bat->getId())
                           ->setNom($bat->getName())
                           ->setDescription($bat->getDesc())
                           ->setBonusCamping(0)
                           ->setKmMax(30)
                           ->setKmMin(0)
                           ->setActif(false)
                           ->setExplorable($bat->getExplorable());
                    
                    preg_match('/^ruin\/(.*?)\./', $bat->getImg(), $matches);
                    $batMaj->setIcon($matches[1]);
                    
                    $this->entityManager->persist($batMaj);
                    $this->entityManager->flush();
                }
                
            }
            
            return new JsonResponse(['codeRetour' => 0, 'libRetour' => 'Mise à jour des bâtiments effectuée'], 200, [],
                                    false);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des bats. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour bats prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
    }
    
    #[Route(path: '/majPrototype_chantier', name: 'majPrototype_chantier', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majPrototype_chantier(Request $request): JsonResponse
    {
        $majChantiers =
            new ChantiersMajPrototype(json_decode($this->myHordesApi->getChantiers($this->user->getApiToken()), true,
                                                  512, JSON_THROW_ON_ERROR));
        
        $chantierBdd = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        
        $chantierBddUid =
            array_combine(array_map(fn(ChantierPrototype $i) => $i->getUid(), $chantierBdd), $chantierBdd);
        
        $itemsBdd = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
        
        $itemsBddMh = array_combine(array_map(fn(ItemPrototype $i) => $i->getIdMh(), $itemsBdd), $itemsBdd);
        
        try {
            foreach ($majChantiers->getChantiers() as $key => $chantier) {
                
                if (isset($chantierBddUid[$key])) {
                    /**
                     * @var ChantierPrototype $chantierMaj
                     */
                    $chantierMaj = $chantierBddUid[$key];
                    
                    
                } else {
                    
                    
                    $chantierMaj = new ChantierPrototype();
                    $chantierMaj->setid($this->entityManager->getRepository(ChantierPrototype::class)
                                                            ->lastId());
                    $chantierMaj->setUid($key)
                                ->setActif(false)
                                ->setWater(0)
                                ->setNiveau(0)
                                ->setRuineBu(false)
                                ->setRuineHo(false)
                                ->setRuineHs(false)
                                ->setOrderby(0)
                                ->setOrderByListing(1)
                                ->setLevelMax(0);
                    
                }
                
                $chantierMaj->setIdMh($chantier->getId())
                            ->setNom($chantier->getName())
                            ->setDescription($chantier->getDesc())
                            ->setPlan($chantier->getRarity())
                            ->setPv($chantier->getMaxLife())
                            ->setDef($chantier->getDef())
                            ->setPa($chantier->getPa())
                            ->setTemp($chantier->getTemporary())
                            ->setIndes($chantier->getIndes());
                
                /* On travaille sur les ressources */
                $ressourcesTab = $chantier->getRessources();
                
                $ressourcesAct = $chantierMaj->getRessources();
                
                
                foreach ($ressourcesAct as $ress) {
                    if (isset($ressourcesTab[$ress->getItem()->getIdMh()])) {
                        $ress->setNombre($ressourcesTab[$ress->getItem()->getIdMh()]->getAmount());
                        unset($ressourcesTab[$ress->getItem()->getId()]);
                    } else {
                        $chantierMaj->removeRessource($ress);
                    }
                }
                
                
                foreach ($ressourcesTab as $key2 => $rsc) {
                    if ($rsc->getAmount() === '0') {
                        break;
                    }
                    
                    $ressource = new RessourceChantier();
                    $ressource->setNombre((int)$rsc->getAmount())
                              ->setItem($itemsBddMh[$key2]);
                    
                    $chantierMaj->addRessource($ressource);
                }
                
                
                preg_match('/^building\/(.*?)\./', $chantier->getImg(), $matches);
                $chantierMaj->setIcon($matches[1]);
                
                $this->entityManager->persist($chantierMaj);
                $this->entityManager->flush();
                
            }
            
            return new JsonResponse(['codeRetour' => 0, 'libRetour' => 'Mise à jour des chantiers effectuée'], 200, [],
                                    false);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des chantiers. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour chantiers prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
    }
    
    #[Route(path: '/majPrototype_items', name: 'majPrototype_item', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majPrototype_item(Request $request): JsonResponse
    {
        $categorieBdd   = $this->entityManager->getRepository(CategoryObjet::class)->findAll();
        $categorieArray = [];
        foreach ($categorieBdd as $cat) {
            $categorieArray[$cat->getNom()] = $cat;
        }
        
        $majItems =
            new ItemsMajPrototype(json_decode($this->myHordesApi->getItems($this->user->getApiToken()), true, 512,
                                              JSON_THROW_ON_ERROR));
        
        $itemsBdd = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
        
        $itemsBddUid = array_combine(array_map(fn(ItemPrototype $i) => $i->getUid(), $itemsBdd), $itemsBdd);
        
        $catInactif = $this->entityManager->getRepository(CategoryObjet::class)->findOneBy(['id' => 10]);
        
        try {
            foreach ($majItems->getItems() as $key => $item) {
                preg_match('/^item\/(.*?)\./', $item->getImg(), $matches);
                
                $iconeItem = $matches[1];
                
                if (isset($itemsBddUid[$key])) {
                    /**
                     * @var ItemPrototype $itemMaj
                     */
                    $itemMaj = $itemsBddUid[$key];
                    
                    if ($item->getId() != $itemMaj->getIdMh()) {
                        $itemMaj->setUpdateByAdmin(true);
                    }
                    
                    if ($item->getName() != $itemMaj->getNom()) {
                        $itemMaj->setUpdateByAdmin(true);
                    }
                    if ($item->getDef() != $itemMaj->getDefBase()) {
                        $itemMaj->setUpdateByAdmin(true);
                    }
                    if ($item->getDesc() != $itemMaj->getDescription()) {
                        $itemMaj->setUpdateByAdmin(true);
                    }
                    if ($iconeItem != $itemMaj->getIcon()) {
                        $itemMaj->setUpdateByAdmin(true);
                    }
                    
                    
                } else {
                    
                    
                    $itemMaj = new ItemPrototype();
                    $itemMaj->setUid($key)
                            ->setId($this->entityManager->getRepository(ItemPrototype::class)->lastId())
                            ->setActif(false)
                            ->setUpdateByAdmin(true);
                    
                }
                
                $itemMaj->setIdMh($item->getId())
                        ->setNom($item->getName())
                        ->setCategoryObjet($categorieArray[$item->getCat()] ?? $catInactif)
                        ->setDefBase($item->getDef())
                        ->setDeco($item->getDeco())
                        ->setEncombrant($item->getEncombrant())
                        ->setDescription($item->getDesc());
                
                if ($item->getDef() !== null && $item->getDef() !== 0) {
                    $itemMaj->setObjetVeille(true);
                }
                if ($item->getDef() === null || ($item->getDef() !== null && $item->getDef() == 0)) {
                    $itemMaj->setObjetVeille(false);
                }
                
                
                $itemMaj->setIcon($iconeItem);
                
                $this->entityManager->persist($itemMaj);
                $this->entityManager->flush();
                
            }
            
            return new JsonResponse(['codeRetour' => 0, 'libRetour' => 'Mise à jour des objets effectuée'], 200, [],
                                    false);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des items. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour items prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
    }
    
    #[Route(path: '/majPrototype_pictos', name: 'majPrototype_pictos', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majPrototype_pictos(Request $request): JsonResponse
    {
        $majPictos =
            new PictosMajPrototype(json_decode($this->myHordesApi->getPictos($this->user->getApiToken()), true, 512,
                                               JSON_THROW_ON_ERROR));
        
        $pictosBdd = $this->entityManager->getRepository(PictoPrototype::class)->findAll();
        
        $pictosBddUid = array_combine(array_map(fn(PictoPrototype $i) => $i->getUuid(), $pictosBdd), $pictosBdd);
        
        try {
            foreach ($majPictos->getPictos() as $key => $picto) {
                
                if (isset($pictosBddUid[$key])) {
                    /**
                     * @var PictoPrototype $pictoMaj
                     */
                    $pictoMaj = $pictosBddUid[$key];
                    
                    
                } else {
                    
                    
                    $pictoMaj = new PictoPrototype();
                    $pictoMaj->setId($this->entityManager->getRepository(PictoPrototype::class)->lastId());
                    $pictoMaj->setUuid($key)
                             ->setActif(false);
                    
                }
                
                $pictoMaj->setIdMh($picto->getId())
                         ->setName($picto->getName())
                         ->setDescription($picto->getDesc())
                         ->setRare($picto->getRare())
                         ->setCommunity($picto->getCommunity());
                
                preg_match('/^pictos\/(.*?)\./', $picto->getImg(), $matches);
                $pictoMaj->setImg($matches[1]);
                
                $this->entityManager->persist($pictoMaj);
                $this->entityManager->flush();
                
            }
            
            return new JsonResponse(['codeRetour' => 0, 'libRetour' => 'Mise à jour des pictos effectuée'], 200, [],
                                    false);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                "Un problème est survenu lors de la mise à jour des pictos. Code erreur :{$codeErreur}";
            $messageLog           =
                "$codeErreur - Problème mise à jour pictos prototype depuis l'api : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return new JsonResponse($retour, 200, [], false);
        }
    }
    
    #[Route('/general', name: 'maj_general', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function maj_general(Request $request): JsonResponse
    {
        try {
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../paramGeneraux.json';
            
            // Récupération du contenu actuel du fichier
            $jsonString = file_get_contents($filePath);
            $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR);
            
            // Récupération du JSON envoyé dans la requête PUT
            $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            
            if ($content === null) {
                return new JsonResponse(['error' => 'Invalid JSON'], 400);
            }
            
            // Mise à jour des données avec celles reçues dans la requête
            // Note: Assurez-vous que les clés dans $content correspondent à celles dans votre fichier JSON
            foreach ($content as $key => $value) {
                $data[$key] = $value;
            }
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse(['error' => 'Failed to update the file'], 500);
            }
            
            // Préparation de la réponse
            $response = new JsonResponse([
                                             'codeRetour' => 0,
                                             'libRetour'  => 'Mise à jour OK',
                                             'generaux'   => [
                                                 'lienMHE'      => $data['lienMHE'] ?? null,
                                                 'lienMHO'      => $data['lienMHO'] ?? null,
                                                 'lienGit'      => $data['lienGit'] ?? null,
                                                 'lienCrowdin'  => $data['lienCrowdin'] ?? null,
                                                 'versionJeu'   => $data['versionJeu'] ?? null,
                                                 'lienDiscord'  => $data['lienDiscord'] ?? null,
                                                 'habilitation' => $data['habilitation'] ?? null,
                                                 'miseEnAvant'  => $data['miseEnAvant'] ?? null,
                                             ],
                                         ],
                                         200);
            
            $response->headers->set('Content-Range', "0-0/0");
            
        } catch (Exception $exception) {
            // Préparation de la réponse
            $response = new JsonResponse([
                                             'codeRetour' => 1,
                                             'libRetour'  => 'Erreur lors de la mise à jour : ' .
                                                             $exception->getMessage(),
                                         ],
                                         500);
        }
        
        return $response;
    }
}