<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\BatPrototype;
use App\Entity\ItemBatiment;
use App\Entity\ItemPrototype;
use App\Utils\StringManipulation;
use Exception;
use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/batiment', name: 'rest_admin_batiment_')]
class AdminBatimentRestController extends AbstractRestGestHordesController
{
    
    /**
     * @throws JsonException
     */
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('b')
           ->from(BatPrototype::class, 'b'); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('b.nom', ':bat'));
            $qb->setParameter('bat', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('b.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort      = $request->query->get('sort');
        $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'nom'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('b.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $bats = $qb->getQuery()->getResult();
        
        $totalBatsAll = count($this->entityManager->getRepository(BatPrototype::class)->findAll());
        
        
        $totalBats = count($bats);
        
        $serializedBatiments = $this->serializerService->serialize($bats, 'json', ['admin_gen']);
        
        $response = new JsonResponse($serializedBatiments, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalBats/$totalBatsAll");
        
        return $response;
    }
    
    #[Route('/maj_json', name: 'post', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(): JsonResponse
    {
        try {
            $listeBatiments = $this->entityManager->getRepository(BatPrototype::class)->findAll();
            
            $lisBatsArray = [];
            
            foreach ($listeBatiments as $batiment) {
                $batArray = [
                    'id'           => $batiment->getId(),
                    'nom'          => $batiment->getNom(),
                    'description'  => $batiment->getDescription(),
                    'kmMin'        => $batiment->getKmMin(),
                    'kmMax'        => $batiment->getKmMax(),
                    'bonusCamping' => $batiment->getBonusCamping(),
                    'explorable'   => $batiment->isExplorable() ? 1 : 0,
                    'icon'         => $batiment->getIcon(),
                    'idMh'         => $batiment->getIdMh(),
                    'idHordes'     => $batiment->getIdHordes(),
                    'actif'        => $batiment->isActif(),
                    'maxCampeur'   => $batiment->getMaxCampeur(),
                ];
                
                $itemsArray = [];
                foreach ($batiment->getItems() as $item) {
                    $itemsArray[] = [
                        'item'  => $item->getItem()->getId(),
                        'count' => $item->getProbabily(),
                    ];
                }
                
                $batArray['items'] = $itemsArray;
                
                
                $lisBatsArray[] = $batArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/bats.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($lisBatsArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(BatPrototype $batiment): JsonResponse
    {
        
        $serializedBatiment = $this->serializerService->serialize($batiment, 'json', ['admin_gen', 'admin_bat']);
        
        return new JsonResponse($serializedBatiment, 200, [], true);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, BatPrototype $bat): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $batNew = new BatPrototype();
        $this->serializerService->deserialize($request->getContent(), BatPrototype::class, 'json', $batNew,
                                              ['admin_bat', 'admin_gen']);
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        
        $bat->setNom($batNew->getNom())
            ->setDescription($batNew->getDescription())
            ->setKmMin($batNew->getKmMin())
            ->setKmMax($batNew->getKmMax())
            ->setBonusCamping($batNew->getBonusCamping())
            ->setExplorable($batNew->isExplorable())
            ->setIcon($batNew->getIcon())
            ->setIdMh($batNew->getIdMh())
            ->setMaxCampeur($batNew->getMaxCampeur())
            ->setActif($batNew->isActif());
        
        
        // On va balayer les items du batiment pour mettre à jour les objets, on va lister les items du batiment modifiés
        $itemNew = [];
        foreach ($batNew->getItems() as $item) {
            $itemNew[$item->getItem()->getId()] = $item;
        }
        
        // On balaye les items du batiment actuel pour voir si il faut les supprimer ou mettre à jour
        foreach ($bat->getItems() as $item) {
            if (isset($itemNew[$item->getItem()->getId()])) {
                // On met à jour la probabilité
                $item->setProbabily($itemNew[$item->getItem()->getId()]->getProbabily());
                unset($itemNew[$item->getItem()->getId()]);
            } else {
                // On supprime l'item
                $this->entityManager->remove($item);
            }
        }
        
        // On ajoute les items restants
        foreach ($itemNew as $item) {
            // On récupère l'item en bdd pour faire l'ajout correctement
            $itemProto = $this->entityManager->getRepository(ItemPrototype::class)->find($item->getItem()->getId());
            
            if ($itemProto !== null) {
                $itemBat = new ItemBatiment();
                
                $itemBat->setItem($itemProto)
                        ->setProbabily($item->getProbabily());
                
                $bat->addItem($itemBat);
            }
        }
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($bat);
        $this->entityManager->flush();
        $this->entityManager->refresh($bat);
        
        $serializedBatiment = $this->serializerService->serialize($bat, 'json', ['admin_gen', 'admin_bat']);
        
        return new JsonResponse($serializedBatiment, 200, [], true);
    }
    
    
}