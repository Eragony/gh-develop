<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutHandler extends LogoutEvent
{
    public function onLogoutSuccess(): Response
    {
        // Supprimer tous les cookies de connexion
        $response = new Response();
        $response->headers->clearCookie('gh_remember_me');
        
        // Ajouter d'autres cookies à supprimer au besoin
        
        return $response;
    }
}