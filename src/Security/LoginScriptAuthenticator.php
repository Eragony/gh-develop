<?php

namespace App\Security;

use App\Entity\User;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Service\Utils\UpdateAPIHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class LoginScriptAuthenticator extends RememberMeSupportingAuthenticator
{
    
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RouterInterface        $router,
        private readonly RequestStack           $requestStack,
        private readonly UpdateAPIHandler       $updateAPIHandler,
    )
    {
        parent::__construct($entityManager);
    }
    
    /**
     * @throws Exception
     */
    public function authenticate(Request $request): Passport
    {
        
        $accessToken = str_replace("/login_script/", "", $request->getRequestUri());
        
        if ($accessToken === null) {
            $userBadge = new UserBadge('null', fn() => null);
            return new Passport($userBadge, new CustomCredentials(fn($credentials, User $user) => false, 0),);
        }
        
        try {
            $myHordesUser = $this->updateAPIHandler->appelApiGestionAttackMaintenance("getMeTown", $accessToken);
            
            $userBadge = new UserBadge($accessToken, function () use ($accessToken, $request, $myHordesUser) {
                
                $user = $this->updateAPIHandler->createUpdateUserMaj($request, $myHordesUser, $accessToken);
                
                //Stockage de l'accessToken en session
                $this->requestStack->getSession()->set('access_token', $accessToken);
                
                return $user;
                
            },
            );
            
            
            return new Passport(
                $userBadge,
                new CustomCredentials(fn($credentials, User $user) => $user->getIdMyHordes() === $credentials,
                    $myHordesUser->id),
            );
        } catch (MyHordesAttackException|MyHordesMajApiException|MyHordesMajException $e) {
            throw new Exception("Erreur dans l'authentification : {$e->getMessage()} - userkey : " . $accessToken);
        }
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?JsonResponse
    {
        
        return new JsonResponse(
            ['retour' => 0,
             'lib'    => "Un problème lors de l'authentification est survenu, veuillez passer par l'annuaire."],
        );
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // Change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('myhordes_maj_script');
        
        $response = new RedirectResponse($targetUrl);
        $response->headers->setCookie(new Cookie('_locale', $request->getSession()->get('_locale'),
                                                 time() + 31_536_000));
        $this->enableRememberMe($token, $response);
        $response->send();
        
        return $response;
    }
    
    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_route') === 'myhordes_login_script';
    }
}
