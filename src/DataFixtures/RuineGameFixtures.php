<?php

namespace App\DataFixtures;

use App\Entity\ItemPrototype;
use App\Entity\RuineGameZonePrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class RuineGameFixtures extends Fixture
{
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $out->writeln('<info>Installation des fixtures: Ruine Game </info>');
        $out->writeln('');
        /* chargement des prototypes */
        
        $jsonString = file_get_contents(__DIR__ . '/data/ruine_prototype_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listRuinePrototypes = $data;
        
        $countRuineProtosSkill = count($listRuinePrototypes);
        
        $out->writeln('<comment>Ruine Prototype : ' . $countRuineProtosSkill . ' fixture entries available.</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start($countRuineProtosSkill);
        $listObjets = $manager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        foreach ($listRuinePrototypes as $key => $ruinePrototype) {
            
            $entity =
                $manager->getRepository(RuineGameZonePrototype::class)->findOneBy(['id' => $ruinePrototype['id']]);
            
            if ($entity === null) {
                $entity = new RuineGameZonePrototype();
                $entity->setId($ruinePrototype['id']);
            }
            
            $entity->setLabel($ruinePrototype['label'])
                   ->setLevelZone($ruinePrototype['level']);
            
            if (isset($ruinePrototype['empreinte'])) {
                $entity->setEmpreinteItem($listObjets[$ruinePrototype['empreinte']]);
            }
            if (isset($ruinePrototype['clef'])) {
                $entity->setEmpreinteItem($listObjets[$ruinePrototype['clef']]);
            }
            if ($ruinePrototype['type'] !== null) {
                $entity->setTypeCase($ruinePrototype['type']);
            }
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
