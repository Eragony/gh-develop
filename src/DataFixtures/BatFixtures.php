<?php

namespace App\DataFixtures;

use App\Entity\BatPrototype;
use App\Entity\ItemBatiment;
use App\Entity\ItemPrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class BatFixtures extends Fixture implements DependentFixtureInterface
{
    
    public static array $tabTransco = [
        -1  => -1,
        2   => 26,
        3   => 4,
        4   => 50,
        5   => 28,
        6   => 34,
        7   => 13,
        8   => 9,
        9   => 60,
        10  => 10,
        11  => 31,
        12  => 32,
        13  => 19,
        14  => 37,
        15  => 51,
        16  => 48,
        17  => 59,
        18  => 25,
        19  => 15,
        20  => 1,
        21  => 33,
        22  => 56,
        23  => 17,
        24  => 5,
        25  => 18,
        27  => 29,
        28  => 24,
        29  => 55,
        30  => 21,
        31  => 54,
        32  => 43,
        33  => 42,
        34  => 57,
        35  => 47,
        36  => 35,
        37  => 30,
        38  => 11,
        39  => 53,
        40  => 23,
        41  => 2,
        42  => 45,
        43  => 16,
        44  => 27,
        45  => 44,
        46  => 41,
        47  => 36,
        48  => 8,
        49  => 49,
        50  => 58,
        51  => 39,
        52  => 20,
        53  => 6,
        54  => 7,
        55  => 40,
        56  => 46,
        57  => 3,
        58  => 38,
        59  => 22,
        60  => 52,
        61  => 12,
        62  => 14,
        100 => 61,
        101 => 62,
        102 => 63,
    ];
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [ObjetFixtures::class];
    }
    
    public function load(ObjectManager $manager):void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        $jsonString = file_get_contents(__DIR__ . '/data/bats.json');
        $listBats   = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listItem = $manager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $out->writeln('<info>Installing fixtures: BatsPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de batiments : ' . count($listBats) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listBats));
        
        foreach ($listBats as $bat) {
            
            $entity = $manager->getRepository(BatPrototype::class)->find((int)$bat['id']);
            
            if ($entity === null) {
                $entity = new BatPrototype();
                $entity->setId((int)$bat['id']);
            }
            
            $entity->setDescription(htmlspecialchars_decode((string)$bat['description']))
                   ->setNom(htmlspecialchars_decode((string)$bat['nom']))
                   ->setBonusCamping((int)$bat['bonusCamping'])
                   ->setKmMin((int)$bat['kmMin'])
                   ->setKmMax((int)$bat['kmMax'])
                   ->setIcon($bat['icon'])
                   ->setMaxCampeur($bat['maxCampeur'] ?? 0)
                   ->setIdHordes(array_flip(BatFixtures::$tabTransco)[(int)$bat['id']])
                   ->setActif(true);
            
            /**
             * @var ItemBatiment[] $listItemBat
             */
            $listItemBat = array_combine(array_map(fn($i) => $i['item'], $bat['items']), $bat['items']);
            
            
            foreach ($entity->getItems() as $itemBat) {
                if (isset($listItemBat[$itemBat->getItem()->getId()])) {
                    $itemBat->setProbabily($listItemBat[$itemBat->getItem()->getId()]['count']);
                    unset($listItemBat[$itemBat->getItem()->getId()]);
                } else {
                    $entity->removeItem($itemBat);
                }
            }
            
            foreach ($listItemBat as $itemBat) {
                $itemBatNew = new ItemBatiment();
                
                $itemBatNew->setItem($listItem[$itemBat['item']])
                           ->setProbabily($itemBat['count']);
                
                $entity->addItem($itemBatNew);
            }
            
            
            if ($bat['explorable']) {
                $entity->setExplorable(true);
            } else {
                $entity->setExplorable(false);
            }
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
