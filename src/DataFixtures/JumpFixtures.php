<?php

namespace App\DataFixtures;

use App\Entity\CreneauHorraire;
use App\Entity\LevelRuinePrototype;
use App\Entity\ObjectifVillePrototype;
use App\Entity\RoleJump;
use App\Entity\StatutInscription;
use App\Entity\StatutUser;
use App\Entity\TypeDispo;
use App\Entity\TypeDispoJump;
use App\Entity\TypeLead;
use App\Entity\TypeVille;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class JumpFixtures extends Fixture
{
    
    public static array $dispo = [
        [
            'id'  => 1,
            'nom' => "Oui",
        ],
        [
            'id'  => 2,
            'nom' => "Peut-être",
        ],
        [
            'id'  => 3,
            'nom' => "Sûrement",
        ],
        [
            'id'  => 4,
            'nom' => "Non",
        ],
    ];
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA_JUMP)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        $jsonString     = file_get_contents(__DIR__ . '/data/lvlRuine.json');
        $listLevelRuine = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString        = file_get_contents(__DIR__ . '/data/objectifVille.json');
        $listObjectifVille =
            json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString   = file_get_contents(__DIR__ . '/data/roleJump.json');
        $listRoleJump = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString = file_get_contents(__DIR__ . '/data/statutAvancement.json');
        $listStatut = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString   = file_get_contents(__DIR__ . '/data/typeLead.json');
        $listTypeLead = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString    = file_get_contents(__DIR__ . '/data/typeVille.json');
        $listTypeVille = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString    = file_get_contents(__DIR__ . '/data/typeDispo.json');
        $listTypeDispo = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString      = file_get_contents(__DIR__ . '/data/typeCreneau.json');
        $listTypeCreneau = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString     = file_get_contents(__DIR__ . '/data/statutJump.json');
        $listStatutJump = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $jsonString = file_get_contents(__DIR__ . '/data/dispo.json');
        $listDispo  = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        
        $out->writeln('<info>Installing fixtures: BatsPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de level Ruine : ' . count($listLevelRuine) . ' .</comment>');
        
        $this->majLvlRuine($manager, $out, $listLevelRuine);
        
        $out->writeln('<comment>Nombre d\'objectif de ville : ' . count($listObjectifVille) . ' .</comment>');
        
        $this->majObjectifVille($manager, $out, $listObjectifVille);
        
        $out->writeln('<comment>Nombre de role jump : ' . count($listRoleJump) . ' .</comment>');
        
        $this->majRoleJump($manager, $out, $listRoleJump);
        
        $out->writeln('<comment>Nombre de statut : ' . count($listStatut) . ' .</comment>');
        
        $this->majStatutAvancement($manager, $out, $listStatut);
        
        $out->writeln('<comment>Nombre de type lead : ' . count($listTypeLead) . ' .</comment>');
        
        $this->majTypeLead($manager, $out, $listTypeLead);
        $out->writeln('<comment>Nombre de type ville : ' . count($listTypeVille) . ' .</comment>');
        
        $this->majTypeVille($manager, $out, $listTypeVille);
        $out->writeln('<comment>Nombre de type dispo : ' . count($listTypeDispo) . ' .</comment>');
        
        $this->majTypeDispo($manager, $out, $listTypeDispo);
        $out->writeln('<comment>Nombre de type creneau : ' . count($listTypeCreneau) . ' .</comment>');
        
        $this->majTypeCreneau($manager, $out, $listTypeCreneau);
        
        $out->writeln('<comment>Nombre de type dispo : ' . count($listDispo) . ' .</comment>');
        
        $this->majDispoJump($manager, $out, $listDispo);
        $out->writeln('<comment>Nombre de type statut joueur : ' . count($listStatutJump) . ' .</comment>');
        
        $this->majStatutJump($manager, $out, $listStatutJump);
        
    }
    
    private function majDispoJump(ObjectManager $manager, ConsoleOutputInterface $output, array $listDispo): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listDispo));
        
        foreach ($listDispo as $dispo) {
            
            $entity = $manager->getRepository(TypeDispoJump::class)->find((int)$dispo['id']);
            
            if ($entity === null) {
                $entity = new TypeDispoJump();
                $entity->setId((int)$dispo['id']);
            }
            
            $entity->setNom(($dispo['nom']));
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majLvlRuine(ObjectManager $manager, ConsoleOutputInterface $output, array $listLevelRuine): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listLevelRuine));
        
        foreach ($listLevelRuine as $lvlRuine) {
            $lvlRuineBdd =
                $manager->getRepository(LevelRuinePrototype::class)->findOneBy(['level' => $lvlRuine['level']]);
            
            if ($lvlRuineBdd === null) {
                $lvlRuineBdd = new LevelRuinePrototype();
                $lvlRuineBdd->setId($lvlRuine['level']);
            }
            
            $lvlRuineBdd->setLevel($lvlRuine['level'])
                        ->setNom($lvlRuine['nom'])
                        ->setDescription($lvlRuine['description']);
            
            $manager->persist($lvlRuineBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majObjectifVille(ObjectManager $manager, ConsoleOutputInterface $output,
                                      array         $listObjectifVille): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listObjectifVille));
        
        foreach ($listObjectifVille as $objectifVille) {
            $objectifVilleBdd =
                $manager->getRepository(ObjectifVillePrototype::class)->findOneBy(['id' => $objectifVille['id']]);
            
            if ($objectifVilleBdd === null) {
                $objectifVilleBdd = new ObjectifVillePrototype();
                $objectifVilleBdd->setId($objectifVille['id']);
            }
            
            $objectifVilleBdd->setNom($objectifVille['nom'])
                             ->setIcon($objectifVille['icon']);
            
            $manager->persist($objectifVilleBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majRoleJump(ObjectManager $manager, ConsoleOutputInterface $output, array $listRoleJump): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listRoleJump));
        
        foreach ($listRoleJump as $role) {
            $roleBdd = $manager->getRepository(RoleJump::class)->findOneBy(['id' => $role['id']]);
            
            if ($roleBdd === null) {
                $roleBdd = new RoleJump();
                $roleBdd->setId($role['id']);
            }
            
            $roleBdd->setNom($role['nom'])
                    ->setIcon($role['icon']);
            
            $manager->persist($roleBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majStatutAvancement(ObjectManager $manager, ConsoleOutputInterface $output,
                                         array         $listStatut): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listStatut));
        
        foreach ($listStatut as $statut) {
            $statutBdd = $manager->getRepository(StatutInscription::class)->findOneBy(['id' => $statut['id']]);
            
            if ($statutBdd === null) {
                $statutBdd = new StatutInscription();
                $statutBdd->setId($statut['id']);
            }
            
            $statutBdd->setNom($statut['nom'])
                      ->setNomGestion($statut['nomGestion'])
                      ->setVisibleCandidature($statut['visibleCandidature'])
                      ->setOrderInGestion($statut['orderInGestion'])
                      ->setNomGestionCourt($statut['nomGestionCourt']);
            
            $manager->persist($statutBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majStatutJump(ObjectManager $manager, ConsoleOutputInterface $output, array $listStatut): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listStatut));
        
        foreach ($listStatut as $statut) {
            
            $entity = $manager->getRepository(StatutUser::class)->find((int)$statut['id']);
            
            if ($entity === null) {
                $entity = new StatutUser();
                $entity->setId((int)$statut['id']);
            }
            
            $entity->setNom(($statut['nom']));
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majTypeCreneau(ObjectManager $manager, ConsoleOutputInterface $output,
                                    array         $listTypeCreneau): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listTypeCreneau));
        
        foreach ($listTypeCreneau as $creneau) {
            $creneauBdd = $manager->getRepository(CreneauHorraire::class)->findOneBy(['id' => $creneau['id']]);
            
            if ($creneauBdd === null) {
                $creneauBdd = new CreneauHorraire();
                $creneauBdd->setId($creneau['id']);
            }
            
            $creneauBdd->setLibelle($creneau['libelle']);
            
            $manager->persist($creneauBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majTypeDispo(ObjectManager $manager, ConsoleOutputInterface $output, array $listTypeDispo): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listTypeDispo));
        
        foreach ($listTypeDispo as $dispo) {
            $dispoBdd = $manager->getRepository(TypeDispo::class)->findOneBy(['id' => $dispo['id']]);
            
            if ($dispoBdd === null) {
                $dispoBdd = new TypeDispo();
                $dispoBdd->setId($dispo['id']);
            }
            
            $dispoBdd->setNom($dispo['libelle'])
                     ->setDescription('');
            
            $manager->persist($dispoBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majTypeLead(ObjectManager $manager, ConsoleOutputInterface $output, array $listTypeLead): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listTypeLead));
        
        foreach ($listTypeLead as $lead) {
            $leadBdd = $manager->getRepository(TypeLead::class)->findOneBy(['id' => $lead['id']]);
            
            if ($leadBdd === null) {
                $leadBdd = new TypeLead();
                $leadBdd->setId($lead['id']);
            }
            
            $leadBdd->setNom($lead['nom'])
                    ->setIcon($lead['icon']);
            
            $manager->persist($leadBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
    
    private function majTypeVille(ObjectManager $manager, ConsoleOutputInterface $output, array $listTypeVille): void
    {
        
        $processBar = new ProgressBar($output->section());
        $processBar->start(count($listTypeVille));
        
        foreach ($listTypeVille as $ville) {
            $villeBdd = $manager->getRepository(TypeVille::class)->findOneBy(['id' => $ville['id']]);
            
            if ($villeBdd === null) {
                $villeBdd = new TypeVille();
                $villeBdd->setId($ville['id']);
            }
            
            $villeBdd->setNom($ville['nom'])
                     ->setAbrev($ville['abrev'])
                     ->setIcon($ville['icon']);
            
            $manager->persist($villeBdd);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $manager->clear();
        
        $processBar->finish();
        $output->writeln('');
    }
}
