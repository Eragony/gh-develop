<?php


namespace App\DataFixtures;


use App\Entity\Ancestor\OldPlanscVille;
use App\Entity\ChantierPrototype;
use App\Entity\PlansChantier;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldToNewPlanFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected DataCollection  $collection,
        protected ConfMaster      $confMaster,
    )
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_OLD_PLAN)) {
            return;
        }
        
        $manager = $this->managerRegistry->getManager();
        
        $out = new ConsoleOutput();
        
        $nombreVille = $manager->getRepository(OldPlanscVille::class)->countMigration()[1] ?? 0;
        
        try {
            $out->writeln('<info>Installing fixtures: plans Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre de ville : ' . $nombreVille . ' .</comment>');
            $out->writeln('');
            
            $this->majPlansVille($manager, $out);
            
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()}</error>");
        }
        
    }
    
    public function majPlansVille(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $plansVilles = $manager->getRepository(OldPlanscVille::class)->findAll();
        
        $out->writeln('<comment>Progression globale / progression ville par ville : </comment>');
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($plansVilles));
        
        foreach ($plansVilles as $plansVille) {
            
            $mapId = (int)$plansVille->getMapId();
            
            $villeExist =
                $manager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
            
            if ($villeExist === null) {
                $processBarGlobale->advance();
                continue;
            }
            
            // User maj
            $userMaj = $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $plansVille->getTwinId()]);
            
            $ville = $villeExist;
            
            // mise à jour info
            
            $ville->setPlansChantierDateMaj($plansVille->getDateMJP());
            $ville->setPlansChantierUpdateBy($userMaj);
            
            //Mise en array des plans obtenus
            
            $arrayTmp = explode('|', (string)$plansVille->getIdC());
            
            $arrayTmp = array_filter($arrayTmp, fn($i) => count(explode('_', $i)) < 2);
            
            $arrayIdChantier = array_map(fn($i) => ChantierFixtures::$tabTransco[$i], $arrayTmp);
            $arrayObt        = explode('|', (string)$plansVille->getPlansC());
            
            foreach ($arrayIdChantier as $key => $idChantier) {
                if ($arrayObt[$key]) {
                    $chantier = $manager->getRepository(ChantierPrototype::class)->find($idChantier);
                    
                    $planVille = new PlansChantier();
                    $planVille->setChantier($chantier);
                    
                    $ville->addPlansChantier($planVille);
                }
            }
            
            $manager->persist($ville);
            $processBarGlobale->advance();
            
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        $out->writeln('');
        
    }
}