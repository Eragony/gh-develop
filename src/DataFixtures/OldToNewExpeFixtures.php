<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldExpeVillem;
use App\Entity\TraceExpedition;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\Utils\ColorGeneratorHandler;
use App\Structures\Conf\GestHordesConf;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class OldToNewExpeFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(protected ConfMaster $confMaster, protected ColorGeneratorHandler $color)
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_OLD_EXPE)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        // Récupération de la totalité des expéditions
        $nbrExpedition = count($manager->getRepository(OldExpeVillem::class)->findAll());
        
        try {
            $out->writeln('<info>Installing fixtures: Outils Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre d\'expéditions : ' . $nbrExpedition . ' .</comment>');
            $out->writeln('');
            
            $this->majExpedition($manager, $out);
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()}</error>");
        }
        
        $manager->flush();
    }
    
    private function majExpedition(ObjectManager $manager, ConsoleOutput $out): void
    {
        
        $out->writeln('<comment>Progression expédition existante : </comment>');
        
        $listExpedition = $manager->getRepository(OldExpeVillem::class)->findAll();
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listExpedition));
        
        foreach ($listExpedition as $expedition) {
            // verification existance de la ville
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $expedition->getMapId(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            if ($ville === null) {
                $processBarGlobale->advance();
                continue;
            }
            
            $citoyenCreateur =
                $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $expedition->getTwinId()]);
            
            if ($expedition->getTwinId() !== $expedition->getTwinIdM()) {
                $citoyenModificateur =
                    $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $expedition->getTwinIdM()]);
            } else {
                $citoyenModificateur = null;
            }
            
            $traces = array_map(fn($i) => explode('-', $i), explode('.', (string)$expedition->getTracesExpe()));
            
            $newExpe = new TraceExpedition();
            
            $newExpe->setVille($ville)
                    ->setNom($expedition->getNomExpe())
                    ->setPa($expedition->getLongExpe())
                    ->setCollab((bool)$expedition->getExpCollab())
                    ->setVisible(true)
                    ->setCreatedAt(new DateTime('NOW'))
                    ->setCreatedBy($citoyenCreateur)
                    ->setCoordonnee($traces)->setJour($expedition->getJourExpe())
                    ->setCouleur($this->color->randomColorHexa());
            
            if ($citoyenModificateur !== null) {
                $newExpe->setModifyAt(new DateTime('NOW'))
                        ->setModifyBy($citoyenModificateur);
            }
            
            $manager->persist($newExpe);
            $processBarGlobale->advance();
            
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        
        $out->writeln('');
        
    }
}
