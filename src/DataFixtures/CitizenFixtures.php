<?php

namespace App\DataFixtures;

use App\Entity\CleanUpCadaver;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\LvlUpHome;
use App\Entity\RessourceHome;
use App\Entity\RessourceUpHome;
use App\Entity\TypeDeath;
use App\Entity\UpHomePrototype;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class CitizenFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(
        protected EntityManagerInterface $em,
        protected ConfMaster             $confMaster,
        protected DataCollection         $collection,
    )
    {
    }
    
    public function getDependencies(): array
    {
        return [ObjetFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        
        $output = new ConsoleOutput();
        
        try {
            $output->writeln('<info>Installing fixtures: Citizen Database</info>');
            $output->writeln('');
            
            $this->insert_professions($manager, $output);
            $output->writeln('');
            
            $this->insert_home_prototypes($manager, $output);
            $output->writeln('');
            $this->insert_home_upgrades($manager, $output);
            $output->writeln('');
            
            $this->insert_causeOfDeath($manager, $output);
            $output->writeln('');
            
            $this->insert_cleanUpCadaver($manager, $output);
            $output->writeln('');
        } catch (Exception $exception) {
            $output->writeln("<error>{$exception->getMessage()}</error>");
        }
        
    }
    
    protected function insert_causeOfDeath(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $jsonString = file_get_contents(__DIR__ . '/data/type_of_death_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $output->writeln('<comment>Nombre de type de morts :' . count($data) . '</comment>');
        
        $progress = new ProgressBar($output->section());
        $progress->start(count($data));
        
        foreach ($data as $key => $value) {
            // On verifie que la cause de mort n'existe pas
            $entity = $manager->getRepository(TypeDeath::class)->findOneBy(['idMort' => $key]);
            if ($entity === null) {
                $entity = new TypeDeath($key);
            }
            
            $entity->setLabel($value['label'])
                   ->setNom($value['nom'])
                   ->setIcon($value['icon']);
            
            $manager->persist($entity);
            $progress->advance();
        }
        
        $manager->flush();
        $progress->finish();
    }
    
    protected function insert_cleanUpCadaver(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $jsonString = file_get_contents(__DIR__ . '/data/clean_up_cadaver_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $output->writeln('<comment>Nombre de clean de cadavre :' . count($data) . '</comment>');
        
        $progress = new ProgressBar($output->section());
        $progress->start(count($data));
        
        foreach ($data as $key => $value) {
            // On verifie que le clean n'existe pas dans la base
            $entity = $manager->getRepository(CleanUpCadaver::class)->find($key);
            if ($entity === null) {
                $entity = new CleanUpCadaver($key);
            }
            
            $entity->setLabel($value['label'])
                   ->setRef($value['ref']);
            
            $manager->persist($entity);
            $progress->advance();
        }
        
        
        $manager->flush();
        $progress->finish();
    }
    
    protected function insert_home_prototypes(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $jsonString = file_get_contents(__DIR__ . '/data/home_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $output->writeln('<comment>Nombre de maison :' . count($data) . '</comment>');
        
        $progress = new ProgressBar($output->section());
        
        
        $progress->start(count($data));
        
        foreach ($data as $value) {
            
            // On verifie la non existance
            $entity = $manager->getRepository(HomePrototype::class)->findOneBy(['id' => $value['id']]);
            
            if ($entity === null) {
                $entity = new HomePrototype();
                $entity->setId($value['id']);
            } else {
                foreach ($entity->getRessources() as $ressource) {
                    $entity->removeRessource($ressource);
                }
                foreach ($entity->getRessourcesUrba() as $ressource) {
                    $entity->removeRessourceUrba($ressource);
                }
            }
            
            $entity->setNom($value['nom'])
                   ->setIcon($value['icon'])
                   ->setDef($value['def'])
                   ->setPa($value['pa'])
                   ->setPaUrba($value['pa_u']);
            
            //On ajoute les ressources
            foreach ($value['ressources'] as $id => $nombre) {
                $ressource = new RessourceHome();
                
                $item = $manager->getRepository(ItemPrototype::class)->find($id);
                
                $ressource->setId($value['id'] * 10000 + $item->getId() * 10)
                          ->setItem($item)
                          ->setNombre($nombre);
                $entity->addRessource($ressource);
            }
            //On ajoute les ressources
            foreach ($value['ressources_u'] as $id => $nombre) {
                $ressource = new RessourceHome();
                
                $item = $manager->getRepository(ItemPrototype::class)->find($id);
                
                $ressource->setId($value['id'] * 10000 + $item->getId() * 10 + 1)
                          ->setItem($item)
                          ->setNombre($nombre);
                $entity->addRessourceUrba($ressource);
            }
            
            $manager->persist($entity);
            $manager->flush();
            $progress->advance();
            
        }
        
        
        $progress->finish();
    }
    
    protected function insert_home_upgrades(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $jsonString = file_get_contents(__DIR__ . '/data/upgrade_home_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $output->writeln('<comment>Nombre d\'amelioration maison :' . count($data) . '</comment>');
        
        $progress = new ProgressBar($output->section());
        $progress->start(count($data));
        
        foreach ($data as $value) {
            
            // On verifie que l'amélioration n'existe ou non
            $entity = $manager->getRepository(UpHomePrototype::class)->findOneBy(['nom' => $value['nom']]);
            if ($entity === null) {
                $entity = new UpHomePrototype();
            } else {
                foreach ($entity->getLevels() as $lvl) {
                    foreach ($lvl->getRessources() as $ressource) {
                        $lvl->removeRessource($ressource);
                    }
                    $entity->removeLevel($lvl);
                }
            }
            
            $entity->setNom($value['nom'])
                   ->setLabel($value['label'])
                   ->setIcon($value['icon']);
            
            // On alimente les levels
            foreach ($value['levels'] as $key => $level) {
                $lvlUpHome = new LvlUpHome();
                $lvlUpHome->setPa($level['pa'])
                          ->setLevel($key);
                // On alimente la ou les ressources
                foreach ($level['ressources'] as $id => $nombre) {
                    $ressource = new RessourceUpHome();
                    
                    $item = $manager->getRepository(ItemPrototype::class)->find($id);
                    
                    $ressource->setItem($item)
                              ->setNombre($nombre);
                    
                    $lvlUpHome->addRessource($ressource);
                }
                
                $entity->addLevel($lvlUpHome);
            }
            
            $manager->persist($entity);
            $progress->advance();
            
        }
        
        $manager->flush();
        $progress->finish();
    }
    
    protected function insert_professions(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $jsonString = file_get_contents(__DIR__ . '/data/profession_data.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $output->writeln('<comment>Nombre de métier :' . count($data) . '</comment>');
        
        $progress = new ProgressBar($output->section());
        
        
        $progress->start(count($data));
        
        foreach ($data as $entry) {
            // On verifie si ça n'existe pas, sinon, on met à jour
            $entity = $this->em->getRepository(JobPrototype::class)->findOneBy(['nom' => $entry['nom']]);
            
            if ($entity === null) {
                $entity = new JobPrototype();
            }
            $entity->setIsHeros($entry['hero']);
            $entity->setNom($entry['nom']);
            $entity->setDescription($entry['desc']);
            $entity->setIcon($entry['icon']);
            $entity->setRef($entry['ref']);
            $entity->setAlternatif($entry['alternatif']);
            
            $manager->persist($entity);
            $progress->advance();
        }
        
        $manager->flush();
        $progress->finish();
        
        
    }
}