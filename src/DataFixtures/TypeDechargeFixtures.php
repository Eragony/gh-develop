<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldObjets;
use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use App\Entity\TypeDecharge;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class TypeDechargeFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [ChantierFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        // Récupération de tous les chantiers de décharge
        $listChantiersId = [118, 116, 117, 112, 113, 114];
        $listChantiers   =
            $manager->getRepository(ChantierPrototype::class)->findBy(['id' => $listChantiersId]);
        
        $out->writeln('<info>Installing fixtures: type_decharge Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de type de décharges : ' . count($listChantiers) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listChantiers));
        
        foreach ($listChantiers as $key => $chantier) {
            $entity = $manager->getRepository(TypeDecharge::class)->findOneBy(['chantier' => $chantier]);
            if ($entity === null) {
                $entity = (new TypeDecharge($key + 1))->setChantier($chantier);
            }
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        $manager->flush();
        $processBar->finish();
        
        $this->updateObjet($manager, $out);
    }
    
    private function updateObjet(ObjectManager $manager, ConsoleOutput $out): void
    {
        // $listDechargeOldId = [0, 118, 116, 117, 112, 113, 114];
        $listDechargeOldId    = [0, 116, 117, 118, 113, 114, 112];
        $listDechargeChantier = $manager->getRepository(TypeDecharge::class)->findAll();
        $idChantierDech       =
            array_map(fn(TypeDecharge $i) => $i->getChantier()->getId(), $listDechargeChantier);
        
        $listDecharge = array_combine($idChantierDech, $listDechargeChantier);
        
        /**
         * récup de la liste des objets anciens qui sont déchargeables
         *
         * @var OldObjets[] $listObjets
         */
        
        $jsonString = file_get_contents(__DIR__ . '/data/listObjet.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listObjets = array_filter(
            $data,
            fn($i) => isset($i['type_decharge']),
        );
        
        $out->writeln('<info>Update fixtures: type_decharge on ItemPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de type de décharges à mettre à jour : ' . count($listObjets) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listObjets));
        
        foreach ($listObjets as $objet) {
            $entity = $manager->getRepository(ItemPrototype::class)->find($objet['id']);
            if ($entity === null) {
                $out->writeln("<error>Objet inexistant {$objet['id']}</error>");
            } else {
                $entity->setTypeDecharge($listDecharge[$listDechargeOldId[$objet['type_decharge']]]);
                $manager->persist($entity);
            }
        }
        
        $manager->flush();
        $processBar->finish();
        $out->write('');
    }
}
