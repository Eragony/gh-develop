<?php

namespace App\DataFixtures;

use App\Entity\PictoPrototype;
use App\Entity\PictoTitrePrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class PictoFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [ObjetFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $jsonString = file_get_contents(__DIR__ . '/data/pictos.json');
        $listPictos = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $out->writeln('<info>Installing fixtures: PictoPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de pictos : ' . count($listPictos) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listPictos));
        
        foreach ($listPictos as $key => $picto) {
            
            $entity = $manager->getRepository(PictoPrototype::class)->find($picto['id']);
            
            if ($entity === null) {
                $entity = new PictoPrototype();
                $entity->setId($picto['id']);
            }
            
            $entity->setName($picto['name'])
                   ->setDescription($picto['desc'])
                   ->setRare($picto['rare'])
                   ->setCommunity($picto['community'])
                   ->setImg($picto['img'])
                   ->setUuid($key)
                   ->setActif(true);
            
            $entity->removeAllTitre();
            
            foreach ($picto['title'] as $titre) {
                
                $titrebdd = $manager->getRepository(PictoTitrePrototype::class)->findOneBy(['id' => $titre['id']]);
                
                if ($titrebdd === null) {
                    $titrebdd = new PictoTitrePrototype();
                    
                    $titrebdd->setNbr($titre['nbr'])
                             ->setPictoPrototype($entity);
                }
                
                $titrebdd->setTitre($titre['titre']);
                
                $entity->addTitre($titrebdd);
                
            }
            
            $manager->persist($entity);
            
            $processBar->advance();
            
        }
        
        
        $manager->flush();
        
        $processBar->finish();
        $out->writeln('');
    }
}
