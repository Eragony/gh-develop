<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldCoajumpVille;
use App\Entity\Ancestor\OldGestVille;
use App\Entity\Ancestor\OldInscripJump;
use App\Entity\Coalition;
use App\Entity\CreneauHorraire;
use App\Entity\CreneauJump;
use App\Entity\DispoJump;
use App\Entity\DisponibiliteJoueurs;
use App\Entity\GestionJump;
use App\Entity\HerosPrototype;
use App\Entity\InscriptionJump;
use App\Entity\JobPrototype;
use App\Entity\Jump;
use App\Entity\LeadInscription;
use App\Entity\LevelRuinePrototype;
use App\Entity\ObjectifVillePrototype;
use App\Entity\RoleJoueurJump;
use App\Entity\RoleJump;
use App\Entity\StatutInscription;
use App\Entity\StatutUser;
use App\Entity\TypeDispo;
use App\Entity\TypeDispoJump;
use App\Entity\TypeLead;
use App\Entity\TypeVille;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldToNewJumpFixture extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(protected ManagerRegistry $managerRegistry, protected DataCollection $collection,
                                protected ConfMaster      $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class, JumpFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()
                              ->get(GestHordesConf::CONF_MIGRATION_OLD_JUMP)) {
            return;
        }
        
        $manager = $this->managerRegistry->getManager();
        
        $out = new ConsoleOutput();
        
        $nombreJump = $manager->getRepository(OldGestVille::class)
                              ->countMigration()[1] ?? 0;
        
        try {
            $out->writeln('<info>Installing fixtures: Jumps Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre de ville : ' . $nombreJump . ' .</comment>');
            $out->writeln('');
            
            $this->majJump($manager, $out);
            
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()}</error>");
        }
        
    }
    
    public function majJump(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $listJumps = $manager->getRepository(OldGestVille::class)
                             ->findAll();
        
        
        $out->writeln('<comment>Progression globale / progression jump par jump : </comment>');
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listJumps));
        
        $listPouvoir = $manager->getRepository(HerosPrototype::class)
                               ->findAll();
        
        $listMetier = $manager->getRepository(JobPrototype::class)
                              ->findAll();
        
        $listRole = $manager->getRepository(RoleJump::class)
                            ->findAll();
        
        $listDispo = $manager->getRepository(TypeDispo::class)
                             ->findAll();
        $listLead  = $manager->getRepository(TypeLead::class)
                             ->findAll();
        
        
        $listAvancement = $manager->getRepository(StatutInscription::class)
                                  ->findAllIndexed();
        
        $listLevelRuine = $manager->getRepository(LevelRuinePrototype::class)
                                  ->findAllIndexed();
        
        $listTypeVille = $manager->getRepository(TypeVille::class)
                                 ->findAllIndexed();
        
        $listObjetif = $manager->getRepository(ObjectifVillePrototype::class)
                               ->findAll();
        
        $listCreneau = $manager->getRepository(CreneauHorraire::class)
                               ->findAll();
        
        $listStatutJump = $manager->getRepository(StatutUser::class)
                                  ->findAll();
        $listDispoJump  = $manager->getRepository(TypeDispoJump::class)
                                  ->findAll();
        
        foreach ($listJumps as $jump) {
            
            $idJump = $jump->getIdgest();
            
            if ($jump->getMapid() !== null) {
                $villeJump = $manager->getRepository(Ville::class)
                                     ->findOneBy(['mapId' => $jump->getMapid(), 'origin' => Ville::ORIGIN_MH]);
            } else {
                $villeJump = null;
            }
            
            $userCreateur = $manager->getRepository(User::class)
                                    ->findOneBy(['idMyHordes' => $jump->getIdcreateur()]);
            
            if ($userCreateur === null) {
                $processBarGlobale->advance();
                continue;
            }
            
            $typeVille = $listTypeVille[$jump->getType()];
            
            
            $jumpNew = new Jump();
            $jumpNew->setNom(mb_convert_encoding((string)$jump->getNomville(), 'ISO-8859-1'))
                    ->setDateApproxJump($jump->getDatejumpapprox())
                    ->setDateDebInscription($jump->getDatedinscription())
                    ->setDateFinInscription($jump->getDatefinscription())
                    ->setDateJumpEffectif($jump->getDatejumpeffectif())
                    ->setVille($villeJump)
                    ->setCreatedBy($userCreateur)
                    ->setDateCreation($jump->getDatecreationjump())
                    ->setDateModif(null)
                    ->setTypeVille($typeVille)
                    ->setVillePrive($jump->getPrive() !== 0);
            
            foreach ($listCreneau as $creneau) {
                $jumpNew->addCreneauSemaine($creneau);
                $jumpNew->addCreneauWeekend($creneau);
            }
            
            
            $jumpNew->addGestionnaire($userCreateur);
            
            // S'il existe des gestionnaires autre que le créateur :
            if ($jump->getIdgestion() !== null) {
                $listId = explode('.', (string)$jump->getIdgestion());
                
                foreach ($listId as $id) {
                    $userGestionnaire = $manager->getRepository(User::class)
                                                ->findOneBy(['idMyHordes' => $id]);
                    
                    if ($userGestionnaire !== null) {
                        $jumpNew->addGestionnaire($userGestionnaire);
                    }
                }
            }
            
            
            // récup les objetctifs du jump sous forme de tableau
            $objectifs = explode('.', (string)$jump->getObjectif());
            
            foreach ($objectifs as $key => $objectif) {
                if ($objectif == 1) {
                    $jumpNew->addObjectif($listObjetif[$key]);
                }
            }
            
            // Récupération des inscriptions
            
            $listInsciptions = $manager->getRepository(OldInscripJump::class)
                                       ->findBy(['idgest' => $idJump]);
            
            foreach ($listInsciptions as $insciption) {
                
                $insciptionNew = new InscriptionJump();
                
                $userInscription = $manager->getRepository(User::class)
                                           ->findOneBy(['idMyHordes' => $insciption->getTwinid()]);
                
                if ($userInscription === null) {
                    continue;
                }
                
                $lvlRuine = $listLevelRuine[$insciption->getLvlruine() - 1];
                $statut   = $listAvancement[$insciption->getAvansins()];
                
                $insciptionNew->setUser($userInscription)
                              ->setCommentaires(
                                  iconv(
                                      mb_detect_encoding($insciption->getComij() ?? "", mb_detect_order(), true),
                                      "UTF-8",
                                      $insciption->getComij() ?? "",
                                  ),
                              )
                              ->setBanCommu($insciption->getBancom() != 0)
                              ->setGouleCommu($insciption->getGoulcom() != 0)
                              ->setMotivations(
                                  iconv(
                                      mb_detect_encoding($insciption->getMotiij() ?? "", mb_detect_order(), true),
                                      "UTF-8",
                                      $insciption->getMotiij() ?? "",
                                  ),
                              )
                              ->setBlocNotes(
                                  iconv(
                                      mb_detect_encoding($insciption->getBlocnoteij() ?? "", mb_detect_order(), true),
                                      "UTF-8",
                                      $insciption->getBlocnoteij() ?? "",
                                  ),
                              )
                              ->setReponse(
                                  iconv(
                                      mb_detect_encoding($insciption->getRetourij() ?? "", mb_detect_order(), true),
                                      "UTF-8",
                                      $insciption->getRetourij() ?? "",
                                  ),
                              )
                              ->setLvlRuine($lvlRuine)
                              ->setStatut($statut)
                              ->setPouvoirFutur($listPouvoir[($insciption->getDerpouv() ?? 0) + 5])
                              ->setDateInscription($insciption->getDateinscription())
                              ->setDateModification($insciption->getDatemodification())
                              ->setMoyenContact(null)
                              ->setTypeDiffusion($insciption->getDiffumoyencont())
                              ->setVoeuxMetier1(($insciption->getVoeuxmet1() == 0) ? null :
                                                    $listMetier[$insciption->getVoeuxmet1() - 1])
                              ->setVoeuxMetier2(($insciption->getVoeuxmet2() == 0) ? null :
                                                    $listMetier[$insciption->getVoeuxmet2() - 1])
                              ->setVoeuxMetier3(($insciption->getVoeuxmet3() == 0) ? null :
                                                    $listMetier[$insciption->getVoeuxmet3() - 1])
                              ->setMetierDef(($insciption->getMetierdef() !== null) ?
                                                 $listMetier[$insciption->getMetierdef() - 1] : null);
                
                $dispoOld = array_map(fn($i) => explode('.', $i), explode('|', (string)$insciption->getDispotype()));
                
                foreach ($listCreneau as $key => $creneau) {
                    $dispoS       = $listDispo[$dispoOld[0][$key] - 1];
                    $dispoSemaine = new DisponibiliteJoueurs();
                    $dispoSemaine->setTypeCreneau(1)
                                 ->setCreneau($creneau)
                                 ->setDispo($dispoS);
                    $dispoW    = $listDispo[$dispoOld[1][$key] - 1];
                    $dispoWeek = new DisponibiliteJoueurs();
                    $dispoWeek->setTypeCreneau(2)
                              ->setCreneau($creneau)
                              ->setDispo($dispoW);
                    
                    $insciptionNew->addDispo($dispoSemaine);
                    $insciptionNew->addDispo($dispoWeek);
                }
                
                $roleOld = explode('.', (string)$insciption->getRoleville());
                
                foreach ($roleOld as $key => $role) {
                    if ($role == 1) {
                        $insciptionNew->addRoleJoueurJump($listRole[$key]);
                    }
                }
                
                $apprentiLead = explode('.', (string)$insciption->getPostalead());
                
                foreach ($apprentiLead as $key => $aLead) {
                    if ($aLead == 1) {
                        $leadInscription = new LeadInscription();
                        $leadInscription->setApprenti(true)
                                        ->setLead($listLead[$key]);
                        $insciptionNew->addLeadInscription($leadInscription);
                    }
                }
                
                $leads = explode('.', (string)$insciption->getPostlead());
                
                foreach ($leads as $key => $lead) {
                    if ($lead == 1) {
                        $leadInscription = new LeadInscription();
                        $leadInscription->setApprenti(false)
                                        ->setLead($listLead[$key]);
                        $insciptionNew->addLeadInscription($leadInscription);
                    }
                }
                
                
                $jumpNew->addInscriptionJump($insciptionNew);
                
                
            }
            
            // création des creneaux de jump
            // Utilisation des creneaux pat et 7h-9h.
            $creneauPAT   = $manager->getRepository(CreneauHorraire::class)->findOneBy(['id' => 0]);
            $creneauMatin = $manager->getRepository(CreneauHorraire::class)->findOneBy(['id' => 2]);
            
            //calcul des 3 jours de jump
            $dateJump1 = new DateTime($jumpNew->getDateApproxJump()->format('Y-m-d'));
            $dateJump2 = new DateTime($jumpNew->getDateApproxJump()->format('Y-m-d'));
            $dateJump3 = new DateTime($jumpNew->getDateApproxJump()->format('Y-m-d'));
            
            $dateJump2->add(new DateInterval('P1D'));
            $dateJump3->add(new DateInterval('P2D'));
            
            // creneau 1 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump1)->setCreneauHorraire($creneauPAT);
            
            $jumpNew->addCreneau($creneauJump);
            // creneau 2 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump1)->setCreneauHorraire($creneauMatin);
            
            $jumpNew->addCreneau($creneauJump);
            // creneau 3 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump2)->setCreneauHorraire($creneauPAT);
            
            $jumpNew->addCreneau($creneauJump);
            // creneau 4 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump2)->setCreneauHorraire($creneauMatin);
            
            $jumpNew->addCreneau($creneauJump);
            // creneau 5 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump3)->setCreneauHorraire($creneauPAT);
            
            $jumpNew->addCreneau($creneauJump);
            // creneau 6 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump3)->setCreneauHorraire($creneauMatin);
            
            $jumpNew->addCreneau($creneauJump);
            
            
            $manager->persist($jumpNew);
            $manager->flush();
            
            $manager->refresh($jumpNew);
            
            // récupération des anciennes coas
            
            /**
             * @var OldCoajumpVille[] $listCoasJump
             */
            $listCoasJump = $manager->getRepository(OldCoajumpVille::class)->findBy(['idGest' => $jump->getIdgest()]);
            
            foreach ($listCoasJump as $coa) {
                
                if ($coa->getTwinIdCoa() !== null && $coa->getTwinIdCoa() !== 0) {
                    $userCoa = $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $coa->getTwinIdCoa()]);
                    
                    if ($userCoa !== null) {
                        
                        $newCoa = new Coalition();
                        $newCoa->setUser($userCoa)
                               ->setNumCoa(ceil($coa->getIdCoas() / 5))
                               ->setPositionCoa(($coa->getIdCoas() % 5) + 1)
                               ->setStatut(($coa->getEtatJoueur() === 0) ? null :
                                               $listStatutJump[$coa->getEtatJoueur() - 1]);
                        
                        $indexCreneau = 0;
                        foreach ($jumpNew->getCreneau() as $creneauJump) {
                            $indexCreneau++;
                            
                            $dispoJoueur = new DispoJump();
                            
                            $method     = 'getDateJump' . $indexCreneau;
                            $choixDispo = ($coa->$method() == null || $coa->$method() == 0) ? null :
                                $listDispoJump[$coa->$method() - 1];
                            
                            $dispoJoueur->setChoixDispo($choixDispo)
                                        ->setCreneauJump($creneauJump);
                            
                            $newCoa->addDispo($dispoJoueur);
                            
                        }
                        
                        $jumpNew->addCoalition($newCoa);
                        
                    }
                    
                }
                
            }
            
            
            $manager->persist($jumpNew);
            $processBarGlobale->advance();
            $manager->flush();
            $processBarGlobale->finish();
            $out->writeln('');
            
        }
    }
}
