<?php

namespace App\EventSubscriber;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class ExceptionSubscriber implements EventSubscriberInterface
{
    
    
    public function __construct(private readonly string          $discordChannelId,
                                private readonly string          $discordToken,
                                private readonly KernelInterface $kernel)
    {
    }
    
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
    
    /**
     * @throws GuzzleException
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        
        
        if ($this->kernel->getEnvironment() === 'prod') {
            
            // Vérifiez si l'exception est de type NotFoundHttpException, MethodNotAllowedHttpException ou TooManyRequestsHttpException
            if (
                $exception instanceof NotFoundHttpException ||
                $exception instanceof MethodNotAllowedHttpException ||
                $exception instanceof TooManyRequestsHttpException ||
                $exception instanceof ResourceNotFoundException ||
                $exception instanceof UnauthorizedHttpException
            ) {
                return;
            }
            
            $requestUri = $event->getRequest()->getRequestUri();
            
            // Liste des fichiers et schémas spécifiques à exclure
            $excludedFilesAndSchemas = [
                '/.env',                 // Exclure l'accès au fichier .env
                '/images',   // Exclure l'accès à l'image favicon.gif
                '/build',   // Exclure l'accès à l'image favicon.gif
                '/img',   // Exclure l'accès à l'image favicon.gif
                '.well-known', // Exclure l'accès à l'image favicon.gif
                '/map',
                '/.git',
                '%22',                   // Exclure toutes les URLs contenant "%22"
            ];
            
            // Vérifiez si l'URI correspond à l'un des fichiers ou schémas exclus
            foreach ($excludedFilesAndSchemas as $exclude) {
                if (str_contains($requestUri, $exclude)) {
                    return;
                }
            }
            
            $apiUrl = "https://discord.com/api/v10/channels/$this->discordChannelId/messages";
            
            
            // Composez le message d'erreur avec les informations de l'exception
            $message = "Une exception a été levée :\n";
            $message .= "- Message : " . $exception->getMessage() . "\n";
            $message .= "- Trace : " . $exception->getTraceAsString() . "\n";
            
            // Préparez les données pour la requête POST
            $data = ['content' => substr($message, 0, 2000)];
            
            // Utilisez Guzzle pour envoyer le message à Discord via le webhook
            $client = new Client();
            $client->post($apiUrl, [
                'headers' => [
                    'Authorization' => "Bot $this->discordToken",
                    'Content-Type'  => 'application/json',
                ],
                'json'    => $data,
            ]);
        }
    }
}