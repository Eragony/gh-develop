<?php

namespace App\Enum;

enum ListColor:string
{
    case JET = 'jet';
    case HSV = 'hsv';
    case HOT = 'hot';
    case COOL = 'cool';
    case SPRING = 'spring';
    case SUMMER = 'summer';
    case AUTUMN = 'autumn';
    case WINTER = 'winter';
    case BONE = 'bone';
    case COPPER = 'copper';
    case GREYS = 'greys';
    case YIGnBu = 'YIGnBu';
    case GREENS = 'greens';
    case YIOrRd = 'YIOrRd';
    case BLUERED = 'bluered';
    case RdBu = 'RdBu';
    case PICNIC = 'picnic';
    case RAINBOW = 'rainbow';
    case PORTLAND = 'portland';
    case BLACKBODY = 'blackbody';
    case EARTH = 'earth';
    case ELECTRIC = 'electric';
    case VIRIDIS = 'viridis';
    case INFERNO = 'inferno';
    case MAGMA = 'magma';
    case PLASMA = 'plasma';
    case WARM = 'warm';
    case RAINBOW_SOFT = 'rainbow-soft';
    case BATHYMETRY = 'bathymetry';
    case CDOM = 'cdom';
    case CHLOROPHYLL = 'chlorophyll';
    case DENSITY = 'density';
    case FREESURFACE_BLUE = 'freesurface-blue';
    case FREESURFACE_RED = 'freesurface-red';
    case OXYGEN = 'oxygen';
    case PAR = 'par';
    case PHASE = 'phase';
    case SALINITY = 'salinity';
    case TEMPERATURE = 'temperature';
    case TURBIDITY = 'turbidity';
    case VELOCITY_BLUE = 'velocity-blue';
    case VELOCITY_GREEN = 'velocity-green';
}
