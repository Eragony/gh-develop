<?php

namespace App\Form\Admin;

use App\Entity\ChantierPrototype;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChantierPrototypeType extends AbstractType
{
    
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $listChantier = $this->entityManager->getRepository(ChantierPrototype::class)->findAllAlpha();
        
        $builder
            ->add('id', IntegerType::class, ['label' => 'Identifiant chantier : '])
            ->add('nom', TextType::class, ['label' => 'Nom : '])
            ->add('icon', TextType::class, ['label' => 'Icon : '])
            ->add('def', IntegerType::class, ['label' => 'Defense : '])
            ->add('water', IntegerType::class, ['label' => 'Eau : '])
            ->add('pa', IntegerType::class, ['label' => 'Coût en PA : '])
            ->add(
                'plan',
                ChoiceType::class,
                [
                    'choices'  => ['Aucun'          => 0, 'Plan commun' => 1, 'Plan inhab' => 2, 'Plan rare' => 3,
                                   'Plan très rare' => 4],
                    'label'    => 'Plan : ',
                    'required' => true,
                ],
            
            )
            ->add(
                'niveau',
                ChoiceType::class,
                [
                    'choices'  => [0, 1, 2, 3],
                    'label'    => 'Niveau : ',
                    'required' => true,
                ],
            
            )
            ->add('temp', CheckboxType::class, ['required' => false, 'label' => 'Temporaire : '])
            ->add('pv', IntegerType::class, ['label' => 'Point de vie : '])
            ->add('indes', CheckboxType::class, ['required' => false, 'label' => 'Indestructible : '])
            ->add('ruineHo', CheckboxType::class, ['required' => false, 'label' => 'l\'Hotel '])
            ->add('ruineHs', CheckboxType::class, ['required' => false, 'label' => 'l\'Hôpital '])
            ->add('ruineBu', CheckboxType::class, ['required' => false, 'label' => 'le Bunker '])
            ->add(
                'orderby',
                IntegerType::class,
                [
                    'label' => 'Ordre : ',
                ],
            )
            ->add(
                'levelMax',
                IntegerType::class,
                [
                    'label' => 'Level max Up : ',
                ],
            )
            ->add(
                'ressources',
                CollectionType::class,
                [
                    'entry_type'    => RessourceType::class,
                    'entry_options' => ['label' => false],
                    'allow_add'     => true,
                    'allow_delete'  => true,
                    'label'         => false,
                    'by_reference'  => false,
                ],
            )
            ->add(
                'parent',
                ChoiceType::class,
                [
                    'choices'      => $listChantier,
                    'choice_value' => 'id',
                    'choice_label' => fn(?ChantierPrototype $chantierPrototype) => $chantierPrototype ?
                        $chantierPrototype->getNom() : '',
                    'placeholder'  => 'Aucun',
                    'label'        => 'Chantier mère : ',
                    'required'     => false,
                ],
            )
            ->add(
                'catChantier',
                ChoiceType::class,
                [
                    'choices'      => $listChantier,
                    'choice_value' => 'id',
                    'choice_label' => fn(?ChantierPrototype $chantierPrototype) => $chantierPrototype ?
                        $chantierPrototype->getNom() : '',
                    'placeholder'  => 'Aucun',
                    'label'        => 'Chantier catégorie : ',
                    'required'     => false,
                ],
            )
            ->add(
                'levelUps',
                CollectionType::class,
                [
                    'entry_type'    => BonusUpChantierType::class,
                    'entry_options' => ['label' => false],
                    'allow_add'     => true,
                    'allow_delete'  => true,
                    'label'         => false,
                    'by_reference'  => false,
                ],
            )
            ->add('save', SubmitType::class, ['row_attr' => ['class' => 'boutSave'], 'label' => 'Sauvegarder']);
        
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class' => ChantierPrototype::class,
                               ]);
    }
}
