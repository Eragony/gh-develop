<?php

namespace App\Form\Admin;

use App\Entity\CategoryObjet;
use App\Entity\ItemPrototype;
use App\Entity\TypeDecharge;
use App\Entity\TypeObjet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemPrototypeType extends AbstractType
{
    
    public function __construct(protected EntityManagerInterface $entityManager)
    {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $listTypeObjet      = $this->entityManager->getRepository(TypeObjet::class)->findAll();
        $listCategorieObjet = $this->entityManager->getRepository(CategoryObjet::class)->findAll();
        $listTypeDecharge   = $this->entityManager->getRepository(TypeDecharge::class)->findAll();
        
        $builder
            ->add('id', IntegerType::class, ['label' => 'Identifiant Objet : '])
            ->add('nom', TextType::class, ['label' => "Nom de l'objet : "])
            ->add('icon', TextType::class, ['label' => 'Icon : '])
            ->add('description',
                  TextareaType::class,
                  ['row_attr' => ['class' => 'textAreaAlign'], 'label' => 'Description : ',
                   'attr'     => ['cols' => 50, 'rows' => 5]],
            )
            ->add(
                'objetVeille',
                CheckboxType::class,
                [
                    'label'    => "Objet de veille : ",
                    'required' => false,
                ],
            )
            ->add('defBase', IntegerType::class, [
                'label'    => "Défense de base de l'objet : ",
                'required' => false,
            ])
            ->add(
                'armurerie',
                CheckboxType::class,
                [
                    'label'    => "Affecté par l'Armurerie : ",
                    'required' => false,
                ],
            )
            ->add(
                'magasin',
                CheckboxType::class,
                [
                    'label'    => "Affecté par le Magasin suédois : ",
                    'required' => false,
                ],
            )
            ->add(
                'tourelle',
                CheckboxType::class,
                [
                    'label'    => "Affecté par la Tourelle lance-eau : ",
                    'required' => false,
                ],
            )
            ->add(
                'lanceBete',
                CheckboxType::class,
                [
                    'label'    => "Affecté par le Lance-bête : ",
                    'required' => false,
                ],
            )
            ->add(
                'encombrant',
                CheckboxType::class,
                [
                    'label'    => "Objet encombrant : ",
                    'required' => false,
                ],
            )
            ->add(
                'usageUnique',
                CheckboxType::class,
                [
                    'label'    => "Objet à usage unique : ",
                    'required' => false,
                ],
            )
            ->add(
                'reparable',
                CheckboxType::class,
                [
                    'label'    => "Objet réparable : ",
                    'required' => false,
                ],
            )
            ->add(
                'typeObjet',
                ChoiceType::class,
                [
                    'choices'      => $listTypeObjet,
                    'choice_value' => 'id',
                    'choice_label' => fn(?TypeObjet $typeObjet) => $typeObjet ? $typeObjet->getNom() : '',
                    'placeholder'  => 'Aucune spécificité',
                    'label'        => "Type de l'objet : ",
                    'required'     => false,
                ],
            )
            ->add(
                'categoryObjet',
                ChoiceType::class,
                [
                    'choices'      => $listCategorieObjet,
                    'choice_value' => 'id',
                    'choice_label' => fn(?CategoryObjet $categoryObjet) => $categoryObjet ? $categoryObjet->getNom() :
                        '',
                    'placeholder'  => 'choix catégorie',
                    'label'        => "Catégorie de l'objet : ",
                    'required'     => true,
                ],
            )
            ->add(
                'typeDecharge',
                ChoiceType::class,
                [
                    'choices'      => $listTypeDecharge,
                    'choice_value' => 'id',
                    'choice_label' => fn(?TypeDecharge $typeDecharge) => $typeDecharge ?
                        $typeDecharge->getChantier()->getNom() : '',
                    'placeholder'  => 'Ne passe pas en décharge',
                    'label'        => "Type de décharge : ",
                    'required'     => false,
                ],
            )
            ->add('save', SubmitType::class, ['row_attr' => ['class' => 'boutSave'], 'label' => 'Sauvegarder']);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class' => ItemPrototype::class,
                               ]);
    }
}
