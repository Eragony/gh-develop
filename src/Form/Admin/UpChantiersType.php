<?php

namespace App\Form\Admin;

use App\Entity\UpChantierPrototype;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpChantiersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'typeBonus',
                ChoiceType::class,
                [
                    'choices'     => array_flip(UpChantierPrototype::TAB_TYPE_EVO),
                    'placeholder' => 'Aucun',
                    'label'       => 'Type bonus : ',
                ],
            )
            ->add('valeurUp', TextType::class, ['label' => 'Valeur up : ']);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class' => UpChantierPrototype::class,
                               ]);
    }
}
