<?php

namespace App\Form\Admin;

use App\Entity\NewsSite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsSiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('titre', TextType::class, ['label' => 'Titre de la news : '])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu de la news : ',
                'attr'  => ['cols' => 60, 'rows' => 20],
            ])
            ->add('valider', SubmitType::class, ['label' => 'Valider'])
            ->add('annuler', SubmitType::class, ['label' => 'Annuler']);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class' => NewsSite::class,
                               ]);
    }
}
