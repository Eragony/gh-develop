<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionUnionType;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'app:generate:typescript-dtos',
    description: 'Add a short description for your command',
)]
class GenerateTypescriptDtoCommand extends Command
{
    protected static $defaultName = 'app:generate:typescript-dtos';
    
    private Filesystem   $filesystem;
    private SymfonyStyle $io;
    
    public function __construct(private EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->filesystem    = new Filesystem();
    }
    
    protected function configure(): void
    {
        $this
            ->setDescription('Generates TypeScript interfaces and classes for Symfony entities.');
    }
    
    /**
     * @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Generating TypeScript DTOs...');
        
        $metaData = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $nbr      = count($metaData);
        $this->io->note("Nombre de fichier à générer : {$nbr}");
        $this->io->progressStart($nbr);
        
        foreach ($metaData as $meta) {
            $this->generateTypeScriptFiles($meta);
        }
        
        $this->io->progressFinish();
        $this->io->success('TypeScript DTOs generated successfully.');
        return Command::SUCCESS;
    }
    
    /**
     * @throws ReflectionException
     */
    private function addImports(string $content, array $associations, ClassMetadata $meta): string
    {
        $imports = $this->generateImports($associations, $meta);
        return $imports . "\n" . $content;
    }
    
    private function camelCaseToSnakeCase(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
    
    private function convertDoctrineTypeToTypeScript(string $doctrineType, ?string $phpDocType = null): string
    {
        if ($doctrineType === 'json' && $phpDocType !== null) {
            return $this->convertPhpDocTypeToTypeScript($phpDocType);
        }
        
        return match ($doctrineType) {
            'string', 'datetime', 'text', 'datetime_immutable' => 'string',
            'float', 'integer', 'int', 'smallint', 'bigint'    => 'number',
            'boolean', 'bool'                                  => 'boolean',
            'json'                                             => 'any[]',
            default                                            => 'any',
        };
    }
    
    private function convertPhpDocTypeToTypeScript(string $phpDocType): string
    {
        $phpToTypeScriptMap = [
            'int'     => 'number',
            'integer' => 'number',
            'float'   => 'number',
            'double'  => 'number',
            'string'  => 'string',
            'bool'    => 'boolean',
            'boolean' => 'boolean',
            'array'   => 'any[]',
            'null'    => 'null',
        ];
        
        // Handle nested types, e.g., int[][] => number[][]
        return preg_replace_callback('/\b(\w+)\b/', function ($matches) use ($phpToTypeScriptMap) {
            return $phpToTypeScriptMap[$matches[1]] ?? 'any';
        },                           $phpDocType);
    }
    
    private function extractPhpDocType(string $phpDoc): ?string
    {
        if (preg_match('/@var\s+([^\s]+)/', $phpDoc, $matches)) {
            return $matches[1];
        }
        
        return null;
    }
    
    private function fieldInGroups(string $field, array $groups): bool
    {
        return array_key_exists($field, $groups);
    }
    
    
    /**
     * @throws ReflectionException
     */
    private function generateImports(array $associations, ClassMetadata $meta): string
    {
        $imports         = '';
        $importedClasses = [];
        $groups          = $this->getGroups($meta);
        
        foreach ($associations as $association) {
            if ($this->fieldInGroups($association, $groups)) {
                $targetEntity = $meta->getAssociationTargetClass($association);
                $shortName    = (new ReflectionClass($targetEntity))->getShortName();
                if (!in_array($shortName, $importedClasses)) {
                    $importedClasses[] = $shortName;
                }
            }
        }
        
        $reflectionClass = $meta->getReflectionClass();
        foreach ($reflectionClass->getMethods() as $method) {
            if ($this->fieldInGroups($method->getName(), $groups)) {
                $returnType = $method->getReturnType();
                if ($returnType) {
                    $type = $this->getMethodReturnType($method);
                    // Si le type contient DTO, on retire DTO dans le nom pour récupérer le nom de la classe
                    if (str_contains($type, 'DTO')) {
                        $type = str_replace('DTO', '', $type);
                        if (str_contains($type, '[]')) {
                            $type = str_replace('[]', '', $type);
                        }
                        if (!in_array($type, $importedClasses)) {
                            $importedClasses[] = $type;
                        }
                    }
                    
                }
            }
        }
        
        foreach ($importedClasses as $importedClass) {
            // Si on doit importer la classe courante, on ne l'ajoute pas
            if ($importedClass === $meta->getReflectionClass()->getShortName()) {
                continue;
            }
            $imports .= "import { {$importedClass}DTO } from './" . lcfirst($importedClass) . ".dto';\n";
        }
        
        return $imports;
    }
    
    
    /**
     * @throws ReflectionException
     */
    private function generateInterfaceContent(string $entityName, array $fields, array $associations, ClassMetadata $meta): string
    {
        
        $groups    = $this->getGroups($meta);
        $interface = "export interface {$entityName}DTO {\n";
        
        //$this->io->section('Création de l\'interface pour ' . $entityName . 'DTO');
        
        foreach ($fields as $field) {
            //$this->io->text($field);
            //$this->io->text($meta->getTypeOfField($field));
            if ($this->fieldInGroups($field, $groups)) {
                $phpDocType     = $this->getFieldPhpDocType($meta, $field);
                $type           = $this->convertDoctrineTypeToTypeScript($meta->getTypeOfField($field), $phpDocType);
                $snakeCaseField = $this->camelCaseToSnakeCase($field);
                $interface      .= "    $snakeCaseField?: $type;\n";
            }
        }
        
        foreach ($associations as $association) {
            if ($this->fieldInGroups($association, $groups)) {
                $associationType = $meta->isCollectionValuedAssociation($association) ? $this->getCollectionType($meta->getAssociationTargetClass($association)) : ($this->getAssociationType($meta->getAssociationTargetClass($association)) . 'DTO');
                $snakeCaseField  = $this->camelCaseToSnakeCase($association);
                $interface       .= "    $snakeCaseField?: $associationType;\n";
            }
        }
        
        $reflectionClass = $meta->getReflectionClass();
        foreach ($reflectionClass->getMethods() as $method) {
            if ($this->fieldInGroups($method->getName(), $groups)) {
                $returnType = $method->getReturnType();
                if ($returnType) {
                    $type           = $this->getMethodReturnType($method);
                    $snakeCaseField = $this->camelCaseToSnakeCase(preg_replace('/^(get|is)/', '', $method->getName()));
                    $interface      .= "    $snakeCaseField?: $type;\n";
                }
            }
        }
        $interface .= "}\n";
        
        
        return $this->addImports($interface, $associations, $meta);
    }
    
    /**
     * @throws ReflectionException
     */
    private function generateTypeScriptFiles(ClassMetadata $meta): void
    {
        $entityName         = $meta->getName();
        $shortName          = (new ReflectionClass($entityName))->getShortName();
        $lowercaseShortName = lcfirst($shortName);
        
        $fields       = $meta->getFieldNames();
        $associations = $meta->getAssociationNames();
        
        $interfaceContent = $this->generateInterfaceContent($shortName, $fields, $associations, $meta);
        $outputDir        = __DIR__ . '/../../assets/react/types/models/';
        $this->filesystem->mkdir($outputDir);
        
        $interfaceFile = $outputDir . $lowercaseShortName . '.dto.ts';
        
        file_put_contents($interfaceFile, $interfaceContent);
        
        //$this->io->success('Fichier ' . $lowercaseShortName . '.dto.ts généré avec succès.');
        $this->io->progressAdvance();
    }
    
    /**
     * @throws ReflectionException
     */
    private function getAssociationType(string $targetEntity): string
    {
        return (new ReflectionClass($targetEntity))->getShortName();
    }
    
    /**
     * @throws ReflectionException
     */
    private function getCollectionType(string $targetEntity): string
    {
        $shortName = (new ReflectionClass($targetEntity))->getShortName() . 'DTO';
        return $shortName . '[]';
    }
    
    /**
     * @throws ReflectionException
     */
    private function getFieldPhpDocType(ClassMetadata $meta, string $fieldName): ?string
    {
        $reflectionClass    = $meta->getReflectionClass();
        $reflectionProperty = $reflectionClass->getProperty($fieldName);
        $phpDoc             = $reflectionProperty->getDocComment();
        
        return $phpDoc ? $this->extractPhpDocType($phpDoc) : null;
    }
    
    private function getGroups(ClassMetadata $meta): array
    {
        $reflectionClass = $meta->getReflectionClass();
        
        $groups = [];
        
        foreach ($reflectionClass->getProperties() as $property) {
            $attributes = $property->getAttributes(name: "Symfony\Component\Serializer\Attribute\Groups");
            foreach ($attributes as $attribute) {
                foreach ($attribute->getArguments()[0] as $group) {
                    $groups[$property->getName()][$group] = true;
                }
            }
        }
        
        foreach ($reflectionClass->getMethods() as $method) {
            $attributes = $method->getAttributes(name: "Symfony\Component\Serializer\Attribute\Groups");
            foreach ($attributes as $attribute) {
                foreach ($attribute->getArguments()[0] as $group) {
                    $groups[$method->getName()][$group] = true;
                }
            }
        }
        
        return $groups;
    }
    
    /**
     * @throws ReflectionException
     */
    private function getMethodReturnType(ReflectionMethod $method, bool $isForClass = false): string
    {
        $returnType = $method->getReturnType();
        if ($returnType) {
            if ($returnType instanceof ReflectionUnionType) {
                // On balaye le getType() pour récupérer array et collection dans un tableau pour récupérer le type de l'entité
                $types     = $returnType->getTypes();
                $typeNames = array_map(function ($type) {
                    return $type->getName();
                }, $types);
                
                // On vérifie que dans types on a bien array ou collection
                if (in_array('array', $typeNames) || in_array('Doctrine\Common\Collections\Collection', $typeNames)) {
                    // On récupère le docComment pour récupérer le type de l'entité
                    $docComment = $method->getDocComment();
                    if ($docComment) {
                        
                        // On récupère le type de l'entité
                        if (preg_match('/@return\s+(Collection<.*?>|array<.*?>|.*?\[\])/', $docComment, $matches)) {
                            $collectionType = $matches[1];
                            // On récupère le nom de l'entité
                            if (preg_match('/(?:Collection<(\w+)>|array<(\w+)>|(\w+)\[\])/', $collectionType, $typeMatches)) {
                                $targetEntity = !empty($typeMatches[1]) ? $typeMatches[1] : (!empty($typeMatches[2]) ? $typeMatches[2] : $typeMatches[3]);
                                return $targetEntity . (!$isForClass ? 'DTO' : '') . '[]';
                            }
                        }
                    }
                } else {
                    // On vérifie si le type est une classe
                    foreach ($types as $type) {
                        if (class_exists($type->getName())) {
                            return $this->getAssociationType($type->getName()) . (!$isForClass ? 'DTO' : '');
                        }
                    }
                    // On convertit le type de doctrine en type de typescript
                    return $this->convertDoctrineTypeToTypeScript($types[0]->getName());
                }
            } else if ($returnType instanceof ReflectionNamedType) {
                $type = $returnType->getName();
                if ($type === 'array') {
                    // On récupère le docComment pour récupérer le type de l'entité
                    $docComment = $method->getDocComment();
                    if ($docComment) {
                        // On récupère le type de l'entité dans le docComment et on garde le nombre de [] pour les tableaux
                        if (preg_match('/@return\s+(array<.*?>|[a-zA-Z]+)(\[\])*/', $docComment, $matches)) {
                            $arrayType = $matches[0];
                            // On récupère le nom de l'entité et on compte le nombre de []
                            if (preg_match('/@return\s+(?:array<(\w+)>|(\w+))(?:\[\])*/', $arrayType, $typeMatches)) {
                                $targetEntity = !empty($typeMatches[1]) ? $typeMatches[1] : (!empty($typeMatches[2]) ? $typeMatches[2] : $typeMatches[3]);
                                $brackets     = substr_count($arrayType, '[]');
                                $isClass      = class_exists($targetEntity);
                                $tsType       = $isClass ? $targetEntity . (!$isForClass ? 'DTO' : '') : $this->convertDoctrineTypeToTypeScript($targetEntity);
                                return $tsType . str_repeat('[]', $brackets);
                            }
                        }
                    }
                } else {
                    if (class_exists($returnType->getName())) {
                        return $this->getAssociationType($returnType->getName()) . (!$isForClass ? 'DTO' : '');
                    }
                    return $this->convertDoctrineTypeToTypeScript($returnType->getName());
                }
            } else {
                if (class_exists($returnType->getName())) {
                    return $this->getAssociationType($returnType->getName()) . (!$isForClass ? 'DTO' : '');
                }
                return $this->convertDoctrineTypeToTypeScript($returnType->getName());
            }
        }
        
        // Handle the case where returnType is not available but PHPDoc is
        $docComment = $method->getDocComment();
        if ($docComment) {
            if (preg_match('/@return\s+([^\s]+)/', $docComment, $matches)) {
                $phpDocReturnType = $matches[1];
                return $this->convertPhpDocTypeToTypeScript($phpDocReturnType);
            }
        }
        
        return 'any';
    }
    
}
