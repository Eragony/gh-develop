<?php

namespace App\Command;

use App\Entity\ContentsVersion;
use App\Entity\GitVersions;
use App\Entity\NewsSite;
use App\Entity\UpChantier;
use App\Entity\User;
use App\Entity\VersionsSite;
use App\Service\CommandHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand('app:migrate')]
class MigrateCommand extends Command
{
    
    protected static array $git_script_repository = [
        '16affda98bced61e1ad0a141dbf66f375c34edb8' => [['app:migrate', ['--update-array-update' => true]]],
        'b2c4de8a3ad2753a92f74f8d0819dd3a9ceed50b' => [['app:migration:git:transfert']],
        'f3b53be13b8dab11f8d3ff9386e776bc2027739c' => [['app:migration:git:add-personnalisation-theme']],
    ];
    
    public function __construct(
        private readonly KernelInterface        $kernel,
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface  $param,
        private readonly CommandHelper          $commandHelper,
        private readonly TranslatorInterface    $translator,
    )
    {
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de faire la migration pour faire un changement de version')
             ->setHelp('Migrations.')
             ->addOption('from-git', 'g', InputOption::VALUE_NONE, 'Switches to the given git branch and updates everything.')
             ->addOption('remote', null, InputOption::VALUE_REQUIRED, 'Sets the git remote for --from-git')
             ->addOption('branch', null, InputOption::VALUE_REQUIRED, 'Sets the git branch for --from-git')
             ->addOption('environment', null, InputOption::VALUE_REQUIRED, 'Set l\'environnement pour construire les assets')
             ->addOption('phar', null, InputOption::VALUE_NONE, 'S\'il est défini, composer sera appelé à l\'aide d\'un fichier composer.phar')
             ->addOption('fast', null, InputOption::VALUE_NONE, 'Si défini, les mises à jour du compositeur et du fil seront ignorées')
             ->addOption('discord', null, InputOption::VALUE_NONE, 'Si défini, affiche un message discord')
             ->addOption("php-bin", null, InputOption::VALUE_OPTIONAL, 'Définit le binaire PHP à utiliser')
             ->addOption('skip-backup', null, InputOption::VALUE_NONE, 'Si défini, aucune sauvegarde de base de données ne sera créée')
             ->addOption('stay-offline', null, InputOption::VALUE_NONE, 'S\'il est défini, le mode de maintenance restera actif après la mise à jour')
             ->addOption('release', null, InputOption::VALUE_NONE, 'Si défini, supprime les informations de validation de la chaîne de version')
             ->addOption('as', null, InputOption::VALUE_REQUIRED, 'Si la mise à jour est lancée par l\'utilisateur root, cette option spécifie l\'utilisateur du serveur Web (par défaut, www-data)')
             ->addOption('in', null, InputOption::VALUE_REQUIRED, 'Si la mise à jour est lancée par l\'utilisateur root, cette option spécifie le groupe de serveurs Web (par défaut, www-data)')
             ->addOption('dbservice', null, InputOption::VALUE_REQUIRED, 'Si la mise à jour est lancée par l\'utilisateur root, cette option spécifie le nom du service de base de données (par défaut mysql)')
             ->addOption('install-db', 'i', InputOption::VALUE_NONE, 'Crée et effectue la création de la base de données et des appareils.')
             ->addOption("skip-optional", null, InputOption::VALUE_NONE, "Lorsqu'il est utilisé avec install-db, ignorez les étapes facultatives")
             ->addOption('update-db', 'u', InputOption::VALUE_NONE, 'Crée et effectue une migration de doctrine, met à jour les luminaires.')
             ->addOption('recover', 'r', InputOption::VALUE_NONE, 'Lorsqu\'il est utilisé avec --update-db, efface toutes les migrations précédentes et réessaye après une erreur.')
             ->addOption('process-db-git', 'p', InputOption::VALUE_NONE, 'Traite les déclencheurs pour les actions de base de données automatisées')
             ->addOption('update-array-update', null, InputOption::VALUE_NONE, 'Mise à jour des tableaux de jour d\'update chantier')
             ->addOption('changelog', 'c', InputOption::VALUE_NONE, 'Met à jour le changelog et la news associés à nouvelle version')
             ->addOption('publish', 'pub', InputOption::VALUE_NONE, "Publie la dernière version spécifiée du changelog sur discord");
    }
    
    
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws JsonException
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io                = new SymfonyStyle($input, $output);
        $roleIdMaintenance = 1213175522788380752;
        $null              = null;
        
        
        if ($input->getOption('from-git')) {
            
            $remote  = $input->getOption('remote');
            $branch  = $input->getOption('branch');
            $env     = $input->getOption('environment');
            $discord = $input->getOption('discord');
            $php     = $input->getOption("php-bin") ?? 'php';
            
            
            $io->title('Mise à jour du code source');
            $io->info("Bonjour !");
            
            
            for ($i = 3; $i > 0; --$i) {
                $io->text("Début de la mise à jour dans <info>$i</info> secondes....");
                sleep(1);
            }
            
            if (!$this->commandHelper->capsule("git fetch --tags $remote $branch", $io, 'Récupération des mises à jour du référentiel... ', false)) {
                return 1;
            }
            if (!$this->commandHelper->capsule("git reset --hard $remote/$branch", $io, 'Application des modifications sur les fichiers... ', false)) {
                return 2;
            }
            
            if (!$input->getOption('skip-backup')) {
                if (!$this->commandHelper->capsule('app:cron backup update', $io, 'Création d\'une sauvegarde de la base de données avant la mise à niveau... ')) {
                    return 100;
                }
            } else {
                $io->text("Ignorer la <info>sauvegarde de la base de données</info>.");
            }
            
            if (!$input->getOption('fast')) {
                if (!$this->commandHelper->capsule(($input->getOption('phar') ? "$php composer.phar" : 'composer') . " dump-env $env", $io, 'Mise à jour des dépendances...', false)) {
                    return 5;
                }
            } else {
                $io->text("Ignorer les <info>mises à jour des dépendances</info>.");
            }
            
            $version = file_get_contents('VERSION');
            
            if (!$this->commandHelper->capsule("cache:clear", $io, 'Vidage du cache... ')) {
                return 8;
            }
            if (!$this->commandHelper->capsule(($input->getOption('phar') ? "$php composer.phar" : 'composer') . " dump-env dev", $io, 'Mise à jour des dépendances...', false)) {
                return 9;
            }
            if (!$this->commandHelper->capsule("app:migrate -u -r", $io, 'Mise à jour de la base de données... ')) {
                return 10;
            }
            if (!$this->commandHelper->capsule(($input->getOption('phar') ? "$php composer.phar" : 'composer') . " dump-env $env", $io, 'Mise à jour des dépendances...', false)) {
                return 11;
            }
            if (!$this->commandHelper->capsule("app:migrate -p -v", $io, 'Exécution de scripts de post-installation... ')) {
                return 12;
            }
            // On passe la conversion des traductions en json pour le front
            if (!$this->commandHelper->capsule("app:translation:convert -y", $io, 'Exécution de la conversion... ')) {
                return 14;
            }
            if (!$this->commandHelper->capsule("app:migrate -c " . (($discord) ? '--discord' : '') . ' --environment ' . $env, $io, 'Exécution du changelog... ')) {
                return 13;
            }
            if (!$this->commandHelper->capsule("cache:clear", $io, 'Vidage du cache... ')) {
                return 14;
            }
            
            if ($env === 'prod') {
                if ($input->getOption('stay-offline')) {
                    
                    $io->text("La maintenance est maintenue active. Désactiver avec '<info>app:migrate --maintenance off</info>'");
                }
            }
            
            if ($version) {
                $io->success("Mise à jour de Gest'Hordes vers la version $version");
            }
            
            return 0;
        }
        
        if ($input->getOption('install-db')) {
            
            $io->section("=== Installation de la base de données ===");
            
            if (!$this->commandHelper->capsule('doctrine:database:create --if-not-exists', $io)) {
                $io->error('Impossible de créer la base de données.');
                return 1;
            }
            
            if (!$this->commandHelper->capsule('doctrine:schema:update --force', $io)) {
                $io->error('Impossible de créer le schéma.');
                return 2;
            }
            
            if (!$this->commandHelper->capsule('doctrine:fixtures:load --append', $io)) {
                $io->error('Impossible de mettre à jour les fixtures.');
                return 3;
            }
            
            return 0;
        }
        
        if ($input->getOption('update-db')) {
            if (!$this->commandHelper->capsule('doctrine:migrations:diff --allow-empty-diff --formatted --no-interaction', $io)) {
                $io->error('Impossible de créer une migration.');
                return 1;
            }
            
            if (!$this->commandHelper->capsule('doctrine:migrations:migrate --all-or-nothing --allow-no-migration --no-interaction', $io)) {
                if ($input->getOption('recover')) {
                    $io->warning('Impossible de migrer la base de données, tentative de restauration.');
                    
                    $source = "{$this->param->get('kernel.project_dir')}/src/Migrations";
                    foreach (scandir($source) as $file) {
                        if ($file && $file[0] !== '.') {
                            $io->text("\tSuppression \"<comment>$file</comment>\"...");
                            unlink("$source/$file");
                            $io->text('<info>Ok.</info>');
                        }
                    }
                    
                    if (!$this->commandHelper->capsule('doctrine:migrations:version --all --delete --no-interaction', $io)) {
                        $io->error('Impossible de nettoyer les migrations.');
                        return 4;
                    }
                    
                    if (!$this->commandHelper->capsule('doctrine:migrations:diff --allow-empty-diff --formatted --no-interaction', $io)) {
                        $io->error('Impossible de créer une migration.');
                        return 1;
                    }
                    
                    if (!$this->commandHelper->capsule('doctrine:migrations:migrate --all-or-nothing --allow-no-migration --no-interaction', $io)) {
                        $io->error('Impossible de migrer la base de données.');
                        return 2;
                    }
                } else {
                    $io->error('Impossible de migrer la base de données.');
                    return 2;
                }
            }
            $version = file_get_contents('VERSION');
            if (!$this->commandHelper->capsule("app:update:prototype --update-version {$version}", $io)) {
                $io->error('Impossible de mettre à jour les fixtures.');
                return 3;
            }
            
            return 0;
        }
        
        if ($input->getOption('process-db-git')) {
            if (file_exists($this->kernel->getProjectDir() . '/.vslist')) {
                $io->section("Obtention de la liste des changements avec <info>.vslist</info>.");
                $hashes = file($this->kernel->getProjectDir() . '/.vslist', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            } else {
                $io->section("Obtention de la liste des changements depuis <info>git</info>.");
                $process = new Process(['git', 'rev-list', 'HEAD']);
                $process->run();
                
                if (!$process->isSuccessful()) {
                    throw new RuntimeException($process->getErrorOutput());
                }
                
                $hashes = array_reverse(explode("\n", trim($process->getOutput())));
            }
            
            $io->text('<info>' . count($hashes) . '</info> correctifs installés trouvés.');
            
            $new = 0;
            
            $git_repo = $this->entityManager->getRepository(GitVersions::class);
            
            foreach ($hashes as $hash) {
                if ($git_repo->count(['version' => trim($hash)]) === 0) {
                    $new++;
                    $this->entityManager->persist((new GitVersions())->setVersion(trim($hash))->setInstalled(false));
                }
            }
            
            if ($new > 0) {
                $this->entityManager->flush();
                $io->text("Des correctifs, <info>$new</info> ont été récemment découverts.");
            } else {
                $io->text("<info>Aucun</info> correctif n'a été récemment découvert.");
            }
            
            /** @var string[] $uninstalled */
            $uninstalled = array_map(fn(GitVersions $g) => $g->getVersion(), $git_repo->findBy(['installed' => false], ['id' => 'ASC']));
            
            $nombreAInstaller = count($uninstalled);
            if ($nombreAInstaller > 0) {
                $io->text("Fin de la configuration de la base de données pour les correctifs <info>{$nombreAInstaller}</info>");
            } else {
                $io->text("Aucun correctif marqué pour l'installation");
            }
            
            foreach ($uninstalled as $version) {
                if (isset(static::$git_script_repository[$version])) {
                    $this->entityManager->flush();
                    $io->text("\tInstallation de <comment>$version</comment>...");
                    foreach (static::$git_script_repository[$version] as $script) {
                        
                        if ($script[0] === 'app:migrate') {
                            $input = new ArrayInput($script[1]);
                        }
                        
                        $input->setInteractive(false);
                        
                        try {
                            if ($script[0] === 'app:migrate') {
                                $this->getApplication()->find($script[0])->run($input, $io);
                            } else {
                                $this->commandHelper->capsule($script[0], $io, null, true);
                            }
                        } catch (Exception $e) {
                            $io->error($e->getMessage());
                            
                            return 1;
                        } catch (ExceptionInterface $e) {
                            $io->error($e->getMessage());
                            return 1;
                        }
                    }
                    
                    $io->success("\t<info>OK!</info>");
                    
                }
                
                $version_object =
                    $this->entityManager->getRepository(GitVersions::class)->findOneBy(['version' => $version]);
                $this->entityManager->persist($version_object->setInstalled(true));
                $this->entityManager->flush();
            }
            
            $this->entityManager->flush();
            
            return 0;
        }
        
        if ($input->getOption('changelog')) {
            
            $env = $input->getOption('environment') ?? 'dev';
            
            $fichierChangelog = 'changelog.json';
            $contentChangelog = 'contentsChangelog.json';
            
            
            $fichierlu = json_decode(file_get_contents($fichierChangelog), true, 512, JSON_THROW_ON_ERROR) ?? [];
            $contenslu = json_decode(file_get_contents($contentChangelog), true, 512, JSON_THROW_ON_ERROR) ?? [];
            
            $versionFichier = file_get_contents('VERSION');
            $versionFichier = preg_replace('/\n/', '', $versionFichier);
            
            $all_version_value = $this->entityManager->getRepository(VersionsSite::class)->findAll();
            
            $all_version_key = array_map(function (VersionsSite $version) {
                if ($version->getTagName() !== null) {
                    $tag = '-' . $version->getTagName();
                    
                    if ($version->getNumTag() !== null) {
                        $tag .= '.' . $version->getNumTag();
                    }
                }
                
                return 'v' . $version->getVersionMajeur() . '.' . $version->getVersionMineur() . '.' .
                       $version->getVersionCorrective() . ($tag ?? '');
            }, $all_version_value);
            
            
            $all_version = array_combine($all_version_key, $all_version_value);
            
            $io->section("=== Mise à jour du changelog ===");
            $io->progressStart(count($fichierlu));
            
            
            $userEra = $this->entityManager->getRepository(User::class)->findOneBy(['idMyHordes' => $_ENV['ID_ERA']]);
            
            foreach ($fichierlu as $version => $contentVersion) {
                
                preg_match('/^(\d+)\.(\d+)\.(\d+)(?:-(\w+))?/', (string)$version, $matches);
                
                $major = $matches[1];           // Le premier chiffre → correspond à la version majeure
                $minor = $matches[2];           // Le deuxième chiffre → correspond à la version mineure
                $patch = $matches[3];           // Le troisième chiffre → correspond à la version corrective
                $label = $matches[4] ?? null;   // L'étiquette qui peut être nul si elle n'existe pas.
                
                $version_existante = $all_version['v' . $version] ?? null;
                
                if ($version_existante === null) {
                    $version_existante = new VersionsSite();
                    $version_existante->setVersionMajeur($major)
                                      ->setVersionMineur($minor)
                                      ->setVersionCorrective($patch)
                                      ->setTagName($label);
                    
                    if (isset($contentVersion['dateVersion'])) {
                        $dateVersion = DateTime::createFromFormat("d/m/Y H:i", $contentVersion['dateVersion']);
                    } else {
                        $dateVersion = new DateTime('now');
                    }
                    
                    $version_existante->setDateVersion($dateVersion);
                }
                
                $version_existante->setTitre($contentVersion['titre'] ?? ('Mise à jour v' . $version));
                
                if (isset($contentVersion['contents'])) {
                    foreach ($contentVersion['contents'] as $content) {
                        if ($version_existante->getContent($content) !== null) {
                            $contentObj = $version_existante->getContent($content);
                            $contentObj->setContents($contenslu[$content])
                                       ->setTypeContent(ContentsVersion::FEATURE);
                        } else {
                            $contentObj = new ContentsVersion();
                            $contentObj->setContents($contenslu[$content])
                                       ->setIdRelatif($content)
                                       ->setTypeContent(ContentsVersion::FEATURE);
                            $version_existante->addContent($contentObj);
                        }
                    }
                }
                if (isset($contentVersion['contentsFix'])) {
                    foreach ($contentVersion['contentsFix'] as $content) {
                        if ($version_existante->getContent($content) !== null) {
                            $contentObj = $version_existante->getContent($content);
                            $contentObj->setContents($contenslu[$content])
                                       ->setTypeContent(ContentsVersion::FIX);
                        } else {
                            $contentObj = new ContentsVersion();
                            $contentObj->setContents($contenslu[$content])
                                       ->setIdRelatif($content)
                                       ->setTypeContent(ContentsVersion::FIX);
                            $version_existante->addContent($contentObj);
                        }
                    }
                }
                if (isset($contentVersion['contentsOther'])) {
                    foreach ($contentVersion['contentsOther'] as $content) {
                        if ($version_existante->getContent($content) !== null) {
                            $contentObj = $version_existante->getContent($content);
                            $contentObj->setContents($contenslu[$content])
                                       ->setTypeContent(ContentsVersion::OTHER);
                        } else {
                            $contentObj = new ContentsVersion();
                            $contentObj->setContents($contenslu[$content])
                                       ->setIdRelatif($content)
                                       ->setTypeContent(ContentsVersion::OTHER);
                            $version_existante->addContent($contentObj);
                        }
                    }
                }
                
                $this->entityManager->persist($version_existante);
                $this->entityManager->flush();
                $this->entityManager->refresh($version_existante);
                
                
                // Création d'une news de mise à jour liée à la version en cours si elle n'existe pas, sinon on met à jour :
                
                $tag = null;
                if ($version_existante->getTagName() !== null) {
                    $tag = '-' . $version_existante->getTagName();
                    
                    if ($version_existante->getNumTag() !== null) {
                        $tag .= '.' . $version_existante->getNumTag();
                    }
                }
                
                $newsBdd = $this->entityManager->getRepository(NewsSite::class)->findOneBy(
                    ['titre' => "Mise à jour Version {$version_existante->getVersionMajeur()}.{$version_existante->getVersionMineur()}.{$version_existante->getVersionCorrective()}" .
                                ($tag ?? '')],
                );
                
                
                if ($newsBdd === null) {
                    $news = new NewsSite();
                } else {
                    $news = $newsBdd;
                }
                
                $news->setTitre("Mise à jour Version {$version_existante->getVersionMajeur()}.{$version_existante->getVersionMineur()}.{$version_existante->getVersionCorrective()}" . ($tag ?? ''));
                
                $news->setVersion($version_existante)
                     ->setAuteur($userEra)
                     ->setDateAjout($version_existante->getDateVersion());
                
                $this->entityManager->persist($news);
                $this->entityManager->flush();
                
                $io->progressAdvance();
                
            }
            
            
            $io->progressFinish();
            $io->success("Changelog publié !");
            return 0;
            
        }
        
        if ($input->getOption('update-array-update')) {
            $io->section("=== Mise à jour des tableaux de jour d'update chantier ===");
            
            // récupération des updates chantiers en base de données
            $updates = $this->entityManager->getRepository(UpChantier::class)->findAll();
            
            $io->progressStart(count($updates));
            
            foreach ($updates as $update) {
                
                $update->setDays([$update->getDays()]);
                $this->entityManager->persist($update);
                $io->progressAdvance();
            }
            $this->entityManager->flush();
            
            $io->progressFinish();
            $io->success("Mise à jour des tableaux de jour d'update chantier effectuée !");
            return 0;
        }
        
        if ($v = $input->getOption("publish")) {
            $roleIdChangelog = 1213182719253811282;
            $versionFichier  = file_get_contents('VERSION');
            $versionFichier  = preg_replace('/\n/', '', $versionFichier);
            // séparation de la version pour récupérer les informations
            preg_match('/^v(\d+)\.(\d+)\.(\d+)(?:-(\w+))?/', (string)$versionFichier, $matches);
            
            $major = $matches[1];           // Le premier chiffre → correspond à la version majeure
            $minor = $matches[2];           // Le deuxième chiffre → correspond à la version mineure
            $patch = $matches[3];           // Le troisième chiffre → correspond à la version corrective
            $label = $matches[4] ?? null;   // L'étiquette qui peut être nul si elle n'existe pas.
            
            $version = $this->entityManager->getRepository(VersionsSite::class)->findOneBy([
                                                                                               'versionMajeur'     => $major,
                                                                                               'versionMineur'     => $minor,
                                                                                               'versionCorrective' => $patch,
                                                                                           ]);
            
            if ($version === null) {
                $io->error("La version {$v} n'existe pas !");
                return 1;
            }
            
            $message    = ":flag_fr: :warning: ***La mise à jour Version $versionFichier a été correctement installée, voici le <@&{$roleIdChangelog}>*** :warning:  :flag_fr: \n";
            $message_gb = ":flag_gb: :warning: ***The Version $versionFichier update has been successfully installed, here's the changelog*** :warning: :flag_gb: \n";
            
            $nouveaute  = [];
            $correction = [];
            $autre      = [];
            
            foreach ($version->getContents() as $content) {
                if ($content->getTypeContent() === ContentsVersion::FEATURE) {
                    $nouveaute[] = $content->getContents();
                }
                if ($content->getTypeContent() === ContentsVersion::FIX) {
                    $correction[] = $content->getContents();
                }
                if ($content->getTypeContent() === ContentsVersion::OTHER) {
                    $autre[] = $content->getContents();
                }
            }
            
            if (count($nouveaute) !== 0) {
                $message    .= "**Nouveauté(s) :** \n";
                $message_gb .= "**New(s) :** \n";
                
                foreach ($nouveaute as $new) {
                    $message    .= "- $new\n";
                    $message_gb .= "- {$this->translator->trans($new, [],'version', 'en')}\n";
                }
                
            }
            if (count($correction) !== 0) {
                $message    .= "**Correctif(s) :** \n";
                $message_gb .= "**Fix(es) :** \n";
                
                foreach ($correction as $correctif) {
                    $message    .= "- $correctif\n";
                    $message_gb .= "- {$this->translator->trans($correctif, [],'version', 'en')}\n";
                }
            }
            if (count($autre) !== 0) {
                $message    .= "**Autres :** \n";
                $message_gb .= "**Others :** \n";
                
                foreach ($autre as $lib) {
                    $message    .= "- $lib\n";
                    $message_gb .= "- {$this->translator->trans($lib, [],'version', 'en')}\n";
                }
                
            }
            
            $message    .= "\nA bientôt pour de nouvelle mise à jour !";
            $message_gb .= "\nSee you soon for a new update!";
            
            $message_tot = $message . " \n " . $message_gb;
            
            $tab_message = $this->splitMessage($message_tot);
            
            foreach ($tab_message as $message) {
                $this->sendMessageDiscord($message);
            }
            
            
        }
        
        
        return 99;
        
    }
    
    private function sendMessageDiscord(string $message): void
    {
        $client               = new Client();
        $channelIdAnnonce     = $_ENV['ID_ANNONCE'];
        $discordToken         = $_ENV['TOKEN_BOT'];
        $apiUrlDernierMessage = "https://discord.com/api/v10/channels/$channelIdAnnonce/messages";
        $roleIdMaintenance    = 1213175522788380752;
        
        try {
            // Envoyez la requête GET à l'API Discord avec le jeton d'accès dans l'en-tête Authorization
            $response = $client->get($apiUrlDernierMessage, [
                'headers' => [
                    'Authorization' => "Bot $discordToken",
                    'Content-Type'  => 'application/json',
                ],
                'query'   => [
                    'limit' => 1, // Récupère le dernier message (ici 1 message)
                ],
            ]);
            
            // Analysez la réponse JSON pour obtenir l'ID du dernier message
            $data = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
            if (!empty($data)) {
                $latestMessageId      = $data[0]['id'];
                $latestMessageContent = $data[0]['content'];
                
                if ($latestMessageContent ===
                    ":flag_fr: :warning: ***<@&{$roleIdMaintenance}> en cours, installation de la prochaine mise à jour*** :warning: :flag_fr:\n:flag_gb: :warning: ***<@&{$roleIdMaintenance}> in progress, installing the next update*** :warning: :flag_gb:") {
                    $apiUrlDelete = "https://discord.com/api/v10/channels/$channelIdAnnonce/messages/$latestMessageId";
                    
                    $client->delete($apiUrlDelete, [
                        'headers' => [
                            'Authorization' => "Bot $discordToken",
                            'Content-Type'  => 'application/json',
                        ]]);
                }
            }
            
            
        } catch (Exception|GuzzleException $e) {
            echo "Erreur lors de l'appel à l'API Discord : " . $e->getMessage();
            return;
        }
        
        
        // Ecriture du message discord pour annoncer la fin de maintenance
        $apiUrl = "https://discord.com/api/v10/channels/$channelIdAnnonce/messages";
        
        try {
            // Envoyez la requête GET à l'API Discord avec le jeton d'accès dans l'en-tête Authorization
            $client->post($apiUrl, [
                'headers' => [
                    'Authorization' => "Bot $discordToken",
                    'Content-Type'  => 'application/json',
                ],
                'json'    => [
                    'content' => $message,
                ],
            ]);
            
            return;
            
        } catch (Exception|GuzzleException $e) {
            echo "Erreur lors de l'appel à l'API Discord : " . $e->getMessage();
            return;
        }
        
    }
    
    private function splitMessage(string $message): array
    {
        $maxCharLimit = 2000;
        $messages     = [];
        
        $paragraphs = explode("\n", $message);
        
        $currentMessage = "";
        foreach ($paragraphs as $paragraph) {
            $newLength = strlen($currentMessage) + strlen($paragraph) + 1; // +1 for the newline character
            
            if ($newLength <= $maxCharLimit) {
                $currentMessage .= $paragraph . "\n";
            } else {
                $messages[]     = $currentMessage;
                $currentMessage = $paragraph . "\n";
            }
        }
        
        if (!empty($currentMessage)) {
            $messages[] = $currentMessage;
        }
        
        return $messages;
    }
    
}