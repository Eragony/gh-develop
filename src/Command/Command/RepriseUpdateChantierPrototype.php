<?php

namespace App\Command\Command;

use App\Entity\ChantierPrototype;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:migration:git:reprise-update-chantiers',
)]
class RepriseUpdateChantierPrototype extends Command
{
    
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $connection = $this->entityManager->getConnection();
            
            // 1. Désactiver les clés étrangères en premier
            $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0');
            
            // 2. Nettoyer directement les tables
            $connection->executeStatement('TRUNCATE TABLE up_chantier_prototype');
            $connection->executeStatement('TRUNCATE TABLE bonus_upChantiers');
            $connection->executeStatement('TRUNCATE TABLE bonus_up_chantier');
            $connection->executeStatement('TRUNCATE TABLE chantiers_bonusUp');
            
            // 3. Réactiver les clés étrangères
            $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1');
            
            // 4. Clear les relations en mémoire
            $chantiers = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
            foreach ($chantiers as $chantier) {
                $chantier->getLevelUps()->clear();
            }
            
            // 5. Flush pour synchroniser
            $this->entityManager->flush();
            $this->entityManager->clear();
            
            $io->success('Les levelUps ont été nettoyés avec succès !');
            
            return Command::SUCCESS;
            
        } catch (Exception $e) {
            $io->error([
                           'Une erreur est survenue pendant le nettoyage:',
                           $e->getMessage()
                       ]);
            
            // En cas d'erreur, on essaie de réactiver les foreign keys
            try {
                $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1');
            } catch (\Exception $e) {
                // On ignore cette erreur car secondaire
            }
            
            return Command::FAILURE;
        }
    }
}