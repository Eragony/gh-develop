<?php

namespace App\Command\Command;

use App\Entity\ThemeUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:migration:git:add-personnalisation-theme',
)]
class AddPersonnalisationTheme extends Command
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet \'ajouter des data avec l\'ajout de nouvelle ligne dans l\'entité ThemeUser')
             ->setHelp('Add defaut value.');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        // récupération de tous les themes des users
        $themes = $this->em->getRepository(ThemeUser::class)->findAll();
        
        $io->title('Ajout de données dans l\'entité ThemeUser');
        $io->text('Ajout en cours...');
        // barre de progression
        $io->progressStart(count($themes));
        
        foreach ($themes as $key => $theme) {
            if ($theme->getBaseTheme() === "light" || $theme->getBaseTheme() === "vintage") {
                $theme->setSuccesColor("#198754FF")
                      ->setErrorColor("#ff0000FF");
            } else {
                $theme->setSuccesColor("#00f300FF")
                      ->setErrorColor("#ff0000FF");
            }
            
            $this->em->persist($theme);
            
            // si on est un multiple de 200, on flush
            if ($key % 200 === 0) {
                $this->em->flush();
            }
            
            $io->progressAdvance();
        }
        
        $this->em->flush();
        $io->progressFinish();
        $io->success('Transfert terminé.');
        
        
        return Command::SUCCESS;
    }
    
}
