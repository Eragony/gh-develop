<?php

namespace App\Command\DataMH;

use App\Entity\CaracteristiquesItem;
use App\Entity\ItemPrototype;
use App\Entity\TypeCaracteristique;
use App\Entity\TypeDecharge;
use App\Service\CommandHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:migration:mh:item-property',
)]
class ImportItemPropertyCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly CommandHelper $commandHelper)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription("Permet d'intégrer des datas depuis les tableaux de MH")
             ->setHelp('Transfert de data.');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        if (!$this->commandHelper->capsule('app:update:prototype --maj-type-carac', $io)) {
            $io->error('Impossible de mettre à jour les types de carac');
            return 1;
        }
        
        // Récupération de tous les chantiers de décharge
        /**
         * Déchardes de bois (1 - 112)
         * Ferraillerie (2 - 112)
         * Enclos (3 - 113)
         * Décharge piégée (4 - 116)
         * Appâts odorants (5 - 117)
         * Décharge blindée (6 - 118)
         */
        $listChantiers = $this->em->getRepository(TypeDecharge::class)->findAll();
        
        foreach ($listChantiers as $key => $chantier) {
            $listDecharge[$chantier->getId()] = $chantier;
        }
        
        $listItems = $this->getProperty();
        
        $io->title('Mise à jour des décharges');
        $io->text('Transfert en cours...');
        
        foreach ($listItems as $key => $value) {
            $item = $this->em->getRepository(ItemPrototype::class)->findOneBy(['uid' => $key]);
            
            if ($item) {
                foreach ($value as $property) {
                    // On va traiter les items qui passent en décharges
                    if ($property === "weapon" || in_array($item->getUid(), [
                            'machine_gun_#00', 'gun_#00', 'chair_basic_#00', 'machine_1_#00', 'machine_2_#00', 'machine_3_#00', 'pc_#00',
                        ])
                    ) {
                        $item->setTypeDecharge($listDecharge[4])
                             ->setDecharge($listDecharge[4]);
                    }
                    
                    if ($property === "defence" && $item->getUid() !== 'tekel_#00' && $item->getUid() !== 'pet_dog_#00'
                        && $item->getUid() !== 'concrete_wall_#00' && $item->getUid() !== 'table_#00'
                    ) {
                        $item->setTypeDecharge($listDecharge[6])
                             ->setDecharge($listDecharge[6]);
                    }
                    
                    if ($property === "food") {
                        $item->setTypeDecharge($listDecharge[5])
                             ->setDecharge($listDecharge[5]);
                    }
                    
                    if ($property === "pet") {
                        $item->setTypeDecharge($listDecharge[3])
                             ->setDecharge($listDecharge[3]);
                    }
                    
                    if ($item->getUid() === "wood_bad_#00" || $item->getUid() === "wood2_#00") {
                        $item->setTypeDecharge($listDecharge[1])
                             ->setDecharge($listDecharge[1]);
                    }
                    if ($item->getUid() === "metal_bad_#00" || $item->getUid() === "metal_#00") {
                        $item->setTypeDecharge($listDecharge[2])
                             ->setDecharge($listDecharge[2]);
                    }
                    
                    
                }
            }
        }
        
        $io->title('Transfert des données de MH pour les propriétés des items');
        $io->text('Transfert en cours...');
        
        // on récupère tous les items
        $listItems = $this->em->getRepository(ItemPrototype::class)->findAll();
        // On récupère tous les types de caractéristiques
        $listTypeCarac = $this->em->getRepository(TypeCaracteristique::class)->createQueryBuilder('tc', 'tc.id')->getQuery()->getResult();
        
        $listProperties = $this->getProperty();
        $io->progressStart(count($listItems));
        $lastId = $this->em->getRepository(CaracteristiquesItem::class)->getLastId();
        foreach ($listItems as $item) {
            // On récupère les propriétés spécifiques de l'item
            $property = $listProperties[$item->getUid()] ?? [];
            
            if (in_array("nw_armory", $property)) {
                $item->setArmurerie(true);
            } else {
                $item->setArmurerie(false);
            }
            
            if (in_array("nw_ikea", $property)) {
                $item->setMagasin(true);
            } else {
                $item->setMagasin(false);
            }
            
            if (in_array("nw_shooting", $property)) {
                $item->setTourelle(true);
            } else {
                $item->setTourelle(false);
            }
            
            if (in_array("nw_trebuchet", $property)) {
                $item->setLanceBete(true);
            } else {
                $item->setLanceBete(false);
            }
            
            $this->em->persist($item);
            $io->progressAdvance();
        }
        
        
        $this->em->flush();
        $io->progressFinish();
        $io->success('Transfert terminé.');
        
        
        return Command::SUCCESS;
    }
    
    protected function getProperty(): array
    {
        return [
            'saw_tool_#00'               => ['impoundable', 'can_opener', 'box_opener'],
            'saw_tool_part_#00'          => ['impoundable'],
            'can_opener_#00'             => ['weapon', 'can_opener', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'screw_#00'                  => ['weapon', 'can_opener', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'swiss_knife_#00'            => ['weapon', 'can_opener', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'wrench_#00'                 => ['weapon', 'box_opener', 'parcel_opener', 'parcel_opener_h'],
            'cutter_#00'                 => ['weapon', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'small_knife_#00'            => ['weapon', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'bone_#00'                   => ['weapon', 'box_opener', 'parcel_opener', 'parcel_opener_h'],
            'cutcut_#00'                 => ['impoundable', 'weapon', 'box_opener', 'esc_fixed', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'hurling_stick_#00'          => ['weapon', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'chair_basic_#00'            => ['box_opener', 'nw_ikea', 'parcel_opener_h', 'amenagement'],
            'chair_#00'                  => ['nw_ikea', 'amenagement'],
            'staff_#00'                  => ['weapon', 'box_opener', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'chain_#00'                  => ['impoundable', 'weapon', 'box_opener', 'esc_fixed', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'cigs_#00'                   => ['impoundable'],
            'pc_#00'                     => ['box_opener', 'nw_ikea', 'amenagement'],
            'door_#00'                   => ['impoundable', 'defence', 'nw_ikea'],
            'car_door_#00'               => ['impoundable', 'defence'],
            'car_door_part_#00'          => ['impoundable'],
            'pet_dog_#00'                => ['defence', 'pet', 'esc_fixed', 'lock', 'nw_trebuchet'],
            'plate_#00'                  => ['impoundable', 'defence', 'deco'],
            'plate_raw_#00'              => ['impoundable'],
            'torch_#00'                  => ['impoundable', 'defence', 'weapon', 'prevent_night'],
            'tekel_#00'                  => ['defence', 'lock', 'pet', 'nw_trebuchet'],
            'trestle_#00'                => ['impoundable', 'defence', 'nw_ikea'],
            'table_#00'                  => ['impoundable', 'defence', 'nw_ikea'],
            'bed_#00'                    => ['impoundable', 'defence', 'nw_ikea'],
            'wood_plate_#00'             => ['impoundable', 'defence', 'deco'],
            'concrete_#00'               => ['impoundable'],
            'concrete_wall_#00'          => ['impoundable', 'defence'],
            'wood_bad_#00'               => ['impoundable', 'ressource'],
            'metal_bad_#00'              => ['impoundable', 'ressource'],
            'wood2_#00'                  => ['impoundable', 'ressource', 'hero_find'],
            'metal_#00'                  => ['impoundable', 'ressource', 'hero_find'],
            'wood_beam_#00'              => ['impoundable', 'ressource'],
            'metal_beam_#00'             => ['impoundable', 'ressource'],
            'courroie_#00'               => ['impoundable', 'ressource'],
            'deto_#00'                   => ['impoundable', 'ressource'],
            'tube_#00'                   => ['impoundable', 'ressource'],
            'rustine_#00'                => ['impoundable', 'ressource'],
            'electro_#00'                => ['impoundable', 'ressource'],
            'meca_parts_#00'             => ['impoundable', 'ressource'],
            'explo_#00'                  => ['impoundable', 'ressource'],
            'pumpkin_on_#00'             => ['impoundable', 'defence'],
            'pumpkin_off_#00'            => ['impoundable', 'amenagement'],
            'pumpkin_raw_#00'            => ['impoundable'],
            'mecanism_#00'               => ['ressource', 'hero_find_lucky'],
            'grenade_#00'                => ['weapon', 'hero_find', 'hero_find_lucky', 'nw_shooting'],
            'bgrenade_#00'               => ['weapon', 'nw_shooting'],
            'boomfruit_#00'              => ['weapon'],
            'pilegun_#00'                => ['weapon'],
            'pilegun_up_#00'             => ['impoundable', 'weapon', 'esc_fixed'],
            'pilegun_up_empty_#00'       => ['impoundable'],
            'pilegun_upkit_#00'          => ['impoundable'],
            'big_pgun_#00'               => ['impoundable', 'weapon', 'esc_fixed'],
            'big_pgun_empty_#00'         => ['impoundable'],
            'big_pgun_part_#00'          => ['impoundable'],
            'mixergun_#00'               => ['weapon'],
            'chainsaw_#00'               => ['impoundable', 'weapon', 'box_opener', 'esc_fixed', 'nw_armory', 'parcel_opener_h'],
            'chainsaw_empty_#00'         => ['impoundable'],
            'chainsaw_part_#00'          => ['impoundable'],
            'taser_#00'                  => ['weapon'],
            'lpoint4_#00'                => ['impoundable', 'weapon', 'esc_fixed'],
            'lpoint3_#00'                => ['impoundable', 'weapon', 'esc_fixed'],
            'lpoint2_#00'                => ['impoundable', 'weapon', 'esc_fixed'],
            'lpoint1_#00'                => ['impoundable', 'weapon', 'esc_fixed'],
            'watergun_opt_5_#00'         => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'watergun_opt_4_#00'         => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'watergun_opt_3_#00'         => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'watergun_opt_2_#00'         => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'watergun_opt_1_#00'         => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'watergun_opt_part_#00'      => ['impoundable'],
            'watergun_opt_empty_#00'     => ['impoundable'],
            'kalach_#00'                 => ['impoundable', 'nw_shooting'],
            'watergun_3_#00'             => ['weapon', 'nw_shooting'],
            'watergun_2_#00'             => ['weapon', 'nw_shooting'],
            'watergun_1_#00'             => ['weapon', 'nw_shooting'],
            'jerrygun_#00'               => ['impoundable', 'weapon', 'esc_fixed', 'nw_shooting'],
            'jerrygun_part_#00'          => ['impoundable'],
            'jerrygun_off_#00'           => ['impoundable'],
            'jerrycan_#00'               => ['impoundable', 'hero_find_lucky'],
            'knife_#00'                  => ['impoundable', 'weapon', 'box_opener', 'esc_fixed', 'nw_armory', 'parcel_opener', 'parcel_opener_h'],
            'lawn_#00'                   => ['weapon', 'nw_armory'],
            'torch_off_#00'              => ['weapon'],
            'iphone_#00'                 => ['weapon'],
            'machine_1_#00'              => ['esc_fixed', 'nw_ikea', 'impoundable', 'amenagement'],
            'machine_2_#00'              => ['esc_fixed', 'nw_ikea', 'impoundable', 'amenagement'],
            'machine_3_#00'              => ['esc_fixed', 'nw_ikea', 'impoundable', 'amenagement'],
            'disinfect_#00'              => ['impoundable', 'drug'],
            'drug_#00'                   => ['can_poison', 'impoundable', 'drug'],
            'drug_hero_#00'              => ['impoundable', 'drug', 'esc_fixed'],
            'drug_random_#00'            => ['drug'],
            'beta_drug_bad_#00'          => ['drug'],
            'beta_drug_#00'              => ['impoundable', 'drug'],
            'xanax_#00'                  => ['drug', 'hero_find_lucky'],
            'drug_water_#00'             => ['drug'],
            'bandage_#00'                => ['impoundable', 'drug'],
            'pharma_#00'                 => ['impoundable', 'drug'],
            'pharma_part_#00'            => ['drug'],
            'lsd_#00'                    => ['impoundable', 'drug'],
            'april_drug_#00'             => ['drug', 'can_cook'],
            'fungus_#00'                 => ['food', 'inedible', 'can_cook'],
            'radio_on_#00'               => ['impoundable', 'nw_ikea', 'amenagement'],
            'water_#00'                  => ['impoundable', 'can_poison', 'hero_find', 'esc_fixed', 'hero_find_lucky', 'found_poisoned', 'is_water'],
            'can_open_#00'               => ['can_poison', 'food', 'can_cook', 'single_use'],
            'vegetable_#00'              => ['can_poison', 'food', 'can_cook', 'single_use'],
            'fruit_#00'                  => ['can_poison', 'food', 'can_cook', 'single_use'],
            'water_can_3_#00'            => ['impoundable', 'esc_fixed', 'is_water', 'nw_shooting'],
            'water_can_2_#00'            => ['impoundable', 'esc_fixed', 'is_water', 'nw_shooting'],
            'water_can_1_#00'            => ['impoundable', 'esc_fixed', 'is_water', 'nw_shooting'],
            'water_can_empty_#00'        => ['impoundable'],
            'water_cup_#00'              => ['is_water', 'esc_fixed'],
            'potion_#00'                 => ['impoundable', 'is_water'],
            'lock_#00'                   => ['lock', 'amenagement'],
            'dfhifi_#01'                 => ['lock', 'amenagement'],
            'pile_#00'                   => ['impoundable', 'hero_find', 'hero_find_lucky'],
            'food_bag_#00'               => ['hero_find'],
            'rsc_pack_3_#00'             => ['impoundable', 'hero_find_lucky'],
            'rsc_pack_2_#00'             => ['impoundable', 'hero_find'],
            'rsc_pack_1_#00'             => ['impoundable'],
            'bretz_#00'                  => ['food', 'can_cook', 'single_use'],
            'undef_#00'                  => ['food', 'can_cook', 'single_use'],
            'dish_#00'                   => ['food', 'single_use'],
            'chama_#00'                  => ['food', 'can_cook', 'single_use'],
            'food_bar1_#00'              => ['food', 'can_cook', 'single_use'],
            'food_bar2_#00'              => ['food', 'can_cook', 'single_use'],
            'food_bar3_#00'              => ['food', 'can_cook', 'single_use'],
            'food_biscuit_#00'           => ['food', 'can_cook', 'single_use'],
            'food_chick_#00'             => ['food', 'can_cook', 'single_use'],
            'food_pims_#00'              => ['food', 'can_cook', 'single_use'],
            'food_tarte_#00'             => ['food', 'can_cook', 'single_use'],
            'food_sandw_#00'             => ['food', 'can_cook', 'single_use'],
            'food_noodles_#00'           => ['food', 'can_cook', 'single_use'],
            'hmeat_#00'                  => ['food', 'can_cook', 'single_use', 'nw_trebuchet'],
            'bone_meat_#00'              => ['food', 'can_cook', 'single_use', 'nw_trebuchet'],
            'cadaver_#00'                => ['food', 'can_cook', 'single_use'],
            'food_noodles_hot_#00'       => ['food', 'esc_fixed', 'single_use'],
            'meat_#00'                   => ['food', 'esc_fixed', 'hero_find_lucky', 'single_use'],
            'vegetable_tasty_#00'        => ['food', 'esc_fixed', 'single_use'],
            'dish_tasty_#00'             => ['impoundable', 'food', 'esc_fixed', 'single_use'],
            'food_candies_#00'           => ['food', 'esc_fixed', 'single_use'],
            'chama_tasty_#00'            => ['food', 'esc_fixed', 'single_use'],
            'woodsteak_#00'              => ['food', 'esc_fixed', 'single_use'],
            'egg_#00'                    => ['food', 'esc_fixed', 'single_use'],
            'apple_#00'                  => ['food', 'esc_fixed', 'single_use'],
            'angryc_#00'                 => ['pet', 'weapon', 'nw_trebuchet', 'amenagement'],
            'pet_cat_#00'                => ['pet', 'nw_trebuchet', 'amenagement'],
            'pet_chick_#00'              => ['pet', 'nw_trebuchet'],
            'pet_pig_#00'                => ['pet', 'nw_trebuchet'],
            'pet_rat_#00'                => ['pet', 'nw_trebuchet'],
            'pet_snake_#00'              => ['pet', 'nw_trebuchet'],
            'pet_snake2_#00'             => ['pet', 'nw_trebuchet'],
            'renne_#00'                  => ['impoundable', 'nw_trebuchet'],
            'book_gen_letter_#00'        => ['esc_fixed'],
            'book_gen_box_#00'           => ['esc_fixed'],
            'postal_box_#00'             => ['esc_fixed'],
            'postal_box_#01'             => ['esc_fixed'],
            'pocket_belt_#00'            => ['esc_fixed', 'no_post'],
            'bag_#00'                    => ['esc_fixed', 'no_post'],
            'bagxl_#00'                  => ['esc_fixed', 'no_post'],
            'cart_#00'                   => ['esc_fixed', 'no_post', 'nw_ikea'],
            'radius_mk2_#00'             => ['impoundable', 'esc_fixed'],
            'radius_mk2_part_#00'        => ['impoundable'],
            'rp_book_#00'                => ['esc_fixed'],
            'rp_book_#01'                => ['esc_fixed'],
            'rp_book2_#00'               => ['esc_fixed'],
            'rp_scroll_#00'              => ['esc_fixed'],
            'rp_scroll_#01'              => ['esc_fixed'],
            'rp_sheets_#00'              => ['esc_fixed'],
            'rp_letter_#00'              => ['esc_fixed'],
            'rp_manual_#00'              => ['esc_fixed'],
            'lilboo_#00'                 => ['esc_fixed', 'prevent_terror'],
            'lights_#00'                 => ['impoundable'],
            'rp_twin_#00'                => ['esc_fixed'],
            'home_box_#00'               => ['impoundable', 'amenagement'],
            'home_box_xl_#00'            => ['impoundable', 'amenagement'],
            'lamp_#00'                   => ['nw_ikea', 'amenagement'],
            'lamp_on_#00'                => ['nw_ikea', 'prevent_night', 'amenagement'],
            'music_#00'                  => ['nw_ikea', 'amenagement'],
            'distri_#00'                 => ['nw_ikea', 'amenagement'],
            'guitar_#00'                 => ['impoundable', 'nw_ikea', 'amenagement'],
            'bureau_#00'                 => ['nw_ikea', 'amenagement'],
            'rlaunc_#00'                 => ['impoundable', 'nw_armory', 'weapon'],
            'repair_one_#00'             => ['impoundable', 'hero_find_lucky'],
            'electro_box_#00'            => ['hero_find_lucky'],
            'christmas_candy_#00'        => ['food', 'can_cook', 'single_use'],
            'omg_this_will_kill_you_#00' => ['can_cook'],
            'chudol_#00'                 => ['prevent_terror', 'amenagement'],
            'maglite_1_#00'              => ['prevent_night', 'amenagement'],
            'maglite_2_#00'              => ['prevent_night', 'amenagement'],
            'wood_xmas_#00'              => ['food', 'single_use'],
            'fence_#00'                  => ['impoundable', 'amenagement'],
            'engine_#00'                 => ['impoundable'],
            'engine_part_#00'            => ['impoundable'],
            'repair_kit_#00'             => ['impoundable'],
            'repair_kit_part_#00'        => ['impoundable'],
            'repair_kit_part_raw_#00'    => ['impoundable'],
            'home_def_#00'               => ['impoundable', 'amenagement'],
            'firework_box_#00'           => ['impoundable', 'amenagement'],
            'firework_powder_#00'        => ['impoundable', 'amenagement'],
            'firework_tube_#00'          => ['impoundable', 'amenagement'],
            'badge_#00'                  => ['impoundable', 'nw_ikea'],
            'wood_log_#00'               => ['impoundable', 'amenagement'],
            'chkspk_#00'                 => ['impoundable'],
            'claymo_#00'                 => ['impoundable'],
            'paques_#00'                 => ['impoundable'],
            'trapma_#00'                 => ['impoundable', 'amenagement'],
            'christmas_suit_full_#00'    => ['impoundable'],
            'christmas_suit_1_#00'       => ['impoundable'],
            'christmas_suit_2_#00'       => ['impoundable'],
            'christmas_suit_3_#00'       => ['impoundable'],
            'leprechaun_suit_#00'        => ['impoundable'],
            'bplan_box_#00'              => ['impoundable'],
            'bplan_box_e_#00'            => ['impoundable'],
            'bplan_drop_#00'             => ['impoundable'],
            'bplan_c_#00'                => ['impoundable'],
            'bplan_u_#00'                => ['impoundable'],
            'bplan_r_#00'                => ['impoundable'],
            'bplan_e_#00'                => ['impoundable'],
            'hbplan_u_#00'               => ['impoundable'],
            'hbplan_r_#00'               => ['impoundable'],
            'hbplan_e_#00'               => ['impoundable'],
            'bbplan_u_#00'               => ['impoundable'],
            'bbplan_r_#00'               => ['impoundable'],
            'bbplan_e_#00'               => ['impoundable'],
            'mbplan_u_#00'               => ['impoundable'],
            'mbplan_r_#00'               => ['impoundable'],
            'mbplan_e_#00'               => ['impoundable'],
            'flash_#00'                  => ['nw_armory'],
            'gun_#00'                    => ['nw_armory', 'amenagement'],
            'machine_gun_#00'            => ['nw_armory', 'amenagement'],
            'cinema_#00'                 => ['nw_ikea'],
            'deco_box_#00'               => ['nw_ikea'],
            'vodka_de_#00'               => ['drunk'],
            'rhum_#00'                   => ['drunk'],
            'fest_#00'                   => ['drunk'],
            'hmbrew_#00'                 => ['drunk'],
            'vodka_#00'                  => ['drunk'],
            'guiness_#00'                => ['drunk'],
            'carpet_#00'                 => ['amenagement'],
            'cdbrit_#00'                 => ['amenagement'],
            'cdelvi_#00'                 => ['amenagement'],
            'cdphil_#00'                 => ['amenagement'],
            'coffee_machine_#00'         => ['amenagement'],
            'dfhifi_#00'                 => ['amenagement'],
            'door_carpet_#00'            => ['amenagement'],
            'hifiev_#00'                 => ['amenagement'],
            'maglite_off_#00'            => ['amenagement'],
            'money_#00'                  => ['amenagement'],
            'music_part_#00'             => ['amenagement'],
            'teddy_#00'                  => ['amenagement'],
            'teddy_#01'                  => ['amenagement'],
            'xmas_gift_#00'              => ['amenagement'],
            'xmas_gift_#01'              => ['amenagement'],
        ];
    }
    
}
