<?php

namespace App\Command\Migration;

use App\Service\Utils\DiscordService;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand('app:maintenance')]
class MaintenanceCommand extends Command
{
    
    
    public function __construct(
        private readonly ParameterBagInterface $param,
        private readonly DiscordService        $discordService,
    )
    {
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de mettre le site en maintenance')
             ->addOption('maintenance', 'm', InputOption::VALUE_REQUIRED, 'Active ou desactive le mode maintenance')
             ->addOption('discord', null, InputOption::VALUE_NONE, 'Si défini, affiche un message discord');
    }
    
    
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $roleIdMaintenance = $_ENV['ID_MAINTENANCE'];
            $m                 = $input->getOption('maintenance');
            $discord           = $input->getOption('discord');
            $file              = "{$this->param->get('kernel.project_dir')}/public/maintenance/.active";
            
            if ($m === 'on') {
                $this->countdown($io, 'Activation du mode maintenance');
                $io->comment('Mise en mode maintenance...');
                file_put_contents($file, "");
                
                if ($discord) {
                    $message = ":flag_fr: :warning: ***<@&{$roleIdMaintenance}> en cours, installation de la prochaine mise à jour*** :warning: :flag_fr:\n:flag_gb: :warning: ***<@&{$roleIdMaintenance}> in progress, installing the next update*** :warning: :flag_gb:";
                    $this->discordService->generateMessageAnnonceDiscord($message);
                }
                $io->success("Mise en maintenance ok");
            } elseif ($m === 'off') {
                $this->countdown($io, 'Désactivation du mode maintenance');
                unlink($file);
                $io->comment("Fin du mode maintenance...");
                
                if ($discord) {
                    $message = ":flag_fr: :warning: ***Fin de maintenance*** :warning: :flag_fr:\n:flag_gb: :warning: ***End of maintenance*** :warning: :flag_gb:";
                    $this->discordService->generateMessageAnnonceDiscord($message);
                }
                $io->success("Fin de maintenance ok");
            } else {
                $io->error("Commande inconnu : {$m}");
                return Command::FAILURE;
            }
            
            return Command::SUCCESS;
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        
    }
    
    private function countdown(SymfonyStyle $io, string $message): void
    {
        for ($i = 3; $i > 0; --$i) {
            $io->text("$message dans <info>$i</info> secondes...");
            sleep(1);
        }
    }
    
}