<?php

declare(strict_types=1);

namespace App\Command\Migration;

use App\Entity\GitVersions;
use App\Service\CommandHelper;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

#[AsCommand(name: 'app:git_data_migration', description: 'Mise à jour des données via git')]
class GitDataMigrationCommand extends Command
{
    protected static array $git_script_repository = [
        'b2c4de8a3ad2753a92f74f8d0819dd3a9ceed50b' => [['app:migration:git:transfert']],
        '061fee440d8a640db24b26ba042b599e4400d9ff' => [['app:migration:git:reprise-plans-chantiers']],
    ];
    
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly KernelInterface        $kernel,
        private readonly CommandHelper          $commandHelper,
    )
    {
        
        parent::__construct();
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            
            $projectDir = $this->kernel->getProjectDir();
            $vslistPath = $projectDir . '/.vslist';
            
            if (file_exists($vslistPath)) {
                $io->section("Obtention de la liste des changements avec <info>.vslist</info>.");
                $hashes = file($vslistPath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            } else {
                $io->section("Obtention de la liste des changements depuis <info>git</info>.");
                $process = new Process(['git', 'rev-list', 'HEAD']);
                $process->run();
                
                if (!$process->isSuccessful()) {
                    throw new RuntimeException($process->getErrorOutput());
                }
                
                $hashes = array_reverse(explode("\n", trim($process->getOutput())));
            }
            
            $io->text('<info>' . count($hashes) . '</info> correctifs installés trouvés.');
            
            $gitRepo = $this->entityManager->getRepository(GitVersions::class);
            $new     = 0;
            
            foreach ($hashes as $hash) {
                if ($gitRepo->count(['version' => trim($hash)]) === 0) {
                    $new++;
                    $this->entityManager->persist((new GitVersions())->setVersion(trim($hash))->setInstalled(false));
                }
            }
            
            if ($new > 0) {
                $this->entityManager->flush();
                $io->text("Des correctifs, <info>$new</info> ont été récemment découverts.");
            } else {
                $io->text("<info>Aucun</info> correctif n'a été récemment découvert.");
            }
            
            $uninstalled      = array_map(fn(GitVersions $g) => $g->getVersion(), $gitRepo->findBy(['installed' => false], ['id' => 'ASC']));
            $nombreAInstaller = count($uninstalled);
            
            if ($nombreAInstaller > 0) {
                $io->text("Fin de la configuration de la base de données pour les correctifs <info>{$nombreAInstaller}</info>");
            } else {
                $io->text("Aucun correctif marqué pour l'installation");
            }
            
            foreach ($uninstalled as $version) {
                if (isset(static::$git_script_repository[$version])) {
                    $this->entityManager->flush();
                    $io->text("\tInstallation de <comment>$version</comment>...");
                    foreach (static::$git_script_repository[$version] as $script) {
                        
                        if ($script[0] === 'app:migrate') {
                            $input = new ArrayInput($script[1]);
                        }
                        
                        $input->setInteractive(false);
                        
                        try {
                            if ($script[0] === 'app:migrate') {
                                $this->getApplication()->find($script[0])->run($input, $io);
                            } else {
                                $this->commandHelper->capsule($script[0], $io, null, true);
                            }
                        } catch (Exception $e) {
                            $io->error($e->getMessage());
                            
                            return 1;
                        } catch (ExceptionInterface $e) {
                            $io->error($e->getMessage());
                            return 1;
                        }
                    }
                    
                    $io->success("\t<info>OK!</info>");
                    
                }
                
                $version_object = $this->entityManager->getRepository(GitVersions::class)->findOneBy(['version' => $version]);
                $this->entityManager->persist($version_object->setInstalled(true));
                $this->entityManager->flush();
            }
            
            $this->entityManager->flush();
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        return Command::SUCCESS;
    }
}
