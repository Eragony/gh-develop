<?php

namespace App\Command\Migration;

use App\Service\CommandHelper;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand('app:data:migration')]
class DatabaseMigrationCommand extends Command
{
    public function __construct(
        private readonly ParameterBagInterface $param,
        private readonly CommandHelper         $commandHelper,
    )
    {
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de faire la sauvegarde de la base de données, puis la migration')
             ->setHelp('Migrations.')
             ->addOption('environment', null, InputOption::VALUE_REQUIRED, 'Set l\'environnement pour construire les assets')
             ->addOption('phar', null, InputOption::VALUE_NONE, 'S\'il est défini, composer sera appelé à l\'aide d\'un fichier composer.phar');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $io->title('Mise à jour de la base de données');
            
            if (!$this->commandHelper->capsule('app:cron backup update', $io, 'Création d\'une sauvegarde de la base de données avant la mise à niveau...')) {
                return Command::FAILURE;
            }
            
            $io->note("Passage en mode développement pour la migration de données...");
            
            $composerCmd = $input->getOption('phar') ? "php composer.phar" : 'composer';
            if (!$this->commandHelper->capsule("$composerCmd dump-env dev", $io, 'Mise à jour des dépendances...', false)) {
                return Command::FAILURE;
            }
            
            // On ne fait plus de diff, on migre directement car on a déjà fait un diff qui sera stocké dans git
/*            $io->note("Création de la migration...");
            if (!$this->commandHelper->capsule('doctrine:migrations:diff --allow-empty-diff --formatted --no-interaction', $io)) {
                $io->error('Impossible de créer une migration.');
                return Command::FAILURE;
            }*/
            
            $io->note("Migration de la base de données...");
            if (!$this->commandHelper->capsule('doctrine:migrations:migrate --all-or-nothing --allow-no-migration --no-interaction', $io)) {
                $io->warning('Impossible de migrer la base de données');
                return Command::FAILURE;
            }
            
            $env = $input->getOption('environment');
            $io->note("Passage en mode {$env} pour la suite du processus...");
            
            if (!$this->commandHelper->capsule("$composerCmd dump-env $env", $io, 'Mise à jour des dépendances...', false)) {
                return Command::FAILURE;
            }
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        return Command::SUCCESS;
    }
}