<?php

namespace App\Command\Migration;

use App\Service\CommandHelper;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('app:git:migration')]
class GitMigrationCommand extends Command
{
    public function __construct(private readonly CommandHelper $commandHelper)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de faire la migration pour faire un changement de version')
             ->setHelp('Migrations.')
             ->addOption('from-git', 'g', InputOption::VALUE_NONE, 'Switches to the given git branch and updates everything.')
             ->addOption('remote', null, InputOption::VALUE_REQUIRED, 'Sets the git remote for --from-git')
             ->addOption('branch', null, InputOption::VALUE_REQUIRED, 'Sets the git branch for --from-git');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $remote = $input->getOption('remote');
            $branch = $input->getOption('branch');
            
            $io->title('Mise à jour du code source');
            
            if (!$this->commandHelper->capsule("git fetch --tags $remote $branch", $io, 'Récupération des mises à jour du référentiel...', false)) {
                return Command::FAILURE;
            }
            if (!$this->commandHelper->capsule("git reset --hard $remote/$branch", $io, 'Application des modifications sur les fichiers...', false)) {
                return Command::FAILURE;
            }
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        return Command::SUCCESS;
    }
}