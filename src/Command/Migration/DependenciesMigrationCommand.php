<?php

namespace App\Command\Migration;

use App\Service\CommandHelper;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('app:dependencies')]
class DependenciesMigrationCommand extends Command
{
    public function __construct(private readonly CommandHelper $commandHelper)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de faire la migration pour faire un changement de version')
             ->setHelp('Migrations.')
             ->addOption('environment', null, InputOption::VALUE_REQUIRED, 'Set l\'environnement pour construire les assets')
             ->addOption('phar', null, InputOption::VALUE_NONE, 'S\'il est défini, composer sera appelé à l\'aide d\'un fichier composer.phar');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $env         = $input->getOption('environment');
            $composerCmd = $input->getOption('phar') ? "php composer.phar" : 'composer';
            
            $io->title('Mise à jour des dépendances');
            
            if (!$this->commandHelper->capsule("$composerCmd install", $io, 'Mise à jour des dépendances...', false)) {
                return Command::FAILURE;
            }
            if (!$this->commandHelper->capsule("$composerCmd dump-env $env", $io, 'Mise à jour des dépendances...', false)) {
                return Command::FAILURE;
            }
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        return Command::SUCCESS;
    }
}