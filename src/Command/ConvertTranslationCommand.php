<?php

namespace App\Command;

use App\Service\CommandHelper;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

#[AsCommand(
    name: 'app:translation:convert',
    description: 'Add a short description for your command',
)]
class ConvertTranslationCommand extends Command
{
    public function __construct(private readonly CommandHelper $commandHelper)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de faire la conversion des fichiers de traduction')
             ->setHelp('Conversion de traduction.')
             ->addOption('ymlToJson', 'y', InputOption::VALUE_NONE, 'convertis les fichiers yml en json')
             ->addOption('jsonToYml', 'j', InputOption::VALUE_REQUIRED, 'Extraction et copie des nouvelles clefs de traduction json dans les fichiers yml');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        if ($input->getOption('ymlToJson')) {
            $io->title('Conversion des fichiers yml en json');
            $finder   = new Finder();
            $yamlPath = __DIR__ . '/../../translations';                                                 // Répertoire des fichiers YAML
            $jsonPath = __DIR__ . '/../../public/translations';                                          // Répertoire des fichiers JSON
            
            // Rechercher tous les fichiers YAML dans le répertoire des traductions
            $finder->files()->in($yamlPath)->name('*.yml');
            
            $io->note("Nombre de fichier à convertir : {$finder->count()}");
            $io->progressStart($finder->count());
            
            foreach ($finder as $file) {
                $yamlFile = $file->getRealPath();
                $yamlData = Yaml::parseFile($yamlFile);
                // Générer le chemin du fichier JSON correspondant
                $relativePath = $file->getRelativePathname();
                $relativePath = preg_replace('/\.yml$/', '.json', $relativePath);
                
                // Extraire la langue à partir du nom du fichier
                $lang = 'en'; // Langue par défaut si non spécifiée
                if (preg_match('/\+intl-icu\.(\w{2})\.json$/', $relativePath, $matches)) {
                    $lang         = $matches[1];
                    $relativePath = preg_replace('/\+intl-icu\.\w{2}\.json$/', '.json', $relativePath);
                } elseif (preg_match('/\.(\w{2})\.json$/', $relativePath, $matches)) {
                    $lang         = $matches[1];
                    $relativePath = preg_replace('/\.\w{2}\.json$/', '.json', $relativePath);
                }
                
                // Retirer intl-icu du chemin
                $relativePath = str_replace('+intl-icu', '', $relativePath);
                
                $jsonFile = $jsonPath . '/' . $lang . '/' . $relativePath;
                
                // Créer le répertoire de destination s'il n'existe pas
                if (!is_dir(dirname($jsonFile))) {
                    mkdir(dirname($jsonFile), 0777, true);
                }
                // Convertir les données YAML en JSON et les écrire dans le fichier JSON
                file_put_contents($jsonFile, json_encode($yamlData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                
                
                $io->progressAdvance();
            }
            $io->progressFinish();
            $io->success('Conversion des fichiers yml en json effectuée');
        }
        if ($lang = $input->getOption('jsonToYml')) {
            $io->title('Extraction et copie des nouvelles clefs de traduction json dans les fichiers yml');
            
            // Extraction des clefs de traduction des fichiers React pour alimenter les fichiers json
            if ($this->commandHelper->capsule('npm run i18n:extract', $io, "Extraction des clefs de React", false)) {
                $io->success('Extraction des clefs de traduction effectuée');
            } else {
                $io->error('Erreur lors de l\'extraction des clefs de traduction');
                return Command::FAILURE;
            }
            
            $finderJson = new Finder();
            $yamlPath   = __DIR__ . '/../../translations';                                                 // Répertoire des fichiers YAML
            $jsonPath   = __DIR__ . '/../../translations/js';                                              // Répertoire des fichiers JSON
            
            // Rechercher tous les fichiers Json dans le répertoire des traductions
            $finderJson->files()->in($jsonPath)->name('*.json');
            
            $io->title("Mise à jour des fichiers YAML à partir des fichiers JSON");
            
            $nombreFile = $finderJson->count();
            $io->note("Nombre de fichier à convertir : $nombreFile");
            $io->progressStart($nombreFile);
            
            $fichierMAJ = [];
            
            foreach ($finderJson as $file) {
                $jsonFilePath = $file->getRealPath();
                $translations = $this->readJsonFile($jsonFilePath);
                $namespace    = basename($jsonFilePath, '.json');
                
                
                $nameFile     = "$namespace+intl-icu.$lang.yml";
                $yamlFilePath = "$yamlPath/$nameFile";
                
                // Lire les traductions existantes du fichier YAML
                $yamlTranslations = $this->readYamlFile($yamlFilePath);
                
                // Ajouter les clés manquantes aux fichiers YAML
                $updated = false;
                foreach ($translations as $key => $value) {
                    if (!isset($yamlTranslations[$key])) {
                        $yamlTranslations[$key] = $key; // Pré-remplit la valeur avec la clé elle-même
                        $updated                = true;
                    }
                }
                
                // Écrire les mises à jour dans le fichier YAML
                if ($updated) {
                    $this->writeYamlFile($yamlFilePath, $yamlTranslations);
                    $fichierMAJ[] = [$nameFile];
                }
                $io->progressAdvance();
            }
            $io->progressFinish();
            
            // Afficher le tableau des fichiers mis à jour
            if (!empty($fichierMAJ)) {
                $io->section('Fichiers mis à jour');
                $io->table(['Fichier'], $fichierMAJ);
            } else {
                $io->success('Aucun fichier YAML mis à jour.');
            }
            
            $io->success('Mise à jour des fichiers YAML terminée.');
        }
        
        return Command::SUCCESS;
    }
    
    private function readJsonFile(string $filePath): array
    {
        if (!file_exists($filePath)) {
            return [];
        }
        return json_decode(file_get_contents($filePath), true);
    }
    
    private function readYamlFile(string $filePath): array
    {
        if (!file_exists($filePath)) {
            return [];
        }
        return Yaml::parseFile($filePath);
    }
    
    private function writeYamlFile(string $filePath, array $data): void
    {
        file_put_contents($filePath, Yaml::dump($data, 2));
    }
}
