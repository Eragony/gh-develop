<?php

namespace App\Command;

use App\Service\CommandHelper;
use App\Service\ConfMaster;
use App\Service\Locksmith;
use App\Structures\Conf\GestHordesConf;
use DateTime;
use DirectoryIterator;
use Exception;
use SplFileInfo;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'app:cron',
    description: 'Cron command'
)]
class CronCommand extends Command
{
    
    private readonly GestHordesConf $conf;
    
    public function __construct(
        private readonly array                 $db,
        private readonly CommandHelper         $helper,
        private readonly ConfMaster            $conf_master,
        private readonly ParameterBagInterface $params,
        private readonly Locksmith             $locksmith,
    )
    {
        
        $this->conf = $this->conf_master->getGlobalConf();
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this
            ->setHelp('This should be run on a regular basis.')
            ->addArgument('task', InputArgument::OPTIONAL, 'The task to perform. Defaults to "host".', 'host')
            ->addArgument('p1', InputArgument::OPTIONAL, 'Parameter 1', -1)
            ->addArgument('p2', InputArgument::OPTIONAL, 'Parameter 2', -1);
    }
    
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io   = new SymfonyStyle($input, $output);
        $task = $input->getArgument('task');
        
        if ($task != 'backup') {
            $io->error('Invalid task.');
            
            return -1;
        }
        
        $lock = $this->locksmith->waitForLock("cron-$task");
        
        return match ($task) {
            'backup' => $this->task_backup($input, $output, $io),
            default  => -1,
        };
        
        
    }
    
    protected function module_run_backups(SymfonyStyle $io): ?string
    {
        
        $last_backups =
            ['any'    => (new DateTime())->setTimestamp(0), 'daily' => (new DateTime())->setTimestamp(0),
             'weekly' => (new DateTime())->setTimestamp(0), 'monthly' => (new DateTime())->setTimestamp(0)];
        
        $path = $this->conf->get(GestHordesConf::CONF_BACKUP_PATH, null);
        if ($path === null) {
            $path = "{$this->params->get('kernel.project_dir')}/var/backup";
        }
        if (file_exists($path)) {
            foreach (new DirectoryIterator($path) as $fileInfo) {
                /** @var SplFileInfo $fileInfo */
                if ($fileInfo->isDot() || $fileInfo->isLink()) {
                    continue;
                } elseif ($fileInfo->isFile() &&
                          in_array(strtolower($fileInfo->getExtension()), ['sql', 'xz', 'gzip', 'bz2'])) {
                    $segments = explode('_', explode('.', $fileInfo->getFilename())[0]);
                    if (count($segments) !== 3 || !in_array($segments[2], ['daily', 'weekly', 'monthly'])) {
                        continue;
                    }
                    
                    $date = date_create_from_format('Y-m-d', $segments[0]);
                    if ($date === false) {
                        continue;
                    }
                    
                    if ($last_backups[$segments[2]] < $date) {
                        $last_backups[$segments[2]] = $date;
                    }
                    if ($last_backups['any'] < $date) {
                        $last_backups['any'] = $date;
                    }
                }
            }
        }
        
        if ($last_backups['any'] >= new DateTime('today')) {
            return null;
        }
        
        if ($last_backups['monthly'] <= new DateTime('today-1month')) {
            $this->helper->capsule('app:cron backup monthly', $io, 'Création de sauvegardes mensuelles... ', true);
            
            return 'monthly';
        } elseif ($last_backups['weekly'] <= new DateTime('today-1week')) {
            $this->helper->capsule('app:cron backup weekly', $io, 'Création d\'une sauvegarde hebdomadaire... ', true);
            
            return 'weekly';
        } else {
            $this->helper->capsule('app:cron backup daily', $io, 'Création de sauvegardes quotidiennes... ', true);
            
            return 'daily';
        }
        
    }
    
    /**
     * @throws Exception
     */
    protected function task_backup(InputInterface $input, OutputInterface $output, SymfonyStyle $io): int
    {
        // Backup task
        $io->info("GestHordes CronJob - Backup Processor", OutputInterface::VERBOSITY_VERBOSE);
        
        [
            'scheme' => $db_scheme, 'host' => $db_host, 'port' => $db_port,
            'user'   => $db_user, 'pass' => $db_pass,
            'path'   => $db_name,
        ] = $this->db;
        
        if ($db_scheme !== 'mysql') {
            throw new Exception('Désolé, seul MySQL est pris en charge pour la sauvegarde !');
        }
        
        if (!isset($this->conf->getData()['backup']['storages']) ||
            count($this->conf->getData()['backup']['storages']) == 0) {
            throw new Exception('Aucun stockage de sauvegarde n\'est défini, il n\'est pas possible de stocker les sauvegardes de la base de données.');
        }
        
        $storages = $this->conf->getData()['backup']['storages'];
        
        $domain = $input->getArgument('p1');
        
        if ($domain === -1) {
            $domain = 'manual';
        }
        if (!in_array($domain, ['nightly', 'daily', 'weekly', 'monthly', 'update', 'manual'])) {
            throw new Exception('Invalid backup domain!');
        }
        
        $path = $this->conf->get(GestHordesConf::CONF_BACKUP_PATH, null);
        
        if ($path === null) {
            $path = "{$this->params->get('kernel.project_dir')}/var/tmp";
        }
        $path = str_replace("~", $this->params->get('kernel.project_dir'), (string)$path);
        
        if (!file_exists($path)) {
            mkdir($path, 0700, true);
        }
        $filename = $path . '/' . (new DateTime())->format('Y-m-d_H-i-s-v_') . $domain . '.sql';
        
        $compression = $this->conf->get(GestHordesConf::CONF_BACKUP_COMPRESSION, null);
        if ($compression === null) {
            $str = "> $filename";
        } elseif ($compression === 'xz') {
            $str = "| xz > {$filename}.xz";
        } elseif ($compression === 'gzip') {
            $str = "| gzip > {$filename}.gz";
        } elseif ($compression === 'bzip2') {
            $str = "| bzip2 > {$filename}.bz2";
        } elseif ($compression === 'lbzip2') {
            $str = "| lbzip2 > {$filename}.bz2";
        } elseif ($compression === 'pbzip2') {
            $str = "| pbzip2 > {$filename}.bz2";
        } else {
            throw new Exception('Invalid compression!');
        }
        
        $relevant_domain_limit = $this->conf->get(GestHordesConf::CONF_BACKUP_LIMITS_INC . $domain, -1);
        
        
        if ($relevant_domain_limit !== 0) {
            $io->writeln("Exécution de <info>mysqldump</info> sur <info>$db_host:$db_port</info>, exportant <info>$db_name</info> <comment>$str</comment>", OutputInterface::VERBOSITY_VERBOSE);
            $this->helper->capsule("mysqldump -h $db_host -P $db_port --user='$db_user' --password='$db_pass' --single-transaction --skip-lock-tables $db_name $str", $io, 'Exécution de la sauvegarde de la base de données... ', false);
        } else {
            $io->writeln("Sauter <info>mysqldump</info> dans le domaine <info>$domain</info> puisque les sauvegardes pour ce domaine sont désactivées.", OutputInterface::VERBOSITY_VERBOSE);
            
            return 0;
        }
        
        $success = true;
        
        // Putting created backup into the different storages
        foreach ($storages as $name => $config) {
            if (!$config['enabled']) {
                continue;
            }
            
            $io->writeln("Placement de la sauvegarde nouvellement créée dans le stockage {$config['type']} '$name'", OutputInterface::VERBOSITY_VERBOSE);
            switch ($config['type']) {
                case "local":
                    $targetPath = str_replace("~", $this->params->get('kernel.project_dir'), (string)$config['path']);
                    if (!is_dir($targetPath)) {
                        if (mkdir($targetPath, 0700, true)) {
                            $io->error("Impossible de créer le dossier de sauvegarde $targetPath !");
                            $success = false;
                            break;
                        }
                    }
                    $filename .= match ($compression) {
                        "xz"                        => ".xz",
                        "gzip"                      => ".gz",
                        "bzip2", "lbzip2", "pbzip2" => ".bz2",
                        null                        => "",
                    };
                    
                    if (!copy($filename, $targetPath . "/" . basename($filename))) {
                        $io->error("Impossible de placer le fichier de sauvegarde " . basename($filename) . " dans le dossier $targetPath !");
                        $success = false;
                    }
                    break;
                default:
                    $io->error("Unknown storage type {$config['type']}");
                    break;
            }
        }
        
        if (!$success) {
            throw new Exception("An error has occured while putting the new backup file into a storage");
        }
        
        // Processing storages to ensure retention policies
        foreach ($storages as $name => $config) {
            if (!$config['enabled']) {
                continue;
            }
            
            $io->writeln("Assurer la politique de rétention sur le stockage <info>{$config['type']}</info> <info>$name</info>.",
                         OutputInterface::VERBOSITY_VERBOSE);
            $backup_files = [];
            
            if ($config['type'] == "local") {
                $targetPath = str_replace("~", $this->params->get('kernel.project_dir'), (string)$config['path']);
                foreach (new DirectoryIterator($targetPath) as $fileInfo) {
                    /** @var SplFileInfo $fileInfo */
                    if ($fileInfo->isDot() || $fileInfo->isLink()) {
                        continue;
                    }
                    
                    if ($fileInfo->isFile() &&
                        in_array(strtolower($fileInfo->getExtension()), ['sql', 'xz', 'gzip', 'bz2'])) {
                        $segments = explode('_', explode('.', $fileInfo->getFilename())[0]);
                        if (count($segments) !== 3 ||
                            !in_array($segments[2], ['nightly', 'daily', 'weekly', 'monthly', 'update', 'manual'])) {
                            continue;
                        }
                        if (!isset($backup_files[$segments[2]])) {
                            $backup_files[$segments[2]] = [];
                        }
                        $backup_files[$segments[2]][] = $fileInfo->getRealPath();
                    }
                }
                foreach (['nightly', 'daily', 'weekly', 'monthly', 'update', 'manual'] as $sel_domain) {
                    $domain_limit = $this->conf->get(GestHordesConf::CONF_BACKUP_LIMITS_INC . $sel_domain, -1);
                    
                    if (!empty($backup_files[$sel_domain]) && $domain_limit >= 0 &&
                        count($backup_files[$sel_domain]) > $domain_limit) {
                        rsort($backup_files[$sel_domain]);
                        while (count($backup_files[$sel_domain]) > $domain_limit) {
                            $f = array_pop($backup_files[$sel_domain]);
                            if ($f === null) {
                                break;
                            }
                            $io->writeln("Suppression d'une ancienne sauvegarde : <info>$f</info>", OutputInterface::VERBOSITY_VERBOSE);
                            unlink($f);
                        }
                    }
                }
            }
        }
        
        // We remove the temporary backup file (as it should be stored in the different enabled storages)
        unlink($filename);
        
        return 0;
    }
    
}