<?php

namespace App\Exception;

use Exception;

class MyHordesMajException extends Exception
{
    
    const INVALID_USER_KEY  = 100;
    const TOO_MANY_REQUESTS = 101;
    const INTERNAL_ERROR    = 102;
    const TOKEN_NULL        = 103;
    
}