<?php

namespace App\Exception;

use Exception;

class GestHordesException extends Exception
{
    const SHOW_MESSAGE          = 100;
    const SHOW_MESSAGE_INFO     = 101;
    const SHOW_MESSAGE_REDIRECT = 102;
}