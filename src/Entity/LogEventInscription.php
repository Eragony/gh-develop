<?php

namespace App\Entity;

use App\Repository\LogEventInscriptionRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LogEventInscriptionRepository::class)]
class LogEventInscription
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 255), Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?string $libelle = null;
    
    #[ORM\Column(type: 'datetime'),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?DateTimeInterface $eventAt = null;
    
    #[ORM\Column(length: 255, nullable: true),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?string $valeurAvant = null;
    
    #[ORM\Column(length: 255, nullable: true),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?string $valeurApres = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?User $declencheur = null;
    
    #[ORM\ManyToOne(inversedBy: 'logEventInscriptions')]
    #[ORM\JoinColumn(nullable: false),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?InscriptionJump $inscription = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false]),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?bool $visible = null;
    
    /**
     * @var int|null
     * Valeur possible :
     * 0 - traduction texte
     * 1 - id JobPrototype
     * 2 - id LevelRuinePrototype
     * 3 - id HerosPrototype
     * 4 - nom statut
     */
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0]),
        Groups(['gestion_inscription', 'inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?int $typologie = 0;
    
    public function getDeclencheur(): ?User
    {
        return $this->declencheur;
    }
    
    public function setDeclencheur(?User $declencheur): self
    {
        $this->declencheur = $declencheur;
        
        return $this;
    }
    
    public function getEventAt(): ?DateTimeInterface
    {
        return $this->eventAt;
    }
    
    public function setEventAt(DateTimeInterface $eventAt): self
    {
        $this->eventAt = $eventAt;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getInscription(): ?InscriptionJump
    {
        return $this->inscription;
    }
    
    public function setInscription(?InscriptionJump $inscription): self
    {
        $this->inscription = $inscription;
        
        return $this;
    }
    
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }
    
    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;
        
        return $this;
    }
    
    public function getTypologie(): ?int
    {
        return $this->typologie;
    }
    
    public function setTypologie(int $typologie): self
    {
        $this->typologie = $typologie;
        
        return $this;
    }
    
    public function getValeurApres(): ?string
    {
        return $this->valeurApres;
    }
    
    public function setValeurApres(?string $valeurApres): self
    {
        $this->valeurApres = $valeurApres;
        
        return $this;
    }
    
    public function getValeurAvant(): ?string
    {
        return $this->valeurAvant;
    }
    
    public function setValeurAvant(?string $valeurAvant): self
    {
        $this->valeurAvant = $valeurAvant;
        
        return $this;
    }
    
    public function isVisible(): ?bool
    {
        return $this->visible;
    }
    
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;
        
        return $this;
    }
}
