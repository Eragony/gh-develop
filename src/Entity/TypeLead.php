<?php

namespace App\Entity;

use App\Repository\TypeLeadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeLeadRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeLead
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['jump'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['jump'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['jump'])]
    private ?string $icon = null;
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeLead
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
