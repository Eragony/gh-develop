<?php

namespace App\Entity;

use App\Repository\UpChantierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UpChantierRepository::class)]
class UpChantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'upChantier')]
    private ?Ville $ville;
    
    #[ORM\Column(type: 'smallint'), Groups(['evo_chantier', 'comparatif'])]
    private ?int $lvlActuel = null;
    
    /**
     * @var int[][]
     */
    #[ORM\Column(type: 'json'), Groups(['evo_chantier', 'comparatif'])]
    private array $days = [];
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false]), Groups(['evo_chantier'])]
    private ?bool $destroy = false;
    
    public function __construct(
        #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
        #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
        #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false), Groups(['evo_chantier', 'comparatif'])]
        private ChantierPrototype $chantier)
    {
    }
    
    public function addDays(array $days): self
    {
        $this->days[] = $days;
        return $this;
    }
    
    public function getChantier(): ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function getDays(): ?array
    {
        return $this->days;
    }
    
    public function setDays(array $days): self
    {
        $this->days = $days;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return UpChantier
     */
    public function setId(int $id): UpChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLvlActuel(): int
    {
        return $this->lvlActuel;
    }
    
    public function setLvlActuel(int $lvlActuel): self
    {
        $this->lvlActuel = $lvlActuel;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function isDestroy(): ?bool
    {
        return $this->destroy;
    }
    
    public function setChantier(ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function setDestroy(bool $destroy): static
    {
        $this->destroy = $destroy;
        
        return $this;
    }
}
