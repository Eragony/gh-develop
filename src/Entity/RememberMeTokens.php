<?php

namespace App\Entity;

use App\Repository\RememberMeTokensRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RememberMeTokensRepository::class)]
#[ORM\Table]
#[ORM\UniqueConstraint(name: 'remember_me_token_unique', columns: ['token'])]
class RememberMeTokens
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(length: 32)]
    private ?string $token = null;
    
    #[ORM\OneToOne(targetEntity: User::class, cascade: ['persist'])]
    private ?User $user = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getToken(): ?string
    {
        return $this->token;
    }
    
    public function setToken(string $token): self
    {
        $this->token = $token;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
}
