<?php

namespace App\Entity;

use App\Repository\CitoyensRepository;
use App\Structures\Utils\Position;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CitoyensRepository::class)]
class Citoyens
{
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private ?bool $ban = false;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 3])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?int $chargeApag = 3;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'citoyen_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private User $citoyen;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'clean_twin_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?User $cleanUpName = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: CleanUpCadaver::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'clean_up_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?CleanUpCadaver $cleanUpType = null;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?bool $cloture = false;
    
    /** @var Collection<CoffreCitoyen> */
    #[ORM\ManyToMany(targetEntity: CoffreCitoyen::class, mappedBy: 'citoyen', cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'coffre_id')]
    #[ORM\JoinTable(name: 'citoyens_coffre')]
    #[ORM\JoinColumn(name: 'citoyen_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'coffre_id', referencedColumnName: 'id')]
    #[Groups(['citoyens'])]
    private Collection $coffres;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe', 'inscription_exp_visu'])]
    private ?bool $corpsSain = true;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['citoyens'])]
    private ?DateTimeInterface $dateMaj = null;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?int $deathDay = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeDeath::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'death_id', referencedColumnName: 'id_mort', nullable: true)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?TypeDeath $deathType = null;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'comparatif'])]
    private ?bool $dehors = false;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens'])]
    private ?bool $donJh = true;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['citoyens', 'comparatif'])]
    private int $id;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens'])]
    private ?bool $immuniser = false;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens'])]
    private ?bool $incarner = false;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: JobPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'job_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private ?JobPrototype $job = null;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $lvlCoinSieste = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $lvlCuisine = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $lvlLabo = 0;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: HomePrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'lvl_maison_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['citoyens', 'outils_chantier', 'comparatif'])]
    private ?HomePrototype $lvlMaison = null;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $lvlRangement = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $lvlRenfort = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens'])]
    private ?int $lvlRuine = 0;
    
    #[ORM\Column(type: 'string', length: 1024, nullable: true)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?string $message = null;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens', 'mort', 'carte_gen', 'outils_chantier', 'comparatif'])]
    private ?bool $mort = null;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $nbBarricade = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'carte', 'carte_gen'])]
    private ?int $nbCamping = 0;
    
    #[ORM\Column(type: 'smallint', options: ['default' => 0])]
    #[Groups(['citoyens', 'outils_chantier'])]
    private ?int $nbCarton = 0;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $pef = true;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $rdh = true;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $sauvetage = true;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $secondSouffle = true;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $trouvaille = true;
    
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[Groups(['citoyens'])]
    private ?User $updateBy = null;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $uppercut = true;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'citoyens')]
    #[ORM\JoinColumn(name: 'ville_id', referencedColumnName: 'id', nullable: false)]
    private ?Ville $ville;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(['citoyens', 'outils_expe'])]
    private ?bool $vlm = true;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'comparatif'])]
    private ?int $x = null;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['citoyens', 'carte', 'carte_gen', 'comparatif'])]
    private ?int $y = null;
    
    #[Groups(['citoyens'])]
    private ?string $moyenContact = null;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(['citoyens'])]
    private ?bool $chargeCamaraderie = false;
    
    /** @var Collection<Expeditionnaire> */
    #[ORM\OneToMany(mappedBy: 'citoyen', targetEntity: Expeditionnaire::class)]
    private Collection $expeditionnaires;
    
    /** @var Collection<Ouvriers> */
    #[ORM\OneToMany(mappedBy: 'citoyen', targetEntity: Ouvriers::class)]
    private Collection $ouvriers;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['citoyens'])]
    private ?bool $shoes = false;
    
    /**
     * @var Collection<int, HerosSkillLevel>
     */
    #[ORM\ManyToMany(targetEntity: HerosSkillLevel::class), Groups(['citoyens'])]
    private Collection $skill;
    
    public function __construct()
    {
        $this->coffres          = new ArrayCollection();
        $this->expeditionnaires = new ArrayCollection();
        $this->ouvriers         = new ArrayCollection();
        $this->skill            = new ArrayCollection();
    }
    
    public function addCoffre(CoffreCitoyen $coffre): self
    {
        if (!$this->coffres->contains($coffre)) {
            $coffre->addCitoyen($this);
            $this->coffres[$coffre->getIdObjet()] = $coffre;
        }
        
        return $this;
    }
    
    public function addExpeditionnaire(Expeditionnaire $expeditionnaire): static
    {
        if (!$this->expeditionnaires->contains($expeditionnaire)) {
            $this->expeditionnaires->add($expeditionnaire);
            $expeditionnaire->setCitoyen($this);
        }
        
        return $this;
    }
    
    public function addOuvrier(Ouvriers $ouvrier): static
    {
        if (!$this->ouvriers->contains($ouvrier)) {
            $this->ouvriers->add($ouvrier);
            $ouvrier->setCitoyen($this);
        }
        
        return $this;
    }
    
    public function addSkill(HerosSkillLevel $skill): static
    {
        if (!$this->skill->contains($skill)) {
            $this->skill->add($skill);
        }
        
        return $this;
    }
    
    public function getBan(): ?bool
    {
        return $this->ban;
    }
    
    public function setBan(bool $ban): self
    {
        $this->ban = $ban;
        
        return $this;
    }
    
    #[Groups(['carte', 'carte_gen'])]
    public function getCampeurPro(): bool
    {
        return $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= 25;
    }
    
    #[Groups(['citoyens', 'carte', 'carte_gen'])]
    public function getChanceCampingMax(): int
    {
        // Si le joueur a le métier d'ermite, il a 100% de chance de survie maximum
        if ($this->hasJob(JobPrototype::job_hunter)) {
            return 100;
        }
        // On va regarder les skills du citoyen en cours, et on va regarder s'il a le skill Reclus de niveau Maître.
        $chance = 90;
        
        foreach ($this->getSkill() as $skill) {
            if ($skill->getHerosSkillType()->getId() === HerosSkillType::ID_RECLUS && $skill->getLvl()->value >= 4) {
                $chance = 99;
                break;
            }
        }
        
        // Dans le cas où le joueur n'a pas de skill enregistré, on va regarder si il a des skill type d'enregistré
        if ($this->getSkill()->isEmpty()) {
            foreach ($this->getCitoyen()->getSkillType() as $skillType) {
                if ($skillType->getHerosSkillType()->getId() === HerosSkillType::ID_RECLUS && $skillType->getLvl()->value >= 4) {
                    $chance = 99;
                    break;
                }
            }
        }
        
        return $chance;
    }
    
    public function getChargeApag(): ?int
    {
        return $this->chargeApag;
    }
    
    public function setChargeApag(int $chargeApag): self
    {
        $this->chargeApag = $chargeApag;
        
        return $this;
    }
    
    public function getCitoyen(): User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getCleanUpName(): ?User
    {
        return $this->cleanUpName;
    }
    
    public function setCleanUpName(?User $cleanUpName): self
    {
        $this->cleanUpName = $cleanUpName;
        
        return $this;
    }
    
    public function getCleanUpType(): ?CleanUpCadaver
    {
        return $this->cleanUpType;
    }
    
    public function setCleanUpType(?CleanUpCadaver $cleanUpType): self
    {
        $this->cleanUpType = $cleanUpType;
        
        return $this;
    }
    
    public function getCloture(): ?bool
    {
        return $this->cloture;
    }
    
    public function setCloture(bool $cloture): self
    {
        $this->cloture = $cloture;
        
        return $this;
    }
    
    public function getCoffre(int $idCoffre): ?CoffreCitoyen
    {
        return $this->coffres->toArray()[$idCoffre] ?? null;
    }
    
    /** @return Collection<int, CoffreCitoyen> */
    public function getCoffres(): Collection
    {
        return $this->coffres;
    }
    
    public function getCorpsSain(): ?bool
    {
        return $this->corpsSain;
    }
    
    public function setCorpsSain(bool $corpsSain): self
    {
        $this->corpsSain = $corpsSain;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getCorpsSainCpt(): ?bool
    {
        return $this->corpsSain && $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= HerosPrototype::ID_CORPS_SAIN;
    }
    
    public function getDateMaj(): ?DateTimeInterface
    {
        return $this->dateMaj;
    }
    
    public function setDateMaj(?DateTimeInterface $dateMaj): self
    {
        $this->dateMaj = $dateMaj;
        
        return $this;
    }
    
    public function getDeathDay(): ?int
    {
        return $this->deathDay;
    }
    
    public function setDeathDay(?int $deathDay): self
    {
        $this->deathDay = $deathDay;
        
        return $this;
    }
    
    public function getDeathType(): ?TypeDeath
    {
        return $this->deathType;
    }
    
    public function setDeathType(?TypeDeath $deathType): self
    {
        $this->deathType = $deathType;
        
        return $this;
    }
    
    public function getDehors(): ?bool
    {
        return $this->dehors;
    }
    
    public function setDehors(bool $dehors): self
    {
        $this->dehors = $dehors;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getDiffDateMaj(): int
    {
        
        $dateJour = new DateTime('now');
        
        $difference = $this->getDateMaj()?->diff($dateJour) ?? $dateJour->diff($dateJour);
        
        return ($difference->days) ?: 0;
        
    }
    
    public function getDonJh(): ?bool
    {
        return $this->donJh;
    }
    
    public function setDonJh(bool $donJh): self
    {
        $this->donJh = $donJh;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getDonJhCpt(): ?bool
    {
        return $this->donJh && $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= HerosPrototype::ID_DON_JH &&
               $this->getCitoyen()->getNbChargeCamaraderie() > 0;
    }
    
    public function getExpeditionnaires(): Collection
    {
        return $this->expeditionnaires;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Citoyens
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getImmuniser(): ?bool
    {
        return $this->immuniser;
    }
    
    public function setImmuniser(bool $immuniser): self
    {
        $this->immuniser = $immuniser;
        
        return $this;
    }
    
    public function getIncarner(): ?bool
    {
        return $this->incarner;
    }
    
    public function setIncarner(bool $incarner): self
    {
        $this->incarner = $incarner;
        
        return $this;
    }
    
    public function getJob(): ?JobPrototype
    {
        return $this->job;
    }
    
    public function setJob(?JobPrototype $job): self
    {
        $this->job = $job;
        
        return $this;
    }
    
    public function getLvlCoinSieste(): ?int
    {
        return $this->lvlCoinSieste;
    }
    
    public function setLvlCoinSieste(int $lvlCoinSieste): self
    {
        $this->lvlCoinSieste = $lvlCoinSieste;
        
        return $this;
    }
    
    public function getLvlCs(): ?int
    {
        return $this->lvlCoinSieste;
    }
    
    public function getLvlCuisine(): ?int
    {
        return $this->lvlCuisine;
    }
    
    public function setLvlCuisine(int $lvlCuisine): self
    {
        $this->lvlCuisine = $lvlCuisine;
        
        return $this;
    }
    
    public function getLvlLabo(): ?int
    {
        return $this->lvlLabo;
    }
    
    public function setLvlLabo(int $lvlLabo): self
    {
        $this->lvlLabo = $lvlLabo;
        
        return $this;
    }
    
    public function getLvlMaison(): ?HomePrototype
    {
        return $this->lvlMaison;
    }
    
    public function setLvlMaison(HomePrototype $lvlMaison): self
    {
        $this->lvlMaison = $lvlMaison;
        
        return $this;
    }
    
    public function getLvlRangement(): ?int
    {
        return $this->lvlRangement;
    }
    
    public function setLvlRangement(int $lvlRangement): self
    {
        $this->lvlRangement = $lvlRangement;
        
        return $this;
    }
    
    public function getLvlRenfort(): ?int
    {
        return $this->lvlRenfort;
    }
    
    public function setLvlRenfort(int $lvlRenfort): self
    {
        $this->lvlRenfort = $lvlRenfort;
        
        return $this;
    }
    
    public function getLvlRuine(): ?int
    {
        return $this->lvlRuine;
    }
    
    public function setLvlRuine(int $lvlRuine): self
    {
        $this->lvlRuine = $lvlRuine;
        
        return $this;
    }
    
    public function getMessage(): ?string
    {
        return $this->message;
    }
    
    public function setMessage(?string $message): self
    {
        $this->message = $message;
        
        return $this;
    }
    
    public function getMort(): ?bool
    {
        return $this->mort;
    }
    
    public function setMort(bool $mort): self
    {
        $this->mort = $mort;
        
        return $this;
    }
    
    public function getMoyenContact(): ?string
    {
        return $this->moyenContact;
    }
    
    public function setMoyenContact(?string $moyenContact): Citoyens
    {
        $this->moyenContact = $moyenContact;
        return $this;
    }
    
    public function getNbBarricade(): ?int
    {
        return $this->nbBarricade;
    }
    
    public function setNbBarricade(int $nbBarricade): self
    {
        $this->nbBarricade = $nbBarricade;
        
        return $this;
    }
    
    public function getNbCamping(): ?int
    {
        return $this->nbCamping;
    }
    
    public function setNbCamping(int $nbCamping): self
    {
        $this->nbCamping = $nbCamping;
        
        return $this;
    }
    
    #[Groups(['citoyens', 'carte', 'carte_gen'])]
    public function getNbCampingProNoob(): int
    {
        
        // Si le joueur a le métier d'habitant, il a 0 en camping pro
        if ($this->hasJob(JobPrototype::job_basic)) {
            return 0;
        }
        
        // On va regarder les skills du citoyen en cours, et on va regarder s'il a le skill Reclus débutant ou Apprenti (ou plus).
        
        $nbCampingProNoob = 6;
        
        foreach ($this->getSkill() as $skill) {
            if ($skill->getHerosSkillType()->getId() === HerosSkillType::ID_RECLUS && $skill->getLvl()->value >= 2) {
                $nbCampingProNoob = 8;
                break;
            }
        }
        
        // Dans le cas où le joueur n'a pas de skill enregistré, on va regarder si il a des skill type d'enregistré
        if ($this->getSkill()->isEmpty()) {
            foreach ($this->getCitoyen()->getSkillType() as $skillType) {
                if ($skillType->getHerosSkillType()->getId() === HerosSkillType::ID_RECLUS && $skillType->getLvl()->value >= 2) {
                    $nbCampingProNoob = 8;
                    break;
                }
            }
        }
        
        return $nbCampingProNoob;
    }
    
    public function getNbCarton(): ?int
    {
        return $this->nbCarton;
    }
    
    public function setNbCarton(int $nbCarton): self
    {
        $this->nbCarton = $nbCarton;
        
        return $this;
    }
    
    public function getOuvriers(): Collection
    {
        return $this->ouvriers;
    }
    
    public function getPef(): ?bool
    {
        return $this->pef;
    }
    
    public function setPef(bool $pef): self
    {
        $this->pef = $pef;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getPefCpt(): ?bool
    {
        return $this->pef && $this->getCitoyen()->getTemArma();
    }
    
    #[Groups(['carte', 'carte_gen'])]
    public function getPositionRelatif(): string
    {
        $texteRetour = "(??/??)";
        if (!$this->getVille()->isDevast()) {
            $position = new Position($this->getVille(), $this->getX(), $this->getY());
            
            $position->calculPositionRelatif();
            
            
            $texteRetour = "({$position->getXRel()}/{$position->getYRel()})";
        }
        
        return $texteRetour;
        
    }
    
    public function getRdh(): ?bool
    {
        return $this->rdh;
    }
    
    public function setRdh(bool $rdh): self
    {
        $this->rdh = $rdh;
        
        return $this;
    }
    
    public function getSauvetage(): ?bool
    {
        return $this->sauvetage;
    }
    
    public function setSauvetage(bool $sauvetage): self
    {
        $this->sauvetage = $sauvetage;
        
        return $this;
    }
    
    public function getSecondSouffle(): ?bool
    {
        return $this->secondSouffle;
    }
    
    public function setSecondSouffle(bool $secondSouffle): self
    {
        $this->secondSouffle = $secondSouffle;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getSecondSouffleCpt(): ?bool
    {
        return $this->secondSouffle && $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= HerosPrototype::ID_SS;
    }
    
    /**
     * @return Collection<int, HerosSkillLevel>
     */
    public function getSkill(): Collection
    {
        return $this->skill;
    }
    
    public function getTrouvaille(): ?bool
    {
        return $this->trouvaille;
    }
    
    public function setTrouvaille(bool $trouvaille): self
    {
        $this->trouvaille = $trouvaille;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getTrouvailleAmeCpt(): ?bool
    {
        return $this->trouvaille &&
               $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= HerosPrototype::ID_TROUVAILLE_AME;
    }
    
    #[Groups(['citoyens'])]
    public function getTrouvailleCpt(): ?bool
    {
        return $this->trouvaille &&
               $this->getCitoyen()->getDerPouv()->getOrdreRecup() < HerosPrototype::ID_TROUVAILLE_AME;
    }
    
    public function getUpdateBy(): ?User
    {
        return $this->updateBy;
    }
    
    public function setUpdateBy(?User $updateBy): self
    {
        $this->updateBy = $updateBy;
        
        return $this;
    }
    
    public function getUppercut(): ?bool
    {
        return $this->uppercut;
    }
    
    public function setUppercut(bool $uppercut): self
    {
        $this->uppercut = $uppercut;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getVlm(): ?bool
    {
        return $this->vlm;
    }
    
    public function setVlm(bool $vlm): self
    {
        $this->vlm = $vlm;
        
        return $this;
    }
    
    #[Groups(['citoyens'])]
    public function getVlmCpt(): ?bool
    {
        return $this->vlm && $this->getCitoyen()->getDerPouv()->getOrdreRecup() >= HerosPrototype::ID_VLM;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(?int $x): self
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(?int $y): self
    {
        $this->y = $y;
        
        return $this;
    }
    
    /**
     * @param int $job (JobPrototype::job_*)
     * @return bool
     */
    public function hasJob(int $job): bool
    {
        return $this->getJob() && $this->getJob()->getId() === $job;
    }
    
    public function isChargeCamaraderie(): ?bool
    {
        return $this->chargeCamaraderie;
    }
    
    public function isShoes(): ?bool
    {
        return $this->shoes;
    }
    
    public function removeCoffre(CoffreCitoyen $coffre): self
    {
        $this->coffres->removeElement($coffre);
        
        return $this;
    }
    
    public function removeExpeditionnaire(Expeditionnaire $expeditionnaire): static
    {
        if ($this->expeditionnaires->removeElement($expeditionnaire)) {
            // set the owning side to null (unless already changed)
            if ($expeditionnaire->getCitoyen() === $this) {
                $expeditionnaire->setCitoyen(null);
            }
        }
        
        return $this;
    }
    
    public function removeOuvrier(Ouvriers $ouvrier): static
    {
        if ($this->ouvriers->removeElement($ouvrier)) {
            // set the owning side to null (unless already changed)
            if ($ouvrier->getCitoyen() === $this) {
                $ouvrier->setCitoyen(null);
            }
        }
        
        return $this;
    }
    
    public function removeSkill(HerosSkillLevel $skill): static
    {
        $this->skill->removeElement($skill);
        
        return $this;
    }
    
    public function setChargeCamaraderie(bool $chargeCamaraderie): self
    {
        $this->chargeCamaraderie = $chargeCamaraderie;
        
        return $this;
    }
    
    public function setShoes(bool $shoes): static
    {
        $this->shoes = $shoes;
        
        return $this;
    }
    
}
