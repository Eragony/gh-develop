<?php

namespace App\Entity;

use App\Repository\StatutInscriptionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: StatutInscriptionRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class StatutInscription
{
    public const ST_CREA     = 0;
    public const ST_INIT     = 1;
    public const ST_PRIS_MOD = 5;
    public const ST_MOD      = 6;
    public const ST_PRIS     = 10;
    public const ST_ACC_RES  = 15;
    public const ST_ACC      = 20;
    public const ST_REFUS    = 50;
    public const ST_ABAN     = 99;
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['list_jump', 'event_inscription', 'coalition', 'jump', 'event', 'candidature_jump'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 60), Groups(['list_jump', 'event_inscription', 'jump', 'event', 'candidature_jump'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['jump', 'event', 'candidature_jump'])]
    private ?string $nomGestion = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['jump', 'event', 'candidature_jump'])]
    private ?bool $visibleCandidature = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['jump', 'event', 'candidature_jump'])]
    private ?int $orderInGestion = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['jump', 'event', 'candidature_jump'])]
    private ?string $nomGestionCourt = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): StatutInscription
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getNomGestion(): ?string
    {
        return $this->nomGestion;
    }
    
    public function setNomGestion(string $nomGestion): self
    {
        $this->nomGestion = $nomGestion;
        
        return $this;
    }
    
    public function getNomGestionCourt(): ?string
    {
        return $this->nomGestionCourt;
    }
    
    public function setNomGestionCourt(string $nomGestionCourt): self
    {
        $this->nomGestionCourt = $nomGestionCourt;
        
        return $this;
    }
    
    public function getOrderInGestion(): ?int
    {
        return $this->orderInGestion;
    }
    
    public function setOrderInGestion(int $orderInGestion): self
    {
        $this->orderInGestion = $orderInGestion;
        
        return $this;
    }
    
    public function getVisibleCandidature(): ?bool
    {
        return $this->visibleCandidature;
    }
    
    public function setVisibleCandidature(bool $visibleCandidature): self
    {
        $this->visibleCandidature = $visibleCandidature;
        
        return $this;
    }
}
