<?php

namespace App\Entity;

use App\Repository\PictoPrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: PictoPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class PictoPrototype
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['picto', 'admin_picto_list', 'admin_picto'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['picto', 'admin_picto_list', 'admin_picto'])]
    private ?string $name = null;
    
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['picto', 'admin_picto'])]
    private ?string $description = null;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['picto', 'admin_picto'])]
    private ?bool $rare = null;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['picto', 'admin_picto'])]
    private ?bool $community = null;
    
    #[ORM\Column(type: 'string', length: 30)]
    #[Groups(['picto', 'admin_picto'])]
    private ?string $img = null;
    
    #[ORM\Column(type: 'string', length: 30)]
    #[Groups(['admin_picto_list', 'admin_picto'])]
    private ?string $uuid = null;
    
    /** @var Collection<Pictos> */
    #[ORM\OneToMany(mappedBy: 'picto', targetEntity: Pictos::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $pictos;
    
    /** @var Collection<PictoTitrePrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'pictoPrototype', targetEntity: PictoTitrePrototype::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['admin_picto', 'picto'])]
    private Collection $titre;
    
    #[ORM\Column(name: 'id_mh', nullable: true)]
    #[Groups(['admin_picto'])]
    private ?int $idMh = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false])]
    #[Groups(['admin_picto_list', 'admin_picto'])]
    private ?bool $actif = null;
    
    /**
     * @var Collection<int, DecouverteTitre>
     */
    #[ORM\OneToMany(mappedBy: 'picto', targetEntity: DecouverteTitre::class, orphanRemoval: true)]
    private Collection $decouverteTitres;
    
    public function __construct()
    {
        $this->pictos           = new ArrayCollection();
        $this->titre            = new ArrayCollection();
        $this->decouverteTitres = new ArrayCollection();
    }
    
    public function addDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if (!$this->decouverteTitres->contains($decouverteTitre)) {
            $this->decouverteTitres->add($decouverteTitre);
            $decouverteTitre->setPicto($this);
        }
        
        return $this;
    }
    
    public function addPicto(Pictos $picto): self
    {
        if (!$this->pictos->contains($picto)) {
            $this->pictos[] = $picto;
            $picto->setPicto($this);
        }
        
        return $this;
    }
    
    public function addTitre(PictoTitrePrototype $titre): self
    {
        if (!$this->titre->contains($titre)) {
            $this->titre[] = $titre;
            $titre->setPictoPrototype($this);
        }
        
        return $this;
    }
    
    public function getCommunity(): ?bool
    {
        return $this->community;
    }
    
    public function setCommunity(?bool $community): PictoPrototype
    {
        $this->community = $community;
        
        return $this;
    }
    
    /**
     * @return Collection<int, DecouverteTitre>
     */
    public function getDecouverteTitres(): Collection
    {
        return $this->decouverteTitres;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): PictoPrototype
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): PictoPrototype
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdMh(): ?int
    {
        return $this->idMh;
    }
    
    public function setIdMh(?int $idMh): self
    {
        $this->idMh = $idMh;
        
        return $this;
    }
    
    public function getImg(): ?string
    {
        return $this->img;
    }
    
    public function setImg(?string $img): PictoPrototype
    {
        $this->img = $img;
        
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    public function setName(?string $name): PictoPrototype
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function getPictos(): Collection
    {
        return $this->pictos;
    }
    
    public function getRare(): ?bool
    {
        return $this->rare;
    }
    
    public function setRare(?bool $rare): PictoPrototype
    {
        $this->rare = $rare;
        
        return $this;
    }
    
    /**
     * @return Collection<PictoTitrePrototype>
     */
    public function getTitre(): Collection
    {
        return $this->titre;
    }
    
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
    
    public function setUuid(?string $uuid): PictoPrototype
    {
        $this->uuid = $uuid;
        
        return $this;
    }
    
    public function isActif(): ?bool
    {
        return $this->actif;
    }
    
    public function removeAllTitre(): self
    {
        
        foreach ($this->titre as $titre) {
            if ($this->titre->removeElement($titre)) {
                // set the owning side to null (unless already changed)
                if ($titre->getPictoPrototype() === $this) {
                    $titre->setPictoPrototype(null);
                }
            }
        }
        
        return $this;
    }
    
    public function removeDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if ($this->decouverteTitres->removeElement($decouverteTitre)) {
            // set the owning side to null (unless already changed)
            if ($decouverteTitre->getPicto() === $this) {
                $decouverteTitre->setPicto(null);
            }
        }
        
        return $this;
    }
    
    public function removePicto(Pictos $picto): self
    {
        if ($this->pictos->removeElement($picto)) {
            // set the owning side to null (unless already changed)
            if ($picto->getPicto() === $this) {
                $picto->setPicto(null);
            }
        }
        
        return $this;
    }
    
    public function removeTitre(PictoTitrePrototype $titre): self
    {
        if ($this->titre->removeElement($titre)) {
            // set the owning side to null (unless already changed)
            if ($titre->getPictoPrototype() === $this) {
                $titre->setPictoPrototype(null);
            }
        }
        
        return $this;
    }
    
    public function setActif(?bool $actif): self
    {
        $this->actif = $actif;
        
        return $this;
    }
    
}
