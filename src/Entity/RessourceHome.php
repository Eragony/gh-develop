<?php

namespace App\Entity;

use App\Repository\RessourceHomeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RessourceHomeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class RessourceHome
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['outils_chantier', 'ency_hab'])]
    private int $id;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false), Groups(['outils_chantier',
                                                                                            'ency_hab'])]
    private ?ItemPrototype $Item = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_chantier', 'ency_hab'])]
    private ?int $nombre = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->Item;
    }
    
    public function setItem(?ItemPrototype $Item): self
    {
        $this->Item = $Item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
}
