<?php

namespace App\Entity;

use App\Repository\ItemProbabilityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ItemProbabilityRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ItemProbability
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ency', 'admin_ass'])]
    private ?int $id;
    
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'itemObtains')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['ency', 'admin_ass'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?ItemPrototype $item;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency', 'admin_ass'])]
    private ?int $taux;
    
    /** @var Collection<ListAssemblage> */
    #[ORM\ManyToMany(targetEntity: ListAssemblage::class, mappedBy: 'itemObtain', fetch: 'EXTRA_LAZY')]
    private Collection $listAssemblage;
    
    public function __construct(ItemPrototype $item, ?int $taux)
    {
        $this->id             = ($taux ?? 0) * 10000 + $item->getId();
        $this->item           = $item;
        $this->taux           = $taux;
        $this->listAssemblage = new ArrayCollection();
        
    }
    
    public function addListingAssemblage(ListAssemblage $listingAssemblage): self
    {
        if (!$this->listAssemblage->contains($listingAssemblage)) {
            $this->listAssemblage[] = $listingAssemblage;
            $listingAssemblage->addItemObtain($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ItemProbability
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getListAssemblages(): Collection
    {
        return $this->listAssemblage;
    }
    
    public function getTaux(): ?int
    {
        return $this->taux;
    }
    
    public function setTaux(?int $taux): self
    {
        $this->taux = $taux;
        
        return $this;
    }
    
    public function removeListingAssemblage(ListAssemblage $listingAssemblage): self
    {
        if ($this->listAssemblage->removeElement($listingAssemblage)) {
            $listingAssemblage->removeItemObtain($this);
        }
        
        return $this;
    }
}
