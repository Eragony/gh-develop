<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

/**
 * RessourceChantier
 */
#[ORM\Table(name: 'ressource_chantier')]
#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class RessourceChantier
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['admin'])]
    private ?int $id;
    
    #[ORM\Column(name: 'nombre', type: 'smallint', nullable: false)]
    #[Groups(['chantier', 'admin'])]
    private ?int $nombre;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: 'ItemPrototype', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id')]
    #[Groups(['chantier', 'admin'])]
    private ItemPrototype $item;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): RessourceChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    
}
