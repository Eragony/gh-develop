<?php

namespace App\Entity;

use App\Repository\DefenseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DefenseRepository::class)]
class Defense
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'defense')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'integer'), Groups(['defense', 'comparatif'])]
    private ?int $total = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $buildings = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $upgrades = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $objet = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $bonusOd = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $maisonCitoyen = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $gardiens = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $veilleurs = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $ames = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $morts = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
    private ?int $tempos = 0;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $bonusSd = 0;
    
    private ?int $bonusSdPct = 0;
    
    public function __construct(#[ORM\Column(type: 'smallint'), Groups(['defense', 'comparatif'])]
                                private int $day)
    {
    }
    
    public function defHSD(): int
    {
        return $this->getTotal() - $this->getBonusSd();
    }
    
    public function defSD(): int
    {
        return $this->getBonusSd();
    }
    
    public function getAmes(): ?int
    {
        return $this->ames;
    }
    
    public function setAmes(int $ames): self
    {
        $this->ames = $ames;
        
        return $this;
    }
    
    public function getBonusOd(): ?int
    {
        return $this->bonusOd;
    }
    
    public function setBonusOd(int $bonusOd): self
    {
        $this->bonusOd = $bonusOd;
        
        return $this;
    }
    
    #[Groups(['defense', 'comparatif'])]
    public function getBonusSd(): ?float
    {
        return $this->bonusSd;
    }
    
    public function setBonusSd(int $bonusSd): self
    {
        $this->bonusSd = $bonusSd;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    #[Groups(['defense', 'comparatif'])]
    public function getBonusSdPct(): ?int
    {
        return $this->bonusSdPct;
    }
    
    /**
     * @param int|null $bonusSdPct
     * @return Defense
     */
    public function setBonusSdPct(?int $bonusSdPct): Defense
    {
        $this->bonusSdPct = $bonusSdPct;
        return $this;
    }
    
    public function getBuildings(): ?int
    {
        return $this->buildings;
    }
    
    public function setBuildings(int $buildings): self
    {
        $this->buildings = $buildings;
        
        return $this;
    }
    
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    #[Groups(['defense', 'comparatif'])]
    public function getDefHSD(): int
    {
        return $this->getTotal() - $this->getBonusSd();
    }
    
    #[Groups(['defense', 'comparatif'])]
    public function getDefSD(): int
    {
        return $this->getBonusSd();
    }
    
    public function getGardiens(): ?int
    {
        return $this->gardiens;
    }
    
    public function setGardiens(int $gardiens): self
    {
        $this->gardiens = $gardiens;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return Defense
     */
    public function setId(int $id): Defense
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMaisonCitoyen(): ?int
    {
        return $this->maisonCitoyen;
    }
    
    public function setMaisonCitoyen(int $maisonCitoyen): self
    {
        $this->maisonCitoyen = $maisonCitoyen;
        
        return $this;
    }
    
    public function getMorts(): ?int
    {
        return $this->morts;
    }
    
    public function setMorts(int $morts): self
    {
        $this->morts = $morts;
        
        return $this;
    }
    
    public function getObjet(): ?int
    {
        return $this->objet;
    }
    
    public function setObjet(int $objet): self
    {
        $this->objet = $objet;
        
        return $this;
    }
    
    public function getTempos(): ?int
    {
        return $this->tempos;
    }
    
    public function setTempos(int $tempos): self
    {
        $this->tempos = $tempos;
        
        return $this;
    }
    
    public function getTotal(): ?int
    {
        return $this->total;
    }
    
    public function setTotal(int $total): self
    {
        $this->total = $total;
        
        return $this;
    }
    
    public function getUpgrades(): ?int
    {
        return $this->upgrades;
    }
    
    public function setUpgrades(int $upgrades): self
    {
        $this->upgrades = $upgrades;
        
        return $this;
    }
    
    public function getVeilleurs(): ?int
    {
        return $this->veilleurs;
    }
    
    public function setVeilleurs(int $veilleurs): self
    {
        $this->veilleurs = $veilleurs;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
}
