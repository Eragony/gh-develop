<?php

namespace App\Entity;

use App\Repository\CoalitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CoalitionRepository::class)]
class Coalition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['coalition'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['coalition'])]
    private ?User $user = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['coalition'])]
    private ?int $numCoa = null;
    
    /** @var Collection<DispoJump> */
    #[ORM\OneToMany(mappedBy: 'coalition', targetEntity: DispoJump::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true),
        Groups(['coalition'])]
    private Collection $dispos;
    
    #[ORM\ManyToOne(targetEntity: StatutUser::class, fetch: 'EXTRA_LAZY'), Groups(['coalition'])]
    private ?StatutUser $statut = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY', inversedBy: 'coalitions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jump $jump = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['coalition'])]
    private int $positionCoa;
    
    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false]), Groups(['coalition'])]
    private ?bool $createur = false;
    
    public function __construct()
    {
        
        $this->dispos = new ArrayCollection();
    }
    
    public function addDispo(DispoJump $dispo): self
    {
        if (!$this->dispos->contains($dispo)) {
            $this->dispos[] = $dispo;
            $dispo->setCoalition($this);
        }
        
        return $this;
    }
    
    public function getDispo(int $i): DispoJump
    {
        return $this->dispos[$i];
    }
    
    public function getDispos(): Collection|array
    {
        return $this->dispos;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): Coalition
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(?Jump $jump): self
    {
        $this->jump = $jump;
        
        return $this;
    }
    
    public function getNumCoa(): ?int
    {
        return $this->numCoa;
    }
    
    public function setNumCoa(int $numCoa): self
    {
        $this->numCoa = $numCoa;
        
        return $this;
    }
    
    public function getPositionCoa(): ?int
    {
        return $this->positionCoa;
    }
    
    public function setPositionCoa(int $positionCoa): self
    {
        $this->positionCoa = $positionCoa;
        
        return $this;
    }
    
    public function getStatut(): ?StatutUser
    {
        return $this->statut;
    }
    
    public function setStatut(?StatutUser $statut): self
    {
        $this->statut = $statut;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function isCreateur(): ?bool
    {
        return $this->createur;
    }
    
    public function removeDispo(DispoJump $dispo): self
    {
        if ($this->dispos->removeElement($dispo)) {
            // set the owning side to null (unless already changed)
            if ($dispo->getCoalition() === $this) {
                $dispo->setCoalition(null);
            }
        }
        
        return $this;
    }
    
    public function setCreateur(?bool $createur): static
    {
        $this->createur = $createur;
        
        return $this;
    }
}
