<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

/**
 * User
 */
#[ORM\Table(name: 'user')]
#[ORM\UniqueConstraint(name: 'UNIQ_8D93D6497BA2F5EB', columns: ['api_token'])]
#[ORM\UniqueConstraint(name: 'UNIQ_8D93D64945B79F11', columns: ['id'])]
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface
{
    
    
    public const THEME_LIGHT        = 'light';
    public const THEME_DARK         = 'dark';
    public const THEME_VINTAGE      = 'vintage';
    public const THEME_HORDES       = 'hordes';
    public const THEME_PERSO        = 'perso';
    public const LIST_THEME         = [
        self::THEME_LIGHT,
        self::THEME_DARK,
        self::THEME_VINTAGE,
        self::THEME_HORDES,
        self::THEME_PERSO,
    ];
    public const LIST_THEME_DEFAULT = [
        self::THEME_LIGHT,
        self::THEME_DARK,
        self::THEME_VINTAGE,
        self::THEME_HORDES,
    ];
    
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(name: 'id', type: 'integer', nullable: false),
        Groups(['admin', 'admin_gen', 'carte_user', 'carte_gen', 'general', 'general_res', 'citoyens', 'option', 'comparatif'])]
    private int $id = 0;
    
    #[ORM\Column(type: 'integer', unique: true, nullable: true),
        Groups(['admin', 'general', 'general_res', 'citoyens', 'comparatif'])]
    private ?int $idMyHordes = null;
    
    #[ORM\Column(type: 'integer', unique: true, nullable: true)]
    private ?int $idTwinoid = null;
    
    #[ORM\Column(name: 'map_id_mh', type: 'integer', nullable: true, options: ['default' => null]),
        Groups(['admin', 'admin_gen', 'carte_user', 'general', 'general_res', 'citoyens'])]
    private ?int $mapId = null;
    
    #[ORM\Column(type: 'integer', nullable: true, options: ['default' => null])]
    private ?int $mapIdHordes = null;
    
    /**
     * @var string[] $roles
     */
    #[ORM\Column(name: 'roles', type: 'json', nullable: false), Groups(['admin', 'general'])]
    private array $roles = ['ROLE_USER'];
    
    #[ORM\Column(name: 'pseudo', type: 'string', length: 255, nullable: false),
        Groups(['admin', 'admin_gen', 'carte_user', 'carte_gen', 'general', 'general_res', 'citoyens', 'news', 'classement',
                'option', 'comparatif'])]
    private string $pseudo = '';
    
    #[ORM\Column(name: 'avatar', type: 'string', length: 255, nullable: true),
        Groups(['admin', 'general', 'citoyens', 'gestion_jump', 'avatar', 'comparatif'])]
    private ?string $avatar = null;
    
    #[ORM\Column(name: 'heros', type: 'boolean', nullable: false, options: ['default' => false]), Groups(['admin'])]
    private bool|int $heros = false;
    
    #[ORM\Column(name: 'date_maj', type: 'datetime', nullable: true), Groups(['admin', 'admin_gen', 'option'])]
    private ?DateTimeInterface $dateMaj = null;
    
    #[ORM\Column(name: 'periode_rappel', type: 'smallint', nullable: false, options: ['default' => 0]),
        Groups(['admin', 'option'])]
    private int $periodeRappel = 0;
    
    #[ORM\Column(name: 'poss_apag', type: 'boolean', nullable: false, options: ['default' => false]),
        Groups(['admin', 'option'])]
    private bool|int $possApag = false;
    
    #[ORM\Column(name: 'tem_arma', type: 'boolean', nullable: false, options: ['default' => false]),
        Groups(['admin', 'citoyens', 'option', 'coalition', 'candidature_jump'])]
    private bool|int $temArma = false;
    
    #[ORM\Column(name: 'force_maj', type: 'boolean', nullable: false, options: ['default' => true]), Groups(['admin'])]
    private bool|int $forceMaj = true;
    
    #[ORM\Column(name: 'legend', type: 'boolean', nullable: false, options: ['default' => false]),
        Groups(['admin', 'citoyens', 'option', 'coalition', 'candidature_jump'])]
    private bool|int $legend = false;
    
    #[ORM\Column(name: 'annonce_news', type: 'boolean', nullable: false, options: ['default' => false]), Groups(['admin'])]
    private bool|int $annonceNews = false;
    
    #[ORM\Column(name: 'api_token', type: 'string', length: 255, nullable: true), Groups(['admin'])]
    private ?string $apiToken = null;
    
    #[ORM\ManyToOne(targetEntity: HerosPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'der_pouv_id', referencedColumnName: 'id', nullable: false),
        Groups(['admin', 'carte_user', 'carte_gen', 'general', 'citoyens', 'option', 'coalition', 'gestion_jump', 'outils_expe', 'inscription_exp_visu']),
        SerializedName('pouvoir')]
    private ?HerosPrototype $derPouv = null;
    
    /** @var Collection<LeadJump> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: LeadJump::class, fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: '')]
    private Collection $leadJumps;
    
    /** @var Collection<InscriptionJump> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: InscriptionJump::class, fetch: 'EXTRA_LAZY', indexBy: 'jump_id')]
    private Collection $inscriptionJumps;
    
    /** @var Collection<Jump> */
    #[ORM\OneToMany(mappedBy: 'createdBy', targetEntity: Jump::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $jumps;
    
    /** @var Collection<Jump> */
    #[ORM\ManyToMany(targetEntity: Jump::class, mappedBy: 'gestionnaires', fetch: 'EXTRA_LAZY')]
    private Collection $gestionJumps;
    
    #[ORM\Column(type: 'string', length: 15, options: ['default' => 'hordes']), Groups(['admin', 'option', 'general'])]
    private ?string $theme = User::THEME_HORDES;
    
    /** @var Collection<Coalition> */
    #[ORM\ManyToMany(targetEntity: Event::class, mappedBy: 'organisateurs', fetch: 'EXTRA_LAZY'), Groups(['admin_jump'])]
    private Collection $orgaEvents;
    
    /** @var Collection<Coalition> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Pictos::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'picto_id')]
    private Collection $pictos;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['general', 'admin'])]
    private ?DateTimeInterface $dateMajAme = null;
    
    #[ORM\Column(type: 'string', length: 2, options: ['default' => 'fr']), Groups(['admin', 'general', 'general_res'])]
    private ?string $lang = 'fr';
    
    #[ORM\Column(type: 'string', length: 255, nullable: true), Groups(['admin'])]
    private ?string $tokenVeille = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['admin'])]
    private ?DateTimeInterface $dateExpTokenVeille = null;
    
    #[ORM\Column(type: 'integer', nullable: true, options: ['default' => 0]), Groups(['admin'])]
    private ?int $villeMaj = null;
    
    /** @var Collection<Coalition> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: DispoUserType::class,
        cascade: ['persist', 'remove'], fetch: 'EAGER', orphanRemoval: true, indexBy: 'id')]
    private Collection $dispoType;
    
    
    #[ORM\Column(nullable: true, options: ['default' => 0]), Groups(['admin'])]
    private ?int $mapIdMajVille = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true]), Groups(['admin'])]
    private ?bool $forceMajOldTown = true;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0]), Groups(['admin', 'citoyens', 'option', 'coalition'])]
    private ?int $nbChargeCamaraderie = 0;
    
    #[ORM\Column(length: 255, nullable: false, options: ['default' => 'Europe/Paris']),
        Groups(['admin', 'general_res', 'carte_user', 'option'])]
    private ?string $fuseau = 'Europe/Paris';
    
    #[ORM\Column(nullable: true)]
    private ?DateTimeImmutable $lastConnexionAt = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['option', 'picto'])]
    private ?bool $showHistoPicto = false;
    
    /** @var Collection<ThemeUser> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ThemeUser::class, cascade: ['persist', 'remove'],
        orphanRemoval: true), Groups(['admin', 'option', 'general'])]
    private Collection $themesUser;
    
    #[ORM\Column(nullable: false, options: ['default' => '0']), Groups(['admin', 'option'])]
    private ?bool $majCo = false;
    
    /** @var Collection<RuineGame> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: RuineGame::class, orphanRemoval: true)]
    private Collection $ruineGames;
    
    #[ORM\Column(length: 24, nullable: true), Groups(['ruine', 'admin'])]
    private ?string $activeRuine = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin'])]
    private ?bool $clientUpdated = false;
    
    /** @var Collection<DispoUserTypeExpedition> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: DispoUserTypeExpedition::class, orphanRemoval: true), Groups(['admin', 'option', 'expe'])]
    private Collection $dispoUserTypeExpeditions;
    
    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove']), Groups(['admin', 'ame', 'expe', 'carte_user', 'option', 'general', 'citoyens', 'ruine'])]
    private ?UserPersonnalisation $userPersonnalisation = null;
    
    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove']), Groups(['option', 'general'])]
    private ?Menu $menu = null;
    
    /**
     * @var Collection<int, DecouverteTitre>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: DecouverteTitre::class, orphanRemoval: true)]
    private Collection $decouverteTitres;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0]), Groups(['admin', 'option', 'citoyens', 'jump', 'candidature_jump', 'gestion_jump', 'coalition'])]
    private ?int $nbrPointsCompetence = 0;
    
    /**
     * @var Collection<int, HerosSkillLevel>
     */
    #[ORM\ManyToMany(targetEntity: HerosSkillLevel::class, inversedBy: 'users')]
    #[ORM\JoinTable(name: 'user_skill'), Groups(['admin', 'option', 'citoyens', 'jump', 'candidature_jump', 'gestion_jump', 'coalition'])]
    private Collection $skill;
    
    /**
     * @var Collection<int, HerosSkillLevel>
     */
    #[ORM\ManyToMany(targetEntity: HerosSkillLevel::class)]
    #[ORM\JoinTable(name: 'user_skill_type'), Groups(['admin', 'option', 'citoyens'])]
    private Collection $skillType;
    
    
    public function __construct()
    {
        $this->gestionJumps             = new ArrayCollection();
        $this->leadJumps                = new ArrayCollection();
        $this->inscriptionJumps         = new ArrayCollection();
        $this->jumps                    = new ArrayCollection();
        $this->orgaEvents               = new ArrayCollection();
        $this->pictos                   = new ArrayCollection();
        $this->dispoType                = new ArrayCollection();
        $this->themesUser               = new ArrayCollection();
        $this->ruineGames               = new ArrayCollection();
        $this->dispoUserTypeExpeditions = new ArrayCollection();
        $this->decouverteTitres         = new ArrayCollection();
        $this->skill                    = new ArrayCollection();
        $this->skillType                = new ArrayCollection();
    }
    
    public function addDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if (!$this->decouverteTitres->contains($decouverteTitre)) {
            $this->decouverteTitres->add($decouverteTitre);
            $decouverteTitre->setUser($this);
        }
        
        return $this;
    }
    
    public function addDispoType(DispoUserType $dispoType): self
    {
        if (!$this->dispoType->contains($dispoType)) {
            $this->dispoType[$dispoType->getId()] = $dispoType;
            $dispoType->setUser($this);
        }
        
        return $this;
    }
    
    public function addDispoUserTypeExpedition(DispoUserTypeExpedition $dispoUserTypeExpedition): static
    {
        if (!$this->dispoUserTypeExpeditions->contains($dispoUserTypeExpedition)) {
            $this->dispoUserTypeExpeditions->add($dispoUserTypeExpedition);
            $dispoUserTypeExpedition->setUser($this);
        }
        
        return $this;
    }
    
    public function addGestionJump(Jump $gestionJump): self
    {
        if (!$this->gestionJumps->contains($gestionJump)) {
            $this->gestionJumps[] = $gestionJump;
            $gestionJump->addGestionnaire($this);
        }
        
        return $this;
    }
    
    public function addInscriptionJump(InscriptionJump $inscriptionJump): self
    {
        if (!$this->inscriptionJumps->contains($inscriptionJump)) {
            $this->inscriptionJumps[$inscriptionJump->getJump()->getId()] = $inscriptionJump;
            $inscriptionJump->setUser($this);
        }
        
        return $this;
    }
    
    public function addJump(Jump $jump): self
    {
        if (!$this->jumps->contains($jump)) {
            $this->jumps[] = $jump;
            $jump->setCreatedBy($this);
        }
        
        return $this;
    }
    
    public function addLeadJump(LeadJump $leadJump): self
    {
        if (!$this->leadJumps->contains($leadJump)) {
            $this->leadJumps[] = $leadJump;
            $leadJump->setUser($this);
        }
        
        return $this;
    }
    
    public function addOrgaEvent(Event $orgaEvent): self
    {
        if (!$this->orgaEvents->contains($orgaEvent)) {
            $this->orgaEvents[] = $orgaEvent;
            $orgaEvent->addOrganisateur($this);
        }
        
        return $this;
    }
    
    public function addPicto(Pictos $picto): self
    {
        if (!$this->pictos->contains($picto)) {
            $this->pictos[$picto->getPicto()->getId()] = $picto;
            $picto->setUser($this);
        }
        
        return $this;
    }
    
    public function addRuineGame(RuineGame $ruineGame): static
    {
        if (!$this->ruineGames->contains($ruineGame)) {
            $this->ruineGames->add($ruineGame);
            $ruineGame->setUser($this);
        }
        
        return $this;
    }
    
    public function addSkill(HerosSkillLevel $skill): static
    {
        if (!$this->skill->contains($skill)) {
            $this->skill->add($skill);
        }
        
        return $this;
    }
    
    public function addSkillType(HerosSkillLevel $skillType): static
    {
        if (!$this->skillType->contains($skillType)) {
            $this->skillType->add($skillType);
        }
        
        return $this;
    }
    
    public function addThemesUser(ThemeUser $themesUser): static
    {
        if (!$this->themesUser->contains($themesUser)) {
            $this->themesUser->add($themesUser);
            $themesUser->setUser($this);
        }
        
        return $this;
    }
    
    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }
    
    public function getRoles(): array
    {
        return $this->roles;
    }
    
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        
        return $this;
    }
    
    public function getUserIdentifier(): string
    {
        return (string)$this->id;
    }
    
    public function getActiveRuine(): ?string
    {
        return $this->activeRuine;
    }
    
    public function setActiveRuine(?string $activeRuine): static
    {
        $this->activeRuine = $activeRuine;
        
        return $this;
    }
    
    public function getAnnonceNews(): ?bool
    {
        return $this->annonceNews;
    }
    
    public function setAnnonceNews(bool|int $annonceNews): self
    {
        $this->annonceNews = (bool)$annonceNews;
        
        return $this;
    }
    
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }
    
    public function setApiToken(?string $apiToken): self
    {
        $this->apiToken = $apiToken;
        
        return $this;
    }
    
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }
    
    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;
        
        return $this;
    }
    
    public function getDateExpTokenVeille(): ?DateTimeInterface
    {
        return $this->dateExpTokenVeille;
    }
    
    public function setDateExpTokenVeille(?DateTimeInterface $dateExpTokenVeille): self
    {
        $this->dateExpTokenVeille = $dateExpTokenVeille;
        
        return $this;
    }
    
    public function getDateMaj(): ?DateTimeInterface
    {
        return $this->dateMaj;
    }
    
    public function setDateMaj(?DateTimeInterface $dateMaj): self
    {
        $this->dateMaj = $dateMaj;
        
        return $this;
    }
    
    public function getDateMajAme(): ?DateTimeInterface
    {
        return $this->dateMajAme;
    }
    
    public function setDateMajAme(?DateTimeInterface $dateMajAme): self
    {
        $this->dateMajAme = $dateMajAme;
        
        return $this;
    }
    
    /**
     * @return Collection<int, DecouverteTitre>
     */
    public function getDecouverteTitres(): Collection
    {
        return $this->decouverteTitres;
    }
    
    public function getDerPouv(): ?HerosPrototype
    {
        return $this->derPouv;
    }
    
    public function setDerPouv(?HerosPrototype $derPouv): self
    {
        $this->derPouv = $derPouv;
        
        return $this;
    }
    
    public function getDispoType(int $idDispo): ?DispoUserType
    {
        return $this->dispoType->toArray()[$idDispo] ?? null;
    }
    
    public function setDispoType(int $typeCreneau, CreneauHorraire $creneauHorraire, TypeDispo $dispo): self
    {
        
        $idDispo = $typeCreneau * 100 + $creneauHorraire->getId();
        if (isset($this->dispoType->toArray()[$idDispo])) {
            $this->dispoType->toArray()[$idDispo]->setDispo($dispo);
        } else {
            $this->addDispoType((new DispoUserType($typeCreneau, $creneauHorraire))->setDispo($dispo));
        }
        
        return $this;
    }
    
    /**
     * @return Collection|DispoUserType[]
     */
    #[Groups(['admin', 'option'])]
    public function getDispoTypes(): Collection|array
    {
        return $this->dispoType;
    }
    
    public function getDispoUserTypeExpeditions(): Collection
    {
        return $this->dispoUserTypeExpeditions;
    }
    
    public function getForceMaj(): ?bool
    {
        return $this->forceMaj;
    }
    
    public function setForceMaj(bool|int $forceMaj): self
    {
        $this->forceMaj = (bool)$forceMaj;
        
        return $this;
    }
    
    public function getFuseau(): ?string
    {
        return $this->fuseau;
    }
    
    public function setFuseau(string $fuseau): static
    {
        $this->fuseau = $fuseau;
        
        return $this;
    }
    
    /**
     * @return Collection<int, Jump>
     */
    public function getGestionJumps(): Collection
    {
        return $this->gestionJumps;
    }
    
    public function getHeros(): ?bool
    {
        return $this->heros;
    }
    
    public function setHeros(bool|int $heros): self
    {
        $this->heros = (bool)$heros;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdMyHordes(): ?int
    {
        return $this->idMyHordes;
    }
    
    public function setIdMyHordes(?int $idMyHordes): self
    {
        $this->idMyHordes = $idMyHordes;
        
        return $this;
    }
    
    public function getIdTwinoid(): ?int
    {
        return $this->idTwinoid;
    }
    
    public function setIdTwinoid(?int $idTwinoid): self
    {
        $this->idTwinoid = $idTwinoid;
        
        return $this;
    }
    
    public function getInscriptionJump(string $idJump): ?InscriptionJump
    {
        return $this->inscriptionJumps->toArray()[$idJump] ?? null;
    }
    
    /**
     * @return Collection<int, InscriptionJump>
     */
    public function getInscriptionJumps(): Collection
    {
        return $this->inscriptionJumps;
    }
    
    public function getJumps(): Collection
    {
        return $this->jumps;
    }
    
    public function getLang(): ?string
    {
        return $this->lang;
    }
    
    public function setLang(?string $lang): self
    {
        $this->lang = $lang;
        
        return $this;
    }
    
    public function getLastConnexionAt(): ?DateTimeImmutable
    {
        return $this->lastConnexionAt;
    }
    
    public function setLastConnexionAt(?DateTimeImmutable $lastConnexionAt): static
    {
        $this->lastConnexionAt = $lastConnexionAt;
        
        return $this;
    }
    
    /**
     * @return Collection<int, LeadJump>
     */
    public function getLeadJumps(): Collection
    {
        return $this->leadJumps;
    }
    
    public function getLegend(): ?bool
    {
        return $this->legend;
    }
    
    public function setLegend(bool|int $legend): self
    {
        $this->legend = (bool)$legend;
        
        return $this;
    }
    
    public function getLienVersAme(string $url): string
    {
        return "<a href='$url' class='lienVersAme'>{$this->getPseudo()}</a>";
    }
    
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    public function setMapId(int|null $mapId): self
    {
        $this->mapId = $mapId;
        
        return $this;
    }
    
    public function getMapIdHordes(): ?int
    {
        return $this->mapIdHordes;
    }
    
    public function setMapIdHordes(?int $mapIdHordes): self
    {
        $this->mapIdHordes = $mapIdHordes;
        
        return $this;
    }
    
    public function getMapIdMajVille(): ?int
    {
        return $this->mapIdMajVille;
    }
    
    public function setMapIdMajVille(?int $mapIdMajVille): self
    {
        $this->mapIdMajVille = $mapIdMajVille;
        
        return $this;
    }
    
    public function getMenu(): ?Menu
    {
        return $this->menu;
    }
    
    public function setMenu(?Menu $menu): static
    {
        // unset the owning side of the relation if necessary
        if ($menu === null && $this->menu !== null) {
            $this->menu->setUser(null);
        }
        
        // set the owning side of the relation if necessary
        if ($menu !== null && $menu->getUser() !== $this) {
            $menu->setUser($this);
        }
        
        $this->menu = $menu;
        
        return $this;
    }
    
    public function getNbChargeCamaraderie(): ?int
    {
        return $this->nbChargeCamaraderie;
    }
    
    public function setNbChargeCamaraderie(int $nbChargeCamaraderie): static
    {
        $this->nbChargeCamaraderie = $nbChargeCamaraderie;
        
        return $this;
    }
    
    public function getNbrPointsCompetence(): ?int
    {
        return $this->nbrPointsCompetence;
    }
    
    public function setNbrPointsCompetence(int $nbrPointsCompetence): static
    {
        $this->nbrPointsCompetence = $nbrPointsCompetence;
        
        return $this;
    }
    
    public function getOrgaEvents(): Collection
    {
        return $this->orgaEvents;
    }
    
    public function getPassword(): null
    {
        return null;
    }
    
    public function getPeriodeRappel(): ?int
    {
        return $this->periodeRappel;
    }
    
    public function setPeriodeRappel(int $periodeRappel): self
    {
        $this->periodeRappel = $periodeRappel;
        
        return $this;
    }
    
    public function getPicto(int $idPicto): Pictos
    {
        return $this->pictos->toArray()[$idPicto];
    }
    
    public function getPictos(): Collection
    {
        return $this->pictos;
    }
    
    public function getPossApag(): ?bool
    {
        return $this->possApag;
    }
    
    public function setPossApag(bool|int $possApag): self
    {
        $this->possApag = (bool)$possApag;
        
        return $this;
    }
    
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }
    
    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;
        
        return $this;
    }
    
    public function getRuineGames(): Collection
    {
        return $this->ruineGames;
    }
    
    public function getSalt(): null
    {
        return null;
    }
    
    /**
     * @return Collection<int, HerosSkillLevel>
     */
    public function getSkill(): Collection
    {
        return $this->skill;
    }
    
    /**
     * @return Collection<int, HerosSkillLevel>
     */
    public function getSkillType(): Collection
    {
        return $this->skillType;
    }
    
    public function getTemArma(): ?bool
    {
        return $this->temArma;
    }
    
    public function setTemArma(bool|int $temArma): self
    {
        $this->temArma = (bool)$temArma;
        
        return $this;
    }
    
    public function getTheme(): ?string
    {
        return $this->theme;
    }
    
    public function setTheme(string $theme): self
    {
        $this->theme = $theme;
        
        return $this;
    }
    
    /**
     * @return Collection<int, ThemeUser>
     */
    public function getThemesUser(): Collection
    {
        return $this->themesUser;
    }
    
    public function getTokenVeille(): ?string
    {
        return $this->tokenVeille;
    }
    
    public function setTokenVeille(?string $tokenVeille): self
    {
        $this->tokenVeille = $tokenVeille;
        
        return $this;
    }
    
    public function getUserPersonnalisation(): ?UserPersonnalisation
    {
        return $this->userPersonnalisation;
    }
    
    public function setUserPersonnalisation(UserPersonnalisation $userPersonnalisation): static
    {
        // set the owning side of the relation if necessary
        if ($userPersonnalisation->getUser() !== $this) {
            $userPersonnalisation->setUser($this);
        }
        
        $this->userPersonnalisation = $userPersonnalisation;
        
        return $this;
    }
    
    public function getUsername(): string
    {
        return $this->getId() . '_' . $this->getPseudo();
    }
    
    public function getVilleMaj(): ?int
    {
        return $this->villeMaj;
    }
    
    public function setVilleMaj(?int $villeMaj): self
    {
        $this->villeMaj = $villeMaj;
        
        return $this;
    }
    
    public function isClientUpdated(): ?bool
    {
        return $this->clientUpdated;
    }
    
    public function isForceMajOldTown(): ?bool
    {
        return $this->forceMajOldTown;
    }
    
    public function isMajCo(): ?bool
    {
        return $this->majCo;
    }
    
    public function isShowHistoPicto(): ?bool
    {
        return $this->showHistoPicto;
    }
    
    public function removeDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if ($this->decouverteTitres->removeElement($decouverteTitre)) {
            // set the owning side to null (unless already changed)
            if ($decouverteTitre->getUser() === $this) {
                $decouverteTitre->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeDispoType(DispoUserType $dispoType): self
    {
        if ($this->dispoType->removeElement($dispoType)) {
            // set the owning side to null (unless already changed)
            if ($dispoType->getUser() === $this) {
                $dispoType->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeDispoUserTypeExpedition(DispoUserTypeExpedition $dispoUserTypeExpedition): static
    {
        if ($this->dispoUserTypeExpeditions->removeElement($dispoUserTypeExpedition)) {
            // set the owning side to null (unless already changed)
            if ($dispoUserTypeExpedition->getUser() === $this) {
                $dispoUserTypeExpedition->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeGestionJump(Jump $gestionJump): self
    {
        if ($this->gestionJumps->removeElement($gestionJump)) {
            $gestionJump->removeGestionnaire($this);
        }
        
        return $this;
    }
    
    public function removeInscriptionJump(InscriptionJump $inscriptionJump): self
    {
        if ($this->inscriptionJumps->removeElement($inscriptionJump)) {
            // set the owning side to null (unless already changed)
            if ($inscriptionJump->getUser() === $this) {
                $inscriptionJump->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeJump(Jump $jump): self
    {
        if ($this->jumps->removeElement($jump)) {
            // set the owning side to null (unless already changed)
            if ($jump->getCreatedBy() === $this) {
                $jump->setCreatedBy(null);
            }
        }
        
        return $this;
    }
    
    public function removeLeadJump(LeadJump $leadJump): self
    {
        if ($this->leadJumps->removeElement($leadJump)) {
            // set the owning side to null (unless already changed)
            if ($leadJump->getUser() === $this) {
                $leadJump->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeOrgaEvent(Event $orgaEvent): self
    {
        if ($this->orgaEvents->removeElement($orgaEvent)) {
            $orgaEvent->removeOrganisateur($this);
        }
        
        return $this;
    }
    
    public function removePicto(Pictos $picto): self
    {
        if ($this->pictos->removeElement($picto)) {
            // set the owning side to null (unless already changed)
            if ($picto->getUser() === $this) {
                $picto->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeRuineGame(RuineGame $ruineGame): static
    {
        if ($this->ruineGames->removeElement($ruineGame)) {
            // set the owning side to null (unless already changed)
            if ($ruineGame->getUser() === $this) {
                $ruineGame->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function removeSkill(HerosSkillLevel $skill): static
    {
        $this->skill->removeElement($skill);
        
        return $this;
    }
    
    public function removeSkillType(HerosSkillLevel $skillType): static
    {
        $this->skillType->removeElement($skillType);
        
        return $this;
    }
    
    public function removeThemesUser(ThemeUser $themesUser): static
    {
        if ($this->themesUser->removeElement($themesUser)) {
            // set the owning side to null (unless already changed)
            if ($themesUser->getUser() === $this) {
                $themesUser->setUser(null);
            }
        }
        
        return $this;
    }
    
    public function setClientUpdated(bool $clientUpdated): static
    {
        $this->clientUpdated = $clientUpdated;
        
        return $this;
    }
    
    public function setForceMajOldTown(bool $forceMajOldTown): self
    {
        $this->forceMajOldTown = $forceMajOldTown;
        
        return $this;
    }
    
    public function setMajCo(bool $majCo): static
    {
        $this->majCo = $majCo;
        
        return $this;
    }
    
    public function setShowHistoPicto(bool $showHistoPicto): static
    {
        $this->showHistoPicto = $showHistoPicto;
        
        return $this;
    }
    
}
