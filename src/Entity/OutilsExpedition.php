<?php

namespace App\Entity;

use App\Doctrine\IdAlphaOutilsExpeditionGenerator;
use App\Repository\OutilsExpeditionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OutilsExpeditionRepository::class)]
class OutilsExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaOutilsExpeditionGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    #[Groups(['outils_expe'])]
    private ?string $id = null;
    
    #[ORM\OneToOne(mappedBy: 'expedition', cascade: ['persist', 'remove'])]
    private ?Outils $outils = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['outils_expe'])]
    private ?int $faoDirection = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['outils_expe'])]
    private ?int $nbrOuvrier = null;
    
    /** @var Collection<Expedition> */
    #[ORM\OneToMany(mappedBy: 'outilsExpedition', targetEntity: Expedition::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe'])]
    private Collection $expeditions;
    
    /** @var Collection<Ouvriers> */
    #[ORM\OneToMany(mappedBy: 'outilsExpedition', targetEntity: Ouvriers::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe'])]
    private Collection $ouvriers;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?DateTimeImmutable $createdAt = null;
    
    #[ORM\ManyToOne]
    #[Groups(['outils_expe'])]
    private ?User $createdBy = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?DateTimeImmutable $modify_at = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $ouvertOuvrier = false;
    
    public function __construct()
    {
        $this->expeditions = new ArrayCollection();
        $this->ouvriers    = new ArrayCollection();
    }
    
    public function addExpedition(Expedition $expedition): static
    {
        if (!$this->expeditions->contains($expedition)) {
            $this->expeditions->add($expedition);
            $expedition->setOutilsExpedition($this);
        }
        
        return $this;
    }
    
    public function addOuvrier(Ouvriers $ouvrier): static
    {
        if (!$this->ouvriers->contains($ouvrier)) {
            $this->ouvriers->add($ouvrier);
            $ouvrier->setOutilsExpedition($this);
        }
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(?DateTimeImmutable $createdAt): OutilsExpedition
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): OutilsExpedition
    {
        $this->createdBy = $createdBy;
        return $this;
    }
    
    /**
     * @return Collection<Expedition>
     */
    public function getExpeditions(): Collection
    {
        return $this->expeditions;
    }
    
    final  public function getFaoDirection(): ?int
    {
        return $this->faoDirection;
    }
    
    public function setFaoDirection(?int $faoDirection): static
    {
        $this->faoDirection = $faoDirection;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(string $id): static
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeImmutable
    {
        return $this->modify_at;
    }
    
    public function setModifyAt(?DateTimeImmutable $modify_at): OutilsExpedition
    {
        $this->modify_at = $modify_at;
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): OutilsExpedition
    {
        $this->modifyBy = $modifyBy;
        return $this;
    }
    
    public function getNbrOuvrier(): ?int
    {
        return $this->nbrOuvrier;
    }
    
    public function setNbrOuvrier(?int $nbrOuvrier): static
    {
        $this->nbrOuvrier = $nbrOuvrier;
        
        return $this;
    }
    
    public function getOutils(): ?Outils
    {
        return $this->outils;
    }
    
    public function setOutils(?Outils $outils): static
    {
        // unset the owning side of the relation if necessary
        if ($outils === null && $this->outils !== null) {
            $this->outils->setExpedition(null);
        }
        
        // set the owning side of the relation if necessary
        if ($outils !== null && $outils->getExpedition() !== $this) {
            $outils->setExpedition($this);
        }
        
        $this->outils = $outils;
        
        return $this;
    }
    
    /**
     * @return Collection<Ouvriers>
     */
    public function getOuvriers(): Collection
    {
        return $this->ouvriers;
    }
    
    public function isOuvertOuvrier(): ?bool
    {
        return $this->ouvertOuvrier;
    }
    
    public function removeExpedition(Expedition $expedition): static
    {
        if ($this->expeditions->removeElement($expedition)) {
            // set the owning side to null (unless already changed)
            if ($expedition->getOutilsExpedition() === $this) {
                $expedition->setOutilsExpedition(null);
            }
        }
        
        return $this;
    }
    
    public function removeOuvrier(Ouvriers $ouvrier): static
    {
        if ($this->ouvriers->removeElement($ouvrier)) {
            // set the owning side to null (unless already changed)
            if ($ouvrier->getOutilsExpedition() === $this) {
                $ouvrier->setOutilsExpedition(null);
            }
        }
        
        return $this;
    }
    
    public function setOuvertOuvrier(bool $ouvertOuvrier): static
    {
        $this->ouvertOuvrier = $ouvertOuvrier;
        
        return $this;
    }
}
