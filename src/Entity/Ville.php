<?php

namespace App\Entity;

use App\Repository\VilleRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'ville')]
#[ORM\Index(columns: ['map_id', 'origin'], name: 'mapid_origin')]
#[ORM\Entity(repositoryClass: VilleRepository::class)]
class Ville
{
    
    public const VILLE_RNE   = 'Région non-éloignée';
    public const VILLE_RE    = 'Région éloignée';
    public const VILLE_PANDE = 'Pandémonium';
    public const ORIGIN_H    = 1;
    public const ORIGIN_MH   = 2;
    #[ORM\Id, ORM\Column(type: 'integer'), Groups(['admin', 'admin_gen', 'general', 'gestion_jump', 'villes'])]
    private ?int $id;
    
    #[ORM\Column(type: 'string'), Groups(['admin', 'admin_gen', 'general', 'carte', 'gestion_jump', 'villes'])]
    private string $nom;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'admin_gen', 'general', 'carte', 'gestion_jump', 'villes'])]
    private int $jour = 1;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'citoyens', 'ruine', 'carte'])]
    private int $posX;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'citoyens', 'ruine', 'carte'])]
    private int $posY;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'carte', 'villes'])]
    private int $weight;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'carte', 'villes'])]
    private int $height;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'villes'])]
    private int $bonus;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin', 'general', 'carte', 'villes'])]
    private bool $prived = false;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin', 'general', 'carte', 'villes'])]
    private bool $porte = false;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin', 'general', 'carte', 'villes'])]
    private bool $hard = false;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin', 'general', 'citoyens', 'carte', 'villes'])]
    private bool $chaos = false;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin', 'general', 'citoyens', 'carte', 'villes'])]
    private bool $devast = false;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'carte', 'villes'])]
    private int $water = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'carte', 'villes'])]
    private int $saison = 0;
    
    #[ORM\Column(type: 'datetime'), Groups(['admin', 'admin_gen', 'general', 'villes'])]
    private DateTime $dateTime;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chaman_id', referencedColumnName: 'id', nullable: true), Groups(['admin', 'citoyens', 'general'])]
    private ?User $chaman = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'guide_id', referencedColumnName: 'id', nullable: true), Groups(['admin', 'citoyens', 'general'])]
    private ?User $guide = null;
    
    /** @var Collection<Banque> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Banque::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $banque;
    
    /** @var Collection<Chantiers> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Chantiers::class,
        cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'chantier_id'), Groups(['plan_construit'])]
    private Collection $chantiers;
    
    /** @var Collection<Citoyens> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Citoyens::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'citoyen_id')]
    private Collection $citoyens;
    
    /** @var Collection<Defense> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Defense::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private Collection $defense;
    
    /** @var Collection<Estimation> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Estimation::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private Collection $estimation;
    
    /** @var Collection<ExpeHordes> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: ExpeHordes::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $expeHordes;
    
    /** @var Collection<Journal> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Journal::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private Collection $journal;
    
    /** @var Collection<UpChantier> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: UpChantier::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', indexBy: 'id')]
    private Collection $upChantier;
    
    /** @var Collection<ZoneMap> */
    #[ORM\OneToMany(mappedBy: 'ville', targetEntity: ZoneMap::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', indexBy: 'idPartiel')]
    private Collection $zone;
    
    /** @var Collection<PlansChantier> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: PlansChantier::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'chantier_id')]
    private Collection $plansChantiers;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['plan'])]
    private ?User $plansChantierUpdateBy = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['plan'])]
    private ?DateTime $plansChantierDateMaj = null;
    
    /** @var Collection<EstimationTdg> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: EstimationTdg::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $estimationsTdg;
    
    /** @var Collection<CalculAttaque> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: CalculAttaque::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $calculAttaques;
    
    /** @var Collection<Ruines> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Ruines::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $ruines;
    
    #[ORM\OneToOne(mappedBy: 'ville', targetEntity: Jump::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private ?Jump $jump = null;
    
    /** @var Collection<Outils> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: Outils::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $outils;
    
    /** @var Collection<TraceExpedition> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: TraceExpedition::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $expedition_manuel;
    
    /** @var Collection<StockVeille> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: StockVeille::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $stockVeilles;
    
    /** @var Collection<HistoriqueVeille> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: HistoriqueVeille::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $historiqueVeilles;
    
    /** @var Collection<AvancementChantier> */
    #[ORM\OneToMany(mappedBy: 'ville',
        targetEntity: AvancementChantier::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'chantier_id')]
    #[Groups(['chantier'])]
    private Collection $avancementChantiers;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['admin'])]
    private ?int $dayMajScrut = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['admin', 'general'])]
    private ?User $updateBy = null;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin'])]
    private ?bool $premiereMaj = false;
    
    #[ORM\Column(length: 20, nullable: true), Groups(['admin'])]
    private ?string $phase = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false]), Groups(['admin'])]
    private ?bool $majAllPicto = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false]), Groups(['admin'])]
    private ?bool $majAllOldTown = false;
    
    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0]), Groups(['admin'])]
    private ?int $countMaj = 0;
    
    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0]), Groups(['admin'])]
    private ?int $countMajScript = 0;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $maxPa = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $maxKm = null;
    
    /** @var Collection<StockExpedition> */
    #[ORM\OneToMany(mappedBy: 'ville', targetEntity: StockExpedition::class, orphanRemoval: true)]
    private Collection $stockExpedition;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $dayChaos = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $dayDevast = null;
    
    /**
     * @var Collection|Citoyens[][] $listCitoyenVie
     */
    private array $listCitoyenVie = [];
    
    /**
     * @var Collection|Citoyens[] $listCitoyenVille
     */
    private array $listCitoyenVille = [];
    
    #[Groups(['carte_gen'])]
    private ?int $maPosition = null;
    
    #[ORM\Column(length: 10, nullable: true), Groups(['admin', 'villes', 'general'])]
    private ?string $lang = null;
    
    public function __construct(
        #[ORM\Column(type: 'integer'), Groups(['admin', 'admin_gen', 'general', 'picto', 'plan', 'citoyens', 'ruine',
                                               'carte', 'villes', 'event_inscription'])]
        private int  $mapId,
        #[ORM\Column(type: 'smallint'), Groups(['admin', 'general', 'picto', 'plan', 'carte', 'villes'])]
        private ?int $origin,
    )
    {
        $this->id = $mapId * 10 + $origin;
        
        $this->banque              = new ArrayCollection();
        $this->chantiers           = new ArrayCollection();
        $this->citoyens            = new ArrayCollection();
        $this->defense             = new ArrayCollection();
        $this->estimation          = new ArrayCollection();
        $this->expeHordes          = new ArrayCollection();
        $this->journal             = new ArrayCollection();
        $this->upChantier          = new ArrayCollection();
        $this->zone                = new ArrayCollection();
        $this->plansChantiers      = new ArrayCollection();
        $this->estimationsTdg      = new ArrayCollection();
        $this->calculAttaques      = new ArrayCollection();
        $this->ruines              = new ArrayCollection();
        $this->outils              = new ArrayCollection();
        $this->expedition_manuel   = new ArrayCollection();
        $this->stockVeilles        = new ArrayCollection();
        $this->historiqueVeilles   = new ArrayCollection();
        $this->avancementChantiers = new ArrayCollection();
        $this->stockExpedition     = new ArrayCollection();
    }
    
    public function addAvancementChantier(AvancementChantier $avancementChantier): self
    {
        if (!$this->avancementChantiers->contains($avancementChantier)) {
            $this->avancementChantiers[$avancementChantier->getChantier()?->getId() ?? 0] = $avancementChantier;
            $avancementChantier->setVille($this);
        }
        
        return $this;
    }
    
    public function addBanque(Banque $banque): self
    {
        if (!$this->banque->contains($banque)) {
            $this->banque[] = $banque;
            $banque->setVille($this);
        }
        
        return $this;
    }
    
    public function addCalculAttaque(CalculAttaque $calculAttaque): self
    {
        if (!$this->calculAttaques->contains($calculAttaque)) {
            $this->calculAttaques[] = $calculAttaque;
            $calculAttaque->setVille($this);
        }
        
        return $this;
    }
    
    public function addChantier(Chantiers $chantier): self
    {
        if (!$this->chantiers->contains($chantier)) {
            $this->chantiers[$chantier->getChantier()->getId()] = $chantier;
            $chantier->setVille($this);
        }
        
        return $this;
    }
    
    public function addCitoyen(Citoyens $citoyen): self
    {
        if (!$this->citoyens->contains($citoyen)) {
            $this->citoyens[$citoyen->getCitoyen()->getId()] = $citoyen;
            $citoyen->setVille($this);
        }
        
        return $this;
    }
    
    public function addDefense(Defense $defense): self
    {
        if (!$this->defense->contains($defense)) {
            $this->defense[] = $defense;
            $defense->setVille($this);
        }
        
        return $this;
    }
    
    public function addEstimation(Estimation $estimation): self
    {
        if (!$this->estimation->contains($estimation)) {
            $this->estimation[] = $estimation;
            $estimation->setVille($this);
        }
        
        return $this;
    }
    
    public function addEstimationsTdg(EstimationTdg $estimationsTdg): self
    {
        if (!$this->estimationsTdg->contains($estimationsTdg)) {
            $this->estimationsTdg[] = $estimationsTdg;
            $estimationsTdg->setVille($this);
        }
        
        return $this;
    }
    
    public function addExpeHorde(ExpeHordes $expeHorde): self
    {
        if (!$this->expeHordes->contains($expeHorde)) {
            $this->expeHordes[] = $expeHorde;
            $expeHorde->setVille($this);
        }
        
        return $this;
    }
    
    public function addExpeditionManuel(TraceExpedition $expeditionManuel): self
    {
        if (!$this->expedition_manuel->contains($expeditionManuel)) {
            $this->expedition_manuel[] = $expeditionManuel;
            $expeditionManuel->setVille($this);
        }
        
        return $this;
    }
    
    public function addHistoriqueVeille(HistoriqueVeille $historiqueVeille): self
    {
        if (!$this->historiqueVeilles->contains($historiqueVeille)) {
            $this->historiqueVeilles[] = $historiqueVeille;
            $historiqueVeille->setVille($this);
        }
        
        return $this;
    }
    
    public function addJournal(Journal $journal): self
    {
        if (!$this->journal->contains($journal)) {
            $this->journal[] = $journal;
            $journal->setVille($this);
        }
        
        return $this;
    }
    
    public function addOutil(Outils $outil): self
    {
        if (!$this->outils->contains($outil)) {
            $this->outils[] = $outil;
            $outil->setVille($this);
        }
        
        return $this;
    }
    
    public function addPlansChantier(PlansChantier $plansChantier): self
    {
        if (!$this->plansChantiers->contains($plansChantier)) {
            $this->plansChantiers[$plansChantier->getChantier()?->getId() ?? 0] = $plansChantier;
            $plansChantier->setVille($this);
        }
        
        return $this;
    }
    
    public function addRuine(Ruines $ruine): self
    {
        if (!$this->ruines->contains($ruine)) {
            $this->ruines[] = $ruine;
            $ruine->setVille($this);
        }
        
        return $this;
    }
    
    public function addStockExpedition(StockExpedition $stockExpedition): static
    {
        if (!$this->stockExpedition->contains($stockExpedition)) {
            $this->stockExpedition->add($stockExpedition);
            $stockExpedition->setVille($this);
        }
        
        return $this;
    }
    
    public function addStockVeille(StockVeille $stockVeille): self
    {
        if (!$this->stockVeilles->contains($stockVeille)) {
            $this->stockVeilles[] = $stockVeille;
            $stockVeille->setVille($this);
        }
        
        return $this;
    }
    
    public function addUpChantier(UpChantier $upChantier): self
    {
        if (!$this->upChantier->contains($upChantier)) {
            $this->upChantier[$upChantier->getChantier()->getId()] = $upChantier;
            $upChantier->setVille($this);
        }
        
        return $this;
    }
    
    public function addZone(ZoneMap $zone): self
    {
        if (!$this->zone->contains($zone)) {
            $this->zone[$zone->getIdPartiel()] = $zone;
            $zone->setVille($this);
        }
        
        return $this;
    }
    
    public function devastLibelle(): string
    {
        return ($this->isDevast()) ? 'Ville dévastée' : (($this->isChaos()) ? 'Ville en chaos' : 'Ville normale');
    }
    
    public function getAvancementChantier(int $id): ?AvancementChantier
    {
        return $this->avancementChantiers->toArray()[$id] ?? null;
    }
    
    public function getAvancementChantiers(): Collection
    {
        return $this->avancementChantiers;
    }
    
    public function getBanque(): Collection
    {
        return $this->banque;
    }
    
    public function getBonus(): int
    {
        return $this->bonus;
    }
    
    public function setBonus(int $bonus): Ville
    {
        $this->bonus = $bonus;
        
        return $this;
    }
    
    public function getCalculAttaques(): Collection
    {
        return $this->calculAttaques;
    }
    
    public function getChaman(): ?User
    {
        return $this->chaman;
    }
    
    public function setChaman(?User $chaman): Ville
    {
        $this->chaman = $chaman;
        
        return $this;
    }
    
    public function getChantier(int $id): ?Chantiers
    {
        return $this->chantiers->toArray()[$idChantier] ?? null;
    }
    
    public function getChantiers(): Collection
    {
        return $this->chantiers;
    }
    
    public function getCitoyen(int $id): ?Citoyens
    {
        return $this->citoyens->toArray()[$id] ?? null;
    }
    
    /**
     * @return Collection<int, Citoyens>
     */
    public function getCitoyens(): Collection
    {
        return $this->citoyens;
    }
    
    public function getCountMaj(): ?int
    {
        return $this->countMaj;
    }
    
    public function setCountMaj(int $countMaj): static
    {
        $this->countMaj = $countMaj;
        
        return $this;
    }
    
    public function getCountMajScript(): ?int
    {
        return $this->countMajScript;
    }
    
    public function setCountMajScript(int $countMajScript): static
    {
        $this->countMajScript = $countMajScript;
        
        return $this;
    }
    
    public function getDateTime(): DateTime
    {
        return $this->dateTime;
    }
    
    public function setDateTime(DateTime $dateTime): Ville
    {
        $this->dateTime = $dateTime;
        
        return $this;
    }
    
    public function getDayChaos(): ?int
    {
        return $this->dayChaos;
    }
    
    public function setDayChaos(?int $dayChaos): static
    {
        $this->dayChaos = $dayChaos;
        
        return $this;
    }
    
    public function getDayDevast(): ?int
    {
        return $this->dayDevast;
    }
    
    public function setDayDevast(?int $dayDevast): static
    {
        $this->dayDevast = $dayDevast;
        
        return $this;
    }
    
    public function getDayMajScrut(): ?int
    {
        return $this->dayMajScrut;
    }
    
    public function setDayMajScrut(?int $dayMajScrut): self
    {
        $this->dayMajScrut = $dayMajScrut;
        
        return $this;
    }
    
    /**
     * @return Collection<Defense>
     */
    public function getDefense(): Collection
    {
        return $this->defense;
    }
    
    #[Groups(['villes'])]
    public function getDevastIcon(): string
    {
        return ($this->isDevast()) ? 'h_death' : (($this->isChaos()) ? 'status_terror' : 'h_smile');
    }
    
    public function getEstimation(): Collection
    {
        return $this->estimation;
    }
    
    public function getEstimationsTdg(): Collection
    {
        return $this->estimationsTdg;
    }
    
    public function getExpeHordes(): Collection
    {
        return $this->expeHordes;
    }
    
    public function getExpeditionManuel(): Collection
    {
        return $this->expedition_manuel;
    }
    
    public function getGuide(): ?User
    {
        return $this->guide;
    }
    
    public function setGuide(?User $guide): Ville
    {
        $this->guide = $guide;
        
        return $this;
    }
    
    public function getHeight(): int
    {
        return $this->height;
    }
    
    public function setHeight(int $height): Ville
    {
        $this->height = $height;
        
        return $this;
    }
    
    public function getHistoriqueVeilles(): Collection
    {
        return $this->historiqueVeilles;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJour(): int
    {
        return $this->jour;
    }
    
    public function setJour(int $jour): Ville
    {
        $this->jour = $jour;
        
        return $this;
    }
    
    /**
     * @return Collection<Journal>
     */
    public function getJournal(): Collection
    {
        return $this->journal;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(?Jump $jump): self
    {
        // unset the owning side of the relation if necessary
        if ($jump === null && $this->jump !== null) {
            $this->jump->setVille(null);
        }
        
        // set the owning side of the relation if necessary
        if ($jump !== null && $jump->getVille() !== $this) {
            $jump->setVille($this);
        }
        
        $this->jump = $jump;
        
        return $this;
    }
    
    public function getLang(): ?string
    {
        return $this->lang;
    }
    
    public function setLang(?string $lang): static
    {
        $this->lang = $lang;
        
        return $this;
    }
    
    /**
     * @return Citoyens[][]
     */
    #[Groups(['carte_gen'])]
    public function getListCitoyenVie(): array
    {
        return $this->listCitoyenVie;
    }
    
    /**
     * @param Citoyens[] $listCitoyenVie
     * @return Ville
     */
    public function setListCitoyenVie(array $listCitoyenVie): Ville
    {
        $this->listCitoyenVie = $listCitoyenVie;
        return $this;
    }
    
    /**
     * @return  Collection<Citoyens>|Citoyens[]
     */
    #[Groups(['carte_gen'])]
    public function getListCitoyenVille(): Collection|array
    {
        return $this->listCitoyenVille;
    }
    
    /**
     * @param Citoyens[] $listCitoyenVille
     * @return Ville
     */
    public function setListCitoyenVille(array $listCitoyenVille): Ville
    {
        $this->listCitoyenVille = $listCitoyenVille;
        return $this;
    }
    
    /**
     * @return int|null
     */
    #[Groups(['carte_gen'])]
    public function getMaPosition(): ?int
    {
        return $this->maPosition;
    }
    
    /**
     * @param int|null $maPositions
     * @return Ville
     */
    public function setMaPosition(?int $maPositions): Ville
    {
        $this->maPosition = $maPositions;
        return $this;
    }
    
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    public function getMaxKm(): ?int
    {
        return $this->maxKm;
    }
    
    public function setMaxKm(?int $maxKm): static
    {
        $this->maxKm = $maxKm;
        
        return $this;
    }
    
    public function getMaxPa(): ?int
    {
        return $this->maxPa;
    }
    
    public function setMaxPa(?int $maxPa): static
    {
        $this->maxPa = $maxPa;
        
        return $this;
    }
    
    /**
     * Fonction permettant de récupérer le nombre de citoyens héros dans la ville
     *
     * @return int
     */
    #[Groups(['general', 'villes'])]
    public function getNbHeros(): int
    {
        return $this->citoyens->filter(
            fn(Citoyens $c) => ($c->getJob()?->getId() ?? 0) > 2,
        )->count();
    }
    
    /**
     * Fonction permettant de récupérer le nombre de citoyens encore vivant dans la ville
     *
     * @return int
     */
    #[Groups(['general', 'villes'])]
    public function getNbVivants(): int
    {
        return $this->citoyens->filter(
            fn(Citoyens $c) => !$c->getMort(),
        )->count();
    }
    
    public function getNom(): string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): Ville
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    /**
     * Fonction permettant de calculer le nombre de points de saison de la ville
     *
     * @return int
     */
    #[Groups(['general', 'villes'])]
    public function getNombrePointSaison(): int
    {
        $tabMortByDay = [];
        
        foreach ($this->getCitoyens() as $citoyen) {
            if ($citoyen->getMort()) {
                if (isset($tabMortByDay[$citoyen->getDeathDay()])) {
                    $tabMortByDay[$citoyen->getDeathDay()]++;
                } else {
                    $tabMortByDay[$citoyen->getDeathDay()] = 1;
                }
            }
        }
        
        // calcul des points par jour
        $nbrMort         = 0;
        $nbrPoint        = 0;
        $nbrCitoyensBase = $this->getCitoyens()->count();
        for ($i = 1; $i <= $this->getJour(); $i++) {
            
            $nbrPoint += $nbrCitoyensBase - $nbrMort;
            $nbrMort  += $tabMortByDay[$i] ?? 0;
        }
        
        return $nbrPoint + $this->getBonus();
        
    }
    
    public function getOrigin(): ?int
    {
        return $this->origin;
    }
    
    public function getOutils(): Collection
    {
        return $this->outils;
    }
    
    public function getPhase(): ?string
    {
        return $this->phase;
    }
    
    public function setPhase(?string $phase): self
    {
        $this->phase = $phase;
        
        return $this;
    }
    
    public function getPlansChantier(int $idChantier): ?PlansChantier
    {
        return $this->plansChantiers->toArray()[$idChantier] ?? null;
    }
    
    public function getPlansChantierDateMaj(): ?DateTimeInterface
    {
        return $this->plansChantierDateMaj;
    }
    
    public function setPlansChantierDateMaj(?DateTimeInterface $plansChantierDateMaj): self
    {
        $this->plansChantierDateMaj = $plansChantierDateMaj;
        
        return $this;
    }
    
    public function getPlansChantierUpdateBy(): ?User
    {
        return $this->plansChantierUpdateBy;
    }
    
    public function setPlansChantierUpdateBy(?User $plansChantierUpdateBy): self
    {
        $this->plansChantierUpdateBy = $plansChantierUpdateBy;
        
        return $this;
    }
    
    public function getPlansChantiers(): Collection
    {
        return $this->plansChantiers;
    }
    
    public function getPosX(): int
    {
        return $this->posX;
    }
    
    public function setPosX(int $posX): Ville
    {
        $this->posX = $posX;
        
        return $this;
    }
    
    public function getPosY(): int
    {
        return $this->posY;
    }
    
    public function setPosY(int $posY): Ville
    {
        $this->posY = $posY;
        
        return $this;
    }
    
    public function getRuines(): Collection
    {
        return $this->ruines;
    }
    
    public function getSaison(): int
    {
        return $this->saison;
    }
    
    public function setSaison(int $saison): Ville
    {
        $this->saison = $saison;
        
        return $this;
    }
    
    public function getStockExpedition(): Collection
    {
        return $this->stockExpedition;
    }
    
    public function getStockVeilles(): Collection
    {
        return $this->stockVeilles;
    }
    
    #[Groups(['villes'])]
    public function getTypeIcon(): string
    {
        return ($this->isHard()) ? 'h_arma' : (($this->getWeight() > 20) ? 'h_ghost' : 'r_guide');
    }
    
    public function getUpChantier(): Collection
    {
        return $this->upChantier;
    }
    
    public function getUpChantierIdx(int $idChantier): UpChantier
    {
        return $this->upChantier->toArray()[$idChantier];
    }
    
    public function getUpdateBy(): ?User
    {
        return $this->updateBy;
    }
    
    public function setUpdateBy(?User $updateBy): self
    {
        $this->updateBy = $updateBy;
        
        return $this;
    }
    
    public function getWater(): int
    {
        return $this->water;
    }
    
    public function setWater(int $water): Ville
    {
        $this->water = $water;
        
        return $this;
    }
    
    public function getWeight(): int
    {
        return $this->weight;
    }
    
    public function setWeight(int $weight): Ville
    {
        $this->weight = $weight;
        
        return $this;
    }
    
    /** @return Collection<ZoneMap> */
    public function getZone(): Collection
    {
        return $this->zone;
    }
    
    /**
     * @return Collection|ZoneMap[]
     */
    #[Groups(['carte'])]
    public function getZones(): Collection|array
    {
        // On veut indexer les zones par idPartiel
        $zone = [];
        foreach ($this->zone as $z) {
            $zone[$z->getIdPartiel()] = $z;
        }
        return $zone;
    }
    
    public function incrementCountMaj(): static
    {
        $this->countMaj++;
        
        return $this;
    }
    
    public function incrementCountMajScript(): static
    {
        $this->countMajScript++;
        
        return $this;
    }
    
    public function isChaos(): bool
    {
        return $this->chaos;
    }
    
    public function setChaos(bool $chaos): Ville
    {
        $this->chaos = $chaos;
        
        return $this;
    }
    
    public function isDevast(): bool
    {
        return $this->devast;
    }
    
    public function setDevast(bool $devast): Ville
    {
        $this->devast = $devast;
        
        return $this;
    }
    
    public function isHard(): bool
    {
        return $this->hard;
    }
    
    public function setHard(bool $hard): Ville
    {
        $this->hard = $hard;
        
        return $this;
    }
    
    public function isMajAllOldTown(): ?bool
    {
        return $this->majAllOldTown;
    }
    
    public function isMajAllPicto(): ?bool
    {
        return $this->majAllPicto;
    }
    
    public function isPorte(): bool
    {
        return $this->porte;
    }
    
    public function setPorte(bool $porte): Ville
    {
        $this->porte = $porte;
        
        return $this;
    }
    
    public function isPremiereMaj(): ?bool
    {
        return $this->premiereMaj;
    }
    
    public function isPrived(): bool
    {
        return $this->prived;
    }
    
    public function setPrived(bool $prived): Ville
    {
        $this->prived = $prived;
        
        return $this;
    }
    
    public function removeAvancementChantier(AvancementChantier $avancementChantier): self
    {
        // set the owning side to null (unless already changed)
        if ($this->avancementChantiers->removeElement($avancementChantier) &&
            $avancementChantier->getVille() === $this) {
            $avancementChantier->setVille(null);
        }
        
        return $this;
    }
    
    public function removeBanque(Banque $banque): self
    {
        // set the owning side to null (unless already changed)
        if ($this->banque->removeElement($banque) && $banque->getVille() === $this) {
            $banque->setVille(null);
        }
        
        return $this;
    }
    
    public function removeCalculAttaque(CalculAttaque $calculAttaque): self
    {
        // set the owning side to null (unless already changed)
        if ($this->calculAttaques->removeElement($calculAttaque) && $calculAttaque->getVille() === $this) {
            $calculAttaque->setVille(null);
        }
        
        return $this;
    }
    
    public function removeChantier(Chantiers $chantier): self
    {
        // set the owning side to null (unless already changed)
        if ($this->chantiers->removeElement($chantier) && $chantier->getVille() === $this) {
            $chantier->setVille(null);
        }
        
        return $this;
    }
    
    public function removeCitoyen(Citoyens $citoyen): self
    {
        // set the owning side to null (unless already changed)
        if ($this->citoyens->removeElement($citoyen) && $citoyen->getVille() === $this) {
            $citoyen->setVille(null);
        }
        
        return $this;
    }
    
    public function removeDefense(Defense $defense): self
    {
        // set the owning side to null (unless already changed)
        if ($this->defense->removeElement($defense) && $defense->getVille() === $this) {
            $defense->setVille(null);
        }
        
        return $this;
    }
    
    public function removeEstimation(Estimation $estimation): self
    {
        // set the owning side to null (unless already changed)
        if ($this->estimation->removeElement($estimation) && $estimation->getVille() === $this) {
            $estimation->setVille(null);
        }
        
        return $this;
    }
    
    public function removeEstimationsTdg(EstimationTdg $estimationsTdg): self
    {
        // set the owning side to null (unless already changed)
        if ($this->estimationsTdg->removeElement($estimationsTdg) && $estimationsTdg->getVille() === $this) {
            $estimationsTdg->setVille(null);
        }
        
        return $this;
    }
    
    public function removeExpeHorde(ExpeHordes $expeHorde): self
    {
        // set the owning side to null (unless already changed)
        if ($this->expeHordes->removeElement($expeHorde) && $expeHorde->getVille() === $this) {
            $expeHorde->setVille(null);
        }
        
        return $this;
    }
    
    public function removeExpeditionManuel(TraceExpedition $expeditionManuel): self
    {
        // set the owning side to null (unless already changed)
        if ($this->expedition_manuel->removeElement($expeditionManuel) && $expeditionManuel->getVille() === $this) {
            $expeditionManuel->setVille(null);
        }
        
        return $this;
    }
    
    public function removeHistoriqueVeille(HistoriqueVeille $historiqueVeille): self
    {
        // set the owning side to null (unless already changed)
        if ($this->historiqueVeilles->removeElement($historiqueVeille) && $historiqueVeille->getVille() === $this) {
            $historiqueVeille->setVille(null);
        }
        
        return $this;
    }
    
    public function removeJournal(Journal $journal): self
    {
        // set the owning side to null (unless already changed)
        if ($this->journal->removeElement($journal) && $journal->getVille() === $this) {
            $journal->setVille(null);
        }
        
        return $this;
    }
    
    public function removeOutil(Outils $outil): self
    {
        // set the owning side to null (unless already changed)
        if ($this->outils->removeElement($outil) && $outil->getVille() === $this) {
            $outil->setVille(null);
        }
        
        return $this;
    }
    
    public function removePlansChantier(PlansChantier $plansChantier): self
    {
        // set the owning side to null (unless already changed)
        if ($this->plansChantiers->removeElement($plansChantier) && $plansChantier->getVille() === $this) {
            $plansChantier->setVille(null);
        }
        
        return $this;
    }
    
    public function removeRuine(Ruines $ruine): self
    {
        // set the owning side to null (unless already changed)
        if ($this->ruines->removeElement($ruine) && $ruine->getVille() === $this) {
            $ruine->setVille(null);
        }
        
        return $this;
    }
    
    public function removeStockExpedition(StockExpedition $stockExpedition): static
    {
        // set the owning side to null (unless already changed)
        if ($this->stockExpedition->removeElement($stockExpedition) && $stockExpedition->getVille() === $this) {
            $stockExpedition->setVille(null);
        }
        
        return $this;
    }
    
    public function removeStockVeille(StockVeille $stockVeille): self
    {
        // set the owning side to null (unless already changed)
        if ($this->stockVeilles->removeElement($stockVeille) && $stockVeille->getVille() === $this) {
            $stockVeille->setVille(null);
        }
        
        return $this;
    }
    
    public function removeUpChantier(UpChantier $upChantier): self
    {
        // set the owning side to null (unless already changed)
        if ($this->upChantier->removeElement($upChantier) && $upChantier->getVille() === $this) {
            $upChantier->setVille(null);
        }
        
        return $this;
    }
    
    public function removeZone(ZoneMap $zone): self
    {
        // set the owning side to null (unless already changed)
        if ($this->zone->removeElement($zone) && $zone->getVille() === $this) {
            $zone->setVille(null);
        }
        
        return $this;
    }
    
    public function setMajAllOldTown(?bool $majAllOldTown): self
    {
        $this->majAllOldTown = $majAllOldTown;
        
        return $this;
    }
    
    public function setMajAllPicto(?bool $majAllPicto): self
    {
        $this->majAllPicto = $majAllPicto;
        
        return $this;
    }
    
    public function setMapId(?int $mapId): self
    {
        $this->mapId = $mapId;
        
        return $this;
    }
    
    public function setOrigin(int $origin): self
    {
        $this->origin = $origin;
        
        return $this;
    }
    
    public function setPremiereMaj(bool $premiereMaj): self
    {
        $this->premiereMaj = $premiereMaj;
        
        return $this;
    }
    
    public function typeLibelle(): string
    {
        return ($this->isHard()) ? 'Pandémonium' :
            (($this->getWeight() > 20) ? 'Région éloignée' : 'Région non-éloignée');
    }
    
}
