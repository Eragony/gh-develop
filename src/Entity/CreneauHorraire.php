<?php

namespace App\Entity;

use App\Repository\CreneauHorraireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CreneauHorraireRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class CreneauHorraire
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['admin', 'option', 'coalition', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['admin', 'option', 'coalition', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?string $libelle = null;
    
    /**
     * @var int|null $typologie
     * 1 => "Jump/Coalition"
     * 2 => "Expedition"
     */
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 1]), Groups(['admin', 'option', 'coalition', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?int $typologie = 1;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): CreneauHorraire
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }
    
    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;
        
        return $this;
    }
    
    public function getTypologie(): ?int
    {
        return $this->typologie;
    }
    
    public function setTypologie(int $typologie): static
    {
        $this->typologie = $typologie;
        
        return $this;
    }
    
}
