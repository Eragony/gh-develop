<?php

namespace App\Entity;

use App\Repository\StockExpeditionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StockExpeditionRepository::class)]
class StockExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $nbrDispo = null;
    
    #[ORM\ManyToOne(inversedBy: 'stockExpedition')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ville $ville = null;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNbrDispo(): ?int
    {
        return $this->nbrDispo;
    }
    
    public function setNbrDispo(int $nbrDispo): self
    {
        $this->nbrDispo = $nbrDispo;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): static
    {
        $this->ville = $ville;
        
        return $this;
    }
    
}
