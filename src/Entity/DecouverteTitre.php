<?php

namespace App\Entity;

use App\Repository\DecouverteTitreRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DecouverteTitreRepository::class)]
class DecouverteTitre
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\ManyToOne(inversedBy: 'decouverteTitres')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PictoPrototype $picto = null;
    
    #[ORM\ManyToOne(cascade: ['persist', 'remove'], inversedBy: 'decouverteTitres')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PictoTitrePrototype $titre = null;
    
    #[ORM\ManyToOne(inversedBy: 'decouverteTitres')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Column]
    private ?DateTimeImmutable $decouvertAt = null;
    
    public function getDecouvertAt(): ?DateTimeImmutable
    {
        return $this->decouvertAt;
    }
    
    public function setDecouvertAt(DateTimeImmutable $decouvertAt): static
    {
        $this->decouvertAt = $decouvertAt;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getPicto(): ?PictoPrototype
    {
        return $this->picto;
    }
    
    public function setPicto(?PictoPrototype $picto): static
    {
        $this->picto = $picto;
        
        return $this;
    }
    
    public function getTitre(): ?PictoTitrePrototype
    {
        return $this->titre;
    }
    
    public function setTitre(?PictoTitrePrototype $titre): static
    {
        $this->titre = $titre;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
}
