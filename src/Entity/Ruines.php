<?php

namespace App\Entity;

use App\Doctrine\IdAlphaRuineGenerator;
use App\Repository\RuinesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RuinesRepository::class)]
class Ruines
{
    #[ORM\Id, ORM\GeneratedValue(strategy: 'CUSTOM'), ORM\CustomIdGenerator(class: IdAlphaRuineGenerator::class)]
    #[ORM\Column(type: 'string', length: 24), Groups(['ruine', 'admin_ruine', 'admin_gen'])]
    private ?string $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: BatPrototype::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'bat_id', referencedColumnName: 'id', nullable: false),
        Groups(['ruine', 'admin_ruine', 'admin_gen'])]
    private ?BatPrototype $Bat = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['ruine', 'admin_ruine', 'admin_gen'])]
    private ?int $x = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['ruine', 'admin_ruine', 'admin_gen'])]
    private ?int $y = null;
    
    /** @var Collection<RuinesPlans> */
    #[ORM\OneToMany(mappedBy: 'ruines', targetEntity: RuinesPlans::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true), Groups(['ruine', 'admin_ruine'])]
    private Collection $Plans;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'ruines'),
        Groups(['ruine', 'admin_gen'])]
    private ?Ville $ville = null;
    
    public function __construct()
    {
        $this->Plans = new ArrayCollection();
    }
    
    public function addPlan(RuinesPlans $plan): self
    {
        if (!$this->Plans->contains($plan)) {
            $this->Plans[] = $plan;
            $plan->setRuines($this);
        }
        
        return $this;
    }
    
    public function getBat(): ?BatPrototype
    {
        return $this->Bat;
    }
    
    public function setBat(?BatPrototype $Bat): self
    {
        $this->Bat = $Bat;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): Ruines
    {
        $this->id = $id;
        
        return $this;
    }
    
    #[Groups(['admin_gen'])]
    public function getNombrePlan(): int
    {
        return $this->getPlans()->count();
    }
    
    public function getPlans(): Collection
    {
        return $this->Plans;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(int $x): self
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(int $y): self
    {
        $this->y = $y;
        
        return $this;
    }
    
    public function removePlan(RuinesPlans $plan): self
    {
        if ($this->Plans->removeElement($plan)) {
            // set the owning side to null (unless already changed)
            if ($plan->getRuines() === $this) {
                $plan->setRuines(null);
            }
        }
        
        return $this;
    }
    
}
