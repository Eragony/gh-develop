<?php

namespace App\Entity;

use App\Repository\DispoExpeditionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DispoExpeditionRepository::class)]
class DispoExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?CreneauHorraire $creneau = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?TypeDispo $dispo = null;
    
    #[ORM\ManyToOne(inversedBy: 'dispo')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Expeditionnaire $expeditionnaire = null;
    
    #[ORM\ManyToOne(inversedBy: 'dispo')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Ouvriers $ouvriers = null;
    
    public function getCreneau(): ?CreneauHorraire
    {
        return $this->creneau;
    }
    
    public function setCreneau(?CreneauHorraire $creneau): static
    {
        $this->creneau = $creneau;
        
        return $this;
    }
    
    public function getDispo(): ?TypeDispo
    {
        return $this->dispo;
    }
    
    public function setDispo(?TypeDispo $dispo): static
    {
        $this->dispo = $dispo;
        
        return $this;
    }
    
    public function getExpeditionnaire(): ?Expeditionnaire
    {
        return $this->expeditionnaire;
    }
    
    public function setExpeditionnaire(?Expeditionnaire $expeditionnaire): static
    {
        $this->expeditionnaire = $expeditionnaire;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): DispoExpedition
    {
        $this->id = $id;
        return $this;
    }
    
    public function getOuvriers(): ?Ouvriers
    {
        return $this->ouvriers;
    }
    
    public function setOuvriers(?Ouvriers $ouvriers): static
    {
        $this->ouvriers = $ouvriers;
        
        return $this;
    }
}
