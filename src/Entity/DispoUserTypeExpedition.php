<?php

namespace App\Entity;

use App\Repository\DispoUserTypeExpeditionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DispoUserTypeExpeditionRepository::class)]
class DispoUserTypeExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'option', 'outils_expe'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 30), Groups(['admin', 'option', 'outils_expe'])]
    private ?string $nom = null;
    
    #[ORM\ManyToOne(inversedBy: 'dispoUserTypeExpeditions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    /** @var Collection<DispoCreneauUserTypeExpedition> */
    #[ORM\OneToMany(mappedBy: 'dispoUserTypeExpedition', targetEntity: DispoCreneauUserTypeExpedition::class, cascade: ['persist', 'remove'], orphanRemoval: true), Groups(['admin', 'option', 'outils_expe'])]
    private Collection $creneau;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['admin', 'option', 'expe'])]
    private ?int $priorite = 0;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'option', 'expe'])]
    private ?bool $favoris = false;
    
    public function __construct()
    {
        $this->creneau = new ArrayCollection();
    }
    
    public function addCreneau(DispoCreneauUserTypeExpedition $creneau): static
    {
        if (!$this->creneau->contains($creneau)) {
            $this->creneau->add($creneau);
            $creneau->setDispoUserTypeExpedition($this);
        }
        
        return $this;
    }
    
    public function getCreneau(): Collection
    {
        return $this->creneau;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): DispoUserTypeExpedition
    {
        $this->id = $id;
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getPriorite(): ?int
    {
        return $this->priorite;
    }
    
    public function setPriorite(int $priorite): static
    {
        $this->priorite = $priorite;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function isFavoris(): ?bool
    {
        return $this->favoris;
    }
    
    public function removeCreneau(DispoCreneauUserTypeExpedition $creneau): static
    {
        if ($this->creneau->removeElement($creneau)) {
            // set the owning side to null (unless already changed)
            if ($creneau->getDispoUserTypeExpedition() === $this) {
                $creneau->setDispoUserTypeExpedition(null);
            }
        }
        
        return $this;
    }
    
    public function setFavoris(bool $favoris): static
    {
        $this->favoris = $favoris;
        
        return $this;
    }
}
