<?php

namespace App\Entity;

use App\Repository\HomePrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Serializer\Attribute\Groups;

#[Index(fields: ["def"], name: "home_idx")]
#[ORM\Entity(repositoryClass: HomePrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class HomePrototype
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab', 'comparatif'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 25)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab', 'comparatif'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 10)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab', 'comparatif'])]
    private ?string $icon = null;
    
    #[ORM\Column(type: 'integer'), Groups(['outils_chantier', 'ency_hab'])]
    private ?int $def = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_chantier', 'ency_hab'])]
    private ?int $pa = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_chantier', 'ency_hab'])]
    private ?int $paUrba = null;
    
    /** @var Collection<RessourceHome> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'homes_ressources'), Groups(['outils_chantier', 'ency_hab'])]
    #[ORM\JoinColumn(name: 'home_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'ressource_id', referencedColumnName: 'id', unique: true)]
    #[ORM\ManyToMany(targetEntity: RessourceHome::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $ressources;
    
    /** @var Collection<RessourceHome> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'homes_ressources_urba'), Groups(['outils_chantier', 'ency_hab'])]
    #[ORM\JoinColumn(name: 'home_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'ressource_id', referencedColumnName: 'id', unique: true)]
    #[ORM\ManyToMany(targetEntity: RessourceHome::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $ressourcesUrba;
    
    public function __construct()
    {
        $this->ressources     = new ArrayCollection();
        $this->ressourcesUrba = new ArrayCollection();
    }
    
    public function addRessource(RessourceHome $ressource): self
    {
        if (!$this->ressources->contains($ressource)) {
            $this->ressources[] = $ressource;
        }
        
        return $this;
    }
    
    public function addRessourceUrba(RessourceHome $ressource): self
    {
        if (!$this->ressourcesUrba->contains($ressource)) {
            $this->ressourcesUrba[] = $ressource;
        }
        
        return $this;
    }
    
    public function getDef(): ?int
    {
        return $this->def;
    }
    
    public function setDef(int $def): self
    {
        $this->def = $def;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): HomePrototype
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): self
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getPaUrba(): ?int
    {
        return $this->paUrba;
    }
    
    public function setPaUrba(?int $paUrba): void
    {
        $this->paUrba = $paUrba;
    }
    
    public function getRessources(): Collection
    {
        return $this->ressources;
    }
    
    public function getRessourcesUrba(): Collection
    {
        return $this->ressourcesUrba;
    }
    
    public function removeRessource(RessourceHome $ressource): self
    {
        $this->ressources->removeElement($ressource);
        
        return $this;
    }
    
    public function removeRessourceUrba(RessourceHome $ressource): self
    {
        $this->ressourcesUrba->removeElement($ressource);
        
        return $this;
    }
    
}
