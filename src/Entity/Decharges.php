<?php

namespace App\Entity;

use App\Repository\DechargesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DechargesRepository::class)]
class Decharges
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['outils_decharge'])]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: OutilsDecharge::class, fetch: 'EXTRA_LAZY', inversedBy: 'decharges')]
    #[ORM\JoinColumn(name: 'outils_decharge_id', referencedColumnName: 'id', nullable: false)]
    private ?OutilsDecharge $outilsDecharge = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: RegroupementItemsDecharge::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'regroupement_items_id', referencedColumnName: 'id', nullable: false), Groups(['outils_decharge'])]
    private ?RegroupementItemsDecharge $regroupItems = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_decharge'])]
    private ?int $nbrEstime = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_decharge'])]
    private ?int $nbrUtilise = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_decharge'])]
    private ?int $defByItem = 0;
    
    public function getDefByItem(): ?int
    {
        return $this->defByItem;
    }
    
    public function setDefByItem(int $defByItem): self
    {
        $this->defByItem = $defByItem;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Decharges
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNbrEstime(): ?int
    {
        return $this->nbrEstime;
    }
    
    public function setNbrEstime(int $nbrEstime): self
    {
        $this->nbrEstime = $nbrEstime;
        
        return $this;
    }
    
    public function getNbrUtilise(): ?int
    {
        return $this->nbrUtilise;
    }
    
    public function setNbrUtilise(int $nbrUtilise): self
    {
        $this->nbrUtilise = $nbrUtilise;
        
        return $this;
    }
    
    public function getOutilsDecharge(): ?OutilsDecharge
    {
        return $this->outilsDecharge;
    }
    
    public function setOutilsDecharge(?OutilsDecharge $outilsDecharge): self
    {
        $this->outilsDecharge = $outilsDecharge;
        
        return $this;
    }
    
    public function getRegroupItems(): ?RegroupementItemsDecharge
    {
        return $this->regroupItems;
    }
    
    public function setRegroupItems(?RegroupementItemsDecharge $regroupItems): self
    {
        $this->regroupItems = $regroupItems;
        
        return $this;
    }
}
