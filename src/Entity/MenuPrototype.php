<?php

namespace App\Entity;

use App\Repository\MenuPrototypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: MenuPrototypeRepository::class)]
class MenuPrototype
{
    #[ORM\Id]
    #[ORM\Column, Groups(['admin', 'general', 'option'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 40), Groups(['admin', 'general', 'option'])]
    private ?string $label = null;
    
    #[ORM\Column(length: 30, nullable: true), Groups(['admin', 'general', 'option'])]
    private ?string $category = null;
    
    #[ORM\Column, Groups(['admin', 'option'])]
    private ?bool $habUser = false;
    
    #[ORM\Column, Groups(['admin', 'option'])]
    private ?bool $habBeta = false;
    
    #[ORM\Column, Groups(['admin', 'option'])]
    private ?bool $habAdmin = false;
    
    #[ORM\Column, Groups(['admin', 'option'])]
    private ?bool $connected = false;
    
    #[ORM\Column, Groups(['admin', 'general'])]
    private ?bool $ville = false;
    
    #[ORM\Column(length: 20, nullable: true), Groups(['admin', 'general', 'option'])]
    private ?string $icone = null;
    
    #[ORM\Column(length: 255), Groups(['admin', 'general', 'option'])]
    private ?string $route = null;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin', 'general', 'option'])]
    private ?bool $myVille = false;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin', 'general', 'option'])]
    private ?bool $forUser = false;
    
    public function getCategory(): ?string
    {
        return $this->category;
    }
    
    public function setCategory(?string $category): static
    {
        $this->category = $category;
        
        return $this;
    }
    
    public function getIcone(): ?string
    {
        return $this->icone;
    }
    
    public function setIcone(?string $icone): static
    {
        $this->icone = $icone;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }
    
    public function setLabel(string $label): static
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getRoute(): ?string
    {
        return $this->route;
    }
    
    public function setRoute(string $route): static
    {
        $this->route = $route;
        
        return $this;
    }
    
    public function isConnected(): ?bool
    {
        return $this->connected;
    }
    
    public function isForUser(): ?bool
    {
        return $this->forUser;
    }
    
    public function isHabAdmin(): ?bool
    {
        return $this->habAdmin;
    }
    
    public function isHabBeta(): ?bool
    {
        return $this->habBeta;
    }
    
    public function isHabUser(): ?bool
    {
        return $this->habUser;
    }
    
    public function isMyVille(): ?bool
    {
        return $this->myVille;
    }
    
    public function isVille(): ?bool
    {
        return $this->ville;
    }
    
    public function setConnected(bool $connected): static
    {
        $this->connected = $connected;
        
        return $this;
    }
    
    public function setForUser(bool $forUser): static
    {
        $this->forUser = $forUser;
        
        return $this;
    }
    
    public function setHabAdmin(bool $habAdmin): static
    {
        $this->habAdmin = $habAdmin;
        
        return $this;
    }
    
    public function setHabBeta(bool $habBeta): static
    {
        $this->habBeta = $habBeta;
        
        return $this;
    }
    
    public function setHabUser(bool $habUser): static
    {
        $this->habUser = $habUser;
        
        return $this;
    }
    
    public function setMyVille(bool $myVille): static
    {
        $this->myVille = $myVille;
        
        return $this;
    }
    
    public function setVille(bool $ville): static
    {
        $this->ville = $ville;
        
        return $this;
    }
}
