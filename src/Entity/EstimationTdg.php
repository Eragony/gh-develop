<?php

namespace App\Entity;

use App\Repository\EstimationTdgRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: EstimationTdgRepository::class)]
class EstimationTdg
{
    public const TYPE_ESTIM_TDG    = 1;
    public const TYPE_ESTIM_PLANIF = 2;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'estimationsTdg')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['tdg'])]
    private ?int $day = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['tdg'])]
    private ?int $typeEstim = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['tdg'])]
    private ?int $valPourcentage = null;
    
    #[ORM\Column(type: 'integer'), Groups(['tdg'])]
    private ?int $minEstim = null;
    
    #[ORM\Column(type: 'integer'), Groups(['tdg'])]
    private ?int $maxEstim = null;
    
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): EstimationTdg
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMaxEstim(): ?int
    {
        return $this->maxEstim;
    }
    
    public function setMaxEstim(int $maxEstim): self
    {
        $this->maxEstim = $maxEstim;
        
        return $this;
    }
    
    public function getMinEstim(): ?int
    {
        return $this->minEstim;
    }
    
    public function setMinEstim(int $minEstim): self
    {
        $this->minEstim = $minEstim;
        
        return $this;
    }
    
    public function getTypeEstim(): ?int
    {
        return $this->typeEstim;
    }
    
    public function setTypeEstim(int $typeEstim): self
    {
        $this->typeEstim = $typeEstim;
        
        return $this;
    }
    
    public function getValPourcentage(): ?int
    {
        return $this->valPourcentage;
    }
    
    public function setValPourcentage(int $valPourcentage): self
    {
        $this->valPourcentage = $valPourcentage;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
}
