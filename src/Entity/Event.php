<?php

namespace App\Entity;

use App\Doctrine\IdAlphaEventGenerator;
use App\Repository\EventRepository;
use App\Structures\Collection\GestionJumps;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaEventGenerator::class)]
    #[ORM\Column(type: 'string', length: 24),
        Groups(['list_event', 'event_inscription', 'gestion_event', 'gestion_jump'])]
    private ?string $id = null;
    
    #[ORM\Column(type: 'string', length: 255),
        Groups(['list_event', 'creation_event', 'event_inscription', 'gestion_event', 'gestion_jump'])]
    private ?string $nom = '';
    
    #[ORM\Column(type: 'datetime'), Groups(['list_event', 'creation_event', 'gestion_event'])]
    private ?DateTimeInterface $debInscriptionDateAt = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['list_event', 'creation_event', 'event_inscription', 'gestion_event'])]
    private ?DateTimeInterface $finInscriptionDateAt = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['list_event', 'creation_event', 'gestion_event'])]
    private ?DateTimeInterface $eventBeginAt = null;
    
    /** @var Collection<User> */
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'orgaEvents', fetch: 'EXTRA_LAZY', indexBy: 'id'),
        Groups(['list_event', 'gestion_event', 'inscription_jump'])]
    private Collection $organisateurs;
    
    /** @var Collection<Jump> */
    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Jump::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true),
        Groups(['creation_event', 'event_inscription', 'gestion_event'])]
    private Collection $listJump;
    
    #[ORM\Column(type: 'boolean'), Groups(['creation_event', 'event_inscription', 'gestion_event', 'inscription_jump'])]
    private ?bool $oneMetier = false;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $eventEndAt = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['creation_event', 'gestion_event'])]
    private ?bool $eventPrived = false;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeVille::class, fetch: 'EXTRA_LAZY'),
        Groups(['list_event', 'creation_event', 'gestion_event'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeVille $typeVille = null;
    
    #[ORM\Column(type: 'text', nullable: true), Groups(['list_event', 'creation_event', 'gestion_event'])]
    private ?string $description = '';
    
    #[ORM\Column(type: 'string', length: 255, nullable: true),
        Groups(['creation_event', 'event_inscription', 'gestion_event'])]
    #[Assert\Length(min: 2, max: 255, minMessage: 'Votre lien pour la bannière doit faire au moins {{ limit }} caractères de long', maxMessage: 'Votre lien ne peux pas excéder plus de  {{ limit }} caractères')]
    private ?string $banniere = null;
    
    /** @var Collection<LogEventEvent> */
    #[ORM\OneToMany(mappedBy: 'event', targetEntity: LogEventEvent::class, cascade: ['persist', 'remove'], orphanRemoval: true),
        Groups(['gestion_event'])]
    private Collection $logEvent;
    
    #[ORM\Column(length: 3, nullable: true), Groups(['list_event', 'creation_event', 'gestion_event'])]
    private ?string $community = null;
    
    public function __construct()
    {
        $this->organisateurs = new ArrayCollection();
        $this->listJump      = new ArrayCollection();
        $this->logEvent      = new ArrayCollection();
    }
    
    public function addListJump(Jump $listJump): self
    {
        if (!$this->listJump->contains($listJump)) {
            $this->listJump[] = $listJump;
            $listJump->setEvent($this);
        }
        
        return $this;
    }
    
    public function addLogEvent(LogEventEvent $logEvent): static
    {
        if (!$this->logEvent->contains($logEvent)) {
            $this->logEvent->add($logEvent);
            $logEvent->setEvent($this);
        }
        
        return $this;
    }
    
    public function addOrganisateur(User $orga): self
    {
        if (!$this->organisateurs->contains($orga)) {
            $this->organisateurs[$orga->getId()] = $orga;
        }
        
        return $this;
    }
    
    /**
     * @throws Exception
     */
    public function arrayListOrga(): array
    {
        $gestionJumps = new GestionJumps();
        
        $gestionJumps->setGestionJumps($this->getOrganisateurs());
        
        try {
            $gestionJumps = $gestionJumps->triByPseudo();
            
            return array_map(fn(User $gest) => $gest->getPseudo(), $gestionJumps->toArray());
        } catch (Exception) {
            throw new Exception("Erreur lors du tri");
        }
        
        
    }
    
    public function getBanniere(): ?string
    {
        return $this->banniere;
    }
    
    public function setBanniere(?string $banniere): self
    {
        $this->banniere = $banniere;
        
        return $this;
    }
    
    public function getCommunity(): ?string
    {
        return $this->community;
    }
    
    public function setCommunity(?string $community): static
    {
        $this->community = $community;
        
        return $this;
    }
    
    public function getDebInscriptionDateAt(): ?DateTimeInterface
    {
        return $this->debInscriptionDateAt;
    }
    
    public function setDebInscriptionDateAt(?DateTimeInterface $debInscriptionDateAt): self
    {
        $this->debInscriptionDateAt = $debInscriptionDateAt;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getEventBeginAt(): ?DateTimeInterface
    {
        return $this->eventBeginAt;
    }
    
    public function setEventBeginAt(DateTimeInterface $eventBeginAt): self
    {
        $this->eventBeginAt = $eventBeginAt;
        
        return $this;
    }
    
    public function getEventEndAt(): ?DateTimeInterface
    {
        return $this->eventEndAt;
    }
    
    public function setEventEndAt(?DateTimeInterface $eventEndAt): self
    {
        $this->eventEndAt = $eventEndAt;
        
        return $this;
    }
    
    public function getEventPrived(): ?bool
    {
        return $this->eventPrived;
    }
    
    public function setEventPrived(bool $eventPrived): self
    {
        $this->eventPrived = $eventPrived;
        
        return $this;
    }
    
    public function getFinInscriptionDateAt(): ?DateTimeInterface
    {
        return $this->finInscriptionDateAt;
    }
    
    public function setFinInscriptionDateAt(DateTimeInterface $finInscriptionDateAt): self
    {
        $this->finInscriptionDateAt = $finInscriptionDateAt;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): Event
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return Collection<Jump>
     */
    public function getListJump(): Collection
    {
        return $this->listJump;
    }
    
    public function getLogEvent(): Collection
    {
        return $this->logEvent;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getOneMetier(): ?bool
    {
        return $this->oneMetier;
    }
    
    public function setOneMetier(bool $oneMetier): self
    {
        $this->oneMetier = $oneMetier;
        
        return $this;
    }
    
    public function getOrganisateur(int $userId): User|null
    {
        return $this->organisateurs->toArray()[$userId];
    }
    
    public function getOrganisateurs(): Collection
    {
        return $this->organisateurs;
    }
    
    public function getTypeVille(): ?TypeVille
    {
        return $this->typeVille;
    }
    
    public function setTypeVille(?TypeVille $typeVille): self
    {
        $this->typeVille = $typeVille;
        
        return $this;
    }
    
    public function removeListJump(Jump $listJump): self
    {
        if ($this->listJump->removeElement($listJump)) {
            // set the owning side to null (unless already changed)
            if ($listJump->getEvent() === $this) {
                $listJump->setEvent(null);
            }
        }
        
        return $this;
    }
    
    public function removeLogEvent(LogEventEvent $logEvent): static
    {
        if ($this->logEvent->removeElement($logEvent)) {
            // set the owning side to null (unless already changed)
            if ($logEvent->getEvent() === $this) {
                $logEvent->setEvent(null);
            }
        }
        
        return $this;
    }
    
    public function removeOrganisateur(User $organisateur): self
    {
        $this->organisateurs->removeElement($organisateur);
        
        return $this;
    }
    
}
