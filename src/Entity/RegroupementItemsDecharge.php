<?php

namespace App\Entity;

use App\Repository\RegroupementItemsDechargeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RegroupementItemsDechargeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class RegroupementItemsDecharge
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['outils_decharge'])]
    private ?int $id = null;
    
    /** @var Collection<ItemPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'regroupement_item_affiche')]
    #[ORM\JoinColumn(name: 'regroupement_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'item_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[Groups(['outils_decharge'])]
    private Collection $itemAffiches;
    
    /** @var Collection<ItemPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'regroupement_item_regrp')]
    #[ORM\JoinColumn(name: 'regroupement_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'item_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[Groups(['outils_decharge'])]
    private Collection $regroupItems;
    
    #[ORM\Column(type: 'string', length: 30)]
    #[Groups(['outils_decharge'])]
    private ?string $nom = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['outils_decharge'])]
    private ?ChantierPrototype $chantierDecharge = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_decharge'])]
    private ?int $pointDefBase = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_decharge'])]
    private ?int $pointDefDecharge = null;
    
    public function __construct()
    {
        $this->itemAffiches = new ArrayCollection();
        $this->regroupItems = new ArrayCollection();
    }
    
    public function addItemAffich(ItemPrototype $itemAffich): self
    {
        if (!$this->itemAffiches->contains($itemAffich)) {
            $this->itemAffiches[] = $itemAffich;
        }
        
        return $this;
    }
    
    public function addRegroupItem(ItemPrototype $regroupItem): self
    {
        if (!$this->regroupItems->contains($regroupItem)) {
            $this->regroupItems[] = $regroupItem;
        }
        
        return $this;
    }
    
    public function getChantierDecharge(): ?ChantierPrototype
    {
        return $this->chantierDecharge;
    }
    
    public function setChantierDecharge(?ChantierPrototype $chantierDecharge): self
    {
        $this->chantierDecharge = $chantierDecharge;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): RegroupementItemsDecharge
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItemAffiches(): Collection
    {
        return $this->itemAffiches;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getPointDefBase(): ?int
    {
        return $this->pointDefBase;
    }
    
    public function setPointDefBase(int $pointDefBase): self
    {
        $this->pointDefBase = $pointDefBase;
        
        return $this;
    }
    
    public function getPointDefDecharge(): ?int
    {
        return $this->pointDefDecharge;
    }
    
    public function setPointDefDecharge(int $pointDefDecharge): self
    {
        $this->pointDefDecharge = $pointDefDecharge;
        
        return $this;
    }
    
    /**
     * @return Collection<int, ItemPrototype>
     */
    public function getRegroupItems(): Collection
    {
        return $this->regroupItems;
    }
    
    public function removeItemAffich(ItemPrototype $itemAffich): self
    {
        $this->itemAffiches->removeElement($itemAffich);
        
        return $this;
    }
    
    public function removeRegroupItem(ItemPrototype $regroupItem): self
    {
        $this->regroupItems->removeElement($regroupItem);
        
        return $this;
    }
}
