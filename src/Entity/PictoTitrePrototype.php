<?php

namespace App\Entity;

use App\Doctrine\IdPictoTitrePrototypeGenerator;
use App\Repository\PictoTitrePrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: PictoTitrePrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class PictoTitrePrototype
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdPictoTitrePrototypeGenerator::class)]
    #[Groups(['admin_picto', 'picto'])]
    private ?int $id;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['admin_picto', 'picto'])]
    private ?int $nbr;
    
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['admin_picto', 'picto'])]
    private ?string $titre = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: PictoPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'titre')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PictoPrototype $pictoPrototype;
    
    /**
     * @var Collection<int, DecouverteTitre>
     */
    #[ORM\OneToMany(mappedBy: 'titre', targetEntity: DecouverteTitre::class, orphanRemoval: true)]
    private Collection $decouverteTitres;
    
    public function __construct()
    {
        $this->decouverteTitres = new ArrayCollection();
    }
    
    public function addDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if (!$this->decouverteTitres->contains($decouverteTitre)) {
            $this->decouverteTitres->add($decouverteTitre);
            $decouverteTitre->setTitre($this);
        }
        
        return $this;
    }
    
    /**
     * @return Collection<int, DecouverteTitre>
     */
    public function getDecouverteTitres(): Collection
    {
        return $this->decouverteTitres;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): PictoTitrePrototype
    {
        $this->id = $id;
        return $this;
    }
    
    public function getNbr(): ?int
    {
        return $this->nbr;
    }
    
    public function setNbr(?int $nbr): self
    {
        $this->nbr = $nbr;
        
        return $this;
    }
    
    public function getPictoPrototype(): ?PictoPrototype
    {
        return $this->pictoPrototype;
    }
    
    public function setPictoPrototype(?PictoPrototype $pictoPrototype): self
    {
        $this->pictoPrototype = $pictoPrototype;
        
        return $this;
    }
    
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;
        
        return $this;
    }
    
    public function removeDecouverteTitre(DecouverteTitre $decouverteTitre): static
    {
        if ($this->decouverteTitres->removeElement($decouverteTitre)) {
            // set the owning side to null (unless already changed)
            if ($decouverteTitre->getTitre() === $this) {
                $decouverteTitre->setTitre(null);
            }
        }
        
        return $this;
    }
}
