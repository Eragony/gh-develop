<?php

namespace App\Entity;

use App\Repository\ExpeditionTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ExpeditionTypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ExpeditionType
{
    public const TYPE_RAMASSAGE = 6;
    
    #[ORM\Id]
    #[ORM\Column, Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 30), Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $nom = null;
    
    #[ORM\Column(length: 10), Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $icon = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): static
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
}
