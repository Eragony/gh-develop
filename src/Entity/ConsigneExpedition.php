<?php

namespace App\Entity;

use App\Repository\ConsigneExpeditionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ConsigneExpeditionRepository::class)]
class ConsigneExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(inversedBy: 'consigneExpeditions')]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?ZoneMap $zone = null;
    
    #[ORM\Column(length: 2000, nullable: true)]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?string $text = null;
    
    #[ORM\Column]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?bool $fait = null;
    
    #[ORM\ManyToOne(inversedBy: 'consignes')]
    #[ORM\JoinColumn(nullable: false), Groups(['carte'])]
    private ?ExpeditionPart $expeditionPart = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?int $ordreConsigne = null;
    
    public function getExpeditionPart(): ?ExpeditionPart
    {
        return $this->expeditionPart;
    }
    
    public function setExpeditionPart(?ExpeditionPart $expeditionPart): static
    {
        $this->expeditionPart = $expeditionPart;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ConsigneExpedition
    {
        $this->id = $id;
        return $this;
    }
    
    public function getOrdreConsigne(): ?int
    {
        return $this->ordreConsigne;
    }
    
    public function setOrdreConsigne(int $ordreConsigne): static
    {
        $this->ordreConsigne = $ordreConsigne;
        
        return $this;
    }
    
    public function getText(): ?string
    {
        return $this->text;
    }
    
    public function setText(?string $text): static
    {
        $this->text = $text;
        
        return $this;
    }
    
    public function getZone(): ?ZoneMap
    {
        return $this->zone;
    }
    
    public function setZone(?ZoneMap $zone): static
    {
        $this->zone = $zone;
        
        return $this;
    }
    
    public function isFait(): ?bool
    {
        return $this->fait;
    }
    
    public function setFait(bool $fait): static
    {
        $this->fait = $fait;
        
        return $this;
    }
}
