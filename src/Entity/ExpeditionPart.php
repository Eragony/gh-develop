<?php

namespace App\Entity;

use App\Repository\ExpeditionPartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ExpeditionPartRepository::class)]
class ExpeditionPart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $number = null;
    
    #[ORM\OneToOne(inversedBy: 'expeditionPart', cascade: ['persist', 'remove'])]
    #[Groups(['outils_expe', 'carte', 'inscription_exp_visu'])]
    private ?TraceExpedition $trace = null;
    
    #[ORM\Column]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?bool $ouverte = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $pa = null;
    
    /** @var Collection<ConsigneExpedition> */
    #[ORM\OneToMany(mappedBy: 'expeditionPart', targetEntity: ConsigneExpedition::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private Collection $consignes;
    
    #[ORM\ManyToOne(inversedBy: 'expeditionParts')]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Expedition $expedition = null;
    
    /** @var Collection<Expeditionnaire> */
    #[ORM\OneToMany(mappedBy: 'expeditionPart', targetEntity: Expeditionnaire::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private Collection $expeditionnaires;
    
    #[ORM\Column(length: 2000, nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $description = null;
    
    public function __construct()
    {
        $this->consignes        = new ArrayCollection();
        $this->expeditionnaires = new ArrayCollection();
    }
    
    public function addConsigne(ConsigneExpedition $consigne): static
    {
        if (!$this->consignes->contains($consigne)) {
            $this->consignes->add($consigne);
            $consigne->setExpeditionPart($this);
        }
        
        return $this;
    }
    
    public function addExpeditionnaire(Expeditionnaire $expeditionnaire): static
    {
        if (!$this->expeditionnaires->contains($expeditionnaire)) {
            $this->expeditionnaires->add($expeditionnaire);
            $expeditionnaire->setExpeditionPart($this);
        }
        
        return $this;
    }
    
    public function getConsignes(): Collection
    {
        return $this->consignes;
    }
    
    public function setConsignes(Collection $consignes): ExpeditionPart
    {
        $this->consignes = $consignes;
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): static
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getExpedition(): ?Expedition
    {
        return $this->expedition;
    }
    
    public function setExpedition(?Expedition $expedition): static
    {
        $this->expedition = $expedition;
        
        return $this;
    }
    
    public function getExpeditionnaire(int $index): Expeditionnaire
    {
        return $this->expeditionnaires[$index];
    }
    
    public function getExpeditionnairePosition(int $position): Expeditionnaire
    {
        foreach ($this->expeditionnaires as $expeditionnaire) {
            if ($expeditionnaire->getPosition() === $position) {
                return $expeditionnaire;
            }
        }
        return new Expeditionnaire();
    }
    
    /** @return Collection<int, Expeditionnaire> */
    public function getExpeditionnaires(): Collection
    {
        return $this->expeditionnaires;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ExpeditionPart
    {
        $this->id = $id;
        return $this;
    }
    
    public function getNumber(): ?int
    {
        return $this->number;
    }
    
    public function setNumber(int $number): static
    {
        $this->number = $number;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): static
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getTrace(): ?TraceExpedition
    {
        return $this->trace;
    }
    
    public function setTrace(?TraceExpedition $trace): static
    {
        $this->trace = $trace;
        
        return $this;
    }
    
    public function isOuverte(): ?bool
    {
        return $this->ouverte;
    }
    
    public function removeConsigne(ConsigneExpedition $consigne): static
    {
        if ($this->consignes->removeElement($consigne)) {
            // set the owning side to null (unless already changed)
            if ($consigne->getExpeditionPart() === $this) {
                $consigne->setExpeditionPart(null);
            }
        }
        
        return $this;
    }
    
    public function removeExpeditionnaire(Expeditionnaire $expeditionnaire): static
    {
        if ($this->expeditionnaires->removeElement($expeditionnaire)) {
            // set the owning side to null (unless already changed)
            if ($expeditionnaire->getExpeditionPart() === $this) {
                $expeditionnaire->setExpeditionPart(null);
            }
        }
        
        return $this;
    }
    
    public function setOuverte(bool $ouverte): static
    {
        $this->ouverte = $ouverte;
        
        return $this;
    }
}
