<?php

namespace App\Entity;

use App\Repository\UpHabitationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UpHabitationRepository::class)]
class UpHabitation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['outils_chantier'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_chantier'])]
    private ?User $citoyen = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: HomePrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_chantier'])]
    private ?HomePrototype $lvlHabitation = null;
    
    #[ORM\ManyToOne(targetEntity: OutilsChantier::class, fetch: 'EXTRA_LAZY', inversedBy: 'upHabitations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OutilsChantier $outilsChantier = null;
    
    #[ORM\Column(length: 50), Groups(['outils_chantier'])]
    private ?string $uuid = null;
    
    public function getCitoyen(): ?User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): UpHabitation
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLvlHabitation(): ?HomePrototype
    {
        return $this->lvlHabitation;
    }
    
    public function setLvlHabitation(?HomePrototype $lvlHabitation): self
    {
        $this->lvlHabitation = $lvlHabitation;
        
        return $this;
    }
    
    public function getOutilsChantier(): ?OutilsChantier
    {
        return $this->outilsChantier;
    }
    
    public function setOutilsChantier(?OutilsChantier $outilsChantier): self
    {
        $this->outilsChantier = $outilsChantier;
        
        return $this;
    }
    
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
    
    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;
        
        return $this;
    }
}
