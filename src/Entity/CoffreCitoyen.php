<?php

namespace App\Entity;

use App\Repository\CoffreCitoyenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CoffreCitoyenRepository::class)]
class CoffreCitoyen
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\Column(name: 'coffre_id', type: 'integer')]
    private ?int $idObjet;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['citoyens'])]
    private ?ItemPrototype $item;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['citoyens'])]
    private ?bool $broken;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['citoyens'])]
    private ?int $nombre = null;
    
    /** @var Collection<Citoyens> */
    #[ORM\JoinTable(name: 'citoyens_coffre')]
    #[ORM\JoinColumn(name: 'coffre_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'citoyen_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: Citoyens::class, inversedBy: 'coffres', fetch: 'EXTRA_LAZY')]
    private Collection $citoyen;
    
    public function __construct(ItemPrototype $item, bool $broken)
    {
        $this->idObjet = $item->getId() * 10 + (($broken) ? 1 : 0);
        $this->item    = $item;
        $this->broken  = $broken;
        $this->citoyen = new ArrayCollection();
        
    }
    
    public function addCitoyen(Citoyens $citoyen): self
    {
        if (!$this->citoyen->contains($citoyen)) {
            $this->citoyen[] = $citoyen;
            $citoyen->addCoffre($this);
        }
        
        return $this;
    }
    
    public function getBroken(): ?bool
    {
        return $this->broken;
    }
    
    public function setBroken(bool $broken): self
    {
        $this->broken = $broken;
        
        return $this;
    }
    
    public function getCitoyen(): Collection
    {
        return $this->citoyen;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdObjet(): ?int
    {
        return $this->idObjet;
    }
    
    public function setIdObjet(?int $idObjet): CoffreCitoyen
    {
        $this->idObjet = $idObjet;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function removeCitoyen(Citoyens $citoyen): self
    {
        if ($this->citoyen->removeElement($citoyen)) {
            $citoyen->removeCoffre($this);
        }
        
        return $this;
    }
    
    
}
