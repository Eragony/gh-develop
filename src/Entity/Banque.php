<?php

namespace App\Entity;

use App\Repository\BanqueRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'banque')]
#[ORM\Index(columns: ['item_id', 'broked'], name: 'items_idx')]
#[ORM\Entity(repositoryClass: BanqueRepository::class)]
class Banque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'banque')]
    private ?Ville $ville = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['banque','comparatif'])]
    private ItemPrototype $item;
    
    #[ORM\Column(type: Types::BOOLEAN)]
    #[Groups(['banque','comparatif'])]
    private ?bool $broked = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['banque','comparatif'])]
    private ?int $nombre = null;
    
    public function getBroked(): bool
    {
        return $this->broked;
    }
    
    public function setBroked(bool $broked): self
    {
        $this->broked = $broked;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Banque
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
}
