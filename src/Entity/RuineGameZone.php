<?php

namespace App\Entity;

use App\Repository\RuineGameZoneRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RuineGameZoneRepository::class)]
class RuineGameZone
{
    public const CASE_VIDE         = 0;
    public const DIRECTION_O       = 1;
    public const DIRECTION_N       = 2;
    public const DIRECTION_E       = 4;
    public const DIRECTION_S       = 8;
    public const DIRECTION_ON      = 3;
    public const DIRECTION_OE      = 5;
    public const DIRECTION_OS      = 9;
    public const DIRECTION_NE      = 6;
    public const DIRECTION_NS      = 10;
    public const DIRECTION_SE      = 12;
    public const DIRECTION_ONE     = 7;
    public const DIRECTION_ONS     = 11;
    public const DIRECTION_OSE     = 13;
    public const DIRECTION_NSE     = 14;
    public const DIRECTION_ONSE    = 15;
    public const CASE_ENTRE        = 16;
    public const CASE_COULOIR_VIDE = 17;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(options: ['unsigned' => true])]
    private ?int $id = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $x = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $y = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $z = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $couloir = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $zombies = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $distance = null;
    
    #[ORM\Column]
    private ?bool $closed = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $distancePiece = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $connect = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    private ?RuineGameZonePrototype $typeCase = null;
    
    #[ORM\ManyToOne(inversedBy: 'ruineGameZones')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RuineGame $ruineGame = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $zombieInitial = null;
    
    #[ORM\Column(nullable: true)]
    private ?bool $caseVu = null;
    
    #[ORM\Column(nullable: true)]
    private ?bool $pieceFouille = null;
    
    #[ORM\Column]
    private ?int $positionPorte = 0;
    
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $decoration = 0;
    
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $decorationVariation = 0;
    
    
    public function addCouloir(int $dirCouloir): self
    {
        if (!$this->hasDirCouloir($dirCouloir)) {
            $this->couloir += $dirCouloir;
        }
        
        return $this;
    }
    
    public function getConnect(): ?int
    {
        return $this->connect;
    }
    
    public function setConnect(int $connect): static
    {
        $this->connect = $connect;
        
        return $this;
    }
    
    public function getCouloir(): ?int
    {
        return $this->couloir;
    }
    
    public function setCouloir(int $couloir): static
    {
        $this->couloir = $couloir;
        
        return $this;
    }
    
    public function getDecoUnified(): int
    {
        return ($this->getDecoration() & 0xFFFF) | (($this->getDecorationVariation() & 0xFFFF) << 16);
    }
    
    public function getDecoration(): ?int
    {
        return $this->decoration;
    }
    
    public function setDecoration(int $decoration): static
    {
        $this->decoration = $decoration;
        
        return $this;
    }
    
    public function getDecorationVariation(): ?int
    {
        return $this->decorationVariation;
    }
    
    public function setDecorationVariation(int $decorationVariation): static
    {
        $this->decorationVariation = $decorationVariation;
        
        return $this;
    }
    
    public function getDistance(): ?int
    {
        return $this->distance;
    }
    
    public function setDistance(int $distance): static
    {
        $this->distance = $distance;
        
        return $this;
    }
    
    public function getDistancePiece(): ?int
    {
        return $this->distancePiece;
    }
    
    public function setDistancePiece(int $distancePiece): static
    {
        $this->distancePiece = $distancePiece;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getPositionPorte(): ?int
    {
        return $this->positionPorte;
    }
    
    public function setPositionPorte(int $positionPorte): static
    {
        $this->positionPorte = $positionPorte;
        
        return $this;
    }
    
    public function getRuineGame(): ?RuineGame
    {
        return $this->ruineGame;
    }
    
    public function setRuineGame(?RuineGame $ruineGame): static
    {
        $this->ruineGame = $ruineGame;
        
        return $this;
    }
    
    public function getTypeCase(): ?RuineGameZonePrototype
    {
        return $this->typeCase;
    }
    
    public function setTypeCase(?RuineGameZonePrototype $typeCase): static
    {
        $this->typeCase = $typeCase;
        
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(int $x): static
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(int $y): static
    {
        $this->y = $y;
        
        return $this;
    }
    
    public function getZ(): ?int
    {
        return $this->z;
    }
    
    public function setZ(int $z): static
    {
        $this->z = $z;
        
        return $this;
    }
    
    public function getZombieInitial(): ?int
    {
        return $this->zombieInitial;
    }
    
    public function setZombieInitial(?int $zombieInitial): static
    {
        $this->zombieInitial = $zombieInitial;
        
        return $this;
    }
    
    public function getZombies(): ?int
    {
        return $this->zombies;
    }
    
    public function setZombies(?int $zombies): static
    {
        $this->zombies = $zombies;
        
        return $this;
    }
    
    public function hasDirCouloir(int $direction): bool
    {
        if ($direction == self::CASE_VIDE) {
            return false;
        }
        $check = [
            self::DIRECTION_S => 1,
            self::DIRECTION_E => 2,
            self::DIRECTION_N => 3,
            self::DIRECTION_O => 4,
        ];
        if (!array_key_exists($direction, $check)) {
            return false;
        }
        $bin = sprintf("%'05d", decbin($this->couloir));
        return $bin[$check[$direction]] == 1;
    }
    
    public function isCaseVu(): ?bool
    {
        return $this->caseVu;
    }
    
    public function isClosed(): ?bool
    {
        return $this->closed;
    }
    
    public function isPieceFouille(): ?bool
    {
        return $this->pieceFouille;
    }
    
    public function removeDirCouloir(int $dirCouloir): self
    {
        if ($this->hasDirCouloir($dirCouloir)) {
            $this->couloir -= $dirCouloir;
        }
        
        return $this;
    }
    
    public function setCaseVu(?bool $caseVu): static
    {
        $this->caseVu = $caseVu;
        
        return $this;
    }
    
    public function setClosed(bool $closed): static
    {
        $this->closed = $closed;
        
        return $this;
    }
    
    public function setDecoUnified(int $decoration): self
    {
        $this->setDecoration($decoration & 0xFFFF);
        $this->setDecorationVariation(($decoration >> 16) & 0xFFFF);
        
        return $this;
    }
    
    public function setPieceFouille(?bool $pieceFouille): static
    {
        $this->pieceFouille = $pieceFouille;
        
        return $this;
    }
    
    
}
