<?php

namespace App\Entity;

use App\Repository\ReparationChantierRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ReparationChantierRepository::class)]
class ReparationChantier
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['outils_repa'])]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: OutilsReparation::class, fetch: 'EXTRA_LAZY', inversedBy: 'reparationChantiers')]
    #[ORM\JoinColumn(name: 'outils_repa_id', referencedColumnName: 'id', nullable: true)]
    private ?OutilsReparation $outilsReparation = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false), Groups(['outils_repa'])]
    private ?ChantierPrototype $chantier = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $pvActuel = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $paRepa70 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $paRepa99 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $paRepa100 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $gainDef70 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $gainDef99 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $gainDef100 = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $pctRepa = 70;
    
    #[ORM\Column(type: 'integer'), Groups(['outils_repa'])]
    private ?int $defActuelle = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['outils_repa'])]
    private ?int $paRepaPerso = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['outils_repa'])]
    private ?int $gainDefPerso = null;
    
    public function getChantier(): ?ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function setChantier(?ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function getDefActuelle(): ?int
    {
        return $this->defActuelle;
    }
    
    public function setDefActuelle(int $defActuelle): self
    {
        $this->defActuelle = $defActuelle;
        
        return $this;
    }
    
    public function getGainDef100(): ?int
    {
        return $this->gainDef100;
    }
    
    public function setGainDef100(int $gainDef100): self
    {
        $this->gainDef100 = $gainDef100;
        
        return $this;
    }
    
    public function getGainDef70(): ?int
    {
        return $this->gainDef70;
    }
    
    public function setGainDef70(int $gainDef70): self
    {
        $this->gainDef70 = $gainDef70;
        
        return $this;
    }
    
    public function getGainDef99(): ?int
    {
        return $this->gainDef99;
    }
    
    public function setGainDef99(int $gainDef99): self
    {
        $this->gainDef99 = $gainDef99;
        
        return $this;
    }
    
    public function getGainDefPerso(): ?int
    {
        return $this->gainDefPerso;
    }
    
    public function setGainDefPerso(int $gainDefPerso): static
    {
        $this->gainDefPerso = $gainDefPerso;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): ReparationChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getOutilsReparation(): ?OutilsReparation
    {
        return $this->outilsReparation;
    }
    
    public function setOutilsReparation(?OutilsReparation $outilsReparation): self
    {
        $this->outilsReparation = $outilsReparation;
        
        return $this;
    }
    
    public function getPaRepa100(): ?int
    {
        return $this->paRepa100;
    }
    
    public function setPaRepa100(int $paRepa100): self
    {
        $this->paRepa100 = $paRepa100;
        
        return $this;
    }
    
    public function getPaRepa70(): ?int
    {
        return $this->paRepa70;
    }
    
    public function setPaRepa70(int $paRepa70): self
    {
        $this->paRepa70 = $paRepa70;
        
        return $this;
    }
    
    public function getPaRepa99(): ?int
    {
        return $this->paRepa99;
    }
    
    public function setPaRepa99(int $paRepa99): self
    {
        $this->paRepa99 = $paRepa99;
        
        return $this;
    }
    
    public function getPaRepaPerso(): ?int
    {
        return $this->paRepaPerso;
    }
    
    public function setPaRepaPerso(int $paRepaPerso): static
    {
        $this->paRepaPerso = $paRepaPerso;
        
        return $this;
    }
    
    public function getPctRepa(): ?int
    {
        return $this->pctRepa;
    }
    
    public function setPctRepa(int $pctRepa): self
    {
        $this->pctRepa = $pctRepa;
        
        return $this;
    }
    
    public function getPvActuel(): ?int
    {
        return $this->pvActuel;
    }
    
    public function setPvActuel(int $pvActuel): self
    {
        $this->pvActuel = $pvActuel;
        
        return $this;
    }
}
