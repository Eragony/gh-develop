<?php

namespace App\Entity;

use App\Repository\TypeDispoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeDispoRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeDispo
{
    public const PAS_CO = 0;
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['admin', 'option', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['admin', 'option', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['admin', 'option', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?string $description = null;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 1]), Groups(['admin', 'option', 'jump', 'expe', 'outils_expe', 'inscription_exp_visu'])]
    private ?int $typologie = 1;
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeDispo
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getTypologie(): ?int
    {
        return $this->typologie;
    }
    
    public function setTypologie(int $typologie): static
    {
        $this->typologie = $typologie;
        
        return $this;
    }
}
