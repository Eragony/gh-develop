<?php

namespace App\Entity;

use App\Repository\RuinesPlansRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RuinesPlansRepository::class)]
class RuinesPlans
{
    
    
    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: 'integer'), Groups(['ruine', 'admin_ruine'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['ruine', 'admin_ruine'])]
    private ?User $CreateBy = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['ruine', 'admin_ruine'])]
    private ?User $ModifyBy = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['ruine', 'admin_ruine'])]
    private ?DateTimeInterface $created_at = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['ruine', 'admin_ruine'])]
    private ?DateTimeInterface $modify_at = null;
    
    /** @var Collection<RuinesCases> */
    #[ORM\OneToMany(mappedBy: 'ruinesPlans', targetEntity: RuinesCases::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true),
        Groups(['ruine_plan', 'admin_ruine'])]
    private Collection $cases;
    
    
    #[ORM\ManyToOne(targetEntity: Ruines::class, fetch: 'EXTRA_LAZY', inversedBy: 'Plans')]
    #[ORM\JoinColumn(name: 'ruines_id', referencedColumnName: 'id', nullable: false), Groups(['admin_gen'])]
    private ?Ruines $ruines = null;
    
    /**
     * @var int[][]|null $traceSafe
     */
    #[ORM\Column(type: 'json', nullable: true), Groups(['admin_ruine'])]
    private ?array $traceSafe = [];
    
    
    public function __construct()
    {
        $this->cases = new ArrayCollection();
    }
    
    public function addCase(RuinesCases $case): self
    {
        if (!$this->cases->contains($case)) {
            $this->cases[] = $case;
            $case->setRuinesPlans($this);
        }
        
        return $this;
    }
    
    /**
     * @return Collection|RuinesCases[]
     */
    public function getCases(): Collection|array
    {
        return $this->cases;
    }
    
    public function getCreateBy(): ?User
    {
        return $this->CreateBy;
    }
    
    public function setCreateBy(?User $CreateBy): self
    {
        $this->CreateBy = $CreateBy;
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }
    
    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->modify_at;
    }
    
    public function setModifyAt(?DateTimeInterface $modify_at): self
    {
        $this->modify_at = $modify_at;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->ModifyBy;
    }
    
    public function setModifyBy(?User $ModifyBy): self
    {
        $this->ModifyBy = $ModifyBy;
        
        return $this;
    }
    
    public function getRuines(): ?Ruines
    {
        return $this->ruines;
    }
    
    public function setRuines(?Ruines $ruines): self
    {
        $this->ruines = $ruines;
        
        return $this;
    }
    
    public function getTraceSafe(): ?array
    {
        return $this->traceSafe;
    }
    
    public function setTraceSafe(?array $traceSafe): self
    {
        $this->traceSafe = $traceSafe;
        
        return $this;
    }
    
    public function removeCase(RuinesCases $case): self
    {
        if ($this->cases->removeElement($case)) {
            // set the owning side to null (unless already changed)
            if ($case->getRuinesPlans() === $this) {
                $case->setRuinesPlans(null);
            }
        }
        
        return $this;
    }
}
