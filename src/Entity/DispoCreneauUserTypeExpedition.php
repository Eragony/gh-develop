<?php

namespace App\Entity;

use App\Repository\DispoCreneauUserTypeExpeditionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DispoCreneauUserTypeExpeditionRepository::class)]
class DispoCreneauUserTypeExpedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'option', 'outils_expe'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false), Groups(['admin', 'option', 'outils_expe'])]
    private ?TypeDispo $dispo = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false), Groups(['admin', 'option', 'outils_expe'])]
    private ?CreneauHorraire $creneau = null;
    
    #[ORM\ManyToOne(inversedBy: 'creneau')]
    #[ORM\JoinColumn(nullable: false)]
    private ?DispoUserTypeExpedition $dispoUserTypeExpedition = null;
    
    public function getCreneau(): ?CreneauHorraire
    {
        return $this->creneau;
    }
    
    public function setCreneau(?CreneauHorraire $creneau): static
    {
        $this->creneau = $creneau;
        
        return $this;
    }
    
    public function getDispo(): ?TypeDispo
    {
        return $this->dispo;
    }
    
    public function setDispo(?TypeDispo $dispo): static
    {
        $this->dispo = $dispo;
        
        return $this;
    }
    
    public function getDispoUserTypeExpedition(): ?DispoUserTypeExpedition
    {
        return $this->dispoUserTypeExpedition;
    }
    
    public function setDispoUserTypeExpedition(?DispoUserTypeExpedition $dispoUserTypeExpedition): static
    {
        $this->dispoUserTypeExpedition = $dispoUserTypeExpedition;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @param int|null $id
     * @return DispoCreneauUserTypeExpedition
     */
    public function setId(?int $id): DispoCreneauUserTypeExpedition
    {
        $this->id = $id;
        return $this;
    }
}
