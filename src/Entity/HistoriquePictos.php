<?php

namespace App\Entity;

use App\Repository\HistoriquePictosRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: HistoriquePictosRepository::class)]
class HistoriquePictos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Column(type: 'integer')]
    #[Groups(['picto'])]
    private ?int $mapId = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: PictoPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['picto'])]
    private ?PictoPrototype $picto = null;
    
    #[ORM\Column(type: 'integer')]
    #[Groups(['picto'])]
    private ?int $nombre = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): HistoriquePictos
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    public function setMapId(int $mapId): self
    {
        $this->mapId = $mapId;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getPicto(): ?PictoPrototype
    {
        return $this->picto;
    }
    
    public function setPicto(?PictoPrototype $picto): self
    {
        $this->picto = $picto;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
}
