<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

/**
 * TypeObjet
 */
#[ORM\Table(name: 'type_objet')]
#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeObjet
{
    public const CASSABLE      = 1;
    public const EMPOISSONABLE = 2;
    public const MARQUEUR      = 3;
    
    #[ORM\Column(name: 'nom', type: 'string', length: 24, nullable: false), Groups(['admin_obj', 'carte', 'outils_expe'])]
    private ?string $nom;
    
    
    public function __construct(#[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
                                #[ORM\Id, Groups(['admin_obj', 'carte', 'outils_expe'])]
                                private int $id)
    {
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function setId(int $id): TypeObjet
    {
        $this->id = $id;
        
        return $this;
    }
    
    
}
