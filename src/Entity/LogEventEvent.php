<?php

namespace App\Entity;

use App\Repository\LogEventEventRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LogEventEventRepository::class)]
class LogEventEvent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['gestion_event'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['gestion_event'])]
    private ?User $declencheur = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['gestion_event'])]
    private ?string $libelle = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['gestion_event'])]
    private ?DateTimeInterface $eventAt = null;
    
    #[ORM\Column(length: 255, nullable: true), Groups(['gestion_event'])]
    private ?string $valeurAvant = null;
    
    #[ORM\Column(length: 255, nullable: true), Groups(['gestion_event'])]
    private ?string $valeurApres = null;
    
    #[ORM\ManyToOne(inversedBy: 'logEvent')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Event $event = null;
    
    /**
     * @var int|null
     * Valeur possible :
     * 0 - traduction texte
     */
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0])]
    private ?int $typologie = 0;
    
    
    public function getDeclencheur(): ?User
    {
        return $this->declencheur;
    }
    
    public function setDeclencheur(?User $declencheur): self
    {
        $this->declencheur = $declencheur;
        
        return $this;
    }
    
    public function getEvent(): ?Event
    {
        return $this->event;
    }
    
    public function setEvent(?Event $event): static
    {
        $this->event = $event;
        
        return $this;
    }
    
    public function getEventAt(): ?DateTimeInterface
    {
        return $this->eventAt;
    }
    
    public function setEventAt(DateTimeInterface $eventAt): self
    {
        $this->eventAt = $eventAt;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): LogEventEvent
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }
    
    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;
        
        return $this;
    }
    
    public function getTypologie(): ?int
    {
        return $this->typologie;
    }
    
    public function setTypologie(int $typologie): static
    {
        $this->typologie = $typologie;
        
        return $this;
    }
    
    public function getValeurApres(): ?string
    {
        return $this->valeurApres;
    }
    
    public function setValeurApres(?string $valeurApres): static
    {
        $this->valeurApres = $valeurApres;
        
        return $this;
    }
    
    public function getValeurAvant(): ?string
    {
        return $this->valeurAvant;
    }
    
    public function setValeurAvant(?string $valeurAvant): static
    {
        $this->valeurAvant = $valeurAvant;
        
        return $this;
    }
}
