<?php

namespace App\Entity;

use App\Repository\TypeCaracteristiqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeCaracteristiqueRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeCaracteristique
{
    public const EAU        = 10;
    public const NOURRITURE = 11;
    public const DRUGS      = 12;
    public const ALCOOL     = 13;
    
    
    #[ORM\Id]
    #[ORM\Column]
    #[Groups(['admin_obj', 'admin_type', 'outils_expe', 'ency_objet'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 30)]
    #[Groups(['admin_obj', 'admin_type', 'outils_expe', 'ency_objet'])]
    private ?string $nom = null;
    
    #[ORM\Column(length: 20, nullable: true)]
    #[Groups(['admin_obj', 'admin_type', 'outils_expe', 'ency_objet'])]
    private ?string $icon = null;
    
    /** @var Collection<CaracteristiquesItem> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'typeCarac', targetEntity: CaracteristiquesItem::class, orphanRemoval: true)]
    private Collection $caracteristiquesItems;
    
    public function __construct()
    {
        $this->caracteristiquesItems = new ArrayCollection();
    }
    
    public function addCaracteristiquesItem(CaracteristiquesItem $caracteristiquesItem): static
    {
        if (!$this->caracteristiquesItems->contains($caracteristiquesItem)) {
            $this->caracteristiquesItems->add($caracteristiquesItem);
            $caracteristiquesItem->setTypeCarac($this);
        }
        
        return $this;
    }
    
    public function getCaracteristiquesItems(): Collection
    {
        return $this->caracteristiquesItems;
    }
    
    public function setCaracteristiquesItems(Collection $caracteristiquesItems): TypeCaracteristique
    {
        $this->caracteristiquesItems = $caracteristiquesItems;
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(?string $icon): static
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeCaracteristique
    {
        $this->id = $id;
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function removeCaracteristiquesItem(CaracteristiquesItem $caracteristiquesItem): static
    {
        if ($this->caracteristiquesItems->removeElement($caracteristiquesItem)) {
            // set the owning side to null (unless already changed)
            if ($caracteristiquesItem->getTypeCarac() === $this) {
                $caracteristiquesItem->setTypeCarac(null);
            }
        }
        
        return $this;
    }
}
