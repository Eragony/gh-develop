<?php

namespace App\Entity;

use App\Repository\StatutUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: StatutUserRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class StatutUser
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['coalition', 'coalition_gen'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['coalition_gen'])]
    private ?string $nom = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): StatutUser
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
