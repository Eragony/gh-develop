<?php

namespace App\Entity;

use App\Repository\BatPrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ORM\Table(name: 'bat_prototype')]
#[ORM\Entity(repositoryClass: BatPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class BatPrototype
{
    
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: Types::SMALLINT, nullable: false)]
    #[SerializedName('id')]
    #[Groups(['admin', 'admin_ville_bat', 'admin_ruine', 'admin_gen', 'banque', 'ency', 'plan', 'ruine', 'bat', 'carte', 'comparatif'])]
    private int $id;
    
    #[ORM\Column(name: 'nom', type: 'string', length: 64, nullable: false)]
    #[Groups(['admin', 'admin_ville_bat', 'admin_ruine', 'admin_gen', 'banque', 'ency', 'plan', 'ruine', 'bat', 'carte', 'comparatif'])]
    private ?string $nom = null;
    
    #[ORM\Column(name: 'description', type: 'string', length: 2550, nullable: false)]
    #[Groups(['ency', 'bat', 'admin_bat', 'carte'])]
    private string $description;
    
    #[ORM\Column(name: 'km_min', type: 'smallint', nullable: false)]
    #[Groups(['ency', 'bat', 'admin_bat', 'carte'])]
    private int $kmMin;
    
    #[ORM\Column(name: 'km_max', type: 'smallint', nullable: false)]
    #[Groups(['ency', 'bat', 'admin_bat', 'carte'])]
    private int $kmMax;
    
    #[ORM\Column(name: 'bonus_camping', type: 'smallint', nullable: false)]
    #[Groups(['ency', 'bat', 'admin_bat', 'carte'])]
    private int $bonusCamping;
    
    #[ORM\Column(name: 'explorable', type: 'boolean', nullable: false), Groups(['bat', 'admin_bat', 'carte', 'ency_bat'])]
    private bool $explorable;
    
    #[ORM\Column(name: 'icon', type: 'string', length: 30, nullable: false)]
    #[Groups(['admin', 'admin_ville_bat', 'ency', 'ruine', 'bat', 'admin_bat', 'admin_gen', 'carte'])]
    private ?string $icon = null;
    
    #[ORM\Column(name: 'id_hordes', type: 'smallint', unique: true, nullable: true)]
    private ?int $idHordes = null;
    
    /** @var Collection<ItemBatiment> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'batPrototype', targetEntity: ItemBatiment::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['ency', 'admin_bat'])]
    private Collection $items;
    
    #[ORM\Column(name: 'id_mh', nullable: true), Groups(['admin_bat'])]
    private ?int $idMh = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false])]
    #[Groups(['ency', 'bat', 'admin_gen'])]
    private ?bool $actif = null;
    
    private ?int $probaItemGlobale = 0;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0]), Groups(['carte', 'ency', 'bat', 'admin_bat'])]
    private ?int $maxCampeur = 0;
    
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    public function addItem(ItemBatiment $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setBatPrototype($this);
        }
        
        return $this;
    }
    
    public function getBonusCamping(): ?int
    {
        return $this->bonusCamping;
    }
    
    public function setBonusCamping(int $bonusCamping): self
    {
        $this->bonusCamping = $bonusCamping;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdHordes(): ?int
    {
        return $this->idHordes;
    }
    
    public function setIdHordes(?int $idHordes): self
    {
        $this->idHordes = $idHordes;
        
        return $this;
    }
    
    public function getIdMh(): ?int
    {
        return $this->idMh;
    }
    
    public function setIdMh(?int $idMh): self
    {
        $this->idMh = $idMh;
        
        return $this;
    }
    
    /** @return Collection<int, ItemBatiment> */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function getKmMax(): ?int
    {
        return $this->kmMax;
    }
    
    public function setKmMax(int $kmMax): self
    {
        $this->kmMax = $kmMax;
        
        return $this;
    }
    
    public function getKmMin(): ?int
    {
        return $this->kmMin;
    }
    
    public function setKmMin(int $kmMin): self
    {
        $this->kmMin = $kmMin;
        
        return $this;
    }
    
    public function getMaxCampeur(): ?int
    {
        return $this->maxCampeur;
    }
    
    public function setMaxCampeur(int $maxCampeur): static
    {
        $this->maxCampeur = $maxCampeur;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    #[Groups(['banque', 'ency', 'bat', 'admin_bat'])]
    public function getProbaItemGlobale(): ?int
    {
        
        if ($this->probaItemGlobale === 0) {
            $this->setProbaItemGlobale();
        }
        
        return $this->probaItemGlobale;
    }
    
    public function setProbaItemGlobale(): void
    {
        $proba = 0;
        
        foreach ($this->getItems() as $item) {
            $proba += $item->getProbabily();
        }
        
        $this->probaItemGlobale = $proba;
    }
    
    public function getTotalProba(): int
    {
        
        $total = 0;
        
        foreach ($this->getItems() as $item) {
            $total += $item->getProbabily();
        }
        
        return $total;
        
    }
    
    public function isActif(): ?bool
    {
        return $this->actif;
    }
    
    public function isExplorable(): bool
    {
        return $this->explorable;
    }
    
    public function setExplorable(bool $explorable): BatPrototype
    {
        $this->explorable = $explorable;
        
        return $this;
    }
    
    public function removeItem(ItemBatiment $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getBatPrototype() === $this) {
                $item->setBatPrototype(null);
            }
        }
        
        return $this;
    }
    
    public function setActif(?bool $actif): self
    {
        $this->actif = $actif;
        
        return $this;
    }
    
}
