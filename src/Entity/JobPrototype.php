<?php

namespace App\Entity;

use App\Repository\JobPrototypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: JobPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class JobPrototype
{
    public const job_none     = 1;
    public const job_basic    = 2;
    public const job_collec   = 3;
    public const job_eclair   = 4;
    public const job_guardian = 5;
    public const job_hunter   = 6;
    public const job_tamer    = 7;
    public const job_tech     = 8;
    public const job_shaman   = 9;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['citoyens', 'carte_gen', 'event_inscription', 'coalition', 'gestion_jump', 'gestion_event', 'jump', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'event_inscription', 'job', 'comparatif'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 15)]
    #[Groups(['citoyens', 'carte_gen', 'event_inscription', 'coalition', 'gestion_jump', 'gestion_event', 'jump', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'event_inscription', 'job', 'comparatif'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    #[Groups(['citoyens', 'carte_gen', 'event_inscription', 'coalition', 'gestion_jump', 'gestion_event', 'jump', 'outils_chantier', 'outils_expe', 'inscription_exp_visu', 'event_inscription', 'job', 'comparatif'])]
    private ?string $icon = null;
    
    #[ORM\Column(type: 'boolean')]
    private ?bool   $isHeros     = null;
    #[ORM\Column(type: 'string', length: 360, nullable: true)]
    private ?string $description = null;
    #[ORM\Column(type: 'string', length: 10)]
    private ?string $ref         = null;
    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['coalition', 'gestion_jump', 'gestion_event', 'jump', 'event_inscription', 'job', 'comparatif'])]
    private ?string $alternatif  = null;
    
    public function getAlternatif(): ?string
    {
        return $this->alternatif;
    }
    
    public function setAlternatif(string $alternatif): self
    {
        $this->alternatif = $alternatif;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): JobPrototype
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIsHeros(): ?bool
    {
        return $this->isHeros;
    }
    
    public function setIsHeros(bool $isHeros): self
    {
        $this->isHeros = $isHeros;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getRef(): ?string
    {
        return $this->ref;
    }
    
    public function setRef(string $ref): self
    {
        $this->ref = $ref;
        
        return $this;
    }
}
