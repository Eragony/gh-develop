<?php

namespace App\Entity;

use App\Repository\LevelRuinePrototypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LevelRuinePrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class LevelRuinePrototype
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['jump'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['jump'])]
    private ?int $level = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['jump'])]
    private ?string $description = null;
    
    #[ORM\Column(type: 'string', length: 30), Groups(['jump'])]
    private ?string $nom = null;
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): LevelRuinePrototype
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLevel(): ?int
    {
        return $this->level;
    }
    
    public function setLevel(int $level): self
    {
        $this->level = $level;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
