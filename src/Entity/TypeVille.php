<?php

namespace App\Entity;

use App\Repository\TypeVilleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeVilleRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeVille
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['jump', 'event'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 25), Groups(['jump', 'event'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 5), Groups(['jump', 'event'])]
    private ?string $abrev = null;
    
    #[ORM\Column(type: 'string', length: 10), Groups(['jump', 'event'])]
    private ?string $icon = null;
    
    public function getAbrev(): ?string
    {
        return $this->abrev;
    }
    
    public function setAbrev(string $abrev): self
    {
        $this->abrev = $abrev;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeVille
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
