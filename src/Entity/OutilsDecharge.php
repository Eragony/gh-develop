<?php

namespace App\Entity;

use App\Doctrine\IdAlphaOutilsDechargeGenerator;
use App\Repository\OutilsDechargeRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OutilsDechargeRepository::class)]
class OutilsDecharge
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaOutilsDechargeGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    #[Groups(['outils_decharge'])]
    private ?string $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['outils_decharge'])]
    private ?User $createdBy = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['outils_decharge'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['outils_decharge'])]
    private ?DateTimeInterface $createdAt = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['outils_decharge'])]
    private ?DateTimeInterface $modifyAt = null;
    
    #[ORM\OneToOne(mappedBy: 'outilsDecharge', targetEntity: Outils::class, cascade: ['persist',
                                                                                      'remove'], fetch: 'EXTRA_LAZY')]
    private ?Outils $outils = null;
    
    /** @var Collection<Decharges> */
    #[ORM\OneToMany(mappedBy: 'outilsDecharge', targetEntity: Decharges::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'regroupement_items_id'),
        Groups(['outils_decharge'])]
    private Collection $decharges;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $defTotale = 0;
    
    public function __construct()
    {
        $this->decharges = new ArrayCollection();
    }
    
    public function addDecharge(Decharges $decharge): self
    {
        if (!$this->decharges->contains($decharge)) {
            $this->decharges[$decharge->getRegroupItems()->getId()] = $decharge;
            $decharge->setOutilsDecharge($this);
        }
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getDecharge(int $idRegroupemement): Decharges|null
    {
        return $this->decharges->toArray()[$idRegroupemement] ?? null;
    }
    
    public function getDecharges(): Collection
    {
        return $this->decharges;
    }
    
    public function getDefTotale(): ?int
    {
        return $this->defTotale;
    }
    
    public function setDefTotale(int $defTotale): self
    {
        $this->defTotale = $defTotale;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): OutilsDecharge
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->modifyAt;
    }
    
    public function setModifyAt(?DateTimeInterface $modifyAt): self
    {
        $this->modifyAt = $modifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): self
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getOutils(): ?Outils
    {
        return $this->outils;
    }
    
    public function setOutils(Outils $outils): self
    {
        $this->outils = $outils;
        
        $outils->setOutilsDecharge($this);
        
        return $this;
    }
    
    public function removeDecharge(Decharges $decharge): self
    {
        
        if ($this->getDecharge($decharge->getRegroupItems()->getId()) !== null) {
            $this->decharges->removeElement($this->getDecharge($decharge->getRegroupItems()->getId()));
            if ($decharge->getOutilsDecharge() === $this) {
                $decharge->setOutilsDecharge(null);
            }
        }
        
        return $this;
    }
    
    public function setDecharge(Decharges $decharge): self
    {
        $this->decharges->toArray()[$decharge->getRegroupItems()->getId()] = $decharge;
        
        return $this;
    }
}
