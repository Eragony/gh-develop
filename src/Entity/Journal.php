<?php

namespace App\Entity;

use App\Repository\JournalRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: JournalRepository::class)]
class Journal
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'journal')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'integer'), Groups(['journal', 'comparatif'])]
    private ?int $def = null;
    
    #[ORM\Column(type: 'integer'), Groups(['journal', 'comparatif'])]
    private ?int $zombie = null;
    
    #[ORM\Column(type: 'string', length: 2000), Groups(['journal', 'comparatif'])]
    private ?string $content = null;
    
    #[ORM\Column(type: 'string', length: 255, nullable: true), Groups(['journal', 'comparatif'])]
    private ?string $regenDir = null;
    
    #[ORM\Column(type: 'integer', nullable: true), Groups(['journal', 'comparatif'])]
    private ?int $water = null;
    
    #[Groups(['journal', 'comparatif'])]
    private ?string $directionTranslate = null;
    
    #[ORM\Column(type: 'string', length: 2000, nullable: true), Groups(['journal', 'comparatif'])]
    private ?string $contentEn = null;
    
    #[ORM\Column(type: 'string', length: 2000, nullable: true), Groups(['journal', 'comparatif'])]
    private ?string $contentDe = null;
    
    #[ORM\Column(type: 'string', length: 2000, nullable: true), Groups(['journal', 'comparatif'])]
    private ?string $contentEs = null;
    
    public function __construct(#[ORM\Column(type: 'smallint'), Groups(['journal', 'comparatif'])]
                                private int $day)
    {
    }
    
    public function getContent(): ?string
    {
        return htmlspecialchars_decode($this->content);
    }
    
    public function setContent(string $content): self
    {
        $this->content = htmlspecialchars($content);
        
        return $this;
    }
    
    public function getContentDe(): ?string
    {
        return $this->contentDe;
    }
    
    public function setContentDe(string $contentDe): static
    {
        $this->contentDe = $contentDe;
        
        return $this;
    }
    
    public function getContentEn(): ?string
    {
        return $this->contentEn;
    }
    
    public function setContentEn(?string $contentEn): static
    {
        $this->contentEn = $contentEn;
        
        return $this;
    }
    
    public function getContentEs(): ?string
    {
        return $this->contentEs;
    }
    
    public function setContentEs(string $contentEs): static
    {
        $this->contentEs = $contentEs;
        
        return $this;
    }
    
    public function getDay(): int
    {
        return $this->day;
    }
    
    public function getDef(): ?int
    {
        return $this->def;
    }
    
    public function setDef(int $def): self
    {
        $this->def = $def;
        
        return $this;
    }
    
    #[Groups(['journal', 'comparatif'])]
    public function getDirectionSansArticle(): string|null
    {
        if ($this->getRegenDir() === null) {
            return null;
        }
        
        return preg_replace('/l(\'|e )/', '', $this->getRegenDir());
        
    }
    
    /**
     * @return string|null
     */
    #[Groups(['journal', 'comparatif'])]
    public function getDirectionTranslate(): ?string
    {
        if ($this->getRegenDir() !== null) {
            $direction = '';
            switch ($this->getRegenDir()) {
                case 'le nord':
                    $direction = 'Nord';
                    break;
                case 'le sud':
                    $direction = 'Sud';
                    break;
                case "l'est":
                    $direction = 'Est';
                    break;
                case "l'ouest":
                    $direction = 'Ouest';
                    break;
                case "le nord-est":
                    $direction = 'Nord-est';
                    break;
                case "le nord-ouest":
                    $direction = 'Nord-ouest';
                    break;
                case "le sud-est":
                    $direction = 'Sud-est';
                    break;
                case "le sud-ouest":
                    $direction = 'Sud-ouest';
                    break;
                case "invalid direction":
                case "Inconnue":
                    $direction = "Inconnue";
                    break;
            }
            
            $this->setDirectionTranslate($direction);
        }
        return $this->directionTranslate;
    }
    
    public function setDirectionTranslate(?string $directionTranslate): Journal
    {
        $this->directionTranslate = $directionTranslate;
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Journal
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getRegenDir(): ?string
    {
        return $this->regenDir;
    }
    
    public function setRegenDir(?string $regenDir): self
    {
        $this->regenDir = $regenDir;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getWater(): ?int
    {
        return $this->water;
    }
    
    public function setWater(?int $water): self
    {
        $this->water = $water;
        
        return $this;
    }
    
    public function getZombie(): ?int
    {
        return $this->zombie;
    }
    
    public function setZombie(int $zombie): self
    {
        $this->zombie = $zombie;
        
        return $this;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
}
