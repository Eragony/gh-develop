<?php

namespace App\Entity;

use App\Repository\CleanUpCadaverRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CleanUpCadaverRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class CleanUpCadaver
{
    public const garbage = 1;
    public const water   = 2;
    public const cook    = 3;
    public const ghoul   = 4;
    #[ORM\Column(type: 'string', length: 10)]
    private ?string $ref = null;
    
    #[ORM\Column(type: 'string', length: 32)]
    #[Groups(['picto', 'citoyens', 'comparatif'])]
    private ?string $label = null;
    
    #[ORM\Column(length: 15)]
    #[Groups(['picto', 'citoyens', 'comparatif'])]
    private ?string $icon = null;
    
    public function __construct(#[ORM\Id]
                                #[ORM\Column(type: 'integer')]
                                private ?int $id)
    {
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }
    
    public function setLabel(string $label): self
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getRef(): ?string
    {
        return $this->ref;
    }
    
    public function setRef(string $ref): self
    {
        $this->ref = $ref;
        
        return $this;
    }
    
    public function setId(?int $id): CleanUpCadaver
    {
        $this->id = $id;
        
        return $this;
    }
}
