<?php

namespace App\Entity;

use App\Repository\ChantierPrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'chantier_prototype')]
#[ORM\Entity(repositoryClass: ChantierPrototypeRepository::class)]
#[ORM\Index(columns: ["plan"], name: "plan_lvl")]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ChantierPrototype
{
    
    public const RUINE_HOPITAL                 = 'Hs';
    public const RUINE_HOTEL                   = 'Ho';
    public const RUINE_BUNKER                  = 'Bu';
    public const ID_CHANTIER_CERCUEIL          = 66;
    public const ID_CHANTIER_CIME              = 67;
    public const ID_CHANTIER_SD                = 70;
    public const ID_CHANTIER_STRATEGIE         = 73;
    public const ID_CHANTIER_ATELIER           = 76;
    public const ID_CHANTIER_PLANIF            = 79;
    public const ID_CHANTIER_CARTE_AME         = 78;
    public const ID_CHANTIER_SCRUT             = 80;
    public const ID_CHANTIER_TDGA              = 101;
    public const ID_CHANTIER_TDG               = 103;
    public const ID_CHANTIER_DECHARDE_BOIS     = 112;
    public const ID_CHANTIER_FERRAILLERIE      = 113;
    public const ID_CHANTIER_ENCLOS            = 114;
    public const ID_CHANTIER_DECHARGE_PIEGE    = 116;
    public const ID_CHANTIER_APPAT             = 117;
    public const ID_CHANTIER_DECHARGE_BLINDE   = 118;
    public const ID_CHANTIER_PHARE             = 121;
    public const ID_CHANTIER_HAMAME            = 144;
    public const ID_CHANTIER_PO                = 153;
    public const ID_CHANTIER_DECHARGE_PUBLIQUE = 120;
    public const ID_CHANTIER_DECHARGE_HUMI     = 119;
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
    #[Groups(['chantier', 'plan', 'evo_chantier', 'carte', 'outils_decharge', 'outils_repa', 'reparation', 'journal',
              'plan_construit', 'admin_gen', 'admin_ch', 'admin_arbre', 'ency_objet', 'ency_decharge', 'comparatif', 'comparatif_spe'])]
    private int $id = 0;
    
    #[ORM\Column(name: "nom", type: "string", length: 32, nullable: false)]
    #[Groups(['chantier', 'plan', 'evo_chantier', 'journal', 'carte', 'reparation', 'outils_repa', 'admin_gen',
              'admin_ch', 'admin_arbre', 'ency_decharge', 'comparatif', 'comparatif_spe'])]
    private string $nom;
    
    #[ORM\Column(name: "icon", type: "string", length: 24, nullable: false)]
    #[Groups(['chantier', 'evo_chantier', 'journal', 'carte', 'outils_repa', 'admin_gen', 'admin_arbre', 'comparatif', 'comparatif_spe'])]
    private string $icon;
    
    #[ORM\Column(name: "def", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'outils_repa', 'admin'])]
    private int $def;
    
    #[ORM\Column(name: "water", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'admin'])]
    private int $water;
    
    #[ORM\Column(name: "pa", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'outils_repa', 'admin'])]
    private int $pa;
    
    #[ORM\Column(name: "niveau", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'admin', 'comparatif'])]
    private int $niveau;
    
    #[ORM\Column(name: "plan", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'plan', 'admin', 'comparatif'])]
    private int $plan;
    
    #[ORM\Column(name: "temp", type: "boolean", nullable: false)]
    #[Groups(['chantier', 'outils_repa', 'admin', 'comparatif'])]
    private bool $temp;
    
    #[ORM\Column(name: "pv", type: "smallint", nullable: false)]
    #[Groups(['chantier', 'outils_repa', 'admin'])]
    private int $pv;
    
    #[ORM\Column(name: "indes", type: "boolean", nullable: false)]
    #[Groups(['chantier', 'outils_repa', 'admin'])]
    private bool $indes;
    
    #[ORM\Column(name: "ruine_ho", type: "boolean", nullable: false)]
    #[Groups(['chantier', 'plan', 'admin'])]
    private bool $ruineHo;
    
    #[ORM\Column(name: "ruine_hs", type: "boolean", nullable: false)]
    #[Groups(['chantier', 'plan', 'admin'])]
    private bool $ruineHs;
    
    #[ORM\Column(name: "ruine_bu", type: "boolean", nullable: false)]
    #[Groups(['chantier', 'plan', 'admin'])]
    private bool $ruineBu;
    
    /** @var Collection<RessourceChantier> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: RessourceChantier::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\JoinTable(name: 'chantiers_ressources')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'ressource_id', referencedColumnName: 'id', unique: true)]
    #[Groups(['chantier', 'admin'])]
    private Collection $ressources;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id')]
    #[Groups(['plan', 'admin'])]
    private ?ChantierPrototype $parent = null;
    
    /** @var Collection<ChantierPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'child_id', referencedColumnName: 'id')]
    #[Groups(['chantier', 'admin_arbre', 'comparatif'])]
    private Collection $children;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['chantier', 'admin', 'admin_arbre', 'comparatif', 'comparatif_spe'])]
    private ?int $orderby = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['chantier', 'evo_chantier', 'admin', 'comparatif_spe'])]
    private int $levelMax;
    
    /** @var Collection<BonusUpChantier> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: BonusUpChantier::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\JoinTable(name: 'chantiers_bonusUp')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'bonusUp_id', referencedColumnName: 'id', unique: true)]
    #[Groups(['evo_chantier', 'admin'])]
    private Collection $levelUps;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'chantierOfCat')]
    #[ORM\JoinColumn(name: 'cat_id', referencedColumnName: 'id')]
    #[Groups(['chantier', 'outils_repa', 'admin', 'comparatif'])]
    private ?ChantierPrototype $catChantier = null;
    
    /** @var Collection<ChantierPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'catChantier', targetEntity: ChantierPrototype::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'chantierCat_id', referencedColumnName: 'id'), Groups(['reparation'])]
    private Collection $chantierOfCat;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['chantier', 'outils_repa', 'admin', 'admin_arbre', 'comparatif'])]
    private ?int $orderByListing = null;
    
    #[ORM\Column(name: 'uid', type: 'string', length: 64, unique: true, nullable: true), Groups(['admin'])]
    private ?string $uid = null;
    
    #[ORM\Column(type: 'smallint', unique: true, nullable: true), Groups(['admin'])]
    private ?int $idHordes = null;
    
    #[ORM\Column(name: 'id_mh', nullable: true), Groups(['admin'])]
    private ?int $idMh = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false])]
    #[Groups(['chantier', 'outils_repa', 'admin', 'admin_gen'])]
    private ?bool $actif = null;
    
    #[ORM\Column(length: 2550, nullable: true)]
    #[Groups(['chantier', 'admin', 'admin_ch'])]
    private ?string $description = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['chantier', 'outils_repa', 'admin', 'admin_gen', 'comparatif'])]
    private ?int $orderByGeneral = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false])]
    #[Groups(['chantier', 'outils_repa', 'admin', 'admin_gen'])]
    private ?bool $specifiqueVillePrive = false;
    
    public function __construct()
    {
        $this->ressources    = new ArrayCollection();
        $this->children      = new ArrayCollection();
        $this->levelUps      = new ArrayCollection();
        $this->chantierOfCat = new ArrayCollection();
    }
    
    public function addChantierOfCat(self $chantierOfCat): self
    {
        if (!$this->chantierOfCat->contains($chantierOfCat)) {
            $this->chantierOfCat[$chantierOfCat->getId()] = $chantierOfCat;
            $chantierOfCat->setCatChantier($this);
        }
        
        return $this;
    }
    
    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }
        
        return $this;
    }
    
    public function addLevelUp(BonusUpChantier $bonusUpChantier): self
    {
        if (!$this->levelUps->contains($bonusUpChantier)) {
            $this->levelUps[] = $bonusUpChantier;
        }
        
        return $this;
    }
    
    public function addRessource(RessourceChantier $ressource): self
    {
        if (!$this->ressources->contains($ressource)) {
            $this->ressources[] = $ressource;
        }
        
        return $this;
    }
    
    public function getCatChantier(): ?self
    {
        return $this->catChantier;
    }
    
    public function setCatChantier(?self $CatChantier): self
    {
        $this->catChantier = $CatChantier;
        
        return $this;
    }
    
    /**
     * @return Collection<ChantierPrototype>
     * @throws Exception
     */
    public function getChantierOfCat(): Collection
    {
        $chantiers = new \App\Structures\Collection\Chantiers();
        
        $chantiers->setChantiers($this->chantierOfCat);
        
        return $chantiers->triByOrder();
    }
    
    
    public function getChildren(): Collection
    {
        return $this->children;
    }
    
    public function getDef(): ?int
    {
        return $this->def;
    }
    
    public function setDef(int $def): self
    {
        $this->def = $def;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): static
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $idChantier): self
    {
        $this->id = $idChantier;
        
        return $this;
    }
    
    public function getIdHordes(): ?int
    {
        return $this->idHordes;
    }
    
    public function setIdHordes(?int $idHordes): self
    {
        $this->idHordes = $idHordes;
        
        return $this;
    }
    
    public function getIdMh(): ?int
    {
        return $this->idMh;
    }
    
    public function setIdMh(?int $idMh): self
    {
        $this->idMh = $idMh;
        
        return $this;
    }
    
    public function getIndes(): ?bool
    {
        return $this->indes;
    }
    
    public function setIndes(bool $indes): self
    {
        $this->indes = $indes;
        
        return $this;
    }
    
    public function getLevelMax(): int
    {
        return $this->levelMax;
    }
    
    public function setLevelMax(int $levelMax): self
    {
        $this->levelMax = $levelMax;
        
        return $this;
    }
    
    public function getLevelUp(int $level): BonusUpChantier|null
    {
        return $this->getLevelUps()->filter(fn(BonusUpChantier $bonusUpChantier) => $bonusUpChantier->getLevel() === $level)
                    ->first() ?: null;
    }
    
    /**
     * @return Collection<BonusUpChantier>
     */
    public function getLevelUps(): Collection
    {
        return $this->levelUps;
    }
    
    public function getNiveau(): ?int
    {
        return $this->niveau;
    }
    
    public function setNiveau(int $niveau): self
    {
        $this->niveau = $niveau;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getOrderByGeneral(): ?int
    {
        return $this->orderByGeneral;
    }
    
    public function setOrderByGeneral(int $orderByGeneral): static
    {
        $this->orderByGeneral = $orderByGeneral;
        
        return $this;
    }
    
    public function getOrderByListing(): ?int
    {
        return $this->orderByListing;
    }
    
    public function setOrderByListing(int $orderByListing): self
    {
        $this->orderByListing = $orderByListing;
        
        return $this;
    }
    
    public function getOrderby(): ?int
    {
        return $this->orderby;
    }
    
    public function setOrderby(int $orderby): self
    {
        $this->orderby = $orderby;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): self
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getParent(): ?self
    {
        return $this->parent;
    }
    
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;
        
        return $this;
    }
    
    public function getPlan(): ?int
    {
        return $this->plan;
    }
    
    public function setPlan(int $plan): self
    {
        $this->plan = $plan;
        
        return $this;
    }
    
    public function getPv(): ?int
    {
        return $this->pv;
    }
    
    public function setPv(int $pv): self
    {
        $this->pv = $pv;
        
        return $this;
    }
    
    /**
     * @return Collection<int, RessourceChantier>
     */
    public function getRessources(): Collection
    {
        return $this->ressources;
    }
    
    public function getRessourcesArray(): array
    {
        return $this->getRessources()->toArray();
    }
    
    public function getRuineBu(): ?bool
    {
        return $this->ruineBu;
    }
    
    public function setRuineBu(bool $ruineBu): self
    {
        $this->ruineBu = $ruineBu;
        
        return $this;
    }
    
    public function getRuineHo(): ?bool
    {
        return $this->ruineHo;
    }
    
    public function setRuineHo(bool $ruineHo): self
    {
        $this->ruineHo = $ruineHo;
        
        return $this;
    }
    
    public function getRuineHs(): ?bool
    {
        return $this->ruineHs;
    }
    
    public function setRuineHs(bool $ruineHs): self
    {
        $this->ruineHs = $ruineHs;
        
        return $this;
    }
    
    public function getTemp(): ?bool
    {
        return $this->temp;
    }
    
    public function setTemp(bool $temp): self
    {
        $this->temp = $temp;
        
        return $this;
    }
    
    public function getUid(): ?string
    {
        return $this->uid;
    }
    
    public function setUid(string $uid): self
    {
        $this->uid = $uid;
        
        return $this;
    }
    
    public function getWater(): ?int
    {
        return $this->water;
    }
    
    public function setWater(int $water): self
    {
        $this->water = $water;
        
        return $this;
    }
    
    public function isActif(): ?bool
    {
        return $this->actif;
    }
    
    public function recupBonusEvolution(int $lvl = 0, int $typeBonus = 0): mixed
    {
        /**
         * @var BonusUpChantier[] $bonusTmp
         */
        $bonusTmp =
            $this->getLevelUps()->filter(fn(BonusUpChantier $bonusUpChantier) => $bonusUpChantier->getLevel() == $lvl)
                 ->toArray();
        
        if (empty($bonusTmp)) {
            return '';
        } else {
            return current($bonusTmp)->getBonusUp($typeBonus)->getValeurUp();
        }
        
    }
    
    public function removeChantierOfCat(self $chantierOfCat): self
    {
        if ($this->chantierOfCat->removeElement($chantierOfCat)) {
            // set the owning side to null (unless already changed)
            if ($chantierOfCat->getCatChantier() === $this) {
                $chantierOfCat->setCatChantier(null);
            }
        }
        
        return $this;
    }
    
    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }
        
        return $this;
    }
    
    public function removeLevelUp(BonusUpChantier $bonusUpChantier): self
    {
        $this->levelUps->removeElement($bonusUpChantier);
        
        return $this;
    }
    
    public function removeRessource(RessourceChantier $ressource): self
    {
        $this->ressources->removeElement($ressource);
        
        return $this;
    }
    
    public function setActif(?bool $actif): self
    {
        $this->actif = $actif;
        
        return $this;
    }

    public function isSpecifiqueVillePrive(): ?bool
    {
        return $this->specifiqueVillePrive;
    }

    public function setSpecifiqueVillePrive(?bool $specifiqueVillePrive): static
    {
        $this->specifiqueVillePrive = $specifiqueVillePrive ?? false;

        return $this;
    }
    
    
}
