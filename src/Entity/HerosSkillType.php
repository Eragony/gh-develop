<?php

namespace App\Entity;

use App\Repository\HerosSkillTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: HerosSkillTypeRepository::class)]
class HerosSkillType
{
    public const ID_HABITANT      = 1;
    public const ID_STRATEGE      = 2;
    public const ID_UNIVERSITAIRE = 3;
    public const ID_PREPARE       = 4;
    public const ID_ENDURANT      = 5;
    public const ID_RECLUS        = 6;
    
    #[ORM\Id]
    #[ORM\Column]
    #[Groups(['admin_heros_list', 'admin_heros_detail', 'ency', 'option', 'citoyens', 'citoyens_skills', 'jump', 'candidature_jump', 'coalition'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 50)]
    #[Groups(['admin_heros_list', 'admin_heros_detail', 'ency', 'option', 'citoyens', 'citoyens_skills', 'jump', 'candidature_jump', 'coalition'])]
    private ?string $name = null;
    
    /**
     * @var Collection<int, HerosSkillLevel>
     */
    #[ORM\OneToMany(mappedBy: 'herosSkillType', targetEntity: HerosSkillLevel::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['admin_heros_detail', 'ency', 'citoyens_skills'])]
    private Collection $level;
    
    public function __construct()
    {
        $this->level = new ArrayCollection();
    }
    
    public function addLevel(HerosSkillLevel $level): static
    {
        if (!$this->level->contains($level)) {
            $this->level->add($level);
            $level->setHerosSkillType($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): HerosSkillType
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return Collection<int, HerosSkillLevel>
     */
    public function getLevel(): Collection
    {
        return $this->level;
    }
    
    public function setLevel(Collection $level): HerosSkillType
    {
        $this->level = $level;
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    public function setName(?string $name): static
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function removeAllLevel(): static
    {
        foreach ($this->level as $level) {
            $this->removeLevel($level);
        }
        
        return $this;
    }
    
    public function removeLevel(HerosSkillLevel &$level): static
    {
        if ($this->level->removeElement($level)) {
            // set the owning side to null (unless already changed)
            if ($level->getHerosSkillType() === $this) {
                $level->setHerosSkillType(null);
            }
        }
        
        return $this;
    }
    
}
