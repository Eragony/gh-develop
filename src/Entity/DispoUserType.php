<?php

namespace App\Entity;

use App\Repository\DispoUserTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DispoUserTypeRepository::class)]
class DispoUserType
{
    #[ORM\Id, ORM\Column(type: 'integer'), Groups(['admin', 'option'])]
    private ?int $id;
    
    #[ORM\Id, ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY', inversedBy: 'dispoType'), ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin', 'option'])]
    private ?int $typeCreneau;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: CreneauHorraire::class, fetch: 'EXTRA_LAZY'), ORM\JoinColumn(nullable: false), Groups(['admin', 'option'])]
    private ?CreneauHorraire $creneau;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeDispo::class, fetch: 'EXTRA_LAZY'), ORM\JoinColumn(nullable: false), Groups(['admin', 'option'])]
    private ?TypeDispo $dispo = null;
    
    public function __construct(int $typeCreneau, CreneauHorraire $creneau)
    {
        $this->setId($typeCreneau * 100 + $creneau->getId());
        $this->setTypeCreneau($typeCreneau);
        $this->setCreneau($creneau);
    }
    
    public function getCreneau(): ?CreneauHorraire
    {
        return $this->creneau;
    }
    
    public function setCreneau(?CreneauHorraire $creneau): self
    {
        $this->creneau = $creneau;
        
        return $this;
    }
    
    public function getDispo(): ?TypeDispo
    {
        return $this->dispo;
    }
    
    public function setDispo(?TypeDispo $dispo): self
    {
        $this->dispo = $dispo;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): DispoUserType
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getTypeCreneau(): ?int
    {
        return $this->typeCreneau;
    }
    
    public function setTypeCreneau(int $typeCreneau): self
    {
        $this->typeCreneau = $typeCreneau;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
}
