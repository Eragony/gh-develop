<?php

namespace App\Entity;

use App\Repository\OutilsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OutilsRepository::class)]
class Outils
{
    
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['outils_expe'])]
    private ?int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, inversedBy: 'outils')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_expe'])]
    private ?int $day;
    
    #[ORM\OneToOne(inversedBy: 'outils', targetEntity: OutilsDecharge::class, cascade: ['persist',
                                                                                        'remove'], fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'decharge_id', referencedColumnName: 'id', nullable: true)]
    private ?OutilsDecharge $outilsDecharge = null;
    
    #[ORM\OneToOne(inversedBy: 'outils',
        targetEntity: OutilsReparation::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'repa_id', referencedColumnName: 'id', nullable: true)]
    private ?OutilsReparation $outilsReparation = null;
    
    #[ORM\OneToOne(inversedBy: 'outils', targetEntity: OutilsVeille::class, cascade: ['persist',
                                                                                      'remove'], fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'veille_id', referencedColumnName: 'id', nullable: true)]
    private ?OutilsVeille $outilsVeille = null;
    
    #[ORM\OneToOne(inversedBy: 'outils', targetEntity: OutilsChantier::class, cascade: ['persist',
                                                                                        'remove'], fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: true)]
    private ?OutilsChantier $outilsChantier = null;
    
    #[ORM\OneToOne(inversedBy: 'outils', cascade: ['persist', 'remove'])]
    #[Groups(['outils_expe'])]
    private ?OutilsExpedition $expedition = null;
    
    public function __construct(Ville $ville, int $day)
    {
        $this->id = $ville->getMapId() * 100 + $day;
        
        $this->setVille($ville)->setDay($day);
    }
    
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
    
    public function getExpedition(): ?OutilsExpedition
    {
        return $this->expedition;
    }
    
    public function setExpedition(?OutilsExpedition $expedition): static
    {
        $this->expedition = $expedition;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getOutilsChantier(): ?OutilsChantier
    {
        return $this->outilsChantier;
    }
    
    public function setOutilsChantier(?OutilsChantier $outilsChantier): self
    {
        $this->outilsChantier = $outilsChantier;
        
        return $this;
    }
    
    public function getOutilsDecharge(): ?OutilsDecharge
    {
        return $this->outilsDecharge;
    }
    
    public function setOutilsDecharge(OutilsDecharge $outilsDecharge): self
    {
        // set the owning side of the relation if necessary
        if ($outilsDecharge->getOutils() !== $this) {
            $outilsDecharge->setOutils($this);
        }
        
        $this->outilsDecharge = $outilsDecharge;
        
        return $this;
    }
    
    public function getOutilsReparation(): ?OutilsReparation
    {
        return $this->outilsReparation;
    }
    
    public function setOutilsReparation(?OutilsReparation $outilsReparation): self
    {
        $this->outilsReparation = $outilsReparation;
        
        return $this;
    }
    
    public function getOutilsVeille(): ?OutilsVeille
    {
        return $this->outilsVeille;
    }
    
    public function setOutilsVeille(?OutilsVeille $outilsVeille): self
    {
        $this->outilsVeille = $outilsVeille;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
}
