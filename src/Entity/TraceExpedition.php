<?php

namespace App\Entity;

use App\Doctrine\IdAlphaTraceExpeditionGenerator;
use App\Repository\TraceExpeditionRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TraceExpeditionRepository::class)]
class TraceExpedition
{
    #[ORM\Id, ORM\GeneratedValue(strategy: 'CUSTOM'), ORM\CustomIdGenerator(class: IdAlphaTraceExpeditionGenerator::class)]
    #[ORM\Column(type: 'string', length: 24), Groups(['admin_expe', 'admin_gen', 'expe', 'outils_expe', 'carte'])]
    private ?string $id = null;
    
    /**
     * @var int[][] $coordonnee
     */
    #[ORM\Column(type: 'json'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private array $coordonnee = [];
    
    #[ORM\Column(type: 'string', length: 255), Groups(['admin_expe', 'admin_gen', 'expe', 'outils_expe'])]
    private ?string $nom = "";
    
    #[ORM\Column(type: 'boolean'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $visible = true;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['admin_expe', 'admin_gen', 'expe', 'outils_expe'])]
    private ?User $createdBy = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?DateTimeInterface $createdAt = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?DateTimeInterface $modifyAt = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $collab = false;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin_expe', 'admin_gen', 'expe', 'outils_expe'])]
    private ?int $jour = 0;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?int $pa = 0;
    
    #[ORM\Column(type: 'string', length: 12), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?string $couleur = "#ffffff";
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'expedition_manuel')]
    #[ORM\JoinColumn(name: 'ville_id', referencedColumnName: 'id', nullable: true), Groups(['admin_gen'])]
    private ?Ville $ville = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false]), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $personnel = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false]), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $inactive = null;
    
    #[ORM\OneToOne(mappedBy: 'trace', cascade: ['persist', 'remove']), Groups(['expe'])]
    private ?ExpeditionPart $expeditionPart = null;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $brouillon = false;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $biblio = false;
    
    #[ORM\Column(options: ['default' => false]), Groups(['admin_expe', 'expe', 'outils_expe'])]
    private ?bool $traceExpedition = false;
    
    public function getCollab(): ?bool
    {
        return $this->collab;
    }
    
    public function setCollab(bool $collab): self
    {
        $this->collab = $collab;
        
        return $this;
    }
    
    public function getCoordonnee(): ?array
    {
        return $this->coordonnee;
    }
    
    public function setCoordonnee(array $coordonnee): self
    {
        $this->coordonnee = $coordonnee;
        
        return $this;
    }
    
    public function getCouleur(): ?string
    {
        return $this->couleur;
    }
    
    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getExpeditionPart(): ?ExpeditionPart
    {
        return $this->expeditionPart;
    }
    
    public function setExpeditionPart(?ExpeditionPart $expeditionPart): static
    {
        // unset the owning side of the relation if necessary
        if ($expeditionPart === null && $this->expeditionPart !== null) {
            $this->expeditionPart->setTrace(null);
        }
        
        // set the owning side of the relation if necessary
        if ($expeditionPart !== null && $expeditionPart->getTrace() !== $this) {
            $expeditionPart->setTrace($this);
        }
        
        $this->expeditionPart = $expeditionPart;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): TraceExpedition
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    public function setJour(int $jour): self
    {
        $this->jour = $jour;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->modifyAt;
    }
    
    public function setModifyAt(?DateTimeInterface $modifyAt): self
    {
        $this->modifyAt = $modifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): self
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): self
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getVisible(): ?bool
    {
        return $this->visible;
    }
    
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;
        
        return $this;
    }
    
    public function isBiblio(): ?bool
    {
        return $this->biblio;
    }
    
    public function isBrouillon(): ?bool
    {
        return $this->brouillon;
    }
    
    public function isInactive(): ?bool
    {
        return $this->inactive;
    }
    
    public function isPersonnel(): ?bool
    {
        return $this->personnel;
    }
    
    public function isTraceExpedition(): ?bool
    {
        return $this->traceExpedition;
    }
    
    public function setBiblio(bool $biblio): static
    {
        $this->biblio = $biblio;
        
        return $this;
    }
    
    public function setBrouillon(bool $brouillon): static
    {
        $this->brouillon = $brouillon;
        
        return $this;
    }
    
    public function setInactive(?bool $inactive): static
    {
        $this->inactive = $inactive;
        
        return $this;
    }
    
    public function setPersonnel(?bool $personnel): static
    {
        $this->personnel = $personnel;
        
        return $this;
    }
    
    public function setTraceExpedition(bool $traceExpedition): static
    {
        $this->traceExpedition = $traceExpedition;
        
        return $this;
    }
}
