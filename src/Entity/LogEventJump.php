<?php

namespace App\Entity;

use App\Repository\LogEventJumpRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LogEventJumpRepository::class)]
class LogEventJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['gestion_jump'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['gestion_jump'])]
    private ?User $declencheur = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['gestion_jump'])]
    private ?string $libelle = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['gestion_jump'])]
    private ?DateTimeInterface $eventAt = null;
    
    #[ORM\Column(length: 255, nullable: true), Groups(['gestion_jump'])]
    private ?string $valeurAvant = null;
    
    #[ORM\Column(length: 255, nullable: true), Groups(['gestion_jump'])]
    private ?string $valeurApres = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY', inversedBy: 'logEvent')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jump $jump = null;
    
    /**
     * @var int|null
     * Valeur possible :
     * 0 - traduction texte
     */
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0])]
    private ?int $typologie = 0;
    
    
    public function getDeclencheur(): ?User
    {
        return $this->declencheur;
    }
    
    public function setDeclencheur(?User $declencheur): self
    {
        $this->declencheur = $declencheur;
        
        return $this;
    }
    
    public function getEventAt(): ?DateTimeInterface
    {
        return $this->eventAt;
    }
    
    public function setEventAt(DateTimeInterface $eventAt): self
    {
        $this->eventAt = $eventAt;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): LogEventJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(?Jump $jump): static
    {
        $this->jump = $jump;
        
        return $this;
    }
    
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }
    
    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;
        
        return $this;
    }
    
    public function getTypologie(): ?int
    {
        return $this->typologie;
    }
    
    public function setTypologie(int $typologie): static
    {
        $this->typologie = $typologie;
        
        return $this;
    }
    
    public function getValeurApres(): ?string
    {
        return $this->valeurApres;
    }
    
    public function setValeurApres(?string $valeurApres): static
    {
        $this->valeurApres = $valeurApres;
        
        return $this;
    }
    
    public function getValeurAvant(): ?string
    {
        return $this->valeurAvant;
    }
    
    public function setValeurAvant(?string $valeurAvant): static
    {
        $this->valeurAvant = $valeurAvant;
        
        return $this;
    }
    
}
