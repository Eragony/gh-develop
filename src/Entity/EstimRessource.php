<?php

namespace App\Entity;

use App\Repository\EstimRessourceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: EstimRessourceRepository::class)]
class EstimRessource
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['outils_chantier'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['outils_chantier'])]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nombre = null;
    
    #[ORM\ManyToOne(targetEntity: OutilsChantier::class, fetch: 'EXTRA_LAZY', inversedBy: 'estimRessources')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OutilsChantier $outilsChantier = null;
    
    #[ORM\Column(length: 50), Groups(['outils_chantier'])]
    private ?string $uuid = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): EstimRessource
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getOutilsChantier(): ?OutilsChantier
    {
        return $this->outilsChantier;
    }
    
    public function setOutilsChantier(?OutilsChantier $outilsChantier): self
    {
        $this->outilsChantier = $outilsChantier;
        
        return $this;
    }
    
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
    
    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;
        
        return $this;
    }
}
