<?php

namespace App\Entity;

use App\Repository\ZoneMapRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ZoneMapRepository::class)]
class ZoneMap
{
    
    public const VILLE                = -2;
    public const NON_EXPLO            = -1;
    public const CASE_VUE             = 1;
    public const CASE_NONVUE          = 0;
    public const VUE_AUJ              = 2;
    public const VUE_24H              = 3;
    public const VUE_48H              = 4;
    public const DIRECTION_VILLE      = 0;
    public const DIRECTION_NORD       = 1;
    public const DIRECTION_EST        = 2;
    public const DIRECTION_SUD        = 4;
    public const DIRECTION_OUEST      = 8;
    public const DIRECTION_NORD_EST   = 3;
    public const DIRECTION_SUD_EST    = 6;
    public const DIRECTION_SUD_OUEST  = 12;
    public const DIRECTION_NORD_OUEST = 9;
    public const Bordure_n            = 1;
    public const Bordure_e            = 2;
    public const Bordure_ne           = 3;
    public const Bordure_s            = 4;
    public const Bordure_ns           = 5;
    public const Bordure_se           = 6;
    public const Bordure_nes          = 7;
    public const Bordure_o            = 8;
    public const Bordure_no           = 9;
    public const Bordure_eo           = 10;
    public const Bordure_neo          = 11;
    public const Bordure_so           = 12;
    public const Bordure_nso          = 13;
    public const Bordure_seo          = 14;
    public const Bordure_cne          = 23;
    public const Bordure_cse          = 26;
    public const Bordure_cno          = 29;
    public const Bordure_cso          = 32;
    
    #[ORM\Id]
    #[ORM\Column(type: 'bigint'), Groups(['admin_ville_bat', 'admin_gen', 'comparatif'])]
    private ?string $id;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin_ville_bat', 'admin_gen', 'carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private int $x;
    
    #[ORM\Column(type: 'smallint'), Groups(['admin_ville_bat', 'admin_gen', 'carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private int $y;
    
    private ?int $idPartiel = 0;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'zone'), Groups(['admin_gen'])]
    private ?Ville $ville;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte', 'comparatif'])]
    private ?int $zombie = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte'])]
    private ?int $pdc = null;
    
    #[ORM\Column(type: 'boolean', nullable: true), Groups(['carte'])]
    private ?bool $dried = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte', 'comparatif'])]
    private ?int $danger = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte'])]
    private ?int $tag = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte'])]
    private ?int $day = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'citoyen_id', referencedColumnName: 'id', nullable: true), Groups(['carte'])]
    private ?User $citoyen = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['carte', 'comparatif'])]
    private ?int $vue = null;
    
    #[ORM\Column(type: 'string', length: 15, nullable: true), Groups(['carte'])]
    private ?string $heureMaj = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: BatPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'bat_id', referencedColumnName: 'id', nullable: true),
        Groups(['admin_ville_bat', 'admin_gen', 'carte', 'comparatif'])]
    private ?BatPrototype $bat = null;
    
    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false]), Groups(['admin_ville_bat', 'carte', 'comparatif'])]
    private bool $camped = false;
    
    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false]), Groups(['admin_ville_bat', 'carte', 'comparatif'])]
    private bool $empty = false;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['carte', 'comparatif'])]
    private ?int $dig = null;
    
    /** @var Collection<MapItem> */
    #[ORM\OneToMany(mappedBy: 'zone', targetEntity: MapItem::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true), Groups(['carte'])]
    private Collection $items;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: BatPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'bat_id_hyp', referencedColumnName: 'id', nullable: true),
        Groups(['admin_ville_bat', 'carte'])]
    private ?BatPrototype $batHypothese = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $zombieMin = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $zombieMax = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $marqueurMajDay = null;
    
    #[ORM\Column(type: 'string', length: 15, nullable: true), Groups(['carte'])]
    private ?string $marqueurMajHeure = null;
    
    #[ORM\ManyToOne, Groups(['carte'])]
    private ?User $marqueurMajBy = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $lvlBalisage = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['carte'])]
    private ?int $km = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['carte'])]
    private ?int $pa = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private ?int $xRel = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private ?int $yRel = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $bordKm = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $bordScrut = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $bordPa = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $bordZonage = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $direction = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true), Groups(['carte'])]
    private ?int $zone = null;
    
    /** @var Collection<ConsigneExpedition> */
    #[ORM\OneToMany(mappedBy: 'zone', targetEntity: ConsigneExpedition::class), Groups(['carte'])]
    private Collection $consigneExpeditions;
    
    private ?string $indicVisite = null;
    
    private ?int  $zombieEstim  = 0;
    private ?bool $marqueurOnly = false;
    /**
     * @var int[] $carteAlter
     */
    private array $carteAlter = [];
    private ?int  $carteScrut = 0;
    /**
     * @var string[] $itemSol
     */
    private array $itemSol = [];
    /**
     * @var string[] $itemSolIcone
     */
    private array $itemSolIcone = [];
    
    #[ORM\Column(nullable: true)]
    private ?DateTimeImmutable $majAt = null;
    
    public function __construct()
    {
        $this->items               = new ArrayCollection();
        $this->consigneExpeditions = new ArrayCollection();
    }
    
    public function addConsigneExpedition(ConsigneExpedition $consigneExpedition): static
    {
        if (!$this->consigneExpeditions->contains($consigneExpedition)) {
            $this->consigneExpeditions->add($consigneExpedition);
            $consigneExpedition->setZone($this);
        }
        
        return $this;
    }
    
    public function addItem(MapItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setZone($this);
        }
        
        return $this;
    }
    
    public function calculKm(): int
    {
        return (int)round(sqrt($this->xNorm() ** 2 + $this->yNorm() ** 2));
    }
    
    public function createId(int $x, int $y, int $mapId): self
    {
        $this->id = $mapId * 10000 + $y * 100 + $x;
        return $this;
    }
    
    public function getBat(): ?BatPrototype
    {
        return $this->bat;
    }
    
    public function setBat(?BatPrototype $bat): self
    {
        $this->bat = $bat;
        
        return $this;
    }
    
    public function getBatHypothese(): ?BatPrototype
    {
        return $this->batHypothese;
    }
    
    public function setBatHypothese(?BatPrototype $batHypothese): self
    {
        $this->batHypothese = $batHypothese;
        
        return $this;
    }
    
    public function getBordKm(): ?int
    {
        return $this->bordKm;
    }
    
    public function setBordKm(?int $bordKm): static
    {
        $this->bordKm = $bordKm;
        
        return $this;
    }
    
    public function getBordPa(): ?int
    {
        return $this->bordPa;
    }
    
    public function setBordPa(?int $bordPa): static
    {
        $this->bordPa = $bordPa;
        
        return $this;
    }
    
    public function getBordScrut(): ?int
    {
        return $this->bordScrut;
    }
    
    public function setBordScrut(?int $bordScrut): static
    {
        $this->bordScrut = $bordScrut;
        
        return $this;
    }
    
    public function getBordZonage(): ?int
    {
        return $this->bordZonage;
    }
    
    public function setBordZonage(?int $bordZonage): static
    {
        $this->bordZonage = $bordZonage;
        
        return $this;
    }
    
    /**
     * @return int[]
     */
    #[Groups(['carte'])]
    public function getCarteAlter(): array
    {
        return $this->carteAlter;
    }
    
    /**
     * @param int[] $carteAlter
     * @return ZoneMap
     */
    public function setCarteAlter(array $carteAlter): ZoneMap
    {
        $this->carteAlter = $carteAlter;
        return $this;
    }
    
    /**
     * @return int|null
     */
    #[Groups(['carte'])]
    public function getCarteScrut(): ?int
    {
        return $this->carteScrut;
    }
    
    /**
     * @param int|null $carteScrut
     * @return ZoneMap
     */
    public function setCarteScrut(?int $carteScrut): ZoneMap
    {
        $this->carteScrut = $carteScrut;
        return $this;
    }
    
    public function getCitoyen(): ?User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getConsigneExpeditions(): Collection
    {
        return $this->consigneExpeditions;
    }
    
    #[Groups(['admin_gen'])]
    public function getCoords(): string
    {
        return "{$this->xNorm()}/{$this->yNorm()}";
    }
    
    public function getDanger(): ?int
    {
        return $this->danger;
    }
    
    public function setDanger(?int $danger): self
    {
        $this->danger = $danger;
        
        return $this;
    }
    
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    public function setDay(?int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
    
    public function getDig(): ?int
    {
        return $this->dig;
    }
    
    public function setDig(?int $dig): self
    {
        $this->dig = $dig;
        
        return $this;
    }
    
    public function getDirection(): ?int
    {
        return $this->direction;
    }
    
    public function setDirection(?int $direction): static
    {
        $this->direction = $direction;
        
        return $this;
    }
    
    public function getDried(): ?bool
    {
        return $this->dried;
    }
    
    public function setDried(?bool $dried): self
    {
        $this->dried = $dried;
        
        return $this;
    }
    
    public function getHeureMaj(): ?string
    {
        return $this->heureMaj;
    }
    
    public function setHeureMaj(?string $heureMaj): ZoneMap
    {
        $this->heureMaj = $heureMaj;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdPartiel(): ?int
    {
        if ($this->idPartiel === 0) {
            $this->idPartiel = $this->y * 100 + $this->x;
        }
        return $this->idPartiel;
    }
    
    public function setIdPartiel(?int $idPartiel): ZoneMap
    {
        if ($idPartiel !== null) {
            $idPartiel = $this->y * 100 + $this->x;
        }
        $this->idPartiel = $idPartiel;
        return $this;
    }
    
    /**
     * @return string|null
     */
    #[Groups(['carte'])]
    public function getIndicVisite(): ?string
    {
        return $this->indicVisite;
    }
    
    /**
     * @param string|null $indicVisite
     * @return ZoneMap
     */
    public function setIndicVisite(?string $indicVisite): ZoneMap
    {
        $this->indicVisite = $indicVisite;
        return $this;
    }
    
    /**
     * @return string[]
     */
    #[Groups(['carte'])]
    public function getItemSol(): array
    {
        return $this->itemSol;
    }
    
    /**
     * @param string[] $itemSol
     * @return ZoneMap
     */
    public function setItemSol(array $itemSol): ZoneMap
    {
        $this->itemSol = $itemSol;
        return $this;
    }
    
    /**
     * @return string[]
     */
    #[Groups(['carte'])]
    public function getItemSolIcone(): array
    {
        return $this->itemSolIcone;
    }
    
    /**
     * @param string[] $itemSolIcone
     * @return ZoneMap
     */
    public function setItemSolIcone(array $itemSolIcone): ZoneMap
    {
        $this->itemSolIcone = $itemSolIcone;
        return $this;
    }
    
    /**
     * @return Collection<int,MapItem>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function setItems(Collection $mapItems): self
    {
        $this->items = $mapItems;
        return $this;
    }
    
    public function getKm(): ?int
    {
        return $this->km;
    }
    
    public function setKm(int $km): static
    {
        $this->km = $km;
        
        return $this;
    }
    
    public function getLvlBalisage(): ?int
    {
        return $this->lvlBalisage;
    }
    
    public function setLvlBalisage(?int $lvlBalisage): static
    {
        $this->lvlBalisage = $lvlBalisage;
        
        return $this;
    }
    
    public function getMajAt(): ?DateTimeImmutable
    {
        return $this->majAt;
    }
    
    public function setMajAt(?DateTimeImmutable $majAt): static
    {
        $this->majAt = $majAt;
        
        return $this;
    }
    
    public function getMarqueurMajBy(): ?User
    {
        return $this->marqueurMajBy;
    }
    
    public function setMarqueurMajBy(?User $marqueurMajBy): static
    {
        $this->marqueurMajBy = $marqueurMajBy;
        
        return $this;
    }
    
    public function getMarqueurMajDay(): ?int
    {
        return $this->marqueurMajDay;
    }
    
    public function setMarqueurMajDay(?int $marqueurMajDay): ZoneMap
    {
        $this->marqueurMajDay = $marqueurMajDay;
        return $this;
    }
    
    public function getMarqueurMajHeure(): ?string
    {
        return $this->marqueurMajHeure;
    }
    
    public function setMarqueurMajHeure(?string $marqueurMajHeure): ZoneMap
    {
        $this->marqueurMajHeure = $marqueurMajHeure;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    #[Groups(['carte'])]
    public function getMarqueurOnly(): ?bool
    {
        return $this->marqueurOnly;
    }
    
    /**
     * @param bool|null $marqueurOnly
     * @return ZoneMap
     */
    public function setMarqueurOnly(?bool $marqueurOnly): ZoneMap
    {
        $this->marqueurOnly = $marqueurOnly;
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): static
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getPdc(): ?int
    {
        return $this->pdc;
    }
    
    public function setPdc(?int $pdc): self
    {
        $this->pdc = $pdc;
        
        return $this;
    }
    
    #[Groups(['carte'])]
    public function getPlan(): ?string
    {
        if ($this->getBat() === null || $this->getBat()->isExplorable()) {
            return null;
        }
        
        return $this->getKm() > 9 ? 'planBleu' : 'planJaune';
    }
    
    #[Groups(['carte'])]
    public function getStatusBat(): ?string
    {
        if ($this->getBat() === null) {
            return null;
        }
        
        $status = 'bat';
        
        if ($this->getBat()->isExplorable()) {
            $status = 'bat-r';
        } else if ($this->getDig() > 0) {
            $status = 'bat-s';
        }
        
        return $status;
    }
    
    public function getTag(): ?int
    {
        return $this->tag;
    }
    
    public function setTag(?int $tag): self
    {
        $this->tag = $tag;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getVue(): ?int
    {
        return $this->vue;
    }
    
    public function setVue(int $vue): self
    {
        $this->vue = $vue;
        
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(int $x): self
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getXRel(): ?int
    {
        return $this->xRel;
    }
    
    public function setXRel(int $xRel): static
    {
        $this->xRel = $xRel;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(int $y): self
    {
        $this->y = $y;
        
        return $this;
    }
    
    public function getYRel(): ?int
    {
        return $this->yRel;
    }
    
    public function setYRel(int $yRel): static
    {
        $this->yRel = $yRel;
        
        return $this;
    }
    
    public function getZombie(): ?int
    {
        return $this->zombie;
    }
    
    public function setZombie(?int $zombie): self
    {
        $this->zombie = $zombie;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    #[Groups(['carte'])]
    public function getZombieEstim(): ?int
    {
        return $this->zombieEstim;
    }
    
    /**
     * @param int|null $zombieEstim
     * @return ZoneMap
     */
    public function setZombieEstim(?int $zombieEstim): ZoneMap
    {
        $this->zombieEstim = $zombieEstim;
        return $this;
    }
    
    public function getZombieMax(): ?int
    {
        return $this->zombieMax;
    }
    
    public function setZombieMax(?int $zombieMax): static
    {
        $this->zombieMax = $zombieMax;
        
        return $this;
    }
    
    public function getZombieMin(): ?int
    {
        return $this->zombieMin;
    }
    
    public function setZombieMin(?int $zombieMin): static
    {
        $this->zombieMin = $zombieMin;
        
        return $this;
    }
    
    public function getZone(): ?int
    {
        return $this->zone;
    }
    
    public function setZone(int $zone): static
    {
        $this->zone = $zone;
        
        return $this;
    }
    
    public function isCamped(): bool
    {
        return $this->camped;
    }
    
    public function setCamped(bool $camped): self
    {
        $this->camped = $camped;
        
        return $this;
    }
    
    public function isEmpty(): bool
    {
        return $this->empty;
    }
    
    public function setEmpty(bool $empty): self
    {
        $this->empty = $empty;
        
        return $this;
    }
    
    public function removeConsigneExpedition(ConsigneExpedition $consigneExpedition): static
    {
        if ($this->consigneExpeditions->removeElement($consigneExpedition)) {
            // set the owning side to null (unless already changed)
            if ($consigneExpedition->getZone() === $this) {
                $consigneExpedition->setZone(null);
            }
        }
        
        return $this;
    }
    
    public function removeItem(MapItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getZone() === $this) {
                $item->setZone(null);
            }
        }
        
        return $this;
    }
    
    public function xNorm(): int
    {
        return $this->getX() - $this->getVille()->getPosX();
    }
    
    public function yNorm(): int
    {
        return $this->getVille()->getPosY() - $this->getY();
    }
    
}
