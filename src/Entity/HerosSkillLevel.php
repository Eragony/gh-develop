<?php

namespace App\Entity;

use App\Enum\LevelSkill;
use App\Repository\HerosSkillLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: HerosSkillLevelRepository::class)]
class HerosSkillLevel
{
    #[ORM\Id]
    #[ORM\Column]
    #[Groups(['admin_heros_detail', 'ency', 'option', 'citoyens', 'citoyens_skills','jump','candidature_jump', 'coalition'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 50)]
    #[Groups(['admin_heros_detail', 'ency', 'option', 'citoyens', 'citoyens_skills','jump','candidature_jump', 'coalition'])]
    private ?string $name = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['admin_heros_detail', 'ency', 'option', 'citoyens'])]
    private ?int $xp = null;
    
    #[ORM\ManyToOne(inversedBy: 'level')]
    #[ORM\JoinColumn(nullable: false), Groups(['option', 'citoyens', 'citoyens_skills','jump','candidature_jump', 'coalition'])]
    private ?HerosSkillType $herosSkillType = null;
    
    /**
     * @var Collection<int, HerosSkillPrototype>
     */
    #[ORM\OneToMany(mappedBy: 'herosSkillLevel', targetEntity: HerosSkillPrototype::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['admin_heros_detail', 'ency', 'option', 'citoyens','candidature_jump','jump', 'coalition'])]
    private Collection $pouvoir;
    
    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'skill')]
    private Collection $users;
    
    #[ORM\Column(type: Types::SMALLINT, enumType: LevelSkill::class), Groups(['admin_heros_detail', 'ency', 'option', 'citoyens', 'citoyens_skills','jump','candidature_jump', 'coalition'])]
    private ?LevelSkill $lvl = LevelSkill::HABITANT;
    
    public function __construct()
    {
        $this->pouvoir = new ArrayCollection();
        $this->users   = new ArrayCollection();
    }
    
    public function addPouvoir(HerosSkillPrototype $pouvoir): static
    {
        if (!$this->pouvoir->contains($pouvoir)) {
            $this->pouvoir->add($pouvoir);
            $pouvoir->setHerosSkillLevel($this);
        }
        
        return $this;
    }
    
    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addSkill($this);
        }
        
        return $this;
    }
    
    public function getHerosSkillType(): ?HerosSkillType
    {
        return $this->herosSkillType;
    }
    
    public function setHerosSkillType(?HerosSkillType $herosSkillType): static
    {
        $this->herosSkillType = $herosSkillType;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): HerosSkillLevel
    {
        $this->id = $id;
        return $this;
    }
    
    public function getLvl(): ?LevelSkill
    {
        return $this->lvl;
    }
    
    public function setLvl(LevelSkill $lvl): static
    {
        $this->lvl = $lvl;
        
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    public function setName(string $name): static
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return Collection<int, HerosSkillPrototype>
     */
    public function getPouvoir(): Collection
    {
        return $this->pouvoir;
    }
    
    public function setPouvoir(Collection $pouvoir): HerosSkillLevel
    {
        $this->pouvoir = $pouvoir;
        return $this;
    }
    
    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }
    
    public function getXp(): ?int
    {
        return $this->xp;
    }
    
    public function setXp(?int $xp): static
    {
        $this->xp = $xp;
        
        return $this;
    }
    
    public function removeAllPouvoir(): static
    {
        foreach ($this->pouvoir as $pouvoir) {
            $this->removePouvoir($pouvoir);
        }
        
        return $this;
    }
    
    public function removePouvoir(HerosSkillPrototype &$pouvoir): static
    {
        if ($this->pouvoir->removeElement($pouvoir)) {
            // set the owning side to null (unless already changed)
            if ($pouvoir->getHerosSkillLevel() === $this) {
                $pouvoir->setHerosSkillLevel(null);
            }
        }
        
        return $this;
    }
    
    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            $user->removeSkill($this);
        }
        
        return $this;
    }
    
}
