<?php


namespace App\Doctrine;


use App\Entity\OutilsVeille;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class IdAlphaOutilsVeilleGenerator extends AbstractIdGenerator
{
    
    /**
     * @inheritDoc
     */
    public function generateId(EntityManagerInterface $em, $entity): string
    {
        $listAlpha     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $uniqueIdFound = false;
        
        do {
            $alphaSuffle = str_shuffle($listAlpha);
            $id          = substr($alphaSuffle, 0, 24);
            
            if (null === $em->getRepository(OutilsVeille::class)->findOneBy(['id' => $id])) {
                $uniqueIdFound = true;
            }
        } while (!$uniqueIdFound);
        
        return $id;
        
    }
}