<?php

namespace App\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class IdItemGenerator extends AbstractIdGenerator
{
    
    public function generateId(EntityManagerInterface $em, $entity): int
    {
        return ($entity->getItem()->getId()) * 10 + ($entity->getBroked()) ? 1 : 0;
    }
}