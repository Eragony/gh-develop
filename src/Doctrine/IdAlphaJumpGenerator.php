<?php


namespace App\Doctrine;


use App\Entity\Jump;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class IdAlphaJumpGenerator extends AbstractIdGenerator
{
    
    /**
     * @inheritDoc
     */
    public function generateId(EntityManagerInterface $em, $entity): string
    {
        $listAlpha     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $uniqueIdFound = false;
        
        do {
            $alphaSuffle = str_shuffle($listAlpha);
            $id          = substr($alphaSuffle, 0, 24);
            
            if (null === $em->getRepository(Jump::class)->findOneBy(['id' => $id])) {
                $uniqueIdFound = true;
            }
        } while (!$uniqueIdFound);
        
        return $id;
        
    }
}