<?php

namespace App\Service;

use JsonException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MyHordesApi
{
    private string $appKey;
    private string $urlApi;
    
    public function __construct(private ContainerBagInterface $containerBag)
    {
        $this->appKey = $_ENV['OAUTH_MH'];
        $this->urlApi = $_ENV['URL_API_MH'];
    }
    
    public function getBats(string $keyUser): string
    {
        $url = $this->urlApi . 'ruins?fields=id,name,img,desc,explorable&languages=fr&appkey=' . $this->appKey .
               '&userkey=' . $keyUser . '';
        
        return $this->jsonCall($url);
    }
    
    public function getChantiers(string $keyUser): string
    {
        $url = $this->urlApi .
               'buildings?fields=id,name,img,resources,uid,parent,temporary,rarity,def,breakable,maxLife,pa,desc&languages=fr&appkey=' .
               $this->appKey . '&userkey=' . $keyUser . '';
        
        return $this->jsonCall($url);
    }
    
    public function getItems(string $keyUser): string
    {
        $url = $this->urlApi . 'items?fields=id,name,img,guard,desc,heavy,cat, deco&languages=fr&appkey=' . $this->appKey .
               '&userkey=' . $keyUser . '';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getMeBasicInfo(string $keyUser): string
    {
        $url =
            $this->urlApi . 'me?fields=id,name,avatar,locale,mapId&appkey=' . $this->appKey . '&userkey=' . $keyUser .
            '';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getMeGazette(string $keyUser): string
    {
        $fields =
            'map.fields(city.fields(news.fields(z,def,content,regenDir,water)))';
        $url    =
            $this->urlApi . 'me?fields=' . $fields . '&appkey=' . $this->appKey . '&userkey=' . $keyUser . '&languages=fr,es,en,de';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getMeOldTownPictos(string $keyUser): string
    {
        $fields =
            'id,rewards.fields(id,number,titles),playedMaps.fields(id,twinId, sp ,etwinId,mapId,type,hard,survival,day,avatar,name,mapName,origin,season,phase,dtype,v1,score,msg,comment,cleanup.fields(user,type),rewards)';
        $url    =
            $this->urlApi . 'me?fields=' . $fields . '&appkey=' . $this->appKey . '&userkey=' . $keyUser . '&lang=fr';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getMeOldTownPictosAllTown(string $keyUser): string
    {
        $fields =
            'id,map.fields(id,citizens.fields(id,playedMaps.fields(id,twinId,sp, etwinId,mapId,type,hard,survival,day,avatar,name,mapName,origin,season,phase,dtype,v1,score,msg,comment,cleanup.fields(user,type),rewards.fields(id,rare,number,img,name,desc,titles,comments))),cadavers.fields(id,twinId,etwinId,mapId,survival,day,avatar,name,mapName,season,v1,origin,score,season,dtype,msg,comment,sp,cleanup.fields(user,type),rewards.fields(id,number,titles))),rewards.fields(id,number,titles),playedMaps.fields(id,twinId,etwinId,mapId,type,hard,survival,day,avatar,name,mapName,origin,season,phase,dtype,v1,score,msg,comment,cleanup.fields(user,type),rewards.fields(id,number,titles))';
        $url    =
            $this->urlApi . 'me?fields=' . $fields . '&appkey=' . $this->appKey . '&userkey=' . $keyUser . '&lang=fr';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getMeTown(string $keyUser): string
    {
        $fields =
            'id,locale,name,isGhost,twinId,etwinId,mapId,homeMessage,avatar,hero,dead,out,baseDef,ban,x,y,map.fields(id,date,wid,hei,conspiracy,days,season,phase,source,bonusPts,guide,shaman,custom,language,citizens.fields(id,twinId,etwinId,name,locale,avatar,isGhost,homeMessage,hero,dead,out,ban,baseDef,x,y,mapId,job.fields(uid)),city.fields(name,door,water,chaos,devast,hard,type,x,y,chantiers.fields(id,actions),buildings.fields(id,def,life),news.fields(z,def,water,content,regenDir),defense.fields(total,base,buildings,upgrades,items,itemsMul,citizenHomes,citizenGuardians,watchmen,souls,temp,cadavers,bonus),upgrades.fields(total,list.fields(level,buildingId)),estimations.fields(days,min,max,maxed),estimationsNext.fields(days,min,max,maxed),bank.fields(id,count,broken)),espeditions.fields(author.fields(id),name,length,points.fields(x,y)),zones.fields(x,y,nvt,tag,danger,details.fields(z,h,dried),building.fields(dig,type,camped,dried),items.fields(id,count,broken)),cadavers.fields(id,twinId,etwinId,survival,day,avatar,name,mapName,season,v1,origin,score,season,dtype,msg,comment,sp,cleanup.fields(user,type))),job.fields(uid)';
        $url    =
            $this->urlApi . 'me?fields=' . $fields . '&appkey=' . $this->appKey . '&userkey=' . $keyUser . '&lang=fr';
        
        return $this->jsonCall($url);
    }
    
    public function getPictos(string $keyUser): string
    {
        $url = $this->urlApi . 'pictos?fields=id,name,img,desc,rare,community&languages=fr&appkey=' . $this->appKey .
               '&userkey=' . $keyUser . '';
        
        return $this->jsonCall($url);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function isAttack(string $keyUser, $update = true): bool
    {
        if ($update) {
            $url = $this->urlApi . 'status?appkey=' . $this->appKey . '&userkey=' . $keyUser . '';
            
            $json = $this->jsonCall($url);
            
            $decodeJson = json_decode($json);
            $isAttack   = (json_last_error() == JSON_ERROR_NONE) ? $decodeJson->attack : false;
        } else {
            $isAttack = true;
        }
        
        return $isAttack;
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function isAttackOrMaintenance(string $keyUser, $update = true): bool
    {
        
        if ($update) {
            $url = $this->urlApi . 'status?appkey=' . $this->appKey . '&userkey=' . $keyUser . '';
            
            $json = $this->jsonCall($url);
            
            $decodeJson = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            
            
            if (isset($decodeJson['error'])) {
                return false;
            }
            
            $isAttack   = $decodeJson['attack'] ?? false;
            $isMaintain = $decodeJson['maintain'] ?? false;
        } else {
            $isAttack   = true;
            $isMaintain = true;
        }
        
        
        return ($isMaintain && $isAttack);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function isMaintenance(string $keyUser, $update = true): bool
    {
        if ($update) {
            $url = $this->urlApi . 'status?appkey=' . $this->appKey . '&userkey=' . $keyUser . '';
            
            $json = $this->jsonCall($url);
            
            json_decode($json);
            
            $isMaintain = !(json_last_error() == JSON_ERROR_NONE);
        } else {
            $isMaintain = true;
        }
        
        return $isMaintain;
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    private function jsonCall(string $url): string
    {
        $client = new CurlHttpClient();
        
        return $client->request(
            'GET',
            $url,
        )->getContent();
        
        //return file_get_contents($url);
    }
    
    /**
     * Pour une auth sur le stagging
     * private function jsonCall(string $url): string
     * {
     * $client = new CurlHttpClient();
     *
     * $auth = base64_encode($_ENV['username'] . ':' . $_ENV['password']);
     *
     * return $client->request(
     * 'GET',
     * $url,
     * [
     * 'headers' => [
     * 'Authorization' => 'Basic ' . $auth
     * ]
     * ]
     * )->getContent();
     *
     * //return file_get_contents($url);
     * }
     */
    
}