<?php


namespace App\Service\Ville;


use App\Entity\Banque;
use App\Entity\BatPrototype;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Citoyens as Citoyen;
use App\Entity\Defense;
use App\Entity\Estimation;
use App\Entity\ExpeHordes;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\MapItem;
use App\Entity\Ruines;
use App\Entity\TraceExpedition;
use App\Entity\TypeObjet;
use App\Entity\UpChantier;
use App\Entity\User;
use App\Entity\UserPersonnalisation;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Structures\Collection\Citoyens;
use App\Structures\Dto\GH\Ville\Carte\SomItemSolDTO;
use App\Utils\ZoneMapHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Exception;
use JsonException;
use Symfony\Contracts\Translation\TranslatorInterface;


readonly class CarteHandler
{
    
    public function __construct(
        private EntityManagerInterface $em,
        private TranslatorInterface    $translator,
        private TranslateHandler       $translateHandler,
        private GestHordesHandler      $gh,
        private UpGradeHandler         $upGrade,
        private ExpeditionHandler      $expedition,
        private UserHandler            $userHandler,
        private LocalFormatterUtils    $lfu, private SerializerService $serializerService,
    )
    {
    }
    
    /**
     * This function is used to get the maximum zone case from an array of zones that is greater than or equal to a specified value (needle).
     * If no such zone is found, it returns a default value.
     *
     * @param int $needle The value to compare each zone against.
     * @param array $arrayZone The array of zones to search in.
     * @param int $valeurParDefaut The default value to return if no zone is found that is greater than or equal to the needle.
     * @return int The maximum zone case that is greater than or equal to the needle, or the default value if no such zone is found.
     */
    public function getZoneMaxCase(int $needle, array $arrayZone, int $valeurParDefaut): int
    {
        foreach ($arrayZone as $zone) {
            if ($zone >= $needle) {
                return $zone;
            }
        }
        return $valeurParDefaut;
    }
    
    public function recupIndicVisit(int $jourVille, int $jourCase, int $vue): ?string
    {
        $indic = null;
        
        if ($vue === ZoneMap::CASE_NONVUE || $vue === ZoneMap::CASE_VUE) {
            if ($jourCase !== 0) {
                $diffDay = abs($jourVille - $jourCase);
                $indic   = match ($diffDay) {
                    0       => 'vueAuj',
                    1       => 'vue24',
                    default => 'vue48',
                };
            }
        }
        
        return $indic;
    }
    
    /**
     * @throws JsonException
     * @throws Exception
     */
    public function recupInfoVilleCarte(Ville $ville, ?User $user, bool $inOutils = false): array
    {
        
        if ($user === null) {
            $user = new User();
            $user->setId(0);
            $perso = new UserPersonnalisation();
            $user->setUserPersonnalisation($perso);
        }
        
        $serializer = $this->gh->getSerializer();
        $format     = 'json';
        
        if (!$inOutils) {
            $outilsExpe = null;
        } else {
            $outilsExpe = $this->expedition->recuperationOutilsExpeditionVilleJour($ville, $ville->getJour());
        }
        $listExpInscrit = $this->expedition->recuperationExpeditionWhereIAm($ville, $user);
        
        $evoVille = $this->em->getRepository(UpChantier::class)->findBy(['ville' => $ville]);
        
        
        $evoVilleJson =
            json_decode($serializer->serialize($evoVille, $format, ['groups' => ['evo_chantier']]), null, 512,
                        JSON_THROW_ON_ERROR);
        
        $evoSD = $this->upGrade->recupBonusSD($ville);
        
        
        $recapDefVille = $this->em->getRepository(Defense::class)->derniereDefense($ville) ?: new Defense($ville->getJour());
        $recapDefVille->setBonusSdPct($evoSD);
        $defSerialize = json_decode($serializer->serialize($recapDefVille, $format, ['groups' => ['defense']]), true, 512, JSON_THROW_ON_ERROR);
        
        $lvlTDG = $this->upGrade->recupLevelChantier($ville, ChantierPrototype::ID_CHANTIER_PO);
        
        $planifConstruit = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, ChantierPrototype::ID_CHANTIER_PLANIF) !== null;
        $tdgConstruit    = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, ChantierPrototype::ID_CHANTIER_TDG) !== null;
        $scrutConstruit  = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, ChantierPrototype::ID_CHANTIER_SCRUT) !== null;
        
        $phareConstruit       = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, ChantierPrototype::ID_CHANTIER_PHARE) !== null;
        $carteAmelioConstruit = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, ChantierPrototype::ID_CHANTIER_CARTE_AME) !== null;
        
        /**
         * Récupération des estimations du jour
         */
        $estimationJour = $this->em->getRepository(Estimation::class)->derniereEstimation($ville) ?: new Estimation($ville->getJour());
        
        if ($estimationJour->getDay() !== $ville->getJour()) {
            $estimationJour = new Estimation($ville->getJour());
        }
        
        /**
         * Récupération des citoyens vivants dedans/dehors
         */
        $listCitoyensVille = $this->em->getRepository(Citoyen::class)->citoyenVille($ville);
        $citoyensVille     = (new Citoyens($this->translator))->setCitoyens($listCitoyensVille);
        
        
        $citoyensVille = $citoyensVille->triByPseudo();
        
        $citDehors = $citoyensVille->citoyensDehorsEnVille();
        $citVille  = $citoyensVille->citoyensDehorsEnVille(true);
        
        $nbColOut = ceil(count($citDehors) / 10);
        $nbColIn  = ceil(count($citVille) / 10);
        
        
        $itemSolTmp       = $this->recupSommeItemSolFull($ville);
        $itemSolBrokedTmp = $this->recupSommeItemSolBrokeFull($ville);
        
        $categorieObjet = $this->em->getRepository(CategoryObjet::class)->findAllExceptInactif();
        
        $itemBanqueTmp      = $this->em->getRepository(Banque::class)->somItemBanque($ville);
        $itemBanqueBrokeTmp = $this->em->getRepository(Banque::class)->somItemBanqueBroke($ville);
        $itemBanque         = [];
        $itemBanqueBroke    = [];
        foreach ($itemBanqueTmp as $item) {
            $itemBanque[$item['id']] = (int)$item['nbrItem'];
        }
        foreach ($itemBanqueBrokeTmp as $item) {
            $itemBanqueBroke[$item['id']] = (int)$item['nbrItem'];
        }
        
        $itemBanqueStockTmp = $this->em->getRepository(Banque::class)->findBy(['ville' => $ville]);
        $itemBanqueStock    = [];
        
        foreach ($itemBanqueStockTmp as $item) {
            $itemBanqueStock[$item->getItem()->getId() * 10 + ($item->getBroked() ? 1 : 0)] = $item->getNombre();
        }
        
        
        $itemSolBroken = [];
        $listCate      = [];
        $serializer    = $this->gh->getSerializer();
        
        // Traitement en catégorie de base si l'utilisateur n'a pas choisis la non séparation en catégorie
        if ($user->getUserPersonnalisation()->isItemSeparedCate()) {
            // On va balayer tous les items afin de rajouter les items de la banque
            foreach ($itemSolTmp as $item) {
                $item->setNbrItemBank($itemBanque[$item->getId()] ?? 0);
            }
            $listCateTmp['objetSol'] = json_decode($serializer->serialize($itemSolTmp, 'json'), true, 512, JSON_THROW_ON_ERROR);
            $listCate[]              = $listCateTmp;
            // De la même façon pour les items cassés
            foreach ($itemSolBrokedTmp as $item) {
                $item->setNbrItemBank($itemBanqueBroke[$item->getId()] ?? 0);
            }
            $itemBrokeTmp    = array_combine(array_map(static fn($i) => $i->getId(), $itemSolBrokedTmp), $itemSolBrokedTmp);
            $itemSolBroken[] = json_decode($serializer->serialize($itemBrokeTmp, 'json'), true, 512, JSON_THROW_ON_ERROR);
            
        } else {
            foreach ($categorieObjet as $cate) {
                $listCateTmp             = json_decode($serializer->serialize($cate, 'json', ['groups' => ['carte']]), true, 512, JSON_THROW_ON_ERROR);
                $listCateTmp['objetSol'] = array_filter($itemSolTmp, static fn($i) => $i->getCategoryObjetId() === $cate->getId());
                foreach ($listCateTmp['objetSol'] as $objet) {
                    $objet->setNbrItemBank($itemBanque[$objet->getId()] ?? 0);
                }
                
                
                $listCateTmp['objetSol'] = json_decode($serializer->serialize($listCateTmp['objetSol'], 'json'), true, 512, JSON_THROW_ON_ERROR);
                
                
                $itemSolBrokedTmpFiltre = array_filter($itemSolBrokedTmp, static fn($i) => $i->getCategoryObjetId() === $cate->getId());
                foreach ($itemSolBrokedTmpFiltre as $objet) {
                    $objet->setNbrItemBank($itemBanqueBroke[$objet->getId()] ?? 0);
                }
                $itemBrokeTmp = array_combine(array_map(static fn($i) => $i->getId(), $itemSolBrokedTmpFiltre), $itemSolBrokedTmpFiltre);
                
                $itemSolBroken[$cate->getId()] = json_decode($serializer->serialize($itemBrokeTmp, 'json'), true, 512, JSON_THROW_ON_ERROR);
                $listCate[]                    = $listCateTmp;
            }
        }
        
        $batimentVille = $this->em->getRepository(ZoneMap::class)->recupBatiment($ville);
        
        $ruinesVille = new ArrayCollection($this->em->getRepository(ZoneMap::class)->recupRuine($ville));
        
        $batJaune = (new ArrayCollection($batimentVille))->filter(fn(ZoneMap $zoneMap) => $zoneMap->calculKm() < 10);
        $batBleu  = (new ArrayCollection($batimentVille))->filter(fn(ZoneMap $zoneMap) => $zoneMap->calculKm() >= 10);
        
        /**
         * Calcul des batiments campés
         */
        $batJauneCamped = $batJaune->filter(fn(ZoneMap $zoneMap) => $zoneMap->isCamped())->count();
        $batBleuCamped  = $batBleu->filter(fn(ZoneMap $zoneMap) => $zoneMap->isCamped())->count();
        
        
        if (!$inOutils) {
            $listExpeBdd = $this->em->getRepository(TraceExpedition::class)->findAllExpeOrderByDayDesc($ville, $user);
            
            $listExpe = [];
            foreach ($listExpeBdd as $expe) {
                if ($expe->getExpeditionPart() === null) {
                    $listExpe[] = $expe;
                } else {
                    
                    // Il faut récupérer que les traces du jour
                    if ($expe->getJour() === $ville->getJour()) {
                        $listExpe[] = $expe;
                    }
                    
                }
            }
            
        } else {
            $listExpe = $this->em->getRepository(TraceExpedition::class)->findAllExpeOrderByDayDesc($ville, $user);
        }
        
        $listUserCreat =
            array_unique(array_map(static fn(TraceExpedition $trace) => $trace->getCreatedBy()?->getId(), $listExpe));
        
        $createurVivant = $this->em->getRepository(Citoyen::class)->createQueryBuilder('c', 'c.citoyen')
                                   ->where('c.citoyen in (:user)')
                                   ->andWhere('c.mort <> 1')
                                   ->andWhere('c.ville = :ville')
                                   ->setParameter('user', $listUserCreat)
                                   ->setParameter('ville', $ville)
                                   ->getQuery()
                                   ->getResult();
        
        
        $carteExpe = array_map(fn(TraceExpedition $traceExpedition) => $this->expedition->calculExpedition($ville,
                                                                                                           $traceExpedition->getCoordonnee()),
            $listExpe);
        
        $carteExpeCase = [];
        
        for ($j = 0; $j < $ville->getHeight(); $j++) {
            for ($i = 0; $i < $ville->getWeight(); $i++) {
                foreach ($carteExpe as $key => $carte) {
                    if ($carte[$j][$i] !== null) {
                        $carteExpeCase[$j * 100 + $i][$listExpe[$key]->getId()] = $carte[$j][$i];
                    }
                }
            }
        }
        
        $tabCouleurExpe = [];
        foreach ($listExpe as $expe) {
            $tabCouleurExpe[$expe->getId()] = $expe->getCouleur();
        }
        
        $listExpeHordes =
            $this->em->getRepository(ExpeHordes::class)->findBy(['ville' => $ville, 'day' => $ville->getJour()]);
        
        $tabCouleur = ['#FFFFFF', '#0000FF', '#FF0000', '#FFA500', '#00FF00', '#4B0082', '#00FFFF', '#FF1493'];
        foreach ($listExpeHordes as $key => $value) {
            $listCouleurMyHordes[$value->getId()] = $tabCouleur[$key % count($tabCouleur)];
        }
        
        
        $carteExpeHordes = array_map(
            fn(ExpeHordes $expe) => $this->expedition->calculExpedition($ville,
                                                                        (new TraceExpedition())->setCoordonnee($expe->getTrace())
                                                                                               ->getCoordonnee()),
            $listExpeHordes,
        );
        
        $carteExpeCaseMyHordes = [];
        for ($j = 0; $j < $ville->getHeight(); $j++) {
            for ($i = 0; $i < $ville->getWeight(); $i++) {
                foreach ($carteExpeHordes as $key => $carte) {
                    if ($carte[$j][$i] !== null) {
                        $carteExpeCaseMyHordes[$j * 100 + $i][$listExpeHordes[$key]->getId()] = $carte[$j][$i];
                    }
                }
            }
        }
        
        if ($inOutils) {
            $listExpeBiblio = $this->em->getRepository(TraceExpedition::class)->findAllBiblio($ville);
            
            $carteExpeBiblio = array_map(fn(TraceExpedition $traceExpedition) => $this->expedition->calculExpedition($ville, $traceExpedition->getCoordonnee()), $listExpeBiblio);
            
            $carteExpeCaseBiblio = [];
            
            for ($j = 0; $j < $ville->getHeight(); $j++) {
                for ($i = 0; $i < $ville->getWeight(); $i++) {
                    foreach ($carteExpeBiblio as $key => $carte) {
                        if ($carte[$j][$i] !== null) {
                            $carteExpeCaseBiblio[$j * 100 + $i][$listExpeBiblio[$key]->getId()] = $carte[$j][$i];
                        }
                    }
                }
            }
            foreach ($listExpeBiblio as $expe) {
                $tabCouleurExpe[$expe->getId()] = $expe->getCouleur();
            }
            
            $listExpeBrouillon = $this->em->getRepository(TraceExpedition::class)->findAllBrouillon($ville);
            
            $carteExpeBrouillon =
                array_map(fn(TraceExpedition $traceExpedition) => $this->expedition->calculExpedition($ville, $traceExpedition->getCoordonnee()), $listExpeBrouillon);
            
            $carteExpeCaseBrouillon = [];
            
            for ($j = 0; $j < $ville->getHeight(); $j++) {
                for ($i = 0; $i < $ville->getWeight(); $i++) {
                    foreach ($carteExpeBrouillon as $key => $carte) {
                        if ($carte[$j][$i] !== null) {
                            $carteExpeCaseBrouillon[$j * 100 + $i][$listExpeBrouillon[$key]->getId()] = $carte[$j][$i];
                        }
                    }
                }
            }
            foreach ($listExpeBrouillon as $expe) {
                $tabCouleurExpe[$expe->getId()] = $expe->getCouleur();
            }
        }
        
        
        $listBat = $this->em->getRepository(BatPrototype::class)->findAll();
        
        
        $listBatJson = json_decode($serializer->serialize($listBat, $format, ['groups' => ['bat']]), null, 512, JSON_THROW_ON_ERROR);
        
        $itemWater        = $this->em->getRepository(ItemPrototype::class)->findOneBy(['uid' => 'water_#00']);
        $itemPotion       = $this->em->getRepository(ItemPrototype::class)->findOneBy(['uid' => 'potion_#00']);
        $itemNourritureB6 = $this->em->getRepository(ItemPrototype::class)->findBy(['uid' => ['can_#00',
                                                                                              'can_open_#00',
                                                                                              'undef_#00',
                                                                                              'hmeat_#00',
                                                                                              'vegetable_#00',
                                                                                              'food_bag_#00',
                                                                                              'food_bar1_#00',
                                                                                              'food_bar2_#00',
                                                                                              'food_bar3_#00',
                                                                                              'food_biscuit_#00',
                                                                                              'food_chick_#00',
                                                                                              'food_pims_#00',
                                                                                              'food_tarte_#00',
                                                                                              'food_sandw_#00',
                                                                                              'food_noodles_#00',
                                                                                              'bone_meat_#00',
                                                                                              'dish_#00',
                                                                                              'fruit_#00',
                                                                                              'woodsteak_#00',
                                                                                              'cadaver_#00',
                                                                                              'bretz_#00',
        ]]);
        $itemNourritureB7 = $this->em->getRepository(ItemPrototype::class)->findBy(['uid' => ['meat_#00',
                                                                                              'food_noodles_hot_#00',
                                                                                              'vegetable_tasty_#00',
                                                                                              'dish_tasty_#00',
                                                                                              'food_candies_#00',
                                                                                              'chama_tasty_#00',
                                                                                              'egg_#00',
                                                                                              'apple_#00',
        ]]);
        $itemAlcool       = $this->em->getRepository(ItemPrototype::class)->findBy(['uid' => ['vodka_de_#00',
                                                                                              'rhum_#00',
                                                                                              'vodka_#00',
                                                                                              'guiness_#00',
                                                                                              'fest_#00',
        ]]);
        $itemDrogue       = $this->em->getRepository(ItemPrototype::class)->findBy(['uid' => ['xanax_#00',
                                                                                              'drug_#00',
                                                                                              'drug_hero_#00',
                                                                                              'drug_water_#00',
                                                                                              'drug_random_#00',
                                                                                              'disinfect_#00',
                                                                                              'beta_drug_bad_#00',
                                                                                              'beta_drug_#00',
                                                                                              'lsd_#00',
        ]]);
        
        $nbrWatter = $this->em->getRepository(Banque::class)->findOneBy(['ville' => $ville, 'item' => $itemWater]);
        $nbrPotion = $this->em->getRepository(Banque::class)->findOneBy(['ville' => $ville, 'item' => $itemPotion]);
        $nbrB6     = $this->em->getRepository(Banque::class)->findBy(['ville' => $ville, 'item' => $itemNourritureB6]);
        
        $nbrB6Tot = 0;
        foreach ($nbrB6 as $b6) {
            $nbrB6Tot += $b6->getNombre();
        }
        
        $nbrB7 = $this->em->getRepository(Banque::class)->findBy(['ville' => $ville, 'item' => $itemNourritureB7]);
        
        $nbrB7Tot = 0;
        foreach ($nbrB7 as $b7) {
            $nbrB7Tot += $b7->getNombre();
        }
        
        $nbrAlcool    = $this->em->getRepository(Banque::class)->findBy(['ville' => $ville, 'item' => $itemAlcool]);
        $nbrAlcoolTot = 0;
        foreach ($nbrAlcool as $alcool) {
            $nbrAlcoolTot += $alcool->getNombre();
        }
        
        $nbrDrogue    = $this->em->getRepository(Banque::class)->findBy(['ville' => $ville, 'item' => $itemDrogue]);
        $nbrDrogueTot = 0;
        foreach ($nbrDrogue as $drogue) {
            $nbrDrogueTot += $drogue->getNombre();
        }
        
        $mapItem = $this->em->getRepository(MapItem::class)->recupMapItemAllVille($ville);
        
        $mapItemArray = [];
        
        foreach ($mapItem as $item) {
            $mapItemArray[$item->getZone()?->getId()][$item->getItem()->getId() * 10 +
                                                      ($item->getBroked() ? 1 : 0)] = $item;
        }
        
        $listDecharge = $this->em->getRepository(ChantierPrototype::class)->findBy(['id' => [
            ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS,
            ChantierPrototype::ID_CHANTIER_FERRAILLERIE,
            ChantierPrototype::ID_CHANTIER_ENCLOS,
            ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE,
            ChantierPrototype::ID_CHANTIER_APPAT,
            ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE,
        ]]);
        
        foreach ($listDecharge as $chantier) {
            $chantier->setNom($this->translator->trans($chantier->getNom(), [], 'chantiers'));
        }
        
        $listDechargeJson = json_decode($serializer->serialize($listDecharge, $format, ['groups' => ['carte']]), true, 512, JSON_THROW_ON_ERROR);
        
        
        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = 0;
        $maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = 0;
        $maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = 0;
        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = 0;
        $maxAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = 0;
        $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = 0;
        $maxAlterAll                                              = 0;
        $maxScrut                                                 = 0;
        
        $listZonage = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29];
        
        
        $arrayCitoyenVie   = [];
        $arrayCitoyenVille = [];
        
        foreach ($listCitoyensVille as $citoyen) {
            
            if (!$citoyen->getMort() && !$ville->isDevast()) {
                $arrayCitoyenVie[$citoyen->getY() * 100 + $citoyen->getX()][] = $citoyen;
                
                if ($citoyen->getCitoyen()->getId() === $user->getId()) {
                    $positionUser = $citoyen->getY() * 100 + $citoyen->getX();
                }
                
            }
            $arrayCitoyenVille[$citoyen->getCitoyen()->getId()] = $citoyen;
        }
        
        $ville->setListCitoyenVie($arrayCitoyenVie)
              ->setListCitoyenVille($arrayCitoyenVille)
              ->setMaPosition($positionUser ?? null);
        
        
        foreach ($listZonage as $zone) {
            $pctMap['zonePa'][$zone]     = $zone;
            $pctMap['epuise'][$zone]     = 0;
            $pctMap['decouverte'][$zone] = 0;
            $pctMap['total'][$zone]      = 0;
        }
        $pctMap['zonePa'][99]     = 99;
        $pctMap['epuise'][99]     = 0;
        $pctMap['decouverte'][99] = 0;
        $pctMap['total'][99]      = 0;
        $jourVille                = $ville->getJour();
        foreach ($ville->getZone() as $zoneMap) {
            if ($zoneMap->getVue() !== ZoneMap::VILLE) {
                $pctMap['epuise'][$zoneMap->getZone()]     += ($zoneMap->getVue() !== ZoneMap::NON_EXPLO && $zoneMap->getDried()) ? 1 : 0;
                $pctMap['decouverte'][$zoneMap->getZone()] += $zoneMap->getVue() !== ZoneMap::NON_EXPLO ? 1 : 0;
                $pctMap['total'][$zoneMap->getZone()]      += 1;
            }
            // Calcul de l'indicateur de visite
            $zoneMap->setIndicVisite($this->recupIndicVisit($jourVille, $zoneMap->getDay() ?? 0, $zoneMap->getVue()));
            // Calcul de l'estimation des zombies
            $zoneMap->setZombieEstim((new ZoneMapHelper())->getEstimZombie($zoneMap, $ville, $carteAmelioConstruit, $lvlTDG));
            
            $marqueurOnly = false;
            
            if (isset($mapItemArray[$zoneMap->getId()])) {
                $maxCarteAlterTmp                                           = 0;
                $maxCarteScrutTmp                                           = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = 0;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = 0;
                $carteScrut                                                 = 0;
                $itemSolArray                                               = [];
                $itemSolArrayIcone                                          = [];
                $presenceMarqueur                                           = false;
                $presenceAutre                                              = false;
                
                /** @var MapItem $mapItem */
                foreach ($mapItemArray[$zoneMap->getId()] as $mapItem) {
                    if ($mapItem->getItem()->getTypeDecharge() !== null) {
                        $carteAlter[$mapItem->getItem()->getTypeDecharge()->getChantier()->getId()] += $mapItem->getNombre();
                        $maxCarteAlterTmp                                                           += $mapItem->getNombre();
                    }
                    $idMap = 'item_' . $mapItem->getItem()->getId();
                    if (!isset($itemSolArray[$idMap])) {
                        $itemSolArray['item_' . $mapItem->getItem()->getId()] = true;
                    }
                    if (!isset($itemSolArrayIcone[$idMap])) {
                        $itemSolArrayIcone['item_' . $mapItem->getItem()->getId()] = $mapItem->getItem()->getIcon();
                    }
                    if ($mapItem->getItem()?->getTypeObjet()?->getId() === TypeObjet::MARQUEUR) {
                        if ($mapItem->getItem()->getId() === ItemPrototype::ITEM_MARQUEURS_SCRUT) {
                            $carteScrut       += $mapItem->getNombre();
                            $maxCarteScrutTmp += $mapItem->getNombre();
                        }
                        $presenceMarqueur = true;
                    } else {
                        $presenceAutre = true;
                    }
                }
                
                if ($presenceMarqueur && !$presenceAutre) {
                    $marqueurOnly = true;
                }
                
                
                $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]);
                $maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = max($maxAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE], $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]);
                $maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = max($maxAlter[ChantierPrototype::ID_CHANTIER_ENCLOS], $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]);
                $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]);
                $maxAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = max($maxAlter[ChantierPrototype::ID_CHANTIER_APPAT], $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]);
                $maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = max($maxAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE], $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE]);
                $maxAlterAll                                              = max($maxAlterAll, $maxCarteAlterTmp);
                $maxScrut                                                 = max($maxScrut, $maxCarteScrutTmp);
                $itemsSol                                                 = array_keys($itemSolArray);
                $itemsSolIcone                                            = $itemSolArrayIcone;
            } else {
                $carteScrut                                                 = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS]   = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_FERRAILLERIE]    = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_ENCLOS]          = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE]  = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_APPAT]           = null;
                $carteAlter[ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE] = null;
                $itemsSol                                                   = [];
                $itemsSolIcone                                              = [];
                
            }
            
            $zoneMap->setCarteAlter($carteAlter)
                    ->setCarteScrut($carteScrut)
                    ->setItemSol($itemsSol)
                    ->setItemSolIcone($itemsSolIcone)
                    ->setMarqueurOnly($marqueurOnly);
        }
        
        
        // Somme des valeurs accumulées pour chaque type de donnée
        foreach (['epuise', 'decouverte', 'total'] as $type) {
            $cumulativeSum = 0; // Initialiser la somme cumulative à 0
            
            foreach ($pctMap[$type] as $zoneCase => $value) {
                $cumulativeSum            += $value;        // Ajouter la valeur actuelle à la somme cumulative
                $pctMap[$type][$zoneCase] = $cumulativeSum; // Mettre à jour la valeur avec la somme cumulative
            }
        }
        
        //dd($villeSerialize['zones'][612]);
        
        
        $libDernierMaj = $this->translator->trans('Dernière mise à jour le {date} par {user}', ['{user}' => $ville->getUpdateBy()?->getPseudo() ?? $this->translator->trans('inconnu', [], 'app'), '{date}' => $this->lfu->getFormattedDateHeure($ville->getDateTime()),], 'app');
        
        $userJson = $this->serializerService->serializeArray($user, $format, ['carte_user']);
        // On récupère les id des plans de la ruine de la ville
        $ruinesPlanId = [];
        $listRuins    = $this->em->getRepository(Ruines::class)->findBy(['ville' => $ville]);
        
        foreach ($listRuins as $ruine) {
            $ruinesPlanId[$ruine->getBat()?->getId()] = $ruine->getId();
        }
        
        $listJobs = $this->em->getRepository(JobPrototype::class)->findAll();
        
        
        $villeSerialize = json_decode($serializer->serialize($ville, $format, ['groups' => ['carte', 'carte_gen']]), true, 512, JSON_THROW_ON_ERROR);
        
        /**
         * Generation de la carte
         */
        return [
            'arrCreateurVivant'      => array_flip(array_keys($createurVivant)),
            'batCarte'               => [
                'batJaune'      => array_map(static fn(ZoneMap $bat) => json_decode($serializer->serialize($bat, $format, ['groups' => ['carte']]), true, 512, JSON_THROW_ON_ERROR), $batJaune->toArray()),
                'batBleu'       => array_map(static fn(ZoneMap $bat) => json_decode($serializer->serialize($bat, $format, ['groups' => ['carte']]), true, 512, JSON_THROW_ON_ERROR), $batBleu->toArray()),
                'ruines'        => array_map(static fn(ZoneMap $bat) => json_decode($serializer->serialize($bat, $format, ['groups' => ['carte']]), true, 512, JSON_THROW_ON_ERROR), $ruinesVille->toArray()),
                'nbCampedJaune' => $batJauneCamped,
                'nbCampedBleu'  => $batBleuCamped,
            ],
            'campingActif'           => true,
            'carteExpe'              => $carteExpeCase,
            'carteExpeHordes'        => $carteExpeCaseMyHordes,
            'carteExpeBiblio'        => ($inOutils) ? $carteExpeCaseBiblio : [],
            'carteExpeBrouillon'     => ($inOutils) ? $carteExpeCaseBrouillon : [],
            'carteOptionPerso'       => false,
            'carteOptionPerso_alter' => false,
            'carteOptionPerso_estim' => false,
            'chaman_banque'          => $nbrPotion?->getNombre() ?? 0,
            'citoyensDehors'         => array_values(json_decode($serializer->serialize($citDehors, $format, ['groups' => ['carte_gen']]), true, 512, JSON_THROW_ON_ERROR)),
            'citoyensVille'          => array_values(json_decode($serializer->serialize($citVille, $format, ['groups' => ['carte_gen']]), true, 512, JSON_THROW_ON_ERROR)),
            'constructionChantier'   => [
                'tdg'    => $tdgConstruit,
                'planif' => $planifConstruit,
                'scrut'  => $scrutConstruit,
                'phare'  => $phareConstruit,
            ],
            'couleurHordes'          => $listCouleurMyHordes ?? [],
            'dateDernMaj'            => $libDernierMaj,
            'defense'                => $defSerialize,
            'devastLibelle'          => $this->translator->trans($ville->devastLibelle(), [], 'app'),
            'estim_day'              => $ville->getJour(),
            'estimation'             => json_decode($serializer->serialize($estimationJour, $format, ['groups' => ['carte_gen']]), true, 512, JSON_THROW_ON_ERROR),
            'evoChantier'            => $evoVilleJson,
            'isLead'                 => $this->userHandler->isLead($ville->getMapId()),
            'listBat'                => $listBatJson,
            'listCategorie'          => $listCate,
            'listDecharge'           => $listDechargeJson,
            'listExp'                => json_decode($serializer->serialize($listExpe, $format, ['groups' => ['expe', 'general_res']]), true, 512, JSON_THROW_ON_ERROR),
            'listExpBiblio'          => ($inOutils) ? json_decode($serializer->serialize($listExpeBiblio, $format, ['groups' => ['expe', 'general_res']]), true, 512, JSON_THROW_ON_ERROR) : [],
            'listExpBrouillon'       => ($inOutils) ? json_decode($serializer->serialize($listExpeBrouillon, $format, ['groups' => ['expe', 'general_res']]), true, 512, JSON_THROW_ON_ERROR) : [],
            'listExpCouleur'         => $tabCouleurExpe,
            'listExpHordes'          => json_decode($serializer->serialize($listExpeHordes, $format, ['groups' => ['expe', 'general_res']]), true, 512, JSON_THROW_ON_ERROR),
            'listExpInscrit'         => $listExpInscrit,
            'listItemsBank'          => $itemBanqueStock ?? [],
            'listItemsSolBroken'     => $itemSolBroken,
            'listJobs'               => json_decode($serializer->serialize($listJobs, $format, ['groups' => ['job']]), true, 512, JSON_THROW_ON_ERROR),
            'listRuine'              => $ruinesPlanId,
            'listZonage'             => $listZonage,
            'lvlTdG'                 => $lvlTDG,
            'maxKm'                  => $ville->getMaxKm(),
            'maxPa'                  => $ville->getMaxPa(),
            'maxAlter'               => $maxAlter,
            'maxAlterAll'            => $maxAlterAll,
            'maxScrut'               => $maxScrut,
            'myVille'                => $this->userHandler->myVille($ville),
            'nbColIn'                => $nbColIn,
            'nbColOut'               => $nbColOut,
            'pctMap'                 => $pctMap,
            'theme'                  => $user->getTheme() ?? User::THEME_LIGHT,
            'typeLibelle'            => $this->translator->trans($ville->typeLibelle(), [], 'app'),
            'user'                   => $userJson,
            'ville'                  => $villeSerialize,
            'water_banque'           => $nbrWatter?->getNombre() ?? 0,
            'nourriture_banque_b6'   => $nbrB6Tot ?? 0,
            'nourriture_banque_b7'   => $nbrB7Tot ?? 0,
            'alcool_banque'          => $nbrAlcoolTot ?? 0,
            'drogue_banque'          => $nbrDrogueTot ?? 0,
            'outilsExpe'             => $outilsExpe,
        ];
    }
    
    /**
     * @return SomItemSolDTO[]
     */
    public function recupSommeItemSol(Ville $ville, array $idZone): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('nbrItem', 'nbrItem', 'integer');
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('nom', 'nom');
        $rsm->addScalarResult('icon', 'icon');
        $rsm->addScalarResult('category_objet_id', 'category_objet_id', 'integer');
        $rsm->newObjectMappings['nbrItem']           = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0, // a result can contain many DTOs, this is the index of the DTO to map to
            'argIndex'  => 0, // each scalar result can be mapped to a different argument of the DTO constructor
        ];
        $rsm->newObjectMappings['id']                = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 1,
        ];
        $rsm->newObjectMappings['nom']               = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 2,
        ];
        $rsm->newObjectMappings['icon']              = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 3,
        ];
        $rsm->newObjectMappings['category_objet_id'] = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 4,
        ];
        
        $requeteSQL = $this->em->createNativeQuery("SELECT SUM(i.nombre) as nbrItem, it.id, it.nom, it.icon,it.category_objet_id
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                    WHERE i.ville_id IN (:arrayId)
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id , nbrItem DESC, i.item_id ", $rsm);
        
        $requeteSQL->setParameter(':ville', $ville->getMapId());
        $requeteSQL->setParameter(':arrayId', $idZone);
        
        return $requeteSQL->getResult();
    }
    
    /**
     * @return SomItemSolDTO[]
     */
    public function recupSommeItemSolBroke(Ville $ville, array $idZone): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('nbrItem', 1, 'integer');
        $rsm->addScalarResult('id', 2, 'integer');
        $rsm->addScalarResult('nom', 3);
        $rsm->addScalarResult('icon', 4);
        $rsm->addScalarResult('category_objet_id', 5, 'integer');
        $rsm->newObjectMappings['nbrItem']           = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0, // a result can contain many DTOs, this is the index of the DTO to map to
            'argIndex'  => 0, // each scalar result can be mapped to a different argument of the DTO constructor
        ];
        $rsm->newObjectMappings['id']                = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 1,
        ];
        $rsm->newObjectMappings['nom']               = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 2,
        ];
        $rsm->newObjectMappings['icon']              = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 3,
        ];
        $rsm->newObjectMappings['category_objet_id'] = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 4,
        ];
        
        $requeteSQL = $this->em->createNativeQuery("SELECT SUM(i.nombre) as nbrItem, it.id, it.nom, it.icon,it.category_objet_id
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                    WHERE i.ville_id in (:arrayId)
                        AND i.broked = 1
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id , nbrItem DESC, i.item_id ", $rsm);
        
        $requeteSQL->setParameter(':ville', $ville->getMapId());
        $requeteSQL->setParameter(':arrayId', $idZone);
        
        return $requeteSQL->getResult();
    }
    
    /**
     * @return SomItemSolDTO[]
     */
    public function recupSommeItemSolBrokeFull(Ville $ville): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('nbrItem', 1, 'integer');
        $rsm->addScalarResult('id', 2, 'integer');
        $rsm->addScalarResult('nom', 3);
        $rsm->addScalarResult('icon', 4);
        $rsm->addScalarResult('category_objet_id', 5, 'integer');
        $rsm->newObjectMappings['nbrItem']           = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0, // a result can contain many DTOs, this is the index of the DTO to map to
            'argIndex'  => 0, // each scalar result can be mapped to a different argument of the DTO constructor
        ];
        $rsm->newObjectMappings['id']                = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 1,
        ];
        $rsm->newObjectMappings['nom']               = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 2,
        ];
        $rsm->newObjectMappings['icon']              = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 3,
        ];
        $rsm->newObjectMappings['category_objet_id'] = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 4,
        ];
        
        $zoneInf = $ville->getId() * 10000;
        $zoneSup = ($ville->getId() + 1) * 10000;
        
        $requeteSQL = $this->em->createNativeQuery("SELECT SUM(i.nombre) as nbrItem, it.id, it.nom, it.icon,it.category_objet_id
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                    WHERE i.ville_id >= :zoneInf
                        AND i.ville_id < :zoneSup
                        AND i.broked = 1
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id , nbrItem DESC, i.item_id ", $rsm);
        
        $requeteSQL->setParameter(':zoneInf', $zoneInf);
        $requeteSQL->setParameter(':zoneSup', $zoneSup);
        
        return $requeteSQL->getResult();
    }
    
    /**
     * @return SomItemSolDTO[]
     */
    public function recupSommeItemSolFull(Ville $ville): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('nbrItem', 'nbrItem', 'integer');
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('nom', 'nom');
        $rsm->addScalarResult('icon', 'icon');
        $rsm->addScalarResult('category_objet_id', 'category_objet_id', 'integer');
        $rsm->newObjectMappings['nbrItem']           = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0, // a result can contain many DTOs, this is the index of the DTO to map to
            'argIndex'  => 0, // each scalar result can be mapped to a different argument of the DTO constructor
        ];
        $rsm->newObjectMappings['id']                = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 1,
        ];
        $rsm->newObjectMappings['nom']               = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 2,
        ];
        $rsm->newObjectMappings['icon']              = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 3,
        ];
        $rsm->newObjectMappings['category_objet_id'] = [
            'className' => SomItemSolDTO::class,
            'objIndex'  => 0,
            'argIndex'  => 4,
        ];
        
        $zoneInf = $ville->getId() * 10000;
        $zoneSup = ($ville->getId() + 1) * 10000;
        
        $requeteSQL = $this->em->createNativeQuery("SELECT SUM(i.nombre) as nbrItem, it.id, it.nom, it.icon,it.category_objet_id
                        FROM map_item i
                        INNER JOIN item_prototype it on i.item_id = it.id
                    WHERE i.ville_id >= :zoneInf
                        AND i.ville_id < :zoneSup
                    GROUP BY it.category_objet_id, i.item_id
                    ORDER BY it.category_objet_id , nbrItem DESC, i.item_id ", $rsm);
        
        $requeteSQL->setParameter(':zoneInf', $zoneInf);
        $requeteSQL->setParameter(':zoneSup', $zoneSup);
        
        return $requeteSQL->getResult();
    }
    
    
}