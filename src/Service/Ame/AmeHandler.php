<?php


namespace App\Service\Ame;


use App\Entity\HistoriqueVille;
use App\Entity\Pictos;
use App\Entity\User;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Service\GestHordesHandler;
use App\Service\Utils\UpdateAPIHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class AmeHandler
{
    
    public function __construct(
        protected EntityManagerInterface $em,
        protected GestHordesHandler      $gh,
        protected TranslatorInterface    $trans,
        protected UpdateAPIHandler       $updateAPIHandler,
    )
    {
    }
    
    public function getListeSaisonForCumul(): array
    {
        $listeSaison   = [];
        $listeSaison[] = ['id' => -1, 'nom' => $this->trans->trans("toutes saisons", [], 'ame')];
        
        for ($i = 15; $i < 18; $i++) {
            $listeSaison[] = ['id' => $i, 'nom' => $this->trans->trans("Saison {number}", ['{number}' => $i], 'ame')];
        }
        
        return $listeSaison;
    }
    
    public function getStatsForUser(User $user): array
    {
        
        // On récupère les stats de l'utilisateur
        // On récupère le nombre de villes où le joueur s'est incarné
        $nbrVille = count($this->em->getRepository(HistoriqueVille::class)->findBy(['user' => $user]));
        // On récupère le le record de survie
        $record_surv = $this->em->getRepository(HistoriqueVille::class)->getRecordSurvie($user);
        // On récupère la durée total d'incarnation
        $jour_total = $this->em->getRepository(HistoriqueVille::class)->getJourTotal($user);
        // On récupère la durée moyenne de survie
        $moyenne_surv = ($nbrVille > 0) ? round($jour_total / $nbrVille) : 0;
        
        // On va calculer maintenant le nombre de jours où le joueur a été héros, pour ça on va prendre les pictos qui correspondent à une incarnation en héros
        $pictosProto  = ["r_jguard_#00", "r_jtech_#00", "r_jcolle_#00", "r_jermit_#00", "r_jrangr_#00", "r_jtamer_#00", "r_jsham_#00"];
        $nbrJourHeros = $this->em->getRepository(Pictos::class)->getNombrePictosInArray($user, $pictosProto);
        
        
        return [
            'nbrVille'     => $nbrVille,
            'record_surv'  => $record_surv,
            'jour_heros'   => $nbrJourHeros,
            'jour_total'   => $jour_total,
            'moyenne_surv' => $moyenne_surv,
        ];
        
    }
    
    /**
     * @throws MyHordesMajException
     * @throws MyHordesAttackException
     * @throws MyHordesMajApiException
     * @throws Exception
     */
    public function updateMySoul(User $user): void
    {
        $maj = $this->updateAPIHandler->appelApiGestionAttackMaintenance("getMeOldTownPictos", $user->getApiToken());
        if ($maj->getRewards() !== null) {
            $this->updateAPIHandler->updatePicto($maj->getRewards(), $user);
        }
        
        if ($maj->getPlayedMap() !== null) {
            $this->updateAPIHandler->updateAllOldVille($maj->getPlayedMap(), $user);
        }
        
    }
}