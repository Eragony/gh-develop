<?php

namespace App\Service\Ame;

use App\Entity\HistoriquePictos;
use App\Entity\PictoPrototype;
use App\Entity\Pictos;
use App\Entity\User;
use App\Entity\Ville;
use App\Exception\GestHordesException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Contracts\Translation\TranslatorInterface;

class PictoHandler
{
    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly TranslatorInterface    $translator)
    {
    }
    
    /**
     * @param User $user
     * @param int $saison
     * @return Pictos[]
     * @throws GestHordesException
     */
    public function getCumulPictosForASaison(User $user, int $saison): array
    {
        /** @var HistoriquePictos[] $cumulHisto */
        $cumulHisto = $this->getRepository(HistoriquePictos::class)->recupHistoPictoForSaison($user, $saison);
        
        // calcul du cumul des pictos par picto
        $cumulPictos = [];
        foreach ($cumulHisto as $histo) {
            if (!isset($cumulPictos[$histo->getPicto()->getId()])) {
                $pictoAme       = new Pictos();
                $pictoPrototype =
                    $this->em->getRepository(PictoPrototype::class)->findOneBy(['id' => $histo->getPicto()->getId()]);
                $pictoAme->setPicto($pictoPrototype);
                $pictoAme->setNombre($histo->getNombre());
                $cumulPictos[$histo->getPicto()->getId()] = $pictoAme;
            } else {
                $cumulPictos[$histo->getPicto()->getId()]->setNombre($cumulPictos[$histo->getPicto()
                                                                                        ->getId()]->getNombre() +
                                                                     $histo->getNombre());
            }
        }
        
        return $cumulPictos;
    }
    
    /**
     * @param User $user
     * @return HistoriquePictos[]
     */
    public function getHistoPictoUser(User $user): array
    {
        $histoPictos = $this->getRepository(HistoriquePictos::class)->recupListHistoriqueVille($user);
        
        /** @var HistoriquePictos[] $listHistoPicto */
        $listHistoPicto = [];
        foreach ($histoPictos as $picto) {
            $listHistoPicto[$picto->getMapId()][] = $picto;
        }
        
        return $listHistoPicto;
    }
    
    /**
     * @return PictoPrototype[]
     */
    public function getListPicto(): array
    {
        $listPicto = $this->getRepository(PictoPrototype::class)->findAll();
        
        foreach ($listPicto as $picto) {
            $picto->setName($this->translator->trans($picto->getName(), [], 'game'))
                  ->setDescription($this->translator->trans($picto->getDescription(), [], 'game'));
            
        }
        
        return $listPicto;
    }
    
    public function getListPictoAll(array $listPicto): array
    {
        $listPictoAll = [];
        foreach ($listPicto as $picto) {
            $listPictoAll[$picto->getId()] = $this->em->getRepository(Pictos::class)->recupClassementPictos($picto);
        }
        
        return $listPictoAll;
    }
    
    public function getListPictoAllForSaison(array $listPicto, int $saison, ?int $limit): array
    {
        $listPictoAll = [];
        foreach ($listPicto as $picto) {
            $listPictoAll[$picto->getId()] = $this->em->getRepository(HistoriquePictos::class)->recupHistoPictoForSaisonForOnePicto($saison, $picto, $limit);
        }
        
        return $listPictoAll;
    }
    
    public function getListPictoAllForSaisonVille(array $listPicto, int $saison, Ville $ville, ?int $limit): array
    {
        $listPictoAll = [];
        foreach ($listPicto as $picto) {
            $listPictoAll[$picto->getId()] = $this->em->getRepository(HistoriquePictos::class)->recupHistoPictoForSaisonForOnePictVilleo($ville, $saison, $picto, $limit);
        }
        
        return $listPictoAll;
    }
    
    /**
     * @param User $user
     * @return Pictos[]
     */
    public function getListPictoUser(User $user): array
    {
        $listPictos = $this->em->getRepository(Pictos::class)->recupPictosUser($user);
        
        foreach ($listPictos as $pictoList) {
            $picto = $pictoList->getPicto();
            $picto->setName($this->translator->trans($picto->getName(), [], 'game'));
        }
        
        return $listPictos;
    }
    
    public function getListPictoVille(array $listPicto, Ville $ville): array
    {
        $listPictoVille = [];
        foreach ($listPicto as $picto) {
            $listPictoVille[$picto->getId()] =
                $this->em->getRepository(Pictos::class)->recupClassementPictosVille($picto, $ville);
        }
        
        return $listPictoVille;
    }
    
    /**
     * Retrieves the PictoPrototype instance for the given picto ID.
     *
     * @param int $idPicto The ID of the picto.
     *
     * @return PictoPrototype The PictoPrototype instance associated with the given picto ID.
     * @throws GestHordesException If the picto with the given ID is not found.
     */
    public function getPicto(int $idPicto): PictoPrototype
    {
        $picto = $this->getRepository(PictoPrototype::class)->findOneBy(['id' => $idPicto]);
        
        if ($picto === null) {
            throw new GestHordesException('Picto non trouvé | fichier ' . __FILE__ . ' | ligne ' . __LINE__);
        } else {
            $this->translatePicto($picto);
        }
        return $picto;
    }
    
    /**
     * Retrieves a repository instance for the given repository class.
     *
     * @param string $repositoryClass The fully qualified name of the repository class.
     *
     * @return EntityRepository A repository instance associated with the given repository class.
     */
    private function getRepository(string $repositoryClass): EntityRepository
    {
        return $this->em->getRepository($repositoryClass);
    }
    
    /**
     * Translates the name of a picto.
     *
     * @param PictoPrototype $picto The picto object to be translated.
     *
     * @return void
     */
    private function translatePicto(PictoPrototype $picto): void
    {
        $picto->setName($this->translator->trans($picto->getName(), [], 'game'));
    }
}