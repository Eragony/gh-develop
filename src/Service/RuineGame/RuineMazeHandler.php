<?php

namespace App\Service\RuineGame;

use App\Entity\RuineGame;
use App\Entity\RuineGameZone;
use App\Entity\RuineGameZonePrototype;
use App\Entity\RuinesCases;
use App\Service\AbstractGHHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\RandomHandler;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\RuineGame\CaseRuineGame;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RuineMazeHandler extends AbstractGHHandler
{
    const ruineSizeX                 = 13;
    const ruineSizeY                 = 13;
    const ruineOffsetX               = -7;
    const ruineOffsetY               = 1;
    const minDirection               = 3;
    const distance_min_portes        = 5;
    const distance_max_portes_closed = 9999;
    const distance_min_entre_portes  = 4;
    private float              $probaSautRuine       = 0;
    private float              $probaJointureCouloir = 0.2;
    private bool               $activeZoneOuverte    = false;
    private readonly RuineGame $ruineGame;
    
    public function __construct(protected EntityManagerInterface $em,
                                protected LoggerInterface        $logger,
                                protected TranslatorInterface    $translator,
                                protected TranslateHandler       $translateHandler,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected SerializerService      $serializerService,
                                protected RandomHandler          $randomHandler,
    )
    {
        parent::__construct($em, $logger, $translator, $translateHandler, $userHandler, $gh,
                            $serializerService);
    }
    
    public function creationRuine(): RuineMazeHandler
    {
        
        $ruineMap = [];
        foreach ($this->ruineGame->getRuineGameZones() as $ruineZone) {
            if (!isset($ruineMap[$ruineZone->getZ()])) {
                $ruineMap[$ruineZone->getZ()] = [];
            }
            if (!isset($ruineMap[$ruineZone->getZ()][$ruineZone->getX()])) {
                $ruineMap[$ruineZone->getZ()][$ruineZone->getX()] = [];
            }
            if (!isset($ruineMap[$ruineZone->getZ()][$ruineZone->getX()][$ruineZone->getY()])) {
                $ruineMap[$ruineZone->getZ()][$ruineZone->getX()][$ruineZone->getY()] = $ruineZone;
            }
        }
        
        if (!isset($ruineMap[0][0][0])) {
            $this->creationRuineZone(0, 0, 0);
        }
        
        for ($x = self::ruineOffsetX; $x < (self::ruineOffsetX + self::ruineSizeX); $x++) {
            for ($y = self::ruineOffsetY; $y < self::ruineOffsetY + self::ruineSizeY; $y++) {
                for ($z = 0; $z < $this->ruineGame->getNbrEtage(); $z++) {
                    
                    if (!isset($ruineMap[$z][$x][$y])) {
                        $this->creationRuineZone($x, $y, $z);
                    }
                }
            }
        }
        return $this;
    }
    
    /**
     * @param int $etage
     * @param array $origine
     * @param int $distance_offset
     * @param bool $go_up
     * @param bool $inverserDirection
     * @param int|null $compteur_piece
     * @return RuineGameZone|null
     */
    public function generatePiece(int  $etage = 0, array $origine = [0, 0], int $distance_offset = 0,
                                  bool $go_up = false, bool $inverserDirection = false,
                                  ?int $compteur_piece = null): ?RuineGameZone
    {
        /** @var RuineGameZone[] $ruineMap */
        $ruineMap = [];
        // Obtenir un tableau à deux dimensions pour cartographier l'emplacement des couloirs
        foreach ($this->ruineGame->getRuineZonesEtage($etage) as $ruineZone) {
            if ($ruineZone->getCouloir() != RuineGameZone::CASE_VIDE) {
                if (!isset($ruineMap[$ruineZone->getX()])) {
                    $ruineMap[$ruineZone->getX()] = [];
                }
                if (!isset($ruineMap[$ruineZone->getX()][$ruineZone->getY()])) {
                    $ruineMap[$ruineZone->getX()][$ruineZone->getY()] = $ruineZone;
                }
            }
        }
        
        // Retourne vrai si les coordonnées données pointent vers un couloir existant
        $zone_exists = function (int $x, int $y) use (&$ruineMap): bool {
            return (isset($ruineMap[$x][$y]));
        };
        
        // Ajoutons des pièces !
        $distance_piece      = self::distance_min_portes + $distance_offset;
        $distance_piece_lock = self::distance_max_portes_closed;
        $piece_dist_entre    = self::distance_min_entre_portes;
        
        // Candidats à accueillir une piece
        $zone_candidates = [];
        foreach ($ruineMap as $x => $line) {
            foreach ($line as $y => $ruineZone) {
                if ($zone_exists($x, $y) && ($x * $y !== 0) && $ruineZone->getDistance() > $distance_piece) {
                    $zone_candidates[] = $ruineZone;
                }
            }
        }
        
        // Récupérer les types de pieces → dans un premier temps, on ne récupère que les pièces ouvertes et les escaliers
        $piece_fermee_type   = $this->em->getRepository(RuineGameZonePrototype::class)->findClosed();
        $piece_ouverte_types = $this->em->getRepository(RuineGameZonePrototype::class)->findOuverte();
        $esc_up_types        = $this->em->getRepository(RuineGameZonePrototype::class)->findUp();
        $esc_down_types      = $this->em->getRepository(RuineGameZonePrototype::class)->findDown();
        
        $up_count   = $go_up ? 1 : 0;
        $down_count = $etage > 0 ? 1 : 0;
        
        /** @var RuineGameZone[] $zone_esc_up */
        $zone_esc_up     = [];
        $position_esc_up = null;
        
        $compteur_piece_fermer = 2; // Pour l'instant, on ne fait pas de piece fermer
        
        while (($compteur_piece > 0 || $up_count > 0 || $down_count > 0) && !empty($zone_candidates)) {
            $place_down = $compteur_piece === 0 && $down_count > 0;
            $place_up   = !$place_down && $up_count > 0;
            
            /** @var RuineGameZone[] $eq_zone_candidates */
            $eq_zone_candidates =
            $zone_candidates = array_filter($zone_candidates, function (RuineGameZone $r) use ($piece_dist_entre) {
                return $r->getDistancePiece() >= $piece_dist_entre;
            });
            
            if ($place_up) {
                
                $zone_esc_up = array_filter($zone_candidates, function (RuineGameZone $r) use ($distance_piece_lock) {
                    return $r->getDistance() > $distance_piece_lock;
                });
            } elseif (!$down_count) {
                // Pour garantir une répartition équitable des pièces, déterminez une distance pour la pièce de manière aléatoire, puis placez la pièce dans une zone correspondant à cette distance.
                $max_distance =
                    array_reduce($zone_candidates, fn($carry, RuineGameZone $r) => max($r->getDistance(), $carry), 0);
                $safeguard    = 0; // Veille à ce qu'il n'y ait jamais de boucle sans fin
                do {
                    $safeguard++;
                    $target_distance    = match (true) {
                        $etage === 0 && $compteur_piece_fermer > 0 &&
                        $distance_piece_lock >= $distance_piece => mt_rand($distance_piece, $distance_piece_lock),
                        $compteur_piece <= 1                    => mt_rand(max($distance_piece, $max_distance - 2),
                                                                           $max_distance),
                        default                                 => mt_rand($distance_piece, $max_distance)
                    };
                    $eq_zone_candidates =
                        array_filter($zone_candidates, fn(RuineGameZone $r) => $r->getDistance() === $target_distance);
                } while (empty($eq_zone_candidates) && $safeguard < 20);
                if (empty($eq_zone_candidates)) {
                    $eq_zone_candidates = $zone_candidates;
                }
            }
            
            /** @var RuineGameZone $room_corridor */
            $room_corridor = $place_down ? $ruineMap[$origine[0]][$origine[1]] :
                $this->randomHandler->pick($place_up && !empty($zone_esc_up) ? $zone_esc_up : $eq_zone_candidates);
            $far           = $room_corridor->getDistance() > $distance_piece_lock;
            if (!$far) {
                $compteur_piece_fermer--;
            }
            
            // Détermine la position pour la porte
            $positions_valide = [];
            if ($room_corridor->hasDirCouloir(RuineGameZone::DIRECTION_E)) {
                $positions_valide = array_merge($positions_valide, [3, 8]);
            } else {
                $positions_valide[] = 5;
            }
            if ($room_corridor->hasDirCouloir(RuineGameZone::DIRECTION_O)) {
                $positions_valide = array_merge($positions_valide, [1, 6]);
            } else {
                $positions_valide[] = 4;
            }
            if (!$room_corridor->hasDirCouloir(RuineGameZone::DIRECTION_N)) {
                $positions_valide[] = 2;
            }
            if (!$room_corridor->hasDirCouloir(RuineGameZone::DIRECTION_S)) {
                $positions_valide[] = 7;
            }
            
            
            $room_corridor->setDistancePiece(0)->setPositionPorte($this->randomHandler->pick($positions_valide));
            
            if ($place_down) {
                $room_corridor->setTypeCase($this->randomHandler->pick($inverserDirection ? $esc_up_types :
                                                                           $esc_down_types))->setConnect(-1);
            } elseif ($place_up) {
                $room_corridor->setTypeCase($this->randomHandler->pick($inverserDirection ? $esc_down_types :
                                                                           $esc_up_types))->setConnect(1);
            } else {
                $room_corridor->setTypeCase($this->randomHandler->pick($far ? $piece_fermee_type :
                                                                           $piece_ouverte_types))->setClosed($far)
                              ->setPieceFouille(false);
            }
            
            
            $this->calculDistance($ruineMap,
                function (RuineGameZone $r): int {
                    return $r->getDistancePiece();
                },
                function (RuineGameZone $r, int $i): void {
                    $r->setDistancePiece($i);
                },
            );
            
            if ($place_up) {
                $position_esc_up = $room_corridor;
            }
            
            if ($place_down) {
                $down_count--;
            } elseif ($place_up) {
                $up_count--;
            } else {
                $compteur_piece--;
            }
        }
        
        // Placement des décorations
        foreach ($ruineMap as $x => $line) {
            /**
             * @var RuineGameZone $ruineZone
             */
            foreach ($line as $y => $ruineZone) {
                // Get decal value
                $filtre_decoration = 0;
                
                // Determine possible locations for the decal
                if (!$ruineZone->hasDirCouloir(RuineGameZone::DIRECTION_E)) {
                    $filtre_decoration |= ((1 << 6) + (1 << 8) + (1 << 12));
                }
                if (!$ruineZone->hasDirCouloir(RuineGameZone::DIRECTION_O)) {
                    $filtre_decoration |= ((1 << 3) + (1 << 7) + (1 << 9));
                }
                if (!$ruineZone->hasDirCouloir(RuineGameZone::DIRECTION_N)) {
                    $filtre_decoration |= ((1 << 0) + (1 << 1) + (1 << 2));
                }
                if (!$ruineZone->hasDirCouloir(RuineGameZone::DIRECTION_S)) {
                    $filtre_decoration |= ((1 << 13) + (1 << 14) + (1 << 15));
                }
                
                // On ne place pas de décoration sur l'entrée
                if ($x === 0 && $y === 0) {
                    $filtre_decoration |= ((1 << 4) + (1 << 5));
                }
                
                // On ne pose pas de décoration sur les portes
                switch ($ruineZone->getPositionPorte()) {
                    case 1:
                        $filtre_decoration |= (1 << 3);
                        break;
                    case 3:
                        $filtre_decoration |= (1 << 6);
                        break;
                    case 6:
                        $filtre_decoration |= (1 << 9);
                        break;
                    case 8:
                        $filtre_decoration |= (1 << 12);
                        break;
                }
                
                $ruineZone->setDecoUnified($ruineZone->getDecoUnified() & (~$filtre_decoration));
            }
        }
        
        
        return $position_esc_up;
    }
    
    public function generateRuineGame(): self
    {
        $this->reinitContenuRuine();
        
        $nbEtages = $this->ruineGame->getNbrEtage();
        
        $origine       = [0, 0];
        $offsetOrigine = 0;
        
        $total_pieces = 10 + (5 * ($nbEtages - 1));
        $min_piece    = 5;
        
        // Calculate rooms per level
        $pieces_etage = [];
        for ($i = 0; $i < $nbEtages; $i++) {
            $pieces_etage[$i] = $min_piece;
            $total_pieces     -= $min_piece;
        }
        while ($total_pieces > 0) {
            $pieces_etage[mt_rand(0, $nbEtages - 1)]++;
            $total_pieces--;
        }
        
        for ($i = 0; $i < $nbEtages; $i++) {
            $this->generationRuine($i, $origine, $offsetOrigine);
            $zoneOrigine =
                $this->generatePiece($i, $origine, $offsetOrigine, $i < ($nbEtages - 1), false, $pieces_etage[$i]);
            if (!$zoneOrigine) {
                break;
            }
            $origine       = [$zoneOrigine->getX(), $zoneOrigine->getY()];
            $offsetOrigine += $zoneOrigine->getDistance() + 1;
        }
        
        $this->populationZombieRuine($this->ruineGame, $this->ruineGame->getNbrZombie() * $nbEtages);
        
        return $this;
    }
    
    /**
     * @param CaseRuineGame[][][] $planRuines
     */
    public function getPlanRuineExplo(array &$planRuines, RuineGame $ruineGame): void
    {
        foreach ($ruineGame->getRuineGameZones() as $ruineZone) {
            if ($ruineZone->isCaseVu()) {
                $caseRuine = $planRuines[$ruineZone->getZ()][$ruineZone->getY()][$ruineZone->getX() -
                                                                                 RuineMazeHandler::ruineOffsetX];
                $caseRuine->setCaseVue($ruineZone->isCaseVu())
                          ->setTypeCase($ruineZone->getCouloir())
                          ->setNbrZombie($ruineZone->getZombies())
                          ->setTypePorte(($ruineZone->getTypeCase()?->getLevelZone() === 0) ?
                                             $ruineZone->getTypeCase()?->getTypeCase() : null)
                          ->setTypeEscalier(($ruineZone->getTypeCase() !== null &&
                                             $ruineZone->getTypeCase()->getLevelZone() !== 0) ?
                                                (($ruineZone->getTypeCase()->getLevelZone() === 1) ? 'up' : 'down') :
                                                null)
                          ->setDecoration($ruineZone->getDecoration())
                          ->setDecorationVariation($ruineZone->getDecoUnified())
                          ->setPositionPorte($ruineZone->getPositionPorte())
                          ->setPorteFouillee($ruineZone->isPieceFouille())
                          ->setZombieKill($ruineZone->getZombieInitial() - $ruineZone->getZombies());
            }
            
        }
    }
    
    /**
     * @return CaseRuineGame[]
     */
    public function getPlanRuineStandardiser(): array
    {
        $ruineZones = $this->ruineGame->getRuineGameZones();
        $ruineCases = [];
        /** @var RuineGameZone[] $ruineMap */
        $ruineMap = [];
        foreach ($ruineZones as $ruineZone) {
            $ruineMap[$ruineZone->getZ()][$ruineZone->getY()][$ruineZone->getX()] = $ruineZone;
        }
        
        for ($z = 0; $z < $this->ruineGame->getNbrEtage(); $z++) {
            for ($y = 0; $y < 14; $y++) {
                for ($x = 0; $x < 15; $x++) {
                    $ruineCase = new CaseRuineGame();
                    
                    if (!isset($ruineMap[$z][$y][$x + self::ruineOffsetX])) {
                        $ruineCase->setX($x)
                                  ->setY($y)
                                  ->setZ($z)
                                  ->setTypeCase(RuinesCases::CASE_VIDE)
                                  ->setNbrZombie(null)
                                  ->setTypePorte(null)
                                  ->setTypeEscalier(null);
                        $ruineCases[$z][$y][$x] = $ruineCase;
                        continue;
                    } else {
                        /** @var RuineGameZone $zoneActuel */
                        $zoneActuel = $ruineMap[$z][$y][$x + self::ruineOffsetX];
                        $ruineCase->setX($x)
                                  ->setY($y)
                                  ->setZ($z)
                                  ->setCaseVue($zoneActuel->isCaseVu())
                                  ->setTypeCase($zoneActuel->getCouloir())
                                  ->setNbrZombie($zoneActuel->getZombies())
                                  ->setTypePorte(($zoneActuel->getTypeCase()?->getLevelZone() === 0) ?
                                                     $zoneActuel->getTypeCase()?->getTypeCase() : null)
                                  ->setTypeEscalier(($zoneActuel->getTypeCase() !== null &&
                                                     $zoneActuel->getTypeCase()->getLevelZone() !== 0) ?
                                                        (($zoneActuel->getTypeCase()->getLevelZone() === 1) ? 'up' :
                                                            'down') : null)
                                  ->setDecoration($zoneActuel->getDecoration())
                                  ->setDecorationVariation($zoneActuel->getDecoUnified())
                                  ->setPositionPorte($zoneActuel->getPositionPorte())
                                  ->setPorteFouillee($zoneActuel->isPieceFouille())
                                  ->setZombieKill($zoneActuel->getZombieInitial() - $zoneActuel->getZombies());
                        
                        if ($x === 0 && $y === 0 && $z === 0) {
                            $ruineCase->setTypeCase(RuinesCases::CASE_ENTRE);
                        }
                    }
                    
                    
                    $ruineCases[$z][$y][$x] = $ruineCase;
                    
                }
            }
        }
        
        
        return $ruineCases;
    }
    
    /**
     * @return CaseRuineGame[][][]
     */
    public function getPlanRuineVide(): array
    {
        $ruineCases = [];
        
        for ($z = 0; $z < $this->ruineGame->getNbrEtage(); $z++) {
            for ($y = 0; $y < 14; $y++) {
                for ($x = 0; $x < 15; $x++) {
                    $ruineCase = new CaseRuineGame();
                    
                    if ($x === -self::ruineOffsetX && $y === 0 && $z === 0) {
                        $ruineCase->setX($x)
                                  ->setY($y)
                                  ->setZ($z)
                                  ->setTypeCase(RuinesCases::CASE_ENTRE)
                                  ->setNbrZombie(0)
                                  ->setTypePorte(null)
                                  ->setTypeEscalier(null);
                    } else {
                        $ruineCase->setX($x)
                                  ->setY($y)
                                  ->setZ($z)
                                  ->setTypeCase(RuinesCases::CASE_VIDE)
                                  ->setNbrZombie(null)
                                  ->setTypePorte(null)
                                  ->setTypeEscalier(null);
                        $ruineCases[$z][$y][$x] = $ruineCase;
                        continue;
                    }
                    
                    
                    $ruineCases[$z][$y][$x] = $ruineCase;
                    
                }
            }
        }
        
        
        return $ruineCases;
    }
    
    /**
     * @return RuineGame
     */
    public function getRuineGame(): RuineGame
    {
        return $this->ruineGame;
    }
    
    /**
     * @param RuineGame $ruineGame
     * @return RuineMazeHandler
     */
    public function setRuineGame(RuineGame $ruineGame): RuineMazeHandler
    {
        $this->ruineGame = $ruineGame;
        return $this;
    }
    
    /**
     * @param RuineGame $ruineGame
     * @param int $zombies
     */
    public function populationZombieRuine(RuineGame $ruineGame, int $zombies): void
    {
        /** @var RuineGameZone[] $ruineZones */
        $ruineZones = $ruineGame->getRuineGameZones()->getValues();
        foreach ($ruineZones as $ruineZone) {
            $zombies += $ruineZone->getZombies();
            if ($ruineZone->getCouloir() !== RuineGameZone::CASE_VIDE) {
                $ruineZone->setZombies(0)->setZombieInitial(0);
            }
        }
        
        // Il suffit d'examiner les zones de ruines concernées.
        $ruineZones = array_filter($ruineZones, function (RuineGameZone $r) {
            return $r->getCouloir() > 0 && $r->getZombies() < 4 && ($r->getX() !== 0 || $r->getY() !== 0);
        });
        shuffle($ruineZones);
        
        while ($zombies > 0 && !empty($ruineZones)) {
            $zone_actuel = array_pop($ruineZones);
            $spawn       = mt_rand(1, min($zombies, 4 - $zone_actuel->getZombies()));
            $nb_zombie   = $zone_actuel->getZombies() + $spawn;
            $zone_actuel->setZombies($nb_zombie)->setZombieInitial($nb_zombie);
            $zombies -= $spawn;
        }
        
    }
    
    public function redefineCouloirEntree(): void
    {
        $ruineZones = $this->ruineGame->getRuineGameZones();
        
        foreach ($ruineZones as $ruineZone) {
            if ($ruineZone->getX() === 0 && $ruineZone->getY() === 0 && $ruineZone->getZ() === 0) {
                $ruineZone->setCouloir(RuineGameZone::CASE_ENTRE);
            }
        }
    }
    
    /**
     * @param RuineGameZone[][] $ruineMap
     * @param callable $get_dist
     * @param callable $set_dist
     */
    private function calculDistance(array $ruineMap, callable $get_dist, callable $set_dist): void
    {
        
        /**
         * @param array $rm
         * @param RuineGameZone $r
         * @return RuineGameZone[]
         */
        $get_voisins = function (array $rm, RuineGameZone $r): array {
            $voisins = [];
            if ($r->getCouloir() === 0) {
                return $voisins;
            }
            if ($r->hasDirCouloir(RuineGameZone::DIRECTION_E)) {
                $voisins[] = $rm[$r->getX() + 1][$r->getY()];
            }
            if ($r->hasDirCouloir(RuineGameZone::DIRECTION_O)) {
                $voisins[] = $rm[$r->getX() - 1][$r->getY()];
            }
            if ($r->hasDirCouloir(RuineGameZone::DIRECTION_S)) {
                $voisins[] = $rm[$r->getX()][$r->getY() + 1];
            }
            if ($r->hasDirCouloir(RuineGameZone::DIRECTION_N)) {
                $voisins[] = $rm[$r->getX()][$r->getY() - 1];
            }
            return $voisins;
        };
        
        /** @var RuineGameZone[] $distance_array */
        $distance_array = [];
        
        // Identifier les puits
        foreach ($ruineMap as $line) {
            foreach ($line as $zone) {
                $dist = $get_dist($zone);
                foreach ($get_voisins($ruineMap, $zone) as $n_zone) {
                    if ($get_dist($n_zone) > ($dist + 1)) {
                        $distance_array[] = $zone;
                        continue 2;
                    }
                }
                
            }
        }
        
        // Calculate distances
        while (!empty($distance_array)) {
            $zoneActuelle = array_pop($distance_array);
            $dist         = $get_dist($zoneActuelle);
            
            foreach ($get_voisins($ruineMap, $zoneActuelle) as $voisin) {
                if ($get_dist($voisin) > ($dist + 1)) {
                    $set_dist($voisin, $dist + 1);
                    $distance_array[] = $voisin;
                }
            }
        }
        
    }
    
    private function creationRuineZone(int $x, int $y, int $z): void
    {
        $this->ruineGame->addRuineGameZone((new RuineGameZone())
                                               ->setCouloir(RuineGameZone::CASE_VIDE)
                                               ->setY($y)
                                               ->setX($x)
                                               ->setZ($z)
                                               ->setZombies(null));
    }
    
    /**
     * @param int $etage
     * @param array $origine
     * @param int $distance_offset
     */
    private function generationRuine(int $etage = 0, array $origine = [0, 0], int $distance_offset = 0): void
    {
        
        /** @var RuineGameZone[][] $ruineTemp */
        $ruineTemp = [];
        $isCouloir = [];
        foreach ($this->ruineGame->getRuineZonesEtage($etage) as $ruineGameZone) {
            if (!isset($ruineTemp[$ruineGameZone->getX()])) {
                $ruineTemp[$ruineGameZone->getX()] = [];
            }
            if (!isset($ruineTemp[$ruineGameZone->getX()][$ruineGameZone->getY()])) {
                $ruineTemp[$ruineGameZone->getX()][$ruineGameZone->getY()] = $ruineGameZone;
                $isCouloir[$ruineGameZone->getX()][$ruineGameZone->getY()] = false;
            }
        }
        
        $isCouloir[$origine[0]][$origine[1]] = true;
        
        // Retourne vrai si les coordonnées données pointent vers une zone existante
        $zone_existe = function (int $x, int $y) use (&$ruineTemp): bool {
            return (isset($ruineTemp[$x][$y]));
        };
        
        // Retourne vrai si les coordonnées données pointent vers une zone existante et si la zone est déjà marquée comme un corridor.
        $couloir_existe = function (int $x, int $y) use (&$isCouloir, &$zone_existe): bool {
            return $zone_existe($x, $y) && $isCouloir[$x][$y];
        };
        
        $zoneActuelle                        = $ruineTemp[$origine[0]][$origine[1]];
        $isCouloir[$origine[0]][$origine[1]] = true;
        
        $explorateur   = [];
        $explorateur[] = $zoneActuelle;
        
        $directions = [[1, 0], [0, 1], [-1, 0], [0, -1]];
        $dTime      = 0;
        
        // On reprend l'algo de MH qui a repris de la MT pour générer des ruines similaires
        while (count($explorateur) > 0) {
            // Parfois, nous changeons l'ordre dans lequel les directions sont vérifiées
            $dTime--;
            if ($dTime < 0) {
                $indexChangement = 1 + mt_rand(0, 2);
                $d               = $directions[$indexChangement];
                array_splice($directions, $indexChangement, 1);
                array_unshift($directions, $d);
                $dTime = self::minDirection + mt_rand(0, 1);
            }
            
            $zoneSuivante = null;
            // Vérifier toutes les directions
            foreach ($directions as $direction) {
                // Parfois, nous sautons une direction
                if ((mt_rand() / mt_getrandmax()) < $this->probaSautRuine) {
                    continue;
                }
                
                $tempX = $zoneActuelle->getX() + $direction[0];
                $tempY = $zoneActuelle->getY() + $direction[1];
                // La direction que nous vérifions a-t-elle des coordonnées valides ?
                if ($zone_existe($tempX, $tempY)) {
                    $zoneSuivante = $ruineTemp[$tempX][$tempY];
                    // Les données à la coordonnée dans la direction que nous vérifions sont-elles un mur ?
                    if (!$isCouloir[$tempX][$tempY]) {
                        $countMur = 0;
                        $murDirX  = 0;
                        $murDirY  = 0;
                        // Nous vérifions les murs autour du cas que nous voulons vérifier, nous les comptons et nous suivons l'axe dans lequel nous les avons trouvés.
                        foreach ($directions as $direction2) {
                            $tempY = $zoneSuivante->getY() + $direction2[1];
                            $tempX = $zoneSuivante->getX() + $direction2[0];
                            if (!$zone_existe($tempX, $tempY) || !$isCouloir[$tempX][$tempY]) {
                                $countMur++;
                                $murDirX += $direction2[0] * $direction2[0];
                                $murDirY += $direction2[1] * $direction2[1];
                            }
                        }
                        
                        // Si nous nous engageons sur une autre voie
                        if ($countMur < 3) {
                            // * L'aléatoire permet la pause
                            // * ET
                            // *** Les zones ouvertes sont possibles
                            // *** OU la rupture se produit sur un seul axe
                            if ((mt_rand() / mt_getrandmax()) < $this->probaJointureCouloir &&
                                ($this->activeZoneOuverte || ($countMur == 2 && ($murDirX == 0 || $murDirY == 0)))) {
                                $isCouloir[$zoneSuivante->getX()][$zoneSuivante->getY()] = true;
                            }
                            // Fin du chemin actuel.
                            $zoneSuivante = null;
                        }
                    } else {
                        // Fin du chemin actuel.
                        $zoneSuivante = null;
                    }
                }
                
                // Il n'est pas nécessaire de vérifier d'autres directions si nous avons trouvé un chemin potentiel.
                if ($zoneSuivante != null) {
                    break;
                }
            }
            if ($zoneSuivante == null) {
                // Nous revenons sur le chemin que nous étions en train de construire, en vérifiant si les cellules précédentes ont la possibilité de commencer un nouveau chemin.
                $zoneActuelle = array_pop($explorateur);
                $dTime        = 0;
            } else {
                // Nous continuons sur notre lancée.
                $isCouloir[$zoneSuivante->getX()][$zoneSuivante->getY()] = true;
                $explorateur[]                                           = $zoneSuivante;
                $zoneActuelle                                            = $zoneSuivante;
            }
        }
        
        // Build the actual map
        foreach ($ruineTemp as $x => $line) {
            foreach ($line as $y => $ruinZone) {
                if ($couloir_existe($x, $y)) {
                    if ($couloir_existe($x + 1, $y)) {
                        $ruinZone->addCouloir(RuineGameZone::DIRECTION_E);
                    }
                    if ($couloir_existe($x - 1, $y)) {
                        $ruinZone->addCouloir(RuineGameZone::DIRECTION_O);
                    }
                    if ($couloir_existe($x, $y + 1)) {
                        $ruinZone->addCouloir(RuineGameZone::DIRECTION_S);
                    }
                    if ($couloir_existe($x, $y - 1)) {
                        $ruinZone->addCouloir(RuineGameZone::DIRECTION_N);
                    }
                    $ruinZone->setCaseVu(false);
                } else {
                    $ruinZone->setCouloir(RuineGameZone::CASE_VIDE);
                }
            }
        }
        
        // Calculate distances
        $ruineTemp[$origine[0]][$origine[1]]->setDistance($distance_offset);
        $this->calculDistance($ruineTemp,
            function (RuineGameZone $r): int {
                return $r->getDistance();
            },
            function (RuineGameZone $r, int $i): void {
                $r->setDistance($i);
            },
        );
    }
    
    private function initRuineGameZone(RuineGameZone $ruineGameZone): void
    {
        $ruineGameZone->setCouloir(RuineGameZone::CASE_VIDE)
                      ->setConnect(0)
                      ->setDistance(9999)
                      ->setDistancePiece(9999)
                      ->setTypeCase(null)
                      ->setClosed(false)
                      ->setPositionPorte(0)
                      ->setDecoUnified(mt_rand(0, 0xFFFFFFFF))
                      ->setZombies(0)
                      ->setZombieInitial(0);
        
    }
    
    private function reinitContenuRuine(): void
    {
        foreach ($this->ruineGame->getRuineGameZones() as $ruineGameZone) {
            $this->initRuineGameZone($ruineGameZone);
        }
    }
    
    
}