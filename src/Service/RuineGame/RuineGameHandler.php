<?php

namespace App\Service\RuineGame;

use App\Entity\RuineGame;
use App\Entity\RuineGameZone;
use App\Entity\RuineGameZonePrototype;
use App\Entity\RuinesCases;
use App\Exception\GestHordesException;
use App\Service\AbstractGHHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\RuineGame\CaseRuineGame;
use App\Structures\Dto\GH\RuineGame\DeplacementPieceRuineGame;
use App\Structures\Dto\GH\RuineGame\DeplacementRuineGame;
use App\Structures\Dto\GH\RuineGame\EndGameRuineGame;
use App\Structures\Dto\GH\RuineGame\FouillePieceRuineGame;
use App\Structures\Dto\GH\RuineGame\FuiteRuineGame;
use App\Structures\Dto\GH\RuineGame\KillZombieRuineGame;
use App\Structures\Dto\GH\RuineGame\NiveauDifficulte;
use App\Structures\Dto\GH\RuineGame\Response\DeplacementRuineResponse;
use App\Structures\Dto\GH\RuineGame\Response\EndGameResponse;
use App\Structures\Dto\GH\RuineGame\Response\HistoriqueRuineGameResponse;
use App\Structures\Dto\GH\RuineGame\Response\KillZombieResponse;
use App\Structures\Dto\GH\RuineGame\Response\MainGameRuineResponse;
use App\Structures\Dto\GH\RuineGame\Response\RuineMazeResponse;
use App\Structures\Dto\GH\RuineGame\Response\RuineOldMazeResponse;
use App\Structures\Dto\GH\RuineGame\ScoreRuineGame;
use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 *
 */
class RuineGameHandler extends AbstractGHHandler
{
    public function __construct(protected EntityManagerInterface $em,
                                protected LoggerInterface        $logger,
                                protected TranslatorInterface    $translator,
                                protected TranslateHandler       $translateHandler,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected SerializerService      $serializerService,
                                protected RuineMazeHandler       $ruineMazeHandler)
    {
        parent::__construct($em, $logger, $translator, $translateHandler, $userHandler, $gh,
                            $serializerService);
    }
    
    /**
     * @throws Exception
     */
    public function calculDurationRuineGame(int $oxygen, DateTimeImmutable $startGame): DateTimeImmutable
    {
        // On calcule la durée de la partie en fonction de la quantité d'oxygène → 1 oxygène = 3 secondes
        $duration = $oxygen * 3;
        return $startGame->add(new DateInterval('PT' . $duration . 'S'));
    }
    
    public function calculManaKillZombie(int $nbZombie, int $etage, int $oxygene): int
    {
        // On calcule le mana de départ en fonction du nombre de zombies, de l'étage et de la quantité d'oxygène
        $zombieTotal  = $etage * $nbZombie;
        $bonusOxygene = floor(($oxygene < 100 ? 1.2 : 0.25) * (100 - $oxygene));
        $bonusZombie  = $zombieTotal * 1.5;
        
        $totalBonus = $bonusZombie + $bonusOxygene;
        
        return ($totalBonus < $zombieTotal) ? $zombieTotal : $totalBonus;
    }
    
    /**
     * @throws Exception
     */
    public function calculOxygeneRestant(RuineGame         $ruineGame, DateTimeImmutable $startGame,
                                         DateTimeImmutable $endGame): int
    {
        // On calcule l'oxygène utilisé en fonction de la durée de la partie
        $duration = $endGame->getTimestamp() - $startGame->getTimestamp();
        // On applique une petite tolérance de 6 secondes supplémentaires
        $duration += 6;
        // 1 oxygène = 3 secondes
        
        $oxygeneUsed = floor($duration / 3);
        return max($ruineGame->getQteOxygene() - $oxygeneUsed, 0);
    }
    
    public function calculPerteOxygeneEscalier(): int
    {
        return rand(0, 3) + 5;
    }
    
    public function calculPerteOxygeneFuite(): int
    {
        return rand(0, 3) + 5;
    }
    
    public function calculScoreRuineGame(RuineGame $ruineGame): ScoreRuineGame
    {
        $scoreGame = new ScoreRuineGame();
        
        $zombieInitial = $ruineGame->getNbrZombie() * $ruineGame->getNbrEtage();
        $porteInitial  = 10 + (($ruineGame->getNbrEtage() - 1) * 5);
        $nombreCase    = 0;
        // On balaye chaque case de la ruinemap pour calculer le score
        $nbZombieKill    = 0;
        $nbPorteFouillee = 0;
        $nbCaseExploree  = 0;
        
        foreach ($ruineGame->getRuineGameZones() as $zone) {
            if ($zone->getCouloir() !== RuineGameZone::CASE_VIDE) {
                $nombreCase++;
                $nbZombieKill    += $zone->getZombieInitial() - $zone->getZombies();
                $nbPorteFouillee += $zone->isPieceFouille() ? 1 : 0;
                $nbCaseExploree  += $zone->isCaseVu() ? 1 : 0;
            }
        }
        
        $scoreGame->setPctKill($nbZombieKill > 0 ? floor(($nbZombieKill / $zombieInitial) * 100) : 0);
        $scoreGame->setPctFouille($nbPorteFouillee > 0 ? floor(($nbPorteFouillee / $porteInitial) * 100) : 0);
        $scoreGame->setPctExplo($nbCaseExploree > 0 ? floor(($nbCaseExploree / $nombreCase) * 100) : 0);
        
        return $scoreGame;
    }
    
    /**
     * @return NiveauDifficulte[]
     */
    public function getNiveauDifficulteRuine(): array
    {
        // On récupère les niveaux de difficulté sur la quantité d'oxygène et le nombre d'étages
        $qqteOxygene = [
            NiveauDifficulte::O2_ULTRA_FACILE,
            NiveauDifficulte::O2_TRES_FACILE,
            NiveauDifficulte::O2_FACILE,
            NiveauDifficulte::O2_MOYEN,
            NiveauDifficulte::O2_DIFFICILE,
        ];
        // générer les étages dans un tableau
        $etagesRuine = [];
        for ($i = NiveauDifficulte::MIN_ETAGE; $i <= NiveauDifficulte::MAX_ETAGE; $i++) {
            $etagesRuine[] = $i;
        }
        // générer nombre de zombies dans la ruine
        $nbZombieRuine = [0, 10, 15, 20, 25, 30, 35, 40, 45, 50];
        
        // Liste des types de plan possible pour la ruine
        $withPlanRuine = [
            NiveauDifficulte::PLAN_COMPLET => "Plan complet",
            NiveauDifficulte::PLAN_TRACE   => "Plan autotracé",
            NiveauDifficulte::SANS_PLAN    => "Sans plan",
        ];
        return [
            'oxygene' => $qqteOxygene,
            'etages'  => $etagesRuine,
            'zombie'  => $nbZombieRuine,
            'plan'    => $withPlanRuine,
        ];
    }
    
    public function getRuineGame(string $idRuine): ?RuineGame
    {
        return $this->em->getRepository(RuineGame::class)->findOneBy(['id' => $idRuine]);
    }
    
    /**
     * Retourne le type de la ruine aléatoirement lors de la génération : bunker, hopital, hotel
     * @return string
     */
    public function getTypeRuineRandom(): string
    {
        $typesRuine = [
            'bunker',
            'hopital',
            'hotel',
        ];
        return $typesRuine[array_rand($typesRuine)];
    }
    
    public function isKillZombie(): bool
    {
        return rand(0, 100) > 25;
    }
    
    /**
     * @throws GestHordesException
     * @throws Exception
     */
    public function traitementDeplacementPiece(DeplacementPieceRuineGame $deplacementPieceRuineGame): void
    {
        $user = $this->userHandler->getCurrentUser();
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($deplacementPieceRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$deplacementPieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($deplacementPieceRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$deplacementPieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie l'existance de la zone de la ruine
        $ruineGameZone = $this->verifyRuineGameZone($deplacementPieceRuineGame->getIdRuineGame(),
                                                    $deplacementPieceRuineGame->getCase());
        if ($ruineGameZone === null) {
            throw new GestHordesException("La zone de la ruine n'existe pas - partie : {$deplacementPieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien un couloir → tentative de triche
        if ($ruineGameZone->getCouloir() === null || $ruineGameZone->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case de la ruine n'est pas un couloir - partie : {$deplacementPieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien une piece ouverte
        if ($ruineGameZone->getTypeCase() !== null &&
            $ruineGameZone->getTypeCase()->getTypeCase() !== RuineGameZonePrototype::piece_ouverte) {
            throw new GestHordesException("La case de la ruine n'a pas de piece ouverte - partie : {$deplacementPieceRuineGame->getIdRuineGame()}");
        }
        
        // On met à jour que le joueur est entrée ou sortie de la piece
        $ruineGame->setInPiece($deplacementPieceRuineGame->isInPiece());
        
        $this->em->persist($ruineGame);
        $this->em->flush();
    }
    
    /**
     * @throws GestHordesException
     * @throws Exception
     */
    public function traitementDeplacementRuine(DeplacementRuineGame $deplacementRuineGame): DeplacementRuineResponse
    {
        $user = $this->userHandler->getCurrentUser();
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($deplacementRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($deplacementRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie l'existance de la zone de la ruine pour la case de départ et d'arrivée
        $ruineGameZone =
            $this->verifyRuineGameZone($deplacementRuineGame->getIdRuineGame(), $deplacementRuineGame->getCase());
        if ($ruineGameZone === null) {
            throw new GestHordesException("La zone de départ de la ruine n'existe pas - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        $ruineGameZoneArrive = $this->verifyRuineGameZone($deplacementRuineGame->getIdRuineGame(),
                                                          $deplacementRuineGame->getCaseSuivante());
        if ($ruineGameZoneArrive === null) {
            throw new GestHordesException("La zone d'arrivée de la ruine n'existe pas - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien un couloir → tentative de triche en passant à travers les murs
        if ($ruineGameZone->getCouloir() === null || $ruineGameZone->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case de depart de la ruine n'est pas un couloir - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        if ($ruineGameZoneArrive->getCouloir() === null ||
            $ruineGameZoneArrive->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case d'arrivée de la ruine n'est pas un couloir - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        
        // On met à jour que la case a été vue et on autorise le déplacement vers la case suivante en renvoyant une réponse OK
        $ruineGameZone->setCaseVu(true);
        $ruineGameZoneArrive->setCaseVu(true);
        
        // Si on change d'étage, on calcule la perte d'oxygène
        if ($ruineGameZone->getZ() !== $ruineGameZoneArrive->getZ()) {
            $nbr_oxy = $this->calculPerteOxygeneEscalier();
            $ruineGame->setOxygenLost($ruineGame->getOxygenLost() + $nbr_oxy);
        } else {
            $nbr_oxy = 0;
        }
        
        // S'il y a encore des zombies sur la case de départ et qu'on n'est pas en état de fuite, on met une erreur, le déplacement est impossible
        if ($ruineGameZone->getZombies() > 0 && !$deplacementRuineGame->isCaseFuite()) {
            throw new GestHordesException("Il y a encore des zombies sur la case de départ - partie : {$deplacementRuineGame->getIdRuineGame()}");
        }
        
        $ruineGame->setX($ruineGameZoneArrive->getX() - RuineMazeHandler::ruineOffsetX)
                  ->setY($ruineGameZoneArrive->getY())
                  ->setZ($ruineGameZoneArrive->getZ())
                  ->setInPiece(false);
        
        $this->em->persist($ruineGameZone);
        $this->em->persist($ruineGameZoneArrive);
        $this->em->persist($ruineGame);
        $this->em->flush();
        
        $retour = new DeplacementRuineResponse();
        $retour->setPerteOxy($nbr_oxy)
               ->setOxygenLostTotal($ruineGame->getOxygenLost());
        return $retour;
    }
    
    /**
     * @throws GestHordesException
     * @throws Exception
     */
    public function traitementEndRuineGame(EndGameRuineGame $endGameRuineGame): EndGameResponse
    {
        $endPlay = new DateTimeImmutable('now');
        $user    = $this->userHandler->getCurrentUser();
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($endGameRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($endGameRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que le temps n'a pas été trafiqué
        if ($ruineGame->getBeginAt() > $endPlay) {
            $user->setActiveRuine(null);
            $this->em->persist($user);
            $this->em->flush();
            throw new GestHordesException("La date de fin de partie est antérieure à la date de début - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        $calculEndGameTheorique = $this->calculDurationRuineGame($ruineGame->getQteOxygene(), $ruineGame->getBeginAt());
        // Si le temps de fin de jeu réel est supérieur à celui théorique avec une tolérance de 3 sec, il y a eu tricherie
        if ($endPlay > $calculEndGameTheorique->modify('+3 seconds')) {
            $user->setActiveRuine(null);
            $this->em->persist($user);
            $this->em->flush();
            throw new GestHordesException("La date de fin de partie est supérieure à la date de fin théorique - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        // On recalcule l'oxygène restant, si on trouve moins d'oxygène que celui de la partie, on met une erreur
        $calculOxygene = $this->calculOxygeneRestant($ruineGame, $ruineGame->getBeginAt(), $endPlay);
        if ($calculOxygene < ($endGameRuineGame->getOxygene() - 20)) {
            $user->setActiveRuine(null);
            $this->em->persist($user);
            $this->em->flush();
            throw new GestHordesException("Il y a trop d'oxygène de retourner par rapport à l'utilisation - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        // on vérifie que le mana fournit correspond à ce qu'on a eu en base
        if ($ruineGame->getManaKill() !== $endGameRuineGame->getMana()) {
            $user->setActiveRuine(null);
            $this->em->persist($user);
            $this->em->flush();
            throw new GestHordesException("Le mana fournit ne correspond pas à celui de la partie - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        if ($this->verifyNoCheat($endGameRuineGame->getIdRuineGame(), $endGameRuineGame->getPlanRuine())) {
            $user->setActiveRuine(null);
            $this->em->persist($user);
            $this->em->flush();
            throw new GestHordesException("La partie a été triché - partie : {$endGameRuineGame->getIdRuineGame()}");
        }
        
        // On calcule le score de chaque objectif
        $scoreGame = $this->calculScoreRuineGame($ruineGame);
        $ejected   = $endGameRuineGame->isEjecter();
        // On met à jour que la partie est terminée
        $ruineGame->setEndedAt($endPlay)
                  ->setEjected($ejected)
                  ->setOxygeneRestant($endGameRuineGame->getOxygene())
                  ->setManaRestant($endGameRuineGame->getMana())
                  ->setPctZombieKill($scoreGame->getPctKill())
                  ->setPctFouille($scoreGame->getPctFouille())
                  ->setPctExploration($scoreGame->getPctExplo());
        
        $scoreGame->setEjected($ejected)
                  ->setOxygenRestant($endGameRuineGame->getOxygene());
        
        $user->setActiveRuine(null);
        
        $this->em->persist($ruineGame);
        $this->em->persist($user);
        $this->em->flush();
        
        $retour = new EndGameResponse();
        $retour->setScore($scoreGame);
        
        return $retour;
    }
    
    /**
     * @throws GestHordesException
     * @throws Exception
     */
    public function traitementFouillePiece(FouillePieceRuineGame $fouillePieceRuineGame): void
    {
        $user = $this->userHandler->getCurrentUser();
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($fouillePieceRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$fouillePieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($fouillePieceRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$fouillePieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie l'existance de la zone de la ruine
        $ruineGameZone =
            $this->verifyRuineGameZone($fouillePieceRuineGame->getIdRuineGame(), $fouillePieceRuineGame->getCase());
        if ($ruineGameZone === null) {
            throw new GestHordesException("La zone de la ruine n'existe pas - partie : {$fouillePieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien un couloir → tentative de triche
        if ($ruineGameZone->getCouloir() === null || $ruineGameZone->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case de la ruine n'est pas un couloir - partie : {$fouillePieceRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien une piece ouverte
        if ($ruineGameZone->getTypeCase() !== null &&
            $ruineGameZone->getTypeCase()->getTypeCase() !== RuineGameZonePrototype::piece_ouverte) {
            throw new GestHordesException("La case de la ruine n'a pas de piece ouverte - partie : {$fouillePieceRuineGame->getIdRuineGame()}");
        }
        
        // On met à jour que la piece a été fouillé, on autorise de pouvoir sortir de la piece en retournant une réponse OK
        $ruineGameZone->setPieceFouille(true);
        
        $ruineGame->setInPiece(true);
        
        $this->em->persist($ruineGame);
        $this->em->persist($ruineGameZone);
        $this->em->flush();
    }
    
    /**
     * @throws GestHordesException
     * @throws Exception
     */
    public function traitementFuiteRuine(FuiteRuineGame $fuiteRuineGame): DeplacementRuineResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($fuiteRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$fuiteRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($fuiteRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$fuiteRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie l'existance de la zone de la ruine pour la case de départ et d'arrivée
        $ruineGameZone = $this->verifyRuineGameZone($fuiteRuineGame->getIdRuineGame(), $fuiteRuineGame->getCase());
        if ($ruineGameZone === null) {
            throw new GestHordesException("La zone de départ de la ruine n'existe pas - partie : {$fuiteRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la case contient bien un couloir → tentative de triche en passant à travers les murs
        if ($ruineGameZone->getCouloir() === null || $ruineGameZone->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case de depart de la ruine n'est pas un couloir - partie : {$fuiteRuineGame->getIdRuineGame()}");
        }
        
        // S'il y a encore des zombies sur la case de départ et qu'on n'est pas en état de fuite, on met une erreur, le déplacement est impossible
        if ($ruineGameZone->getZombies() > 0 && !$fuiteRuineGame->isCaseFuite()) {
            throw new GestHordesException("Il y a encore des zombies sur la case de départ - partie : {$fuiteRuineGame->getIdRuineGame()}");
        }
        
        // Si on change d'étage, on calcule la perte d'oxygène
        if ($ruineGameZone->getZombies() > 0 && $fuiteRuineGame->isCaseFuite()) {
            $nbr_oxy = $this->calculPerteOxygeneFuite();
        } else {
            $nbr_oxy = 0;
        }
        
        $ruineGame = $ruineGameZone->getRuineGame();
        $ruineGame->setOxygenLost($ruineGame->getOxygenLost() + $nbr_oxy);
        
        $this->em->persist($ruineGameZone);
        $this->em->persist($ruineGame);
        $this->em->flush();
        
        $retour = new DeplacementRuineResponse();
        $retour->setPerteOxy($nbr_oxy)
               ->setOxygenLostTotal($ruineGame->getOxygenLost());
        return $retour;
    }
    
    /**
     * @throws Exception
     */
    public function traitementGetMazeRuine(NiveauDifficulte $niveauDifficulte): RuineMazeResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        $ruineGame = new RuineGame();
        $mana      = $this->calculManaKillZombie($niveauDifficulte->getNbZombie(), $niveauDifficulte->getNombreEtage(),
                                                 $niveauDifficulte->getOxygene());
        $ruineGame->setNbrEtage($niveauDifficulte->getNombreEtage())
                  ->setNbrZombie($niveauDifficulte->getNbZombie())
                  ->setQteOxygene($niveauDifficulte->getOxygene())
                  ->setTypePlan($niveauDifficulte->getTypePlan())
                  ->setTypeRuine($this->getTypeRuineRandom())
                  ->setUser($user)
                  ->setManaKill($mana)
                  ->setManaInitial($mana)
                  ->setOxygenLost(0)
                  ->setSaison(0);
        
        $this->ruineMazeHandler->setRuineGame($ruineGame)
                               ->creationRuine()
                               ->generateRuineGame()
                               ->redefineCouloirEntree();
        
        // On effectue la sauvegarde des données de la partie afin de lancer le jeu
        // On met en place la sauvegarde de la partie
        $startGame = new DateTimeImmutable('now');
        
        $ruineGame->setBeginAt($startGame)
                  ->setEjected(false);
        
        
        $this->em->persist($ruineGame);
        $this->em->flush();
        
        // On récupère les ID générées
        $this->em->refresh($ruineGame);
        
        $user->setActiveRuine($ruineGame->getId());
        $this->em->persist($user);
        $this->em->flush();
        
        $ruineGame->setBeginAt($startGame->setTimezone(new DateTimeZone("Europe/Paris")));
        
        $retour = new RuineMazeResponse();
        $retour->setRuineMaze($this->ruineMazeHandler->getPlanRuineStandardiser())
               ->setRuineMazeVide($this->ruineMazeHandler->getPlanRuineVide())
               ->setGame($ruineGame);
        return $retour;
    }
    
    /**
     * @throws Exception
     */
    public function traitementHistoriquePartie(): HistoriqueRuineGameResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        $response = new HistoriqueRuineGameResponse();
        
        // On récupère toutes les parties du joueur en ne prenant pas les parties non finalisés suite à une erreur.
        $ruineGames = $this->em->getRepository(RuineGame::class)->createQueryBuilder('rg')->where('rg.user = :user')
                               ->andWhere('rg.endedAt IS NOT NULL')
                               ->setParameter('user', $user)
                               ->orderBy('rg.beginAt', 'DESC')
                               ->getQuery()
                               ->getResult();
        
        $listRuineGames['bunker']  = "Bunker";
        $listRuineGames['hopital'] = "Hôpital";
        $listRuineGames['hotel']   = "Hôtel";
        
        $response->setListHistorique($ruineGames)
                 ->setTraductionRuineGame($listRuineGames);
        
        return $response;
    }
    
    /**
     * @throws Exception
     */
    public function traitementKillZombie(KillZombieRuineGame $killZombieRuineGame): ?KillZombieResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        // On vérifie que le joueur connecté est bien le joueur de la partie
        if ($killZombieRuineGame->getIdUser() !== $user->getId()) {
            throw new GestHordesException("Le joueur connecté n'est pas le joueur de la partie - partie : {$killZombieRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie que la partie est bien celui du joueur
        $ruineGame = $this->verifyRuineGameIsForUser($killZombieRuineGame->getIdRuineGame(), $user->getId());
        if ($ruineGame === null) {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$killZombieRuineGame->getIdRuineGame()}");
        }
        
        // On vérifie l'existance de la zone de la ruine
        $ruineGameZone =
            $this->verifyRuineGameZone($killZombieRuineGame->getIdRuineGame(), $killZombieRuineGame->getCase());
        if ($ruineGameZone === null) {
            throw new GestHordesException("La zone de la ruine n'existe pas - partie : {$killZombieRuineGame->getIdRuineGame()}");
        }
        // On vérifie que la case contient bien un couloir → tentative de triche
        if ($ruineGameZone->getCouloir() === null || $ruineGameZone->getCouloir() === RuineGameZone::CASE_VIDE) {
            throw new GestHordesException("La case de la ruine n'est pas un couloir - partie : {$killZombieRuineGame->getIdRuineGame()}");
        }
        
        // S'il n'y a plus de zombie à tuer, on dit que c'est ok
        if ($ruineGameZone->getZombies() === 0) {
            return null;
        }
        
        // On vérifie que le joueur a bien assez de mana pour tuer les zombies
        $ruineGame = $ruineGameZone->getRuineGame();
        if ($ruineGame->getManaKill() <= 0) {
            return null;
        } else {
            $ruineGame->setManaKill($ruineGame->getManaKill() - 1);
            $this->em->persist($ruineGame);
            // on calcule si le joueur à tuer ou non le zombie
            if ($this->isKillZombie()) {
                $ruineGameZone->setZombies($ruineGameZone->getZombies() - 1);
                $this->em->persist($ruineGameZone);
            }
            $this->em->flush();
        }
        
        $retour = new KillZombieResponse();
        $retour->setZombieRestant($ruineGameZone->getZombies())
               ->setZombieKill($ruineGameZone->getZombieInitial() - $ruineGameZone->getZombies())
               ->setManaKill($ruineGame->getManaKill());
        return $retour;
    }
    
    /**
     * @throws Exception
     */
    public function traitementMain(): MainGameRuineResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        $response = new MainGameRuineResponse();
        
        // On récupère la partie en cours du joueur s'il y en a une
        if ($user->getActiveRuine() !== null) {
            $ruineGame = $this->getRuineGame($user->getActiveRuine());
            // Si la ruine existe, on vient mapper la ruine
            if ($ruineGame !== null) {
                $this->ruineMazeHandler->setRuineGame($ruineGame)
                                       ->getPlanRuineStandardiser();
                
                
                $ruineGame->setBeginAt($ruineGame->getBeginAt()->setTimezone(new DateTimeZone("Europe/Paris")));
                
                $ruineVide = $this->ruineMazeHandler->getPlanRuineVide();
                $this->ruineMazeHandler->getPlanRuineExplo($ruineVide, $ruineGame);
                
                $response->setRuineMaze($this->ruineMazeHandler->getPlanRuineStandardiser())
                         ->setRuineMazeVide($ruineVide)
                         ->setGame($ruineGame);
            }
        }
        
        $response->setTradPorte([
                                    RuinesCases::TYPE_PORTE_OUV   => "small_enter",
                                    RuinesCases::TYPE_PORTE_FERM  => "item_lock",
                                    RuinesCases::TYPE_PORTE_DECAP => "item_classicKey",
                                    RuinesCases::TYPE_PORTE_PERCU => "item_bumpKey",
                                    RuinesCases::TYPE_PORTE_MAGN  => "item_magneticKey",
                                ]);
        
        $response->setListeNiveauDifficulte($this->getNiveauDifficulteRuine())
                 ->setUserCss($user);
        
        
        return $response;
    }
    
    /**
     * @throws Exception
     */
    public function traitementMapOld(string $idRuine): RuineOldMazeResponse
    {
        $user = $this->userHandler->getCurrentUser();
        
        $response = new RuineOldMazeResponse();
        
        // On récupère la partie correspondant à l'id fourni de la partie
        $ruineGame = $this->getRuineGame($idRuine);
        
        // Si la ruine existe, on standardise le plan
        if ($ruineGame !== null) {
            $this->ruineMazeHandler->setRuineGame($ruineGame);
            
            $response->setRuinePlan($this->ruineMazeHandler->getPlanRuineStandardiser());
        } else {
            throw new GestHordesException("La partie n'existe pas ou n'est pas celle du joueur - partie : {$idRuine}");
        }
        
        
        return $response;
    }
    
    /**
     * @param string $idRuineGame
     * @param CaseRuineGame[][][] $ruineCases
     * @return bool
     */
    public function verifyNoCheat(string $idRuineGame, array $ruineCases): bool
    {
        // On vérifie que l'utilisateur n'a pas triché en vérifiant la cohérence des données
        $ruineGame = $this->getRuineGame($idRuineGame);
        foreach ($ruineGame->getRuineGameZones() as $zone) {
            $ruineCase = $ruineCases[$zone->getZ()][$zone->getY()][$zone->getX() - RuineMazeHandler::ruineOffsetX];
            if ($ruineCase->getTypeCase() !== RuineGameZone::CASE_VIDE) {
                if ($ruineCase->getZombieKill() !== $zone->getZombieInitial() - $zone->getZombies()) {
                    return true;
                }
                if ($zone->isCaseVu() !== null && $ruineCase->getCaseVue() !== $zone->isCaseVu()) {
                    return true;
                }
                if ($zone->isPieceFouille() !== null && $ruineCase->getPorteFouillee() !== $zone->isPieceFouille()) {
                    return true;
                }
            }
        }
        // Si tout est cohérent, on retourne false, il n'y a pas eu de tricherie
        return false;
    }
    
    public function verifyRuineGameIsForUser(string $idRuineGame, int $idUser): ?RuineGame
    {
        // On vérifie que la zone de la ruine appartient bien à l'utilisateur
        return $this->em->getRepository(RuineGame::class)->findOneBy(['id' => $idRuineGame, 'user' => $idUser]);
    }
    
    public function verifyRuineGameZone(string $idRuineGame, CaseRuineGame $zone): ?RuineGameZone
    {
        // On vérifie que la zone de la ruine existe bien en retournant la zone si elle existe
        return $this->em->getRepository(RuineGameZone::class)->findOneBy(['x'         => $zone->getX() +
                                                                                         RuineMazeHandler::ruineOffsetX,
                                                                          'y'         => $zone->getY(),
                                                                          'z'         => $zone->getZ(),
                                                                          'ruineGame' => $idRuineGame]);
    }
}