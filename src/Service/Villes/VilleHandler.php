<?php

namespace App\Service\Villes;

use App\Entity\Banque;
use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\Defense;
use App\Entity\Estimation;
use App\Entity\HistoriqueVille;
use App\Entity\Journal;
use App\Entity\PlansChantier;
use App\Entity\TypeCaracteristique;
use App\Entity\UpChantier;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\VilleHistorique;
use App\Entity\ZoneMap;
use App\Enum\EtatVille;
use App\Structures\Collection\Banques;
use App\Structures\Collection\Statistiques;
use App\Structures\Dto\GH\Villes\ComparatifVillesDto;
use App\Structures\Dto\GH\Villes\Objets\CarteDetailDto;
use App\Structures\Dto\GH\Villes\Objets\CarteDto;
use App\Structures\Dto\GH\Villes\Objets\CitoyensDto;
use App\Structures\Dto\GH\Villes\Objets\ProvisionsDto;
use App\Structures\Dto\GH\Villes\Objets\RessourcesDto;
use App\Structures\Dto\GH\Villes\Objets\ResumeDto;
use App\Structures\Dto\GH\Villes\Objets\VilleDto;
use DateMalformedStringException;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class VilleHandler
{
    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly TranslatorInterface    $translator)
    {
    }
    
    /**
     * @throws DateMalformedStringException
     * @throws \Exception
     */
    public function getComparaisonVille(int $idVille): ComparatifVillesDto
    {
        $comparatifVillesDto = new ComparatifVillesDto();
        
        // Recherche de la ville à partir de son id
        $ville = $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $idVille, 'origin' => Ville::ORIGIN_MH]);
        
        if ($ville !== null) {
            $comparatifVillesDto->setDerniereMaj(new DateTimeImmutable($ville->getDateTime()->format('Y-m-d H:i:s')));
            
            // Gestion des informations sur la ville
            $villeDto = new VilleDto();
            $villeDto->setNom($ville->getNom())
                     ->setMapId($ville->getMapId())
                     ->setSaison($ville->getSaison())
                     ->setJour($ville->getJour())
                     ->setEtat(EtatVille::getEtatFromVille($ville))
                     ->setPoints($ville->getNombrePointSaison())
                     ->setNomJump($ville->getJump()?->getNom())
                     ->setX($ville->getPosX())
                     ->setY($ville->getPosY());
            
            // Gestion des informations sur les citoyens
            $citoyensDto = new CitoyensDto();
            $citoyensDto->setNbVivant($ville->getNbVivants())
                        ->setNbMort($ville->getCitoyens()->count() - $ville->getNbVivants())
                        ->setNbBanni($ville->getCitoyens()->filter(fn(Citoyens $citoyen) => $citoyen->getBan() && !$citoyen->getMort())->count());
            
            // Gestion des informations sur les provisions
            $provisionsDto = new ProvisionsDto();
            $provisionsDto->setNbNourriture($this->em->getRepository(Banque::class)->somItemBanqueOneTypeCarac($ville, TypeCaracteristique::NOURRITURE))
                          ->setNbEau($this->em->getRepository(Banque::class)->somItemBanqueOneTypeCarac($ville, TypeCaracteristique::EAU) + $ville->getWater())
                          ->setNbAlcool($this->em->getRepository(Banque::class)->somItemBanqueOneTypeCarac($ville, TypeCaracteristique::ALCOOL))
                          ->setNbDrogues($this->em->getRepository(Banque::class)->somItemBanqueOneTypeCarac($ville, TypeCaracteristique::DRUGS))
                          ->setNbCafe($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["coffee_#00"]));
            
            // Gestion des ressources
            $resssourceDto = new RessourcesDto();
            $resssourceDto->setNbBois($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["wood2_#00", "wood_bad_#00", "wood_beam_#00"]))
                          ->setNbMetal($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["metal_bad_#00", "metal_#00", "metal_beam_#00"]))
                          ->setNbBeton($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["concrete_#00", "concrete_wall_#00"]))
                          ->setNbCompo($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["electro_#00"]))
                          ->setNbExplo($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["explo_#00"]))
                          ->setNbLentille($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["lens_#00"]))
                          ->setNbPiles($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["pile_#00"]))
                          ->setNbPve($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["meca_parts_#00"]))
                          ->setNbSac($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["ryebag_#00"]))
                          ->setNbTube($this->em->getRepository(Banque::class)->somItemBanqueItems($ville, ["tube_#00"]));
            
            // Gestion des informations de la défense
            $defense = $this->em->getRepository(Defense::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            // Gestion des estimations
            $estimation = $this->em->getRepository(Estimation::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            $resumeDto = (new ResumeDto())
                ->setVille($villeDto)
                ->setCitoyens($citoyensDto)
                ->setProvisions($provisionsDto)
                ->setRessources($resssourceDto)
                ->setDefense($defense ?? new Defense($ville->getJour()))
                ->setEstim($estimation);
            
            // Recupération de la dernière gazette
            $gazette = $this->em->getRepository(Journal::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
            
            // Gestion des citoyens
            $citoyens = $this->em->getRepository(Citoyens::class)->findBy(['ville' => $ville]);
            
            // Gestion de la banque, on récupère les items de la banque et les catégories de la banque
            $banque = (new Banques($this->translator))->setBanques($ville->getBanque())->triByCategoryNombreNom();
            
            // Gestion des chantiers
            $chantierVille = $this->em->getRepository(Chantiers::class)->findBy(['ville' => $ville]);
            
            // Gestion des plans de chantiers
            $plansChantier = $this->em->getRepository(PlansChantier::class)->findBy(['ville' => $ville]);
            
            // Gestion des évolutions
            $evoVille = $this->em->getRepository(UpChantier::class)->findBy(['ville' => $ville]);
            
            
            // Gestion de la carte
            $carteDto = new CarteDto();
            
            // Récupération des batiments de la carte
            $batiments = $this->em->getRepository(ZoneMap::class)->recupBatiment($ville);
            $ruines    = $this->em->getRepository(ZoneMap::class)->recupRuine($ville);
            
            // Récupération des zones
            $zones = $this->em->getRepository(ZoneMap::class)->findBy(['ville' => $ville]);
            
            $carteDetail = new CarteDetailDto();
            
            $carteDetail->setNbDecouvert($this->em->getRepository(ZoneMap::class)->getExploration($ville))
                        ->setNbEpuise($this->em->getRepository(ZoneMap::class)->getCaseEpuise($ville))
                        ->setNbTotal($ville->getWeight() * $ville->getHeight())
                        ->setTaille($ville->getWeight())
                        ->setNbCitoyen(count($this->em->getRepository(Citoyens::class)->findBy(['ville' => $ville, 'dehors' => true, 'mort' => false])));
            
            foreach ($zones as $zone) {
                $carteDetail->addZone($zone);
            }
            
            
            $carteDto->setDetail($carteDetail)
                     ->setBatiments(new ArrayCollection($batiments))
                     ->setRuines(new ArrayCollection($ruines));
            
            // Statistiques de la ville sur les estimations/attaques
            $stats = new Statistiques();
            $stats->setEstimation($ville->getEstimation())
                  ->setJournaux($ville->getJournal())
                  ->setEstimMinMax([])
                  ->setAttaqueMoy([]);
            
            $comparatifVillesDto->setResume($resumeDto)
                                ->setGazette($gazette ?? new Journal($ville->getJour()))
                                ->setCitoyens(new ArrayCollection($citoyens))
                                ->setBanques($banque)
                                ->setChantiers(new ArrayCollection($chantierVille))
                                ->setPlansChantiers(new ArrayCollection($plansChantier))
                                ->setUpChantiers(new ArrayCollection($evoVille))
                                ->setCarte($carteDto)
                                ->setStats($stats->recupAtkArray());
            
            
        }
        
        return $comparatifVillesDto;
    }
    
    public function getNbrVille(User $user, ?int $saison, ?string $phase): int
    {
        
        return count($this->em->getRepository(VilleHistorique::class)->neighborTown($user, $saison, $phase));
        
    }
    
    public function getNeighbor(User $user, ?int $saison, ?string $phase): array
    {
        
        return $this->em->getRepository(VilleHistorique::class)->neighborUser($user, $saison, $phase);
        
    }
    
    /**
     * @param User $user
     * @return HistoriqueVille[]
     */
    public function getOldVillesHistos(User $user): array
    {
        $oldVillesHistos = $this->em->getRepository(HistoriqueVille::class)->findBy(['user' => $user]);
        
        $listOldVilleHisto = [];
        
        foreach ($oldVillesHistos as $oldVillesHisto) {
            
            $villeOld = $oldVillesHisto->getVilleHisto();
            if ($villeOld !== null) {
//                if ($villeOld->getSaison() === 0 && $villeOld->getPhase() === 'alpha') {
//                    $villeOld->setSaison(15);
//                }
                
                $cleanType = $oldVillesHisto->getCleanTypeHisto();
                $cleanType?->setLabel($this->translator->trans($cleanType->getLabel(), [], 'game'));
                $typeMort = $oldVillesHisto->getTypeMort();
                $typeMort?->setLabel($this->translator->trans($typeMort->getLabel(), [], 'game'));
                
                
                $listOldVilleHisto[] = $oldVillesHisto;
            }
            
        }
        
        usort($listOldVilleHisto, [$this, 'sortHistoriqueVille']);
        
        return $listOldVilleHisto;
    }
    
    public function getVilleCommune(User $myUser, User $user): array
    {
        
        return $this->em->getRepository(VilleHistorique::class)->commonTown($myUser, $user);
        
    }
    
    public function getVilleString(User $user): array
    {
        $villeString = ['nom' => null, 'mapId' => null];
        
        if ($user->getMapId() !== null) {
            $ville = $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $user->getMapId()]);
            if ($ville === null) {
                $villeString['nom']   = 'Inconnu';
                $villeString['mapId'] = $user->getMapId();
            } else {
                $villeString['nom']   = $ville->getNom();
                $villeString['mapId'] = $ville->getMapId();
            }
        }
        
        return $villeString;
    }
    
    private function sortHistoriqueVille(HistoriqueVille $a, HistoriqueVille $b): int
    {
        $phase_order = ["native", "import", "beta", "alpha"];
        
        $villeA = $a->getVilleHisto();
        $villeB = $b->getVilleHisto();
        
        
        // Tri par saison du plus grand au plus petit
        if ($villeA->getSaison() < $villeB->getSaison()) {
            return 1;
        } elseif ($villeA->getSaison() > $villeB->getSaison()) {
            return -1;
        }
        
        // Tri par phase de native à alpha
        $a_phase_index = array_search($villeA->getPhase(), $phase_order);
        $b_phase_index = array_search($villeB->getPhase(), $phase_order);
        if ($a_phase_index < $b_phase_index) {
            return -1;
        } elseif ($a_phase_index > $b_phase_index) {
            return 1;
        }
        
        // Tri par mapId de plus grand au plus petit
        if ($villeA->getMapid() < $villeB->getMapid()) {
            return 1;
        } elseif ($villeA->getMapid() > $villeB->getMapid()) {
            return -1;
        }
        
        // Les deux éléments sont égaux
        return 0;
        
    }
}