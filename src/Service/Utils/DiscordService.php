<?php

namespace App\Service\Utils;

use App\Structures\Dto\GH\Generality\ImageContactDto;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class DiscordService
{
    private string $discordToken;
    private Client $client;
    
    public function __construct()
    {
        $this->discordToken = $_ENV['TOKEN_BOT'];
        $this->client       = new Client();
    }
    
    /**
     * @throws GuzzleException
     */
    public function deleteMessageDiscord(int $channelId, int $messageId): void
    {
        $this->client->delete("https://discord.com/api/v10/channels/$channelId/messages/$messageId", [
            'headers' => [
                'Authorization' => "Bot $this->discordToken",
                'Content-Type'  => 'application/json',
            ],
        ]);
    }
    
    public function generateMessageAnnonceDiscord(string $message): void
    {
        // Avant, on vérifie si le message de maintenance est présent, si oui, on le supprime
        $this->verifyAndDeleteMessageAnnonceDiscord();
        
        // On envoie le message de maintenance, mais on va le split en plusieurs messages si besoin
        $messagesSplit = $this->splitMessage($message);
        
        foreach ($messagesSplit as $messageOne) {
            
            $this->client->post("https://discord.com/api/v10/channels/{$_ENV['ID_ANNONCE']}/messages", [
                'headers' => [
                    'Authorization' => "Bot $this->discordToken",
                    'Content-Type'  => 'application/json',
                ],
                'json'    => ['content' => $messageOne],
            ]);
        };
    }
    
    /**
     * @throws GuzzleException
     */
    public function generateMessageLogDiscord(string $message): void
    {
        $this->client->post("https://discord.com/api/v10/channels/{$_ENV['ID_LOG']}/messages", [
            'headers' => [
                'Authorization' => "Bot {$this->discordToken}",
                'Content-Type'  => 'application/json',
            ],
            'json'    => ['content' => substr($message, 0, 2000)],
        ]);
    }
    
    /**
     * @throws GuzzleException
     */
    public function generateMessageMessageDiscord(string $message, array $file): void
    {
        $timestamp = date("c", strtotime("now"));
        $arrayFile = array_map(fn($value, $key) => [
            "title"     => "Image - $key",
            "type"      => "rich",
            "timestamp" => $timestamp,
            "color"     => hexdec("3366ff"),
            "image"     => ["url" => $value],
        ], $file, array_keys($file));
        
        $data = [
            'content' => substr($message, 0, 2000),
            'embeds'  => $arrayFile,
        ];
        
        $this->client->post("https://discord.com/api/v10/channels/{$_ENV['ID_LOG']}/messages", [
            'headers' => [
                'Authorization' => "Bot {$this->discordToken}",
                'Content-Type'  => 'application/json',
            ],
            'json'    => $data,
        ]);
    }
    
    /**
     * @param string $titre
     * @param string $message
     * @param array $tag
     * @param ImageContactDto[] $file
     * @param bool $confidential
     * @param string $contenuConfidentiel
     * @return string
     * @throws GuzzleException
     */
    public function generateMessageThreadDiscord(string $titre, string $message, array $tag = [],
                                                 array  $file = [], bool $confidential = false,
                                                 string $contenuConfidentiel = ""): string
    {
        // Schéma (HTTP ou HTTPS)
        $schema = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https://' : 'http://';
        
        // Nom du serveur
        $serverName = $_SERVER['SERVER_NAME'];
        
        // URL de base du serveur
        $baseUrl = $schema . $serverName . '/uploads/discord/';
        
        // Id du forum selon le caractère confidential
        $idForum = $confidential ? $_ENV['ID_FORUM_CONF'] : $_ENV['ID_FORUM'];
        
        // message à envoyer
        $numeroTicket = uniqid();
        $message      = "**Numéro de ticket** : {$numeroTicket}\n" . $message;
        
        // Générer un tableau de message pour garder un message maximum de 2000 caractères
        // et un tableau de fichier pour garder un maximum de 10 fichiers
        $messageArray = str_split($message, 2000);
        
        
        $apiUrl    = "https://discord.com/api/v10/channels/{$idForum}/threads";
        $timestamp = date("c", strtotime("now"));
        $arrayFile = [];
        foreach ($file as $key => $value) {
            $arrayFile[] = [
                // Title
                "title"     => $value->getLegend(),
                // Embed Type, do not change.
                "type"      => "rich",
                // Timestamp, only ISO8601
                "timestamp" => $timestamp,
                // Left border color, in HEX
                "color"     => hexdec("3366ff"),
                // Embed image
                "image"     => [
                    "url" => $baseUrl . $value->getSrc(),
                ],
            ];
        }
        $arrayFileMessage = [];
        // si arrayfile contient plus de 10 éléments, on prépare un message supplémentaire pour chaque 10 éléments
        if (count($arrayFile) > 0) {
            $nbMessage     = count($arrayFile) / 10;
            $nbMessage     = ceil($nbMessage);
            $arrayFileTemp = $arrayFile;
            for ($i = 0; $i < $nbMessage; $i++) {
                $arrayFileMessage[$i] = array_slice($arrayFileTemp, $i * 10, 10);
            }
        }
        
        $arrayTag = [];
        foreach ($tag as $key => $value) {
            $arrayTag[] = match ($value) {
                'Bug'        => $_ENV['TAG_BUG'],
                'Suggestion' => $_ENV['TAG_SUGG'],
            };
        }
        
        // Préparez les données pour la requête POST avec des fichiers joints
        $data = [
            'name'         => substr($titre, 0, 100),
            'applied_tags' => $arrayTag,
            'message'      => [
                'content' => $messageArray[0],
                'embeds'  => $arrayFileMessage[0] ?? [],
            ],
        ];
        
        
        // Utilisez Guzzle pour envoyer le message à Discord via le webhook
        
        $retour = $this->client->post($apiUrl, [
            'headers' => [
                'Authorization' => "Bot {$this->discordToken}",
                'Content-Type'  => 'application/json',
            ],
            'json'    => $data,
        ]);
        
        // on récupére l'id du thread crée pour pouvoir poster les messages suivants ou les fichiers suivants
        $idThread = json_decode($retour->getBody()->getContents(), true)['id'];
        
        // Si le tableau de message contient plus de 1 élément ou si le tableau de fichier contient d'autres éléments, on envoie les messages suivants
        if (count($messageArray) > 1 || count($arrayFileMessage) > 1) {
            // on envoie les messages suivants
            for ($i = 1; $i < count($messageArray); $i++) {
                $apiUrl = "https://discord.com/api/v10/channels/{$idThread}/messages";
                $data   = [
                    'content' => $messageArray[$i],
                ];
                $this->client->post($apiUrl, [
                    'headers' => [
                        'Authorization' => "Bot {$this->discordToken}",
                        'Content-Type'  => 'application/json',
                    ],
                    'json'    => $data,
                ]);
            }
            // on envoie les fichiers suivants
            for ($i = 1; $i < count($arrayFileMessage); $i++) {
                $apiUrl = "https://discord.com/api/v10/channels/{$idThread}/messages";
                $data   = [
                    'embeds' => $arrayFileMessage[$i],
                ];
                $this->client->post($apiUrl, [
                    'headers' => [
                        'Authorization' => "Bot {$this->discordToken}",
                        'Content-Type'  => 'application/json',
                    ],
                    'json'    => $data,
                ]);
            }
        }
        // Génération du message contenant les informations sur le ticket si $contenuConfidentiel n'est pas vide
        if ($contenuConfidentiel !== "") {
            $dataMessageTicket = [
                'name'    => "Numéro de ticket : {$numeroTicket}",
                'message' => [
                    'content' => $contenuConfidentiel,
                ],
            ];
            
            $this->client->post("https://discord.com/api/v10/channels/{$_ENV['ID_FORUM_CONF']}/threads", [
                'headers' => [
                    'Authorization' => "Bot {$this->discordToken}",
                    'Content-Type'  => 'application/json',
                ],
                'json'    => $dataMessageTicket,
            ]);
        }
        
        return $idThread;
    }
    
    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getLastMessageDiscord(int $channelId, ?int $limit = 1): array
    {
        $response = $this->client->get("https://discord.com/api/v10/channels/$channelId/messages", [
            'headers' => [
                'Authorization' => "Bot $this->discordToken",
                'Content-Type'  => 'application/json',
            ],
            'query'   => [
                'limit' => $limit, // Récupère le dernier message (ici $limit message - par défaut le dernier)
            ],
        ]);
        
        return json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
    }
    
    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function verifyAndDeleteMessageAnnonceDiscord(): void
    {
        $lastMessage = $this->getLastMessageDiscord($_ENV['ID_ANNONCE'], 1);
        
        if (!empty($lastMessage) && $lastMessage[0]['content'] === ":flag_fr: :warning: ***<@&{$_ENV['ID_MAINTENANCE']}> en cours, installation de la prochaine mise à jour*** :warning: :flag_fr:\n:flag_gb: :warning: ***<@&{$_ENV['ID_MAINTENANCE']}> in progress, installing the next update*** :warning: :flag_gb:") {
            $this->client->delete("https://discord.com/api/v10/channels/{$_ENV['ID_ANNONCE']}/messages/{$lastMessage[0]['id']}", [
                'headers' => [
                    'Authorization' => "Bot $this->discordToken",
                    'Content-Type'  => 'application/json',
                ],
            ]);
        }
    }
    
    private function splitMessage(string $message): array
    {
        $maxCharLimit   = 2000;
        $messages       = [];
        $currentMessage = "";
        
        foreach (explode("\n", $message) as $paragraph) {
            if (strlen($currentMessage) + strlen($paragraph) + 1 <= $maxCharLimit) {
                $currentMessage .= $paragraph . "\n";
            } else {
                $messages[]     = $currentMessage;
                $currentMessage = $paragraph . "\n";
            }
        }
        
        if (!empty($currentMessage)) {
            $messages[] = $currentMessage;
        }
        
        return $messages;
    }
    
}