<?php

namespace App\Service\Utils;

class HtmlCleanerService
{
    public function cleanHtmlParag(?string $html): ?string
    {
        if (empty($html)) {
            return null;
        }
        
        // Nettoie les balises <p> en préservant les retours à la ligne entre paragraphes
        $cleaned = preg_replace('/^<p>|<\/p>$|<\/p>\s*<p>/', "\n", $html);
        
        // Enlève les retours à la ligne multiples
        $cleaned = preg_replace('/\n+/', "\n", $cleaned);
        
        // Enlève les espaces superflus
        return trim($cleaned);
    }
}