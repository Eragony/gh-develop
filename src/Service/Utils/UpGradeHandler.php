<?php


namespace App\Service\Utils;


use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\UpChantier;
use App\Entity\UpChantierPrototype;
use App\Entity\Ville;
use App\Service\GestHordesHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpGradeHandler
{
    public function __construct(protected EntityManagerInterface $em, protected GestHordesHandler $gh,
                                protected TranslatorInterface    $translator)
    {
    }
    
    public function recupAllLevelChantierArray(Ville $ville): array
    {
        $evoChantier = $this->em->getRepository(UpChantier::class)->findBy(['ville' => $ville]);
        if ($evoChantier === null) {
            return [];
        } else {
            $bonus = [];
            foreach ($evoChantier as $evo) {
                $chantier = $evo->getChantier();
                
                $array = [
                    'id'      => $chantier->getId(),
                    'nom'     => $chantier->getNom(),
                    'lvl_max' => $chantier->getLevelMax(),
                    'icon'    => $chantier->getIcon(),
                ];
                
                $nbConstruction    = count($evo->getDays());
                $evolutionDetruite = $evo->isDestroy() || $nbConstruction > 1;
                
                foreach ($evo->getDays() as $key => $dayArray) {
                    foreach ($dayArray as $day => $dayLvl) {
                        $array['lvl']       = $day + 1;
                        $array['destroy']   = $evolutionDetruite && $key < ($nbConstruction - 1) || $evo->isDestroy();
                        $bonus[$dayLvl + 1] = $array;
                    }
                }
                
            }
            
            return $bonus;
        }
        
    }
    
    public function recupBonusAtelierPv(Ville $ville): int
    {
        
        $chantier_atelier = $this->em->getRepository(ChantierPrototype::class)
                                     ->findOneBy(['id' => ChantierPrototype::ID_CHANTIER_ATELIER]);
        
        $upAtelier =
            $this->em->getRepository(UpChantier::class)->findOneBy(['ville'    => $ville,
                                                                    'chantier' => $chantier_atelier]);
        
        if ($upAtelier === null) {
            return 2;
        }
        
        return $chantier_atelier->getLevelUp($upAtelier->getLvlActuel())
                                ->getBonusUp(UpChantierPrototype::TYPE_MOD_COUT_PA)->getValeurUp() ?? 2;
        
    }
    
    public function recupBonusAtelierReductionPA(Ville $ville): int
    {
        
        $chantier_atelier = $this->em->getRepository(ChantierPrototype::class)
                                     ->findOneBy(['id' => ChantierPrototype::ID_CHANTIER_ATELIER]);
        
        $upAtelier = $this->em->getRepository(UpChantier::class)->findOneBy(['ville'    => $ville,
                                                                             'chantier' => $chantier_atelier]);
        
        if ($upAtelier === null) {
            return 0;
        }
        
        return $chantier_atelier->getLevelUp($upAtelier->getLvlActuel())->getBonusUp(UpChantierPrototype::TYPE_RED_PA)
                                ->getValeurUp() ?? 0;
        
    }
    
    public function recupBonusSD(Ville $ville): int
    {
        
        $chantier_SD = $this->em->getRepository(ChantierPrototype::class)
                                ->findOneBy(['id' => ChantierPrototype::ID_CHANTIER_SD]);
        
        $upSD = $this->em->getRepository(UpChantier::class)->findOneBy(['ville' => $ville, 'chantier' => $chantier_SD]);
        
        // verif si le chantier est au moins construit
        $chantier_SDConstruit =
            $this->em->getRepository(Chantiers::class)->findOneBy(['ville' => $ville, 'chantier' => $chantier_SD]);
        
        if ($chantier_SDConstruit === null) {
            $bonusSD = 0;
        } else {
            $bonusSD = 10;
        }
        
        if ($upSD === null) {
            return $bonusSD;
        }
        
        return $chantier_SD->getLevelUp($upSD->getLvlActuel())->getBonusUp(UpChantierPrototype::TYPE_ADD_DEF_PCT)
                           ->getValeurUp() ?? $bonusSD;
        
        
    }
    
    public function recupLevelByDayChantier(Ville $ville, int $idChantier): array
    {
        
        $chantier = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, $idChantier);
        
        if ($chantier === null) {
            return [];
        }
        
        $evoChantier = $this->em->getRepository(UpChantier::class)->findOneBy(['ville' => $ville, 'chantier' => $chantier->getChantier()]);
        if ($evoChantier === null) {
            return [];
        } else {
            return $evoChantier->getDays();
        }
        
    }
    
    public function recupLevelChantier(Ville $ville, int $idChantier): ?int
    {
        
        $chantier = $this->em->getRepository(Chantiers::class)->chantierConstruit($ville, $idChantier);
        
        if ($chantier === null) {
            return null;
        }
        
        $evoChantier = $this->em->getRepository(UpChantier::class)->findOneBy(['ville'    => $ville,
                                                                               'chantier' => $chantier->getChantier()]);
        if ($evoChantier === null) {
            return 0;
        } else {
            return $evoChantier->getLvlActuel();
        }
        
    }
    
    public function recupLevelChantierArray(int $idChantier, int $type_bonus): array
    {
        
        $chantier = $this->em->getRepository(ChantierPrototype::class)->findOneBy(['id' => $idChantier]);
        
        if ($chantier === null || $chantier->getLevelMax() < 1) {
            return [];
        }
        
        $bonus = [];
        foreach ($chantier->getLevelUps() as $levelUp) {
            $bonus_val = $levelUp->getBonusUp($type_bonus);
            if ($bonus_val !== null) {
                $bonus[$levelUp->getLevel()] = $bonus_val->getValeurUp();
            }
        }
        
        return $bonus;
        
    }
    
    
}