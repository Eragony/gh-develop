<?php

namespace App\Service\Utils;

use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\Citoyens;
use App\Entity\CleanUpCadaver;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\PictoPrototype;
use App\Entity\Ruines;
use App\Entity\TypeDeath;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use Doctrine\ORM\EntityManagerInterface;

readonly class DataService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }
    
    public function getDataVille(string $mapId): ?Ville
    {
        return $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
    }
    
    public function getDerniereVille(User $user): ?Citoyens
    {
        return $this->entityManager->getRepository(Citoyens::class)->derniereVille($user);
    }
    
    public function getFindOneUser(array $creteria): ?User
    {
        return $this->entityManager->getRepository(User::class)
                                   ->findOneBy($creteria);
    }
    
    /**
     * @return HomePrototype[]
     */
    public function getHomePrototypeAll(): array
    {
        return $this->entityManager->getRepository(HomePrototype::class)->findAll();
    }
    
    /**
     * @return HomePrototype[]
     */
    public function getHomePrototypeByDef(): array
    {
        $listHoeProto = $this->getHomePrototypeAll();
        return array_combine(array_map(fn(HomePrototype $homeProto) => $homeProto->getDef(), $listHoeProto), $listHoeProto);
    }
    
    /**
     * @return BatPrototype[]
     */
    public function getListBatIndexed(): array
    {
        return $this->entityManager->getRepository(BatPrototype::class)
                                   ->createQueryBuilder('b', 'b.idMh')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return ChantierPrototype[]
     */
    public function getListChantierIndexed(): array
    {
        return $this->entityManager->getRepository(ChantierPrototype::class)
                                   ->createQueryBuilder('c', 'c.idMh')
                                   ->getQuery()
                                   ->getResult();
    }
    
    public function getListCitoyensByIds(array $citoyensIds): array
    {
        return $this->entityManager->getRepository(User::class)
                                   ->createQueryBuilder('u', 'u.idMyHordes')
                                   ->where('u.idMyHordes in (:idMyHordes)')
                                   ->setParameter('idMyHordes', $citoyensIds)
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return CleanUpCadaver[]
     */
    public function getListCleanUpTypeIndexed(): array
    {
        return $this->entityManager->getRepository(CleanUpCadaver::class)
                                   ->createQueryBuilder('cuc', 'cuc.ref')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return JobPrototype[]
     */
    public function getListJobIndexed(): array
    {
        return $this->entityManager->getRepository(JobPrototype::class)
                                   ->createQueryBuilder('j', 'j.ref')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return TypeDeath[]
     */
    public function getListMortIndexed(): array
    {
        return $this->entityManager->getRepository(TypeDeath::class)
                                   ->createQueryBuilder('td', 'td.idMort')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return ItemPrototype[]
     */
    public function getListObjetIndexed(): array
    {
        return $this->entityManager->getRepository(ItemPrototype::class)
                                   ->createQueryBuilder('i', 'i.idMh')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return PictoPrototype[]
     */
    public function getListPictoIndexed(): array
    {
        return $this->entityManager->getRepository(PictoPrototype::class)
                                   ->createQueryBuilder('pp', 'pp.idMh')
                                   ->getQuery()
                                   ->getResult();
    }
    
    /**
     * @return Ruines[]
     */
    public function getRuineVille(Ville $ville): array
    {
        return $this->entityManager->getRepository(Ruines::class)->findBy(['ville' => $ville]) ?? [];
    }
    
    public function updateZoneVue(Ville $ville): void
    {
        $this->entityManager->getRepository(ZoneMap::class)->majVue($ville);
    }
}