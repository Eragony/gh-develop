<?php

namespace App\Service\Utils;

class RandomHandler
{
    /**
     * Randomly selects N elements from an array
     * @param array $a
     * @param int $num
     * @param bool $force_array
     * @return mixed|array|null
     */
    public function pick(array $a, int $num = 1, bool $force_array = false): mixed
    {
        if ($num <= 0 || empty($a)) {
            if ($force_array) {
                return [];
            }
            return null;
        } elseif ($num === 1) {
            return $force_array ? [$a[array_rand($a, 1)]] : $a[array_rand($a, 1)];
        } elseif (count($a) === 1) {
            return array_values($a);
        } else {
            return array_map(function ($k) use (&$a) {
                return $a[$k];
            }, array_rand($a, min($num, count($a))));
        }
    }
    
}