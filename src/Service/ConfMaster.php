<?php

namespace App\Service;

use App\Structures\Conf\GestHordesConf;

class ConfMaster
{
    
    private readonly array $global;
    
    public function __construct(array $global, array $local)
    {
        $this->global = array_merge($global, $local);
    }
    
    public function getGlobalConf(): GestHordesConf
    {
        return $this->global_conf ?? ($this->global_conf = (new GestHordesConf($this->global))->complete());
    }
    
}