<?php


namespace App\Service\Jump;


use App\Entity\Coalition;
use App\Entity\CreneauHorraire;
use App\Entity\CreneauJump;
use App\Entity\HerosPrototype;
use App\Entity\InscriptionJump;
use App\Entity\JobPrototype;
use App\Entity\Jump;
use App\Entity\LogEventInscription;
use App\Entity\StatutInscription;
use App\Entity\StatutUser;
use App\Entity\TypeDispoJump;
use App\Entity\User;
use App\Service\AbstractGHHandler;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;

class JumpHandler extends AbstractGHHandler
{
    
    
    /**
     * @throws Exception
     */
    public function calculCrenauJump(Jump &$jump): void
    {
        
        // Utilisation des creneaux pat et 7h-9h.
        $creneauPAT = $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => 0]);
        //$creneauMatin = $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => 2]);
        
        //calcul des 3 jours de jump
        $dateJump1 = new DateTime($jump->getDateApproxJump()->format('Y-m-d'));
        
        $dateJump2 = new DateTime($jump->getDateApproxJump()->format('Y-m-d'));
        
        // On calcul plus le 3éme jour -> possibilité de faire une demande de création, aucune utilité de faire plus
        //$dateJump3 = new DateTime($jump->getDateApproxJump()->format('Y-m-d'));
        
        
        $dateJump2->add(new DateInterval('P1D'));
        //$dateJump3->add(new DateInterval('P2D'));
        
        
        //Si des créneaux n'existent pas déjà, on crée, sinon on met juste à jour la date
        if (count($jump->getCreneau()) == 0) {
            // creneau 1 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump1)
                        ->setCreneauHorraire($creneauPAT);
            
            $jump->addCreneau($creneauJump);
            // creneau 2 - les creneaux du matin ne sont plus proposé:
            //$creneauJump = new CreneauJump();
            //$creneauJump->setDateCreneau($dateJump1)
            //           ->setCreneauHorraire($creneauMatin);
            
            //$jump->addCreneau($creneauJump);
            
            // creneau 3 :
            $creneauJump = new CreneauJump();
            $creneauJump->setDateCreneau($dateJump2)
                        ->setCreneauHorraire($creneauPAT);
            
            $jump->addCreneau($creneauJump);
            
            
            // creneau 4  - les creneaux du matin ne sont plus proposé:
            //$creneauJump = new CreneauJump();
            //$creneauJump->setDateCreneau($dateJump2)
            //           ->setCreneauHorraire($creneauMatin);
            
            //$jump->addCreneau($creneauJump);
            
            
            // creneau 5 - le 3éme creneau n'est plus proposé:
            //$creneauJump = new CreneauJump();
            //$creneauJump->setDateCreneau($dateJump3)
            //->setCreneauHorraire($creneauPAT);
            
            //$jump->addCreneau($creneauJump);
            
            // creneau 6 - le 3éme creneau n'est plus proposé:
            //$creneauJump = new CreneauJump();
            //$creneauJump->setDateCreneau($dateJump3)
            //          ->setCreneauHorraire($creneauMatin);
            
            //$jump->addCreneau($creneauJump);
        } else {
            
            foreach ($jump->getCreneau() as $key => $creneau) {
                
                $id = ($key + 1);
                
                $variable = 'dateJump' . $id;
                
                if (isset(${$variable})) {
                    $creneau->setDateCreneau(${$variable});
                }
                
                
            }
            
        }
        
        
    }
    
    /**
     * @return CreneauHorraire[]
     */
    public function getListCreneau(): array
    {
        $listCreneau = $this->em->getRepository(CreneauHorraire::class)->findAll();
        foreach ($listCreneau as $creneau) {
            $creneau->setLibelle($this->translator->trans($creneau->getLibelle(), [], 'jump'));
        }
        
        return $listCreneau;
    }
    
    /**
     * @return HerosPrototype[]
     */
    public function getListPouvoir(): array
    {
        $listPouvoir = $this->em->getRepository(HerosPrototype::class)->findAll();
        foreach ($listPouvoir as $pouvoir) {
            $pouvoir->setNom($this->translator->trans($pouvoir->getNom(), [], 'game'));
        }
        return $listPouvoir;
    }
    
    /**
     * @return TypeDispoJump[]
     */
    public function getListTypeDispo(): array
    {
        $listTypeDispo = $this->em->getRepository(TypeDispoJump::class)->findAll();
        foreach ($listTypeDispo as $dispo) {
            $dispo->setNom($this->translator->trans($dispo->getNom(), [], 'jump'));
        }
        return $listTypeDispo;
    }
    
    public function majCompFige(Jump $jump, bool $fige): bool
    {
        $jump->setFigeCompetence($fige);
        
        
        try {
            $this->em->persist($jump);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $this->logger->error(
                "User : {$jump->getId()} - Exception maj figeage des compétences pour le Jump : {$jump->getId()}  - Exception : {$exception->getMessage()}",
            );
            
            return false;
        }
    }
    
    public function majMetierFige(Jump $jump, bool $fige): bool
    {
        $jump->setJobFige($fige);
        
        
        try {
            $this->em->persist($jump);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $this->logger->error(
                "User : {$jump->getId()} - Exception maj figeage de métier le Jump : {$jump->getId()}  - Exception : {$exception->getMessage()}",
            );
            
            return false;
        }
    }
    
    public function majStatut(InscriptionJump $candidature, StatutInscription $newStatut, User $user): bool
    {
        
        $libTranslate    = $this->translator->trans("Modification du statut d'inscription du jump", [], 'jumpEvent');
        $logModification = new LogEventInscription();
        $logModification->setLibelle("Modification du statut d'inscription du jump")
                        ->setValeurAvant($candidature->getStatut()->getNom())
                        ->setValeurApres($newStatut->getNom())
                        ->setDeclencheur($user)
                        ->setVisible(true)
                        ->setTypologie(4)
                        ->setEventAt(new DateTimeImmutable('now'));
        
        $candidature->setStatut($newStatut)
                    ->addLogEventInscription($logModification);
        
        if ($newStatut->getId() !== StatutInscription::ST_ACC) {
            // Recherche si la candidature était déjà en coa
            $coalition = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $candidature->getJump(),
                                                                                'user' => $candidature->getUser()]);
            
            if ($coalition !== null) {
                $this->em->remove($coalition);
            }
        }
        
        try {
            $this->em->persist($candidature);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $this->logger->error(
                "Jump : {$candidature->getJump()->getId()} - Exception maj statut sur la candidature : {$candidature->getId()}  - Exception : {$exception->getMessage()}",
            );
            
            return false;
        }
    }
    
    public function recuperationJobVille(bool $villePrivee): array
    {
        if ($villePrivee) {
            $listJob = $this->em->getRepository(JobPrototype::class)->findAllOrdo();
        } else {
            $listJob = $this->em->getRepository(JobPrototype::class)->findAllExceptChaman();
        }
        
        foreach ($listJob as $job) {
            $job->setNom($this->translator->trans($job->getNom(), [], 'game'))
                ->setAlternatif($this->translator->trans($job->getAlternatif(), [], 'game'));
        }
        
        return $listJob;
    }
    
    public function recuperationJump(string $idJump): ?Jump
    {
        return $this->em->getRepository(Jump::class)->findOneBy(['id' => $idJump]);
    }
    
    /**
     * @return StatutInscription[]
     */
    public function recuperationListStatut(): array
    {
        $listStatut = $this->em->getRepository(StatutUser::class)->findAll();
        foreach ($listStatut as $statut) {
            $statut->setNom($this->translator->trans($statut->getNom(), [], 'jump'));
        }
        
        return $listStatut;
    }
    
    /**
     * @param Jump $jump
     * @return array
     */
    public function recuperationMoyenContact(Jump $jump): array
    {
        $listMoyenContact = [];
        
        foreach ($jump->getInscriptionJumps() as $inscriptionJump) {
            if ($inscriptionJump->getStatut()->getId() === StatutInscription::ST_ACC) {
                if ($inscriptionJump->getTypeDiffusion() === 0 ||
                    ($inscriptionJump->getTypeDiffusion() === 1 && $this->userHandler->isLeadOrOrga($jump))) {
                    $listMoyenContact[$inscriptionJump->getUser()->getId()] =
                        $inscriptionJump->dechiffreMoyenContact($this->gh);
                }
            }
        }
        return $listMoyenContact;
    }
    
    public function recuperationStatut(int $statutId): ?StatutInscription
    {
        return $this->em->getRepository(StatutInscription::class)->findOneBy(['id' => $statutId]);
    }
    
}