<?php


namespace App\Service\Jump;


use App\Entity\Coalition;
use App\Entity\DispoJump;
use App\Entity\Jump;
use App\Entity\StatutUser;
use App\Entity\TypeDispoJump;
use App\Entity\User;
use App\Service\GestHordesHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\Pure;
use Psr\Log\LoggerInterface;

class CoalitionHandler
{
    
    public function __construct(protected EntityManagerInterface $em, protected LoggerInterface $logger,
                                protected GestHordesHandler      $gh)
    {
    }
    
    /**
     * @return bool
     */
    public function controleExistanceCoalition(Jump $jump, User $user): bool
    {
        return $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $user]) !== null;
    }
    
    /**
     * @return bool
     */
    public function controleExistanceCreneauJump(Jump $jump, int $numCreneau): bool
    {
        $jump = $this->em->getRepository(Jump::class)->findOneBy(['id' => $jump->getId()]);
        
        return $numCreneau >= 0 && $numCreneau < $jump->getCreneau()->count();
    }
    
    /**
     * @return bool
     */
    public function controleExistanceDispoJump(int $dispo): bool
    {
        if ($dispo === 0) {
            return true;
        }
        return $this->em->getRepository(TypeDispoJump::class)->findOneBy(['id' => $dispo]) !== null;
    }
    
    /**
     * @return bool
     */
    public function controleExistanceStatut(int $statut): bool
    {
        if ($statut === 0) {
            return true;
        }
        return $this->em->getRepository(StatutUser::class)->findOneBy(['id' => $statut]) !== null;
    }
    
    /**
     * @param Coalition|null $coalition
     * @return void
     */
    public function creationModificationCoalition(?Coalition &$coalition, User $user, Jump $jump, int $numCoa,
                                                  int        $posCoa): void
    {
        if ($coalition === null) {
            $coalition = new Coalition();
            
            $coalition->setUser($user)
                      ->setJump($jump);
            
            foreach ($jump->getCreneau() as $creneau) {
                $dispo = new DispoJump();
                $dispo->setCreneauJump($creneau);
                $coalition->addDispo($dispo);
            }
        }
        
        $coalition->setNumCoa($numCoa)
                  ->setPositionCoa($posCoa);
    }
    
    /**
     * @return void
     */
    public function echangeCoalitionDouble(Coalition &$coalitionIni, Coalition &$coalitionCible): void
    {
        // On modifie juste les positions initiales et cibles
        $numCoaIni = $coalitionIni->getNumCoa();
        $posCoaIni = $coalitionIni->getPositionCoa();
        
        $coalitionIni->setNumCoa($coalitionCible->getNumCoa())
                     ->setPositionCoa($coalitionCible->getPositionCoa());
        
        $coalitionCible->setNumCoa($numCoaIni)
                       ->setPositionCoa($posCoaIni);
    }
    
    /**
     * @return void
     */
    public function echangeCoalitionSimple(Coalition &$coalitionCible, User $user): void
    {
        $coalitionCible->setUser($user)
                       ->setStatut(null);
        
        // Remise à 0 des dispos
        foreach ($coalitionCible->getDispos() as $dispo) {
            $dispo->setChoixDispo(null);
        }
    }
    
    /**
     * @param User|null $userMaj
     * @return bool
     */
    public function echangeDeCoalition(Jump $jump, User $userEchange1, User $userEchange2, ?User $userMaj = null): bool
    {
        
        // Récup Coalition du joueur initiale pour récupérer les positions initiales
        $coaIni = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $userEchange1]);
        
        // Récup Coalition du joueur ciblé pour échange
        $coaCible = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $userEchange2]);
        
        if ($coaIni === null) {
            
            $this->echangeCoalitionSimple($coaCible, $userEchange1);
            
        } else {
            
            $this->echangeCoalitionDouble($coaIni, $coaCible);
            
            $this->em->persist($coaIni);
            
        }
        
        $this->em->persist($coaCible);
        
        try {
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $idUser = $userMaj?->getId() ?? "null";
            
            $messageLog =
                "User : {$idUser} - Exception échange Coalition Jump : {$jump->getId()} - Joueur Ini : {$userEchange1->getId()} - Joueur Cible : {$userEchange1->getId()} - Exception : {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return false;
        }
        
    }
    
    /**
     * @param User|null $userMaj
     * @return bool
     * @throws GuzzleException
     */
    public function enregistrementCreateurCoalition(Jump  $jump, User $user, int $numCoa, int $posCoa, bool $createur,
                                                    ?User $userMaj = null): bool
    {
        // Récupération de la coalition de l'utilisateur sur le jump
        $coalition = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $user]);
        
        if ($coalition === null) {
            return false;
        }
        
        $coalition->setCreateur($createur);
        
        try {
            $this->em->persist($coalition);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $idUser = $userMaj?->getId() ?? "null";
            
            $messageLog =
                "User : {$idUser} - Exception ajout createur Coalition Jump : {$jump->getId()} - NumCoa : {$numCoa} - PosCoa : {$posCoa} - Exception : {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return false;
        }
        
    }
    
    /**
     * @param User|null $userMaj
     * @return bool
     * @throws GuzzleException
     */
    public function enregistrementNouvelleCoalition(Jump  $jump, User $user, int $numCoa, int $posCoa,
                                                    ?User $userMaj = null): bool
    {
        
        // Récupération de la coalition potentielle de l'utilisateur sur le jump si existante
        $coalition = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $user]);
        
        $this->creationModificationCoalition($coalition, $user, $jump, $numCoa, $posCoa);
        
        try {
            $this->em->persist($coalition);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $idUser = $userMaj?->getId() ?? "null";
            
            $messageLog =
                "User : {$idUser} - Exception enregistrement Coalition Jump : {$jump->getId()} - NumCoa : {$numCoa} - PosCoa : {$posCoa} - Exception : {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return false;
        }
        
    }
    
    /**
     * @return bool
     */
    public function miseAJourDispoCoa(Jump $jump, User $user, int $numDispo, int $typeDispoJump): bool
    {
        // récupération de la ligne Coa du joueur
        $coalition = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $user]);
        
        if ($coalition === null) {
            $messageLog =
                "Impossible de mettre à jour la dispo du joueur dans la coalition - Jump : {$jump->getId()} - User : {$user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return false;
        }
        if ($typeDispoJump === 0) {
            $dispoJoueur = null;
        } else {
            // récupération du statut associé
            $dispoJoueur = $this->em->getRepository(TypeDispoJump::class)->findOneBy(['id' => $typeDispoJump]);
            if ($dispoJoueur === null) {
                $messageLog =
                    "Impossible de mettre à jour la dispo du joueur dans la coalition - Jump : {$jump->getId()} - User : {$user->getId()} - dispo introuvé : {$typeDispoJump}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                return false;
            }
        }
        
        
        $coalition->getDispo($numDispo)->setChoixDispo($dispoJoueur);
        
        
        try {
            $this->em->persist($coalition);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $messageLog =
                "User : {$user->getId()} - Exception maj dispo coa Jump : {$jump->getId()} - NumCoa : {$coalition->getNumCoa()} - PosCoa : {$coalition->getPositionCoa()} - Exception : {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return false;
        }
    }
    
    /**
     * @return bool
     */
    public function miseAJourStatutCoa(Jump $jump, User $user, int $idStatut): bool
    {
        // récupération de la ligne Coa du joueur
        $coalition = $this->em->getRepository(Coalition::class)->findOneBy(['jump' => $jump, 'user' => $user]);
        
        if ($coalition === null) {
            $messageLog =
                "Impossible de mettre à jour le statut du joueur dans la coalition - Jump : {$jump->getId()} - User : {$user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            
            return false;
        }
        
        if ($idStatut === 0) {
            $statut = null;
        } else {
            // récupération du statut associé
            $statut = $this->em->getRepository(StatutUser::class)->findOneBy(['id' => $idStatut]);
            if ($statut === null) {
                $messageLog =
                    "Impossible de mettre à jour le statut du joueur dans la coalition - Jump : {$jump->getId()} - User : {$user->getId()} - statut introuvé : {$idStatut}";
                $this->logger->error($messageLog);
                $this->gh->generateMessageDiscord($messageLog);
                
                return false;
            }
        }
        
        
        $coalition->setStatut($statut);
        
        
        try {
            $this->em->persist($coalition);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $messageLog =
                "User : {$user->getId()} - Exception maj statut coa Jump : {$jump->getId()} - NumCoa : {$coalition->getNumCoa()} - PosCoa : {$coalition->getPositionCoa()} - Exception : {$exception->getMessage()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return false;
        }
        
    }
    
    /**
     * @return Coalition[]
     */
    #[Pure] public function recuperationListingCoa(Jump $jump): array
    {
        $arrCoa = [];
        
        for ($numCoa = 1; $numCoa < 9; $numCoa++) {
            for ($posCoa = 1; $posCoa < 6; $posCoa++) {
                $arrCoa[$numCoa . '-' . $posCoa] = null;
            }
        }
        
        // Récupération des coas existantes
        
        
        $listCoa = $jump->getCoalitions();
        
        foreach ($listCoa as $coa) {
            
            $arrCoa[$coa->getNumCoa() . '-' . $coa->getPositionCoa()] = $coa;
            
        }
        
        
        return $arrCoa;
        
    }
    
    /**
     * @return bool
     */
    public function verificationPlaceCoaLibre(Jump $jump, int $numCoa, int $posCoa): bool
    {
        $coa = $this->em->getRepository(Coalition::class)->findOneBy(['jump'        => $jump, 'numCoa' => $numCoa,
                                                                      'positionCoa' => $posCoa]);
        
        if ($coa === null) {
            return true;
        } else {
            return false;
        }
    }
}