<?php


namespace App\Service\Jump;


use App\Entity\InscriptionJump;
use App\Entity\JobPrototype;
use App\Entity\Jump;
use App\Entity\StatutInscription;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class InscriptionHandler
{
    
    public function __construct(protected EntityManagerInterface $em, protected LoggerInterface $logger)
    {
    }
    
    public function controleExistanceInscriptionAcc(Jump $jump, User $user): bool
    {
        return $this->em->getRepository(InscriptionJump::class)->findOneBy([
                                                                               'Jump'   => $jump,
                                                                               'user'   => $user,
                                                                               'statut' => StatutInscription::ST_ACC,
                                                                           ]) !== null;
    }
    
    public function miseAJourMetierDef(Jump $jump, User $user, int $idJob): bool
    {
        // récupération de la ligne Coa du joueur
        $inscription = $this->em->getRepository(InscriptionJump::class)->findOneBy(['Jump' => $jump, 'user' => $user]);
        
        if ($inscription === null) {
            $this->logger->error(
                "Impossible de mettre à jour l'inscription du joueur : {$jump->getId()} - User : {$user->getId()}",
            );
            
            return false;
        }
        
        if ($idJob === 0) {
            $job = null;
        } else {
            // récupération du job def
            $job = $this->em->getRepository(JobPrototype::class)->findOneBy(['id' => $idJob]);
            if ($job === null) {
                $this->logger->error(
                    "Impossible de mettre à jour le métier définitif du joueur dans l'inscription - Jump : {$jump->getId()} - User : {$user->getId()} - job introuvé : {$idJob}",
                );
                
                return false;
            }
        }
        
        
        $inscription->setMetierDef($job);
        
        
        try {
            $this->em->persist($inscription);
            $this->em->flush();
            
            return true;
        } catch (Exception $exception) {
            $this->logger->error(
                "User : {$user->getId()} - Exception maj metier définitif de l'inscription sur le Jump : {$jump->getId()}  - Exception : {$exception->getMessage()}",
            );
            
            return false;
        }
    }
    
}