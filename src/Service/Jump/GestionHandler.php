<?php


namespace App\Service\Jump;


use App\Entity\Event;
use App\Entity\Jump;
use App\Service\GestHordesHandler;
use App\Structures\Dto\RetourAjax;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GestionHandler
{
    public const NoError                 = 0;
    public const ErrorGenerationBanniere = 1;
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected GestHordesHandler      $gh,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
    )
    {
    }
    
    /**
     * @param $image
     * @param $filename
     * @param $ext
     * @return int
     */
    public function gestionBanniere($image, $filename, $ext): int
    {
        $erreur = $this->gh->regenerationImage($image, GestHordesHandler::ImageProcessingPreferImagick, $ext);
        
        if ($erreur !== GestHordesHandler::NoError) {
            return self::ErrorGenerationBanniere;
        }
        
        $fileopen = (fopen('../public/uploads/banniere/' . $filename, 'w'));
        fwrite($fileopen, (string)$image);
        fclose($fileopen);
        
        return self::NoError;
    }
    
    public function updateBanniere(string $id_banniere, ?string $filename = null, int $typeUpdate = 1,
                                   ?Event $event = null, ?Jump $jump = null): RetourAjax
    {
        
        $retourAjax = new RetourAjax();
        
        try {
            $type = '';
            if ($event !== null) {
                $this->entityManager->persist($event);
                $type = 'event';
            } elseif ($jump !== null) {
                $this->entityManager->persist($jump);
                $type = 'jump';
            }
            $this->entityManager->flush();
            
            $retourAjax->setCodeRetour(0);
            $retourAjax->setLibRetour($this->translator->trans("Mise à jour effectuée.", [], 'jumpEvent'));
            if ($typeUpdate == 1) {
                $retourAjax->setZoneRetour(json_encode(['urlBanniere' => '/uploads/banniere/' . $filename],
                                                       JSON_THROW_ON_ERROR));
            } else {
                $retourAjax->setZoneRetour(json_encode(['urlBanniere' => null]));
            }
        } catch (Exception $exception) {
            if ($typeUpdate == 1) {
                $this->logger->error('Probleme lors de la modification/ajout de la bannière - ' . $type . ' : ' .
                                     $id_banniere . ' - ' . $exception);
                $this->gh->generateMessageDiscord('Probleme lors de la modification/ajout de la bannière - ' . $type .
                                                  ' : ' . $id_banniere . ' - ' . $exception);
                
            } else {
                $this->logger->error('Probleme lors de la suppression de la bannière - ' . $type . ' : ' .
                                     $id_banniere . ' - ' . $exception);
                $this->gh->generateMessageDiscord('Probleme lors de la suppression de la bannière - ' . $type . ' : ' .
                                                  $id_banniere . ' - ' . $exception);
                
            }
            $retourAjax->setCodeRetour(1);
            $retourAjax->setLibRetour(
                $this->translator->trans(
                    "Un problème est survenu, veuillez actualiser et recommencer. Si le problème est toujours persistant, contacter l'administrateur.",
                    [],
                    'jumpEvent',
                ),
            );
        }
        
        return $retourAjax;
    }
}