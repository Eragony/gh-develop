<?php


namespace App\Service\OptionPerso;


use App\Entity\CreneauHorraire;
use App\Entity\DispoCreneauUserTypeExpedition;
use App\Entity\DispoUserType;
use App\Entity\DispoUserTypeExpedition;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\ThemeUser;
use App\Entity\TypeDispo;
use App\Entity\User;
use App\Entity\UserPersoCouleur;
use App\Entity\UserPersonnalisation;
use App\Enum\ListColor;
use App\Exception\GestHordesException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class OptionPersoHandler
{
    public function __construct(private readonly EntityManagerInterface $em, private readonly User $user)
    {
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleCheckModifyUser(User &$user, User $userModify, string $property): void
    {
        $getter = 'get' . ucfirst($property);
        $isser  = 'is' . ucfirst($property);
        
        if (method_exists($user, $getter)) {
            $method = $getter;
        } elseif (method_exists($user, $isser)) {
            $method = $isser;
        }
        $setter = 'set' . ucfirst($property);
        
        if ($userModify->$method() === null) {
            throw new GestHordesException(sprintf('La propriété %s doit être renseigné', $property));
        } else {
            if ($user->$method() !== $userModify->$method()) {
                $user->$setter($userModify->$method());
            }
        }
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleCheckModifyUserPerso(UserPersonnalisation|UserPersoCouleur &$user, UserPersonnalisation|UserPersoCouleur $userModify, string $property): void
    {
        
        $getter = 'get' . ucfirst($property);
        $isser  = 'is' . ucfirst($property);
        
        if (method_exists($user, $getter)) {
            $method = $getter;
        } elseif (method_exists($user, $isser)) {
            $method = $isser;
        }
        $setter = 'set' . ucfirst($property);
        
        if ($userModify->$method() === null) {
            throw new GestHordesException(sprintf('La propriété %s doit être renseigné', $property));
        } else {
            if ($user->$method() !== $userModify->$method()) {
                $user->$setter($userModify->$method());
            }
        }
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleChoixDerPouvoir(User &$user, User $userNew): void
    {
        if ($userNew->getDerPouv() === null) {
            throw new GestHordesException('Le dernier pouvoir acquis est mal renseigné');
        } else {
            if ($user->getDerPouv()->getId() !== $userNew->getDerPouv()->getId()) {
                
                // On récupère le pouboir héros en base de donnée
                $derPouvBdd = $this->em->getRepository(HerosPrototype::class)->findOneBy(['id' => $userNew->getDerPouv()
                                                                                                          ->getId()]);
                
                if ($derPouvBdd === null) {
                    throw new GestHordesException('Le dernier pouvoir acquis n\'existe pas en base de donnée');
                }
                
                $user->setDerPouv($derPouvBdd);
            }
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleChoixTheme(User &$user, User $userNew, int $themeId): void
    {
        if ($userNew->getTheme() === null || !in_array($userNew->getTheme(), User::LIST_THEME)) {
            throw new GestHordesException('Le thème est mal renseigné');
        } else {
            
            
            if ($user->getTheme() !== $userNew->getTheme() || $userNew->getTheme() === User::THEME_PERSO) {
                
                // On récupère le thème en base de donnée pour vérifier qu'il existe
                $themeBdd = $this->em->getRepository(ThemeUser::class)->findOneBy(['id' => $themeId, 'user' => $user]);
                
                // Si le thème n'existe pas en base de donnée, on fait une erreur et on arrête
                if ($themeBdd === null) {
                    throw new GestHordesException('Le thème n\'existe pas en base de donnée');
                }
                
                // On recherche le theme dans la liste des thèmes de l'utilisateur, on désélectionne tous les thèmes et on sélectionne le thème choisi
                foreach ($user->getThemesUser() as $themeUser) {
                    if ($themeUser->getId() === $themeId) {
                        $themeUser->setSelected(true);
                    } else {
                        $themeUser->setSelected(false);
                    }
                }
                
                $user->setTheme($userNew->getTheme());
            }
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleColorMap(UserPersoCouleur &$user, UserPersoCouleur $userNew): void
    {
        // controle si la couleur choisie est dans la liste
        if ($userNew->getColormap() === null || !in_array($userNew->getColormap(), ListColor::cases())) {
            throw new GestHordesException('La couleur de la map est mal renseignée');
        } else {
            if ($user->getColormap() !== $userNew->getColormap()) {
                $user->setColormap($userNew->getColormap());
            }
        }
        // controle de la transparence
        if ($userNew->getAlphaColormap() === null || $userNew->getAlphaColormap() < 0 ||
            $userNew->getAlphaColormap() > 100) {
            throw new GestHordesException('La transparence de la map est mal renseignée');
        } else {
            if ($user->getAlphaColormap() !== $userNew->getAlphaColormap()) {
                $user->setAlphaColormap($userNew->getAlphaColormap());
            }
        }
    }
    
    public function controleCompetencePrefUser(User &$user, User $userNew): void
    {
        $newCompetence = [];
        foreach ($userNew->getSkillType() as $skill) {
            $newCompetence[$skill->getId()] = $skill;
        }
        
        foreach ($user->getSkillType() as $skill) {
            if (!isset($newCompetence[$skill->getId()])) {
                $user->removeSkillType($skill);
            } else {
                unset($newCompetence[$skill->getId()]);
            }
        }
        
        foreach ($newCompetence as $skill) {
            $skillBdd = $this->em->getRepository(HerosSkillLevel::class)->find($skill->getId());
            $user->addSkillType($skillBdd);
        }
    }
    
    public function controleCompetenceUser(User &$user, User $userNew): void
    {
        $newCompetence = [];
        foreach ($userNew->getSkill() as $skill) {
            $newCompetence[$skill->getId()] = $skill;
        }
        
        foreach ($user->getSkill() as $skill) {
            if (!isset($newCompetence[$skill->getId()])) {
                $user->removeSkill($skill);
            } else {
                unset($newCompetence[$skill->getId()]);
            }
        }
        
        foreach ($newCompetence as $skill) {
            $skillBdd = $this->em->getRepository(HerosSkillLevel::class)->find($skill->getId());
            $user->addSkill($skillBdd);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleCouleurModifyThemeUser(ThemeUser &$themeUser, ThemeUser $themeUserMod, string $method, int $length = 9): void
    {
        $getter = 'get' . ucfirst($method);
        $setter = 'set' . ucfirst($method);
        if (strlen($themeUserMod->$getter()) !== $length) {
            throw new GestHordesException(sprintf('La couleur %s doit faire %d caractères', $method, $length));
        } else {
            if ($themeUser->$getter() !== $themeUserMod->$getter()) {
                $themeUser->$setter($themeUserMod->$getter());
            }
        }
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleCouleurModifyUser(UserPersoCouleur &$user, UserPersoCouleur $userModify, string $method, int $length = 9): void
    {
        $getter = 'get' . ucfirst($method);
        $setter = 'set' . ucfirst($method);
        if (strlen($userModify->$getter()) !== $length) {
            throw new GestHordesException(sprintf('La couleur %s doit faire %d caractères', $method, $length));
        } else {
            if ($user->$getter() !== $userModify->$getter()) {
                $user->$setter($userModify->$getter());
            }
        }
        
    }
    
    public function controleDispoTypeExpeUser(User &$user, User $userNew): void
    {
        // On va balayer les dispos types du nouvel utilisateur pour modifier l'utilisateur actuel, on ne faire rien si la dispo n'a pas d'id
        $dispoNew = [];
        foreach ($userNew->getDispoUserTypeExpeditions() as $dispoTypeExpe) {
            if ($dispoTypeExpe->getId() !== null) {
                $dispoNew[$dispoTypeExpe->getId()] = $dispoTypeExpe;
            }
        }
        
        // On va balayer les dispos types de l'utilisateur pour les mettre à jour
        foreach ($user->getDispoUserTypeExpeditions() as $dispoTypeExpe) {
            // on vérifie si la dispo existe dans les nouvelles dispos
            if (isset($dispoNew[$dispoTypeExpe->getId()])) {
                $dispoTypeExpe->setPriorite($dispoNew[$dispoTypeExpe->getId()]->getPriorite());
            }
        }
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleDispoUser(User &$user, User $userNew): void
    {
        // contrôle des différentes dispos fournis
        foreach ($userNew->getDispoTypes() as $dispo) {
            
            if ($dispo === null || $dispo->getId() === null) {
                throw new GestHordesException('La disponibilité est mal renseigné');
            } else {
                if ($user->getDispoType($dispo->getId()) !== null &&
                    $user->getDispoType($dispo->getId())->getDispo()->getId() !== $dispo->getDispo()->getId()) {
                    
                    $dispoBdd =
                        $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispo->getDispo()->getId()]);
                    if ($dispoBdd === null) {
                        throw new GestHordesException('La disponibilité n\'existe pas en base de donnée');
                    }
                    $creneauBdd =
                        $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => $dispo->getCreneau()
                                                                                                   ->getId()]);
                    if ($creneauBdd === null) {
                        throw new GestHordesException('Le créneau horaire n\'existe pas en base de donnée');
                    }
                    
                    $user->setDispoType($dispo->getTypeCreneau(), $creneauBdd, $dispoBdd);
                } else {
                    if ($user->getDispoType($dispo->getId()) === null) {
                        $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispo->getDispo()
                                                                                                         ->getId()]);
                        if ($dispoBdd === null) {
                            throw new GestHordesException('La disponibilité n\'existe pas en base de donnée');
                        }
                        $creneauBdd =
                            $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => $dispo->getCreneau()
                                                                                                       ->getId()]);
                        if ($creneauBdd === null) {
                            throw new GestHordesException('Le créneau horaire n\'existe pas en base de donnée');
                        }
                        
                        $user->addDispoType((new DispoUserType($dispo->getTypeCreneau(),
                                                               $creneauBdd))->setDispo($dispoBdd));
                    }
                }
                
            }
            
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleFuseauHorraire(User &$user, User $userNew): void
    {
        if ($userNew->getFuseau() === null) {
            throw new GestHordesException('Le fuseau horaire est mal renseigné');
        } else {
            if ($user->getFuseau() !== $userNew->getFuseau()) {
                $user->setFuseau($userNew->getFuseau());
            }
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleSaisieCouleurPersonnalisation(UserPersoCouleur $couleurPersoTheme, UserPersoCouleur $couleurPersoThemeNew): void
    {
        // Contrôle de toutes les zones de couleur pour les mettre à jour si besoin
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurScrut');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurKm');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurPos');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurBat');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurSelectObj');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurSelectBat');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurSelectCit');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurVueAuj');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurVue24');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurVue48');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurEpuise');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorSelExp');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurSelCaseMaj');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDanger0');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDanger1');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDanger2');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDanger3');
        // $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDanger4');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurPA');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurZone');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorZombie');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorEstimationZombie');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurCarte');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorStairUp');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorStairDown');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorSelectObjetRuine');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorDispo1');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorDispo2');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorDispo3');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorFlag');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorFlagFinish');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorMyExp');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorVilleCommune');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'myColorExpe');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorTown');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorCity');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorNonVu');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurControleOk');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurContoleNok');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'colorBatEpuise');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurNbrCitoyen');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurNbrItemsSol');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurChantierConstruit');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurChantierARepa');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurChantierEnConstruction');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurChantierDispo');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurPlanManquant');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurDebutant');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurApprenti');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurExpert');
        $this->controleCouleurModifyUser($couleurPersoTheme, $couleurPersoThemeNew, 'couleurElite');
        $this->controleCheckModifyUserPerso($couleurPersoTheme, $couleurPersoThemeNew, 'carteTextured');
        $this->controleColorMap($couleurPersoTheme, $couleurPersoThemeNew);
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleSaisieUtilisateur(User $userNew, int $themeId): User
    {
        // récupération de l'utilisateur en bdd pour pouvoir comparer les valeurs et mettre à jour si besoin
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userNew->getId()]);
        
        if ($user === null) {
            throw new GestHordesException('Utilisateur non trouvé');
        }
        
        $userPersonnalisation    = $user->getUserPersonnalisation();
        $userPersonnalisationNew = $userNew->getUserPersonnalisation();
        
        $themeUser    = $user->getThemesUser()->filter(fn(ThemeUser $theme) => $theme->isSelected())->current();
        $themeUserNew = $userNew->getThemesUser()->filter(fn(ThemeUser $theme) => $theme->isSelected())->current();
        
        $couleurPersoTheme    = $themeUser->getUserPersoCouleur();
        $couleurPersoThemeNew = $themeUserNew->getUserPersoCouleur();
        
        $this->controleSaisieCouleurPersonnalisation($couleurPersoTheme, $couleurPersoThemeNew);
        
        // Contrôle de toutes les zones checkbox ou radio pour les mettre à jour si besoin
        $this->controleCheckModifyUser($user, $userNew, 'temArma');
        $this->controleCheckModifyUser($user, $userNew, 'legend');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'citoyensModeCompact');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'blocMajCitoyens');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'blocMajCitoyen');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'popUpClick');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'figeMenu');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'affichageNbCitoyen');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'afficherNbrItemsSol');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'zombieDiscret');
        $this->controleCheckModifyUser($user, $userNew, 'showHistoPicto');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'keepMenuOpen');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'majCo');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'expeOnTheTop');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'onTopCitoyen');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'widthCase');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'resizabled');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'fixInscriptionExpe');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'menuBandeau');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'itemSeparedCate');
        //$this->controleCheckModifyUser($user, $userNew, 'menuBandeau');
        $this->controleCheckModifyUser($user, $userNew, 'nbChargeCamaraderie');
        $this->controleCheckModifyUser($user, $userNew, 'nbrPointsCompetence');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'motifEpuisement');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'affichageBat');
        $this->controleCheckModifyUserPerso($userPersonnalisation, $userPersonnalisationNew, 'actionDiffCarteAlter');
        
        // Contrôle de diverse zone
        $this->controleChoixTheme($user, $userNew, $themeId);
        $this->controleChoixDerPouvoir($user, $userNew);
        $this->controleDispoUser($user, $userNew);
        
        $this->controleFuseauHorraire($user, $userNew);
        
        // On va mettre à jour les dispos types des expéditions
        $this->controleDispoTypeExpeUser($user, $userNew);
        
        // On va mettre à jours les compétences de l'utilisateur
        $this->controleCompetenceUser($user, $userNew);
        $this->controleCompetencePrefUser($user, $userNew);
        return $user;
    }
    
    /**
     * @throws GestHordesException
     */
    public function controleSaisieUtilisateurForThemeUser(User &$user, ThemeUser $themeUser): ThemeUser
    {
        // récupération de l'utilisateur en bdd pour pouvoir comparer les valeurs et mettre à jour si besoin
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
        
        if ($user === null) {
            throw new GestHordesException('Utilisateur non trouvé');
        }
        
        // Verification si le theme existe en base de donnée, si oui, on récupère, sinon, on en instancie un nouveau
        $themeUserBdd = $this->em->getRepository(ThemeUser::class)->findOneBy(['id' => $themeUser->getId()]);
        if ($themeUserBdd === null) {
            $themeUserBdd = new ThemeUser();
            $themeUserBdd->setUser($user);
            $themeUserBdd->setUserPersoCouleur(new UserPersoCouleur());
            $user->addThemesUser($themeUserBdd);
            
            // Dans le cas d'un nouveau theme, on vérifie que userPersoCouleur est bien renseigné
            if ($themeUser->getUserPersoCouleur() === null) {
                $themeUser->setUserPersoCouleur(new UserPersoCouleur());
            }
        } else {
            // on vérifie que le theme récupérer correspond bien à l'utilisateur
            if ($themeUserBdd->getUser()->getId() !== $user->getId()) {
                throw new GestHordesException('Le thème récupéré ne correspond pas à l\'utilisateur');
            }
        }
        
        // Contrôle de toutes les zones de couleur pour les mettre à jour si besoin
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'statsBgColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'primaryBorderColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'backgroundColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'myLineColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'primaryColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'secondaryColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'tertiaryColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'hoverFontColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'primaryFontColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'tertiaryFontColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'primaryRowColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'secondaryRowColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'statsFontColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'statsBorderColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'bgHoverColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'succesColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'errorColor');
        $this->controleCouleurModifyThemeUser($themeUserBdd, $themeUser, 'specifiqueColor');
        
        // Contrôle si le theme de base est renseigné et correspond à l'un des 5 thèmes
        if ($themeUser->getBaseTheme() === null || !in_array($themeUser->getBaseTheme(), User::LIST_THEME)) {
            throw new GestHordesException('Le thème est mal renseigné');
        } else {
            if ($themeUserBdd->getBaseTheme() !== $themeUser->getBaseTheme()) {
                $themeUserBdd->setBaseTheme($themeUser->getBaseTheme());
            }
        }
        
        // On vérifie si le nom du theme ne fait pas plus de 60 caractéres
        if (strlen($themeUser->getNom()) > 60) {
            throw new GestHordesException('Le nom du thème ne doit pas faire plus de 60 caractères');
        } else {
            if ($themeUserBdd->getNom() !== $themeUser->getNom()) {
                $themeUserBdd->setNom($themeUser->getNom());
            }
        }
        
        $this->controleSaisieCouleurPersonnalisation($themeUserBdd->getUserPersoCouleur(), $themeUser->getUserPersoCouleur());
        
        return $themeUserBdd;
    }
    
    /**
     * @throws GestHordesException
     */
    public function createNewDispo(User $user, DispoUserTypeExpedition $dispoExpe): DispoUserTypeExpedition
    {
        
        // On récupère la dernière dispo de l'utilisateur pour récupérer la dernière priorité
        $lastDispo = $this->em->getRepository(DispoUserTypeExpedition::class)->findOneBy(['user' => $user], ['priorite' => 'DESC']);
        
        $newDispo = new DispoUserTypeExpedition();
        $newDispo->setUser($user)
                 ->setNom($dispoExpe->getNom())
                 ->setPriorite($lastDispo === null ? 1 : $lastDispo->getPriorite() + 1);
        
        // On va balayer les disponibilités types pour les ajouter à la dispo de l'utilisateur
        foreach ($dispoExpe->getCreneau() as $dispoCreneau) {
            // On récupère le créneau horaire en base de donnée
            $creneauBdd = $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => $dispoCreneau->getCreneau()->getId()]);
            // Si le créneau n'existe pas en base de donnée, on fait une erreur et on arrête
            if ($creneauBdd === null) {
                throw new GestHordesException('Le créneau horaire n\'existe pas en base de donnée');
            }
            // On récupère la dispo type en base de donnée
            $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispoCreneau->getDispo()->getId()]);
            // Si la dispo n'existe pas en base de donnée, on fait une erreur et on arrête
            if ($dispoBdd === null) {
                throw new GestHordesException('La disponibilité n\'existe pas en base de donnée');
            }
            
            $newDispo->addCreneau((new DispoCreneauUserTypeExpedition())->setCreneau($creneauBdd)->setDispo($dispoBdd));
        }
        
        $this->em->persist($newDispo);
        $this->em->flush();
        $this->em->refresh($newDispo);
        
        return $newDispo;
    }
    
    /**
     * @throws GestHordesException
     */
    public function majDispo(User $user, DispoUserTypeExpedition $dispoExpe): DispoUserTypeExpedition
    {
        
        // On récupère la dispo en base de donnée
        $dispoExpeBdd = $this->em->getRepository(DispoUserTypeExpedition::class)->findOneBy(['id' => $dispoExpe->getId()]);
        // Si la disponibilité type n'existe pas en base de donnée, on fait une erreur et on arrête
        if ($dispoExpeBdd === null) {
            throw new GestHordesException('La disponibilité type n\'existe pas en base de donnée');
        }
        
        // On effectue que de la mise à jour
        $dispoExpeBdd->setNom($dispoExpe->getNom());
        
        // On va balayer les nouvelles dispos type pour chaque créneau pour créer un tableau de comparaison
        $newDispo = [];
        $oldDispo = [];
        foreach ($dispoExpe->getCreneau() as $dispoCreneau) {
            if ($dispoCreneau->getId() === null) {
                $newDispo[] = $dispoCreneau;
            } else {
                $oldDispo[$dispoCreneau->getId()] = $dispoCreneau;
            }
        }
        
        // On va balayer les dispos pour modifier les dispos de l'utilisateur
        foreach ($dispoExpeBdd->getCreneau() as $dispoCreneauBdd) {
            if (!isset($oldDispo[$dispoCreneauBdd->getId()])) {
                $dispoExpeBdd->removeCreneau($dispoCreneauBdd);
            } else {
                $dispoCreneau = $oldDispo[$dispoCreneauBdd->getId()];
                
                // On récupère la disponibilité en base de donnée pour mettre à jour la disponibilité
                $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispoCreneau->getDispo()->getId()]);
                // Si la dispo n'existe pas en base de donnée, on fait une erreur et on arrête
                if ($dispoBdd === null) {
                    throw new GestHordesException('La disponibilité n\'existe pas en base de donnée');
                }
                
                $dispoCreneauBdd->setDispo($dispoBdd);
            }
        }
        
        // s'il reste des dispos non traitées
        foreach ($newDispo as $dispoCreneau) {
            // On récupère le créneau horaire en base de donnée
            $creneauBdd = $this->em->getRepository(CreneauHorraire::class)->findOneBy(['id' => $dispoCreneau->getCreneau()->getId()]);
            // Si le créneau n'existe pas en base de donnée, on fait une erreur et on arrête
            if ($creneauBdd === null) {
                throw new GestHordesException('Le créneau horaire n\'existe pas en base de donnée');
            }
            // On récupère la dispo en base de donnée
            $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispoCreneau->getDispo()->getId()]);
            // Si la disponibilité n'existe pas en base de donnée, on fait une erreur et on arrête
            if ($dispoBdd === null) {
                throw new GestHordesException('La disponibilité n\'existe pas en base de donnée');
            }
            
            $dispoExpeBdd->addCreneau((new DispoCreneauUserTypeExpedition())->setCreneau($creneauBdd)->setDispo($dispoBdd));
        }
        
        $this->em->persist($dispoExpeBdd);
        $this->em->flush();
        $this->em->refresh($dispoExpeBdd);
        
        return $dispoExpeBdd;
    }
    
    /**
     * @throws GestHordesException
     */
    public function majDispoExpeditionUser(User $user, DispoUserTypeExpedition $dispoExpe): DispoUserTypeExpedition
    {
        // Verification si la dispo existe en base de donnée, si oui, on récupère, sinon, on en instancie une nouvelle
        $dispoExpeBdd = $this->em->getRepository(DispoUserTypeExpedition::class)->findOneBy(['id' => $dispoExpe->getId()]);
        if ($dispoExpeBdd === null) {
            return $this->createNewDispo($user, $dispoExpe);
        } else {
            return $this->majDispo($user, $dispoExpe);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function majThemeUserDeselect(User $user): void
    {
        try {
            foreach ($user->getThemesUser() as $themeUser) {
                if ($themeUser->isSelected()) {
                    $themeUser->setSelected(false);
                    $this->em->persist($themeUser);
                }
            }
            
            $this->em->flush();
        } catch (Exception $e) {
            throw new GestHordesException('Erreur lors de la désélection du thème');
        }
        
    }
    
}