<?php

namespace App\Service;

use App\Entity\HerosPrototype;
use App\Entity\Jump;
use App\Entity\Menu;
use App\Entity\ThemeUser;
use App\Entity\User;
use App\Entity\UserPersoCouleur;
use App\Entity\UserPersonnalisation;
use App\Entity\Ville;
use App\Exception\UserNotFoundException;
use App\Service\Generality\MenuHandler;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Utils\NewUser;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserHandler
{
    
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack           $requestStack,
        private readonly Security               $security,
        private readonly SerializerService      $serializerService,
        private MenuHandler                     $menuHandler,
    )
    {
    }
    
    public function createNewUserFromApi(NewUser $newUser, bool $isTheUser = false): User
    {
        $user = new User();
        
        $derPouv = $this->em->getRepository(HerosPrototype::class)->find(28);
        
        $user->setIdMyHordes($newUser->getIdMh())
             ->setPseudo($newUser->getPseudo())
             ->setAvatar($newUser->getAvatar())
             ->setMapId($newUser->getMapId())
             ->setLang($newUser->getLang())
             ->setRoles(['ROLE_USER'])
             ->setTheme(User::THEME_HORDES)
             ->setDerPouv($derPouv);
        
        $user->setUserPersonnalisation(new UserPersonnalisation());
        
        
        // création des 4 thémes de base pour l'utilisateur
        $baseHordes = new ThemeUser();
        $baseHordes->setNom('Hordes')
                   ->setStatsBgColor("#252525FF")
                   ->setPrimaryBorderColor("#DDDDDDFF")
                   ->setBackgroundColor("#00000000")
                   ->setMyLineColor("#2300B44C")
                   ->setPrimaryColor("#5C2B20FF")
                   ->setSecondaryColor("#7E4D2AFF")
                   ->setTertiaryColor("#47271CFF")
                   ->setHoverFontColor("#FFFFFFFF")
                   ->setPrimaryFontColor("#FFFFFFFF")
                   ->setTertiaryFontColor("#eba475FF")
                   ->setPrimaryRowColor("#653A26FF")
                   ->setSecondaryRowColor("#8A5432FF")
                   ->setStatsFontColor("#ffffffFF")
                   ->setStatsBorderColor("#FFFFFF26")
                   ->setBgHoverColor("#47271CFF")
                   ->setSuccesColor("#00f300FF")
                   ->setErrorColor("#ff0000FF")
                   ->setBaseTheme(User::THEME_HORDES)
                   ->setSelected(true)
                   ->setUserPersoCouleur(new UserPersoCouleur());
        
        $user->addThemesUser($baseHordes);
        
        $baseDark = new ThemeUser();
        $baseDark->setNom('Dark')
                 ->setStatsBgColor("#252525FF")
                 ->setPrimaryBorderColor("#DDDDDDFF")
                 ->setBackgroundColor("#00000000")
                 ->setMyLineColor("#2300B44C")
                 ->setPrimaryColor("#1d2536FF")
                 ->setSecondaryColor("#3A4161FF")
                 ->setTertiaryColor("#333469FF")
                 ->setHoverFontColor("#FFFFFFFF")
                 ->setPrimaryFontColor("#DDDDDDFF")
                 ->setTertiaryFontColor("#1d2536FF")
                 ->setPrimaryRowColor("#2b3650FF")
                 ->setSecondaryRowColor("#3A4161FF")
                 ->setStatsFontColor("#ffffffFF")
                 ->setStatsBorderColor("#FFFFFF26")
                 ->setBgHoverColor("#333469FF")
                 ->setSuccesColor("#00f300FF")
                 ->setErrorColor("#ff0000FF")
                 ->setBaseTheme(User::THEME_DARK)
                 ->setSelected(false)
                 ->setUserPersoCouleur(new UserPersoCouleur());
        
        $user->addThemesUser($baseDark);
        
        $baseLight = new ThemeUser();
        $baseLight->setNom('Light')
                  ->setStatsBgColor("#FFFFFFFF")
                  ->setPrimaryBorderColor("#000000FF")
                  ->setBackgroundColor("#00000000")
                  ->setMyLineColor("#FFA4004C")
                  ->setPrimaryColor("#fff2d4FF")
                  ->setSecondaryColor("#f7dcb1FF")
                  ->setTertiaryColor("#339966FF")
                  ->setHoverFontColor("#FFFFFFFF")
                  ->setPrimaryFontColor("#000000FF")
                  ->setTertiaryFontColor("#2d755bFF")
                  ->setPrimaryRowColor("#d6c3aeFF")
                  ->setSecondaryRowColor("#fff2d4FF")
                  ->setStatsFontColor("#000000FF")
                  ->setStatsBorderColor("#00000026")
                  ->setBgHoverColor("#2d755bFF")
                  ->setSuccesColor("#198754FF")
                  ->setErrorColor("#ff0000FF")
                  ->setBaseTheme(User::THEME_LIGHT)
                  ->setSelected(false)
                  ->setUserPersoCouleur(new UserPersoCouleur());
        
        $user->addThemesUser($baseLight);
        
        $baseVintage = new ThemeUser();
        $baseVintage->setNom('Vintage')
                    ->setStatsBgColor("#FFFFFFFF")
                    ->setPrimaryBorderColor("#000000FF")
                    ->setBackgroundColor("#00000000")
                    ->setMyLineColor("#FFA4004C")
                    ->setPrimaryColor("#fff2d4FF")
                    ->setSecondaryColor("#fff2d4FF")
                    ->setTertiaryColor("#fbc379FF")
                    ->setHoverFontColor("#000000FF")
                    ->setPrimaryFontColor("#000000FF")
                    ->setTertiaryFontColor("#000000FF")
                    ->setPrimaryRowColor("#ffd991FF")
                    ->setSecondaryRowColor("#fff2d4FF")
                    ->setStatsFontColor("#000000FF")
                    ->setStatsBorderColor("#00000026")
                    ->setBgHoverColor("#ffffffff")
                    ->setSuccesColor("#198754FF")
                    ->setErrorColor("#ff0000FF")
                    ->setBaseTheme(User::THEME_VINTAGE)
                    ->setSelected(false)
                    ->setUserPersoCouleur(new UserPersoCouleur());
        $user->addThemesUser($baseVintage);
        
        if ($isTheUser) {
            $user->setLastConnexionAt(new DateTimeImmutable('now'));
        }
        
        $this->em->persist($user);
        $this->em->flush();
        $this->em->refresh($user);
        
        return $user;
    }
    
    public function getBaseTheme(): array
    {
        $arrayBaseCouleur = $this->serializerService->serializeArray(new UserPersoCouleur(), 'json', ['option']);
        
        return [
            'dark'    => [
                'nom'                => 'Dark',
                'statsBgColor'       => '#252525FF',
                'primaryBorderColor' => '#DDDDDDFF',
                'backgroundColor'    => '#00000000',
                'myLineColor'        => '#2300B44C',
                'primaryColor'       => '#1d2536FF',
                'secondaryColor'     => '#3A4161FF',
                'tertiaryColor'      => '#333469FF',
                'hoverFontColor'     => '#FFFFFFFF',
                'primaryFontColor'   => '#DDDDDDFF',
                'tertiaryFontColor'  => '#1d2536FF',
                'primaryRowColor'    => '#2b3650FF',
                'secondaryRowColor'  => '#3A4161FF',
                'statsFontColor'     => '#ffffffFF',
                'statsBorderColor'   => '#FFFFFF26',
                'bgHoverColor'       => '#333469FF',
                'specifiqueColor'    => '#ff0000FF',
                'successColor'       => '#00f300FF',
                'errorColor'         => '#ff0000FF',
                'perso'              => $arrayBaseCouleur,
            ],
            'light'   => [
                'nom'                => 'Light',
                'statsBgColor'       => '#FFFFFFFF',
                'primaryBorderColor' => '#000000FF',
                'backgroundColor'    => '#00000000',
                'myLineColor'        => '#FFA4004C',
                'primaryColor'       => '#fff2d4FF',
                'secondaryColor'     => '#f7dcb1FF',
                'tertiaryColor'      => '#339966FF',
                'hoverFontColor'     => '#FFFFFFFF',
                'primaryFontColor'   => '#000000FF',
                'tertiaryFontColor'  => '#2d755bFF',
                'primaryRowColor'    => '#d6c3aeFF',
                'secondaryRowColor'  => '#fff2d4FF',
                'statsFontColor'     => '#000000FF',
                'statsBorderColor'   => '#00000026',
                'bgHoverColor'       => '#2d755bFF',
                'specifiqueColor'    => '#ff0000FF',
                'successColor'       => '#198754FF',
                'errorColor'         => '#ff0000FF',
                'perso'              => $arrayBaseCouleur,
            ],
            'vintage' => [
                'nom'                => 'Vintage',
                'statsBgColor'       => '#FFFFFFFF',
                'primaryBorderColor' => '#000000FF',
                'backgroundColor'    => '#00000000',
                'myLineColor'        => '#FFA4004C',
                'primaryColor'       => '#fff2d4FF',
                'secondaryColor'     => '#fff2d4FF',
                'tertiaryColor'      => '#fbc379FF',
                'hoverFontColor'     => '#000000FF',
                'primaryFontColor'   => '#000000FF',
                'tertiaryFontColor'  => '#000000FF',
                'primaryRowColor'    => '#FFFFFF33',
                'secondaryRowColor'  => '#00000033',
                'statsFontColor'     => '#000000FF',
                'statsBorderColor'   => '#00000026',
                'bgHoverColor'       => '#ffffffFF',
                'specifiqueColor'    => '#ff0000FF',
                'successColor'       => '#198754FF',
                'errorColor'         => '#ff0000FF',
                'perso'              => $arrayBaseCouleur,
            ],
            'hordes'  => [
                'nom'                => 'Hordes',
                'statsBgColor'       => '#252525FF',
                'primaryBorderColor' => '#DDDDDDFF',
                'backgroundColor'    => '#00000000',
                'myLineColor'        => '#2300B44C',
                'primaryColor'       => '#5C2B20FF',
                'secondaryColor'     => '#7E4D2AFF',
                'tertiaryColor'      => '#47271CFF',
                'hoverFontColor'     => '#FFFFFFFF',
                'primaryFontColor'   => '#FFFFFFFF',
                'tertiaryFontColor'  => '#eba475FF',
                'primaryRowColor'    => '#653A26FF',
                'secondaryRowColor'  => '#8A5432FF',
                'statsFontColor'     => '#ffffffFF',
                'statsBorderColor'   => '#FFFFFF26',
                'bgHoverColor'       => '#47271CFF',
                'specifiqueColor'    => '#ff0000FF',
                'successColor'       => '#00f300FF',
                'errorColor'         => '#ff0000FF',
                'perso'              => $arrayBaseCouleur,
            ],
            'perso'   => [
                'nom'                => '',
                'statsBgColor'       => '#252525FF',
                'primaryBorderColor' => '#DDDDDDFF',
                'backgroundColor'    => '#5C2B2080',
                'myLineColor'        => '#2300B44C',
                'primaryColor'       => '#5C2B20FF',
                'secondaryColor'     => '#7E4D2AFF',
                'tertiaryColor'      => '#47271CFF',
                'hoverFontColor'     => '#FFFFFFFF',
                'primaryFontColor'   => '#FFFFFFFF',
                'tertiaryFontColor'  => '#eba475FF',
                'primaryRowColor'    => '#653A26FF',
                'secondaryRowColor'  => '#8A5432FF',
                'statsFontColor'     => '#ffffffFF',
                'statsBorderColor'   => '#FFFFFF26',
                'bgHoverColor'       => '#47271CFF',
                'specifiqueColor'    => '#ff0000FF',
                'successColor'       => '#00f300FF',
                'errorColor'         => '#ff0000FF',
                'perso'              => $arrayBaseCouleur,
            ],
        ];
    }
    
    public function getCurrentUser(): User
    {
        /** @var User $user */
        $user = $this->security->getUser();
        
        if (!$user) {
            throw new AuthenticationException('Utilisateur non authentifié | fichier ' . __FILE__ . ' | ligne ' . __LINE__);
        }
        
        return $user;
    }
    
    public function getMenuUser(bool $isCo, ?Ville $ville): Menu
    {
        $user = $this->security->getUser() ?? new User();
        
        $menu = $this->menuHandler->recuperationMenu($user);
        
        $inVille = ($ville?->getMapId() ?? null) !== null;
        $myVille = $this->myVille($ville);
        
        return $this->menuHandler->filtrageMenuHabilitation($menu, $user, $isCo, $inVille, $myVille);
    }
    
    public function getTownBySession(?int $mapId = 0): ?Ville
    {

//		if ($mapId !== 0) {
//			//$this->requestStack->getSession()->set('mapId', $mapId);
//		} else {
//			$myMapId = $this->security->getUser()?->getMapId();
//			//$mapId   = $this->requestStack->getSession()->get('mapId') ?? $myMapId ?? null;
//			$mapId = $myMapId ?? null;
//		}
        if ($mapId === 0) {
            $myMapId = $this->security->getUser()?->getMapId();
            $mapId   = $myMapId ?? null;
        }
        
        if ($mapId != null) {
            $ville =
                $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
        } else {
            $ville = null;
        }
        
        
        return $ville;
    }
    
    public function getTownOfUser(User $user): ?Ville
    {
        $mapId = $user->getMapId();
        
        if ($mapId !== null) {
            $town = $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $mapId]);
        } else {
            $town = null;
        }
        
        return $town;
    }
    
    public function getTownUser(User|UserInterface $user): ?Ville
    {
        if ($user->getMapId() != null) {
            $ville = $this->em->getRepository(Ville::class)->findOneBy(['mapId'  => $user->getMapId(),
                                                                        'origin' => Ville::ORIGIN_MH]);
            
            $this->requestStack->getSession()->set('mapId', $user->getMapId());
            $this->requestStack->getSession()->set('ville', $ville);
            
        } else {
            $ville = null;
        }
        
        
        return $ville;
        
    }
    
    public function getUserById(int $userId): User
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);
        
        if (!$user) {
            throw new UserNotFoundException('Utilisateur non trouvé | fichier ' . __FILE__ . ' | ligne ' . __LINE__);
        }
        
        return $user;
    }
    
    public function getUserByIdMh(int $userId): User
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['idMyHordes' => $userId]);
        
        if (!$user) {
            throw new UserNotFoundException('Utilisateur non trouvé | fichier ' . __FILE__ . ' | ligne ' . __LINE__);
        }
        
        return $user;
    }
    
    public function inVille(): bool
    {
        return $this->security->getUser()?->getMapId() != null;
    }
    
    public function isLead(?int $mapId = null): bool
    {
        if (!is_null($this->security->getUser())) {
            foreach ($this->security->getUser()?->getLeadJumps() as $leadJump) {
                
                $villeJump = $leadJump->getJump()->getVille();
                
                if ($mapId === null) {
                    $mapId = $this->requestStack->getSession()->get('mapId');
                }
                
                if ($villeJump !== null && $villeJump->getMapId() == $mapId) {
                    return true;
                }
                
            }
        }
        
        return false;
    }
    
    public function isLeadOrOrga(Jump $jump): bool
    {
        if (!is_null($this->security->getUser())) {
            
            if ($this->isOrga($jump)) {
                return true;
            }
            
            foreach ($this->security->getUser()?->getLeadJumps() as $leadJump) {
                
                $villeJump = $leadJump->getJump();
                
                if ($villeJump !== null && $villeJump->getId() == $jump->getId()) {
                    return true;
                }
                
                
            }
        }
        
        return false;
    }
    
    public function isOrga(Jump $jump): bool
    {
        if (!is_null($this->security->getUser())) {
            foreach ($this->security->getUser()?->getGestionJumps() as $gestionJump) {
                
                if ($gestionJump->getId() == $jump->getId()) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public function myVille(?Ville $ville): bool
    {
        if (!is_null($this->security->getUser())) {
            $user = $this->security->getUser();
            foreach ($user->getLeadJumps() as $leadJump) {
                
                $villeJump = $leadJump->getJump()->getVille();
                
                if ($villeJump !== null && $villeJump->getMapId() == ($ville?->getMapId() ?? null)) {
                    return true;
                }
                
            }
        }
        
        return $this->security->getUser()?->getMapId() != null &&
               $this->security->getUser()?->getMapId() == ($ville?->getMapId() ?? null);
    }
    
    public function recuperationUser(int $idUser): ?User
    {
        return $this->em->getRepository(User::class)->findOneBy(['id' => $idUser]);
    }
    
    public function resolveUser(int $userId = 0): User
    {
        return $userId ? $this->getUserById($userId) : $this->getCurrentUser();
    }
    
    public function resolveUserMh(int $userId = 0): User
    {
        return $userId ? $this->getUserByIdMh($userId) : $this->getCurrentUser();
    }
    
    public function sauvegardeLang(?User $user, string $lang): void
    {
        
        if ($user !== null) {
            
            $userAMaj = $this->em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
            
            $userAMaj->setLang($lang);
            
            $this->em->persist($userAMaj);
            $this->em->flush();
            
        }
        
        
    }
    
    public function verifyUserIsConnected(): void
    {
        $this->getCurrentUser();
    }
    
    public function visitVille(): bool
    {
        return $this->requestStack->getSession()->get('mapId') != null;
    }
}