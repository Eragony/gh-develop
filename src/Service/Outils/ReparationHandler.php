<?php

namespace App\Service\Outils;

use App\Entity\Chantiers;
use App\Entity\OutilsReparation;
use App\Entity\ReparationChantier;
use App\Entity\Ville;
use App\Structures\Collection\Reparations;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class ReparationHandler
{
    
    public function __construct(private TranslatorInterface    $translator,
                                private EntityManagerInterface $entityManager)
    {
    }
    
    /**
     * @param ReparationChantier $reparationChantier
     * @param int $paTotRecalc
     * @return void
     */
    public function calculCoutPARepa(ReparationChantier $reparationChantier, int &$paTotRecalc): void
    {
        $paTotRecalc += match ($reparationChantier->getPctRepa()) {
            0       => 0,
            70      => $reparationChantier->getPaRepa70(),
            99      => $reparationChantier->getPaRepa99(),
            100     => $reparationChantier->getPaRepa100(),
            default => $reparationChantier->getPaRepaPerso()
        };
    }
    
    /**
     * @param ReparationChantier $reparationChantier
     * @param int $gainDefTot
     * @return void
     */
    public function calculGainDef(ReparationChantier $reparationChantier, int &$gainDefTot): void
    {
        $gainDefTot += match ($reparationChantier->getPctRepa()) {
            0       => 0,
            70      => $reparationChantier->getGainDef70(),
            99      => $reparationChantier->getGainDef99(),
            100     => $reparationChantier->getGainDef100(),
            default => $reparationChantier->getGainDefPerso()
        };
    }
    
    public function calculPaALaisser(
        ReparationChantier $reparationChantier,
        bool               $isTwoStep,
        int                $pvByPa,
        int                &$paALaisser,
        int                &$pvAfficher,
        bool               &$repaAFinir,
    ): void
    {
        
        $pctChoix = $reparationChantier->getPctRepa();
        
        $pct = ($pctChoix === 0) ? 0 : (($isTwoStep) ? ($reparationChantier->getPctRepa() > 70 ? 70 : $reparationChantier->getPctRepa()) : $reparationChantier->getPctRepa());
        
        $paRepa = match ($pct) {
            0       => 0,
            70      => $reparationChantier->getPaRepa70(),
            99      => $reparationChantier->getPaRepa99(),
            100     => $reparationChantier->getPaRepa100(),
            default => $reparationChantier->getPaRepaPerso()
        };
        
        $pvRepa = $paRepa * $pvByPa;
        
        $pvAfficher = min($reparationChantier->getPvActuel() + $pvRepa, $reparationChantier->getChantier()->getPv());
        
        if ($pvAfficher <= $reparationChantier->getPvActuel()) {
            $repaAFinir = false;
        } else {
            $repaAFinir = true;
        }
        
        $transfoPvRestant = max($reparationChantier->getChantier()->getPv() - $pvAfficher, 0);
        
        $paALaisser = ceil($transfoPvRestant / $pvByPa);
    }
    
    /**
     * @param ReparationChantier $reparationChantier
     * @param Chantiers $chantiers
     * @param int $pvByPa
     * @param int $paTotReCalc
     * @param int $gainDefTot
     * @param int|null $modPct
     * @return void
     */
    public function calculReparation(ReparationChantier &$reparationChantier, Chantiers $chantiers, int $pvByPa, int &$paTotReCalc, int &$gainDefTot, ?int $modPct = null): void
    {
        
        $pvActuel = $chantiers->getPv();
        
        $defActuel = $chantiers->getDef();
        
        $chantierProto = $chantiers->getChantier();
        
        if ($reparationChantier->getPctRepa() == null) {
            $reparationChantier->setPctRepa(70);
        } else if ($modPct !== null) {
            $reparationChantier->setPctRepa($modPct);
        }
        
        // Calcul des pvs pour le chantier : 70%+1, 99% et 100%
        $pv70    = ceil($chantierProto->getPv() * 0.7 + 1);
        $pv99    = max($chantierProto->getPv() - ($pvByPa - 1), $pv70);
        $pv100   = $chantierProto->getPv();
        $pvPerso = ceil($chantierProto->getPv() * $reparationChantier->getPctRepa() / 100);
        
        $diff70    = max($pv70 - $pvActuel, 0);
        $diff99    = max($pv99 - $pvActuel, 0);
        $diff100   = max($pv100 - $pvActuel, 0);
        $diffPerso = max($pvPerso - $pvActuel, 0);
        
        
        $pa70    = ceil($diff70 / $pvByPa);
        $pa99    = ceil($diff99 / $pvByPa);
        $pa100   = ceil($diff100 / $pvByPa);
        $paPerso = ceil($diffPerso / $pvByPa);
        
        if ($chantierProto->getDef() !== 0) {
            
            $def70    = min(floor(($pvActuel + $pa70 * $pvByPa) * $chantierProto->getDef() / $chantierProto->getPv()), $chantierProto->getDef());
            $def99    = min(floor(($pvActuel + $pa99 * $pvByPa) * $chantierProto->getDef() / $chantierProto->getPv()), $chantierProto->getDef());
            $def100   = $chantierProto->getDef();
            $defPerso = min(floor(($pvActuel + $paPerso * $pvByPa) * $chantierProto->getDef() / $chantierProto->getPv()), $chantierProto->getDef());
            
            $gainDef70    = max($def70 - $defActuel, 0);
            $gainDef99    = max($def99 - $defActuel, 0);
            $gainDef100   = max($def100 - $defActuel, 0);
            $gainDefPerso = max($defPerso - $defActuel, 0);
            
        } else {
            $gainDef70    = 0;
            $gainDef99    = 0;
            $gainDef100   = 0;
            $gainDefPerso = 0;
        }
        
        
        $reparationChantier->setPvActuel($pvActuel)
                           ->setDefActuelle($defActuel)
                           ->setPaRepa70($pa70)
                           ->setPaRepa99($pa99)
                           ->setPaRepa100($pa100)
                           ->setGainDef70($gainDef70)
                           ->setGainDef99($gainDef99)
                           ->setGainDef100($gainDef100)
                           ->setPaRepaPerso($paPerso)
                           ->setGainDefPerso($gainDefPerso);
        
        
        $this->calculCoutPARepa($reparationChantier, $paTotReCalc);
        $this->calculGainDef($reparationChantier, $gainDefTot);
        
    }
    
    /**
     * @param OutilsReparation $outilsReparation
     * @param Ville $ville
     * @param int $pvByPa
     * @return string
     * @throws Exception
     */
    public function generateText(OutilsReparation $outilsReparation, Ville $ville, int $pvByPa): string
    {
        $text = "";
        
        if ($outilsReparation->getReparationChantiers()->count() == 0) {
            return $this->translator->trans("Les réparations sont terminées", [], 'service');
        }
        
        if ($ville->isHard()) {
            $text .= "[rp=" . $this->translator->trans("On optimise les :ap:", [], 'service') . ']' .
                     $this->translator->trans("En pandémonium, il faut réparer les chantiers.", [], 'service');
            $text .= "<br/>";
            $text .= $this->translator->trans("Mais avec minimum à 70 % + 1 (et jusqu'à 99/100% pour certains chantiers) selon la liste ci-dessous :",
                                              [], 'service') . "[/rp]";
            
        } else {
            $text .= $this->translator->trans("On répare les chantiers suivants selon la liste ci-dessous :", [],
                                              'service');
        }
        $text .= "<br/>";
        
        // Il faut maintenant trier par ordre des chantiers
        $reparations     = new Reparations();
        $listChantierTri = $reparations->setReparations($outilsReparation->getReparationChantiers())->triByOrder();
        
        foreach ($listChantierTri as $reparationChantier) {
            $paALaisser = 0;
            $pvAfficher = 0;
            $repaAFinir = true;
            
            $this->calculPaALaisser($reparationChantier, $outilsReparation->getTwoStep(), $pvByPa, $paALaisser,
                                    $pvAfficher, $repaAFinir);
            
            if ($repaAFinir) {
                $text .= "[*]";
                $text .= $this->translator->trans($reparationChantier->getChantier()->getNom(), [], 'chantiers');
                $text .= "→ **";
                $text .= $this->translator->trans("Laisser", [], 'service');
                $text .= "** {$paALaisser}";
                $text .= $this->translator->trans("PA", [], 'service');
                $text .= "[i]({$pvAfficher}/{$reparationChantier->getChantier()->getPv()} ";
                $text .= $this->translator->trans("PV", [], 'service');
                $text .= ")[/i]";
                $text .= "<br/>";
            }
        }
        
        if ($outilsReparation->getTwoStep()) {
            $text .= "<br/>";
            $text .= $this->translator->trans("Une fois les réparations à 70%+1 terminées, veuillez réparer à 99/100% les chantiers dans l'ordre suivant :",
                                              [], 'service');
            $text .= " <br/>";
            
            $listChantier = $outilsReparation->getReparationChantiers()
                                             ->filter(fn(ReparationChantier $repa) => $repa->getPctRepa() > 70);
            
            // Il faut maintenant trier par ordre de rentabilité
            $reparations     = new Reparations();
            $listChantierTri = $reparations->setReparations($listChantier)->triByRentabilite();
            
            foreach ($listChantierTri as $reparationChantier) {
                $paALaisser = 0;
                $pvAfficher = 0;
                $repaAFinir = true;
                $this->calculPaALaisser($reparationChantier, false, $pvByPa, $paALaisser, $pvAfficher, $repaAFinir);
                
                if ($repaAFinir) {
                    $text .= "[*]";
                    $text .= $this->translator->trans($reparationChantier->getChantier()->getNom(), [], 'chantiers');
                    $text .= "→ **";
                    $text .= $this->translator->trans("Laisser", [], 'service');
                    $text .= "** {$paALaisser}";
                    $text .= $this->translator->trans("PA", [], 'service');
                    $text .= "[i]({$pvAfficher}/{$reparationChantier->getChantier()->getPv()}";
                    $text .= $this->translator->trans("PV", [], 'service');
                    $text .= ")[/i]";
                    $text .= "<br/>";
                }
            }
            
        }
        
        $defApres = $outilsReparation->getDefBase() + $outilsReparation->getGainDef();
        
        $text .= "[i]";
        $text .= $this->translator->trans("Pour ceux qui n'ont pas compris, c'est le nombre de :ap: qu'il faut laisser aux chantiers.",
                                          [], 'service');
        $text .= "[/i]<br/>";
        $text .= "[rp=" . $this->translator->trans("Totaux", [], 'service') . "] ";
        $text .= $this->translator->trans("{paTotaux} :ap: pour {gainDef} :guard:",
                                          ['{paTotaux}' => $outilsReparation->getPaTot(),
                                           '{gainDef}'  => $outilsReparation->getGainDef()],
                                          'service');
        $text .= "[/rp]";
        $text .= "<br/>";
        $text .= "[rp=" . $this->translator->trans("Def avant/après", [], 'service') . "] ";
        $text .= $this->translator->trans("Défense chantier en cours : {defAvant}",
                                          ['{defAvant}' => $outilsReparation->getDefBase()], 'service');
        $text .= "<br/>";
        $text .= $this->translator->trans("Défense chantier après les réparations : {defApres}",
                                          ['{defApres}' => $defApres], 'service');
        $text .= "[/rp]";
        $text .= "<br/>";
        
        return $text;
    }
    
    public function recalculPaDefReparation(OutilsReparation &$outilsReparation, Ville $ville, int $pvByPa): void
    {
        $paTotReCalc = 0;
        $gainDefTot  = 0;
        
        $listChantier = $this->entityManager->getRepository(Chantiers::class)->recupChantierAReparer($ville);
        
        // On balaye les chantiers dans le cas d'un existant
        foreach ($outilsReparation->getReparationChantiers() as $reparationChantier) {
            if (isset($listChantier[$reparationChantier->getChantier()->getId()])) {
                if ($reparationChantier->getPvActuel() !=
                    $listChantier[$reparationChantier->getChantier()->getId()]->getPv()) {
                    $this->calculReparation(
                        $reparationChantier,
                        $listChantier[$reparationChantier->getChantier()->getId()],
                        $pvByPa,
                        $paTotReCalc,
                        $gainDefTot,
                    );
                } else {
                    $this->calculCoutPARepa($reparationChantier, $paTotReCalc);
                    $this->calculGainDef($reparationChantier, $gainDefTot);
                }
                unset($listChantier[$reparationChantier->getChantier()->getId()]);
            } else {
                $outilsReparation->removeReparationChantier($reparationChantier);
            }
        }
        
        foreach ($listChantier as $chantier) {
            $reparationChantier = new ReparationChantier();
            $reparationChantier->setChantier($chantier->getChantier());
            $this->calculReparation($reparationChantier, $chantier, $pvByPa, $paTotReCalc, $gainDefTot);
            $outilsReparation->addReparationChantier($reparationChantier);
        }
        
        $outilsReparation->setPaTot($paTotReCalc)
                         ->setGainDef($gainDefTot);
    }
}