<?php

namespace App\Service\Encyclopedie;

use App\Entity\ChantierPrototype;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChantiersHandler
{
    
    public function __construct(protected TranslatorInterface $translator)
    {
    }
    
    /**
     * @param ChantierPrototype[] $categories
     * @param ChantierPrototype[] $chantiersApplatis
     * @return void
     */
    public function aplatirChantiers(array $categories, array &$chantiersApplatis = []): void
    {
        foreach ($categories as $categorie) {
            $chantiersApplatis[] = $categorie;
            
            if (!empty($categorie->getChildren())) {
                $this->aplatirChantiers($categorie->getChildren()->toArray(), $chantiersApplatis);
            }
        }
    }
    
    /**
     * @param ChantierPrototype[] $chantiers
     * @return void
     */
    public function recalculOrderBy(array &$chantiers): void
    {
        $order = 0;
        foreach ($chantiers as $chantier) {
            $chantier->setOrderByListing($order++);
        }
        
        foreach ($chantiers as $chantier) {
            if (!empty($chantier->getChildren())) {
                $chantiersChildren = $chantier->getChildren()->toArray();
                $this->recalculOrderBy($chantiersChildren);
            }
        }
    }
    
    public function recurciveDeterministeCategory(ChantierPrototype $chantierPrototype): ChantierPrototype
    {
        if ($chantierPrototype->getParent() !== null) {
            return $this->recurciveDeterministeCategory($chantierPrototype->getParent());
        } else {
            return $chantierPrototype;
        }
    }
    
}