<?php

namespace App\Service\Generality;

use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\Journal;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class StatsHandler
{
    
    public function __construct(
        protected EntityManagerInterface $em,
        protected TranslatorInterface    $translator,
    )
    {
    }
    
    public function getBatiments(?int $saison, ?string $phase): array
    {
        
        // On récupère tous les batiments trouvés via le repository zonemap en faisant une jointure
        $statBatiment = $this->em->getRepository(ZoneMap::class)->recuperationStatBatimentByVille($saison, $phase);
        
        // retravaille du tableau pour faire le total, et n'avoir qu'une ligne par batiment pour le tableau de stats
        $batArray   = [];
        $totalArray = [
            'nbr'      => 0,
            'nbrRne'   => 0,
            'nbrRe'    => 0,
            'nbrPande' => 0,
        ];
        
        foreach ($statBatiment as $batiment) {
            if (!isset($batArray[$batiment['id']])) {
                $batArray[$batiment['id']] = [
                    'nom'      => $this->translator->trans($batiment['nom'], [], 'bats'),
                    'nbr'      => 0,
                    'nbrRne'   => 0,
                    'nbrRe'    => 0,
                    'nbrPande' => 0,
                ];
            }
            $batArray[$batiment['id']]['nbr'] += $batiment['nbr'];
            $totalArray['nbr']                += $batiment['nbr'];
            if ($batiment['typeVille'] == 'RNE') {
                $batArray[$batiment['id']]['nbrRne'] = $batiment['nbr'];
                $totalArray['nbrRne']                += $batiment['nbr'];
            } elseif ($batiment['typeVille'] == 'RE') {
                $batArray[$batiment['id']]['nbrRe'] = $batiment['nbr'];
                $totalArray['nbrRe']                += $batiment['nbr'];
            } elseif ($batiment['typeVille'] == 'Pande') {
                $batArray[$batiment['id']]['nbrPande'] = $batiment['nbr'];
                $totalArray['nbrPande']                += $batiment['nbr'];
            }
        }
        
        return [
            'tabsRuins' => $batArray,
            'total'     => $totalArray,
        ];
        
    }
    
    public function getBuildings(?int $saison, ?string $phase): array
    {
        
        // On récupère tous les chantiers construits via le repository chantiers en faisant une jointure
        $statChantier = $this->em->getRepository(Chantiers::class)->statsChantierByTypeVille($saison, $phase);
        
        // retravaille du tableau pour faire le total, et n'avoir qu'une ligne par chantier pour le tableau de stats
        $chantierArray = [];
        $totalArray    = [
            'nbr'      => 0,
            'nbrRne'   => 0,
            'nbrRe'    => 0,
            'nbrPande' => 0,
        ];
        
        foreach ($statChantier as $chantier) {
            if (!isset($chantierArray[$chantier['id']])) {
                $chantierArray[$chantier['id']] = [
                    'nom'      => $this->translator->trans($chantier['nom'], [], 'chantiers'),
                    'nbr'      => 0,
                    'nbrRne'   => 0,
                    'nbrRe'    => 0,
                    'nbrPande' => 0,
                ];
            }
            $chantierArray[$chantier['id']]['nbr'] += $chantier['nbr'];
            $totalArray['nbr']                     += $chantier['nbr'];
            if ($chantier['typeVille'] == 'RNE') {
                $chantierArray[$chantier['id']]['nbrRne'] = $chantier['nbr'];
                $totalArray['nbrRne']                     += $chantier['nbr'];
            } elseif ($chantier['typeVille'] == 'RE') {
                $chantierArray[$chantier['id']]['nbrRe'] = $chantier['nbr'];
                $totalArray['nbrRe']                     += $chantier['nbr'];
            } elseif ($chantier['typeVille'] == 'Pande') {
                $chantierArray[$chantier['id']]['nbrPande'] = $chantier['nbr'];
                $totalArray['nbrPande']                     += $chantier['nbr'];
            }
        }
        
        return [
            'tabsWork' => $chantierArray,
            'total'    => $totalArray,
        ];
        
    }
    
    public function getJobs(?int $saison, ?string $phase): array
    {
        $jobs = $this->em->getRepository(Citoyens::class)->statistiqueJobs($saison, $phase);
        
        // rework array to have a better display in the chart (array of array)
        $jobsArray['legend'] = array_map(fn($i) => $this->translator->trans($i['nom'], [], 'game'), $jobs);
        $jobsArray['nbr']    = array_map(fn($i) => $i['nbr'], $jobs);
        $jobsArray['total']  = array_map(fn($i) => $i['total'], $jobs);
        
        return $jobsArray;
    }
    
    public function getListSaison(): array
    {
        return [
            [
                'idSaison' => -1,
                'nom'      => $this->translator->trans('Toutes les saisons', [], 'stats'),
            ],
            [
                'idSaison' => 150,
                'nom'      => $this->translator->trans('Saison 15 Alpha', [], 'stats'),
            ],
            [
                'idSaison' => 151,
                'nom'      => $this->translator->trans('Saison 15 Beta', [], 'stats'),
            ],
            [
                'idSaison' => 152,
                'nom'      => $this->translator->trans('Saison 15', [], 'stats'),
            ],
            [
                'idSaison' => 160,
                'nom'      => $this->translator->trans('Saison 16', [], 'stats'),
            ],
            [
                'idSaison' => 170,
                'nom'      => $this->translator->trans('Saison 17', [], 'stats'),
            ],
        ];
    }
    
    public function getMort(?int $saison, ?string $phase): array
    {
        $statsDeath = $this->em->getRepository(Citoyens::class)->statistiqueMort($saison, $phase);
        
        $statsDeathFinal = [];
        // calcul de la moyenne de mort par jour
        foreach ($statsDeath as $value) {
            $statsDeathFinal[$value['Jour']][] = $value['NombreMorts'];
        }
        
        ksort($statsDeathFinal);
        
        $jour = array_keys($statsDeathFinal);
        
        // rework array to have a better display in the chart (array of array)
        $themeArray['legend'] = $jour;
        $themeArray['nbr']    = $statsDeathFinal;
        
        return $themeArray;
    }
    
    public function getPower(): array
    {
        $usersPower = $this->em->getRepository(User::class)->statistiquePowerUser();
        
        // rework array to have a better display in the chart (array of array)
        $powerArray['legend'] = array_map(fn($i) => $this->translator->trans($i['nom'], [], 'game'), $usersPower);
        $powerArray['nbr']    = array_map(fn($i) => $i['nbr'], $usersPower);
        
        return $powerArray;
    }
    
    public function getScrutateurStats(?int $saison, ?string $phase): array
    {
        // On récupère toutes les directions de scrutateur connues via le repository chantiers en faisant une jointure
        $statScrutateur = $this->em->getRepository(Journal::class)->statsScrutateur($saison, $phase);
        
        // retravaille du tableau pour faire le total, et n'avoir qu'une ligne par direction pour le tableau de stats
        $scrutArray = [];
        $totalArray = [
            'nbr'      => 0,
            'nbrRne'   => 0,
            'nbrRe'    => 0,
            'nbrPande' => 0,
        ];
        
        foreach ($statScrutateur as $journal) {
            if ($journal['direction'] !== null) {
                $direction = '';
                switch ($journal['direction']) {
                    case 'le nord':
                    case 'le Nord':
                        $direction = $this->translator->trans('Nord', [], 'hotel');
                        break;
                    case 'le sud':
                    case 'le Sud':
                        $direction = $this->translator->trans('Sud', [], 'hotel');
                        break;
                    case "l'est":
                    case "l'Est":
                        $direction = $this->translator->trans('Est', [], 'hotel');
                        break;
                    case "l'ouest":
                    case "l'Ouest":
                        $direction = $this->translator->trans('Ouest', [], 'hotel');
                        break;
                    case "le nord-est":
                    case "le Nord-Est":
                    case "le Nord-est":
                    case "le nord-Est":
                        $direction = $this->translator->trans('Nord-est', [], 'hotel');
                        break;
                    case "le nord-ouest":
                    case "le Nord-Ouest":
                    case "le nord-Ouest":
                    case "le Nord-ouest":
                        $direction = $this->translator->trans('Nord-ouest', [], 'hotel');
                        break;
                    case "le sud-est":
                    case "le Sud-Est":
                    case "le sud-Est":
                    case "le Sud-est":
                        $direction = $this->translator->trans('Sud-est', [], 'hotel');
                        break;
                    case "le sud-ouest":
                    case "le Sud-Ouest":
                    case "le sud-Ouest":
                    case "le Sud-ouest":
                        $direction = $this->translator->trans('Sud-ouest', [], 'hotel');
                        break;
                }
                
                if (!isset($scrutArray[$direction])) {
                    $scrutArray[$direction] = [
                        'nom'      => $direction,
                        'nbr'      => 0,
                        'nbrRne'   => 0,
                        'nbrRe'    => 0,
                        'nbrPande' => 0,
                    ];
                }
                $scrutArray[$direction]['nbr'] += $journal['nbr'];
                $totalArray['nbr']             += $journal['nbr'];
                if ($journal['typeVille'] == 'RNE') {
                    $scrutArray[$direction]['nbrRne'] = $journal['nbr'];
                    $totalArray['nbrRne']             += $journal['nbr'];
                } elseif ($journal['typeVille'] == 'RE') {
                    $scrutArray[$direction]['nbrRe'] = $journal['nbr'];
                    $totalArray['nbrRe']             += $journal['nbr'];
                } elseif ($journal['typeVille'] == 'Pande') {
                    $scrutArray[$direction]['nbrPande'] = $journal['nbr'];
                    $totalArray['nbrPande']             += $journal['nbr'];
                }
            }
            
        }
        
        return [
            'tabsScrut' => $scrutArray,
            'total'     => $totalArray,
        ];
    }
    
    public function getThemes(): array
    {
        $usersTheme = $this->em->getRepository(User::class)->statistiqueThemeUserActif();
        
        // rework array to have a better display in the chart (array of array)
        $themeArray['legend'] = array_map(fn($i) => $i['theme'], $usersTheme);
        $themeArray['nbr']    = array_map(fn($i) => $i['nbr'], $usersTheme);
        
        return $themeArray;
    }
    
    public function getTypeMort(?int $saison, ?string $phase): array
    {
        $statsDeath = $this->em->getRepository(Citoyens::class)->statistiqueTypeMort($saison, $phase);
        
        // rework array to have a better display in the chart (array of array)
        $deatArray['legend'] = array_map(fn($i) => $this->translator->trans($i['label'], [], 'game'), $statsDeath);
        $deatArray['nbr']    = array_map(fn($i) => $i['nbr'], $statsDeath);
        
        return $deatArray;
    }
    
    public function getTypeVille(?int $saison, ?string $phase): array
    {
        
        $countRne   = $this->em->getRepository(Ville::class)->countRne($saison, $phase);
        $countRe    = $this->em->getRepository(Ville::class)->countRe($saison, $phase);
        $countPande = $this->em->getRepository(Ville::class)->countPande($saison, $phase);
        
        $statsTypeFinal[$this->translator->trans('RNE', [], 'game')]   = $countRne;
        $statsTypeFinal[$this->translator->trans('RE', [], 'game')]    = $countRe;
        $statsTypeFinal[$this->translator->trans('Pande', [], 'game')] = $countPande;
        
        // rework array to have a better display in the chart (array of array)
        $themeArray['legend'] = ['RNE', 'RE', 'Pande'];
        $themeArray['nbr']    = $statsTypeFinal;
        
        return $themeArray;
    }
    
    public function recuperationStatVilleByDay(?int $saison, ?string $phase): array
    {
        $statsRne   = $this->em->getRepository(Ville::class)->countRNEByDay($saison, $phase);
        $statsRe    = $this->em->getRepository(Ville::class)->countREByDay($saison, $phase);
        $statsPande = $this->em->getRepository(Ville::class)->countPandeByDay($saison, $phase);
        
        // mise en forme des tableaux pour utiliser les jours comme index
        $statsVilleRne   = [];
        $statsVilleRe    = [];
        $statsVillePande = [];
        
        foreach ($statsRne as $value) {
            $statsVilleRne[$value['jour']] = $value['nb'];
        }
        
        foreach ($statsRe as $value) {
            $statsVilleRe[$value['jour']] = $value['nb'];
        }
        
        foreach ($statsPande as $value) {
            $statsVillePande[$value['jour']] = $value['nb'];
        }
        
        $statsVilleFinal = [];
        // On va fusionner les 3 tableaux pour avoir un seul tableau avec les 3 types de villes et surtout des jours identiques, si un type de ville n'a pas de valeur pour un jour, on met 0
        $maxJour = max(max(array_keys($statsVilleRne)), max(array_keys($statsVilleRe)), max(array_keys($statsVillePande)));
        $jour    = [];
        for ($i = 0; $i <= $maxJour; $i++) {
            $statsVilleFinal[0][$i]  = $statsVilleRne[$i] ?? 0;
            $statsVilleFinal[1] [$i] = $statsVilleRe[$i] ?? 0;
            $statsVilleFinal[2][$i]  = $statsVillePande[$i] ?? 0;
            $jour[]                  = $i;
        }
        
        ksort($statsVilleFinal);
        
        // rework array to have a better display in the chart (array of array)
        $themeArray['legend']       = $jour;
        $themeArray['nbr']          = $statsVilleFinal;
        $themeArray['total']        = [
            0 => array_sum($statsVilleRne),
            1 => array_sum($statsVilleRe),
            2 => array_sum($statsVillePande),
        ];
        $themeArray['legend_titre'] = [
            0 => $this->translator->trans("RNE", [], 'game'),
            1 => $this->translator->trans("RE", [], 'game'),
            2 => $this->translator->trans("Pande", [], 'game'),
        ];
        
        return $themeArray;
    }
    
    public function returnSaisonPhase(?int $saison): array
    {
        switch ($saison) {
            case 150:
                $saison = 15;
                $phase  = 'alpha';
                break;
            case 151:
                $saison = 15;
                $phase  = 'beta';
                break;
            case 152:
                $saison = 15;
                $phase  = 'native';
                break;
            case 160:
                $saison = 16;
                $phase  = 'native';
                break;
            case 170:
                $saison = 17;
                $phase  = 'native';
                break;
            default:
                $saison = null;
                $phase  = null;
        }
        
        return [
            'saison' => $saison,
            'phase'  => $phase,
        ];
    }
    
}