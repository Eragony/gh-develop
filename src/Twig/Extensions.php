<?php

namespace App\Twig;

use App\Entity\ItemPrototype;
use App\Entity\Jump;
use App\Entity\TypeObjet;
use App\Entity\User;
use App\Entity\VersionsSite;
use App\Service\GestHordesHandler;
use App\Service\Outils\ChantierHandler;
use App\Service\UserHandler;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\UpGradeHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class Extensions extends AbstractExtension implements GlobalsInterface
{
    
    public function __construct(
        private readonly UserHandler            $u,
        private readonly GestHordesHandler      $gh,
        private readonly EntityManagerInterface $em,
        private readonly ChantierHandler        $c,
        private readonly UpGradeHandler         $up,
        private readonly LocalFormatterUtils    $lfu,
        private readonly TranslatorInterface    $trans,
    )
    {
    }
    
    public function affIcon(ItemPrototype $item): string
    {
        
        $html = '';
        if (($item->getTypeObjet()?->getId() ?? 0) === TypeObjet::MARQUEUR) {
            $classObj[]   = 'videImg4';
            $typeObjet [] = $this->trans->trans('(Marqueur)', [], 'items');
            $type[]       = 4;
        } elseif ($item->getId() >= 2000 && $item->getId() < 3000) {
            $classObj[]   = 'videImg3';
            $typeObjet [] = $this->trans->trans('empoisonné(e)', [], 'items');
            $type[]       = 3;
        } else {
            $classObj[]   = 'videImg';
            $typeObjet [] = '';
            $type[]       = 1;
            if (($item->getTypeObjet()?->getId() ?? 0) === TypeObjet::CASSABLE) {
                $classObj[]   = 'videImg2';
                $typeObjet [] = $this->trans->trans('cassé(e)', [], 'items');
                $type[]       = 2;
            }
        }
        
        $nom = htmlspecialchars($this->trans->trans($item->getNom(), [], 'items'), ENT_QUOTES);
        
        foreach ($classObj as $key => $class) {
            
            $infoBulle = "<span class='info'>{$nom} {$typeObjet[$key]} (id : {$item->getId()})</span>";
            
            $data = "data-name='{$nom}' data-type='{$type[$key]}' data-id='{$item->getId()}'";
            
            $html .= "<div class='videListing' {$data}><span class='{$class}'><span class='infoBulle'>{$this->gh->svgImg($item->getIcon())}{$infoBulle}</span></span></div>";
        }
        
        return $html;
        
    }
    
    public function affNbrIcon(ItemPrototype $item, int $nbr, bool $broked = false): string
    {
        
        $typeName = '';
        $type     = 1;
        
        if (($item->getTypeObjet()?->getId() ?? 0) === TypeObjet::MARQUEUR) {
            $classImage = 'videImg4';
            $typeName   = $this->trans->trans('(Marqueur)', [], 'items');
            $type       = 4;
        } else {
            if ($item->getId() >= 2000 && $item->getId() < 3000) {
                $classImage = 'videImg3';
                $typeName   = $this->trans->trans('empoisonné(e)', [], 'items');
                $type       = 3;
            } else {
                if ($broked) {
                    $classImage = 'videImg2';
                    $typeName   = $this->trans->trans('cassé(e)', [], 'items');
                    $type       = 2;
                } else {
                    $classImage = 'videImg';
                }
            }
        }
        
        $nom = htmlspecialchars($this->trans->trans($item->getNom(), [], 'items'), ENT_QUOTES);
        
        $infoBulle = "<span class='info'>{$nom} {$typeName} (id : {$item->getId()})</span>";
        
        $data = "data-name='{$nom}' data-type='{$type}' data-id='{$item->getId()}' data-nbr={$nbr}";
        
        return "<div class='videItem' {$data}><span class='nbrItems'>{$nbr}</span><span class='{$classImage}'><span class='infoBulle'>{$this->gh->svgImg($item->getIcon())}{$infoBulle}</span></span></div>";
    }
    
    public function format_filesize(int $size): string
    {
        if ($size >= 1_099_511_627_776) {
            return round($size / 1_099_511_627_776, 0) . ' TB';
        } elseif ($size >= 1_073_741_824) {
            return round($size / 1_073_741_824, 1) . ' GB';
        } elseif ($size >= 1_048_576) {
            return round($size / 1_048_576, 2) . ' MB';
        } elseif ($size >= 1024) {
            return round($size / 1024, 0) . ' KB';
        } else {
            return $size . ' B';
        }
    }
    
    public function getFilters(): array
    {
        return [
            new TwigFilter('filesize', $this->format_filesize(...)),
        ];
    }
    
    public function getFunctions(): array
    {
        return [
            new TwigFunction('svgImg', $this->gh->svgImg(...), ['is_safe' => ['html']]),
            new TwigFunction('affNbrIcon', $this->affNbrIcon(...), ['is_safe' => ['html']]),
            new TwigFunction('affIcon', $this->affIcon(...), ['is_safe' => ['html']]),
            new TwigFunction('getLibCoord', $this->gh->getLibCoord(...), ['is_safe' => ['html']]),
            new TwigFunction('getVersionSite', $this->getVersionSite(...), ['is_safe' => ['html']]),
            new TwigFunction('getMajJHSite', $this->getMajJHSite(...), ['is_safe' => ['html']]),
            new TwigFunction('getCoordNorm', $this->gh->getCoordNorm(...), ['is_safe' => ['html']]),
            new TwigFunction('isLeadOrOrga', $this->u->isLeadOrOrga(...), ['is_safe' => ['html']]),
            new TwigFunction('isOrga', $this->u->isOrga(...), ['is_safe' => ['html']]),
            new TwigFunction('getPlan', $this->c->returnPlanChantier(...), ['is_safe' => ['html']]),
            new TwigFunction('getHabilitationGestion', $this->getHabilitationGestion(...), ['is_safe' => ['html']]),
            new TwigFunction('getFormatedDateHeure', $this->lfu->getFormattedDateHeure(...), ['is_safe' => ['html']]),
            new TwigFunction('getFormattedDateMoisJour', $this->lfu->getFormattedDateMoisJour(...),
                             ['is_safe' => ['html']]),
        ];
    }
    
    #[ArrayShape([
        'infoVille'         => "\App\Entity\Ville|null",
        'inVille'           => "bool",
        'isLead'            => "bool",
        'visitVille'        => "bool",
        'title_site'        => "mixed",
        'gh'                => GestHordesHandler::class,
        'chantiers_handler' => ChantierHandler::class,
        'upGrade'           => UpGradeHandler::class,
    ])] public function getGlobals(): array
    {
        return [
            'infoVille'         => $this->u->getTownBySession(),
            'isLead'            => $this->u->isLead(),
            'inVille'           => $this->u->inVille(),
            'visitVille'        => $this->u->visitVille(),
            'title_site'        => $_ENV['TITRE_PAGE'],
            'gh'                => $this->gh,
            'chantiers_handler' => $this->c,
            'upGrade'           => $this->up,
        ];
    }
    
    public function getHabilitationGestion(Jump $jump, User $user): string
    {
        $jumpRepo = $this->em->getRepository(Jump::class);
        $retour   = '';
        
        if ($jump->getEvent() !== null) {
            
            $retour = ($jumpRepo->isOrgaEvent($jump, $user->getId())) ? "Orga" : "";
            
        }
        
        $retour .= ($jumpRepo->isGestionnaire($jump, $user->getId())) ? "Gest" : "";
        
        return $retour;
        
    }
    
    public function getMajJHSite(): string
    {
        /** @var VersionsSite $lastVersion */
        $lastVersion = $this->em->getRepository(VersionsSite::class)->last();
        
        $date = $lastVersion !== null ? $lastVersion->getDateVersion() : new DateTime();
        
        return $this->lfu->getFormattedDateHeure($date);
    }
    
    public function getVersionSite(): string
    {
        $lastVersion = $this->em->getRepository(VersionsSite::class)->last();
        
        if ($lastVersion === null) {
            return "unknown";
        }
        
        if ($lastVersion->getTagName() !== null) {
            return "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}-{$lastVersion->getTagName()}.{$lastVersion->getNumTag()}";
        } else {
            return "{$lastVersion->getVersionMajeur()}.{$lastVersion->getVersionMineur()}.{$lastVersion->getVersionCorrective()}";
        }
        
    }
    
}