<?php


namespace App\Translation;


use App\Entity\BatPrototype;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\CleanUpCadaver;
use App\Entity\ContentsVersion;
use App\Entity\CreneauHorraire;
use App\Entity\EtatPrototype;
use App\Entity\ExpeditionType;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillPrototype;
use App\Entity\HerosSkillType;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\LevelRuinePrototype;
use App\Entity\MenuPrototype;
use App\Entity\NewsSite;
use App\Entity\ObjectifVillePrototype;
use App\Entity\PictoPrototype;
use App\Entity\PictoTitrePrototype;
use App\Entity\RegroupementItemsDecharge;
use App\Entity\RoleJump;
use App\Entity\StatutInscription;
use App\Entity\StatutUser;
use App\Entity\TypeActionAssemblage;
use App\Entity\TypeCaracteristique;
use App\Entity\TypeDeath;
use App\Entity\TypeDispo;
use App\Entity\TypeDispoJump;
use App\Entity\TypeLead;
use App\Entity\TypeObjet;
use App\Entity\TypeVille;
use App\Entity\UpChantierPrototype;
use App\Entity\UpHomePrototype;
use App\Entity\VersionsSite;
use App\Service\TranslationConfigGlobal;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\Extractor\ExtractorInterface;
use Symfony\Component\Translation\MessageCatalogue;

class DatabaseExtractor implements ExtractorInterface
{
    
    protected static $has_been_run = false;
    protected string $prefix;
    
    public function __construct(protected EntityManagerInterface $em, protected TranslationConfigGlobal $conf)
    {
    }
    
    public function extract(iterable|string $resource, MessageCatalogue $catalogue): void
    {
        if (self::$has_been_run) {
            return;
        }
        self::$has_been_run = true;
        // Traduction pour les items
        foreach ($this->em->getRepository(ItemPrototype::class)->findAll() as $item) {
            $this->insert($catalogue, $item->getNom(), 'items', ItemPrototype::class);
            $this->insert($catalogue, $item->getDescription(), 'items', ItemPrototype::class);
        }
        foreach ($this->em->getRepository(CategoryObjet::class)->findAll() as $cate) {
            $this->insert($catalogue, $cate->getNom(), 'items', CategoryObjet::class);
        }
        foreach ($this->em->getRepository(TypeObjet::class)->findAll() as $type) {
            $this->insert($catalogue, $type->getNom(), 'items', TypeObjet::class);
        }
        foreach ($this->em->getRepository(TypeActionAssemblage::class)->findAll() as $type) {
            $this->insert($catalogue, $type->getNom(), 'items', TypeActionAssemblage::class);
            $this->insert($catalogue, $type->getNomItemNeed(), 'items', TypeActionAssemblage::class);
            $this->insert($catalogue, $type->getNomItemObtain(), 'items', TypeActionAssemblage::class);
        }
        foreach ($this->em->getRepository(TypeCaracteristique::class)->findAll() as $type) {
            $this->insert($catalogue, $type->getNom(), 'items', TypeCaracteristique::class);
        }
        
        // Traduction pour les batiments
        foreach ($this->em->getRepository(BatPrototype::class)->findAll() as $bat) {
            $this->insert($catalogue, $bat->getNom(), 'bats', BatPrototype::class);
            $this->insert($catalogue, $bat->getDescription(), 'bats', BatPrototype::class);
        }
        
        // Traduction pour les chantiers
        foreach ($this->em->getRepository(ChantierPrototype::class)->findAll() as $chantier) {
            $this->insert($catalogue, $chantier->getNom(), 'chantiers', ChantierPrototype::class);
            $this->insert($catalogue, $chantier->getDescription(), 'chantiers', ChantierPrototype::class);
        }
        foreach ($this->em->getRepository(UpChantierPrototype::class)->findAll() as $upChantier) {
            $this->insert($catalogue, $upChantier->getValeurUp(), 'chantiers', UpChantierPrototype::class);
        }
        
        // Traduction "jeu"
        foreach ($this->em->getRepository(CleanUpCadaver::class)->findAll() as $clean) {
            $this->insert($catalogue, $clean->getLabel(), 'game', CleanUpCadaver::class);
        }
        foreach ($this->em->getRepository(EtatPrototype::class)->findAll() as $etat) {
            $this->insert($catalogue, $etat->getNom(), 'game', EtatPrototype::class);
        }
        foreach ($this->em->getRepository(HomePrototype::class)->findAll() as $home) {
            $this->insert($catalogue, $home->getNom(), 'game', HomePrototype::class);
        }
        foreach ($this->em->getRepository(HerosPrototype::class)->findAll() as $hero) {
            $this->insert($catalogue, $hero->getNom(), 'game', HerosPrototype::class);
            $this->insert($catalogue, $hero->getDescription(), 'game', HerosPrototype::class);
        }
        foreach ($this->em->getRepository(HerosSkillType::class)->findAll() as $hero) {
            $this->insert($catalogue, $hero->getName(), 'game', HerosSkillType::class);
        }
        foreach ($this->em->getRepository(HerosSkillLevel::class)->findAll() as $hero) {
            $this->insert($catalogue, $hero->getName(), 'game', HerosSkillLevel::class);
        }
        foreach ($this->em->getRepository(HerosSkillPrototype::class)->findAll() as $hero) {
            $this->insert($catalogue, $hero->getText(), 'game', HerosSkillPrototype::class);
        }
        foreach ($this->em->getRepository(JobPrototype::class)->findAll() as $job) {
            $this->insert($catalogue, $job->getNom(), 'game', JobPrototype::class);
            $this->insert($catalogue, $job->getAlternatif(), 'jump', JobPrototype::class);
            $this->insert($catalogue, $job->getDescription(), 'game', JobPrototype::class);
        }
        foreach ($this->em->getRepository(PictoPrototype::class)->findAll() as $picto) {
            $this->insert($catalogue, $picto->getName(), 'game', PictoPrototype::class);
            $this->insert($catalogue, $picto->getDescription(), 'game', PictoPrototype::class);
        }
        foreach ($this->em->getRepository(PictoTitrePrototype::class)->findAll() as $titre) {
            $this->insert($catalogue, $titre->getTitre(), 'game', PictoTitrePrototype::class);
        }
        foreach ($this->em->getRepository(TypeDeath::class)->findAll() as $mort) {
            $this->insert($catalogue, $mort->getNom(), 'game', TypeDeath::class);
            $this->insert($catalogue, $mort->getLabel(), 'game', TypeDeath::class);
        }
        foreach ($this->em->getRepository(TypeVille::class)->findAll() as $ville) {
            $this->insert($catalogue, $ville->getNom(), 'game', TypeVille::class);
            $this->insert($catalogue, $ville->getAbrev(), 'game', TypeVille::class);
        }
        foreach ($this->em->getRepository(UpHomePrototype::class)->findAll() as $upHome) {
            $this->insert($catalogue, $upHome->getNom(), 'game', UpHomePrototype::class);
            $this->insert($catalogue, $upHome->getLabel(), 'game', UpHomePrototype::class);
            // $this->insert($catalogue, $upHome->getDescription(), 'game', UpHomePrototype::class);
        }
        
        
        // Traduction "outils"
        foreach ($this->em->getRepository(RegroupementItemsDecharge::class)->findAll() as $rgpmt) {
            $this->insert($catalogue, $rgpmt->getNom(), 'outils', RegroupementItemsDecharge::class);
        }
        foreach ($this->em->getRepository(ExpeditionType::class)->findAll() as $expe) {
            $this->insert($catalogue, $expe->getNom(), 'outils', ExpeditionType::class);
            $this->insert($catalogue, $expe->getDescription(), 'outils', ExpeditionType::class);
        }
        
        // Traduction "jump"
        foreach ($this->em->getRepository(CreneauHorraire::class)->findAll() as $creneau) {
            $this->insert($catalogue, $creneau->getLibelle(), 'jump', CreneauHorraire::class);
        }
        foreach ($this->em->getRepository(ObjectifVillePrototype::class)->findAll() as $objectif) {
            $this->insert($catalogue, $objectif->getNom(), 'jump', ObjectifVillePrototype::class);
        }
        foreach ($this->em->getRepository(LevelRuinePrototype::class)->findAll() as $ruine) {
            $this->insert($catalogue, $ruine->getNom(), 'jump', LevelRuinePrototype::class);
            $this->insert($catalogue, $ruine->getDescription(), 'jump', LevelRuinePrototype::class);
        }
        foreach ($this->em->getRepository(RoleJump::class)->findAll() as $role) {
            $this->insert($catalogue, $role->getNom(), 'jump', RoleJump::class);
        }
        foreach ($this->em->getRepository(StatutUser::class)->findAll() as $statut) {
            $this->insert($catalogue, $statut->getNom(), 'jump', StatutUser::class);
        }
        foreach ($this->em->getRepository(TypeDispo::class)->findAll() as $dispo) {
            $this->insert($catalogue, $dispo->getNom(), 'jump', TypeDispo::class);
        }
        foreach ($this->em->getRepository(TypeDispoJump::class)->findAll() as $dispoJump) {
            $this->insert($catalogue, $dispoJump->getNom(), 'jump', TypeDispoJump::class);
        }
        foreach ($this->em->getRepository(TypeLead::class)->findAll() as $lead) {
            $this->insert($catalogue, $lead->getNom(), 'jump', TypeLead::class);
        }
        foreach ($this->em->getRepository(StatutInscription::class)->findAll() as $statut) {
            $this->insert($catalogue, $statut->getNom(), 'jump', StatutInscription::class);
            $this->insert($catalogue, $statut->getNomGestion(), 'jump', StatutInscription::class);
            $this->insert($catalogue, $statut->getNomGestionCourt(), 'jump', StatutInscription::class);
        }
        
        // Traduction "version"
        foreach ($this->em->getRepository(ContentsVersion::class)->findAll() as $content) {
            $this->insert($catalogue, $content->getContents(), 'version', ContentsVersion::class);
        }
        foreach ($this->em->getRepository(VersionsSite::class)->findAll() as $version) {
            $this->insert($catalogue, $version->getTitre(), 'version', VersionsSite::class);
        }
        foreach ($this->em->getRepository(NewsSite::class)->findAll() as $news) {
            $this->insert($catalogue, $news->getTitre(), 'version', VersionsSite::class);
            if ($news->getContent() !== null) {
                $this->insert($catalogue, $news->getContent(), 'version', VersionsSite::class);
            }
        }
        
        // Traduction "Menus"
        foreach($this->em->getRepository(MenuPrototype::class)->findAll() as $menu){
            $this->insert($catalogue, $menu->getLabel(), 'app', MenuPrototype::class);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function setPrefix(string $prefix): void
    {
        $this->prefix = $prefix;
    }
    
    public function insert(MessageCatalogue &$catalogue, string $message, string $domain, string $class): void
    {
        if (!empty($message)) {
            $catalogue->set($message, $this->prefix . $message, $domain);
            $this->conf->add_source_for($message, $domain, 'db', str_replace('App\\Entity\\', '', $class));
        }
    }
}