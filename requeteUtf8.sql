UPDATE old_inscrip_jump
set comIJ = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(comIJ USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
            );
UPDATE old_inscrip_jump
set retourIJ = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(retourIJ USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
               );
UPDATE old_inscrip_jump
set motiIJ = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(motiIJ USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
             );

UPDATE old_perso_option
set pseudo = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(pseudo USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
             );

UPDATE old_info_ville
set nomV = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(nomV USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
           );
UPDATE old_expe_villem
set nomExpe = CONVERT(
        CONVERT(
                CONVERT(
                        CONVERT(
                                CONVERT(nomExpe USING binary)
                                using utf8
                        )
                        using latin1
                )
                using binary
        )
        using utf8
              );